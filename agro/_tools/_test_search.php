<?php
/**
 * User: ZvvAsyA
 * Date: 23.09.15 -  13:16
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include.php");
if(!CModule::IncludeModule("iblock")) return;

$arFilter   = array( 'ID' =>302194);
$rsElements = CIBlockElement::GetList(
	array(),//
	$arFilter,
	false,
	$arNavParams,
	array(
		"ID",
		"NAME",
		"DETAIL_PICTURE",
		"PREVIEW_PICTURE", "PREVIEW_TEXT",
		"IBLOCK_SECTION_ID",
		"IBLOCK_ID",
		"PROPERTY_FIRM.ID",
		"PROPERTY_FIRM.IBLOCK_ID",
		"PROPERTY_FIRM.NAME",
		"PROPERTY_FIRM.PREVIEW_PICTURE",
		"PROPERTY_FIRM.DETAIL_PAGE_URL",
		"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
		"PROPERTY_FIRM.PROPERTY_EXT_SORT",
	)
);

while ( $arElements = $rsElements->GetNext() ) {

	if ( $arElements["PROPERTY_FIRM_PROPERTY_REMOVE_REL_VALUE"] == '��' ) {
		continue;
	}

	$picture_id                                  = $arElements['DETAIL_PICTURE'] == '' ? $arElements['PREVIEW_PICTURE'] : $arElements['DETAIL_PICTURE'];
	$arElements['PREVIEW_PICTURE']               = Resizer( $picture_id, array(
		'width'  => 50,
		'height' => 50
	), BX_RESIZE_IMAGE_PROPORTIONAL, true );
	$arElements['PROPERTY_FIRM_PREVIEW_PICTURE'] = Resizer( $arElements['PROPERTY_FIRM_PREVIEW_PICTURE'], array(
		'width'  => 50,
		'height' => 50
	), BX_RESIZE_IMAGE_PROPORTIONAL, true );

	$arResult["SEARCH_ITEMS"][ $arElements['ID'] ] = $arElements;
}

PrintAdmin($arResult);