<?
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );


$arFilter["IBLOCK_ID"] = $_REQUEST['TYPE'] == 'uslugi' ? IBLOCK_USLUGI : ($_REQUEST['TYPE'] == 'yarmarka' ? IBLOCK_YARMARKA : IBLOCK_PRODUCTS);
$arFilter["ACTIVE"]    = "Y";

$categories = explode( ',', $_POST['categories'] );
$result     = array();
foreach ( $categories as $category ) {

	$arFilter["SECTION_ID"] = $category;

	$arFilter["INCLUDE_SUBSECTIONS"]    = "Y";

	if ($_REQUEST['TYPE'] == 'yarmarka') {
		$arFilter["=PROPERTY_ACTIVE"] = false;
	}
	if (!empty($_REQUEST['region'])) {


		$arParams = $arFilter;
		$arResult["THIS_SECTION"]["ID"] = $category;
		$arParams['REGION'] = $_REQUEST['region'];

		$arResult["COMPANY_LIST"] = cacheFunction( 36000, 'COMPANY_LIST' . $category.$arParams['REGION'], function () use ( $arParams, $arResult ) {
			$res           = [ ];
		    $filter = array(
				"IBLOCK_ID"           => $arParams["IBLOCK_ID"],
				"ACTIVE"              => "Y",
				"SECTION_ID"          => $arResult["THIS_SECTION"]["ID"],
				"INCLUDE_SUBSECTIONS" => "Y"
			);


			$rsAllElements = CIBlockElement::GetList(
				array( "PROPERTY_FIRM.NAME" => "ASC" ),
				$filter,
				false,
				false,
				array(
					"ID",
					"NAME",
					"IBLOCK_SECTION_ID",
					"PROPERTY_FIRM.ID",
					"PROPERTY_FIRM.NAME",
					"PROPERTY_FIRM.DETAIL_PAGE_URL",
					"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
					"PROPERTY_FIRM.PROPERTY_YARMARKA",
					"PROPERTY_FIRM.PROPERTY_REGION"
				)
			);
			while ( $arAllElements = $rsAllElements->GetNext() ) {
				//TODO: ������
				if ( empty($arAllElements["PROPERTY_FIRM_ID"]) ) {
					continue;
				}
				if ( empty($arAllElements["PROPERTY_FIRM_PROPERTY_YARMARKA_VALUE"]) ) {
					continue;
				}
				if ( $arAllElements["PROPERTY_FIRM_PROPERTY_REGION_VALUE"] != intval($arParams['REGION']) ) {
					continue;
				}
				//���������� � ��������
				if ( ! isset( $res[ $arAllElements["PROPERTY_FIRM_ID"] ] ) ) {

					$res[ $arAllElements["PROPERTY_FIRM_ID"] ] = array(
						"ID"              => $arAllElements["PROPERTY_FIRM_ID"],
						"NAME"            => $arAllElements["PROPERTY_FIRM_NAME"],
						"DETAIL_PAGE_URL" => $arAllElements["PROPERTY_FIRM_DETAIL_PAGE_URL"],
						"ITEMS_COUNT"     => 1
					);
				} else {
					$res[ $arAllElements["PROPERTY_FIRM_ID"] ]["ITEMS_COUNT"] ++;
				}

			}

			return $res;
		} );
		if (!empty($arResult["COMPANY_LIST"])){
			$arFilter['PROPERTY_FIRM'] =[];
			foreach($arResult["COMPANY_LIST"] as $key=>$item)
				$arFilter['PROPERTY_FIRM'] []= $key;
		} else {
			$arFilter['PROPERTY_FIRM']= 0;
		}

	}

	$res = CIBlockElement::GetList(
		false,
		$arFilter,
		array( 'IBLOCK_ID' )
	);

	if ( $el = $res->Fetch() ) {
		$result[ $category ] = $el['CNT'];
	} else {
		$result[ $category ] = 'remove';
	}
}

echo json_encode( $result );
