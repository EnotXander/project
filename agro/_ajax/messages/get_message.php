<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


if (!CModule::IncludeModule("socialnetwork"))
{
   ShowError(GetMessage("SONET_MODULE_NOT_INSTALL"));
   return;
}

global $APPLICATION, $USER, $DB;

$userId = (int)$_REQUEST["user_id"];
$from = (int)$_REQUEST["from"];
if(!$from) $from = 0;
$isError = false;

$APPLICATION->IncludeComponent("whipstudio:socialnetwork.messages_chat", "ajax", array(
        "USER_ID" => $userId,
        "FROM" => $from,
        "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",
        "SET_TITLE" => "Y"
    )
);