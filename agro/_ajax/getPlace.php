<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include.php");?>

<?
$extID = (int)$_POST["ext_id"];
$type = $_POST["type"];

$arPlaces = array();
if(!CModule::IncludeModule("iblock")) return;
if($type == "region")
{
   $title = "������";
   $iblock_id = 22;
   $rsPlace = CIBlockElement::GetList(
      array("SORT" => "ASC", "NAME" => "ASC"),
      array(
         "IBLOCK_ID" => $iblock_id,
         "ACTIVE" => "Y",
         "PROPERTY_COUNTRY" => $extID
      ),
      false,
      false,
      array("ID", "NAME"));
   while($arPlace = $rsPlace->GetNext())
   {
      $arPlaces[] = $arPlace;
   }
}
elseif($type == "city")
{
   $title = "�����";
   $iblock_id = 23;
   $rsPlace = CIBlockElement::GetList(
      array("SORT" => "ASC", "NAME" => "ASC"),
      array(
         "IBLOCK_ID" => $iblock_id,
         "ACTIVE" => "Y",
         "PROPERTY_REGION" => $extID
      ),
      false,
      false,
      array("ID", "NAME"));
   while($arPlace = $rsPlace->GetNext())
   {
      $arPlaces[] = $arPlace;
   }
}
else return;

$response = array();
$response["listOptionHTML"] = array();
?>

<?
$response["listOptionHTML"][] = "<option value='0' selected>�������� {$title}</option>";

foreach($arPlaces as $arPlace)
{
   $html = "<option class='del' value='{$arPlace["ID"]}'>{$arPlace["NAME"]}</option>";
   $response["listOptionHTML"][] = $html;
}
$response["WWW"] = $arPlaces;
echo json_encode_cyr(convert('cp1251', 'utf-8', $response));

function convert($from, $to, $var)
{
    if (is_array($var))
    {
        $new = array();
        foreach ($var as $key => $val)
        {
            $new[convert($from, $to, $key)] = convert($from, $to, $val);
        }
        $var = $new;
    }
    else if (is_string($var))
    {
        $var = iconv($from, $to, $var);
    }
    return $var;
}

function json_encode_cyr($str) {
	$arr_replace_utf = array('\u0410', '\u0430','\u0411','\u0431','\u0412','\u0432',
	'\u0413','\u0433','\u0414','\u0434','\u0415','\u0435','\u0401','\u0451','\u0416',
	'\u0436','\u0417','\u0437','\u0418','\u0438','\u0419','\u0439','\u041a','\u043a',
	'\u041b','\u043b','\u041c','\u043c','\u041d','\u043d','\u041e','\u043e','\u041f',
	'\u043f','\u0420','\u0440','\u0421','\u0441','\u0422','\u0442','\u0423','\u0443',
	'\u0424','\u0444','\u0425','\u0445','\u0426','\u0446','\u0427','\u0447','\u0428',
	'\u0448','\u0429','\u0449','\u042a','\u044a','\u042b','\u044b','\u042c','\u044c',
	'\u042d','\u044d','\u042e','\u044e','\u042f','\u044f');
	$arr_replace_cyr = array('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	'�', '�', '�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�',
	'�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�',
	'�','�','�','�','�','�','�','�','�','�','�','�','�','�');
	$str1 = json_encode($str);
	$str2 = str_replace($arr_replace_utf,$arr_replace_cyr,$str1);
	return $str2;
}
?>