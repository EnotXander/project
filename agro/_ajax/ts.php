<?php
/**
 * User: ZvvAsyA
 * Date: 18.08.15 -  15:37
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$arrFilter = [];
$arFilter                  = array();
$arFilter["IBLOCK_ID"]     = 81;
$arFilter["ACTIVE"]        = "Y";
$arFilter["SECTION_ID"]    = 5068;
//$arFilter["PROPERTY_FIRM"] = array_keys( $arResult["COMPANY_ONPAGE"] );

$arFilter["PROPERTY_FIRM.PROPERTY_REGION"] = 830 ;
PrintAdmin(array_merge( $arrFilter, $arFilter ));
$rsElements = CIBlockElement::GetList(
			$arResult["SORT_FOR_ITEMS"],
			array_merge( $arrFilter, $arFilter ),
			false,
			$arNavParams,
			array(
				"ID",
				"NAME",
				"PREVIEW_PICTURE",
				"PREVIEW_TEXT",
				"DETAIL_PICTURE",
				"DETAIL_TEXT",
				"DETAIL_PAGE_URL","SHOW_COUNTER","DATE_CREATE","PROPERTY_OPT","PROPERTY_ROZNICA",
				"PROPERTY_COST",
				"PROPERTY_CURRENCY",
				"PROPERTY_unit_tov",
				"PROPERTY_FIRM.ID",
				"PROPERTY_FIRM.IBLOCK_ID",
				"PROPERTY_FIRM.NAME",
				"PROPERTY_FIRM.PREVIEW_PICTURE",
				"PROPERTY_FIRM.DETAIL_PICTURE",
				"PROPERTY_FIRM.DETAIL_PAGE_URL",
				"PROPERTY_FIRM.PROPERTY_USER",
				"PROPERTY_FIRM.PROPERTY_EMAIL",
				"PROPERTY_FIRM.PROPERTY_MANAGER",
				"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
				"PROPERTY_FIRM.PROPERTY_YARMARKA",
				"PROPERTY_FIRM.PROPERTY_REGION",
				"PROPERTY_FIRM.PROPERTY_PREMIUM"
			)
		);
?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
