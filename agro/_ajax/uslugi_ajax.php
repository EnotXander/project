<?
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );

global $USER;
global $APPLICATION;

$obParser                   = new CTextParser;
$arParams["PAGER_TEMPLATE"] = "market";

$arList['ID'] = (int) $_POST['category'];
$srt          = unserialize( $_POST['url'] );
if ( $srt['CURPAGE'] ) {
	if ( $srt['PAGEN'] ) {

		$_REQUEST["PAGEN"] = (int) $srt['PAGEN'];

	} else {

		$_REQUEST["PAGEN"] = 1;

	}
} else {

	$_REQUEST["PAGEN"] = 1;

}

$arParams["DETAIL_TEXT_CUT"] = 155;
if ( $arList['ID'] != $srt['CURPAGE'] ) {

	$_REQUEST["CURPAGE"] = $arList['ID'];

} else {

	$_REQUEST["CURPAGE"] = (int) $srt['CURPAGE'];

}


if ( (int) $_REQUEST["PAGEN"] && (int) $_REQUEST["CURPAGE"] == $arList["ID"] ) {
	$arNavParams["iNumPage"] = (int) $_REQUEST["PAGEN"];
} else {
	$arNavParams["iNumPage"] = 1;
}

if ( $srt['sort_by'] ) {

	$arResult["SORT_BY"] = $srt['sort_by'];

} else {

	$arResult["SORT_BY"] = 'date';

}

if ( $srt['sort_type'] ) {

	$arResult["SORT_TYPE"] = $srt['sort_type'];

} else {

	$arResult["SORT_TYPE"] = 'desc';

}
if ( ! $_GET['per_page'] ) {

	$arParams["PAGE_ELEMENT_COUNT"] = 20;

} else {

	$arParams["PAGE_ELEMENT_COUNT"] = $_GET['per_page'];

}


    $arResult["SORT_FOR_ITEMS"] = array(
	    $arResult["SORT_BY"]=> $arResult["SORT_TYPE"],
	);

   //��������� ������ ��� ��������
   if($arResult["SORT_BY"] != "company" || true)
   {

	   if ( $arResult["SORT_BY"] == "company" ) {
		 //���������� ������
		       $arResult["SORT_FOR_ITEMS"] = array(
		          "PROPERTY_SORT_COMPANY" => "DESC",
		          "SORT" => "ASC",
		          "NAME" => "ASC"
		       );
	   }

      //����������
      $arFilter = array();
      $arFilter["IBLOCK_ID"] = IBLOCK_USLUGI;
      $arFilter["ACTIVE"] = "Y";
      $arFilter["SECTION_ID"] = $arList["ID"];
      
     

     //���������
      //$GLOBALS["NavNum"] = $arList["ID"];
      $arNavParams = array();//**************
      $arNavParams = array(
           "nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
           "bShowAll" => 'N',
      );
      if((int)$_REQUEST["PAGEN"] && (int)$_REQUEST["CURPAGE"] == $arList["ID"])
         $arNavParams["iNumPage"] = (int)$_REQUEST["PAGEN"];
      else
         $arNavParams["iNumPage"] = 1;
   
    
   }
   else
   {
  //����������
      $arFilter = array();
      $arFilter["IBLOCK_ID"] = IBLOCK_USLUGI;
      $arFilter["ACTIVE"] = "Y";
      $arFilter["SECTION_ID"] = $arList["ID"];

      //���������� ������
      $arResult["SORT_FOR_ITEMS"] = array(
         "PROPERTY_SORT_COMPANY" => "DESC",
         "SORT" => "ASC",
         "NAME" => "ASC"
      );

   }
   
   //��������� ������� � �������
   $arList["GROUPS"] = array();
   $groupIndex = -1;//����� ��������������� �� ��������� ������� � ������
   //printAdmin($arResult["SORT_FOR_ITEMS"]);

   $rsElements = CIBlockElement::GetList(
           $arResult["SORT_FOR_ITEMS"],
           $arFilter,
           false,
            $arNavParams,
           array(
               "ID",
               "NAME",
               "PREVIEW_PICTURE",
               "PREVIEW_TEXT",
               "DETAIL_PICTURE",
               "DETAIL_TEXT",
               "DETAIL_PAGE_URL",
               "PROPERTY_COST",
               "PROPERTY_CURRENCY",
               "PROPERTY_unit_tov",
               "PROPERTY_FIRM.ID",
               "PROPERTY_FIRM.IBLOCK_ID",
               "PROPERTY_FIRM.NAME",
               "PROPERTY_FIRM.PREVIEW_PICTURE",
               "PROPERTY_FIRM.DETAIL_PICTURE",
               "PROPERTY_FIRM.DETAIL_PAGE_URL",
               "PROPERTY_FIRM.PROPERTY_USER",
               "PROPERTY_FIRM.PROPERTY_EMAIL",
               "PROPERTY_FIRM.PROPERTY_MANAGER",
               "PROPERTY_FIRM.PROPERTY_REMOVE_REL"
           )
   );
   $rsElements->SetUrlTemplates("", $arParams["DETAIL_URL"]);
   while($arElements = $rsElements->GetNext())
   {
      //�������� ������������� ��������
      $arMultiPropList = array("FAVORITES");
      foreach ($arMultiPropList as $multiPropName)
      {
         $arElements["PROPERTY_".$multiPropName] = array();
         $rsMultiProp = CIBlockElement::GetProperty(
                  $arElements["IBLOCK_ID"],
                  $arElements["ID"],
                  array(),
                  array("CODE" => $multiPropName)
         );
         while($arMultiProp = $rsMultiProp->GetNext())
         {
            $arElements["PROPERTY_".$multiPropName][] = $arMultiProp;
            $arElements["PROPERTY_".$multiPropName."_VALUE"][] = $arMultiProp["VALUE"];
         }
      }
      
      //������ �����
      if(!strlen($arElements["PREVIEW_TEXT"]))
      {
         $arElements["PREVIEW_TEXT"] = strip_tags($obParser->html_cut($arElements["DETAIL_TEXT"], $arParams["DETAIL_TEXT_CUT"]));
      }
      
      //������
      $arElements["PREVIEW_PICTURE"] = Resizer(
              array($arElements["PREVIEW_PICTURE"], $arElements["DETAIL_PICTURE"]),
              array("width" => 55, "height" => 55),
              BX_RESIZE_IMAGE_EXACT
      );
      if(!strlen($arElements["PREVIEW_PICTURE"]["SRC"]))
         $arElements["PREVIEW_PICTURE"]["SRC"] = SITE_TEMPLATE_PATH."/images/withoutphoto54x44.png";
      
      //�������������� ������/������ ���������
      $arElements["PROPERTY_CURRENCY_FORMATED"] = StrUnion(array(
          $arElements["PROPERTY_CURRENCY_VALUE"],
          $arElements["PROPERTY_unit_tov_VALUE"]
      ), "/");
      
      //���� �������� ���������, �� ������� ���� � ��������
      if($arElements["PROPERTY_FIRM_PROPERTY_REMOVE_REL_ENUM_ID"] != false)
      {
         $arElements["PROPERTY_FIRM_ID"] = 0;
      }
      
      //��������� ���� � ��������
      if(!isset($arResult["COMPANY_DETAIL"][$arElements["PROPERTY_FIRM_ID"]]))
      {
         //TODO: ������ ���������� �������� �������
         $arCompany = array(
             "ID" => $arElements["PROPERTY_FIRM_ID"],
             "IBLOCK_ID" => $arElements["PROPERTY_FIRM_IBLOCK_ID"],
             "NAME" => $arElements["PROPERTY_FIRM_NAME"],
             "PREVIEW_PICTURE" => $arElements["PROPERTY_FIRM_PREVIEW_PICTURE"],
             "DETAIL_PICTURE" => $arElements["PROPERTY_FIRM_DETAIL_PICTURE"],
             "DETAIL_PAGE_URL" => $arElements["PROPERTY_FIRM_DETAIL_PAGE_URL"],
             "PROPERTY_USER" => $arElements["PROPERTY_FIRM_PROPERTY_USER_VALUE"],
             "PROPERTY_EMAIL" => $arElements["PROPERTY_FIRM_PROPERTY_EMAIL_VALUE"],
             "PROPERTY_MANAGER" => $arElements["PROPERTY_FIRM_PROPERTY_MANAGER_VALUE"],
             "ITEMS_COUNT" => array($arList["ID"] => 1)
         );
         //������� �������� ��������
         if($arParams["COMPANY_NAME_CUT"] && (strlen(html_entity_decode($arCompany["NAME"])) > ($arParams["COMPANY_NAME_CUT"] + 2)))
            $arCompany["NAME_CUT"] = $obParser->html_cut(html_entity_decode($arCompany["NAME"]), $arParams["COMPANY_NAME_CUT"]);
         else
            $arCompany["NAME_CUT"] = $arCompany["NAME"];
         //������
         $arCompany["PREVIEW_PICTURE"] = Resizer(
                 array($arCompany["PREVIEW_PICTURE"], $arCompany["DETAIL_PICTURE"]),
                 array("width" => 148, "height" => 40),
                 BX_RESIZE_IMAGE_PROPORTIONAL_ALT
         );
         //�������� ������������� ��������
         $arMultiPropList = array("phone", "URL", "FAVORITES");
         foreach ($arMultiPropList as $multiPropName)
         {
            $arCompany["PROPERTY_".$multiPropName] = array();
            $rsMultiProp = CIBlockElement::GetProperty(
                     $arCompany["IBLOCK_ID"],
                     $arCompany["ID"],
                     array(),
                     array("CODE" => $multiPropName)
            );
            while($arMultiProp = $rsMultiProp->GetNext())
            {
               //$arCompany["PROPERTY_".$multiPropName][] = $arMultiProp;
               $arCompany["PROPERTY_".$multiPropName."_VALUE"][] = $arMultiProp["VALUE"];
            }
         }

         //�������� �������������
         if(ENABLE_PREDSTAVITEL_MODE)
         {
            $arPredstavitelInfo = PredstavitelGetByCompany($arCompany["ID"]);
            $arCompany["PROPERTY_USER"] = $arPredstavitelInfo["RELATED"];
         }
         if(!$arCompany["PROPERTY_USER"])
            $arCompany["PROPERTY_USER"] = $arCompany["PROPERTY_MANAGER"];
         if($arCompany["PROPERTY_USER"])
         {
            $arCompany["PREDSTAVITEL"] = $USER->GetByID($arCompany["PROPERTY_USER"])->Fetch();
            $arCompany["PREDSTAVITEL_MESSAGE_LINK"] = "/personal/messages/{$arCompany["PREDSTAVITEL"]["ID"]}/";
            $arElements["PREDSTAVITEL_MESSAGE_LINK"] = "{$arCompany["PREDSTAVITEL_MESSAGE_LINK"]}?product={$arElements["ID"]}";
         }
         else
         {
            $arCompany["PREDSTAVITEL_MESSAGE_LINK"] = "mailto:{$arCompany["PROPERTY_EMAIL"]}";
            $arElements["PREDSTAVITEL_MESSAGE_LINK"] = $arCompany["PREDSTAVITEL_MESSAGE_LINK"];
         }

         //���������� ��������
         $arResult["COMPANY_DETAIL"][$arElements["PROPERTY_FIRM_ID"]] = $arCompany;
      }
      else
	      $arResult["COMPANY_DETAIL"][$arElements["PROPERTY_FIRM_ID"]]["ITEMS_COUNT"][$arList["ID"]]++;
      $company = $arResult["COMPANY_DETAIL"][$arElements["PROPERTY_FIRM_ID"]];
      
      //������ �� ������� ������
      $arElements["PREDSTAVITEL_MESSAGE_LINK"] = $company["PREDSTAVITEL_MESSAGE_LINK"];
      if(strpos($company["PREDSTAVITEL_MESSAGE_LINK"], "mailto") === false)
         $arElements["PREDSTAVITEL_MESSAGE_LINK"] .= "?product={$arElements["ID"]}";
         
      if($arResult["SORT_BY"] != "company" || true)
      {
         //����������� �� ���������
         if(
            ($groupIndex < 0)//� ������ ��� ��� �� ����� ������
            || !$arElements["PROPERTY_FIRM_ID"]//����� �� ����������� ��������
            || ($arElements["PROPERTY_FIRM_ID"] != $arList["GROUPS"][$groupIndex]["COMPANY"])//�������� ����� ������ ���������� �� �������� ���� ������
         )
         {
            $groupIndex++;
         }
         $arList["GROUPS"][$groupIndex]["COMPANY"] = $arElements["PROPERTY_FIRM_ID"];
         $arList["GROUPS"][$groupIndex]["ITEMS"][] = $arElements;
      }
      else
      {
         //������ ������� ���� � �������� �������� ������ ������
         if($company["ITEMS_COUNT"][$arList["ID"]] > $arParams["COMPANY_ELEMENT_COUNT"]) $arElements["HIDE"] = true;
         //�������� ������ ������ ������� ���������
         if(!(($company["ITEMS_COUNT"][$arList["ID"]] - $arParams["COMPANY_ELEMENT_COUNT"]) % $arParams["COMPANY_ELEMENT_MORE_COUNT"])) $arElements["HIDE_PANEL"] = true;
         //����������� �� ���������
         $groupKey = array_search($arElements["PROPERTY_FIRM_ID"], array_keys($arResult["COMPANY_ONPAGE"]));
         $arList["GROUPS"][$groupKey]["COMPANY"] = $arElements["PROPERTY_FIRM_ID"];
         $arList["GROUPS"][$groupKey]["ITEMS"][] = $arElements;
      }
      
   }
   
   if($arResult["SORT_BY"] != "company" || true)
   {
      //������ ���������
      $rsElements->NavNum = $arList["ID"];
      $arList["NAV_STRING"] = $rsElements->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
      $arList["NAV_STRING"]=str_replace('/_ajax/uslugi_ajax.php',(string)$_POST['path'],$arList["NAV_STRING"]);
      $arList["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
      //$arList["NAV_RESULT"] = $rsElements;
   }
   else
   {
      //���������� ����� ��� ����������� ������� ��������
      ksort($arList["GROUPS"]);
   }
   
   //������ ���������� ������� � �������
   $arList["ITEMS_COUNT"] = CIBlockElement::GetList(Array(), array_merge($arrFilter, $arFilter), Array());
   
   //����������
   $arResult["LISTS"][$arListKey] = $arList;
 ?>
        <?if(count($arResult["LISTS"])):?>
         <?if(count($arResult["LISTS"]) > 1):?>
            <script>
               $(function(){
                  $(".accordion_block").accordion({
                     heightStyle: "content",
                     active: parseInt($('.accordion_block').attr("data-active"))
                  });
               });
            </script>
         <?endif;?>
         <ul class="accordion_block " data-active="<?=$arResult["ACCORDION_ACTIVE"]?>">
            <?foreach ($arResult["LISTS"] as $listKey => $arList):?>
               <li class="js-section_list">
               
                  <?if(count($arResult["LISTS"]) > 1):?>
                  
                     <div id="tab<?=$listKey+1?>" data-category="<?=$arList['ID'];?>" class="h3-title ajax-catalog"><?=$arList["NAME"]?> <sup class="sup"></sup></div>
                  <?endif;?>
                  <ul class="accordion-content">
                     <div class="ads_row_wrapblock">

                        <div class="ads_row_rowsblock">
                           <?foreach ($arList["GROUPS"] as $arGroup):?>
                              <div class="ads_3_row js-hideitems-group">
                                 <div class="ads_row_left ">
                                    <?foreach ($arGroup["ITEMS"] as $arItemKey => $arItem):?>
                               
                                       <div class="ads_row premium" data-old="<?=$arItem["HIDE"] ? ' item_hide' : ''?>">
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <td class="ic_star" style="vertical-align:top;">
                                                      <?if($USER->IsAuthorized()):?>
                                                         <a href="javascript:void(0)" onclick="AddToFavorites({
                                                                     productId: <?=$arItem["ID"]?>,
                                                                    context: $(this).find('i'),
                                                                    inFavorites: function(){this.addClass('marked')},
                                                                    outFavorites: function(){this.removeClass('marked')}
                                                         })">
                                                            <i class="i_star<? if(in_array($USER->GetID(), $arItem["PROPERTY_FAVORITES_VALUE"])):?> marked<? endif; ?>"></i>
                                                         </a>
                                                      <?endif;?>
                                                   </td>
                                                   <td class="image_box">
                                                      <a title="���������� ���������" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                                         <img title="������ <?=$arItem["NAME"]?>"  alt="<?=$arItem["NAME"]?>"  width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                                                      </a>
                                                   </td>
                                                   <td class="ads_info">
                                                      <div class="tdWrap">
                                                         <div class="ads_title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?> </a></div>
                                                         <?if($arGroup["COMPANY"]):?>
                                                            <div class="ads_text"><?=$arItem["PROPERTY_FIRM_NAME"]?></div>
                                                         <?endif;?>
                                                         <div class="meta"><?=$arItem["PREVIEW_TEXT"]?> </div>
                                                      </div>                      
                                                   </td>
                                                   <td class="ads_price">
                                                      <div class="tdWrap">
                                                         <?if($arItem["PROPERTY_COST_VALUE"]):?>
                                                            <div class="sum"><?=$arItem["PROPERTY_COST_VALUE"]?></div>
                                                            <div class="curr-val"><?=$arItem["PROPERTY_CURRENCY_FORMATED"]?></div>
                                                         <?else:?>
	                                                         <img alt="���� ���������" src="/bitrix/templates/energorb/images/detailprice.png" width="77" height="29">
                                                         <?endif;?>
                                                         <!--<a href="<?=$arItem["PREDSTAVITEL_MESSAGE_LINK"]?>" class="buy-button">������</a>-->
                                                         <a href="javascript: void(0);" class="buy-button" onclick="javascript:showPopup({id: $('.fast_order_popup'), productID: <?=$arItem["ID"]?>, sender: $(this)})"></a>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <?if(($arItem["HIDE_PANEL"]) && $arGroup["ITEMS"][$arItemKey+1]["HIDE"]):?>
                                                   <tr>
                                                      <td colspan="3">
                                                         <div class="show_position">
                                                            <a href="javascript:void(0)" class="js-hideitems-show" data-more="<?=$arParams["COMPANY_ELEMENT_MORE_COUNT"]?>">
                                                               <i class="plus_icon"></i>
                                                               <span>�������� <?=$arParams["COMPANY_ELEMENT_MORE_COUNT"] ? "���"/*." {$arParams["COMPANY_ELEMENT_MORE_COUNT"]} ������"*/ : " ��� �������"?></span></a>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                <?endif;?>
                                             </tbody>
                                          </table>
                                       </div><!--ads_row-->
                                    <?endforeach;?>
                                 </div>

                                 <?if($arGroup["COMPANY"]):
	                                // PrintAdmin( $arResult["COMPANY_DETAIL"][$arGroup["COMPANY"]]);
                                    $arCompany = $arResult["COMPANY_DETAIL"][$arGroup["COMPANY"]];?>
                                    <div class="ads_row_right">
                                        <noindex>
                                       <div class="ads_row_rheader" onclick="window.open('<?=$arCompany["DETAIL_PAGE_URL"]?>','_blank'); return false;">
                                          <?if(is_array($arCompany["PREVIEW_PICTURE"])):?>
                                             <img src="<?=$arCompany["PREVIEW_PICTURE"]["SRC"]?>" alt="" />
                                          <?else:?>
                                             <img src="/images/search_placeholder.png" />
                                          <?endif;?>
                                          <span class="company-name"<?=(strlen($arCompany["NAME_CUT"]) != strlen($arCompany["NAME"])) ? " title='".$arCompany["NAME"]."'" : ''?>><?=$arCompany["NAME_CUT"]?><sup><?=$arCompany["ITEMS_COUNT"][$arList["ID"]]?></sup></span>
                                       </div>
                                       <div class="ads_row_rcontent">
                                          <div class="all_company_info" id="allCompanyInfoID" >
                                             <a class="btns" style="margin-top: 10px;" href="<?=$arCompany["PREDSTAVITEL_MESSAGE_LINK"]?>"><i></i>��������</a>
                                             <a class="c_name" href="<?=$arCompany["DETAIL_PAGE_URL"]?>">�������� ��������</a>
                                             <ul class="js-phone-hide-block" data-company="<?=$arCompany['ID']?>">
                                                <?foreach ($arCompany["PROPERTY_phone_VALUE"] as $phone):?>
                                                   <li>
                                                      <span class="js-phone-code">+375</span><span>
                                                      <span class="phone-hide js-phone-hide">�������� �������</span></span>
                                                      <span class="phone-show"><?= phone($phone) ?></span>
                                                   </li>
                                                <?endforeach;?>
                                                <?foreach ($arCompany["PROPERTY_URL_VALUE"] as $url):?>
	                                                <?
	                                                if((strpos($url, 'http://') === false) && (strpos($url, 'https://') === false))
	                                                   $VALUE_HREF = 'http://'.$url;
	                                                else
		                                               $VALUE_HREF = $url;
	                                                ?>
                                                   <li><a class="c_site" rel='nofollow' href="<?= $VALUE_HREF ?>"><?=$url?></a></li>
                                                <?endforeach;?>
                                             </ul>
                                             <a class="c_mess" href="javascript:void(0);">
                                                <div>
                                                   �������� ����������,<br> ��� ����� ��� �� EnergoBelarus.by
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                        </noindex>
                                    </div>
                                 <?endif;?>
								 
                                 <div class="clear"></div>
                              </div>
                           <?endforeach;?>
                           <?= $arList["NAV_STRING"] ?>

                        </div>
						
                     </div>

                  </ul>
               </li>
            <?endforeach;?>
         </ul>
      <?endif;?>
<?

//PrintAdmin( $arResult["COMPANY_DETAIL"]);
