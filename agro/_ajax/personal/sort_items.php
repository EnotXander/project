<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$pagen = (int)$_REQUEST["pagen"];
$sizen = (int)$_REQUEST["sizen"];
$companyId = (int)$_REQUEST["company"];
$iblockId = $_REQUEST["iblock"];
$arElements = $_REQUEST["elements"];
$copyId = (int)$_REQUEST["copyId"];
$isError = array();
$list = "";

if($copyId)
{
   if (!CModule::IncludeModule("iblock")) die();
   $objElement = new CIBlockElement();
   global $USER;
   
   if(!$USER->IsAuthorized())
      $isError[] = iconv("cp1251", "UTF-8", "��������� �����������");
   if(!$iblockId)
      $isError[] = iconv("cp1251", "UTF-8", "������������ ID ���������: {$iblockId}");

   if(!count($isError))
   {
      $arElementFilter = array("IBLOCK_ID" => $iblockId, "ID" => $copyId);
      if($companyId)
      {
         $rsCompany = $objElement->GetList(array(), array("IBLOCK_ID" => 65, "ACTIVE" => "Y", "ID" => $companyId, "PROPERTY_USER" => $USER->GetID()));
         if(!$rsCompany->FieldsCount())
            $isError[] = iconv("cp1251", "UTF-8", "�������� {$companyId} ��� ����� {$USER->GetID()} �� �������");
         else
            $arElementFilter["PROPERTY_FIRM"] = $companyId;
      }
      else
      {
         $arElementFilter["PROPERTY_AUTHOR"] = $USER->GetID();
      }

      $rsElement = $objElement->GetList(
              array(),
              $arElementFilter
       );
      if($obElement = $rsElement->GetNextElement())
      {
         $arElement = $obElement->GetFields();
         $arElement["PROP"] = $obElement->GetProperties();

         $arFieldsCopy = array(
             "NAME" => "����� ".$arElement["NAME"],
             "IBLOCK_ID" => $iblockId,
             "IBLOCK_SECTION_ID" => $arElement["IBLOCK_SECTION_ID"],
             "ACTIVE" => "N",
             "DATE_ACTIVE_FROM" => $arElement["DATE_ACTIVE_FROM"],
             "DATE_CREATE" => ConvertTimeStamp(time()+CTimeZone::GetOffset(), "FULL"),
             "SORT" => $arElement["SORT"],
             "PREVIEW_PICTURE" => CopyFileField($arElement["PREVIEW_PICTURE"]),
             "DETAIL_PICTURE" => CopyFileField($arElement["DETAIL_PICTURE"]),
             "PREVIEW_TEXT" => $arElement["PREVIEW_TEXT"],
             "PREVIEW_TEXT_TYPE" => $arElement["PREVIEW_TEXT_TYPE"],
             "DETAIL_TEXT" => $arElement["DETAIL_TEXT"],
             "DETAIL_TEXT_TYPE" => $arElement["DETAIL_TEXT_TYPE"],
         );

         //�������� �� ������������ ����������� ����
         $arFieldsCopy["CODE"] = Cutil::translit($arFieldsCopy["NAME"], "ru", array("replace_space" => "_", "replace_other" => "_"));
         $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arFieldsCopy["CODE"]));
         if ($element = $res->GetNext())
            $arFieldsCopy["CODE"] = $arFieldsCopy["CODE"]."_".rand(1, 1000);

         $arFieldsCopy['PROPERTY_VALUES'] = array();
         foreach ($arElement['PROP'] as $property)
         {
            //��������, ������� �� ����������
            if($property["CODE"] == "FAVORITES")
               continue;
            //�����������
            if ($property['PROPERTY_TYPE'] == 'F')
            {
               if ($property['MULTIPLE'] == 'Y')
               {
                  if (is_array($property['VALUE']))
                  {
                     foreach ($property['VALUE'] as $key => $arElEnum)
                        $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']][$key] = CopyFileField($arElEnum);
                  }
               }else
                  $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = CopyFileField($property['VALUE']);
            }
            elseif ($arProp['PROPERTY_TYPE'] == 'L')
            {
               if ($arProp['MULTIPLE'] == 'Y')
               {
                  $arFieldsCopy['PROPERTY_VALUES'][$arProp['CODE']] = array();
                  foreach ($arProp['VALUE_ENUM_ID'] as $enumID)
                  {
                     $arFieldsCopy['PROPERTY_VALUES'][$arProp['CODE']][] = array(
                         'VALUE' => $enumID
                     );
                  }
               } else
               {
                  $arFieldsCopy['PROPERTY_VALUES'][$arProp['CODE']] = array(
                      'VALUE' => $arProp['VALUE_ENUM_ID']
                  );
               }
            }
            else
               $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = $property['VALUE'];
         }

         if ($NEW_ID = $objElement->Add($arFieldsCopy))
         {
            $arTemp = array();
            foreach($arElements as $sortedElement)
            {
               $arTemp[] = $sortedElement;
               if($sortedElement == $copyId)
                  $arTemp[] = $NEW_ID;
            }
            $arElements = $arTemp;
         }
         else
            $isError[] = iconv("cp1251", "UTF-8", $objElement->LAST_ERROR);

      }
      else
         $isError[] = iconv("cp1251", "UTF-8", "������� {$copyId} ��� �������� {$companyId} �� ������");

   }
}

if(!count($isError))
{
   $resort = TovarsResort($pagen, $sizen, $companyId, $iblockId, $arElements);
   if($copyId)
   {
      global $arrProductListFilter;
      $arrProductListFilter = array("ID" => $NEW_ID, "ACTIVE" => "");
      switch($iblockId)
      {
         case 58:
         case 64:
            $iblockType = "services";
            $listTemplate = "personal_product_list_ajax";
            break;
         case 52:
            $iblockType = "information_part";
            $listTemplate = "personal_news_list_ajax";
            break;
         case 55:
            $iblockType = "information_part";
            $listTemplate = "personal_news_list_ajax";
            break;
         /*case 36:
            $iblockType = "list";
            $listTemplate = "personal_advert_list_ajax";
            break;*/
         default:
            $list = iconv("cp1251", "UTF-8", "���������������� ��� ��������� {$iblockId}");
      }
      ob_start();
      $APPLICATION->IncludeComponent("bitrix:news.list", $listTemplate, array(
               "IBLOCK_TYPE" => $iblockType,
               "IBLOCK_ID" => $iblockId,
               "NEWS_COUNT" => 1,
               "SORT_BY1" => "PROPERTY_SORT_COMPANY",
               "SORT_ORDER1" => "DESC",
               "SORT_BY2" => "ACTIVE_FROM",
               "SORT_ORDER2" => "ASC",
               "FILTER_NAME" => "arrProductListFilter",
               "FIELD_CODE" => array("ACTIVE", "PREVIEW_PICTURE", "DETAIL_PICTURE"),
               "PROPERTY_CODE" => array("FAVORITES", "FIRM", "NUMBER", "COST"),
               "CHECK_DATES" => "N",
               "DETAIL_URL" => "",
               "AJAX_MODE" => "N",
               "AJAX_OPTION_JUMP" => "N",
               "AJAX_OPTION_STYLE" => "Y",
               "AJAX_OPTION_HISTORY" => "N",
               "CACHE_TYPE" => "N",
               "CACHE_TIME" => 3600*24,
               "CACHE_FILTER" => "N",
               "CACHE_GROUPS" => "Y",
               "PREVIEW_TRUNCATE_LEN" => "",
               "ACTIVE_DATE_FORMAT" => "d.m h:m",
               "SET_TITLE" => "N",
               "SET_STATUS_404" => "N",
               "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
               "ADD_SECTIONS_CHAIN" => "N",
               "HIDE_LINK_WHEN_NO_DETAIL" => "N",
               "PARENT_SECTION" => "",
               "PARENT_SECTION_CODE" => "",
               "DISPLAY_TOP_PAGER" => "Y",
               "DISPLAY_BOTTOM_PAGER" => "Y",
               "PAGER_TITLE" => "�������",
               "PAGER_SHOW_ALWAYS" => "Y",
               "PAGER_TEMPLATE" => "",
               "PAGER_DESC_NUMBERING" => "N",
               "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
               "PAGER_SHOW_ALL" => "Y",
               "DISPLAY_DATE" => "N",
               "DISPLAY_NAME" => "N",
               "DISPLAY_PICTURE" => "Y",
               "DISPLAY_PREVIEW_TEXT" => "Y",
               "AJAX_OPTION_ADDITIONAL" => "",
               "STATUS_NEW" => "N",
               "EDIT_URL" => "admin_serv.php",
               "COMPANY_ID" => $companyId
                   )
      );
      $list = iconv("cp1251", "UTF-8", ob_get_clean());
   }
}

echo json_encode(array("error" => $isError, "list" => $list, "resort" => $resort));
