<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$companyId = (int)$_REQUEST["companyId"];
$isError = array();
$answer = array(
    "AUTHORIZE" => false,
    "REQUEST_EXISTS" => true,
    "REQUEST_CREATE" => false,
    "EMAIL" => false
);

if (!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
global $USER;

if(!$USER->IsAuthorized())
   $isError[] = iconv("cp1251", "UTF-8", "��������� �����������");
if(!$companyId)
   $isError[] = iconv("cp1251", "UTF-8", "�������� ID ��������: {$companyId}");

if(!count($isError))
{
   $answer["AUTHORIZE"] = true;
   $userId = $USER->GetByID($USER->GetID())->Fetch();
   $rsCompany = $objElement->GetList(array(), array("IBLOCK_ID" => IBLOCK_COMPANY, "ACTIVE" => "Y", "ID" => $companyId));
   if($obCompany = $rsCompany->GetNextElement())
   {
      $arCompany = $obCompany->GetFields();
      $arCompany["PROP"] = $obCompany->GetProperties();
      $rsRequest = $objElement->GetList(array(), array(
          "IBLOCK_ID" => IBLOCK_PREPSTAVITEL,
          "ACTIVE" => "Y",
          "PROPERTY_COMPANY" => $companyId,
          "PROPERTY_USER" => $userId["ID"],
          "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
          ));
      if($arRequest = $rsRequest->GetNext())
         $isError[] = iconv("cp1251", "UTF-8", "������ �� ������������ {$userId["NAME"]} {$userId["LAST_NAME"]} ��� �������� {$arCompany["NAME"]} ��� ���������");
      else
      {
         $answer["REQUEST_EXISTS"] = false;
         $props = array(
             "COMPANY" => $companyId,
             "USER" => $userId["ID"],
             "STATUS" => (LANG == 's1' ? 64 : 143),//��������������� (LANG == 's1' ? 64 : 143)
             "PREDSTAVITEL" => 146//"Y" 71
         );
          $name = "{$userId["NAME"]} {$userId["LAST_NAME"]} - {$arCompany["NAME"]}";
          $arParams = array("replace_space"=>"-","replace_other"=>"-");
          $trans = Cutil::translit($name,"ru",$arParams);

         //���������� ������ ��� ���������� � �������
         $arToSave = array(
            "MODIFIED_BY"    => $USER->GetID(), // ������� ������� ������� �������������
            "PROPERTY_VALUES"=> $props,
            "IBLOCK_ID"      => IBLOCK_PREPSTAVITEL,
            "NAME"           => "{$userId["NAME"]} {$userId["LAST_NAME"]} - {$arCompany["NAME"]}",
            "CODE"			 => $trans,
            "ACTIVE"         => "Y",            // �������
         );
         $newID = $objElement->Add($arToSave);
         if(intval($newID) > 0)
         {
            $answer["REQUEST_CREATE"] = true;
            $answer["REQUEST_ID"] = $newID;
            //�������� ���������
            if($arCompany["PROP"]["MANAGER"]["VALUE"])
            {
               $arEmailTo = array();
               if($arCompany["PROP"]["MANAGER"]["VALUE"])
               {
                  $arTempUser = $USER->GetByID($arCompany["PROP"]["MANAGER"]["VALUE"])->Fetch();
                  $arEmailTo[] = $arTempUser["EMAIL"];
               }
               if(count($arEmailTo))
               {
                  $event = new CEvent;
                  $arEmailTo = implode(",", $arEmailTo);
                  $arEventFields = array(
                      "EMAIL_TO" => $arEmailTo,
                      "USER_NAME" =>$userId["User"]["NAME"]." ".$userId["User"]["LAST_NAME"],
                      "USER_EMAIL" => $userId["User"]["EMAIL"],
                      "COMPANY_NAME" => $arCompany["NAME"],
                  );
                  //PrintObject($arEventFields);
                  $event->SendImmediate("USER_COMPANY_PREDSTAVITEL", SITE_ID, $arEventFields);
                  //$answer["EMAIL"] = true;
               }
            }
         }
         else
            $isError[] = iconv("cp1251", "UTF-8", $objElement->LAST_ERROR);
      }
   }
   else
      $isError[] = iconv("cp1251", "UTF-8", "�� ������� ��������: {$companyId}");
}

echo json_encode(array("error" => $isError, "answer" => $answer, "www"=> $www));
