<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
if (count($_GET) > 0)
{
	$curPageParams = '?';
	foreach($_GET as $key => $param)
	{
		if($key == "user"){
			continue;
		}
		$curPageParams .= $key.'='.$param.'&';
	}
	$curPageParams = substr($curPageParams, 0, -1);
}
else
{
	$curPageParams = '';
}
$aMenuLinksExt=$APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
	"IS_SEF" => "Y",
	"SEF_BASE_URL" => "/advert/",
	"SECTION_PAGE_URL" => "#SECTION_ID#/",//.$curPageParams,
	"DETAIL_PAGE_URL" => "#SECTION_ID#/#ELEMENT_ID#",
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => IBLOCK_ADVARE,
	"DEPTH_LEVEL" => "1",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600"
),
	false
);

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>