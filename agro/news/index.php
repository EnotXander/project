<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("�������");
?>
<?
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    if (isset($_REQUEST["news_id"])) // �������� �� ������ �������
    {
       $newsID = (int) $_REQUEST["news_id"];
       if ($newsID > 0)
       {
          if (!CModule::IncludeModule("iblock")) die();
          $objElement = new CIBlockElement();
          $res = $objElement->GetList(array(), array("IBLOCK_ID" => 52, "ACTIVE" => "Y", "EXTERNAL_ID" => $newsID));
          if ($element = $res->GetNext()) // ���������� �� ������ � ����� ��������
             LocalRedirect($element["DETAIL_PAGE_URL"], false, "301 Moved Permanently");
          else // ������ ������� �� ����������
             LocalRedirect("/404.php", false, "404 Not found");
       }
    }

    $iblockID = 52;
    if (isset($_REQUEST["ELEMENT_ID"])) // �������� �� ������ �������
    {
       if (!CModule::IncludeModule("iblock")) die();
       $objElement = new CIBlockElement();
       $res = $objElement->GetList(array(), array("ID" => $_REQUEST["ELEMENT_ID"], "IBLOCK_ID" => $iblockID));
       if ($element = $res->GetNext())
          LocalRedirect($element["DETAIL_PAGE_URL"], false, "301 Moved Permanently");
       else
          LocalRedirect("/404.php", false, "404 Not found");
    } else if(isset($_REQUEST["SECTION_ID"]))
    {
       if (!CModule::IncludeModule("iblock")) die();
       $objSection = new CIBlockSection();
       $res = $objSection->GetList(array(), array("ID" => $_REQUEST["SECTION_ID"], "IBLOCK_ID" => $iblockID));
       if ($section = $res->GetNext())
          LocalRedirect($section["SECTION_PAGE_URL"], false, "301 Moved Permanently");
       else
          LocalRedirect("/404.php", false, "404 Not found");
    }

    $APPLICATION->AddHeadString('<link href="/bitrix/templates/agrorb/css/additional-v2-ns.css" rel="stylesheet" type="text/css">', true);
}
?>
<?
$APPLICATION->IncludeComponent(
"bitrix:news", 
"news_energo1", 
array(
    "IBLOCK_TYPE" => "information_part",
    "IBLOCK_ID" => "52",
    "NEWS_COUNT" => "10",
    "USE_SEARCH" => "Y",
    "USE_RSS" => "N",
    "USE_RATING" => "N",
    "USE_CATEGORIES" => "N",
    "USE_REVIEW" => "N",
    "MESSAGES_PER_PAGE" => "10",
    "USE_CAPTCHA" => "Y",
    "REVIEW_AJAX_POST" => "N",
    "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
    "FORUM_ID" => "9",
    "URL_TEMPLATES_READ" => "",
    "SHOW_LINK_TO_FORUM" => "N",
    "POST_FIRST_MESSAGE" => "N",
    "USE_FILTER" => "N",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_ORDER1" => "DESC",
    "SORT_BY2" => "ACTIVE_FROM",
    "SORT_ORDER2" => "DESC",
    "CHECK_DATES" => "Y",
    "SEF_MODE" => "Y",
    "SEF_FOLDER" => "/news/",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "SET_TITLE" => "N",
    "SET_STATUS_404" => "Y",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "Y",
    "USE_PERMISSIONS" => "N",
    "PREVIEW_TRUNCATE_LEN" => "100",
    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y G:i",
    "LIST_FIELD_CODE" => array(
        "TAGS",
        "PREVIEW_PICTURE",
        "DETAIL_TEXT",
        "DETAIL_PICTURE",
    ),
    "LIST_PROPERTY_CODE" => array(),
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "DISPLAY_NAME" => "Y",
    "META_KEYWORDS" => "KEYWORDS",
    "META_DESCRIPTION" => "DESCRIPTION",
    "BROWSER_TITLE" => "BROWSER_TITLE",
    "SECTION_META_KEYWORDS" => "UF_KEYWORDS",
    "SECTION_META_DESCRIPTION" => "UF_DESCRIPTION",
    "SECTION_BROWSER_TITLE" => "UF_SEO_TITLE",
    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y G:i",
    "DETAIL_FIELD_CODE" => array("TAGS"),
    "DETAIL_PROPERTY_CODE" => array("FIRM"),
    "DETAIL_DISPLAY_TOP_PAGER" => "N",
    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
    "DETAIL_PAGER_TITLE" => "",
    "DETAIL_PAGER_TEMPLATE" => "",
    "DETAIL_PAGER_SHOW_ALL" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "",
    "PAGER_SHOW_ALWAYS" => "Y",
    "PAGER_TEMPLATE" => "defaultv2",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "Y",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "USE_SHARE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "SEF_URL_TEMPLATES" => array(
        "news" => "",
        "section" => "#SECTION_CODE#/",
        "detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
        "search" => "search/",
    )
        ), false
);
?>

<?if((defined('ERROR_404') && ERROR_404 == 'Y'))
   LocalRedirect("/404.php", false, "404 Not found");?>
<!-- (C)2000-2015 Gemius SA - gemiusTraffic / ver 11.1 / News --> 
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var gemius_identifier = new String('bD6QotSrP9UcJRkbDBTBKMWmDtaNXcL0dDDQjazOFm7.97');
//--><!]]>
</script>
<script type="text/javascript" src="http://agrobelarus.by/js/gemius.js"></script> 



<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>