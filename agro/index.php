<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "������������ � ��� �� ���������������� ��������� ��������");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("������������");
?>

<div class="content_box" style="overflow: visible;">
   <div class="right_col">
      <div class="rc_block">
         <div class="right_banner_300x120">
            <?
            $APPLICATION->IncludeComponent("bitrix:advertising.banner", "", array(
                "TYPE" => "A2_1_AGRO",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0",
                "CACHE_NOTES" => ""
                    )
            );
            ?>
         </div>
         <div class="right_banner_300x120">
            <?
            $APPLICATION->IncludeComponent("bitrix:advertising.banner", "", array(
                "TYPE" => "A2_2_AGRO",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0",
                "CACHE_NOTES" => ""
                    )
            );
            ?>
         </div>
         <div class="right_banner_300x120">
            <?
            $APPLICATION->IncludeComponent("bitrix:advertising.banner", "", array(
                "TYPE" => "A2_3_AGRO",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0",
                "CACHE_NOTES" => ""
                    )
            );
            ?>
         </div>
      </div>

      <div class="rc_block toog withTitlePhoto" style="margin-bottom: 0px;">
         <ul class="mb_menu">
            <li class="mb_title sel" id="mb_title_f">
               <a href="#1">���������</a>
               <div class="sclew"></div>
            </li>
         </ul>
         <div class="gray_box catalog_list tab">
            <?//����������
            $APPLICATION->IncludeComponent(
				"bitrix:catalog.sections.top", 
				"main_p", 
				array(
					"IBLOCK_TYPE" => "media_content",
					"IBLOCK_ID" => "62",
					"SECTION_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"SECTION_USER_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"SECTION_SORT_FIELD" => "TIMESTAMP_X",
					"SECTION_SORT_ORDER" => "desc",
					"ELEMENT_SORT_FIELD" => "sort",
					"ELEMENT_SORT_ORDER" => "asc",
					"FILTER_NAME" => "",
					"SECTION_URL" => "",
					"DETAIL_URL" => "",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"SECTION_COUNT" => "7",
					"ELEMENT_COUNT" => "1",
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"USE_PRODUCT_QUANTITY" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "300",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER2" => "desc",
					"HIDE_NOT_AVAILABLE" => "N",
					"LINE_ELEMENT_COUNT" => "3",
					"BASKET_URL" => "/personal/basket.php",
					"DISPLAY_COMPARE" => "N",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"PRICE_CODE" => array(
					),
					"USE_PRICE_COUNT" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"PRICE_VAT_INCLUDE" => "Y",
					"PRODUCT_PROPERTIES" => array(
					),
					"CONVERT_CURRENCY" => "N"
				),
				false
			);?>
         </div>

      </div>
   </div><!--VS right col 1 end-->
   <!--VS start midl1-->
   <div class="middle_col">
      <div class="mc_block withTitleMoln" style="overflow:hidden; height:310px;">
         <ul class="mb_menu">
            <li class="mb_title"><a href="#1">������� �������</a><div class="sclew"></div></li>
            <li class="sel"><a>������� ����</a><div class="sclew"></div></li>
            <li><a href="#3">��������</a><div class="sclew"></div></li>
            <li><a href="#3">������� ��������</a><div class="sclew"></div></li>
            <li><a href="#3">������ � ���</a><div class="sclew"></div></li>
            <li><a href="#3">� ����</a><div class="sclew"></div></li>
         </ul>
         <?
         $APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"moln_main",
				array(
					"IBLOCK_TYPE" => "information_part",
					"IBLOCK_ID" => "52",
					"NEWS_COUNT" => "4",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array(
						0 => "DETAIL_PICTURE",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "300",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "N",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m H:i",
					"SET_TITLE" => "Y",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "3198",
					"PARENT_SECTION_CODE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "�������",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",

					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_LAST_MODIFIED" => "N",
					"INCLUDE_SUBSECTIONS" => "Y"
				),
				false
			);
         ?>
         <?
         $APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"moln_main",
				array(
					"IBLOCK_TYPE" => "information_part",
					"IBLOCK_ID" => "52",
					"NEWS_COUNT" => "4",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array(
						0 => "DETAIL_PICTURE",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "300",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "N",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m H:i",
					"SET_TITLE" => "Y",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "3199",
					"PARENT_SECTION_CODE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "�������",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"INCLUDE_SUBSECTIONS" => "Y"
				),
				false
			);
         ?>
         <?
         $APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"moln_main",
				array(
					"IBLOCK_TYPE" => "information_part",
					"IBLOCK_ID" => "52",
					"NEWS_COUNT" => "4",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array(
						0 => "DETAIL_PICTURE",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "300",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "N",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m H:i",
					"SET_TITLE" => "Y",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "3200",
					"PARENT_SECTION_CODE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "�������",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"INCLUDE_SUBSECTIONS" => "Y"
				),
				false
			);
         ?> 
         <?
         $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"moln_main", 
	array(
		"IBLOCK_TYPE" => "information_part",
		"IBLOCK_ID" => "52",
		"NEWS_COUNT" => "4",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m H:i",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "3201",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "�������",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"INCLUDE_SUBSECTIONS" => "Y"
	),
	false
);
         ?> 
         <?
         $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"moln_main", 
	array(
		"IBLOCK_TYPE" => "information_part",
		"IBLOCK_ID" => "52",
		"NEWS_COUNT" => "4",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m H:i",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "3202",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "�������",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"INCLUDE_SUBSECTIONS" => "Y"
	),
	false
);
         ?> 
      </div>

      <div class="mc_block withTitle" style="width: 674px;">
         <div class="lenta_title_1">
            <a href="/news/">����� ���� ��������</a>
         </div>
         <div class="lenta_title_2">
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner", "", Array(
                "TYPE" => "LENTA",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0",
                "CACHE_NOTES" => ""
                    )
            );
            ?>
         </div>

         <?
         $APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"main_razv",
				array(
					"IBLOCK_TYPE" => "information_part",
					"IBLOCK_ID" => "52",
					"NEWS_COUNT" => "10",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array(
						0 => "DETAIL_PICTURE",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "300",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "N",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m H:i",
					"SET_TITLE" => "Y",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "�������",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"INCLUDE_SUBSECTIONS" => "Y",
					"NEWS_COUNT_VISIBLE" => 6
				),
				false
			);
         ?> 
      </div><!--mc_block-->
   </div>			
   <!--VS end midl1-->
</div>

<?
$APPLICATION->IncludeComponent(
      "bitrix:advertising.banner", "center", Array(
  "TYPE" => "ENERGO_A2",
  "NOINDEX" => "N",
  "CACHE_TYPE" => "A",
  "CACHE_TIME" => "0",
  "CACHE_NOTES" => ""
      )
);
?>
<?
$APPLICATION->IncludeComponent(
      "bitrix:advertising.banner", "center", Array(
  "TYPE" => "A4",
  "NOINDEX" => "N",
  "CACHE_TYPE" => "A",
  "CACHE_TIME" => "0",
  "CACHE_NOTES" => ""
      )
);
?>

<?
 $APPLICATION->IncludeComponent("bitrix:advertising.banner", "center", Array(
     "TYPE" => "AGRO_A2",
     "NOINDEX" => "N",
     "CACHE_TYPE" => "N",
     "CACHE_TIME" => "3600",
     "CACHE_NOTES" => ""
         )
 );?>


<div class="content_box" style="margin-top: 15px">

   <!--VS right col 2 start-->
   <div class="right_col brdr_box double_brdr_box fl_right" style="  width: 298px;  margin-right: 20px;  padding-right: 0;">
      <div class="wrap"> <div class="rc_block toog withTitleMarket" style="margin-bottom: 0">
         <a href="/advert/rss/" rel="nofollow" class="icons icon_rss" title="����������� �� �����"></a>
         <div class="title"><a href="/advert/">����������</a><sup><?= GetCountElements(IBLOCK_ADVARE); ?></sup></div>
         <div class="title_list_wrap" >
            <?
            $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                "SALT" => LAZYLOAD_SALT,
                "COMPONENTS" => array(
                    array(
                        "NAME" => "bitrix:news.list",
                        "TEMPLATE" => "list_main",
                        "PARAMETERS" => array(
                              "IBLOCK_TYPE" => "services",
                              "IBLOCK_ID" => IBLOCK_ADVARE,
                              "NEWS_COUNT" => "20",
                              "SORT_BY1" => "ACTIVE_FROM",
                              "SORT_ORDER1" => "DESC",
                              "SORT_BY2" => "SORT",
                              "SORT_ORDER2" => "ASC",
                              "FILTER_NAME" => "",
                              "FIELD_CODE" => array(
                                  0 => "",
                                  1 => "",
                              ),
                              "PROPERTY_CODE" => array(
                                  0 => "",
                                  1 => "",
                              ),
                              "CHECK_DATES" => "Y",
                              "DETAIL_URL" => "",
                              "AJAX_MODE" => "N",
                              "AJAX_OPTION_JUMP" => "N",
                              "AJAX_OPTION_STYLE" => "Y",
                              "AJAX_OPTION_HISTORY" => "N",
                              "CACHE_TYPE" => "Y",
                              "CACHE_TIME" => "300",
                              "CACHE_FILTER" => "N",
                              "CACHE_GROUPS" => "N",
                              "PREVIEW_TRUNCATE_LEN" => "",
                              "ACTIVE_DATE_FORMAT" => "d.m H:i",
                              "SET_TITLE" => "N",
                              "SET_STATUS_404" => "N",
                              "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                              "ADD_SECTIONS_CHAIN" => "Y",
                              "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                              "PARENT_SECTION" => "",
                              "PARENT_SECTION_CODE" => "",
                              "DISPLAY_TOP_PAGER" => "N",
                              "DISPLAY_BOTTOM_PAGER" => "Y",
                              "PAGER_TITLE" => "�������",
                              "PAGER_SHOW_ALWAYS" => "Y",
                              "PAGER_TEMPLATE" => "",
                              "PAGER_DESC_NUMBERING" => "N",
                              "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                              "PAGER_SHOW_ALL" => "Y",
                              "DISPLAY_DATE" => "Y",
                              "DISPLAY_NAME" => "Y",
                              "DISPLAY_PICTURE" => "Y",
                              "DISPLAY_PREVIEW_TEXT" => "Y",
                              "AJAX_OPTION_ADDITIONAL" => ""
                                  )
                       )
                   )
               )
            );
            ?>
         </div>
         <div class="nav_row">
            <a style="margin-left: -10px;" href="/personal/advert/" class="btns fl_right"><i></i>���������� ����������</a>
            <a href="/advert/" class="nodecor bold">��� ����������</a>
            <div class="clear"></div>
         </div>
      </div></div>
   </div>

<!--VS start midl2-->
   <div class="middle_col">
      <div class="mc_block">
         <div class=" double_brdr_box fl_left" style="min-width: 320px; min-height: 400px;">

	              <div class="rc_block toog withTitleMarket">
                       <ul class="mb_menu">
                          <li class="mb_title"><a href="#1">������<sup class="nodec"><?= GetCountElements(58); ?></sup></a><div class="sclew"></div></li>
                          <li class="sel"><a>������</a><div class="sclew"></div></li>
                          <li><a href="#3">������</a><div class="sclew"></div></li>
                       </ul>
                       <div class="gray_box catalog_list tab" style="padding-bottom:1px;">
                          <?
                          $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                              "SALT" => LAZYLOAD_SALT,
                              "COMPONENTS" => array(
                                  array(
                                      "NAME" => "bitrix:news.list",
                                      "TEMPLATE" => "list_main_service",
                                      "PARAMETERS" => array(
                                            "IBLOCK_TYPE" => "services",
                                            "IBLOCK_ID" => "58",
                                            "NEWS_COUNT" => "5",
                                            "SORT_BY1" => "ACTIVE_FROM",
                                            "SORT_ORDER1" => "DESC",
                                            "SORT_BY2" => "SORT",
                                            "SORT_ORDER2" => "ASC",
                                            "FILTER_NAME" => "",
                                            "FIELD_CODE" => array(
                                                0 => "",
                                                1 => "",
                                            ),
                                            "PROPERTY_CODE" => array(
                                                0 => "",
                                                1 => "",
                                            ),
                                            "CHECK_DATES" => "Y",
                                            "DETAIL_URL" => "",
                                            "AJAX_MODE" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "300",
                                            "CACHE_FILTER" => "N",
                                            "CACHE_GROUPS" => "N",
                                            "PREVIEW_TRUNCATE_LEN" => "",
                                            "ACTIVE_DATE_FORMAT" => "d.m H:i",
                                            "SET_TITLE" => "Y",
                                            "SET_STATUS_404" => "N",
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                            "ADD_SECTIONS_CHAIN" => "Y",
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                            "PARENT_SECTION" => "",
                                            "PARENT_SECTION_CODE" => "",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "Y",
                                            "PAGER_TITLE" => "�������",
                                            "PAGER_SHOW_ALWAYS" => "Y",
                                            "PAGER_TEMPLATE" => "",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "Y",
                                            "DISPLAY_DATE" => "Y",
                                            "DISPLAY_NAME" => "Y",
                                            "DISPLAY_PICTURE" => "Y",
                                            "DISPLAY_PREVIEW_TEXT" => "Y",
                                            "AJAX_OPTION_ADDITIONAL" => ""
                                                )
                                     )
                                 )
                             )
                          );
                          ?>
                          <div class="nav_row">
                             <a class="btns fl_right" href="/personal/company/"><i></i>���������� ������</a>
                             <a href="/market/" class="nodecor bold">��� ������</a><sup><?= GetCountElements(IBLOCK_PRODUCTS); ?></sup>
                             <div class="clear"></div>
                          </div>
                       </div>
                       <div class="gray_box catalog_list tab" style="padding-bottom: 1px; ">
                          <?
                          $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                              "SALT" => LAZYLOAD_SALT,
                              "COMPONENTS" => array(
                                  array(
                                      "NAME" => "bitrix:news.list",
                                      "TEMPLATE" => "list_main_service",
                                      "PARAMETERS" => array(
                                            "IBLOCK_TYPE" => "services",
                                            "IBLOCK_ID" => "64",
                                            "NEWS_COUNT" => "7",
                                            "SORT_BY1" => "ACTIVE_FROM",
                                            "SORT_ORDER1" => "DESC",
                                            "SORT_BY2" => "SORT",
                                            "SORT_ORDER2" => "ASC",
                                            "FILTER_NAME" => "",
                                            "FIELD_CODE" => array(
                                                0 => "",
                                                1 => "",
                                            ),
                                            "PROPERTY_CODE" => array(
                                                0 => "PROPERTY_FIRM",
                                                1 => "",
                                            ),
                                            "CHECK_DATES" => "Y",
                                            "DETAIL_URL" => "",
                                            "AJAX_MODE" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "CACHE_TYPE" => "N",
                                            "CACHE_TIME" => "300",
                                            "CACHE_FILTER" => "N",
                                            "CACHE_GROUPS" => "N",
                                            "PREVIEW_TRUNCATE_LEN" => "",
                                            "ACTIVE_DATE_FORMAT" => "d.m H:i",
                                            "SET_TITLE" => "Y",
                                            "SET_STATUS_404" => "N",
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                            "ADD_SECTIONS_CHAIN" => "Y",
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                            "PARENT_SECTION" => "",
                                            "PARENT_SECTION_CODE" => "",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "Y",
                                            "PAGER_TITLE" => "�������",
                                            "PAGER_SHOW_ALWAYS" => "Y",
                                            "PAGER_TEMPLATE" => "",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "Y",
                                            "DISPLAY_DATE" => "Y",
                                            "DISPLAY_NAME" => "Y",
                                            "DISPLAY_PICTURE" => "Y",
                                            "DISPLAY_PREVIEW_TEXT" => "Y",
                                            "AJAX_OPTION_ADDITIONAL" => ""
                                                )
                                     )
                                 )
                             )
                          );
                          ?>
                          <div class="nav_row">
                             <a class="btns fl_right" href="/personal/addprice/"><i></i>���������� �����</a>
                             <a href="/services/" class="nodecor bold">��� ������</a> <sup><?= GetCountElements(IBLOCK_USLUGI); ?></sup>
                             <div class="clear"></div>
                          </div>
                       </div>
                    </div><!--rc_block-->


         </div>
         <div class=" double_brdr_box fl_right">
	         <div class="rc_block toog " style="margin-bottom: 0px; min-width: 320px; min-height: 400px;">
                             <ul class="mb_menu">
                                <li class="mb_title"><span>��������</span><div class="sclew"></div></li>
                             </ul>

                             <div class="gray_box catalog_list tab" style="padding-bottom: 1px;">
                                <?
                                $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                                    "SALT" => LAZYLOAD_SALT,
                                    "COMPONENTS" => array(
                                        array(
                                            "NAME" => "bitrix:news.list",
                                            "TEMPLATE" => "list_main_kompany",
                                            "PARAMETERS" => array(
                                                  "IBLOCK_TYPE" => "directories",
                                                  "IBLOCK_ID" => "65",
                                                  "NEWS_COUNT" => "6",
                                                  "SORT_BY1" => "PROPERTY_EXT_SORT",
                                                  "SORT_ORDER1" => "DESC",
                                                  "SORT_BY2" => "SORT",
                                                  "SORT_ORDER2" => "ASC",
                                                  "FILTER_NAME" => "",
                                                  "FIELD_CODE" => array(
                                                      0 => "",
                                                      1 => "",
                                                  ),
                                                  "PROPERTY_CODE" => array(
                                                      0 => "PROPERTY_FIRM",
                                                      1 => "",
                                                  ),
                                                  "CHECK_DATES" => "Y",
                                                  "DETAIL_URL" => "",
                                                  "AJAX_MODE" => "N",
                                                  "AJAX_OPTION_JUMP" => "N",
                                                  "AJAX_OPTION_STYLE" => "Y",
                                                  "AJAX_OPTION_HISTORY" => "N",
                                                  "CACHE_TYPE" => "A",
                                                  "CACHE_TIME" => "300",
                                                  "CACHE_FILTER" => "N",
                                                  "CACHE_GROUPS" => "N",
                                                  "PREVIEW_TRUNCATE_LEN" => "",
                                                  "ACTIVE_DATE_FORMAT" => "d.m H:i",
                                                  "SET_TITLE" => "N",
                                                  "SET_STATUS_404" => "N",
                                                  "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                                  "ADD_SECTIONS_CHAIN" => "Y",
                                                  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                  "PARENT_SECTION" => "",
                                                  "PARENT_SECTION_CODE" => "",
                                                  "DISPLAY_TOP_PAGER" => "N",
                                                  "DISPLAY_BOTTOM_PAGER" => "Y",
                                                  "PAGER_TITLE" => "�������",
                                                  "PAGER_SHOW_ALWAYS" => "Y",
                                                  "PAGER_TEMPLATE" => "",
                                                  "PAGER_DESC_NUMBERING" => "N",
                                                  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                  "PAGER_SHOW_ALL" => "Y",
                                                  "DISPLAY_DATE" => "Y",
                                                  "DISPLAY_NAME" => "Y",
                                                  "DISPLAY_PICTURE" => "Y",
                                                  "DISPLAY_PREVIEW_TEXT" => "Y",
                                                  "AJAX_OPTION_ADDITIONAL" => ""
                                                      )
                                           )
                                       )
                                   )
                                );
                                ?>
                                <div class="nav_row">
                                   <a class="btns fl_right" href="/personal/company/"><i></i>���������� ��������</a>
                                   <a href="/company/" class="nodecor bold">��� ��������</a> <sup><?= GetCountElements(IBLOCK_COMPANY); ?></sup>
                                   <div class="clear"></div>
                                </div>
                             </div>
                          </div><!--rc_block-->
         </div>
         <div class="clear"></div>
      </div><!--mc_block-->
   </div>
   <!--VS end midl2-->
</div>

      <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"center",
			array(
				"TYPE" => "AGRO_A7",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "0",
				"CACHE_NOTES" => ""
			),
			false
		);
      ?>
	<div class="content_box" style="">


	<div class="right_col">


    <div class="rc_block brdr_box">

       <?$APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
           "SALT" => LAZYLOAD_SALT,
           "COMPONENTS" => array(
               array(
                   "NAME" => "bitrix:news.index",
                   "TEMPLATE" => "faces_right",
                   "PARAMETERS" => array(
                         "IBLOCK_TYPE" => "information_part",
                         "IBLOCKS" => Array("56"),
                         "NEWS_COUNT" => "9",
                         "IBLOCK_SORT_BY" => "SORT",
                         "IBLOCK_SORT_ORDER" => "ASC",
                         "SORT_BY1" => "date_active_from",
                         "SORT_ORDER1" => "DESC",
                         "SORT_BY2" => "ID",
                         "SORT_ORDER2" => "ASC",
                         "FIELD_CODE" => Array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
                         "PROPERTY_CODE" => Array(),
                         "FILTER_NAME" => "",
                         "IBLOCK_URL" => "/interview/",
                         "DETAIL_URL" => "/interview/#ELEMENT_CODE#/",
                         "ACTIVE_DATE_FORMAT" => "d-m-Y",
                         "CACHE_TYPE" => "Y",
                         "CACHE_TIME" => "3600",
                         "CACHE_GROUPS" => "Y"
                             )
                  )
              )
          )
       );?>
    </div>
	</div><!--rc_block-->

   <!--VS start midl3-->
   <div class="middle_col">
      <div class="mc_block withTitleArt">
         <ul class="mb_menu">
            <li class="mb_title sel"><a href="javascript: void(0)">���������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">���������������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">��������������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">�����������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">���������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">�����</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">����������</a><div class="sclew"></div></li>
         </ul>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part",
                           "IBLOCK_ID" => 55,
                           "NEWS_COUNT" => 5,
                           "SORT_BY1" => "ACTIVE_FROM",
                           "SORT_ORDER1" => "DESC",
                           "SORT_BY2" => "SORT",
                           "SORT_ORDER2" => "ASC",
                           "FILTER_NAME" => "",
                           "FIELD_CODE" => array("DETAIL_TEXT", "DETAIL_PICTURE"),
                           "PROPERTY_CODE" => array(),
                           "CHECK_DATES" => "Y",
                           "DETAIL_URL" => "",
                           "AJAX_MODE" => "N",
                           "AJAX_OPTION_JUMP" => "N",
                           "AJAX_OPTION_STYLE" => "Y",
                           "AJAX_OPTION_HISTORY" => "N",
                           "CACHE_TYPE" => "N",
                           "CACHE_TIME" => "3600",
                           "CACHE_FILTER" => "N",
                           "CACHE_GROUPS" => "N",
                           "PREVIEW_TRUNCATE_LEN" => "",
                           "ACTIVE_DATE_FORMAT" => "d.m H:i",
                           "SET_TITLE" => "Y",
                           "SET_STATUS_404" => "N",
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                           "ADD_SECTIONS_CHAIN" => "N",
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                           "PARENT_SECTION" => "",//default
                           "PARENT_SECTION_CODE" => "",
                           "DISPLAY_TOP_PAGER" => "N",
                           "DISPLAY_BOTTOM_PAGER" => "N",
                           "PAGER_TITLE" => "�������",
                           "PAGER_SHOW_ALWAYS" => "Y",
                           "PAGER_TEMPLATE" => "",
                           "PAGER_DESC_NUMBERING" => "N",
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                           "PAGER_SHOW_ALL" => "Y",
                           "DISPLAY_DATE" => "Y",
                           "DISPLAY_NAME" => "Y",
                           "DISPLAY_PICTURE" => "Y",
                           "DISPLAY_PREVIEW_TEXT" => "Y",
                           "AJAX_OPTION_ADDITIONAL" => ""
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "55", // ��� ��������������� �����
                           "NEWS_COUNT" => "5", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "3600", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "N", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "Y", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "3203", // ID ������� - rastenie
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "55", // ��� ��������������� �����
                           "NEWS_COUNT" => "5", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "3600", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "N", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "Y", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "3204", // ID ������� - zhivotno
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "55", // ��� ��������������� �����
                           "NEWS_COUNT" => "5", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "36000000", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "Y", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "Y", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "3205", // ID ������� - prodovolst (pererab)
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "55", // ��� ��������������� �����
                           "NEWS_COUNT" => "5", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "36000000", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "Y", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "Y", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "3206", // ID ������� - ekonomika
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "55", // ��� ��������������� �����
                           "NEWS_COUNT" => "5", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "36000000", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "Y", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "Y", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "3207", // ID ������� - nauka
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "55", // ��� ��������������� �����
                           "NEWS_COUNT" => "5", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "36000000", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "Y", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "Y", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "3208", // ID ������� - tehnologii
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                        )
                    )
                )
            )
         );
         ?>
      </div>
   </div>
   <!--VS end midl3-->
</div>
<!--<div class="middle_banner" id="3" style="height:60px;" style=" margin:0px 20px; overflow:hidden;">-->
<!-- </div> -->
<!-- (C)2000-2015 Gemius SA - gemiusTraffic / ver 11.1 / Glavnaja stranitsa sajta --> 
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var gemius_identifier = new String('bJg6RtSJC_28vyYdtDbpDcV4nMmN5BfvFJsurrTZJE3.h7');
//--><!]]>
</script>
<script type="text/javascript" src="http://agrobelarus.by/js/gemius.js"></script> 



<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>