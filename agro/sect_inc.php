<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if(!isset($_SESSION["SHOW_ME"]))
   $_SESSION["SHOW_ME"] = 1;
else
   $_SESSION["SHOW_ME"]++;
?>

<div class="lc_block">
   <div class="title catalog_title js-leftmenu-title" rel="1">
      <span>������� ������� � </span><a href="javascript:void(0);" class="dotted">��������</a>
   </div>
   <div class="title catalog_title js-leftmenu-title" rel="2" style="display: none">
      <span>������� </span><a href="javascript:void(0);" class="dotted">�������</a><span> � ��������</span>
   </div>
   <div class="js-leftmenu-content" rel="1">
      <?$APPLICATION->IncludeComponent("bitrix:menu", "left_menu", array(
            "ROOT_MENU_TYPE" => "left2",
        "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "36000000",
        "MENU_CACHE_USE_GROUPS" => "N",
        "MENU_CACHE_GET_VARS" => array(
        ),
        "MAX_LEVEL" => "2",
        "CHILD_MENU_TYPE" => "left",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "Y"
        ),
        false
    ); ?>
   </div>
   <div class="js-leftmenu-content" rel="2" style="display: none">
      <?$APPLICATION->IncludeComponent("bitrix:menu", "left_menu", array(
            "ROOT_MENU_TYPE" => "left",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_TIME" => "36000000",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => array(),
            "MAX_LEVEL" => "2",
            "CHILD_MENU_TYPE" => "left",
            "USE_EXT" => "Y",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "Y",
            "VISIBLE_COUNT" => 35
         ), false
      ); ?>
   </div>
</div><!--lc_block-->
<script>
   $(document).ready(function(){
      $('.js-leftmenu-title a').live("click", function(){
         var index = $(this).closest('.js-leftmenu-title').attr("rel");
         $('.js-leftmenu-title, .js-leftmenu-content')
            .not('.js-leftmenu-title[rel!='+index+'], .js-leftmenu-content[rel!='+index+']')
            .hide();
         $('.js-leftmenu-title[rel!='+index+'], .js-leftmenu-content[rel!='+index+']').show();
      })
   })
</script>
<!-- ad A5-1 -->
<div class="lc_block">
   <div class="left_banner_185">
      <?$APPLICATION->IncludeComponent(
	"bitrix:advertising.banner", 
	".default", 
	array(
		"TYPE" => "A5_1",
		"NOINDEX" => "N",
		"CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600"
         ), false
);?>
   </div>
</div><!--lc_block-->

<div class="lc_block">
   <div class="left_banner_185">
      <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
            "TYPE" => "A5_2",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "CACHE_NOTES" => ""
         )
      );?>
   </div>
</div><!--lc_block-->
<div class="lc_block">
   <div class="left_banner_185">
      <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
            "TYPE" => "A5_3",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "CACHE_NOTES" => ""
         )
      );?>
   </div>
</div><!--lc_block-->