<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//���� ��������, � ������� �������������� ������� �
if(!CModule::IncludeModule("iblock")) return;
if(ENABLE_PREDSTAVITEL_MODE)
{
   $arPredstavitelInfo = PredstavitelGetByUser($USER->GetID());
   if($arPredstavitelInfo["RELATED"])
   {
      $arrProductListFilter = array("PROPERTY_FIRM" => $arPredstavitelInfo["RELATED"], "ACTIVE" => "");
      $arCompany = CIBlockElement::GetByID($arPredstavitelInfo["RELATED"])->GetNext();
   }
}
else
{
$rsCompany = CIBlockElement::GetList(
   array("NAME" => "ASC", "SORT" => "ASC"),
   array(
    //  "IBLOCK_ID" => 65,
      "ACTIVE" => "Y",
      "PROPERTY_USER" => $USER->GetID()
   )
);
if($arCompany = $rsCompany->GetNext())
   {}
}
if(is_array($arCompany) && count($arCompany))
{
   $rsProducts = CIBlockElement::GetList(
      array(),
      array(
        // "IBLOCK_ID" => 64,
         "PROPERTY_FIRM" => $arCompany["ID"]
      )
   );
   while($product = $rsProducts->GetNext())
      $products[] = $product["ID"];
}
else LocalRedirect("/personal/company/");

// ���������� ������� � ���������� � ������������
$APPLICATION->IncludeComponent("bitrix:main.include", "title", array(
      "AREA_FILE_SHOW" => "sect",
      "AREA_FILE_SUFFIX" => "title",
      "AREA_FILE_RECURSIVE" => "Y",
      "EDIT_MODE" => "html",
    ), false, Array('HIDE_ICONS' => 'Y')
);
?>

<?$APPLICATION->IncludeComponent("whipstudio:product.add.form","", array(
      "COMPANY" => $arCompany,
      "PRODUCTS_IDS" => $products,
      "SEF_MODE" => "N",
      "IBLOCK_TYPE" => "services",
      //"IBLOCK_ID" => 64,
      "PROPERTY_CODES" => array(
          //1 �������
          "���������� �� ������" => array(
              array(
                  "IBLOCK_SECTION",
                  "NAME",
                  126,
                  "PREVIEW_PICTURE",
                  "DETAIL_TEXT"
               )
          )
      ),
      "PROPERTY_CODES_REQUIRED" => array(
         "IBLOCK_SECTION",
         "NAME",
         "PREVIEW_PICTURE",
         "DETAIL_TEXT"
      ),
      "GROUPS" => array(5), // ������ ������� ����� ���������/�������������
      "STATUS_NEW" => 1,
      "STATUS" => array(1, 2, 3),
      "LIST_URL" => "/personal/company/prod_and_serv/serv.php",
      "ELEMENT_ASSOC" => "", // �� ������ �������� ������ ����� � ��������������
      "ELEMENT_ASSOC_PROPERTY" => "",
      "MAX_USER_ENTRIES" => 1000000,
      "MAX_LEVELS" => 1, // ���������� ������ � ���������
      "LEVEL_LAST" => "N",
      "USE_CAPTCHA" => "N",
      "USER_MESSAGE_EDIT" => "",
      "USER_MESSAGE_ADD" => "",
      "DEFAULT_INPUT_SIZE" => 30,
      "RESIZE_IMAGES" => "Y",
      "MAX_FILE_SIZE" => 0,
      "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
      "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
      "CUSTOM_TITLE_IBLOCK_SECTION" => "���������",
      "CUSTOM_TITLE_NAME" => "",
      "CUSTOM_TITLE_PREVIEW_PICTURE" => "����",
      "CUSTOM_TITLE_264" => "�������������� ����",
      "CUSTOM_TITLE_243" => "������������",
      "CUSTOM_TITLE_DETAIL_TEXT" => "��������� ��������",
      "SEF_FOLDER" => "/",
      "VARIABLE_ALIASES" => array(),
      "THUMB_WIDTH" => 450,
      "THUMB_HEIGHT" => 150,
      "PAGE_TITLE" => "���������� ������",
   )
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
