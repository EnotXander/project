<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//���� ��������, � ������� �������������� ������� �
if(ENABLE_PREDSTAVITEL_MODE)
{
   $arPredstavitelInfo = PredstavitelGetByUserAgro($USER->GetID());
   if(!empty($arPredstavitelInfo["RELATED"]))
   {
      $APPLICATION->SetTitle("���������� ���������");
      $sSectionName = "���������� ���������";
   }
    else
   {
       $APPLICATION->SetTitle("�������� ��������");
       $sSectionName = "�������� ��������";
   }
}


else
{
   if(!CModule::IncludeModule("iblock")) return;
   $rsCompany = CIBlockElement::GetList(
      array("NAME" => "ASC", "SORT" => "ASC"),
      array(
         "IBLOCK_ID" => 65,
         "ACTIVE" => "Y",
         "PROPERTY_USER" => $USER->GetID()
      )
   );
   if($arCompany = $rsCompany->GetNext())
   {
      $APPLICATION->SetTitle("���������� ���������");
      $sSectionName = "���������� ���������";
   }
    else
   {
       $APPLICATION->SetTitle("�������� ��������");
       $sSectionName = "�������� ��������";
   }
}
?>

<? // ���������� ������� � ���������� � ������������
$APPLICATION->IncludeComponent("bitrix:main.include", "title", array(
      "AREA_FILE_SHOW" => "sect",
      "AREA_FILE_SUFFIX" => "title",
      "AREA_FILE_RECURSIVE" => "Y",
      "EDIT_MODE" => "html",
    ), false, Array('HIDE_ICONS' => 'Y')
);
?>

<?$APPLICATION->IncludeComponent("agro:iblock.element.add.form","", array(
      "SEF_MODE" => "N",
      "IBLOCK_TYPE" => "directories",
      "IBLOCK_ID" => 65,
      "PROPERTY_CODES" => array(
          //1 �������
          "������������ ����" => array(
              array(
                  "NAME",
                  531,
                  "IBLOCK_SECTION",
                  "DETAIL_TEXT",
                  539, // ������ 
                  540, // ������ 
                  541, // �����
                  543, // �����
                  548, // �����
                  545, // �������
                  534, // ��� email
                  535, // email ��� ��������
               )
          ),
          //2 �������
          "�������������� ����" => array(
              array("DETAIL_PICTURE"),
              array(
                  542, // ������
                  544, // ����� ������
                  546, // ����� ������
                  547, // ���� ��������
                  GetPropertyId("SKYPE", 65), // �����
              ),
              array(
                  GetPropertyId("RASCHETNY", 65), // ��������� ����
                  GetPropertyId("ADRESS_BANK", 65), // ����� �����
                  GetPropertyId("UNN", 65), // ���
              ),
              array(
                  GetPropertyId("ENGLISH_TITLE", 65), // ENGLISH_TITLE 
                  581, // ENGLISH_DESCRIPTION
              ),
              array(
                  549, // ����� � youtube
              )
         )
      ),
      "PROPERTY_CODES_REQUIRED" => array(
         "NAME",
         "IBLOCK_SECTION",
         "DETAIL_TEXT",
         545, 
         535,
         543,
         539,
         540, 
         541, 
      ),
      "GROUPS" => array(5), // ������ ������� ����� ���������/�������������
      "STATUS_NEW" => 1,
      "STATUS" => array(1, 2, 3),
      "LIST_URL" => "",
      "ELEMENT_ASSOC" => "PROPERTY_ID", // �� ������ �������� ������ ����� � ��������������
      "ELEMENT_ASSOC_PROPERTY" => "USER",
      "MAX_USER_ENTRIES" => 1000000,
      "MAX_LEVELS" => 100, // ���������� ������ � ���������
      "LEVEL_LAST" => "N",
      "USE_CAPTCHA" => "N",
      "USER_MESSAGE_EDIT" => "",
      "USER_MESSAGE_ADD" => "",
      "DEFAULT_INPUT_SIZE" => 30,
      "RESIZE_IMAGES" => "Y",
      "MAX_FILE_SIZE" => 0,
      "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
      "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
      "CUSTOM_TITLE_NAME" => "",
      "CUSTOM_TITLE_305" => "������������� ��������",
      "CUSTOM_TITLE_DETAIL_PICTURE" => "�������",
      "CUSTOM_TITLE_DETAIL_TEXT" => "���������� � ��������", 
      "CUSTOM_TITLE_IBLOCK_SECTION" => "����� ������������",
      "CUSTOM_TITLE_69" => "���� ��������",
      "CUSTOM_TITLE_70" => "Email ��� �����",
      "CUSTOM_TITLE_308" => "Email ��� ������",
      "CUSTOM_TITLE_102" => "��������� �� �����",    
      "CUSTOM_TITLE_316" => "Description in English",  
      "CUSTOM_TITLE_".GetPropertyId("ENGLISH_TITLE", 65) => "Title in english", 
      "SEF_FOLDER" => "/",
      "VARIABLE_ALIASES" => array()
   )
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
