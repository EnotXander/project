<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("C������� ������� �� ������� AgroBelarus.by");
?>
	<style>
   .content_box .left_col{
      width: auto !important;
      float: none !important;
   }
   .text_block{
	   margin: 0 240px 0 295px;

   }
	.bk_{
		background-image: url('bk.jpg');
		height: 800px;
	}
		.title_1{
			font-size: 24px;
			font-family: Myriad Pro;
			margin-bottom: 20px;
			color: #010101;
		}
		.title_2{
			font-size: 28px;
			font-family: Georgia;
			text-align: center;
			margin-bottom: 20px;
		}
		h1{
			font-family: georgia, serif;
			font-size: 28px;
			text-align: center;
			margin-bottom: 30px;
		}
		h2{
			font-family: "Arial", "sans-serif";
			font-size: 14px;
			margin-bottom: 30px;
			text-align: center;
			color: #58585a;
		}
		.green{
			color: #00a050;;
		}
		.grey_text {
			color: #ababab;
			margin-bottom: 50px;
			text-align: center;
		}
		.tel_block{
			background-image: url('tel_bk.png');
			width: 263px;
		    height: 98px;
		}
		.tel_block_tel{
			font-family: "Arial", "sans-serif";
			font-weight: bold;
			color: #fff;
			text-shadow: 1px 0  1px #000;
			font-size: 28px;
			text-align: center;
			padding-top: 9px;
		}
		.grey_line{
			width: 100%;;
			border-top: 2px solid #ededed;
			margin-bottom: 20px;
		}
		.input_block{
			width: 100%;;

		}
		.input_block{
			width: 100%;;
			font-size: 12px;
		}
		.input_block input, .input_block textarea{
			width: 95%;
			padding: 3px;
			border: 1px solid #cccccc;
		}
		.input_block input.red{
			color: red;
			border: 1px solid red;
		}
		.input_block textarea{
			height: 100px;

		}
		.input_block td{
			padding: 7px 0;
		}
		.input_block span{
			color: red;
		}
		.btn_send{
			float: right;
			display: inline-block;
			background-image: url('btn_send.png');
			height: 41px;
			width: 178px;
			background-position: -187px 0;
		}
		.btn_send:hover{
			background-image: url('btn_send.png');
			height: 41px;
			width: 178px;
			background-position: 0px 0;
		}
</style>
<div class="bk_">
	<div class="text_block">
		<h1>���������� � ������� ��������� �������</h1>
		<h2>
			<span class="green">��������� �������</span> � ��� ���������� ����������� ���������� ����������� ��������������������
			 ��������� �����������  ������-�������.
		</h2>
		<div class="title_2">
			������ ���������� ���� ��������� <br/>
			� ��������� �������?
		</div>
		<div class="title_2">
			������ ��������� ��� ��� ��������� ����� ������!
		</div>
		<div class="grey_text">������������ �������� �������� � ���� � ���������������� �� ���� �������� ����������.  </div>
		<form id="send_yarmarka">
			<table>
				<tr>
					<td>
						<div class="title_1">�������� ������ �� ����������:</div>
						<div class="grey_line"></div>
						<table class="input_block">
							<tr>
								<td><span>*</span> ���: </td>
								<td><input name="NAME" /></td>
							</tr>
							<tr>
								<td><span>*</span> �����������: </td>
								<td><input name="ORG" /></td>
							</tr>
							<tr>
								<td><span>*</span> ���������� �������:</td>
								<td><input name="TELL" /></td>
							</tr>
							<tr>
								<td>E-mail:</td>
								<td><input name="EMAIL" /></td>
							</tr>
							<tr>
								<td>����������:</td>
								<td><textarea name="MESSAGE" ></textarea></td>
							</tr>
							<tr>
								<td></td>
								<td><a class="btn_send" id="btn_send" href="#" ></a></td>
							</tr>
						</table>

					</td>
					<td style="padding-left: 90px;">
						<div  class="title_1">���������:</div>
						<div class="tel_block">
							<div class="tel_block_tel">(017) 380-49-26</div>
							<div class="tel_block_tel">(017) 290-22-25</div>
						</div>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>

<script type="application/javascript">
	$(function(){
		$(".input_block input[name=TELL]").mask("+999 (99) 999-99-99");

		$('#btn_send').bind('click', function(){
			$('#send_yarmarka').submit();

			return false;
		});

		$('#send_yarmarka').bind('submit', function(){
			// �������� ���������


			// ���������
			var errors=[];
			var fld = ['NAME', 'ORG', 'TELL'];
			$(fld).each(function(i, e ){
				var em = 'input[name='+e+']';
				// console.log( $(em).val() );
				if ($(em).val() == '') {
					errors.push (e);
					$(em).addClass('red');
				} else {
					$(em).removeClass('red');
				}
			});
			if (errors.length>0) return false;
			showOverlay();
			$.post('/_ajax/save_request_yarmarka.php', $(this).serialize(), function(data){
				// console.log(data);
				// �������� ���������
				if (data == 'ok') {
					alert('������� �� ����������� ������. � ��������� ����� � ���� �������� ������������� �������� � ���������������� �� ���� ��������.');

					var fld = ['NAME', 'ORG', 'TELL', 'EMAIL'];
					$(fld).each(function(i, e ){
						var em = 'input[name='+e+']';
						$(em).val('')
					});
					$('textarea[name="MESSAGE"]').val('');

				} else {
					alert('������ �������� ���������, �� �������� ��� ��� ��� ���������� ��������');
				}
				//
			}).always(function() {
				HideOverlay();	//������ ���������

			});
			return false;
		});
	});

	function showOverlay(){
	    var h = $('body').height();
	    $('#overlay').css('height', h).show();
	}
	function HideOverlay(){

	    $('#overlay').hide();
	}
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");