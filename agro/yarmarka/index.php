<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("C������� ������� �� ������� AgroBelarus.by");
?><?
$APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/components/bitrix/catalog/market2/catalog_styles.css");

?><?
$APPLICATION->IncludeComponent("bitrix:catalog", "market_yarmarka", array(
	"HIDE_TOP" => true,
	"SITE_NAME" => 'AgroBelarus.by',
	"PAGE_NAME" => 'C������� �������',
	"IBLOCK_PRODUCT_ID"=>"58",
	"IBLOCK_USLUGI_ID"=>"64",
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => IBLOCK_YARMARKA,
	"HIDE_NOT_AVAILABLE" => "N",
	"BASKET_URL" => "/personal/purchaser/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/yarmarka/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "Y",
	"USE_ELEMENT_COUNTER" => "Y",
	"USE_FILTER" => "N",
	"USE_REVIEW" => "N",
	"USE_COMPARE" => "N",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"CONVERT_CURRENCY" => "N",
	"SHOW_TOP_ELEMENTS" => "N",
	"SECTION_COUNT_ELEMENTS" => "Y",
	"SECTION_TOP_DEPTH" => "2",
	"PAGE_ELEMENT_COUNT" => $perPage,
	"LINE_ELEMENT_COUNT" => "1",
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER2" => "desc",
	"LIST_PROPERTY_CODE" => array(
		0 => "FIRM",
		1 => "COST",
		2 => "BREND",
		3 => "NUMBER",
		4 => "SEARCH_NUMBER",
		5 => "SPEC",
		6 => "KEYWORDS",
		7 => "",
	),
	"INCLUDE_SUBSECTIONS" => "Y",
	"LIST_META_KEYWORDS" => "UF_KEYWORDS",
	"LIST_META_DESCRIPTION" => "UF_DESCRIPTION",
	"LIST_BROWSER_TITLE" => "UF_SEO_TITLE",
	"DETAIL_PROPERTY_CODE" => array(
		0 => "FIRM",
		1 => "COST",
		2 => "BREND",
		3 => "NUMBER",
		4 => "SEARCH_NUMBER",
		5 => "SPEC",
		6 => "KEYWORDS",
		7 => "FAVORITES",
		8 => "SHOW_COUNTER",
	),
	"DETAIL_META_KEYWORDS" => "KEYWORDS",
	"DETAIL_META_DESCRIPTION" => "DESCRIPTION",
	"DETAIL_BROWSER_TITLE" => "BROWSER_TITLE",
	"LINK_IBLOCK_TYPE" => "",
	"LINK_IBLOCK_ID" => "",
	"LINK_PROPERTY_SID" => "",
	"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
	"USE_ALSO_BUY" => "N",
	"USE_STORE" => "N",
	"DISPLAY_TOP_PAGER" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "������",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"AJAX_OPTION_ADDITIONAL" => "",
	"SEF_URL_TEMPLATES" => array(
		"sections" => "",
		"section" => "#SECTION_CODE#/",
		"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
		"compare" => "compare.php?action=#ACTION_CODE#",
	),
	"VARIABLE_ALIASES" => array(
		"compare" => array(
			"ACTION_CODE" => "action",
		),
	)
	),
	false
);
?>

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	    (function (d, w, c) {
	        (w[c] = w[c] || []).push(function() {
	            try {
	                w.yaCounter33053858 = new Ya.Metrika({
	                    id:33053858,
	                    clickmap:true,
	                    trackLinks:true,
	                    accurateTrackBounce:true,
	                    webvisor:true
	                });
	            } catch(e) { }
	        });

	        var n = d.getElementsByTagName("script")[0],
	            s = d.createElement("script"),
	            f = function () { n.parentNode.insertBefore(s, n); };
	        s.type = "text/javascript";
	        s.async = true;
	        s.src = "https://mc.yandex.ru/metrika/watch.js";

	        if (w.opera == "[object Opera]") {
	            d.addEventListener("DOMContentLoaded", f, false);
	        } else { f(); }
	    })(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/33053858" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-68901233-1', 'auto');
	  ga('send', 'pageview');

	</script>
<!-- (C)2000-2015 Gemius SA - gemiusTraffic / ver 11.1 / Yarmarka --> 
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var gemius_identifier = new String('cjKapouRG_3nVa.2GdJlRbbtrswwxQLEZdvAx4JpDjb.E7');
//--><!]]>
</script>
<script type="text/javascript" src="http://agrobelarus.by/js/gemius.js"></script> 



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");