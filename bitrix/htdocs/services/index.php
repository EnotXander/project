<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������������");
?>


<?
$iblockID = 28;
if (isset($_REQUEST["ELEMENT_ID"])) // �������� �� ������ �������
{
   if (!CModule::IncludeModule("iblock")) die();
   $objElement = new CIBlockElement();
   $res = $objElement->GetList(array(), array("ID" => $_REQUEST["ELEMENT_ID"], "IBLOCK_ID" => $iblockID));
   if ($element = $res->GetNext()) 
      LocalRedirect($element["DETAIL_PAGE_URL"], false, "301 Moved Permanently");
   else
      LocalRedirect("/404.php", false, "404 Not found");
} else if(isset($_REQUEST["SECTION_ID"]))
{
   if (!CModule::IncludeModule("iblock")) die();
   $objSection = new CIBlockSection();
   $res = $objSection->GetList(array(), array("ID" => $_REQUEST["SECTION_ID"], "IBLOCK_ID" => $iblockID));
   if ($section = $res->GetNext())
      LocalRedirect($section["SECTION_PAGE_URL"], false, "301 Moved Permanently");
   else 
      LocalRedirect("/404.php", false, "404 Not found");
}

$APPLICATION->IncludeComponent("bitrix:catalog", "market", array(
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => $iblockID,
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/services/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"USE_FILTER" => "N",
	"USE_REVIEW" => "N",
	"USE_COMPARE" => "N",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"SHOW_TOP_ELEMENTS" => "N",
	"PAGE_ELEMENT_COUNT" => "30",
	"LINE_ELEMENT_COUNT" => "1",
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_ORDER" => "asc",
	"LIST_PROPERTY_CODE" => array(
		0 => "KEYWORDS",
		1 => "FIRM",
		2 => "COST",
		3 => "BREND",
		4 => "SEARCH_NUMBER",
		5 => "NUMBER",
		6 => "SPEC",
		7 => "",
	),
	"INCLUDE_SUBSECTIONS" => "Y",
	"LIST_META_KEYWORDS" => "UF_KEYWORDS",
	"LIST_META_DESCRIPTION" => "UF_DESCRIPTION",
	"LIST_BROWSER_TITLE" => "UF_SEO_TITLE",
	"DETAIL_PROPERTY_CODE" => array(
		0 => "KEYWORDS",
		1 => "FIRM",
		2 => "COST",
		3 => "BREND",
		4 => "SEARCH_NUMBER",
		5 => "NUMBER",
		6 => "SPEC",
		7 => "",
	),
	"DETAIL_META_KEYWORDS" => "KEYWORDS",
	"DETAIL_META_DESCRIPTION" => "DESCRIPTION",
	"DETAIL_BROWSER_TITLE" => "BROWSER_TITLE",
	"LINK_IBLOCK_TYPE" => "",
	"LINK_IBLOCK_ID" => "",
	"LINK_PROPERTY_SID" => "",
	"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
	"USE_ALSO_BUY" => "N",
	"DISPLAY_TOP_PAGER" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "������",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"AJAX_OPTION_ADDITIONAL" => "",
	"SEF_URL_TEMPLATES" => array(
		"sections" => "",
		"section" => "#SECTION_CODE#/",
		"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
		"compare" => "compare.php?action=#ACTION_CODE#",
	),
	"VARIABLE_ALIASES" => array(
		"compare" => array(
			"ACTION_CODE" => "action",
		),
	)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>