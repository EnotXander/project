<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arParams['GROUP_ID'] = Array(3);
$arResult = Array();

if($this->StartResultCache(false, $USER->GetGroups()))
{
	$filter = Array("GROUP_ID" => $arParams['GROUP_ID']);
	$rsUsers = CUser::GetList(($by="NAME"), ($order="ASC"), $filter); // выбираем пользователей
	$rsUsers->NavStart(15); // разбиваем постранично по 50 записей
	$arResult['NAV_RESULT'] = $rsUsers->NavPrint("Страницы");
	$arResult['USERS'] = Array();
	while($arUser = $rsUsers->GetNext()):
		$arResult['USERS'][] = $arUser;
	endwhile;

	$this->IncludeComponentTemplate();
}
?>
