<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="middle_col">
<form method="post" enctype="multipart/form-data" name="form_tov">
<div class="addproduct">
<!-- ---------------------- -->
<div class="subject">
<h1>�������� ����� ��� <a class="red" href="#" style="text-decoration: underline">����������� ������������</a></h1>
</div>

<!-- ---------------------- -->
<div class="tabs">
<ul class="mb_menu">
<li class="sel"><a href="#1">����� ���������</a><div class="sclew"></div></li>
<li><a href="/market/">����� ��������� �� �������� ������</a><div class="sclew"></div></li>
<li><a href="/advert/">���������� ����� ���������</a><div class="sclew"></div></li>
</ul>
</div>

<div class="category">
<table class="category" width="100%">
<tr>
<?foreach ($arResult['SECTIONS_BLOCK'] as $arSectionsBlock):?>

<td>
<select class="category" size="11" onchange="selectSection(this, this.value);">
<?foreach ($arSectionsBlock as $arSection):?>
<option value="<?=$arSection['ID']?>" <?if ($arSection['SELECTED']) echo 'selected';?>><?=$arSection['NAME']?></option>
<?endforeach;?>
</select>
</td>

<?if ($arSection['SELECTED']) $sectionId = $arSection['ID'];?>
<?endforeach;?>
</tr>
</table>
</div>
<input type="hidden" name="SECTION_ID" value="<?=$arSection['ID']?>">
<!-- ---------------------- -->

<div class="specify" style="padding: 15px 0px">
<h2 style="padding-bottom: 10px">������� �������� ���������� � ������</h2>

<div class="shape"></div>

<table width="100%" class="specify">

<tr>
<td class="ftd">��������:</td>
<td class="std"><input class="long" name="NAME" value="<?=$arResult['NAME']?>"> <b class="grey">�� ����� 200 ��������</b></td>
</tr>

<tr>
<td class="ftd">����:</td>
<td class="std">

<input class="short" name="PROPERTY[COST]" value="<?=$arResult['PROPERTIES']['COST']['VALUE']?>">

<select class="currency">
<option>BYR</option>
<option>RUB</option>
<option>USD</option>
</select>

��

<select class="number">
<option>��.</option>
<option>��.</option>
<option>��.</option>
</select>

</td>
</tr>

<tr>

<td class="ftd">����:</td>
<td class="std" colspan="2">
<?if (is_array($arResult['DETAIL_PICTURE'])):?>
<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" width="150"><br/>
<label><input type="checkbox" value="Y" name="del_DETAIL_PICTURE">�������</label><br/>
<?endif;?>

<div class="addphoto">
<div style="text-align: center; overflow: hidden">
<p class="addspec">���������� ����������</p>
<input type="file" size="1"  value="" name="PROPERTY-MORE_PHOTO-n0">
</div>
</div>

<div style="display:table-cell; text-align: center; vertical-align:middle;">
<b class="grey">����������� � ������� JPG, GIF, PNG �� 5 ��</b>
</div>
</td>

</tr>

<tr>
<td class="ftd">������������:</td>
<td class="std" colspan="2">

<div class="addspec">
<div style="text-align: center; overflow: hidden">
<p class="addspec">���������� �����</p>
<input type="file" size="1" value="" name="PROPERTY-files-n0">
</div>
</div>


</td>
</tr>

</table>

</div>

<div class="shape"></div>
<!-- ---------------------- -->
<div style="padding-top: 15px">
<h4 style="padding-bottom: 10px">��������� ��������:</h4>
	<?
	CModule::IncludeModule("fileman");
	$LHE = new CLightHTMLEditor;
	$LHE->Show(array(
		'id' => "DETAIL_TEXT_ID",
		'width' => '100%',
		'height' => '200px',
		'inputName' => "DETAIL_TEXT",
		'content' => $arResult["DETAIL_TEXT"],
		'bUseFileDialogs' => false,
		'bFloatingToolbar' => false,
		'bArisingToolbar' => false,
		'toolbarConfig' => array(
			'Bold', 'Italic', 'Underline', 'RemoveFormat',
			'CreateLink', 'DeleteLink', 'Image', 'Video',
			'BackColor', 'ForeColor',
			'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
			'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
			'StyleList', 'HeaderList',
			'FontList', 'FontSizeList',
		),
	));
	?>
<b class="grey">��������: 10 000 �������(��). <a class="rule" href="#">������� �������������� ������</a><br>
������ ���������: ���������� ����������, ����������� � ������� ����� ��� ������</b>
</div>
<!-- ---------------------- -->

<div class="short_desc" style="padding-top: 15px">

<h4 style="padding-bottom: 10px">������� ��������:</h4>
<textarea class="short_desc" name="PREVIEW_TEXT"><?=$arResult['PREVIEW_TEXT']?></textarea>
</div>
<input type="hidden" name="SAVE" value="Y">
<!-- ---------------------- -->
<div class="buttons" style="padding-top: 15px">


<div class="button"><a onclick="document.form_tov.submit(); return false;"><p class="button">��������� �����</p></a></div>
<!--<a href="#"><div class="wbutton"><p class="wbutton">��������� ��������</p></div></a>--></td>

</div>

</div>
</form>
</div>

<!-- ---------------------- -->
<!--
<div class="news-detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img class="detail_picture" border="0" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" />
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h3><?=$arResult["NAME"]?></h3>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<?foreach($arResult["FIELDS"] as $code=>$value):?>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			<br />
	<?endforeach;?>
	<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

		<?=$arProperty["NAME"]?>:&nbsp;
		<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
		<?else:?>
			<?=$arProperty["DISPLAY_VALUE"];?>
		<?endif?>
		<br />
	<?endforeach;?>
	<?
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>
-->