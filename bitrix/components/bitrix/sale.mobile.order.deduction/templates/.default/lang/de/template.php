<?
$MESS["SMODE_TITLE"] = "Bestellausf�hrung";
$MESS["SMODE_PRODUCT_NAME"] = "Produktname";
$MESS["SMODE_PRODUCT"] = "Produkt";
$MESS["SMODE_SAVE"] = "Speichern";
$MESS["SMODE_BACK"] = "Zur�ck";
$MESS["SMODE_STORE"] = "Lager";
$MESS["SMODE_BARCODE"] = "Strichcode";
$MESS["SMODE_DEDUCT"] = "Ausf�hren";
$MESS["SMODE_READY"] = "Fertig";
$MESS["SMODE_ERROR"] = "Fehler bei der Ausf�hrung";
$MESS["SMODE_DEDUCT_UNDO"] = "Abbrechen";
$MESS["SMODE_TITLE_UNDO"] = "Ausf�hrung abbrechen";
$MESS["SMODE_DEDUCT_UNDO_REASON"] = "Grund zum Abbrechen";
$MESS["SMODE_DEDUCT_HINT"] = "Klicken Sie auf \"Ausf�hren\", um die Bestellung abzuschlie�en.";
?>