<?
$MESS["F_INDEX_TEMPLATE"] = "P�gina de lista de foros";
$MESS["F_READ_TEMPLATE"] = "P�gina de tema le�do";
$MESS["F_URL_TEMPLATES"] = "Procesar URLs";
$MESS["F_DEFAULT_FID"] = "ID  del foro";
$MESS["F_DEFAULT_PATH_TO_SMILE"] = "Ruta para la carpeta de Smiles (relativo a la ra�z)";
$MESS["F_DEFAULT_PATH_TO_ICON"] = "Ruta para la carpeta de �conos (relativo a la ra�z)";
$MESS["F_SET_NAVIGATION"] = "Muestra Breadcrumb de navegaci�n";
$MESS["F_LIST_TEMPLATE"] = "P�gina de lista de temas";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "P�gina del perfil";
$MESS["F_DEFAULT_MID"] = "ID del mensaje";
$MESS["F_MESSAGE_TYPE"] = "Acci�n del mensaje";
$MESS["F_DATE_TIME_FORMAT"] = "Formato de fecha y formato";
$MESS["F_DISPLAY_PANEL"] = "Visualizar el panel de botones para este componente";
$MESS["F_MESSAGE_TEMPLATE"] = "P�gina de mensaje visto";
$MESS["F_SHOW_VOTE"] = "Mostrar encuestas";
$MESS["F_VOTE_CHANNEL_ID"] = "Grupo de encuestas";
$MESS["F_VOTE_GROUP_ID"] = "Grupos de usaurios permitidos a crear encuestas";
$MESS["F_VOTE_SETTINGS"] = "Configurar encuestas";
$MESS["F_NAME_TEMPLATE"] = "Formato de nombre";
?>