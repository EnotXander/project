<?
$MESS["SUP_EDIT_DEFAULT_TEMPLATE_PARAM_1_NAME"] = "U�klausos ID";
$MESS["SUP_EDIT_DEFAULT_TEMPLATE_PARAM_2_NAME"] = "U�klausim� s�ra�o puslapis";
$MESS["SUP_EDIT_MESSAGES_PER_PAGE"] = "Prane�im� skai�ius viename puslapyje";
$MESS["SUP_DESC_YES"] = "Taip";
$MESS["SUP_DESC_NO"] = "Ne";
$MESS["SUP_SET_PAGE_TITLE"] = "Nustatyti puslapio antra�t�";
$MESS["SUP_SHOW_COUPON_FIELD"] = "Atvaizduoti kupono �vedimo lauk�";
$MESS["SUP_MESSAGE_SORT_ORDER"] = "Biliet� prane�im� r��iavimo tvark�";
$MESS["SUP_MESSAGE_MAX_LENGTH"] = "Maksimalus nelau�tos eilut�s ilgis";
$MESS["SUP_MESSAGE_MAX_LENGTH_TIP"] = "Did�iausias nesuvyniotos teksto eilut�s ilgis, kurioje n�ra tarp� ar skyrybos �enkl�; ilgesn�s eilut�s bus suvyniotos.";
$MESS["SUP_SORT_ASC"] = "did�jan�ia tvarka ";
$MESS["SUP_SORT_DESC"] = "ma��jan�ia tvarka";
$MESS["SUP_SORT_FIELD_ID"] = "ID";
$MESS["SUP_SORT_FIELD_NUMBER"] = "Prane�imo numeris";
?>