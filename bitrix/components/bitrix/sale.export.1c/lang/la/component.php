<?
$MESS["CC_BSC1_ERROR_AUTHORIZE"] = "Error de autorizaci�n. Login � contrase�a equivocada.";
$MESS["CC_BSC1_PERMISSION_DENIED"] = "Usted no tiene permisos para ejecutar cambios. Revise configuraciones del componente.";
$MESS["CC_BSC1_ERROR_MODULE"] = "El M�dulo E-Store no est� instalado.";
$MESS["CC_BSC1_ERROR_HTTP_READ"] = "Error al leer data HTTP.";
$MESS["CC_BSC1_ERROR_UNKNOWN_COMMAND"] = "Comando desconocido";
$MESS["CC_BSC1_NO_ORDERS_IN_IMPORT"] = "N� de pedidos en xml.";
$MESS["CC_BSC1_ERROR_DIRECTORY"] = "Carpeta temporal err�nea.";
$MESS["CC_BSC1_ERROR_FILE_WRITE"] = "Error al escribir #FILE_NAME#.";
$MESS["CC_BSC1_ERROR_FILE_OPEN"] = "Error al abrir #FILE_NAME# para escribir.";
$MESS["CC_BSC1_ERROR_INIT"] = "Error en el inicio de la carpeta temporal";
$MESS["CC_BSC1_ZIP_ERROR"] = "Error mientras descomprime el archivo.";
$MESS["CC_BSC1_EMPTY_CML"] = "Importar archivo vac�o.";
$MESS["CC_BSC1_PRODUCT_NOT_FOUND"] = "Producto no encontrado para el pedido #";
$MESS["CC_BSC1_UNZIP_ERROR"] = "No puede extraer archivos desde el archivo. Por favor cargue un archivo no archivado.";
$MESS["CC_BSC1_FINAL_NOT_EDIT"] = "El pedido # #ID# no puede ser modificado (el estado es final, pagar � entregar permitidos.)";
$MESS["CC_BSC1_ORDER_NOT_FOUND"] = "El pedido # #ID# no puede ser encontrado en el sitio.";
$MESS["CC_BSC1_COM_INFO"] = "Informaci�n comercial";
$MESS["CC_BSC1_DOCUMENT"] = "Documento";
$MESS["CC_BSC1_OPERATION"] = "Transacci�n";
$MESS["CC_BSC1_ORDER"] = "Pedido";
$MESS["CC_BSC1_NUMBER"] = "N�mero";
$MESS["CC_BSC1_SUMM"] = "Monto";
$MESS["CC_BSC1_COMMENT"] = "Comentario";
$MESS["CC_BSC1_REK_VALUES"] = "Entrada de documentos";
$MESS["CC_BSC1_REK_VALUE"] = "Entrada de documentos";
$MESS["CC_BSC1_NAME"] = "Nombre";
$MESS["CC_BSC1_VALUE"] = "Valor";
$MESS["CC_BSC1_ITEMS"] = "Art�culos";
$MESS["CC_BSC1_ITEM"] = "Producto";
$MESS["CC_BSC1_PRICE_PER_UNIT"] = "Precio Unitario";
$MESS["CC_BSC1_QUANTITY"] = "Cantidad";
$MESS["CC_BSC1_PROPS_ITEMS"] = "Propiedades del Art�culo";
$MESS["CC_BSC1_PROP_ITEM"] = "Propiedades del Art�culo";
$MESS["CC_BSC1_ITEM_TYPE"] = "Tipo de art�culo";
$MESS["CC_BSC1_ID"] = "ID";
$MESS["CC_BSC1_TAXES"] = "Impuestos";
$MESS["CC_BSC1_TAX"] = "Impuesto";
$MESS["CC_BSC1_TAX_VALUE"] = "Valor del impuesto";
$MESS["CC_BSC1_IN_PRICE"] = "En el precio";
$MESS["CC_BSC1_SERVICE"] = "Servicio";
$MESS["CC_BSC1_CANCELED"] = "Cancelada";
$MESS["CC_BSC1_1C_PAYED_DATE"] = "Fecha de pago 1C";
$MESS["CC_BSC1_1C_PAYED_NUM"] = "No. de Pago 1C";
$MESS["CC_BSC1_1C_DELIVERY_DATE"] = "Fecha de Env�o 1C";
$MESS["SALE_EXPORT_FORM_SUMM"] = "Formato suma";
$MESS["SALE_EXPORT_FORM_QUANT"] = "Cantidad de Formato";
$MESS["SALE_EXPORT_FORM_CRD"] = "CRD";
$MESS["CC_BSC1_DISCOUNTS"] = "Descuentos";
$MESS["CC_BSC1_DISCOUNT"] = "Descuentos";
$MESS["CC_BSC1_DISCOUNT_PERCENT"] = "porcentaje";
$MESS["CC_BSC1_AGENT"] = "Contratista";
$MESS["CC_BSC1_AGENTS"] = "Contratistas";
$MESS["CC_BSC1_PRICE_ONE"] = "Precio";
$MESS["CC_BSC1_ITEM_UNIT"] = "Unidad";
$MESS["CC_BSC1_ITEM_UNIT_NAME"] = "Nombre completo";
$MESS["CC_BSC1_ITEM_UNIT_CODE"] = "ID";
$MESS["CC_BSC1_VERSION_1C"] = "N�mero de versi�n";
$MESS["CC_BSC1_ID_1C"] = "1CID";
$MESS["CC_BSC1_1C_DATE"] = "1CDate";
$MESS["CC_BSC1_1C_TIME"] = "Hora";
$MESS["CC_BSC1_ZIP_PROGRESS"] = "Ahora descomprima el archivo.";
$MESS["CC_BSC1_ZIP_DONE"] = "El archivo fue descomprimido.";
$MESS["CC_BSC1_ORDER_NO_AGENT_ID"] = "No se encontr� informaci�n del contratista para la orden ##ID#. La orden no fue creada.";
$MESS["CC_BSC1_ORDER_ADD_PROBLEM"] = "Error creando la orden ##ID#.";
$MESS["CC_BSC1_ORDER_USER_PROBLEM"] = "Error al registrar al usuarios cuando se crea la orden ##ID#.";
$MESS["CC_BSC1_ORDER_PERSON_TYPE_PROBLEM"] = "Tipo de pagados no puede ser definido por orden ##ID#.";
$MESS["CC_BSC1_AGENT_NO_AGENT_ID"] = "Informaci�n del contratista no encontrada.";
$MESS["CC_BSC1_AGENT_USER_PROBLEM"] = "Error al registrar al usuario cuando se crea el contratista ##ID#.";
$MESS["CC_BSC1_AGENT_DUPLICATE"] = "El contratista ##ID#. ya existe.";
$MESS["CC_BSC1_AGENT_PERSON_TYPE_PROBLEM"] = "Error al resolver el tipo de pago cuando se crea el contratista ##ID#.";
$MESS["CC_BSC1_DI_GENERAL"] = "Directorio";
$MESS["CC_BSC1_DI_STATUSES"] = "Estatus";
$MESS["CC_BSC1_DI_PS"] = "Systema de pago";
$MESS["CC_BSC1_DI_ELEMENT"] = "Elemento";
$MESS["CC_BSC1_DI_ID"] = "ID";
$MESS["CC_BSC1_DI_NAME"] = "Nombre";
$MESS["CC_BSC1_ERROR_SOURCE_CHECK"] = "Error reviando la fuente de  la solicitud. Por favor, actualice el m�dulo de intercambio.";
$MESS["CC_BSC1_ERROR_SESSION_ID_CHANGE"] = "Cambio de ID de sesi�n est� actvo. Edite el archivo en el cual el componente de intercambio se ha inckluido y defina la constante BX_SESSION_ID_CHANGE antes de incluir el c�digo prolog: define('BX_SESSION_ID_CHANGE', false);\";";
$MESS["CP_BCI1_CHANGE_STATUS_FROM_1C"] = "Utilice los datos 1C para establecer el estado de la orden";
$MESS["CC_BSC1_1C_STATUS_ID"] = "ID del estado del pedido";
$MESS["CC_BSC1_1C_DELIVERY_NUM"] = "Refencia de env�o 1C";
?>