<?
$MESS ['SPCA_FILTER'] = "Muestra per�odo";
$MESS ['SPCA_PERIOD'] = "Per�odo:";
$MESS ['SPCA_SET'] = "Fijar";
$MESS ['SPCA_UNSET'] = "Resetear";
$MESS ['SPCA_DATE'] = "Fecha";
$MESS ['SPCA_INCOME'] = "Renta ";
$MESS ['SPCA_OUTCOME'] = "Costos ";
$MESS ['SPCA_COMMENT'] = "Comentario ";
$MESS ['SPCA_NO_ACT'] = "Ninguna de las transacciones ocurri� durante el per�odo especificado ";
$MESS ['SPCA_UNACTIVE_AFF'] = "Eres el afiliado v�lido pero tu cuenta est� inactiva. Entrar en contacto por favor con la administraci�n para m�s informaci�n. ";
$MESS ['SPCA_ON_ACCT'] = "En cuenta para";
?>