<?
$MESS["SONET_C4_SUBMIT"] = "Abonnement bearbeiten";
$MESS["SONET_C3_ON_MAIL"] = "auf der Seite und per E-Mail";
$MESS["SONET_C3_ON_SITE"] = "nur auf der Seite";
$MESS["SONET_C4_T_CANCEL"] = "Abbrechen";
$MESS["SONET_C4_T_MY_SUBSCR"] = "Meine Abonnements verwalten";
$MESS["SONET_FEATURES_NAME"] = "�berschrift";
$MESS["SONET_C3_NOWHERE"] = "nein";
$MESS["SONET_C4_FUNC_TITLE"] = "Aktualisierung '#NAME#' anzeigen";
$MESS["SONET_C4_GR_SUCCESS"] = "Das Abonnement f�r die Gruppenaktualisierung wurde erfolgreich ge�ndert.";
$MESS["SONET_C4_US_SUCCESS"] = "Das Abonnement f�r die Nutzeraktualisierung wurde erfolgreich ge�ndert.";
$MESS["SONET_C3_VISIBLE_Y"] = "Anzeigen";
$MESS["SONET_C3_VISIBLE_N"] = "Ausblenden";
$MESS["SONET_C3_TRANSPORT_MAIL"] = "E-Mail-Benachrichtigung";
$MESS["SONET_C3_TRANSPORT_XMPP"] = "Sofortnachrichten";
$MESS["SONET_C3_TRANSPORT_DIGEST"] = "T�glicher Digest";
$MESS["SONET_C3_TRANSPORT_DIGEST_WEEK"] = "W�chentlicher Digest";
$MESS["SONET_C3_CREATED_BY_TITLE"] = "Von diesem Nutzer erstellte Events";
$MESS["SONET_C3_ENTITY_TITLE_U"] = "Events im Nutzerprofil";
$MESS["SONET_C3_ENTITY_TITLE_G"] = "Gruppenevents";
$MESS["SONET_C3_CREATED_BY"] = "Standardm��ig";
$MESS["SONET_C3_TRANSPORT_NONE"] = "Nicht benachrichtigen";
$MESS["SONET_C3_INHERITED"] = "Standardm��ig";
$MESS["SONET_C3_SHOW_IN_LIST"] = "Im Activity Stream anzeigen";
?>