<?
$MESS['BLOG_MODULE_NOT_INSTALL'] = 'Tinklara��i� modulis n�ra �diegtas';
$MESS['B_B_MES_REQUEST_ALREADY'] = 'J�s� u�klausa jau laukia tinklara��io autoriaus svarstymo';
$MESS['B_B_MES_REQUEST_ADDED'] = 'J�s� u�klausa priimta tinklara��io autoriaus svarstymui';
$MESS['B_B_MES_REQUEST_ERROR'] = 'Klaida �terpiant u�klaus�';
$MESS['B_B_MES_REQUEST_AUTH'] = 'Privalote autorizuotis kad gal�tum�te prid�ti u�klaus�';
$MESS['B_B_MES_NO_RIGHTS'] = 'Tr�ksta teisi� per�i�r�ti prane�im�';
$MESS['B_B_MES_NO_MES'] = 'Prane�imas nerastas';
$MESS['B_B_MES_FR_ONLY'] = '�io tinklara��io prane�imai prieinami tik autoriaus draugams.';
$MESS['B_B_MES_U_CAN'] = 'Jus galyte si�sti';
$MESS['B_B_MES_U_CAN1'] = 'u�klausim� tinklara��io autoriui';
$MESS['B_B_MES_U_CAN2'] = 'skaityti jo prane�im�';
$MESS['B_B_MES_U_AUTH'] = 'Privalote autorizuotis kad gal�tum�te si�sti u�klausim� skaityti prane�im�.';
$MESS['B_B_MES_NO_BLOG'] = 'Tinklara��io nerasta';
$MESS['B_B_MES_NO_POST'] = 'Prane�imas nerastas';
$MESS['BLOG_BLOG_BLOG_MES_DELED'] = 'Prane�imas s�kmingai pa�alintas';
$MESS['BLOG_BLOG_BLOG_MES_DEL_ERROR'] = 'Klaida �alinant prane�im�';
$MESS['BLOG_BLOG_BLOG_MES_DEL_NO_RIGHTS'] = 'Jums nesuteiktos teis�s �alinti �io prane�imo';
$MESS['BLOG_SONET_MODULE_NOT_AVAIBLE'] = 'Tinklara��iai neprieinami �iam naudotojui.';
?>