<?
$MESS["BLOG_URL_TIP"] = "Especifica el c�digo en el ID del blog que ser� pasado.";
$MESS["PATH_TO_BLOG_TIP"] = "La ruta de acceso a la p�gina principal del blog. Ejemplo: blog_blog.php?page=blog&blog=#blog#. ";
$MESS["PATH_TO_BLOG_CATEGORY_TIP"] = "La ruta de acceso a la p�gina con el filtro de etiqueta. Ejemplo: <nobr>blog_filter.php?page=blog&blog=#blog#<b>&category=#category#</b>.</nobr>";
$MESS["PATH_TO_POST_EDIT_TIP"] = "La ruta a la p�gina del blog del env�o. Ejemplo: blog_p_edit.php?page=post_edit&blog=#blog#&post_id=#post_id#.";
$MESS["PATH_TO_USER_TIP"] = "La ruta a la p�gina del perfil del usuario. Ejemplo: <nobr>blog_user.php?page=user&user_id=#user_id#.</nobr>";
$MESS["BLOG_VAR_TIP"] = "Especificar aqu� el nombre de la variable a la cual el ID del blog podr� ser pasado.";
$MESS["POST_VAR_TIP"] = "Especificar aqu� el nombre de la variable a la cual el ID del blog de env�o que podr� ser pasado.";
$MESS["USER_VAR_TIP"] = "Especificar aqu� el nombre de la variable a la cual el ID del blog del usuario que podr� ser pasado.";
$MESS["PAGE_VAR_TIP"] = "Especificar aqu� el nombre de la variable a la cual la p�gina del blog podr� ser pasado.";
$MESS["ID_TIP"] = "Especificar el c�digo en el cual el ID del env�o podr� ser pasado.";
$MESS["SET_NAV_CHAIN_TIP"] = "Si se selecciona, el nombre del blog parental que podr� ser agregado a la cadena de navegaci�n.";
$MESS["SET_TITLE_TIP"] = "Si la opci�n est� activa, el t�tulo de la p�gina podr� ser fijado para el t�tulo del env�o.";
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: el cach� es v�lida durante el tiempo predefinido en la configuraci�n del cach�;<br /><i>Cache</i>: el cach� es v�lido durante el tiempo predefinido en la configuraci�n del cach�;<br /><i>ning�n cach�</i>: no se realiza el almacenamiento en cach�.";
$MESS["CACHE_TIME_TIP"] = "Especificar el per�odo de tiempo durante el cual la memoria cach� es v�lida.";
$MESS["PATH_TO_SMILE_TIP"] = "La ruta a una carpeta que contiene emoticonos.";
$MESS["POST_PROPERTY_TIP"] = "Seleccione aqu�  propiedades adicionales del env�o que se mostrar�n en la p�gina de opini�n.";
?>