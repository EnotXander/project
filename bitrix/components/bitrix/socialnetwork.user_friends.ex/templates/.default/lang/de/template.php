<?
$MESS["SONET_UFE_T_FRIENDS_SUBTITLE"] = "Freunde";
$MESS["SONET_UFE_T_BAN_SUBTITLE"] = "Gesperrte Nutzer";
$MESS["SONET_UFE_T_ACTIONS_TITLE"] = "Aktionen";
$MESS["SONET_UFE_T_ACTION_ADDTOFRIENDS"] = "Freunde hinzuf�gen";
$MESS["SONET_UFE_T_ACTION_EXCLUDEFROMFRIENDS"] = "Aus Freunden entfernen";
$MESS["SONET_UFE_T_ACTION_EXCLUDEFROMFRIENDS_CONFIRM"] = "M�chten Sie diese Nutzer aus Freunden wirklich entfernen?";
$MESS["SONET_UFE_T_ACTION_BAN"] = "Sperren";
$MESS["SONET_UFE_T_ACTION_UNBAN"] = "Entsperren";
$MESS["SONET_UFE_T_FRIEND_ID_NOT_DEFINED"] = "Es wurde kein einziger Nutzer ausgew�hlt.";
$MESS["SONET_UFE_T_FRIEND_ID_INCORRECT"] = "W�hlen Sie einen Nutzer aus.";
$MESS["SONET_UFE_T_FRIEND_ID_INCORRECT_2"] = "Diese Operation kann auf ausgew�hlte Nutzer nicht angewendet werden..";
$MESS["SONET_UFE_T_USER_ID_NOT_DEFINED"] = "Nutzer wurde nicht gefunden.";
$MESS["SONET_UFE_T_NOT_ATHORIZED"] = "Sie sind nicht angemeldet.";
$MESS["SONET_UFE_T_MODULE_NOT_INSTALLED"] = "Das Modul Soziales Netzwerk ist nicht installiert.";
$MESS["SONET_UFE_T_NO_PERMS"] = "Sie haben nicht gen�gend Rechte, Freunde dieses Nutzers zu verwalten.";
$MESS["SONET_UFE_T_SESSION_WRONG"] = "Ihre Sitzung ist abgelaufen, versuchen Sie bitte erneut.";
$MESS["SONET_UFE_T_ACTION_FAILED"] = "Die Aktion brachte ein ung�ltiges Ergebnis: #ERROR#";
$MESS["SONET_UFE_T_WAIT"] = "Warten Sie bitte&hellip;";
$MESS["SONET_UFE_T_NO_FRIENDS"] = "Keine Freunde";
?>