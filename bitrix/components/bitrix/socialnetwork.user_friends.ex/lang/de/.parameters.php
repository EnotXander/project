<?
$MESS["SONET_SET_NAVCHAIN"] = "Breadcrumb-Navigation festlegen";
$MESS["SONET_ITEMS_COUNT"] = "Anzahl der Eintr�ge in der Liste";
$MESS["SONET_PATH_TO_USER"] = "Pfadvorlage zum Nutzerprofil";
$MESS["SONET_PATH_TO_SEARCH"] = "Pfadvorlage zur Seite der Nutzersuche";
$MESS["SONET_PAGE_VAR"] = "Variable f�r die Seite";
$MESS["SONET_USER_VAR"] = "Variable f�r den Nutzer";
$MESS["SONET_ID"] = "Nutzer-ID";
$MESS["SONET_VARIABLE_ALIASES"] = "Variablennamen";
$MESS["SONET_NAME_TEMPLATE"] = "Name anzeigen";
$MESS["SONET_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["SONET_SHOW_LOGIN"] = "Login anzeigen, wenn kein Name vorhanden";
$MESS["SONET_THUMBNAIL_LIST_SIZE"] = "Gr��e des Nutzerbildes (Px)";
?>