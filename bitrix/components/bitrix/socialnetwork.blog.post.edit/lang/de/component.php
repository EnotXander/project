<?
$MESS["BLOG_MODULE_NOT_INSTALL"] = "Das Modul \"Blog\" wurde nicht installiert.";
$MESS["B_B_MES_NO_BLOG"] = "Die Nachricht wurde nicht gefunden";
$MESS["BLOG_POST_EDIT"] = "Nachricht bearbeiten";
$MESS["BLOG_NEW_MESSAGE"] = "Erstellung einer neuen Nachricht";
$MESS["BLOG_ERR_NO_RIGHTS"] = "Fehler! Sie haben nicht gen�gend Rechte, um eine Nachricht zu schreiben";
$MESS["BLOG_P_INSERT"] = "Klicken Sie hier, um das Bild einzuf�gen";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "Nachrichten sind f�r diesen Nutzer nicht verf�gbar.";
$MESS["BLG_NAME"] = "Nutzernachrichten von";
$MESS["BLG_GRP_NAME"] = "Gruppennachrichten von";
$MESS["BPE_HIDDEN_POSTED"] = "Ihre Nachricht wurde erfolgreich hinzugef�gt. Die Nachrichten zu diesem Thema werden vormoderiert, Ihre Nachricht wird nach der �berpr�fung sichtbar.";
$MESS["BPE_SESS"] = "Ihre Sitzung ist abgelaufen. Bitte senden Sie die Nachricht erneut ab.";
$MESS["BPE_COPY_DELETE_ERROR"] = "Ein Fehler beim Versuch, die urspr�ngliche Nachricht zu l�schen.<br />";
$MESS["B_B_MES_NO_GROUP"] = "Die Gruppe des sozialen Netzwerks wurde nicht gefunden.";
$MESS["B_B_MES_NO_GROUP_ACTIVE"] = "Diese Funktionalit�t ist f�r die Gruppe des sozialen Netzwerks nicht verf�gbar.";
$MESS["B_B_MES_NO_GROUP_RIGHTS"] = "Sie haben nicht gen�gend Rechte, um eine Nachricht in der Gruppe des sozialen Netzwerks zu erstellen.";
$MESS["BLOG_BLOG_BLOG_MES_DEL_ERROR"] = "Fehler beim L�schen der Nachricht";
$MESS["BLOG_BLOG_BLOG_MES_DEL_NO_RIGHTS"] = "Sie haben nicht gen�gend Rechte, diese Nachricht zu l�schen";
$MESS["BLOG_BLOG_BLOG_MES_DEL_OK"] = "Die Nachricht wurde gel�scht";
$MESS["BLOG_BLOG_SESSID_WRONG"] = "Ihre Sitzung ist abgelaufen, versuchen Sie bitte erneut";
$MESS["BLOG_BPE_EXTRANET_ERROR"] = "Sie k�nnen die Nachricht nicht an alle senden, geben Sie zumindest einen Empf�nger an.";
$MESS["BLOG_SONET_GROUP_MODULE_NOT_AVAIBLE"] = "Nachrichten sind nicht verf�gbar f�r diese Nutzergruppe.";
$MESS["BLOG_GRAT_IBLOCKELEMENT_NAME"] = "Auszeichnung f�r #GRAT_NAME#";
$MESS["BLOG_EMPTY_TITLE_PLACEHOLDER"] = "Bild";
$MESS["BLOG_BPE_DESTINATION_EMPTY"] = "Geben Sie bitte mindestens eine Person an.";
$MESS["B_B_HIDDEN_GROUP"] = "Ausgeblendete Gruppe";
$MESS["B_B_PC_DUPLICATE_POST"] = "Sie haben Ihren Beitrag bereits hinzugef�gt";
?>