<?
$MESS["BPE_ID"] = "Nachricht-ID";
$MESS["BPE_PATH_TO_BLOG"] = "Vorlage des Pfads zur Seite der Nachrichten";
$MESS["BPE_PATH_TO_POST"] = "Vorlage des Pfads zur Seite der Nachricht";
$MESS["BPE_PATH_TO_USER"] = "Vorlage des Pfads zum Nutzerprofil";
$MESS["BH_PATH_TO_DRAFT"] = "Vorlage des Pfads zur Seite der Nachrichtenentw�rfe";
$MESS["BPE_PATH_TO_POST_EDIT"] = "Seite der Bearbeitung von Blogbeitr�gen";
$MESS["BPE_POST_VAR"] = "Variable f�r die Nachricht-ID";
$MESS["BPE_USER_VAR"] = "Variable f�r die Nutzer-ID";
$MESS["BPE_PAGE_VAR"] = "Variable f�r die Seite";
$MESS["POST_PROPERTY"] = "Zus�tzliche Nachrichteneigenschaften anzeigen";
$MESS["BB_PATH_TO_SMILE"] = "Pfad zum Smiley-Verzeichnis, relativ zum Root-Verzeichnis";
$MESS["B_VARIABLE_ALIASES"] = "Variablen";
$MESS["BC_DATE_TIME_FORMAT"] = "Datum- und Zeitformat";
$MESS["BPC_IMAGE_MAX_WIDTH"] = "Maximal zul�ssige Bildbreite";
$MESS["BPC_IMAGE_MAX_HEIGHT"] = "Maximal zul�ssige Bildh�he";
$MESS["BPC_ALLOW_POST_CODE"] = "Den Symbolcode der Nachricht als ID benutzen";
?>