<?
$MESS["SALE_SLS_ID_PARAMETER"] = "ID de ubicaci�n";
$MESS["SALE_SLS_CODE_PARAMETER"] = "C�digo simb�lico de ubicaci�n";
$MESS["SALE_SLS_INPUT_NAME_PARAMETER"] = "Nombre del campo de entrada";
$MESS["SALE_SLS_JSCONTROL_GLOBAL_ID_PARAMETER"] = "ID de control JavaScript";
$MESS["SALE_SLS_EXCLUDE_SUBTREE_PARAMETER"] = "Exclusi�n de elementos sub�rbol";
$MESS["SALE_SLS_FILTER_BY_SITE_PARAMETER"] = "Filtrar por sitio";
$MESS["SALE_SLS_FILTER_SITE_ID_PARAMETER"] = "Sitio";
$MESS["SALE_SLS_FILTER_SITE_ID_CURRENT"] = "actuales";
$MESS["SALE_SLS_JS_CALLBACK"] = "Llamada de retorno JavaScript";
$MESS["SALE_SLS_SHOW_DEFAULT_LOCATIONS_PARAMETER"] = "Mostrar ubicaciones predeterminadas";
$MESS["SALE_SLS_SEARCH_BY_PRIMARY_PARAMETER"] = "Buscar por ID y el c�digo";
$MESS["SALE_SLS_PROVIDE_LINK_BY_PARAMETER"] = "Enlazar con";
$MESS["SALE_SLS_PROVIDE_LINK_BY_PARAMETER_ID"] = "ID";
$MESS["SALE_SLS_PROVIDE_LINK_BY_PARAMETER_CODE"] = "C�digo simb�lico (c�digo)";
?>