<?
$MESS["STPC_TO_LIST"] = "De nuevo a lista ";
$MESS["STPC_TIMESTAMP"] = "La fecha pasada de la modificaci�n: ";
$MESS["STPC_ACTIV"] = "Activo: ";
$MESS["STPC_SORT"] = "�ndice de la clase: ";
$MESS["STPC_PAY_SYSTEM"] = "Sistema del pago: ";
$MESS["STPC_CURRENCY"] = "Modernidad en la cual se permiten los pagos:";
$MESS["STPC_ANY"] = "(cualesquiera) ";
$MESS["STPC_TYPE"] = "Tipo de la tarjeta: ";
$MESS["STPC_CNUM"] = "N�mero de la tarjeta de cr�dito: ";
$MESS["STPC_CEXP"] = "Fecha de vencimiento:";
$MESS["STPC_MIN_SUM"] = "Pago permitido m�nimo: ";
$MESS["STPC_MAX_SUM"] = "Pago permitido m�ximo:";
$MESS["STPC_SUM_CURR"] = "Modernidad del pago: ";
$MESS["STPC_SAVE"] = "Guardar";
$MESS["STPC_APPLY"] = "Aplicar";
$MESS["STPC_CANCEL"] = "Cancelar";
?>