<?
$MESS["WIKI_VERSION_FROM"] = "Ansicht";
$MESS["WIKI_PREV_VERSION"] = "Mit der fr�heren";
$MESS["WIKI_PREV_VERSION_TITLE"] = "Diese Version mit der fr�heren vergleichen";
$MESS["WIKI_CURR_VERSION"] = "Mit der aktuellen";
$MESS["WIKI_CURR_VERSION_TITLE"] = "Die aktuelle Version des Dokuments mit dieser Version vergleichen";
$MESS["WIKI_VERSION_TITLE"] = "Diese Version anzeigen";
$MESS["WIKI_VERSION"] = "Anzeigen";
$MESS["WIKI_DISCUSSION"] = "Diskussion";
$MESS["WIKI_DISCUSSION_TITLE"] = "Diskussion";
$MESS["WIKI_RECOVER"] = "Wiederherstellen";
$MESS["WIKI_RECOVER_TITLE"] = "Diese Version aktuell machen";
$MESS["WIKI_HISTORY_NOT_FIND"] = "Die Versionsgeschichte wurde nicht gefunden";
$MESS["WIKI_DIFF_VERSION"] = "Die ausgew�hlten Versionen vergleichen";
$MESS["NAV_TITLE"] = "Versionsgeschichte";
$MESS["DIFF_WITH"] = "Vergleichen";
$MESS["WIKI_SELECT_DIFF"] = "Zum Vergleich markieren";
$MESS["WIKI_COLUMN_AUTHOR"] = "Autor";
$MESS["WIKI_COLUMN_DATE_CREATE"] = "Erstellt am:";
$MESS["WIKI_ALL"] = "Gesamt";
$MESS["WIKI_DELETE_CURR_VERSION_TITLE"] = "Version l�schen";
$MESS["WIKI_DELETE_CURR_VERSION"] = "Version l�schen";
$MESS["WIKI_DELETE_CURR_VERSION_DELETE_CONFIRM"] = "M�chten Sie das wirklich l�schen?";
$MESS["WIKI_DELETE_CURR_VERSION_DELETE"] = "L�schen";
$MESS["WIKI_MODIFY_COMMENT"] = "Kommentar zum Update";
?>