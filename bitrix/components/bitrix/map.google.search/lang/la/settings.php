<?
$MESS["MYMV_SET_POPUP_TITLE"] = "Ajustes de mapa";
$MESS["MYMV_SET_POPUP_WINDOW_TITLE"] = "Editar configuraci�n del mapa";
$MESS["MYMV_SET_POPUP_WINDOW_DESCRIPTION"] = "Configurar diagrama del mapa y de los objetos visibles";
$MESS["MYMV_SET_START_POS"] = "Ubicaci�n inicial";
$MESS["MYMV_SET_START_POS_FIX"] = "Arreglar";
$MESS["MYMV_SET_START_POS_RESTORE"] = "Restaurar";
$MESS["MYMV_SET_START_POS_LAT"] = "Latitud";
$MESS["MYMV_SET_START_POS_LON"] = "Longitud";
$MESS["MYMV_SET_START_POS_SCALE"] = "Escala";
$MESS["MYMV_SET_START_POS_VIEW"] = "Ver";
$MESS["MYMV_SET_POINTS"] = "Signos de puntuaci�n";
$MESS["MYMV_SET_POINTS_ADD"] = "Agregar puntos";
$MESS["MYMV_SET_POINTS_ADD_DESCRIPTION"] = "Doble-click en el mapa para agregar signos de puntuaci�n.";
$MESS["MYMV_SET_POINTS_ADD_FINISH"] = "Terminar";
$MESS["MYMV_SET_SUBMIT"] = "Guardar";
$MESS["MYMV_SET_NONAME"] = "--- Sin t�tulo ---";
$MESS["MYMS_PARAM_INIT_MAP_TYPE_MAP"] = "Mapa";
$MESS["MYMS_PARAM_INIT_MAP_TYPE_SATELLITE"] = "Sat�lite";
$MESS["MYMS_PARAM_INIT_MAP_TYPE_HYBRID"] = "H�brido";
$MESS["MYMS_PARAM_INIT_MAP_NOTHING_FOUND"] = "(no se encontr�)";
$MESS["MYMV_SET_ADDRESS_SEARCH"] = "Buscar mapas";
$MESS["MYMS_PARAM_INIT_MAP_TYPE_TERRAIN"] = "terreno";
?>