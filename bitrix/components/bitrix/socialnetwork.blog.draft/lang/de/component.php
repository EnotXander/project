<?
$MESS["BLOG_MODULE_NOT_INSTALL"] = "Das Modul Blogs ist nicht installiert.";
$MESS["BLOG_BLOG_BLOG_MES_DELED"] = "Die Nachricht wurde erfolgreich gel�scht.";
$MESS["BLOG_BLOG_BLOG_MES_DEL_ERROR"] = "Fehler beim L�schen der Nachricht.";
$MESS["BLOG_BLOG_BLOG_MES_DEL_NO_RIGHTS"] = "Sie haben nicht gen�gend Rechte, um diese Nachricht zu l�schen.";
$MESS["BLOG_BLOG_BLOG_FRIENDS_ONLY"] = "Sie haben nicht gen�gend Rechte, um die Nachrichten in der Kommunikation zu lesen.";
$MESS["BLOG_BLOG_BLOG_NO_BLOG"] = "Blog wurde nicht gefunden.";
$MESS["MESSAGE_COUNT"] = "Nachrichten";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "Nachrichten sind f�r diesen Nutzer nicht verf�gbar.";
$MESS["BLOG_BLOG_BLOG_MES_PUB"] = "Nachricht wurde ver�ffentlicht.";
$MESS["BLOG_BLOG_BLOG_MES_PUB_ERROR"] = "Fehler bei der Ver�ffentlichung der Nachricht.";
$MESS["BLOG_BLOG_BLOG_MES_PUB_NO_RIGHTS"] = "Sie haben nicht gen�gend Rechte, um diese Nachricht zu ver�ffentlichen.";
$MESS["BLOG_BLOG_SESSID_WRONG"] = "Ihre Sitzung ist abgelaufen. Versuchen Sie bitte erneut.";
$MESS["BLOG_DR_ANOTHER_USER_DRAFT"] = "Sie haben nicht gen�gend Rechte, um die Entw�rfe von anderen Nutzern anzuzeigen.";
$MESS["BLOG_DR_TITLE"] = "Entw�rfe";
$MESS["BLOG_DR_PUB"] = "Ver�ffentlichen";
$MESS["BLOG_DR_DELETE"] = "L�schen";
$MESS["BLOG_DR_DELETE_CONFIRM"] = "M�chten Sie diese Nachricht wirklich l�schen?";
?>