<?
$MESS["SONET_MODULE_NOT_INSTALL"] = "Ne�diegtas socialinio tinklo modulis.";
$MESS["SONET_P_USER_NO_USER"] = "Vartotojas nerastas.";
$MESS["SONET_P_USER_NO_GROUP"] = "Grup� nerasta.";
$MESS["SONET_C11_TITLE"] = "Kvietimas � grup�";
$MESS["SONET_C11_NO_PERMS"] = "Neturite leidimo kviesti vartotoj� � �i� grup�.";
$MESS["SONET_C11_ERR_SELF"] = "Sav�s prie grup�s prid�ti negalite.";
$MESS["SONET_C11_BAD_USER"] = "Kvietimo �iam vartotojui si�sti negalima.";
$MESS["SONET_C11_BAD_RELATION"] = "�is vartotojas jau pakviestas � grup� arba jau yra jos narys.";
$MESS["SONET_C11_NO_MESSAGE"] = "Nenurodytas vartotojo prane�imas.";
?>