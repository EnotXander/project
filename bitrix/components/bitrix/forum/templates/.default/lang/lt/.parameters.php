<?
$MESS["F_SHOW_TAGS"] = "Atvaizduoti �ymes";
$MESS["F_DEFAULT_PATH_TO_SMILE"] = "Kelias iki �ypsen�li� katalogo ";
$MESS["F_DEFAULT_PATH_TO_ICON"] = "Kelias nuo �akninio katalogo iki tem� piktogram� katalogo ";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Navigacijos grandin�s �ablono pavadinimas";
$MESS["F_THEMES"] = "Temos";
$MESS["F_PAGE_NAVIGATION_WINDOW"] = "Puslapi� skai�ius puslapi� navigacijoje";
$MESS["F_THEME_BEIGE"] = "Sm�lio spalva";
$MESS["F_THEME_BLUE"] = "�ydra";
$MESS["F_THEME_FLUXBB"] = "FluxBB";
$MESS["F_THEME_GRAY"] = "Pilka";
$MESS["F_THEME_GREEN"] = "�alia";
$MESS["F_THEME_ORANGE"] = "Oran�in�";
$MESS["F_THEME_RED"] = "Raudona";
$MESS["F_THEME_WHITE"] = "Balta";
$MESS["F_SHOW_AUTH"] = "Rodyti leidimo suteikimo form�";
$MESS["F_SHOW_ADDITIONAL_MARKER"] = "Papildoma �ym� naujiems prane�imams";
$MESS["F_SHOW_NAVIGATION"] = "Rodyti nar�ymo kelio navigacij�";
$MESS["F_SHOW_SUBSCRIBE_LINK"] = "Rodyti prenumeratos nuorod�";
$MESS["F_SHOW_NAME_LINK"] = "Rodyti vardo nuorod�";
$MESS["F_SMILES_COUNT"] = "Rodom� statini� �ypsniuk� skai�ius (jei norite �ypsniukus apskai�iuoti dinami�kai, nustatykite ties 0)";
$MESS["F_SHOW_LEGEND"] = "Rodyti legend�";
$MESS["F_SHOW_FIRST_POST"] = "Visada rodyti pirm� prane�im�";
$MESS["F_SHOW_FORUMS"] = "Rodyti forumo sutrumpinimus";
$MESS["F_SHOW_AUTHOR_COLUMN"] = "Tem� puslapyje rodyti autoriaus stulpel�";
$MESS["F_SHOW_STATISTIC"] = "Rodyti statistik�";
$MESS["F_SEO_USER"] = "Neleisti paie�kos programoms indeksuoti vartotojo profilio nuorodos";
?>