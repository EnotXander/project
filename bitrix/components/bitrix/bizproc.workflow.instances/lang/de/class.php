<?
$MESS["BPWI_PAGE_TITLE"] = "Gestartete Workflows";
$MESS["BPWI_MODIFIED"] = "Ge�ndert am";
$MESS["BPWI_OWNED_UNTIL"] = "Gesperrt bis";
$MESS["BPWI_WS_STARTED"] = "Gestartet am";
$MESS["BPWI_WS_STARTED_BY"] = "Gestartet von";
$MESS["BPWI_WS_WORKFLOW_TEMPLATE_ID"] = "Workflow";
$MESS["BPWI_NO_MATTER"] = "(nicht relevant)";
$MESS["BPWI_YES"] = "Ja";
$MESS["BPWI_NO"] = "Nein";
$MESS["BPWI_WORKFLOW_ID_ANY"] = "(beliebig)";
$MESS["BPWI_FILTER_PRESET_LOCKED"] = "H�ngende";
$MESS["BPWI_FILTER_DOCTYPE_ALL"] = "Alle";
$MESS["BPWI_FILTER_DOCTYPE_CLAIMS"] = "Workflows";
$MESS["BPWI_FILTER_DOCTYPE_CRM"] = "CRM";
$MESS["BPWI_FILTER_DOCTYPE_DISK"] = "Dateien";
$MESS["BPWI_FILTER_DOCTYPE_LISTS"] = "Listen";
$MESS["BPWI_FILTER_STATUS"] = "Status";
$MESS["BPWI_NO_ACCESS"] = "Nur die Portaladministratoren haben Zugriff auf diese Seite.";
$MESS["BPWI_DELETE_LABEL"] = "L�schen";
$MESS["BPWI_DELETE_CONFIRM"] = "M�chten Sie diesen Prozess und alle damit zusammenh�ngenden Daten wirklich l�schen?";
$MESS["BPWI_OPEN_DOCUMENT"] = "Dokument (im neuen Fenster)";
$MESS["BPWI_NO_DOCUMENT"] = "Das Dokument wurde nicht gefunden.";
$MESS["BPWI_IS_LOCKED"] = "H�ngt";
$MESS["BPWI_WS_MODULE_ID"] = "Modul";
$MESS["BPWI_DOCUMENT_NAME"] = "Dokument";
$MESS["BPWI_MODULE_CRM"] = "CRM";
$MESS["BPWI_MODULE_IBLOCK"] = "Listen";
$MESS["BPWI_MODULE_DISK"] = "Drive";
$MESS["BPWI_MODULE_LISTS"] = "Workflows";
?>