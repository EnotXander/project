<?
$MESS["WIKI_MODULE_NOT_INSTALLED"] = "El m�dulo Wiki no est� instalado.";
$MESS["WIKI_SOCNET_INITIALIZING_FAILED"] = "Social Network fall� al inicializar.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "El m�dulo Information Blocks no se instalado.";
$MESS["IBLOCK_NOT_ASSIGNED"] = "No hay block de Informaci�n seleccionado.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "El m�dulo Social Network no est� instalado. ";
$MESS["WIKI_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["WIKI_CATEGORY_ALL"] = "Todas las p�ginas";
$MESS["WIKI_CATEGORY_ALL_TITLE"] = "Todas las p�ginas con cualquier categoria";
$MESS["WIKI_CATEGORY_NOCAT"] = "P�ginas sin categorias";
$MESS["WIKI_CATEGORY_NOCAT_TITLE"] = "Todas las p�ginas sin categorias";
?>