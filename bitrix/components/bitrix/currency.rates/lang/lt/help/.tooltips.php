<?
$MESS['arrCURRENCY_FROM_TIP'] = 'Pasirinkite valiutas kuri� vertes nor�site matyti lentel�je.';
$MESS['CURRENCY_BASE_TIP'] = 'Pasirinkite vien� i� nurodyt� valiut�. Kitos valiutos bus atvaizduojamos priklausomai nuo jos.';
$MESS['RATE_DAY_TIP'] = 'Pasirinkite dat� kuria turi atitikti valiutos kursas. Jeigu nurodytai datai valiutos kursas ne�inomas atvaizduojama vert� pagal nutyl�jim�.';
$MESS['SHOW_CB_TIP'] = 'Jeigu pa�ym�ta bus atvaizduojami valiut� kursai pagal Rusijos banko duomenis. Veikia tuo atveju jeigu laukas \"Valiuta konveruotjama �\" nustatytas RUR.';
$MESS['CACHE_TYPE_TIP'] = '<i>Auto</i>: pod�lis (cache) galioja  pod�lio nustatymuose nurodyt� laiko tarp�;
<i>Ke�uoti</i>:Visada ke�uoti nurodyt� laiko tarp�;
<i>Neke�uoti</i>:pod�lis nenaudojamas';
$MESS['CACHE_TIME_TIP'] = 'Lauke nurodomas pod�lio (cache) naudojimo laikas sekund�mis.';
?>