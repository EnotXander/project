<?
$MESS["WIKI_MODULE_NOT_INSTALLED"] = "Wiki-Modul ist nicht installiert.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Modul des Informationsblocks ist nicht installiert.";
$MESS["IBLOCK_NOT_ASSIGNED"] = "Informationsblock wurde nicht ausgew�hlt";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Modul Soziales Netzwerk ist nicht installiert.";
$MESS["WIKI_SOCNET_INITIALIZING_FAILED"] = "Fehler bei der Initialisierung des sozialen Netzwerks";
$MESS["WIKI_ACCESS_DENIED"] = "Zugriff verweigert.";
$MESS["WIKI_SESS_TIMEOUT"] = "Ihre Sitzung ist abgelaufen. Bitte speichern Sie die Seite erneut.";
$MESS["WIKI_IMAGE_INSERT"] = "Ein Bild einf�gen";
$MESS["WIKI_PAGE_NOT_FIND"] = "Diese Seite enth�lt zur Zeit keinen Text. Sie k�nnen <a href=\"%LINK%\">eine Seiten mit diesem Namen erstellen</a>.";
$MESS["WIKI_DEFAULT_DETAIL_TEXT"] = "%NEWLINE%%HEAD%%NEWLINE%%NEWLINE%Diese Seite enth�lt zur Zeit keinen Text.";
$MESS["WIKI_ERROR_RENAME"] = "Eine Seite mit dem angegebenen  Namen existiert bereits";
$MESS["WIKI_ERROR_NAME_BAD_SYMBOL"] = "Der Name enth�lt nicht zul�ssige Zeichen (/)";
$MESS["WIKI_IMAGE_UPLOAD_ERROR"] = "Bilder k�nnen nur in folgenden Formaten hochgeladen werden: jpg, bmp, jpeg, jpe, gif, png";
$MESS["WIKI_ERROR_NAME_EMPTY"] = "Der Seitentitel wurde nicht angegeben";
$MESS["WIKI_CHOISE_CATEGORY"] = "W�hlen Sie die Kategorie aus";
$MESS["FILE_NAME"] = "Datei";
$MESS["CATEGORY_NAME"] = "Kategorie";
$MESS["WIKI_SONET_LOG_TITLE_TEMPLATE"] = "Artikel \"#TITLE#\" wurde hinzugef�gt oder aktualisiert";
$MESS["WIKI_DEL_SONET_LOG_TITLE_TEMPLATE"] = "Wiki-Artikel \"#TITLE#\" wurde gel�scht";
$MESS["WIKI_ERROR_TEXT_EMPTY"] = "Die Seite enth�lt keinen Text.";
$MESS["WIKI_DELETE_ERROR"] = "Fehler beim L�schen der Seite";
$MESS["WIKI_MODIFY_COMMENT"] = "Kommentar zum Update";
$MESS["WIKI_MODIFY_COMMENT_ABSENT"] = "Keine";
$MESS["WIKI_REDIRECT"] = "Umleiten";
$MESS["WIKI_PAGE_RENAMED"] = "Seite %OLD_NAME% wurde umbenannt zu %NEW_NAME%.";
$MESS["WIKI_SONET_IM_ADD"] = "Eine neue Wiki-Seite \"#title#\" wurde zur Gruppe #group_name# hinzugef�gt.";
$MESS["WIKI_SONET_IM_EDIT"] = "Die Wiki-Seite \"#title#\" in der Gruppe #group_name# wurde aktualisiert.";
$MESS["WIKI_SONET_IM_DELETE"] = "Die Wiki-Site \"#title#\" wurde aus der Gruppe #group_name# entfernt.";
$MESS["WIKI_SONET_IM_RENAME"] = "Die Wiki-Seite \"#title_old#\" in der Gruppe #group_name#r wurde in \"#title_new#\" umbenannt.";
?>