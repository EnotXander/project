<?
$MESS["BP_PATH_TO_BLOG"] = "Vorlage des Pfads zur Seite der Nachrichten";
$MESS["BP_PATH_TO_BLOG_CATEGORY"] = "Vorlage des Pfads zur Seite der Nachrichten mit dem Tagfilter";
$MESS["BP_PATH_TO_POST_EDIT"] = "Vorlage des Pfads zur Seite der Nachrichtbearbeitung";
$MESS["BP_PATH_TO_USER"] = "Vorlage des Pfads zum Nutzerprofil";
$MESS["BP_POST_VAR"] = "Variable f�r die Nachricht-ID";
$MESS["BP_USER_VAR"] = "Variable f�r die Nutzer-ID";
$MESS["BP_PAGE_VAR"] = "Variable f�r die Seite";
$MESS["BP_SET_NAV_CHAIN"] = "Punkt in die Navigationskette hinzuf�gen";
$MESS["BP_ID"] = "Nachricht-ID";
$MESS["POST_PROPERTY"] = "Zus�tzliche Nachrichteneigenschaften anzeigen";
$MESS["BB_PATH_TO_SMILE"] = "Pfad zum Smiley-Verzeichnis, relativ zum Root-Verzeichnis";
$MESS["B_VARIABLE_ALIASES"] = "Variablen";
$MESS["BC_DATE_TIME_FORMAT"] = "Datum- und Zeitformat";
$MESS["SHOW_RATING"] = "Ranking aktivieren";
$MESS["SHOW_RATING_CONFIG"] = "Standardm��ig";
$MESS["RATING_TYPE"] = "Design der Ranking-Schaltfl�chen";
$MESS["RATING_TYPE_CONFIG"] = "Standardm��ig";
$MESS["RATING_TYPE_STANDART_TEXT"] = "Gef�llt mir / Gef�llt mir nicht (Text)";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "Gef�llt mir / Gef�llt mir nicht (Bild)";
$MESS["RATING_TYPE_LIKE_TEXT"] = "Gef�llt mir (Text)";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "Gef�llt mir (Bild)";
$MESS["BPC_IMAGE_MAX_WIDTH"] = "Maximal zul�ssige Bildbreite";
$MESS["BPC_IMAGE_MAX_HEIGHT"] = "Maximal zul�ssige Bildh�he";
?>