<?
$MESS["F_MODERATOR"] = "Moderatorius";
$MESS["F_EDITOR"] = "Redaktorius";
$MESS["F_ADMIN"] = "Administratorius";
$MESS["F_GUEST"] = "Sve�ias";
$MESS["F_USER"] = "Vartotojas";
$MESS["F_NO_MODULE"] = "Forumo modulis ne�diegtas";
$MESS["F_TITLE_NAV"] = "Prane�imai";
$MESS["F_MESS_SUCCESS_ADD"] = "Skelbimai prid�ti s�kmingai";
$MESS["F_ERROR_TID_IS_LOST"] = "Tema nerasta";
$MESS["FORUM_SONET_MODULE_NOT_AVAIBLE"] = "Forumo �iam vartotojui n�ra.";
$MESS["SONET_MODULE_NOT_INSTALL"] = "Ne�diegtas socialinio tinklo modulis.";
$MESS["FORUM_SONET_NO_ACCESS"] = "Nepakanka leidim� �iam forumui per�i�r�ti.";
$MESS["F_MID_IS_LOST"] = "Tokio skelbimo nerasta";
$MESS["F_FORUM"] = "Forumas";
?>