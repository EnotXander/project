<?
$MESS["LISTS_JS_STATUS_ACTION_SUCCESS"] = "Erfolgreich";
$MESS["LISTS_JS_STATUS_ACTION_ERROR"] = "Fehler";
$MESS["LISTS_ADD_STAFF"] = "Mitarbeiter hinzuf�gen";
$MESS["LISTS_ADD_STAFF_MORE"] = "Mehr hinzuf�gen";
$MESS["LISTS_TITLE_SET_RIGHT"] = "Zugriffsrechte definieren f�r:";
$MESS["LISTS_SELECT_STAFF_SET_RIGHT"] = "W�hlen Sie Nutzer aus, f�r die Sie Zugriffsrechte definieren m�chten.";
$MESS["LISTS_SAVE_BUTTON_SET_RIGHT"] = "Speichern";
$MESS["LISTS_CANCEL_BUTTON_SET_RIGHT"] = "Abbrechen";
$MESS["LISTS_SELECT_STAFF_SET_RESPONSIBLE"] = "Verantwortliche definieren";
$MESS["LISTS_SELECT_STAFF_SET_RESPONSIBLE_NEW"] = "Workflow-Parameter konfigurieren";
$MESS["LISTS_NOTIFY_ADMIN_TITLE_WHY"] = "Warum der Workflow #NAME_PROCESSES# nicht verf�gbar ist";
$MESS["LISTS_NOTIFY_ADMIN_TEXT_ONE"] = "Der Workflow \"#NAME_PROCESSES#\" muss konfiguriert werden";
$MESS["LISTS_NOTIFY_ADMIN_TEXT_TWO"] = "Eine Nachricht an einen der Nutzer senden, die den Workflow \"#NAME_PROCESSES#\" konfigurieren k�nnen bzw. entsprechende Zugriffsrechte an Sie delegieren k�nnen.";
$MESS["LISTS_NOTIFY_ADMIN_MESSAGE"] = "Benachrichtigen:";
$MESS["LISTS_NOTIFY_ADMIN_MESSAGE_BUTTON"] = "Nachricht senden:";
$MESS["LISTS_CANCEL_BUTTON_CLOSE"] = "Schlissen";
$MESS["LISTS_CANCEL_BUTTON_INSTALL"] = "Installieren";
$MESS["LISTS_TITLE_POPUP_MARKETPLACE"] = "Workflow-Verzeichnis";
$MESS["LISTS_MARKETPLACE_TITLE_SYSTEM_PROCESSES"] = "Vordefinierte Workflows";
$MESS["LISTS_MARKETPLACE_TITLE_USER_PROCESSES"] = "Nutzer-Workflows";
?>