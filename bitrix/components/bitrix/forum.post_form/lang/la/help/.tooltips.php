<?
$MESS["FID_TIP"] = "ID del Foro";
$MESS["TID_TIP"] = "ID del Tema";
$MESS["MID_TIP"] = "ID del Env�o";
$MESS["PAGE_NAME_TIP"] = "El ID del componente llamado";
$MESS["URL_TEMPLATES_MESSAGE_TIP"] = "P�gina de la vista del Post";
$MESS["URL_TEMPLATES_LIST_TIP"] = "P�gina de los Temas";
$MESS["URL_TEMPLATES_HELP_TIP"] = "P�gina de ayuda del Foro";
$MESS["URL_TEMPLATES_RULES_TIP"] = "P�gina de reglas del Foro";
$MESS["PATH_TO_SMILE_TIP"] = "Ruta a la carpeta de smiley, relativa a la ra�z del sitio";
$MESS["PATH_TO_ICON_TIP"] = "Ruta a la carpeta de los �conos tem�ticos, relativo a la ra�z del sitio.";
$MESS["SMILE_TABLE_COLS_TIP"] = "Smileys por fila";
$MESS["AJAX_TYPE_TIP"] = "Usar AJAX";
$MESS["CACHE_TYPE_TIP"] = "Tipo de Cach�";
$MESS["CACHE_TIME_TIP"] = "Tiempo de vida del Cach�. (sec.)";
$MESS["SHOW_VOTE_TIP"] = "Mostrar Encuestas";
$MESS["MESSAGE_TYPE_TIP"] = "Editor de dise�o del formulario (respuesta; nuevo tema;  modificar)";
?>