<?
$MESS["F_DEFAULT_PATH_TO_ICON"] = "Kelias nuo �akninio katalogo iki tem� ikon�li� katalogo ";
$MESS["F_DEFAULT_PATH_TO_SMILE"] = "Kelias iki �ypsen�li� katalogo ";
$MESS["F_DEFAULT_MESSAGE_TYPE"] = "Redagavimo formos atvaizdavimo tipas (atsakymas, redagavimas, nauja tema)";
$MESS["F_DEFAULT_FID"] = "Forumo ID";
$MESS["F_DEFAULT_TID"] = "Temos ID";
$MESS["F_DEFAULT_MID"] = "Prane�imo ID";
$MESS["F_DEFAULT_PAGE_NAME"] = "I�kvie�ian�io komponento ID";
$MESS["F_LIST_TEMPLATE"] = "Tem� s�ra�o puslapis";
$MESS["F_MESSAGE_TEMPLATE"] = "Prane�im� skaitymo puslapis";
$MESS["F_SMILE_TABLE_COLS"] = "�ypsen�li� skai�ius eilut�je";
$MESS["F_HELP_TEMPLATE"] = "Forumo pagalbos puslapis";
$MESS["F_RULES_TEMPLATE"] = "Forumo taisykli� puslapis";
$MESS["F_SHOW_VOTE"] = "Rodyti apklausas";
$MESS["F_VOTE_CHANNEL_ID"] = "Apklausos grup�";
$MESS["F_VOTE_GROUP_ID"] = "Vartotoj� grup�s, kurioms leid�iama kurti apklausas";
$MESS["F_VOTE_SETTINGS"] = "Apklausos nustatymai";
?>