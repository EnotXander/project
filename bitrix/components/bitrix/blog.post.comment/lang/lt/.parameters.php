<?
$MESS['BPC_BLOG_URL'] = 'Tinklara��io URL adresas';
$MESS['BPC_PATH_TO_BLOG'] = 'Kelio �ablonas � tinklara��io puslap�';
$MESS['BPC_PATH_TO_USER'] = 'Kelio �ablonas � tinklara��io naudotojo puslap�';
$MESS['BPC_PATH_TO_SMILE'] = 'Kelias � aplank� su �ypsen�l�mis, svetain�s �akninio aplanko at�vilgiu';
$MESS['BPC_BLOG_VAR'] = 'Tinklara��io identifikavimo kintamasis';
$MESS['BPC_USER_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io naudotojo identifikatorius';
$MESS['BPC_POST_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io prane�imo identifikatorius';
$MESS['BPC_PAGE_VAR'] = 'Puslapio kintamojo pavadinimas';
$MESS['BPC_ID'] = 'Prane�imo ID';
$MESS['BPC_COMMENTS_COUNT'] = 'Komentar� skai�ius puslapyje';
$MESS['BPC_SIMPLE_COMMENT'] = 'Supaprastintas re�imas (be prane�imo k�rimo)';
$MESS['B_VARIABLE_ALIASES'] = 'Kintam�j� vardai';
$MESS['BC_DATE_TIME_FORMAT'] = 'Datos ir laiko atvaizdavimo formatas';
$MESS['BPC_USE_ASC_PAGING'] = 'Naudoti tiesi� navigacijos grandin�';
$MESS['BPC_COMMENT_ID_VAR'] = 'Kintamojo vardas identifikuoti komentar�';
?>