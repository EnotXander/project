<?
$MESS['IBLOCK_TYPE_TIP'] = 'I�skleid�iamajame s�ra�e galite pasirinkti vien� i� sukurt� sistemoje informacini� blok� tip�. Paspaudus mygtuk� <b><i>OK</i></b> bus �kelti pasirinkto tipo informaciniai blokai.';
$MESS['IBLOCK_ID_TIP'] = 'Pasirinkite vien� i� esan�i� informacini� blok�. Pasirinkus (kitas) gretimame lange tur�site nurodyti informacinio bloko ID.';
$MESS['SECTION_ID_TIP'] = 'Laukas nurodo kod� kuriuo perduodamas skyriaus ID. Reik�m� pagal nutyl�jim� ={$_REQUEST["SECTION_ID"]}.';
$MESS['ELEMENT_ID_TIP'] = 'Laukas nurodo kod� kuriuo perduodamas elemento ID. Reik�m� pagal nutyl�jim� ={$_REQUEST["ELEMENT_ID"]}.';
$MESS['SECTION_CODE_TIP'] = 'Nurodomas skyriaus kodas, galite nenurodyti jeigu nurodytas <i>Skyriaus ID</i>';
$MESS['ELEMENT_CODE_TIP'] = 'Nurodomas elemento kodas, galite nenurodyti jeigu nurodytas <i>Elemento ID</i>';
$MESS['ELEMENT_SORT_FIELD_TIP'] = 'Nurodomas laukas pagal kur� bus sur��iuotos nuotraukos. Naudojama navigacijoje "pirmyn" ';
?>