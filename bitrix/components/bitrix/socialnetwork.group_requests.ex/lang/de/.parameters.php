<?
$MESS["SONET_SET_NAVCHAIN"] = "Breadcrumb-Navigation festlegen";
$MESS["SONET_ITEMS_COUNT"] = "Anzahl der Eintr�ge in der Liste";
$MESS["SONET_PATH_TO_USER"] = "Pfadvorlage zum Nutzerprofil";
$MESS["SONET_PATH_TO_GROUP"] = "Pfadvorlage zur Gruppenseite";
$MESS["SONET_PAGE_VAR"] = "Variable f�r die Seite";
$MESS["SONET_GROUP_VAR"] = "Variable f�r die Gruppe";
$MESS["SONET_USER_VAR"] = "Variable f�r den Nutzer";
$MESS["SONET_GROUP_ID"] = "Gruppen-ID";
$MESS["SONET_VARIABLE_ALIASES"] = "Variablennamen";
$MESS["SONET_NAME_TEMPLATE"] = "Name anzeigen";
$MESS["SONET_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["SONET_SHOW_LOGIN"] = "Login anzeigen, wenn kein Name vorhanden";
$MESS["SONET_PATH_TO_GROUP_EDIT"] = "Vorlage des Pfads zur Seite der Arbeitsgruppeneinstellungen";
$MESS["SONET_THUMBNAIL_LIST_SIZE"] = "Gr��e des Nutzerbildes (Px)";
?>