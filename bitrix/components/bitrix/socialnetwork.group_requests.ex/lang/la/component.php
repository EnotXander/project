<?
$MESS["SONET_MODULE_NOT_INSTALL"] = "El m�dulo Social Network no est� instalado.";
$MESS["SONET_P_USER_NO_USER"] = "No se encontr� el usuario.";
$MESS["SONET_GRE_NO_GROUP"] = "No se encontr� el grupo de trabajo.";
$MESS["SONET_GRE_NO_PERMS"] = "No tienes permiso para ver este grupo.";
$MESS["SONET_GRE_TITLE"] = "Solicitudes de invitaci�n a miembros del grupos de trabajo";
$MESS["SONET_GRE_CANT_INVITE"] = "Usted no tiene permiso para aprobar las solicitudes de membres�a de este grupo.";
$MESS["SONET_GRE_NOT_SELECTED"] = "No hay usuarios seleccionados.";
$MESS["SONET_GRE_NAV"] = "usuarios";
$MESS["SONET_GRE_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["SONET_GRE_CANT_DELETE_INVITATION"] = "No se puede eliminar la invitaci�n al usuario ID=#RELATION_ID#";
?>