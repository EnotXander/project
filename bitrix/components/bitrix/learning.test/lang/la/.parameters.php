<?
$MESS["T_LEARNING_DETAIL_ID"] = "Probar la identificaci�n ";
$MESS["LEARNING_DESC_YES"] = "S� ";
$MESS["LEARNING_DESC_NO"] = "No";
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Comprobar los permisos permitidos ";
$MESS["LEARNING_GRADEBOOK_TEMPLATE_NAME"] = "URL de la p�gina de los resultados de la prueba ";
$MESS["LEARNING_PAGE_WINDOW_NAME"] = "N�mero de preguntas en la navegaci�n ";
$MESS["LEARNING_SHOW_TIME_LIMIT"] = "Demostrar el contador del tiempo";
$MESS["T_LEARNING_PAGE_NUMBER_VARIABLE"] = "Identificaci�n de la pregunta ";
$MESS["LEARNING_COURSE_ID"] = "Identificaci�n del curso";
?>