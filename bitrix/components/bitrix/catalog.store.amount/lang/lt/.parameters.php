<?
$MESS["CP_CSA_GROUP_STORE"] = "Sand�liai";
$MESS["CP_CSA_PARAM_ELEMENT_ID"] = "Prek�";
$MESS["CP_CSA_PARAM_STORE_PATH"] = "URL, rodantis sand�lio innformacij�";
$MESS["CP_CSA_PARAM_USE_STORE_PHONE"] = "Rodyti sannd�lio telefono numer�";
$MESS["CP_CSA_PARAM_SCHEDULE"] = "Rodyti sannd�lio darbo valandas";
$MESS["CP_CSA_PARAM_USE_MIN_AMOUNT"] = "Rodyti pasiekiamumo status�, o ne tiksl� kiek�";
$MESS["CP_CSA_PARAM_MIN_AMOUNT"] = "Minimalus kiekis teigiamam prieinamumo statusui";
$MESS["CP_CSA_PARAM_MAIN_TITLE"] = "Pavadinimas";
$MESS["ELEMENT_ID_TIP"] = "Produkto, kurio atsargos turi b�ti rodomos, ID";
$MESS["STORE_PATH_TIP"] = "Nurodo puslapio, rodanti sand�lio informacij�, URL";
$MESS["USE_STORE_PHONE_TIP"] = "Nurodo rodyti sand�lio telefono numer�";
$MESS["SCHEDULE_TIP"] = "Nurodo rodyti sand�lio darbo valandas";
$MESS["USE_MIN_AMOUNT_TIP"] = "Jei pa�ym�ta, ataskaita tik nurodys, ar produktas turi pakankamai atsarg� tam tikrame sand�lyje, o ne tikslius skai�ius";
$MESS["MIN_AMOUNT_TIP"] = "Produktas turi pakankamai atsarg�, jei kiekis yra didesnis u� nurodyt� reik�m�, kitaip nepakankamai";
$MESS["MAIN_TITLE_TIP"] = "Nurodo sand�lio atsarg� ataskaitos pavadinim�";
?>