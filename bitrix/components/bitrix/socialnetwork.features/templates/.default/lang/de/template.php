<?
$MESS["SONET_C4_FUNC_TITLE"] = "Die Funktion '#NAME#' ist aktiviert";
$MESS["SONET_C4_FUNC_TITLE_ON"] = "Die Funktion '#NAME#' ist aktiviert";
$MESS["SONET_C4_FUNC_TITLE_OFF"] = "Die Funktion '#NAME#' ist deaktiviert";
$MESS["SONET_C4_SUBMIT"] = "Speichern";
$MESS["SONET_C4_GR_SUCCESS"] = "Die Gruppen-Parameter wurden erfolgreich ge�ndert.";
$MESS["SONET_C4_US_SUCCESS"] = "Die Nutzerparameter wurden erfolgreich ge�ndert.";
$MESS["SONET_C4_T_CANCEL"] = "Abbrechen";
$MESS["SONET_FEATURES_NAME"] = "�berschrift";
$MESS["SONET_C4_NO_FEATURES"] = "Die Funktionalit�t ist deaktiviert. Sie k�nnen diese Funktionalit�t auf der Seite der Gruppenparameterbearbeitung aktivieren.";
?>