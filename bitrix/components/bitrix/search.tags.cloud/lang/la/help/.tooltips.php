<?
$MESS["SEARCH_TIP"] = "Buscar";
$MESS["TAGS_TIP"] = "Etiqueta";
$MESS["URL_SEARCH_TIP"] = "Ruta a la p�gina de b�squedas (relativa a la raiz del sitio).";
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: el cach� es v�lido durante el tiempo predefinido in las configuraciones del cach�; <br /><i>Cache</i>:cach� para el periodo especificad en el sigueinte campo;<br /><i>Sin cach�</i>: no se realiza almacenamiento en el cach�.";
$MESS["CACHE_TIME_TIP"] = "Especifique el periodo de tiempo mediate el cual el cach� es v�lido.";
$MESS["SORT_TIP"] = "Seleccione ac� el modo de clasificar las etiquetas: por nombre o por vistas.";
$MESS["PAGE_ELEMENTS_TIP"] = "Especifique el n�mero de etiquetas en la nube.";
$MESS["TAGS_INHERIT_TIP"] = "Escpecifique la estrech�s de la b�squeda.";
$MESS["CHECK_DATES_TIP"] = "Especifique si la busqueda se realizar� s�lo en elementos activos.";
$MESS["arrFILTER_TIP"] = "Permit� especificar donde se realzar�n las b�squedas. Por ejemplo, usted puede especificar s�lo archivos est�ticos.";
$MESS["PERIOD_TIP"] = "Especifique el n�mero de d�as recientes para cada etiqueta seleccionada.";
$MESS["arrFILTER_socialnetwork_user_TIP"] = "Mostrar ID del usuario que deber�a estar incluido en los resultados de b�squeda.";
?>