<?
$MESS['SEARCH_FORUM'] = 'Forumai';
$MESS['SEARCH_ALL'] = '(visk�)';
$MESS['SEARCH_NO_LIMIT'] = 'neriboti';
$MESS['SEARCH_STATIC'] = 'Statiniai failai';
$MESS['SEARCH_IBLOCK_TYPE1'] = 'Ie�koti informaciniuose blokuose kuri� tipas "';
$MESS['SEARCH_IBLOCK_TYPE2'] = '"';
$MESS['SEARCH_WHERE_FILTER'] = 'Paie�kos srities apribojimas';
$MESS['SEARCH_CHECK_DATES'] = 'Ie�koti tik galiojan�iuose dokumentuose';
$MESS['SEARCH_BLOG'] = 'Tinklara��iai';
$MESS['SEARCH_SOCIALNETWORK'] = 'Socialinis tinklas';
$MESS['SEARCH_INTRANET_USERS'] = 'Naudotojai';
$MESS['SEARCH_SORT'] = '�ymi� r��iavimas';
$MESS['SEARCH_NAME'] = 'Pagal pavadinim�';
$MESS['SEARCH_CNT'] = 'Pagal reiting�';
$MESS['SEARCH_PAGE_ELEMENTS'] = '�ymi� kiekis';
$MESS['SEARCH_PERIOD'] = 'Rodyti �ymes (dien�)';
$MESS['SEARCH_URL_SEARCH'] = 'Kelias � paie�kos puslap� (�akninio aplanko at�vilgiu)';
$MESS['SEARCH_URL'] = 'URL prasideda nuo betkurio i� i�vadito kelio.';
$MESS['SEARCH_TAGS_INHERIT'] = 'Susiaurinti paie�k�';
?>