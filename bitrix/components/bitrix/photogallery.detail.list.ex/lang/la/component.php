<?
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Los m�dulos del block de informaci�n no est�n instalados.";
$MESS["P_PHOTOS"] = "Fotos";
$MESS["P_LIST_PHOTO"] = "Fotos";
$MESS["P_MODULE_IS_NOT_INSTALLED"] = "El m�dulo de la galer�a de fotos no est� instalado.";
$MESS["IBLOCK_WRONG_SESSION"] = "Su sesi�n ha expirado. Por favor guarde la base de datos de nuevo.";
$MESS["SAVE_DESC_ERROR"] = "Ocurri� un error cuando guardaba la descripci�n.";
$MESS["DEL_ITEM_ERROR"] = "Ocurri� un error al eliminar la foto.";
$MESS["ROTATE_ERROR"] = "Ocurri� un error al guardar la foto.";
$MESS["ALBUM_NOT_FOUND_ERROR"] = "Usted no tiene permiso para ver este �lbum.";
$MESS["PHOTOS_NOT_FOUND_ERROR"] = "Ninguna foto ha sido encontrada.";
$MESS["UNKNOWN_AUTHOR"] = "<Sin t�tulo>";
?>