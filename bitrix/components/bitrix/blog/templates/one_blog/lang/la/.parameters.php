<?
$MESS["USER_PROPERTY_NAME"] = "Nombre de las propiedades adicionales de la etiqueta";
$MESS["ONE_BLOG_BLOG_URL"] = "Nombre simb�lico del blog";
$MESS["BLG_THEME"] = "Plantilla del tema";
$MESS["BLG_THEME_BLUE"] = "Azul";
$MESS["BLG_THEME_GREEN"] = "Verde";
$MESS["BLG_THEME_RED"] = "Rojo";
$MESS["BLG_THEME_RED2"] = "Rojo simple";
$MESS["BLG_THEME_ORANGE"] = "Anaranjado";
$MESS["B_SHOW_NAVIGATION"] = "Mostrar ruta de navegaci�n para los blogs";
$MESS["B_SEO_USER"] = "Prevenir la b�squeda de ara�as y bots que enlacen al perfil de usuario";
$MESS["BC_NAME_TEMPLATE"] = "Formato de nombre";
$MESS["BC_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["BC_SHOW_LOGIN"] = "Mostrar nombre de usuario si no son necesarios, campos de nombre de usuario est�n disponibles";
$MESS["BC_PATH_TO_CONPANY_DEPARTMENT"] = "Plantilla de ruta a la p�gina del departamento";
$MESS["BC_PATH_TO_VIDEO_CALL"] = "P�gina de video conferencia";
$MESS["BC_PATH_TO_SONET_USER_PROFILE"] = "Plantilla de ruta al perfil del usuario de Social Network ";
$MESS["BC_PATH_TO_MESSAGES_CHAT"] = "Ruta de plantilla del mensaje del usuario";
$MESS["BC_DO_NOT_SHOW_SIDEBAR"] = "Ocultar la columna derecha del blog";
$MESS["BC_DO_NOT_SHOW_MENU"] = "Ocultar el men� superior del blog";
$MESS["BC_USE_SHARE"] = "Mostrar barra de favoritos de Show Social";
$MESS["BC_SHARE_HIDE"] = "Ocultar barra de favoritos de Social Network por defecto";
$MESS["BC_SHARE_TEMPLATE"] = "Plantilla de favoritos de Social Network";
$MESS["BC_SHARE_SYSTEM"] = "Utilizar Social Network y barra de favoritos";
$MESS["BC_SHARE_SHORTEN_URL_LOGIN"] = "Inicio de sesi�n bit.ly";
$MESS["BC_SHARE_SHORTEN_URL_KEY"] = "Clave bit.ly";
?>