<?
$MESS['USER_PROPERTY_NAME'] = 'Papildom� savybi� kortel�s pavadinimas';
$MESS['SEARCH_FONT_MAX'] = 'Did�iausias �ymi� debesio �rifto dydis (px)';
$MESS['SEARCH_FONT_MIN'] = 'Ma�iausias �ymi� debesio �rifto dydis (px)';
$MESS['SEARCH_COLOR_OLD'] = 'Naujausios �ymi� debesio �ym�s spalva (pvz. \"FEFEFE\")';
$MESS['SEARCH_COLOR_NEW'] = 'Seniausios �ymi� debesio �ym�s spalva (pvz. \"FEFEFE\")';
$MESS['SEARCH_PERIOD_NEW_TAGS'] = '�ym� laikyti nauja (dien�) ';
$MESS['SEARCH_COLOR_TYPE'] = 'Naudoti spalv� per�jimus';
$MESS['SEARCH_WIDTH'] = '�ymi� debesies plotis (pvz. \"100%\", \"100px\", \"100pt\" arba \"100in\") ';
$MESS['SEARCH_PERIOD'] = 'Rodyti �ymes (dien�)';
$MESS['GENERAL_PAGE_GROUP_ID'] = 'Tinklara��i� grup�s per�i�rai';
$MESS['BLG_THEME'] = '�ablono tema';
$MESS['BLG_THEME_BLUE'] = 'M�lyna';
$MESS['BLG_THEME_GREEN'] = '�alia';
$MESS['BLG_THEME_RED'] = 'Raudona';
$MESS['BLG_THEME_RED2'] = 'Paprasta raudona';
$MESS['BLG_THEME_ORANGE'] = 'Oran�in�';
$MESS['B_SHOW_NAVIGATION'] = 'Rodyti mar�ruto kelio navigacij� dienora��iams';
?>