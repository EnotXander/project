<?
$MESS["LEARNING_PASSAGE_FOLLOW_EDIT"] = "Der �bergang zur n�chsten Frage ohne eine Antwort auf die aktuelle Frage ist erlaubt. Man <b>darf</b> seine Antworten �ndern.";
$MESS["LEARNING_PASSAGE_FOLLOW_NO_EDIT"] = "Der �bergang zur n�chsten Frage ohne eine Antwort auf die aktuelle Frage ist erlaubt. Man darf seine Antworten <b>nicht</b> �ndern.";
$MESS["LEARNING_TEST_ATTEMPT_LIMIT"] = "Anzahl der Versuche";
$MESS["LEARNING_BTN_CONTINUE"] = "Weiter";
$MESS["LEARNING_TEST_TIME_LIMIT_MIN"] = "Min.";
$MESS["LEARNING_TEST_TIME_LIMIT_UNLIMITED"] = "ohne Begrenzung";
$MESS["LEARNING_PASSAGE_NO_FOLLOW_NO_EDIT"] = "�bergang zur n�chsten Frage ohne eine Antwort auf die aktuelle Frage verbieten. Der User kann seine Antworten <b>nicht</b> �ndern.";
$MESS["LEARNING_BTN_START"] = "Starten";
$MESS["LEARNING_TEST_NAME"] = "Test�berschrift";
$MESS["LEARNING_TEST_TIME_LIMIT"] = "Zeitbegrenzung";
$MESS["LEARNING_PASSAGE_TYPE"] = "Typ des Testdurchlaufs";
$MESS["LEARNING_TEST_ATTEMPT_UNLIMITED"] = "unbegrenzte Anzahl";
$MESS["LEARNING_PREV_TEST_REQUIRED"] = "Um auf diesen Test zugreifen zu k�nnen, m�ssen Sie den Test #TEST_LINK# machen und dabei mindestens  #TEST_SCORE#% von der maximal m�glichen Punkteanzahl  erreichen.";
?>