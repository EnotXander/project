<?
$MESS["BIZPROC_WFEDIT_ERROR_TYPE"] = "Dokumento tipas yra privelomas.";
$MESS["BIZPROC_WFEDIT_DEFAULT_TITLE"] = "Verslo proceso �ablonas";
$MESS["BIZPROC_WFEDIT_SAVE_ERROR"] = "�vyko klaida i�saugant objekt�:";
$MESS["BIZPROC_WFEDIT_CATEGORY_MAIN"] = "Pagrindinis";
$MESS["BIZPROC_WFEDIT_CATEGORY_DOC"] = "Dokument� apdorojimas";
$MESS["BIZPROC_WFEDIT_CATEGORY_CONSTR"] = "Konstravimas";
$MESS["BIZPROC_WFEDIT_CATEGORY_INTER"] = "Interaktyv�s nustatymai";
$MESS["BIZPROC_WFEDIT_CATEGORY_OTHER"] = "Kita";
$MESS["BIZPROC_WFEDIT_TITLE_ADD"] = "Naujo verslo proceso �ablonas";
$MESS["BIZPROC_WFEDIT_TITLE_EDIT"] = "Redaguoti  verslo proceso �ablon�";
$MESS["BIZPROC_WFEDIT_IMPORT_ERROR"] = "Klaida importuojant verslo proceso �ablon�.";
$MESS["BIZPROC_USER_PARAMS_SAVE_ERROR"] = "Vienas ar daugiau veiksm� \"Mano veiksmai\" juostoje yra per didelis. Pakeitimai nebus i�saugoti.";
?>