<?
$MESS["BIZPROC_WFEDIT_MENU_PARAMS_TITLE"] = "�ablono parametrai; kintamieji; auto paleidimas";
$MESS["BIZPROC_WFEDIT_MENU_PARAMS"] = "�ablono parametrai";
$MESS["BIZPROC_WFEDIT_MENU_LIST"] = "�ablonai";
$MESS["BIZPROC_WFEDIT_MENU_LIST_TITLE"] = "Per�i�r�ti �ablonus";
$MESS["BIZPROC_WFEDIT_MENU_ADD_STATE"] = "Valstyb�s valdomas verslo procesas";
$MESS["BIZPROC_WFEDIT_MENU_ADD_STATE_TITLE"] = "Valstyb�s valdomas verslo procesas yra t�stinis verslo procesas su prieigos teisi� skirstymu, skirtu tvarkyti �vairi� status� dokumentus";
$MESS["BIZPROC_WFEDIT_MENU_ADD_SEQ"] = "Nuoseklus verslo procesas";
$MESS["BIZPROC_WFEDIT_MENU_ADD_SEQ_TITLE"] = "Nuoseklus verslo procesas yra paprastas verslo procesas, skirtas vykdyti nuosekli� veiksm� su dokumentais serij�";
$MESS["BIZPROC_WFEDIT_MENU_ADD_WARN"] = "Visi pakeitimai bus prarasti, jeigu j�s nei�saugojote j�. T�sti?";
$MESS["BIZPROC_WFEDIT_MENU_ADD"] = "Sukurti �ablon�";
$MESS["BIZPROC_WFEDIT_MENU_ADD_TITLE"] = "Prid�ti nauj� �ablon�";
$MESS["BIZPROC_WFEDIT_SAVE_BUTTON"] = "I�saugoti";
$MESS["BIZPROC_WFEDIT_APPLY_BUTTON"] = "Pritaikyti";
$MESS["BIZPROC_WFEDIT_CANCEL_BUTTON"] = "At�aukti";
$MESS["BIZPROC_WFEDIT_MENU_EXPORT"] = "Eksportuoti";
$MESS["BIZPROC_WFEDIT_MENU_EXPORT_TITLE"] = "Eksportuoti verslo proceso �ablon�";
$MESS["BIZPROC_WFEDIT_MENU_IMPORT"] = "Importuoti";
$MESS["BIZPROC_WFEDIT_MENU_IMPORT_TITLE"] = "Importuoti verslo proceso �ablon�";
$MESS["BIZPROC_IMPORT_BUTTON"] = "Importuoti";
$MESS["BIZPROC_IMPORT_TITLE"] = "Importuoti �ablon�";
$MESS["BIZPROC_IMPORT_FILE"] = "Failas";
$MESS["BIZPROC_WFEDIT_MENU_IMPORT_PROMT"] = "Dabartinis verslo proces� �ablonas bus pakeistas importuojamu. T�sti?";
?>