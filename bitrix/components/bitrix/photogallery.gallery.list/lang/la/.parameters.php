<?
$MESS["IBLOCK_TYPE"] = "Tipo de block de informaci�n";
$MESS["IBLOCK_IBLOCK"] = "Block de informaci�n";
$MESS["P_USER_ID"] = "ID del usuario";
$MESS["P_GALLERY_EDIT_URL"] = "Galer�a de fotos (edici�n)";
$MESS["P_GALLERY_URL"] = "Galer�a de fotos";
$MESS["P_INDEX_URL"] = "�ndice de la p�gina";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Muestra panel de botones para este componente";
$MESS["P_UPLOAD_URL"] = "Cargar fotos";
$MESS["IBLOCK_SECTION_PAGE_ELEMENT_COUNT"] = "N�mero de elementos por p�gina";
$MESS["IBLOCK_PAGE_NAVIGATION_TEMPLATE"] = "Plantila de la p�gina de navegaci�n";
$MESS["T_DATE_TIME_FORMAT"] = "Formato de fecha";
$MESS["IBLOCK_SORT_SORT"] = "�ndice de clasificaci�n";
$MESS["IBLOCK_SORT_NAME"] = "nombre";
$MESS["IBLOCK_SORT_DATE"] = "fecha";
$MESS["IBLOCK_SORT_ASC"] = "ascendente";
$MESS["IBLOCK_SORT_DESC"] = "descendente";
$MESS["IBLOCK_SECTION_SORT_FIELD"] = "Campo para clasificar secciones";
$MESS["IBLOCK_SECTION_SORT_ORDER"] = "Orden de clasificaci�n de secciones";
$MESS["P_GALLERY_GROUPS"] = "Grupos de usuarios cuyos mienbros pueden crear galer�as";
$MESS["P_GALLERY_SIZE"] = "Tama�o de la galer�a (MB)";
$MESS["P_ONLY_ONE_GALLERY"] = "Usuarios registrados pueden crear s�lo una galer�a";
$MESS["P_GALLERY_AVATAR_SIZE"] = "Tama�o de la galer�a de avatar (px)";
$MESS["P_SET_STATUS_404"] = "Establece el estado 404 si el elemento o la secci�n no se encuentra";
?>