<?
$MESS['T_IBLOCK_DESC_NEWS_DATE'] = 'Atvaizduoti elemento dat�';
$MESS['T_IBLOCK_DESC_NEWS_PICTURE'] = 'Atvaizduoti elemento anonso paveiksl�';
$MESS['T_IBLOCK_DESC_NEWS_TEXT'] = 'Atvaizduoti teksto anons�';
$MESS['SEARCH_PAGE_ELEMENTS'] = '�ymi� kiekis';
$MESS['SEARCH_FONT_MAX'] = 'Did�iausias �rifto dydis (px)';
$MESS['SEARCH_FONT_MIN'] = 'Ma�iausias �rifto dydis (px)';
$MESS['SEARCH_COLOR_OLD'] = 'Naujausios �ym�s spalva (pvz. "FEFEFE")';
$MESS['SEARCH_COLOR_NEW'] = 'Seniausios �ym�s spalva (pvz. "C0C0C0")';
$MESS['SEARCH_PERIOD_NEW_TAGS'] = '�ym� laikyti nauja (dien�)';
$MESS['SEARCH_WIDTH'] = '�ymi� debesies plotis (pvz. "100%", "100px", "100pt" arba "100in") ';
$MESS['TP_CBIV_DISPLAY_AS_RATING'] = 'Atvaizduoti reitinge';
$MESS['TP_CBIV_AVERAGE'] = 'Vidutin� reik�m�';
$MESS['TP_CBIV_RATING'] = 'Reitingas';
?>