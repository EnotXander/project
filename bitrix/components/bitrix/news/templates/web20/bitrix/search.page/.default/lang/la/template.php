<?
$MESS["SEARCH_ALL"] = "(Todos)";
$MESS["SEARCH_GO"] = "Ir";
$MESS["SEARCH_ERROR"] = "Error al bscar frase:";
$MESS["SEARCH_CORRECT_AND_CONTINUE"] = "Por favor busque la frase e intente de nuevo.";
$MESS["SEARCH_LOGIC"] = "Operadores l�gicos:";
$MESS["SEARCH_OPERATOR"] = "Operadores";
$MESS["SEARCH_SYNONIM"] = "Sin�nimos";
$MESS["SEARCH_DESCRIPTION"] = "Descripci�n";
$MESS["SEARCH_AND"] = "y";
$MESS["SEARCH_OR"] = "�";
$MESS["SEARCH_OR_ALT"] = "Operador <i>logical or</i>  permite b�squeda para Entidades conteniendo al menos uno de los operadores.";
$MESS["SEARCH_NOT"] = "ninguno";
$MESS["SEARCH_NOT_ALT"] = "Operador <i>logical not</i> restringe b�squeda para las p�ginas que no contienen el operando.";
$MESS["SEARCH_BRACKETS_ALT"] = "<i>Rodeado de par�ntesis</i> definir la procedencia del operador l�gico. ";
$MESS["SEARCH_MODIFIED"] = "Modificado:";
$MESS["SEARCH_NOTHING_TO_FOUND"] = "Desafortunadamente, su b�squeda no coincidi� con ning�n documento.";
$MESS["SEARCH_PATH"] = "Ruta:";
$MESS["SEARCH_SORT_BY_RANK"] = "Clasificar por relevancia";
$MESS["SEARCH_SORTED_BY_DATE"] = "Clasificado por fecha";
$MESS["SEARCH_SORTED_BY_RANK"] = "Clasificado por relevancia";
$MESS["SEARCH_SORT_BY_DATE"] = "Clasificar por fecha";
$MESS["SEARCH_SINTAX"] = "<p><b>Buscar la sint&aacute;xis de la consulta :</b><br />
  <br />
Una b&uacute;squeda de consulta com&uacute;n es de una &oacute; m&aacute;s palabras por ejemplo:<br />
<i>informaci&oacute;n de contacto: </i><br />
Esta consulta podr&aacute; encontrar p&aacute;ginas que contengan ambas palabras consultadas</p>
<p>  Operadores l&oacute;gicos permiten construir consultas m&aacute;s complejas, por ejemplo: <br />
<i>informaci&oacute;n de contacto &oacute; tel&eacute;fono<br />
</i>Esta consulta podr&aacute; hallar p&aacute;ginas que no contengan tampoco palabras &quot;contacto&quot; and &quot;informaci&oacute;n&quot;&oacute; &quot;tel&eacute;fono&quot;.<br />
  <br /> 
  <i>informaci&oacute;n de contacto information no tel&eacute;fono</i><br />
  Esta consulta podr&aacute; hallar p&aacute;ginas que contengan las palabras &quot;contacto&quot; e &quot;informaci&oacute;n&quot; pero no &quot;tel&eacute;fono&quot;.<br />
  Usted puede usar corchetes para construir consultas m&aacute;s complejas.<br />
</p>";
$MESS["SEARCH_AND_ALT"] = "Operador <i>l�gico est� implicado y debe ser omitido: una consulta &quot;contact information&quot;es completamente equivalente a &quot;contact y information&quot;.";
$MESS["CT_BSP_KEYBOARD_WARNING"] = "Consulta de entrada de idioma cambiado por \\\"#query#\\\".";
?>