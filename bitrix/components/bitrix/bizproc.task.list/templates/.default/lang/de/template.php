<?
$MESS["BPATL_CURRENT_TASKS"] = "Aktuelle Aufgaben";
$MESS["BPATL_DESCRIPTION"] = "Beschreibung";
$MESS["BPATL_DOCUMENT"] = "Dokument";
$MESS["BPATL_MODIFIED"] = "Ge�ndert";
$MESS["BPATL_NAME"] = "Name";
$MESS["BPATL_FINISHED_TASKS"] = "Aufgabenarchiv";
$MESS["BPATL_EMPTY"] = "Die Aufgabenliste ist leer.";
$MESS["BPATL_DOCUMENT_TITLE"] = "Dokument ansehen";
$MESS["BPWC_WLCT_TOTAL"] = "Gesamt";
$MESS["BPATL_GROUP_ACTION_YES"] = "Best�tigen";
$MESS["BPATL_GROUP_ACTION_NO"] = "Ablehnen";
$MESS["BPATL_GROUP_ACTION_OK"] = "Ausf�hren";
$MESS["BPATL_GROUP_ACTION_DELEGATE"] = "Delegieren";
?>