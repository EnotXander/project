<?
$MESS["BPABS_TITLE"] = "Paskyrimas";
$MESS["BPATL_NAV"] = "Paskyrimas";
$MESS["BPTL_C_DETAIL"] = "Informacija";
$MESS["BPTL_C_DOCUMENT"] = "Dokumentas (naujame lange)";
$MESS["BPWC_WLC_ERROR"] = "Klaida";
$MESS["BPATL_NAME"] = "Pavadinimas";
$MESS["BPATL_DESCRIPTION"] = "Apra�ymas";
$MESS["BPATL_MODIFIED"] = "Pakeista";
$MESS["BPATL_OVERDUE_DATE"] = "Termino data";
$MESS["BPATL_WORKFLOW_NAME"] = "Verslo proceso pavadinimas";
$MESS["BPATL_WORKFLOW_STATE"] = "Verslo proceso statusas";
$MESS["INTS_TASKS_NAV"] = "U�duotys";
$MESS["BPATL_STARTED"] = "Aktyvi forma";
$MESS["BPATL_STARTED_BY"] = "Prad�jo";
$MESS["BPATL_WORKFLOW_ID"] = "Verslo procesas";
$MESS["BPATL_WORKFLOW_ID_ANY"] = "(bet koks)";
?>