<?
$MESS["SONET_C30_T_USER"] = "Nutzer";
$MESS["SONET_C30_T_MESSAGE_HIDE"] = "Ausblenden";
$MESS["SONET_C30_T_MESSAGE_SHOW"] = "Anzeigen";
$MESS["SONET_C30_T_MESSAGE_EXPAND"] = "Maximieren";
$MESS["SONET_C30_NO_SUBSCRIPTIONS"] = "Keine Abonnements verf�gbar";
$MESS["SONET_C30_INHERITED"] = "Standardm��ig";
$MESS["SONET_C30_DIALOG_CLOSE_BUTTON"] = "Schlie�en";
$MESS["SONET_C30_DIALOG_SUBMIT_BUTTON"] = "Speichern";
$MESS["SONET_C30_DIALOG_CANCEL_BUTTON"] = "L�schen";
$MESS["SONET_C30_DIALOG_TRANSPORT_TITLE"] = "Benachrichtigung:";
$MESS["SONET_C30_COMMENT_SUBMIT"] = "Senden";
$MESS["SONET_C30_MORE"] = "Mehr Events";
$MESS["SONET_C30_T_MORE_WAIT"] = "Nachrichten werden geladen&hellip;";
$MESS["SONET_C30_T_RELOAD_NEEDED"] = "Den Activity Stream neu laden";
$MESS["SONET_C30_COUNTER_TEXT_1"] = "Neue Beitr�ge: ";
$MESS["SONET_C30_MENU_TITLE_TRANSPORT"] = "Benachrichtigungen";
$MESS["SONET_C30_MENU_TITLE_FAVORITES_N"] = "Lesezeichen";
$MESS["SONET_C30_MENU_TITLE_FAVORITES_Y"] = "Aus Lesezeichen entfernen";
$MESS["SONET_C30_MENU_TITLE_LINK"] = "Link zur Nachricht";
$MESS["SONET_ERROR_SESSION"] = "Ihre Sitzung ist abgelaufen. Bitte senden Sie den Kommentar erneut ab.";
$MESS["SONET_C30_MENU_TITLE_HREF"] = "Nachricht �ffnen";
$MESS["SONET_C30_MENU_TITLE_DELETE"] = "Aus dem Activity Stream l�schen";
$MESS["SONET_C30_MENU_TITLE_DELETE_CONFIRM"] = "M�chten Sie diesen Beitrag und alle dazu geh�rigen Kommentare wirklich l�schen?";
$MESS["SONET_C30_MENU_TITLE_DELETE_SUCCESS"] = "Der Beitrag wurde gel�scht.";
$MESS["SONET_C30_MENU_TITLE_DELETE_FAILURE"] = "Fehler beim L�schen des Beitrags.";
$MESS["SONET_C30_T_RELOAD_ERROR"] = "Aktualisierung des Activity Streams ist fehlgeschlagen";
?>