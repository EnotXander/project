<?
$MESS["PATH_TO_USER_TIP"] = "La ruta de acceso a una p�gina de perfil de usuario. Ejemplo: sonet_user.php?page=user&user_id=#user_id#.";
$MESS["PATH_TO_USER_EDIT_TIP"] = "La ruta hacia una p�gina del editor de perfil de usuario. Ejemplo: sonet_user_edit.php?page=user&user_id=#user_id#&mode=edit.";
$MESS["PAGE_VAR_TIP"] = "Especifique aqu� el nombre de una variable a la p�gina de usuario de Social Network.";
$MESS["USER_VAR_TIP"] = "Especifique aqu� el nombre de una variable en la que la red social ID de usuario ser� aprobado.";
$MESS["ID_TIP"] = "Especifica el c�digo que se eval�a como un ID de usuario.";
$MESS["SET_TITLE_TIP"] = "Esta opci�n establece el t�tulo de la p�gina para <i>\"user name\"</i> <b>User Profile</b>.";
$MESS["USER_PROPERTY_TIP"] = "Seleccione aqu� propiedades adicionales que se mostrar�n en el perfil de usuario.";
?>