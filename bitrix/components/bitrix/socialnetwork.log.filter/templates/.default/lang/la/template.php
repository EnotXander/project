<?
$MESS["SONET_C30_T_FILTER_TITLE"] = "Buscar";
$MESS["SONET_C30_T_SUBMIT"] = "Seleccionar";
$MESS["SONET_C30_T_FILTER_CREATED_BY"] = "Autor";
$MESS["SONET_C30_T_FILTER_GROUP"] = "Grupo";
$MESS["SONET_C30_T_FILTER_DATE"] = "Fecha ";
$MESS["SONET_C30_T_SHOW_HIDDEN"] = "Mostrar categor�as ocultas";
$MESS["SONET_C30_T_FILTER_COMMENTS"] = "B�scar en comentarios tambi�n.";
$MESS["SONET_C30_T_RESET"] = "Resetear";
$MESS["SONET_C30_PRESET_FILTER_ALL"] = "Todos los eventos";
$MESS["SONET_C30_SMART_FOLLOW"] = "Activar el modo de seguimiento inteligente";
$MESS["SONET_C30_SMART_FOLLOW_HINT"] = "Modo de seguimiento inteligente est� activado. S�lo los mensajes de los que es autor, destinatario o en los cuales se le menciona en el texto ser�n trasladados a la parte superior. Usted autom�ticamente sigue cualquier mensaje sobre el que comente.";
$MESS["SONET_C30_SMART_FILTER_MYGROUPS"] = "S�lo mis grupos";
$MESS["SONET_C30_T_FILTER_USER"] = "Receptor";
?>