<?
$MESS["FID_TIP"] = "ID del foro";
$MESS["TID_TIP"] = "ID del tema";
$MESS["URL_TEMPLATES_INDEX_TIP"] = "P�gina de foros";
$MESS["URL_TEMPLATES_LIST_TIP"] = "P�gina de temas";
$MESS["URL_TEMPLATES_READ_TIP"] = "P�gina de vista de temas";
$MESS["URL_TEMPLATES_MESSAGE_TIP"] = "P�gina de vista de mensajes (con ID del mensaje)";
$MESS["URL_TEMPLATES_MESSAGE_APPR_TIP"] = "P�gina del mensajes ocultos";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "Plantilla del URL  del perfil de usuario";
$MESS["URL_TEMPLATES_PM_EDIT_TIP"] = "P�gina de mensajes personales";
$MESS["URL_TEMPLATES_MESSAGE_SEND_TIP"] = "P�gina de creaci�n de mensajes";
$MESS["URL_TEMPLATES_TOPIC_NEW_TIP"] = "P�gina de creaci�n de nuevos temas";
$MESS["MESSAGES_PER_PAGE_TIP"] = "Post por p�gina";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "Nombre de la plantilla del Breadcrumb";
$MESS["PAGE_NAVIGATION_WINDOW_TIP"] = "P�ginas en Breadcrumbs";
$MESS["PATH_TO_SMILE_TIP"] = "Ruta a la carpeta de los emoticons, relativo a la ra�z del sitio";
$MESS["WORD_LENGTH_TIP"] = "Longitud de la palabra";
$MESS["IMAGE_SIZE_TIP"] = "Dimensi�n de la imagen adjunta (px)";
$MESS["DATE_FORMAT_TIP"] = "Formato de fecha";
$MESS["DATE_TIME_FORMAT_TIP"] = "Formato de fecha y hora";
$MESS["SET_NAVIGATION_TIP"] = "Mostrar controles de navegaci�n";
$MESS["DISPLAY_PANEL_TIP"] = "El panel de control muestra los botones para este componente";
$MESS["SET_TITLE_TIP"] = "Establecer t�tulo de la p�gina";
$MESS["CACHE_TYPE_TIP"] = "Tipo de cach�";
$MESS["CACHE_TIME_TIP"] = "Duraci�n del cach� (seg.)";
?>