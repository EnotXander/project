<?
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "No est� instalado el m�dulo de \"Portal de Intranet\".";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "No est� instalado el m�dulo de \"Bloques de informaci�n\".";
$MESS["EC_IBLOCK_ACCESS_DENIED"] = "Acceso denegado";
$MESS["EC_USER_NOT_FOUND"] = "No se encontr� el usuario.";
$MESS["EC_GROUP_NOT_FOUND"] = "No se encontr� el grupo.";
$MESS["EC_USER_ID_NOT_FOUND"] = "No se puede mostrar el calendario de usuario porque no se ha especificado el ID del usuario.";
$MESS["EC_GROUP_ID_NOT_FOUND"] = "No se puede mostrar el calendario de grupo porque no se ha especificado el ID de grupo.";
?>