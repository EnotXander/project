<?
$MESS["SUP_LIST_TICKETS_PER_PAGE"] = "Tickets por p�gina";
$MESS["SUP_SET_PAGE_TITLE"] = "Aplicar t�tulo a la p�gina";
$MESS["SUP_EDIT_MESSAGES_PER_PAGE"] = "Mensajes por p�gina";
$MESS["SUP_DESC_YES"] = "S�";
$MESS["SUP_DESC_NO"] = "No";
$MESS["SUP_TICKET_ID_DESC"] = "ID del ticket";
$MESS["SUP_TICKET_LIST_DESC"] = "Lista de tickets";
$MESS["SUP_TICKET_EDIT_DESC"] = "P�gina de edici�n de tickets";
$MESS["SUP_SHOW_COUPON_FIELD"] = "Mostrar campo de ingreso de Cup�n de Atenci�n.";
$MESS["SUP_SORT_ASC"] = "ascendente";
$MESS["SUP_SORT_DESC"] = "descendente";
$MESS["SUP_SORT_FIELD_ID"] = "ID";
$MESS["SUP_SORT_FIELD_NUMBER"] = "N�mero de mensajes";
$MESS["SUP_MESSAGE_SORT_ORDER"] = "Ordenamiento para los tickets";
$MESS["SUP_MESSAGE_MAX_LENGTH"] = "M�ximo largo de la cadena sin saltos";
$MESS["SUP_MESSAGE_MAX_LENGTH_TIP"] = "El largo m�ximo de una cadena de texto sin saltos no contiene ni saltos ni puntuaciones; lineas largas ser�n cortadas.";
$MESS["SUP_SHOW_USER_FIELD"] = "Mostrar campos personalizados";
?>