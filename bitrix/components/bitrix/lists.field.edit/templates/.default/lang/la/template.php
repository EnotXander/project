<?
$MESS["CT_BLFE_TOOLBAR_FIELDS"] = "Lista de campos";
$MESS["CT_BLFE_TOOLBAR_FIELDS_TITLE"] = "Configurar lista de campos";
$MESS["CT_BLFE_TOOLBAR_DELETE"] = "Eliminar campos";
$MESS["CT_BLFE_TOOLBAR_DELETE_TITLE"] = "Eliminar un campo y abrir campos del formulario";
$MESS["CT_BLFE_TOOLBAR_DELETE_WARNING"] = "Usted esta seguro de querer eliminar este campo?";
$MESS["CT_BLFE_TAB_EDIT"] = "Ajustes ";
$MESS["CT_BLFE_TAB_EDIT_TITLE"] = "Par�metros generales";
$MESS["CT_BLFE_TAB_LIST"] = "Lista";
$MESS["CT_BLFE_TAB_LIST_TITLE"] = "Lista de propiedades de los valores";
$MESS["CT_BLFE_FIELD_SORT"] = "Ordenar";
$MESS["CT_BLFE_FIELD_NAME"] = "Nombre";
$MESS["CT_BLFE_FIELD_IS_REQUIRED"] = "Requerido";
$MESS["CT_BLFE_FIELD_MULTIPLE"] = "M�ltiple";
$MESS["CT_BLFE_FIELD_DEFAULT_VALUE"] = "Valor por defecto";
$MESS["CT_BLFE_FIELD_TYPE"] = "Tipo";
$MESS["CT_BLFE_FIELD_ACTIVE_FROM_EMPTY"] = "Indefinido";
$MESS["CT_BLFE_FIELD_ACTIVE_FROM_NOW"] = "Fecha y hora actual";
$MESS["CT_BLFE_FIELD_ACTIVE_FROM_TODAY"] = "fecha actual";
$MESS["CT_BLFE_FIELD_PREVIEW_PICTURE_FROM_DETAIL"] = "Crear una imagen previa a partir de la imagen total (si no es especificada)";
$MESS["CT_BLFE_FIELD_PREVIEW_PICTURE_DELETE_WITH_DETAIL"] = "Eliminar imagen previa cuando eliminemos la imagen total";
$MESS["CT_BLFE_FIELD_PREVIEW_PICTURE_UPDATE_WITH_DETAIL"] = "Siempre crear una imagen previa a partir de la imagen total";
$MESS["CT_BLFE_FIELD_PICTURE_SCALE"] = "Reducir el tama�o de las imagenes";
$MESS["CT_BLFE_FIELD_PICTURE_WIDTH"] = "Ancho m�xima ";
$MESS["CT_BLFE_FIELD_PICTURE_HEIGHT"] = "Altura M�xima ";
$MESS["CT_BLFE_FIELD_PICTURE_IGNORE_ERRORS"] = "Ignorar la escala de errores";
$MESS["CT_BLFE_LIST_ITEM_ADD"] = "Agregar";
$MESS["CT_BLFE_FIELD_SECTION_LINK_IBLOCK_ID"] = "Lista de secciones como valores";
$MESS["CT_BLFE_FIELD_ELEMENT_LINK_IBLOCK_ID"] = "Lista de elementos como valores";
$MESS["CT_BLFE_NO_VALUE"] = "(sin conjunto)";
$MESS["CT_BLFE_ENUM_IMPORT"] = "Importar una lista";
$MESS["CT_BLFE_DELETE_TITLE"] = "Eliminar";
$MESS["CT_BLFE_ENUM_IMPORT_HINT"] = "Convertir cada linea en un registro de la lista.";
$MESS["CT_BLFE_ENUM_DEFAULTS"] = "Valores por defecto";
$MESS["CT_BLFE_ENUM_DEFAULT"] = "Valor por defecto";
$MESS["CT_BLFE_ENUM_NO_DEFAULT"] = "(ninguno)";
$MESS["CT_BLFE_TEXT_WIDTH"] = "Control de Ancho (unidades en px y % son permitidas)";
$MESS["CT_BLFE_TEXT_HEIGHT"] = "Control de Alto (unidades en px y % son permitidas)";
$MESS["CT_BLFE_TEXT_USE_EDITOR"] = "Usar el Editor Visual";
$MESS["CT_BLFE_FIELD_INPUT_SIZE"] = "Tama�o del campo (Filas y Columnas)";
$MESS["CT_BLFE_SORT_TITLE"] = "Mover";
$MESS["CT_BLFE_FIELD_SHOW_ADD_FORM"] = "Mostrar en el nuevo formulario de elementos";
$MESS["CT_BLFE_FIELD_SHOW_EDIT_FORM"] = "Mostrar en el  formulario de edici�n de elementos";
$MESS["CT_BLFE_TEXT_WIDTH_NEW"] = "Ancho del Control (px)";
$MESS["CT_BLFE_TEXT_HEIGHT_NEW"] = "Alto del Control (px)";
?>