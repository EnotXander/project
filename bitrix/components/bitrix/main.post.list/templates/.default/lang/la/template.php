<?
$MESS["B_B_MS_LINK"] = "Enlace";
$MESS["BPC_MES_EDIT"] = "Editar";
$MESS["BPC_MES_SHOW"] = "Mostrar";
$MESS["BPC_MES_HIDE"] = "Ocultar";
$MESS["BPC_MES_DELETE_POST_CONFIRM"] = "�Est� seguro que desea eliminar el comentario?";
$MESS["BPC_MES_DELETE"] = "Eliminar";
$MESS["B_B_MS_ADD_COMMENT"] = "Agregar comentario";
$MESS["BLOG_C_HIDE"] = "Ocultar comentario";
$MESS["BLOG_C_REPLY"] = "Responder";
$MESS["BLOG_C_BUTTON_MORE"] = "M�s";
$MESS["B_B_PC_COM_ERROR"] = "Error al agregar comentario:";
$MESS["BLOG_C_VIEW"] = "Ver los comentarios m�s antiguos";
$MESS["JERROR_NO_MESSAGE"] = "Texto del comentario es necesario
";
$MESS["JQOUTE_AUTHOR_WRITES"] = "escribir";
$MESS["MPL_MES_HREF"] = "Ver comentarios";
$MESS["MPL_HAVE_WRITTEN"] = "escribi�:";
$MESS["MPL_SAFE_EDIT"] = "Usted est� escribiendo un mensaje. �Restablecer los cambios?";
?>