<?
$MESS["SB_TPL_ELEMENT_DELETE_CONFIRM"] = "Alle Informationen, die mit diesem Eintrag verbunden sind, werden gel�scht. Fortfahren?";
$MESS["SB_TPL_MESS_BTN_BUY"] = "Kaufen";
$MESS["SB_TPL_MESS_BTN_ADD_TO_BASKET"] = "In den Warenkorb";
$MESS["SB_TPL_MESS_PRODUCT_NOT_AVAILABLE"] = "Nicht verf�gbar";
$MESS["SB_TPL_MESS_BTN_DETAIL"] = "Details";
$MESS["SB_TPL_MESS_BTN_SUBSCRIBE"] = "Abonnieren";
$MESS["SB_CATALOG_SET_BUTTON_BUY"] = "Warenkorb anzeigen";
$MESS["SB_ADD_TO_BASKET_OK"] = "Das Produkt wurde zum Warenkorb hinzugef�gt";
$MESS["SB_TPL_MESS_PRICE_SIMPLE_MODE"] = "ab #PRICE# f�r #MEASURE#";
$MESS["SB_TPL_MESS_MEASURE_SIMPLE_MODE"] = "#VALUE# #UNIT#";
$MESS["SB_TPL_MESS_BTN_COMPARE"] = "Vergleichen";
$MESS["SB_CATALOG_TITLE_ERROR"] = "Fehler";
$MESS["SB_CATALOG_TITLE_BASKET_PROPS"] = "Produkteigenschaften, die zum Warenkorb hinzugef�gt werden";
$MESS["SB_CATALOG_BASKET_UNKNOWN_ERROR"] = "Unbekannter Fehler beim Hinzuf�gen des Produktes zum Warenkorb";
$MESS["SB_CATALOG_BTN_MESSAGE_CLOSE"] = "Schlie�en";
$MESS["SB_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "Warenkorb anzeigen";
$MESS["SB_CATALOG_BTN_MESSAGE_SEND_PROPS"] = "Ausw�hlen";
$MESS["SB_NO_PRODUCTS"] = "Keine Produkte.";
$MESS["SB_QUANTITY"] = "Menge";
$MESS["SB_HREF_TITLE"] = "Topseller";
$MESS["SB_DAYS"] = "Tage";
$MESS["SB_SHOW_LIDER_BY"] = "Nach:";
$MESS["SB_SHOW_PERIOD"] = "F�r:";
$MESS["SB_AMOUNT"] = "Erl�s";
?>