<?
$MESS["SMOB_PRODUCT_NAME"] = "Produktname";
$MESS["SMOB_BUTTON_CHECK"] = "Pr�fen";
$MESS["SMOB_BUTTON_SAVE"] = "Speichern";
$MESS["SMOB_BUTTON_BACK"] = "Zur�ck";
$MESS["SMOB_BARCODE_ENTER"] = "Geben Sie den Strichcode ein";
$MESS["SMOB_BARCODE_SCAN"] = "Scannen";
$MESS["SMOB_CHECK_ERROR"] = "Fehler bei der �berpr�fung des Strichcodes";
$MESS["SMOB_SCAN_ERROR"] = "Fehler beim Scannen des Strichcodes";
$MESS["SMOB_STORE_NAME_TMPL"] = "Lager: \"##STORE_NAME##\" (##STORE_ID##)";
?>