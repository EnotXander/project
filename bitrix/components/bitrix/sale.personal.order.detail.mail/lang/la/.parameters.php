<?
$MESS["SPOD_DESC_YES"] = "Si";
$MESS["SPOD_DESC_NO"] = "No";
$MESS["SPOD_PATH_TO_LIST"] = "P�gina de Lista de pedidos";
$MESS["SPOD_PATH_TO_CANCEL"] = "P�gina de la orden de cancelaci�n";
$MESS["SPOD_PATH_TO_PAYMENT"] = "P�gina de integraci�n del sistemas de pago";
$MESS["SPOD_ID"] = "ID de la orden";
$MESS["SPOD_PROPS_NOT_SHOW"] = "Ocultar Propiedades en Tipo dePagador";
$MESS["SPOD_SHOW_ALL"] = "(mostrar todos)";
$MESS["SPOD_ACTIVE_DATE_FORMAT"] = "Formato de fechas";
$MESS["SPOD_CACHE_GROUPS"] = "Respecto de permisos de acceso";
$MESS["SPOD_PARAM_PREVIEW_PICTURE_WIDTH"] = "Ancho m�ximo de la imagen en miniatura, px";
$MESS["SPOD_PARAM_PREVIEW_PICTURE_HEIGHT"] = "Altura m�xima de la imagen en miniatura, px";
$MESS["SPOD_PARAM_DETAIL_PICTURE_WIDTH"] = "Ancho m�ximo de la imagen detallada, px";
$MESS["SPOD_PARAM_DETAIL_PICTURE_HEIGHT"] = "Altura m�xima de la imagen detallada, px";
$MESS["SPOD_PARAM_CUSTOM_SELECT_PROPS"] = "Informaci�n adicional de las propiedades";
$MESS["SPOD_PARAM_RESAMPLE_TYPE"] = "Escala de la imagen";
$MESS["SPOD_PARAM_RESAMPLE_TYPE_BX_RESIZE_IMAGE_EXACT"] = "Crop";
$MESS["SPOD_PARAM_RESAMPLE_TYPE_BX_RESIZE_IMAGE_PROPORTIONAL"] = "Restringir por proporciones";
$MESS["SPOD_PARAM_RESAMPLE_TYPE_BX_RESIZE_IMAGE_PROPORTIONAL_ALT"] = "Restringir proporciones, el mejoramiento de la calidad";
?>