<?
$MESS["BLOG_MODULE_NOT_INSTALL"] = "Das Modul Blogs ist nicht installiert.";
$MESS["BLOG_BLOG_BLOG_MES_DELED"] = "Die Nachricht wurde erfolgreich gel�scht.";
$MESS["BLOG_BLOG_BLOG_MES_DEL_ERROR"] = "Fehler beim L�schen der Nachricht.";
$MESS["BLOG_BLOG_BLOG_MES_DEL_NO_RIGHTS"] = "Sie haben nicht gen�gend Rechte, um diese Nachricht zu l�schen.";
$MESS["BLOG_BLOG_BLOG_FRIENDS_ONLY"] = "Sie haben nicht gen�gend Rechte, um Nachrichten zu lesen.";
$MESS["MESSAGE_COUNT"] = "Nachrichten";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "Nachrichten sind nicht verf�gbar f�r diese Nutzergruppe.";
$MESS["BLOG_BLOG_SESSID_WRONG"] = "Ihre Sitzung ist abgelaufen. Versuchen Sie bitte erneut.";
$MESS["BLOG_BLOG_BLOG_MES_PUB"] = "Nachricht wurde ver�ffentlicht.";
$MESS["BLOG_BLOG_BLOG_MES_PUB_ERROR"] = "Fehler beim L�schen der Nachricht.";
$MESS["BLOG_BLOG_BLOG_MES_PUB_NO_RIGHTS"] = "Sie haben nicht gen�gend Rechte, diese Nachricht zu ver�ffentlichen.";
$MESS["BLOG_MOD_NO_SOCNET_GROUP"] = "Gruppe des sozialen Netzwerks ist nicht angegeben.";
$MESS["BLOG_MOD_NO_RIGHTS"] = "Sie haben nicht gen�gend Rechte, um die Nachrichten in dieser Gruppe zu moderieren.";
$MESS["BLOG_MOD_TITLE"] = "Moderation";
$MESS["BLOG_MOD_PUB"] = "Ver�ffentlichen";
$MESS["BLOG_MOD_DELETE"] = "L�schen";
$MESS["BLOG_MOD_DELETE_CONFIRM"] = "M�chten Sie diese Nachricht wirklich l�schen?";
$MESS["BLOG_MOD_NO_SOCNET_USER"] = "Sie haben nicht gen�gend Rechte, die zu moderierenden Nachrichten anzuzeigen";
?>