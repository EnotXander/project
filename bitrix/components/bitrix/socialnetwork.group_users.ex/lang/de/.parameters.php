<?
$MESS["SONET_SET_NAVCHAIN"] = "Breadcrumb-Navigation festlegen";
$MESS["SONET_ITEMS_COUNT"] = "Anzahl der Eintr�ge in der Liste";
$MESS["SONET_PATH_TO_USER"] = "Pfadvorlage zum Nutzerprofil";
$MESS["SONET_PATH_TO_GROUP"] = "Pfadvorlage zur Gruppenseite";
$MESS["SONET_PAGE_VAR"] = "Variable f�r die Seite";
$MESS["SONET_GROUP_VAR"] = "Variable f�r die Gruppe";
$MESS["SONET_USER_VAR"] = "Variable f�r den Nutzer";
$MESS["SONET_PATH_TO_GROUP_EDIT"] = "Pfadvorlage zur Seite der Parameter�nderung der Gruppe";
$MESS["SONET_GROUP_ID"] = "Gruppen-ID";
$MESS["SONET_VARIABLE_ALIASES"] = "Variablennamen";
$MESS["SONET_NAME_TEMPLATE"] = "Name anzeigen";
$MESS["SONET_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["SONET_SHOW_LOGIN"] = "Login anzeigen, wenn kein Name vorhanden";
$MESS["SONET_GROUP_USE_BAN"] = "Funktion der Schwarzen Liste in den Arbeitsgruppen benutzen";
$MESS["SONET_THUMBNAIL_LIST_SIZE"] = "Gr��e des Nutzerbildes (Px)";
?>