<?
$MESS["F_NO_MODULE"] = "Forumo modulis n�ra �diegtas";
$MESS["F_NO_MODULE_IBLOCK"] = "Informacini� blok� modulis n�ra �diegtas";
$MESS["F_ERR_FID_EMPTY"] = "Nenurodytas atsiliepim� forumas";
$MESS["F_ERR_FID_IS_NOT_EXIST"] = "Atsiliepim� forumo #FORUM_ID# n�ra";
$MESS["F_ERR_EID_EMPTY"] = "Nurodykite informacinio bloko element�";
$MESS["F_ERR_EID_IS_NOT_EXIST"] = "Elementas #ELEMENT_ID# nerastas";
$MESS["COMM_COMMENT_OK"] = "Atsiliepimas s�kmingai prid�tas";
$MESS["COMM_COMMENT_OK_AND_NOT_APPROVED"] = "Komentaras s�kmingai prid�tas. Jis bus rodomas po to, kai j� patvirtins moderatorius.";
$MESS["NAV_OPINIONS"] = "Atsiliepimai";
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "J�s� sesijos laikas baig�si. Pra�ome i�si�sti J�s� prane�im� dar kart�.";
$MESS["F_ERR_NO_REVIEW_TEXT"] = "�veskite atsiliepimo tekst�";
$MESS["F_GUEST"] = "Sve�ias";
$MESS["POSTM_CAPTCHA"] = "Apsaugos kodas nuo automatini� prane�im� yra neteisingas";
$MESS["F_ERR_NOT_RIGHT_FOR_ADD"] = "N�ra teisi� �d�ti komentar�";
$MESS["F_ERR_ADD_TOPIC"] = "Temos �terpimo klaida";
$MESS["F_ERR_ADD_MESSAGE"] = "Prane�imo �terpimo klaida";
$MESS["F_FORUM_TOPIC_ID"] = "Komentar� forumo tema";
$MESS["F_FORUM_MESSAGE_CNT"] = "Komentarai";
?>