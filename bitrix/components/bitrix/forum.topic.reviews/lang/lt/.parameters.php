<?
$MESS["F_MESSAGES_PER_PAGE"] = "Prane�im� skai�ius viename puslapyje";
$MESS["F_PATH_TO_SMILE"] = "Kelias iki �ypsen�li� katalogo ";
$MESS["F_FORUM_ID"] = "Forumo ID";
$MESS["F_IBLOCK_TYPE"] = "Informacijos bloko tipas";
$MESS["F_IBLOCK_ID"] = "Informacijos blokas";
$MESS["IBLOCK_ELEMENT_ID"] = "Elemento ID";
$MESS["F_USE_CAPTCHA"] = "Naudoti CAPTCHA";
$MESS["F_READ_TEMPLATE"] = "Forumo temos skaitymo puslapis";
$MESS["F_DETAIL_TEMPLATE"] = "Informacinio bloko elemento puslapis";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "Profilio puslapis";
$MESS["F_PREORDER"] = "Atvaizduoti prane�imus tiesiogine tvarka";
$MESS["F_DATE_TIME_FORMAT"] = "Datos ir laiko atvaizdavimo formatas";
$MESS["F_DISPLAY_PANEL"] = "Atvaizduoti �ios komponent�s valdymo mygtukus";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Navigacijos grandin�s �ablono pavadinimas";
$MESS["F_POST_FIRST_MESSAGE"] = "Prad�ti elemento tekstu";
$MESS["F_POST_FIRST_MESSAGE_TEMPLATE"] = "Teksto �ablonas pirmam temos prane�imui";
?>