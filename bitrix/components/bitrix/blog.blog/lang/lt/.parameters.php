<?
$MESS['BB_MESSAGE_COUNT'] = 'Prane�im� skai�ius viename puslapyje';
$MESS['BB_PATH_TO_BLOG'] = 'Kelio �ablonas � tinklara��io puslap�';
$MESS['BB_PATH_TO_BLOG_CATEGORY'] = 'Kelio �ablonas � tinklara��io puslap� su filtru pagal �ym�';
$MESS['BB_PATH_TO_POST'] = 'Kelio �ablonas � tinklara��io prane�im� puslap�';
$MESS['BB_PATH_TO_POST_EDIT'] = 'Kelio � tinklara��io prane�imo redagavimo puslap� �ablonas.';
$MESS['BB_PATH_TO_USER'] = 'Kelio �ablonas � tinklara��io naudotojo puslap�';
$MESS['BB_PATH_TO_SMILE'] = 'Kelias � aplank� su �ypsen�l�mis, svetain�s �akninio aplanko at�vilgiu';
$MESS['BB_BLOG_VAR'] = 'Tinklara��io identifikavimo kintamasis';
$MESS['BB_POST_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io prane�imo identifikatorius';
$MESS['BB_USER_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io naudotojo identifikatorius';
$MESS['BB_PAGE_VAR'] = 'Puslapio kintamojo pavadinimas';
$MESS['BB_CACHE_TIME_LONG'] = 'Likusi� puslapi� ke�avimo laikas';
$MESS['BB_BLOG_URL'] = 'Tinklara��io URL adresas';
$MESS['BB_YEAR'] = 'Filtras pagal metus';
$MESS['BB_MONTH'] = 'Filtras pagal menes�';
$MESS['BB_DAY'] = 'Filtras pagal dien�';
$MESS['BB_CATEGORY_ID'] = 'Filtravimo �ymi� identifikatorius';
$MESS['BB_SET_NAV_CHAIN'] = 'Prid�ti punkt� � navigacijos grandin�';
$MESS['POST_PROPERTY_LIST'] = 'Atvaizduoti prane�imo papildomas savybes tinklara�tyje';
$MESS['BB_FILTER_NAME_IN'] = 'Masyvo pavadinimas kuriame saugomi reik�m�s prane�im� filtravimui';
$MESS['B_VARIABLE_ALIASES'] = 'Kintam�j� vardai';
$MESS['BC_DATE_TIME_FORMAT'] = 'Datos ir laiko atvaizdavimo formatas';
$MESS['BB_NAV_TEMPLATE'] = 'Navigacijos grandin�s �ablono pavadinimas';
?>