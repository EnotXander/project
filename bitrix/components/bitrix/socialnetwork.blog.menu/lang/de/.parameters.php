<?
$MESS["BH_PATH_TO_BLOG"] = "Vorlage des Pfads zur Startseite der Nachrichten";
$MESS["BH_PATH_TO_POST_EDIT"] = "Vorlage des Pfads zur Seite der Nachrichtbearbeitung";
$MESS["BH_PATH_TO_DRAFT"] = "Vorlage des Pfads zur Seite der Entw�rfe";
$MESS["BH_PATH_TO_USER"] = "Vorlage des Pfads zum Nutzerprofil";
$MESS["BH_POST_VAR"] = "Variable f�r die Nachricht-ID";
$MESS["BH_PAGE_VAR"] = "Variable f�r die Seite";
$MESS["BH_USER_VAR"] = "Variable f�r die Nutzer-ID";
$MESS["BH_SET_NAV_CHAIN"] = "Punkt in die Navigationskette hinzuf�gen";
$MESS["B_VARIABLE_ALIASES"] = "Variablen";
?>