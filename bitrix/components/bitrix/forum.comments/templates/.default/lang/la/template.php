<?
$MESS["OPINIONS_NAME"] = "Su nombre";
$MESS["OPINIONS_EMAIL"] = "Su correo";
$MESS["OPINIONS_SEND"] = "Enviar";
$MESS["OPINIONS_PREVIEW"] = "Anterior";
$MESS["F_C_GOTO_FORUM"] = "Ir al foro &gt;&gt;";
$MESS["F_CAPTCHA_PROMT"] = "Caracteres de la imagen CAPTCHA";
$MESS["F_QUOTE_FULL"] = "Cotizaci�n";
$MESS["F_WANT_ALLOW_SMILES"] = "�Usted desea habilitar <b>enable</b> los emoticonos en este mensaje?";
$MESS["F_WANT_SUBSCRIBE_TOPIC"] = "Suscribirse a los Nuevos Mensajes de este Tema";
$MESS["F_WANT_SUBSCRIBE_FORUM"] = "Suscribirse a los Nuevos Mensajes de este Foro";
$MESS["F_QUOTE_HINT"] = "Para citar el mensaje en forma de respuesta, seleccione el texto y haga clic aqu�";
$MESS["F_NAME"] = "Nombre";
$MESS["JQOUTE_AUTHOR_WRITES"] = "escribir";
$MESS["F_PREVIEW"] = "Anterior";
$MESS["F_HIDE"] = "Ocultar";
$MESS["F_SHOW"] = "Mostrar";
$MESS["F_DELETE"] = "Eliminar";
$MESS["F_DELETE_CONFIRM"] = "El mensaje ser� eliminado irreversiblemente. �Desea Continuar?";
$MESS["F_CAPTCHA_TITLE"] = "Protecci�n del bot spam (CAPTCHA)";
?>