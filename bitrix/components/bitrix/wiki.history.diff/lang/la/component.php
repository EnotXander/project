<?
$MESS["WIKI_MODULE_NOT_INSTALLED"] = "El m�dulo de wiki no est� instalado.";
$MESS["WIKI_SOCNET_INITIALIZING_FAILED"] = "Social network fall� al inicializar.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "El m�dulo Information Blocks no est� instalado.";
$MESS["IBLOCK_NOT_ASSIGNED"] = "No se ha seleccionado un block de informaci�n.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "El m�dulo Social Network no est� instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El m�dulo Business Processes no est� instalado.";
$MESS["WIKI_NOT_CHANGE_BIZPROC"] = "El block de informaci�n de Wiki no debe registrarse con procesos de negocio para mantener el historial de la versi�n.";
$MESS["WIKI_ACCESS_DENIED"] = "Acceso Denegado.";
?>