<?
$MESS["BPABL_TYPE_5"] = "Veiksmas '#ACTIVITY#'#NOTE#";
$MESS["BPABL_TYPE_4"] = "Veiksmo '#ACTIVITY#'#NOTE# klaida";
$MESS["BPABL_STATUS_3"] = "At�auktas";
$MESS["BPABL_STATE_NAME"] = "Dabartin� verslo proceso b�kl�";
$MESS["BPABL_LOG"] = "Verslo proceso istorija";
$MESS["BPABL_RES_3"] = "At�auktas";
$MESS["BPABL_TYPE_3"] = "At�auktas veiksmas '#ACTIVITY#'#NOTE#";
$MESS["BPABL_STATUS_4"] = "Atliktas";
$MESS["BPABL_TYPE_2"] = "Atliktas veiksmas '#ACTIVITY#'   gr��imo statusas: '#STATUS#'	 rezultatas: '#RESULT#'#NOTE#";
$MESS["BPABL_STATE_MODIFIED"] = "Dabartin�s b�kl�s data";
$MESS["BPABL_STATUS_5"] = "Klaida";
$MESS["BPABL_RES_4"] = "Klaida";
$MESS["BPABL_STATUS_2"] = "Vykdoma";
$MESS["BPABL_STATUS_1"] = "Inicializuota";
$MESS["BPABL_TYPE_1"] = "Paleistas veiksmas '#ACTIVITY#'#NOTE#";
$MESS["BPABL_RES_1"] = "Ne";
$MESS["BPABL_RES_5"] = "Neinicializuota";
$MESS["BPABL_TYPE_6"] = "Buvo atliktas ne�inomas veiksmas, susij�s su '#ACTIVITY#'#NOTE#";
$MESS["BPABL_RES_2"] = "Sekm�";
$MESS["BPABL_STATUS_6"] = "Nenurodytas";
$MESS["BPABL_RES_6"] = "Nenurodytas";
$MESS["BPWC_WLCT_TOTAL"] = "I� viso";
?>