<?
$MESS["CC_BLL_MODULE_NOT_INSTALLED"] = "Das Listenmodul ist nicht installiert.";
$MESS["CC_BLL_WRONG_IBLOCK_TYPE"] = "Nicht korrekter Typ des Informationsblocks.";
$MESS["CC_BLL_WRONG_IBLOCK"] = "Die definierte Liste ist falsch.";
$MESS["CC_BLL_LISTS_FOR_SONET_GROUP_DISABLED"] = "Die Listen sind f�r diese Gruppe deaktiviert.";
$MESS["CC_BLL_UNKNOWN_ERROR"] = "Unbekannter Fehler.";
$MESS["CC_BLL_ACCESS_DENIED"] = "Keine Berechtigung zur Vorschau und Bearbeitung der Liste.";
$MESS["CC_BLL_TITLE"] = "Liste: #NAME#";
$MESS["CC_BLL_ELEMENT_ACTION_MENU_EDIT"] = "Bearbeiten";
$MESS["CC_BLL_ELEMENT_ACTION_MENU_DELETE"] = "L�schen";
$MESS["CC_BLL_ELEMENT_ACTION_MENU_START_BP"] = "Gesch�ftsprozess ausf�hren";
$MESS["CC_BLL_ELEMENT_ACTION_MENU_DELETE_CONF"] = "Wollen Sie dieses Element wirklich l�schen?";
$MESS["CC_BLL_ELEMENT_ACTION_MENU_VIEW"] = "Vorschau";
$MESS["CC_BLL_SECTION"] = "Bereich";
$MESS["CC_BLL_ANY"] = "(beliebig)";
$MESS["CC_BLL_UPPER_LEVEL"] = "Obere Ebene";
$MESS["CC_BLL_COLUMN_SECTION"] = "Bereiche";
$MESS["CC_BLL_COLUMN_BIZPROC"] = "Gesch�ftsprozesse";
$MESS["CC_BLL_ELEMENT_ACTION_MENU_COPY"] = "Kopieren";
$MESS["CC_BLL_DELETE_ERROR"] = "Beim L�schen ist ein Fehler aufgetreten.";
$MESS["CC_BLL_TITLE_PROCESS"] = "Workflow: #NAME#";
$MESS["CC_BLL_COMMENTS"] = "Kommentare";
$MESS["CC_BLL_ELEMENT_ACTION_MENU_RUNNING_BP"] = "Initiierte Workflows";
$MESS["CT_BLL_BIZPROC_DELETE"] = "Workflow l�schen";
$MESS["CT_BLL_BIZPROC_STOP"] = "Workflow stoppen";
?>