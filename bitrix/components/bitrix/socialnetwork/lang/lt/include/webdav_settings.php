<?
$MESS["SONET_FILES"] = "Failai";
$MESS["SONET_FILES_IS_NOT_ACTIVE"] = "Ne�jungta fail� funkcija.";
$MESS["SONET_WD_MODULE_IS_NOT_INSTALLED"] = "Ne�diegtas dokument� bibliotekos modulis.";
$MESS["SONET_IB_MODULE_IS_NOT_INSTALLED"] = "Ne�diegtas informacijos blok� modulis.";
$MESS["SONET_IBLOCK_ID_EMPTY"] = "Nenurodytas informacijos blokas.";
$MESS["SONET_ACCESS_DENIED"] = "Prieiga draud�iama.";
$MESS["SONET_GROUP_NOT_EXISTS"] = "Tokios grup�s n�ra.";
$MESS["SONET_GROUP_PREFIX"] = "Grup�:";
$MESS["SONET_WEBDAV_NOT_EXISTS"] = "Dokument� n�ra.";
$MESS["SONET_UF_USE_BP"] = "Naudoti verslo proces�";
$MESS["SN_TITLE"] = "Dokument� biblioteka";
$MESS["SN_TITLE_TITLE"] = "Dokument� bibliotekos parametrai";
$MESS["SN_BP"] = "Verslo procesai:";
$MESS["SN_BP_LABEL"] = "naudoti vis� verslo proces� versij�";
?>