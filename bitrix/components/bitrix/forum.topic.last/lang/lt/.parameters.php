<?
$MESS['F_URL_TEMPLATES'] = 'Tinklapi� URL administravimas';
$MESS['F_SET_NAVIGATION'] = 'Rodyti navigacij�';
$MESS['F_TOPICS_PER_PAGE'] = 'Tem� skai�ius viename puslapyje';
$MESS['F_SHOW_FORUM_ANOTHER_SITE'] = 'Atvaizduoti kit� svetaini� forumus';
$MESS['F_INDEX_TEMPLATE'] = 'Forum� s�ra�o puslapis';
$MESS['F_READ_TEMPLATE'] = 'Tem� skaitymo puslapis';
$MESS['F_LIST_TEMPLATE'] = 'Tem� s�ra�o puslapis';
$MESS['F_PROFILE_VIEW_TEMPLATE'] = 'Naudotojo profilio puslapis';
$MESS['F_DEFAULT_FID'] = 'Forumo ID';
$MESS['F_SHOW_TITLE'] = 'Pavadinimas';
$MESS['F_SHOW_USER_START_NAME'] = 'Autorius';
$MESS['F_SHOW_POSTS'] = 'Atsakymai';
$MESS['F_SHOW_VIEWS'] = 'Per�i�r�';
$MESS['F_SHOW_LAST_POST_DATE'] = 'Paskutinis prane�imas';
$MESS['F_SORTING_ORD'] = 'Laukas r��iavimui';
$MESS['F_SORTING_BY'] = 'R��iavimo kryptis';
$MESS['F_DESC_ASC'] = 'did�jan�ia';
$MESS['F_DESC_DESC'] = 'ma��jan�ia';
$MESS['F_DATE_TIME_FORMAT'] = 'Datos ir laiko atvaizdavimo formatas';
$MESS['F_TOPICS'] = 'Temos';
$MESS['F_DISPLAY_PANEL'] = 'Atvaizduoti �io komponento valdymo mygtukus';
$MESS['F_MESSAGE_TEMPLATE'] = '�inu�i� skaitymo puslapis';
$MESS['F_SORT_BY_SORT_FIRST'] = 'Atvaizduoti vir�uje prisegtas temas';
?>