<?
$MESS['BF_MESSAGE_COUNT'] = 'Prane�im� skai�ius viename puslapyje';
$MESS['BF_PATH_TO_BLOG'] = 'Kelio �ablonas � tinklara��io puslap�';
$MESS['BF_PATH_TO_BLOG_CATEGORY'] = 'Kelio �ablonas � tinklara��io puslap� su filtru pagal �ym�';
$MESS['BF_PATH_TO_POST'] = 'Kelio �ablonas � tinklara��io prane�im� puslap�';
$MESS['BF_PATH_TO_POST_EDIT'] = 'Kelio � tinklara��io prane�imo redagavimo puslap� �ablonas.';
$MESS['BF_PATH_TO_USER'] = 'Kelio �ablonas � tinklara��io naudotojo puslap�';
$MESS['BF_PATH_TO_SMILE'] = 'Kelias � aplank� su �ypsen�l�mis, svetain�s �akninio aplanko at�vilgiu';
$MESS['BF_BLOG_VAR'] = 'Tinklara��io identifikavimo kintamasis';
$MESS['BF_POST_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io prane�imo identifikatorius';
$MESS['BF_USER_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io naudotojo identifikatorius';
$MESS['BF_PAGE_VAR'] = 'Puslapio kintamojo pavadinimas';
$MESS['BF_ID'] = 'Naudotojo ID';
$MESS['B_VARIABLE_ALIASES'] = 'Kintam�j� vardai';
$MESS['BC_DATE_TIME_FORMAT'] = 'Datos ir laiko atvaizdavimo formatas';
?>