<?
$MESS["SPOL_DESC_YES"] = "Si";
$MESS["SPOL_DESC_NO"] = "No";
$MESS["SPOL_PATH_TO_DETAIL"] = "La orden detalla la p�gina ";
$MESS["SPOL_PATH_TO_COPY"] = "Repetir la p�gina de la orden ";
$MESS["SPOL_PATH_TO_CANCEL"] = "P�gina de la cancelaci�n de la orden";
$MESS["SPOL_PATH_TO_BASKET"] = "P�gina de la cesta ";
$MESS["SPOL_ORDERS_PER_PAGE"] = "N�mero de �rdenes por p�gina ";
$MESS["SPOL_ID"] = "ID del pedido";
$MESS["SPOL_SAVE_IN_SESSION"] = "Almacenar las configuraciones del filtro en la sesi�n del usuario";
$MESS["SPOL_NAV_TEMPLATE"] = "Plantilla de los Breadcrumbs de Navegaci�n";
$MESS["SPOL_ACTIVE_DATE_FORMAT"] = "Formato de fecha";
$MESS["SPOL_HISTORIC_STATUSES"] = "Historial de estatus";
$MESS["SPOL_CACHE_GROUPS"] = "Respetar permisos de acceso";
?>