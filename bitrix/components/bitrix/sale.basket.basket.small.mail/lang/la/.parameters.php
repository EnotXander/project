<?
$MESS["SBBS_PATH_TO_ORDER"] = "P�gina de la orden";
$MESS["SBBS_SHOW_DELAY"] = "Mostrar productos reservados ";
$MESS["SBBS_SHOW_NOTAVAIL"] = "Mostrar art�culos que no est�n en el inventario";
$MESS["SBBS_SHOW_SUBSCRIBE"] = "Mostrar productos a los que est� suscrito el usuario";
$MESS["SBB_COLUMNS_LIST"] = "Columnas";
$MESS["SBB_BNAME"] = "Nombre del producto";
$MESS["SBB_BPROPS"] = "Propiedades de elemento del producto";
$MESS["SBB_BPRICE"] = "Precio";
$MESS["SBB_BSUM"] = "Total";
$MESS["SBB_BTYPE"] = "Tipo de precio";
$MESS["SBB_BQUANTITY"] = "Cantidad";
$MESS["SBB_BDELETE"] = "Eliminar";
$MESS["SBB_BDELAY"] = "Mantener";
$MESS["SBB_BWEIGHT"] = "Peso";
$MESS["SBB_BDISCOUNT"] = "Descuento";
$MESS["SBBS_PATH_TO_BASKET"] = "P�gina de la cesta";
?>