<?
$MESS["SONET_MODULE_NOT_INSTALL"] = "El m�dulo Social Network no est� instalado.";
$MESS["SONET_C33_NO_GROUP_ID"] = "El grupo no est� especificado.";
$MESS["SONET_C33_NO_GROUP"] = "Grupo no fue encontrado.";
$MESS["SONET_C33_NO_PERMS"] = "No tienes permiso para invitar a usuarios al grupo.";
$MESS["SONET_C33_PAGE_TITLE"] = "Invitaci�n al Grupo";
$MESS["SONET_C33_NO_USERS"] = "La invitaci�n a los destinatarios no se especifica.";
$MESS["SONET_C33_NO_USER1"] = "El usuario '#NAME#' no fue encontrado.";
$MESS["SONET_C11_ERR_SELF"] = "Usted no puede agregarse a usted mismo al grupo.";
$MESS["SONET_C11_BAD_USER"] = "El usuario ' #NAME#' no fue encontrado. ";
$MESS["SONET_C11_BAD_RELATION"] = "El usuario '#NAME#' ya ha sido invitado a la agrupaci�n o ya es miembro.";
$MESS["SONET_C33_CANNOT_USER_ADD"] = "Usuario con mail '#EMAIL#' no fue agregado:";
$MESS["SONET_C33_NOT_EMPLOYEE"] = "Un usuario con correo electr�nico \\'#EMAIL#\\' no fue invitado porque no es usuario registrado o no un empleado.";
?>