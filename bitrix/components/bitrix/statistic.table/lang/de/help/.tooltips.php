<?
$MESS["CACHE_TYPE_TIP"] = "<i>Automatisch</i>: Der Cache ist g�ltig gem�� Definition in den Cache-Einstellungen;<br /><i>Cache</i>: immer cachen f�r den Zeitraum, der im n�chsten Feld definiert wird;<br /><i>Nicht cachen</i>: es wird kein Caching ausgef�hrt.";
$MESS["CACHE_TIME_TIP"] = "Feld f�r die Eingabe der Cache-Laufzeit in Sekunden.";
$MESS["CACHE_FOR_ADMIN_TIP"] = "Standardm��ig ist der Cache f�r die Nutzergruppe Administratoren deaktiviert, so dass die Anfragen an die Datenbank ausgef�hrt werden, wenn die Seite angezeigt wird. Aktivieren Sie diese Option, um die Datenbank-Belastung zu reduzieren.";
?>