<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$arFields = array(
    'ONLY' => array("NAME", "LAST_NAME", "PERSONAL_PHOTO", "LOGIN", "PASSWORD", "NEW_PASSWORD", "CONFIRM_PASSWORD", "PERSONAL_PHONE", "UF_COUNTRY", "UF_REGION", "UF_CITY", "PERSONAL_STREET", "PERSONAL_ZIP"),
);
/*
$arFields = array(
    'PERSONAL' => array(
        'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO', 'PERSONAL_GENDER', 'PERSONAL_BIRTHDAY', 'PERSONAL_BIRTHDATE',
        'PERSONAL_PROFESSION', 'PERSONAL_NOTES', 'AUTO_TIME_ZONE', 'TIME_ZONE',
    ),
);*/
/*
if (!empty($arResult["arSocServ"]))
   $arFields['SOCSERV'] = array('SOCSERVICES');

$arFields['CONTACT'] = array(
    'EMAIL', 'PERSONAL_PHONE', 'PERSONAL_MOBILE', 'PERSONAL_WWW', 'PERSONAL_ICQ', 'PERSONAL_FAX', 'PERSONAL_PAGER',
    'PERSONAL_COUNTRY', 'PERSONAL_STREET', 'PERSONAL_MAILBOX', 'PERSONAL_CITY', 'PERSONAL_STATE', 'PERSONAL_ZIP',
);

$arFields['WORK'] = array(
    'WORK_COUNTRY', 'WORK_CITY', 'WORK_COMPANY', 'WORK_DEPARTMENT', 'WORK_PROFILE', 'WORK_WWW', 'WORK_PHONE',
    'WORK_FAX', 'WORK_PAGER', 'WORK_LOGO', 'WORK_POSITION',
);

$arFields['AUTH'] = array(
    'LOGIN', 'PASSWORD', 'CONFIRM_PASSWORD',
);

if ($arParams['IS_FORUM'] == 'Y')
{
   $arFields['FORUM'] = array(
       'FORUM_SHOW_NAME', 'FORUM_HIDE_FROM_ONLINE', 'FORUM_SUBSC_GET_MY_MESSAGE', 'FORUM_DESCRIPTION', 'FORUM_INTERESTS', 'FORUM_SIGNATURE', 'FORUM_AVATAR'
   );
}

if ($arParams['IS_BLOG'] == 'Y')
{
   $arFields['BLOG'] = array(
       'BLOG_ALIAS', 'BLOG_DESCRIPTION', 'BLOG_INTERESTS', 'BLOG_AVATAR'
   );
}*/

foreach ($arParams['EDITABLE_FIELDS'] as $FIELD)
{
   $bFound = false;
   if ($arResult['USER_PROP'][$FIELD])
   {
      foreach ($arFields as $FIELD_TYPE => $arTypeFields)
      {
         if (is_array($arParams['USER_PROPERTY_' . $FIELD_TYPE]) && in_array($FIELD, $arParams['USER_PROPERTY_' . $FIELD_TYPE]))
         {
            $arFields[$FIELD_TYPE][] = $FIELD;
            $bFound = true;
            break;
         }
      }

      /*if (!$bFound)
         $arFields['PERSONAL'][] = $FIELD;*/
   }
}
//PrintObject($arFields);
$GROUP_ACTIVE = false;

foreach ($arFields as $GROUP => $arGroupFields)
{
   $arFields[$GROUP] = array_unique($arGroupFields);
   foreach ($arGroupFields as $fkey => $FIELD)
   {
      if (!in_array($FIELD, $arParams['EDITABLE_FIELDS']))
      {
         unset($arGroupFields[$fkey]);
      } elseif (!$GROUP_ACTIVE)
         $GROUP_ACTIVE = $GROUP;
   }

   $arFields[$GROUP] = array_unique($arGroupFields);
}
//PrintObject($arFields);
//echo '<pre>'; print_r($arFields); echo '</pre>';

$current_fieldset = $_REQUEST['current_fieldset'] ? $_REQUEST['current_fieldset'] : ($GROUP_ACTIVE ? $GROUP_ACTIVE : 'PERSONAL');

if (!in_array($current_fieldset, array_keys($arFields)))
   $current_fieldset = 'PERSONAL';

if ($arResult['ERROR_MESSAGE'])
{
   ?>
   <div class="bx-sonet-profile-edit-error">
      <?
      ShowError($arResult['ERROR_MESSAGE']);
      ?>
   </div>
   <?
}

//echo '<pre>'; print_r($arFields); echo '</pre>';
?>
<form name="bx_user_profile_form" id="user_profil_edit_form" method="POST" action="<? echo POST_FORM_ACTION_URI; ?>" enctype="multipart/form-data">
   <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
   <? echo bitrix_sessid_post() ?>
   <input type="hidden" name="current_fieldset" value="<? echo $current_fieldset ?>" />
   <div class="bx-sonet-profile-edit-layout">

      <div class="bx-sonet-profile-edit-layers">
         <?
         //$GROUP_ID = "ONLY";
         //$arGroupFields = $arFields[$GROUP_ID];
         $arGroupFields = $arParams["DISPLAY_FIELDS"]?>
         <div id="bx_sonet_fieldset_<? echo $GROUP_ID ?>" class="bx-sonet-profile-fieldset">
            <table class="bx-sonet-profile-fieldset-table bodyground">
               <?
               foreach ($arGroupFields as $fieldKey => $FIELD):
                  if ($FIELD == 'EMPTY'):
                     ?><tr style="background-color:#ffffff;"><td colspan="3"></td></tr><?
                     continue;
                  endif;
                  if (($FIELD == 'AUTO_TIME_ZONE' || $FIELD == 'TIME_ZONE') && $arResult["TIME_ZONE_ENABLED"] <> true)
                     continue;
                  $value = $arResult['User'][$FIELD];
                  ?>
                  <tr class="<?= strlen($arResult["VALID"][$FIELD]) ? 'error' : '' ?> <?=(($fieldKey == count($arGroupFields)-1) || ($arGroupFields[$fieldKey+1] == "EMPTY")) ? 'auth-last' : ''?>">
                     <td class="bx-sonet-profile-fieldcaption">
                        <?
                        if ($arResult['USER_PROPERTY_ALL'][$FIELD]['MANDATORY'] == "Y"):
                           ?><span class="required-field">*</span><?
                        endif;

                        if ($FIELD == "PASSWORD" && intval($arResult["PASSWORD_MIN_LENGTH"]) > 0)
                           echo str_replace(array('#MIN_LENGTH#'), $arResult["PASSWORD_MIN_LENGTH"], GetMessage('ISL_PASSWORD_1')).":";
                        /*elseif(($FIELD == "UF_REGION") || $FIELD == "UF_CITY")
                           {}*/
                        else
                           echo $arResult['USER_PROP'][$FIELD] ? $arResult['USER_PROP'][$FIELD].":" : GetMessage('ISL_' . $FIELD).":";
                        ?>
                     </td>				
                     <td class="bx-sonet-profile-field">
                        <?switch ($FIELD)
                        {
                           case 'PERSONAL_COUNTRY':
                           case 'WORK_COUNTRY':
                              echo SelectBoxFromArray($FIELD, GetCountryArray(), $value, GetMessage("ISL_COUNTRY_EMPTY"));
                              break;
                           case 'PASSWORD':
                           case 'NEW_PASSWORD':
                           case 'CONFIRM_PASSWORD':
                              ?><input type="password" id="register-form-<?=$FIELD?>" name="<? echo $FIELD ?>" autocomplete="off" />
                              <div class="errortext"><?= $arResult["VALID"][$FIELD] ?></div><?
                              break;
                           case 'NAME':
                           case 'LAST_NAME':
                           case 'LOGIN':
                           case 'PERSONAL_PHONE':
                           case 'PERSONAL_STREET':
                           case 'PERSONAL_ZIP':
                              ?><input type="text" id="register-form-<?=$FIELD?>" name="<?=$FIELD?>" value="<?=$arResult["User"][$FIELD]?>">
                              <div class="errortext"><?= $arResult["VALID"][$FIELD] ?></div><?
                              break;
                           case 'UF_COUNTRY':
                              ?><select class="select_location js_country_sel" name="UF_COUNTRY">
                                 <option value="0">�������� ������</option>
                                 <? foreach ($arResult["PLACE"]["COUNTRY"] as $key => $val): ?>
                                    <option value = "<?= $val["ID"] ?>" <?= $val["ID"]==$arResult["USER_PROPERTY_ALL"]["UF_COUNTRY"]["VALUE"] ? 'selected' : '' ?> ><?= $val["NAME"] ?></option>
                                 <? endforeach; ?>
                              </select>
                              <div class="errortext"><?= $arResult["VALID"][$FIELD] ?></div><?
                              break;
                           case 'UF_REGION':
                              ?><select<?=(!$arResult["USER_PROPERTY_ALL"]["UF_REGION"]["VALUE"]) ? " disabled" : ""?> class="select_location js_region_sel" name="UF_REGION">
                                 <option value="0">�������� ������</option>
                                 <?if($arResult["USER_PROPERTY_ALL"]["UF_REGION"]["VALUE"]):?>
                                    <? foreach ($arResult["PLACE"]["REGION"] as $key => $val): ?>
                                       <option value = "<?= $val["ID"] ?>" <?= $val["ID"]==$arResult["USER_PROPERTY_ALL"]["UF_REGION"]["VALUE"] ? 'selected' : '' ?> ><?= $val["NAME"] ?></option>
                                    <? endforeach; ?>
                                 <?endif;?>
                              </select>
                              <div class="errortext"><?= $arResult["VALID"][$FIELD] ?></div><?
                              break;
                           case 'UF_CITY':
                              ?><select<?=(!$arResult["USER_PROPERTY_ALL"]["UF_CITY"]["VALUE"]) ? " disabled" : ""?> class="select_location js_city_sel" name="UF_CITY">
                                 <option value="0">�������� �����</option>
                                 <?if($arResult["USER_PROPERTY_ALL"]["UF_CITY"]["VALUE"]):?>
                                    <? foreach ($arResult["PLACE"]["CITY"] as $key => $val): ?>
                                       <option value = "<?= $val["ID"] ?>" <?= $val["ID"]==$arResult["USER_PROPERTY_ALL"]["UF_CITY"]["VALUE"] ? 'selected' : '' ?> ><?= $val["NAME"] ?></option>
                                    <? endforeach; ?>
                                 <?endif;?>
                              </select>
                              <div class="errortext"><?= $arResult["VALID"][$FIELD] ?></div><?
                              break;
                           case 'PERSONAL_PHOTO':
                              //PrintObject($arResult['User']);
                              if ($arResult['User'][$FIELD.'_IMG']):?>
                                 <div class="bx-sonet-profile-field-photo"><?echo $arResult['User'][$FIELD.'_IMG']?></div>
                                 <input type="checkbox" name="<?echo $FIELD?>_del" id="<?echo $FIELD?>_del" value="Y" /><label for="<?echo $FIELD;?>_del"><?echo GetMessage('ISL_'.$FIELD.'_del')?></label><br />
                              <?endif; ?>
                                 <input type="file" name="<?echo $FIELD?>" />
                              <?break;
                        }?>
                     </td>
                     <td class="bordered"><div></div></td>
                  </tr>
               <?endforeach;?>
               <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
               <? /*
               <!--��������� ��������-->
               <tr class="auth-last js-add js-addcompany">
                  <td class="bx-sonet-profile-fieldcaption">��������� ��������:</td>
                  <td class="bx-sonet-profile-field js-addcompany-block" data-status-text="<?=$arParams["EMPLOYEE_STATUS_TEXT"]?>">
                     <div class="js-addcompany-exists">
                        <?foreach($arResult["EMPLOYEE"]["LIST"] as $arEmployee):?>
                           <div class="addcompany-exists-block">
                              <div class="addcompany-exists-row">
                                 <div>��������:</div>
                                 <div class="addcompany-delete-place"><input type="checkbox"<?=in_array($arEmployee["ID"], $arResult["EMPLOYEE_POST"]["DELETE"]) ? ' checked' : ''?> name="employee_delete[]" value="<?=$arEmployee["ID"]?>">�������</div>
                                 <div><a href="<?=$arResult["EMPLOYEE"]["COMPANIES"][$arEmployee["COMPANY"]]["DETAIL_PAGE_URL"]?>"><?=$arResult["EMPLOYEE"]["COMPANIES"][$arEmployee["COMPANY"]]["NAME"]?></a></div>
                              </div>
                              <div class="addcompany-exists-row">
                                 <div>���������:</div>
                                 <div><?=strlen($arEmployee["MANUAL"]) ? $arEmployee["POSITION_MANUAL"] : $arResult["EMPLOYEE"]["POSITIONS"][$arEmployee["POSITION"]]["NAME"]?></div>
                              </div>
                              <div class="addcompany-exists-row">
                                 <div>������:</div>
                                 <div><?=$arEmployee["STATUS_TEXT"]?></div>
                              </div>
                              <!--<div class="addcompany-exists-row">
                                 <div>����������:</div>
                                 <div><input type="checkbox"<?=in_array($arEmployee["ID"], $arResult["EMPLOYEE_POST"]["DELETE"]) ? ' checked' : ''?> name="employee_delete[]" value="<?=$arEmployee["ID"]?>">�������</div>
                              </div>-->
                           </div>
                        <?endforeach;?>
                     </div>
                     <div class="js-addcompany-newblock">
                        <?foreach($arResult["EMPLOYEE_POST"]["ADD"] as $employeeKey => $arEmployee):?>
                           <div class="addcompany-exists-block">
                              <div class="addcompany-exists-row">
                                 <div>��������:</div>
                                 <div><a href="<?=$arResult["COMPANY"][$arEmployee["COMPANY"]]["DETAIL_PAGE_URL"]?>"><?=$arResult["COMPANY"][$arEmployee["COMPANY"]]["NAME"]?></a></div>
                              </div>
                              <div class="addcompany-exists-row">
                                 <div>���������:</div>
                                 <div><?=strlen($arEmployee["POSITION_MANUAL"]) ? $arEmployee["POSITION_MANUAL"] : $arResult["POSITION"][$arEmployee["POSITION"]]["NAME"]?></div>
                              </div>
                              <div class="addcompany-exists-row">
                                 <div>������:</div>
                                 <div><?=$arParams["EMPLOYEE_STATUS_TEXT"]?></div>
                              </div>
                              <div class="addcompany-exists-row">
                                 <div>����������:</div>
                                 <div><a class="js-addcompany-delete" href="javascript:void(0);">�������</a></div>
                              </div>
                              <input type="hidden" name="employee_new[<?=$employeeKey?>][company]" value="<?=$arEmployee["COMPANY"]?>">
                              <input type="hidden" name="employee_new[<?=$employeeKey?>][position]" value="<?=$arEmployee["POSITION"]?>">
                              <input type="hidden" name="employee_new[<?=$employeeKey?>][manual]" value="<?=$arEmployee["POSITION_MANUAL"]?>">
                           </div>
                        <?endforeach;?>
                     </div>
                     <div class="js-addcompany-forms" style="width: 100%; display: none;">
                        <div class="js-addcompany-company">
                           <select class="select_location js_company_sel" name="UF_COMPANY">
                              <option value="0">�������� ��������</option>
                              <? foreach ($arResult["COMPANY"] as $key => $val): ?>
                                 <option value="<?= $val["ID"] ?>" rel="<?=$val["DETAIL_PAGE_URL"]?>"><?= $val["NAME"] ?></option>
                              <? endforeach; ?>
                           </select>
                        </div>
                        <div class="js-addcompany-position">
                           <select class="select_location js_position_sel" name="UF_POSITION">
                              <option value="0">�������� ���������</option>
                              <? foreach ($arResult["POSITION"] as $key => $val): ?>
                                 <option value = "<?= $val["ID"] ?>"><?= $val["NAME"] ?></option>
                              <? endforeach; ?>
                                 <option value = "other">������...</option>
                           </select>
                        </div>
                        <div class="js-addcompany-position-manual">
                           <div>��� ������� ������ ���������</div>
                           <input type="text" name="" autocomplete="off" />
                        </div>
                        <div style="/*display: none;">
                           <input type="button" class="js-addcompany-save" disabled name="" autocomplete="off" value="��������"/>
                           <input type="button" class="js-addcompany-close" name="" autocomplete="off" value="�������"/>
                        </div>
                     </div>
                     <div class="js-addcompany-trigger">
                        <a href="javascript:void(0)">+�������� ������</a>
                     </div>
                  </td>
                  <td class="bordered"><div></div></td>
               </tr> */ ?>
               <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
               
               <!--������������� ��������-->
               <tr class="auth-last js-add js-addpredstavitel">
                  <td class="bx-sonet-profile-fieldcaption">������ �� ����������������� ��������:</td>
                  <td class="bx-sonet-profile-field js-addcompany-block" data-status-text="<?=$arParams["PREDSTAVITEL_STATUS_TEXT"]?>">
                     <div class="js-addcompany-exists">
                        <?foreach($arResult["PREDSTAVITEL"]["LIST"] as $arEmployee):?>
                           <div class="addcompany-exists-block">
                              <div class="addcompany-exists-row">
                                 <div>��������:</div>
                                 <div class="addcompany-delete-place"><input type="checkbox"<?=in_array($arEmployee["ID"], $arResult["PREDSTAVITEL_POST"]["DELETE"]) ? ' checked' : ''?> name="predstavitel_delete[]" value="<?=$arEmployee["ID"]?>">�������</div>
                                 <div><a href="<?=$arResult["PREDSTAVITEL"]["COMPANIES"][$arEmployee["COMPANY"]]["DETAIL_PAGE_URL"]?>"><?=$arResult["PREDSTAVITEL"]["COMPANIES"][$arEmployee["COMPANY"]]["NAME"]?></a></div>
                              </div>
                              <div class="addcompany-exists-row">
                                 <div>������:</div>
                                 <div><?=$arEmployee["STATUS_TEXT"]?></div>
                              </div>
                              <!--<div class="addcompany-exists-row">
                                 <div>����������:</div>
                                 <div><input type="checkbox"<?=in_array($arEmployee["ID"], $arResult["PREDSTAVITEL_POST"]["DELETE"]) ? ' checked' : ''?> name="predstavitel_delete[]" value="<?=$arEmployee["ID"]?>">�������</div>
                              </div>-->
                           </div>
                        <?endforeach;?>
                     </div>
                     <div class="js-addcompany-newblock">
                        <?foreach($arResult["PREDSTAVITEL_POST"]["ADD"] as $employeeKey => $arEmployee):?>
                           <div class="addcompany-exists-block">
                              <div class="addcompany-exists-row">
                                 <div>��������:</div>
                                 <div><a href="<?=$arResult["COMPANY"][$arEmployee["COMPANY"]]["DETAIL_PAGE_URL"]?>"><?=$arResult["COMPANY"][$arEmployee["COMPANY"]]["NAME"]?></a></div>
                              </div>
                              <div class="addcompany-exists-row">
                                 <div>������:</div>
                                 <div><?=$arParams["PREDSTAVITEL_STATUS_TEXT"]?></div>
                              </div>
                              <div class="addcompany-exists-row">
                                 <div>����������:</div>
                                 <div><a class="js-addcompany-delete" href="javascript:void(0);">�������</a></div>
                              </div>
                              <input type="hidden" name="predstavitel_new[<?=$employeeKey?>][company]" value="<?=$arEmployee["COMPANY"]?>">
                           </div>
                        <?endforeach;?>
                     </div>
                     <div class="js-addcompany-forms" style="width: 100%; display: none;">
                        <div class="js-addcompany-company">
                           <select class="select_location js_company_sel" name="UF_COMPANY">
                              <option value="0">�������� ��������</option>
                              <? //* foreach ($arResult["COMPANY"] as $key => $val): ?>
                                 <option value="<?= $val["ID"] ?>" rel="<?=$val["DETAIL_PAGE_URL"]?>"><?= $val["NAME"] ?></option>
                              <? // endforeach; ?>
                           </select>
                        </div>
                        <div style="/*display: none;*/">
                           <input type="button" class="js-addcompany-save" disabled name="" autocomplete="off" value="��������"/>
                           <input type="button" class="js-addcompany-close" name="" autocomplete="off" value="�������"/>
                        </div>
                     </div>
                     <!--
                     <div class="js-addcompany-trigger" <?=count($arResult["PREDSTAVITEL"]["LIST"]) || count($arResult["PREDSTAVITEL_POST"]["ADD"]) ? ' style="display: none;"' : ''?>>
                        <a href="javascript:void(0)">+�������� ������</a>
                     </div> -->
                  </td>
                  <td class="bordered"><div></div></td>
               </tr>
               <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>

               <tr class="auth-last">
                  <td class="bx-sonet-profile-fieldcaption">������������� ��������</td>
                  <td class="bx-sonet-profile-field">
                     <a href="/personal/company/"><?=(is_array($arResult["MANAGED_COMPANY"]) && count($arResult["MANAGED_COMPANY"])) ? '���������� ���������' : '�������� ��������'?></a>
                  </td>
                  <td class="bordered"><div></div></td>
               </tr>
               <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
               <tr style="background-color:#ffffff;">
                  <td style="padding: 0px;"></td>
                  <td style="padding: 0px 13px;">
                     <div class="bx-sonet-profile-edit-buttons">
                        <input type="submit" name="submit" value="<? echo GetMessage('SOCNET_SUPE_TPL_SUBMIT') ?>" />
                        <input type="button" name="cancel" value="<? echo GetMessage('SOCNET_SUPE_TPL_CANCEL') ?>" onclick="location.href = '<?
                        echo htmlspecialcharsbx(CUtil::addslashes(
                                        $_REQUEST['backurl'] ? $_REQUEST['backurl'] : CComponentEngine::MakePathFromTemplate($arParams["PATH_TO_USER"], array("user_id" => $arParams["ID"]))
                                ))
                        ?>'" />
                     </div>
                  </td>
                  <td class="bordered"><div></div></td>
               </tr>
            </table>
         </div>
        <?if (substr($_REQUEST['backurl'], 0, 1) != "/")
            $_REQUEST['backurl'] = "/" . $_REQUEST['backurl'];
         ?>
      </div>
      
   </div>
</form>