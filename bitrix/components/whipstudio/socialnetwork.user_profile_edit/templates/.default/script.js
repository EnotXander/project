//���������� ������
function ClearAndReset($select, $chozen, $input){                       
   $select.find("option[value=0]").attr("selected", "selected");
   $select.attr("disabled", "disabled");
   $chozen.trigger("liszt:updated");
   $input.addClass("hidden").val("");
}

function SetManual($select, $chozen, $input){
   $select.find("option[value=-1]").attr("selected", "selected");
   $select.attr("disabled", "disabled");
   $chozen.trigger("liszt:updated");
   $input.val("").removeClass("hidden");
}

$(document).ready(function(){
   //$(".js_company_sel").chosen();
   var $selectRegionChosen = $(".js_country_sel").chosen({no_results_text: "������ �� ������� �� �������", search_contains: true}).change(function(){
      var   $this = $(this),
      val = $this.val(),
      inputVal = $this.find(":selected").html(),
      $input = $(".js_country"),
      $selectChild = $(".js_region_sel");   

      ClearAndReset($selectChild, $selectCityChosen, $(".js_region"));
      ClearAndReset($(".js_city_sel"), $selectIndexChosen, $(".js_city"));

      if(val > 0)
      {
         $input.addClass("hidden");
         $.ajax({
            type: "POST",
            url: "/_ajax/getPlace.php", 
            data: "ext_id=" + val + "&type=region",
            dataType: "json",
            success: function(data){
               $selectChild.html('');
               $.each(data.listOptionHTML, function(key, value){
                  $selectChild.append(value);               
               }); 
               $selectChild.removeAttr("disabled");
               $selectCityChosen.trigger("chosen:updated");
               $input.val(inputVal);
            }
         }); 
      } else if(val == -1)
      {
         $input.removeClass("hidden");
         $input.val("");

         SetManual($selectChild, $selectCityChosen, $(".js_region"));
         SetManual($(".js_city_sel"), $selectIndexChosen, $(".js_city"));                              
      } else
      {
         $input.val("");   
      }
   });

   var $selectCityChosen = $(".js_region_sel").chosen({no_results_text: "������ �� ������� �� �������", search_contains: true}).change(function(){
      var   $this = $(this),
      val = $this.val(),
      inputVal = $this.find(":selected").html(),
      $input = $(".js_region"),
      $selectChild = $(".js_city_sel");                                  

      ClearAndReset($selectChild, $selectIndexChosen, $(".js_city"));

      if(val > 0)
      {
         $input.addClass("hidden");
         $.ajax({
            type: "POST",
            url: "/_ajax/getPlace.php", 
            data: "ext_id=" + val + "&type=city",
            dataType: "json",
            success: function(data){
               $.each(data.listOptionHTML, function(key, value){
                  $selectChild.html('');
                  $.each(data.listOptionHTML, function(key, value){
                     $selectChild.append(value);               
                  }); 
                  $selectChild.removeAttr("disabled");
                  $selectIndexChosen.trigger("chosen:updated");
               }); 
               $input.val(inputVal);
            }
         }); 
      } else if(val == -1)
      {
         $input.removeClass("hidden");
         $input.val("");

         SetManual($selectChild, $selectIndexChosen, $(".js_city"));  
      } else
      {
         $input.val("");   
      }
   });

   var $selectIndexChosen = $(".js_city_sel").chosen({no_results_text: "������ �� ������� �� �������", search_contains: true}).change(function(){
      var   $this = $(this),
      val = $this.val(),
      inputVal = $this.find(":selected").html(),
      $input = $(".js_city");   

      if(val > 0)
      {
         $input.addClass("hidden");
         $input.val(inputVal);
         hideErrorFunc($this);
      } else if(val == -1)
      {
         $input.removeClass("hidden");
         $input.val("");
      } else
      {
         $input.val("");   
      }
   });
});



//���������
//������
function showErrorFunc($element, text){
   var $tr = $element.closest('tr');
   $tr.addClass("error");
   $tr.find('.errortext').text(text);
}
function hideErrorFunc($element){
   var $tr = $element.closest('tr');
   if($tr.hasClass("error")){
      $tr.removeClass("error");
      $tr.find('.errortext').text("");
   }
}
//��������
function fieldEmptyFunc($element){
   return $element.val().length
}
function fieldConfirmEmptyFunc($element){
   return (!((!$element.val().length) && ($('#register-form-NEW_PASSWORD').val().length)))
}
function fieldMinMaxFunc($element){
   return (($element.val().length >= 3) && ($element.val().length <= 50))
}
function fieldSymbolsFunc($element){
   return checkStringJS($element.val())
}
function fieldEmailFunc($element){
   return checkEmailJS($element.val())
}
function fieldPasswordEmptyFunc($element){
   return (!(!$element.val().length && $('#register-form-NEW_PASSWORD').val().length))
}
function fieldShortPassFunc($element){
   return (($element.val().length >= 6) || (!$element.val().length))
}
function fieldConfirmPassFunc($element){
   return ($element.val() == $('#register-form-NEW_PASSWORD').val())
}
function fieldContractFunc($element){
   return ($element.prop("checked"))
}
function fieldConfirmPlaceToBe($element){
   return ($element.val() > 0);
}
//����������
validName = validation({//���
      selector: '#register-form-NAME',
      require: [
      {
         compare: fieldEmptyFunc,
         errortext: "��� �� ���������"
      },{
         compare: fieldMinMaxFunc,
         errortext: "��� ������ ��������� �� 3 �� 50 ��������"
      },{
         compare: fieldSymbolsFunc,
         errortext: "������ ������� �������� � ����������� �������� � �����"
      }
      ],
      showError: showErrorFunc,
      hideError: hideErrorFunc
   });
validLastname = validation({//�������
   selector: "#register-form-LAST_NAME",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "������� �� ���������"
   },{
      compare: fieldMinMaxFunc,
      errortext: "������� ������ ��������� �� 3 �� 50 ��������"
   },{
      compare: fieldSymbolsFunc,
      errortext: "������ ������� �������� � ����������� �������� � �����"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});
validEmail = validation({//�����
   selector: "#register-form-LOGIN",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "����� ����������� ����� �� ��������"
   },{
      compare: fieldEmailFunc,
      errortext: "������������ ����� ����������� �����"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});
validPassword = validation({
   selector: "#register-form-PASSWORD",
   require: [
   {
      compare: fieldPasswordEmptyFunc,
      errortext: "������� ������ �� ��������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});
validNewPassword = validation({
   selector: "#register-form-NEW_PASSWORD",
   require: [
   {
      compare: fieldShortPassFunc,
      errortext: "����������� ����� ������ - 6 ��������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});
validConfirm = validation({
   selector: "#register-form-CONFIRM_PASSWORD",
   require: [
   {
      compare: fieldConfirmEmptyFunc,
      errortext: "��������� ������"
   },{
      compare: fieldConfirmPassFunc,
      errortext: "������ �� ���������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});
validPlace = validation({
   selector: ".js_city_sel",
   require: [
   {
      compare: fieldConfirmPlaceToBe,
      errortext: "������� ����� ����������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});

$(document).ready(function(){
   //���������
   //���
   $('#register-form-NAME').live("focusout", function(){
      //console.log("validName", validName);
      validName();
   });
   //�������
   $('#register-form-LAST_NAME').live("focusout", function(){
      validLastname();
   });
   //�����
   $('#register-form-LOGIN').live("focusout", function(){
      validEmail();
   });
   //Password
   $('#register-form-PASSWORD').live("focusout", function(){
      validPassword();
   });
   //New Password
   $('#register-form-NEW_PASSWORD').live("focusout", function(){
      validPassword();
      validNewPassword();
      validConfirm();
   });
   //Confirm Password
   $('#register-form-CONFIRM_PASSWORD').live("focusout", function(){
      validConfirm();
   });
            
   //������
   $('#user_profil_edit_form').live("submit", function(){
      var $this = $(this);
      var $form = $this.closest('form');
      //�������� ������
      validName();
      validLastname();
      validEmail();
      validPassword();
      validNewPassword();
      validConfirm();
      //validPlace();
      if($form.find('.error').size()) return false;
   });
});


//���������� ��������
$(document).ready(function(){
   var addCompanyInit = false;//������������������ �� chosen
   var addPredstavitelInit = false;//������������������ �� chosen
   var addCompanyCount = $('.js-addcompany-newblock .addcompany-exists-block').size();//������������ ������� ����������� ��������
   var addCompanyEnable = function($forms){
      if(~~$forms.find('.js_company_sel').val() && (~~$forms.find('.js_position_sel').val() || $forms.find('.js-addcompany-position-manual input').val().length)){
         $forms.find('.js-addcompany-save').removeAttr("disabled");
         return true;
      }else{
         $forms.find('.js-addcompany-save').attr("disabled", "disabled");
         return false;
      }
   }
   var addPredstavitelEnable = function($forms){
      if(~~$forms.find('.js_company_sel').val()){
         $forms.find('.js-addcompany-save').removeAttr("disabled");
         return true;
      }else{
         $forms.find('.js-addcompany-save').attr("disabled", "disabled");
         return false;
      }
   }
   
   //�������� ����� ���������� ��������
   $('.js-addcompany-trigger a').live("click", function(){
      var $this = $(this);
      var $global = $(this).closest('.js-add');
      var $forms = $this.closest('.js-addcompany-block').find('.js-addcompany-forms');
      $this.hide();
      $forms.show()
      
      if($global.hasClass("js-addcompany")){
         if(addCompanyInit == false){
            $forms.find('.js_company_sel, .js_position_sel').chosen({no_results_text: "������ �� ������� �� �������", search_contains: true}).change(function(event){
               addCompanyEnable($forms);
            });
            addCompanyInit = true;
         }else{
            $forms.find('.js_company_sel, .js_position_sel, .js-addcompany-position-manual input').val('').trigger("chosen:updated").end()
               .find('.js-addcompany-save').attr("disabled", "disabled");
         }
      }
      else if($global.hasClass("js-addpredstavitel")){
         if(addPredstavitelInit == false){
            $forms.find('.js_company_sel, .js_position_sel').chosen({no_results_text: "������ �� ������� �� �������", search_contains: true}).change(function(event){
               addPredstavitelEnable($forms);
            });
            addPredstavitelInit = true;
         }else{
            $forms.find('.js_company_sel, .js_position_sel, .js-addcompany-position-manual input').val('').trigger("chosen:updated").end()
               .find('.js-addcompany-save').attr("disabled", "disabled");
         }
      }
   });
   
   //������������� ������ �������� ����� ����� ���������
   $('.js-addcompany .js-addcompany-position-manual input').live("keypress focusout", function(){
      addCompanyEnable($(this).closest('.js-addcompany-forms'));
   });
   
   //�������� ����� ���������� ��������
   $('.js-addcompany-close').live("click", function(){
      $(this).closest('.js-addcompany-forms').hide().end()
         .find('.js_company_sel, .js_position_sel, .js-addcompany-position-manual input').val('').trigger("chosen:updated").end()
         .closest('.js-addcompany-block').find('.js-addcompany-trigger a').show();
   });
   
   //���������� �������� �� ����� � ������
   $('.js-addcompany .js-addcompany-save').live("click", function(){
      var $this = $(this);
      var $block = $this.closest('.js-addcompany-block');
      var companyInfo = {
            companyId: $block.find('.js_company_sel').val(),
            positionId: $block.find('.js_position_sel').val(),
            companyName: $block.find('.js_company_sel option[value='+$block.find('.js_company_sel').val()+']').text(),
            positionName: $block.find('.js_position_sel option[value='+$block.find('.js_position_sel').val()+']').text(),
            manual: $block.find('.js-addcompany-position-manual input').val(),
            positionDisplayed: ($block.find('.js-addcompany-position-manual input').val().length ? $block.find('.js-addcompany-position-manual input').val() : $block.find('.js_position_sel option[value='+$block.find('.js_position_sel').val()+']').text()),
            link: $block.find('.js_company_sel option[value='+$block.find('.js_company_sel').val()+']').attr("rel")
         }
      $block.find('.js-addcompany-newblock').append(
         $('<div class="addcompany-exists-block js-addcompany-new">\n\
<div class="addcompany-exists-row"><div>��������:</div><div class="addcompany-delete-place"><a class="js-addcompany-delete" href="javascript:void(0);">X �������</a></div><div><a href="'+companyInfo.link+'">'+companyInfo.companyName+'</a></div></div>\n\
<div class="addcompany-exists-row"><div>���������:</div><div>'+companyInfo.positionDisplayed+'</div></div>\n\
<div class="addcompany-exists-row"><div>������:</div><div>'+$block.attr("data-status-text")+'</div></div>\n\
<input type="hidden" name="employee_new['+addCompanyCount+'][company]" value="'+companyInfo.companyId+'">\n\
<input type="hidden" name="employee_new['+addCompanyCount+'][position]" value="'+companyInfo.positionId+'">\n\
<input type="hidden" name="employee_new['+addCompanyCount+'][manual]" value="'+companyInfo.manual+'">\n\
</div>')
      ).end()
         .find('.js-addcompany-trigger a').show().end()
         .find('.js-addcompany-forms').hide()
            .find('.js_company_sel, .js_position_sel, .js-addcompany-position-manual input').val('').trigger("chosen:updated");
      addCompanyCount++;
   });
   $('.js-addpredstavitel .js-addcompany-save').live("click", function(){
      var $this = $(this);
      var $block = $this.closest('.js-addcompany-block');
      var companyInfo = {
            companyId: $block.find('.js_company_sel').val(),
            companyName: $block.find('.js_company_sel option[value='+$block.find('.js_company_sel').val()+']').text(),
            link: $block.find('.js_company_sel option[value='+$block.find('.js_company_sel').val()+']').attr("rel")
         }
      $block.find('.js-addcompany-newblock').append(
         $('<div class="addcompany-exists-block js-addcompany-new">\n\
<div class="addcompany-exists-row"><div>��������:</div><div class="addcompany-delete-place"><a class="js-addcompany-delete" href="javascript:void(0);">X �������</a></div><div><a href="'+companyInfo.link+'">'+companyInfo.companyName+'</a></div></div>\n\
<div class="addcompany-exists-row"><div>������:</div><div>'+$block.attr("data-status-text")+'</div></div>\n\
<input type="hidden" name="predstavitel_new[company]" value="'+companyInfo.companyId+'">\n\
</div>')
      ).end()
         //.find('.js-addcompany-trigger a').show().end()//����� �������� ������ ������ �������������
         .find('.js-addcompany-forms').hide()
            .find('.js_company_sel, .js_position_sel, .js-addcompany-position-manual input').val('').trigger("chosen:updated");
   });
   
   //�������� ������ ��� ����������� ��������
   $('.js-addcompany-delete').live("click", function(){
      var $global = $(this).closest('.js-add');
      $(this).closest('.js-addcompany-new').remove();
      if($global.hasClass("js-addpredstavitel")){
         $global.find('.js-addcompany-trigger a').show();
      }
   });
   
   //������ �����
   $('#user_profil_edit_form').live("submit", function(){
      //����������
      if(addCompanyEnable($(this).find('.js-addcompany .js-addcompany-forms')))
         $(this).find('.js-addcompany .js-addcompany-save').trigger("click");
      else
         $(this).find('.js-addcompany .js-addcompany-close').trigger("click");
      //�������������
      if(addPredstavitelEnable($(this).find('.js-addpredstavitel .js-addcompany-forms')))
         $(this).find('.js-addpredstavitel .js-addcompany-save').trigger("click");
      else
         $(this).find('.js-addpredstavitel .js-addcompany-close').trigger("click");
   });
});