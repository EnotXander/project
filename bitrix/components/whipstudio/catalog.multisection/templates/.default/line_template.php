<?php
/**
 * User: ZvvAsyA
 * Date: 13.08.15 -  17:45
 */

function renderLine($arResult, $arList, $listKey,  $arParams ){
	global $USER;

	//PrintAdmin($arResult["COMPANY_DETAIL"]);
	?>

	<li class="js-section_list">

	                  <?if(count($arResult["LISTS"]) > 1):?>
	                     <div id="tab<?=$listKey+1?>" data-category="<?=$arList['ID'];?>" class="h3-title ajax-catalog">
		                     <?=$arList["NAME"]?>
	                         <sup><?=$arList["ITEMS_COUNT"]?> </sup>
	                     </div>
	                  <?endif;?>
	                  <ul class="accordion-content" id="AC<?= $arList['ID']; ?>">
	                     <div class="cat_row_wrapblock">

	                        <div class="cat_row_rowsblock  my-box-<?=$arList['ID']?>">
	                           <?foreach ($arList["GROUPS"] as $arGroup):?>
	                              <div class="cat_3_row js-hideitems-group">
	                                 <div class="cat_row_left ">

	                                    <?foreach ($arGroup["ITEMS"] as $arItemKey => $arItem):?>
											<?
		                                    $arItem["PROPERTY_COST_VALUE"] = str_replace(' ', '', $arItem["PROPERTY_COST_VALUE"]);

		                                    ?>
	                                       <div class="cat_row <? if( $arResult["COMPANY_DETAIL"][$arGroup["COMPANY"]]['PREMIUM'] == '��') {
	                                           ?>premium<?
	                                       } ?><?=$arItem["HIDE"] ? ' item_hide' : ''?>">
	                                          <table>
	                                             <tbody>
	                                                <tr>
	                                                   <td class="ic_star" style="vertical-align:top;">
	                                                      <?if($USER->IsAuthorized()):?>
	                                                         <a href="javascript:void(0)" onclick="AddToFavorites({
	                                                                     productId: <?=$arItem["ID"]?>,
	                                                                    context: $(this).find('i'),
	                                                                    inFavorites: function(){this.addClass('marked')},
	                                                                    outFavorites: function(){this.removeClass('marked')}
	                                                         })">
	                                                            <i class="i_star<? if(in_array($USER->GetID(), $arItem["PROPERTY_FAVORITES_VALUE"])):?> marked<? endif; ?>"></i>
	                                                         </a>
	                                                      <?endif;?>
	                                                   </td>
	                                                   <td class="image_box">
	                                                      <a title="���������� ���������" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
	                                                         <img title="������ <?=$arItem["NAME"]?>"  alt="<?=$arItem["NAME"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>">
	                                                      </a>
	                                                   </td>
	                                                   <td class="cat_info">
	                                                      <div class="tdWrap">
	                                                         <div class="cat_title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?> </a></div>
	                                                         <?if($arGroup["COMPANY"]):?>
	                                                            <div class="cat_text"><?=$arItem["PROPERTY_FIRM_NAME"]?></div>
	                                                         <?endif;?>
		                                                      <noindex>
	                                                            <div class="meta"><?=$arItem["PREVIEW_TEXT"]?></div>
		                                                      </noindex>
	                                                      </div>
	                                                   </td>
	                                                   <td class="cat_price">
	                                                      <div class="tdWrap">
	                                                         <?if($arItem["PROPERTY_COST_VALUE"]):?>
	                                                            <div class="sum">
		                                                            <?= number_format(
			                                                            $arItem['PROPERTY_COST_VALUE'],
                                                                        ($arItem['PROPERTY_CURRENCY_VALUE']=='EUR' || $arItem['PROPERTY_CURRENCY_VALUE']=='USD' ?1:0),
                                                                        ',', ' ');
                                                                    ?>
	                                                            </div>
	                                                            <div class="curr-val"><?=$arItem["PROPERTY_CURRENCY_FORMATED"]?></div>
	                                                         <?else:?>

		                                                         <img alt="���� ���������" src="/bitrix/templates/energorb/images/pricedetail.png" width="77" height="28">
	                                                         <?endif;?>
		                                                      <noindex>
	                                                            <a href="#" rel="nofollow" class="buy-button" onclick="javascript:showPopup({id: $('.fast_order_popup'), productID: <?=$arItem["ID"]?>, sender: $(this)}); return false;"></a>
		                                                      </noindex>
	                                                      </div>
	                                                   </td>
	                                                </tr>
	                                                <?if(($arItem["HIDE_PANEL"]) && $arGroup["ITEMS"][$arItemKey+1]["HIDE"]):?>
	                                                   <tr>
	                                                      <td colspan="3">
	                                                         <div class="show_position">
	                                                            <a href="javascript:void(0)" class="js-hideitems-show" data-more="<?=$arParams["COMPANY_ELEMENT_MORE_COUNT"]?>">
	                                                               <i class="plus_icon"></i>
	                                                               <span>�������� <?=$arParams["COMPANY_ELEMENT_MORE_COUNT"] ? "���"/*." {$arParams["COMPANY_ELEMENT_MORE_COUNT"]} ������"*/ : " ��� �������"?></span></a>
	                                                         </div>
	                                                      </td>
	                                                   </tr>
	                                                <?endif;?>
	                                             </tbody>
	                                          </table>
	                                       </div><!--ads_row-->
	                                    <?endforeach;?>
	                                 </div>

	                                 <?if(!empty($arGroup["COMPANY"])):
	                                    $arCompany = $arResult["COMPANY_DETAIL"][$arGroup["COMPANY"]];?>
	                                    <div class="cat_row_right ">
	                                       <div class="cat_row_rheader <?= $arCompany['LIQUIDATED'] == '��' ? 'LIQUIDATED' : '' ?>" onclick="window.open('<?=$arCompany["DETAIL_PAGE_URL"]?>','_blank'); return false;">
	                                          <?if(is_array($arCompany["PREVIEW_PICTURE"])):?>
	                                             <img src="<?=$arCompany["PREVIEW_PICTURE"]["SRC"]?>" alt="" />
	                                          <?else:?>
	                                             <img src="/images/search_placeholder.png" />
	                                          <?endif;?>
	                                          <span class="company-name"<?=(strlen($arCompany["NAME_CUT"]) != strlen($arCompany["NAME"])) ? " title='".$arCompany["NAME"]."'" : ''?>><?=$arCompany["NAME_CUT"]?><sup><?=$arCompany["ITEMS_COUNT"][$arList["ID"]]?></sup></span>
	                                          <span class="after"></span>
	                                       </div>
	                                       <div class="cat_row_rcontent">
	                                          <!--<a class="show" href="javascript:void(0)" onClick="jQuery(this).parent().find('.all_company_info').toggle();">�������� �������� <i></i></a>-->
	                                          <div class="all_company_info" id="allCompanyInfoID" ><noindex>
	                                             <a class="btns" rel="nofollow" style="margin-top: 10px;" href="<?=$arCompany["PREDSTAVITEL_MESSAGE_LINK"]?>">
		                                             <i></i>��������
	                                             </a></noindex>
	                                             <a class="c_name" href="<?=$arCompany["DETAIL_PAGE_URL"]?>">�������� ��������</a>
		                                          <noindex>
		                                             <ul class="js-phone-hide-block" data-company="<?=$arCompany['ID']?>">
		                                                <?foreach ($arCompany["PROPERTY_phone"] as $phone):?>
		                                                   <li>
		                                                      <span class="js-phone-code">+375</span>
		                                                      <span class="phone-hide js-phone-hide">�������� �������</span>
		                                                      <span class="phone-show"><?= phone($phone["VALUE"])?></span>
		                                                   </li>
		                                                <?endforeach;?>
		                                                <?foreach ($arCompany["PROPERTY_phone_VALUE"] as $phone):?>
		                                                   <li>
		                                                      <span class="js-phone-code">+375</span>
		                                                      <span class="phone-hide js-phone-hide">�������� �������</span>
		                                                      <span class="phone-show"><?= phone($phone)?></span>
		                                                   </li>
		                                                <?endforeach;?>
		                                                <?foreach ($arCompany["PROPERTY_URL"] as $url):?>
		                                                   <li><a class="c_site" rel='nofollow' href="<?=$url["VALUE_HREF"]?>"><?=$url["VALUE"]?></a></li>
		                                                <?endforeach;?>
		                                             </ul>
		                                          </noindex>
	                                             <a class="c_mess"  rel="nofollow"  href="javascript:void(0);">
	                                                <div>
	                                                   �������� ����������,<br> ��� ����� ��� �� <?= (LANG == 's1' ? 'EnergoBelarus.by' : 'AgroBelarus.by')?>
	                                                </div>
	                                             </a>
	                                          </div>
	                                       </div>
	                                    </div>
	                                 <?endif;?>
	                                 <div class="clear"></div>
	                              </div>
	                           <?endforeach;?>

	                              <?= $arList["NAV_STRING"] ?>

	                        </div>

	                     </div>

	                  </ul>
	               </li>
	<?
}

