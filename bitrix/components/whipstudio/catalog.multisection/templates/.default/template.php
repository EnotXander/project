<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include ('line_template.php');

global $GBL_SHOW_FAST_ORDER;
$GBL_SHOW_FAST_ORDER = true;
?>
<span id="CURPAGE"  data-type="<?= $arParams['IBLOCK_ID'] == IBLOCK_USLUGI ? 'uslugi' : 'catalog' ?>" data-id="<?=$_GET['CURPAGE']?>" data-url='<?= serialize($_GET)?>'></span>
<div class="left_col">
   <?if(count($arResult["LISTS"])>1):?>
      <div class="lc_block">
         <div class="title">���������</div>
         <ul class="left_menu">
            <?foreach ($arResult["LISTS"] as $arList):?>
               <li><a href="<?=$arList["SECTION_PAGE_URL"]?>"><?=$arList["NAME"]?></a></li>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
   <?if(!empty($arResult["COMPANY_LIST"])):?>

      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?foreach ($arResult["COMPANY_LIST"] as $arCompany):?>
               <?if(!$arCompany['REMOVE_REL']):?>
                  <li><a href="<?=$arCompany["DETAIL_PAGE_URL"]?>"><?=$arCompany["NAME"]?></a></li>
               <?endif;?>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
   <?if(count($arResult["BRANDS_LIST"])):?>
      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?foreach ($arResult["BRANDS_LIST"] as $arBrands):?>
               <li><a href="<?=$arBrands["DETAIL_PAGE_URL"]?>"><?=$arBrands["NAME"]?></a> <img src="<?=$arBrands["PREVIEW_PICTURE"]?>"> </li>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
	
	<!-- ad E4-1 -->
	<div class="lc_block">
      <div class="left_banner_185">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E4_1",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E4-2 -->
	<div class="lc_block">
      <div class="left_banner_185">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E4_2",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
</div>

<div class="middle_col">
   <h1><?=$arResult["THIS_SECTION"]["NAME"]?></h1>
   <div class="mc_block">
		<!-- ad E1 -->
      <div class="mc_block_banner">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "MARKET_LIST_TOP",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>

      <ul class="mb_menu ml">
         <li class="sel">
            <a href="#"><i class="icons icon_list list_sel"></i>������</a>
            <div class="sclew"></div>
         </li>
         <?if($USER->IsAuthorized()):?>
            <li><noindex>
	               <a rel="nofollow" href="<?=$APPLICATION->GetCurPage()."?FAVORITES=Y"?>"><i class="icons icon_star star_sel"></i>���������</a>
	               <div class="sclew"></div>
	            </noindex>
            </li>
         <?endif;?>
      </ul>
      <!--<div class="search-box">
         <input type="text" name="" id="" />
         <a href="#">�����</a>
      </div>-->
      <div class="clear"></div>
      <div class="paging brdr_bot_0 brdr_box topP">
          <noindex>
              <div class="show_col">
                <form method="get" name="top_f">
                   <span class="meta">���������� ��:</span>
                   <select name="per_page" onchange="document.top_f.submit()">
                      <option value="20"<?=$arParams["PAGE_ELEMENT_COUNT"]==20 ? 'selected' : ''?>>20</option>
                      <option value="50"<?=$arParams["PAGE_ELEMENT_COUNT"]==50 ? 'selected' : ''?>>50</option>
                      <option value="100"<?=$arParams["PAGE_ELEMENT_COUNT"]==100 ? 'selected' : ''?>>100</option>
                   </select>
                </form>
             </div>
             <?if(count($arResult["SORT"])):?>
                <div class="sort_col_pdr">
                   <?foreach ($arResult["SORT"] as $sortBy => $arSort):?>
                      <div class="<?=$sortBy?>_col">
                         <a rel="nofollow" <?=$arSort["ACTIVE"] ? ' class="arrows-active"' : ''?>
                            href="<?=$arSort["LINK"]?>"><?=$arSort["NAME"]?>
                             <i class="sort-arrows<?=$arSort["ARROW"] ? ' arrows-reverse' : ''?>"></i>
                         </a>
                      </div>
                   <?endforeach;?>
                </div>
             <?endif;?>
          </noindex>
			<!-- ad E3 -->
			<?$APPLICATION->IncludeComponent("bitrix:advertising.banner","market-popup",array(
				"TYPE" => "E3",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0",
				"CACHE_NOTES" => ""
			));?>
      </div><!--paging-->

      <?if(count($arResult["LISTS"])):?>
         <?if(count($arResult["LISTS"]) > 1):?>
            <script>
               $(function(){
                  $(".accordion_block").accordion({
                     heightStyle: "content",
                     active: parseInt($('.accordion_block').attr("data-active"))
                  });
               });
            </script>
         <?endif;?>
         <ul class="accordion_block" data-active="<?=$arResult["ACCORDION_ACTIVE"]?>">
            <?foreach ($arResult["LISTS"] as $listKey => $arList):?>
                <? renderLine($arResult, $arList, $listKey, $arParams ) ?>
            <?endforeach;?>
         </ul>
      <?endif;?>

      <div class="mc_block_banner bottom"><!-- ad MARKET_LIST_BOTTOM --></div>
		
   </div>
   <? if (!isset($_REQUEST['PAGEN']) ) {?>


       <?if(($arParams['SHOW_SEO_TEXT'] == 'Y') && strlen($arResult["THIS_SECTION"]["DESCRIPTION"]) ):?>
          <div class="rc_block">
             <div class="rc_block_info rc_block_description">
                <div><?=$arResult["THIS_SECTION"]["DESCRIPTION"]?></div>
             </div>
          </div>
       <?endif;?>

   <? } ?>

</div>
<div class="content_right_col">
   <?if($arParams["SHOW_RUBRIKATOR"] == "Y"):?>
      <div class="rc_block">
         <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "filament", Array(
                         "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                         "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                         "SECTION_ID" => $arResult["THIS_SECTION"]["ID"],
                         "SECTION_CODE" => "",
                         "SECTION_URL" => "",
                         "COUNT_ELEMENTS" => "Y",
                         "TOP_DEPTH" => "10",
                         "SECTION_FIELDS" => "",
                         "SECTION_USER_FIELDS" => "",
                         "ADD_SECTIONS_CHAIN" => "Y",
                         "CACHE_TYPE" => "N",
                         "CACHE_TIME" => "36000000",
                         "CACHE_NOTES" => "",
                         "CACHE_GROUPS" => "Y"
                 )		
         );?>
      </div>
   <?endif;?>
	<!-- ad E2-1 -->
   <div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "market-rightcol", Array(
               "TYPE" => "MARKET_LIST_RIGHT",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-2 -->
   <div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_2",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-3 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_3",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-4 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_4",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-5 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_5",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	
	<!-- ad E2-6 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_6",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>

   <div class="rc_block">
      <div class="right_banner_220">

<div id="yandex_ad"></div>
<script type="text/javascript">
(function(w, d, n, s, t) {
    w[n] = w[n] || [];
    w[n].push(function() {
Ya.Direct.insertInto(window.location.hostname=='agrobelarus.by'?135492:135481, "yandex_ad", {
            ad_format: "direct",
            type: "adaptive",
            border_type: "block",
            limit: 3,
            title_font_size: 3,
            border_radius: true,
            links_underline: true,
            site_bg_color: "FFFFFF",
            border_color: "CCCCCC",
            title_color: "266BAB",
            url_color: "000099",
            text_color: "000000",
            hover_color: "0B9DF1",
            sitelinks_color: "0000CC",
            favicon: true,
            no_sitelinks: false,
            height: 500,
            width: 220
        });
    });
    t = d.getElementsByTagName("script")[0];
    s = d.createElement("script");
    s.src = "//an.yandex.ru/system/context.js";
    s.type = "text/javascript";
    s.async = true;
    t.parentNode.insertBefore(s, t);
})(window, document, "yandex_context_callbacks");
</script>
      </div>
   </div>
</div>