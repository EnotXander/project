<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
}
if ( ! CModule::IncludeModule( "iblock" ) ) ;


include ( $_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/tools/companies.php');
include ( 'functions.php');

global $USER;
global $APPLICATION;
global $CACHE_MANAGER;
$obParser = new CTextParser;


//���������� ������
global ${$arParams["FILTER_NAME"]};
$arrFilter = ${$arParams["FILTER_NAME"]};
if ( ! is_array( $arrFilter ) ) {
	$arrFilter = array();
}
$arParams["COMPANY_ELEMENT_MORE_COUNT"] = (int) $arParams["COMPANY_ELEMENT_MORE_COUNT"];
$arParams["CURPAGE"]                    = (int) $_REQUEST["CURPAGE"] ? (int) $_REQUEST["CURPAGE"] : 0;

$arResult = array();

//*****************************************************************************************************************
//
//          �������
//
//*****************************************************************************************************************

//�������� ���������� �� ������� �������
if ( ! $arParams["SECTION_ID"] && ! strlen( $arParams["SECTION_CODE"] ) ) {
	ShowError( "������ �� ������" );

	return;
}
$thisSectionFilter              = array();
$thisSectionFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
if ( $arParams["SECTION_ID"] ) {
	$thisSectionFilter["ID"] = $arParams["SECTION_ID"];
} else {
	$thisSectionFilter["=CODE"] = $arParams["SECTION_CODE"];
}
$rsThisSection = CIBlockSection::GetList(
	array(),
	$thisSectionFilter,
	false,
	array(
		$arParams["BROWSER_TITLE"],
		$arParams["META_DESCRIPTION"],
		$arParams["META_KEYWORDS"]
	)
);
$rsThisSection->SetUrlTemplates( "", $arParams["SECTION_URL"] );
if ( ! $arResult["THIS_SECTION"] = $rsThisSection->GetNext() ) {
	ShowError( "������ {$arParams["SECTION_ID"]} �� ������" );

	return;
}

//�������� ��������� �������

$arResult["LISTS"] = cacheFunction( 36000, 'LISTS' . $arResult["THIS_SECTION"]["ID"] . $arParams["IBLOCK_ID"], function () use ( $arParams, $arResult ) {

	$res = array();

	$arFilter = Array( 'ID' => $arResult["THIS_SECTION"]["ID"] , 'ELEMENT_SUBSECTIONS' => 'N',
			'CNT_ACTIVE'          => 'Y');
	$db_list  = CIBlockSection::GetList( Array(), $arFilter, true );

	if ( $ar_result = $db_list->GetNext() ) {

		if ( $ar_result['ELEMENT_CNT'] > 0 ) {
			$res[] = $arResult["THIS_SECTION"]; //
		}
	}

	if ( $arParams["INCLUDE_SUBLISTS"] ) {
		$rsSublistSection = CIBlockSection::GetList(
			array( "SORT" => "ASC", "NAME" => "ASC" ),
			array(
				"IBLOCK_ID"  => $arParams["IBLOCK_ID"],
				"ACTIVE"     => "Y",
				"SECTION_ID" => $arResult["THIS_SECTION"]["ID"]
			), false, array( 'NAME', 'ID', 'SECTION_PAGE_URL' )
		);

		$rsSublistSection->SetUrlTemplates( "", $arParams["SECTION_URL"] );
		while ( $arSublistSection = $rsSublistSection->GetNext() ) {
			$res[] = $arSublistSection;
		}
	}


	return $res;
} );


//*****************************************************************************************************************
//
//          ����������
//
//*****************************************************************************************************************

$arResult["SORT"] = array();
//�� ���������
$sortDefaultBy = $arParams["SORT_DEFAULT"];
$arSortDefault = $arParams["SORT"];
//��������� �� ����
$sort_by = $_REQUEST["sort_by"];
if ( ! strlen( $sort_by ) ) {
	$sort_by = $sortDefaultBy;
}
$sort_type = urldecode( $_REQUEST["sort_type"] );
if ( ! strlen( $sort_type ) ) {
	$sort_type = $arSortDefault[ $sortDefaultBy ]["TYPE"];
}
//���������
$arSortArrow   = array(
	"asc"  => true,
	"desc" => false,
);
$arSortReverse = array(
	"asc"        => "desc",
	"desc"       => "asc",
	"asc,nulls"  => "desc,nulls",
	"desc,nulls" => "asc,nulls"
);

//������
foreach ( $arSortDefault as $sort => $sortDefault ) {
	//���
	$arResult["SORT"][ $sort ]["NAME"] = $arSortDefault[ $sort ]["NAME"];
	//������������ ������
	if ( $sort_by == $sort ) {
		$arResult["SORT"][ $sort ]["ACTIVE"] = true;
		if ( $sort_type == $sortDefault["TYPE"] ) {
			$sort_type_link = $arSortReverse[ $sortDefault["TYPE"] ];
		} else {
			$sort_type_link = $sortDefault["TYPE"];
		}
	} else {
		//$arResult["SORT"][$sort]["ARROW"] = $arSortArrow[$sortDefault["TYPE"]];
		$sort_type_link = $sortDefault["TYPE"];
	}
	$sort_type_link                    = urlencode( $sort_type_link );
	$arResult["SORT"][ $sort ]["LINK"] = $APPLICATION->GetCurPageParam( "sort_by={$sort}&sort_type={$sort_type_link}", array(
		"SECTION_ID",
		"sort_by",
		"sort_type"
	) );
	//�������
	$arResult["SORT"][ $sort ]["ARROW"] = $arSortArrow[ $arSortReverse[ $sort_type_link ] ];
}

//������� ���������� ��� GetList
$arResult["SORT_BY"] = $sort_by;
foreach ( $arSortDefault as $code => $value ) {
	if ( $sort_by == $code ) {
		$sort_by = $value["CODE"];
	}
}
$arResult["SORT_FOR_ITEMS"] = array(
	$sort_by => $sort_type,
	//"SORT" => "ASC",
	//"NAME" => "ASC"
);


//*****************************************************************************************************************
//
//          ��������
//
//*****************************************************************************************************************

//��������� ��������, ������� ������� � ���������
$arResult["COMPANY_LIST"] = array();
$arResult["BRANDS_LIST"]  = array();
$arResult["SORT_COMPANY"] = array(
	"ITEMS_IDS"   => array(),
	"COMPANY_IDS" => array()
);



$cmp = new Companies();
$arResult["COMPANY_LIST"] = $cmp->CompaniesInSection( $arParams["IBLOCK_ID"], $arResult["THIS_SECTION"]["ID"] );
//*****************************************************************************************************************
//
//          ��������
//
//*****************************************************************************************************************

//�������� �������� ������� �������� �� ������� ������
$arResult["COMPANY_DETAIL"] = array();

if ( count( $arResult["LISTS"] ) == 1 ) {

	unset( $arParams["SHOW_GOODS"] );

}

//$arResult["LISTS"] ������
if ( count($arResult["LISTS"]) == 1 ) { // ���� ��������� �������
	foreach ( $arResult["LISTS"] as $arListKey => $arList ) {
		if ( ( $arList["ID"] == $arResult["THIS_SECTION"]["ID"] ) && ( count( $arResult["LISTS"] ) > 1 ) ) {
			$arList["NAME"] = "��� � �������";
		}
		if ( $arList["NAME"] == "��� � �������" || count( $arResult["LISTS"] ) == 1 ) {
			if ( $arResult["SORT_BY"] != "company" ) {
				//����������

				$arFilter               = array();
				$arFilter["IBLOCK_ID"]  = $arParams["IBLOCK_ID"];
				$arFilter["ACTIVE"]     = "Y";
				$arFilter["!=PROPERTY_ACTIVE"] = "N";

				$arFilter["SECTION_ID"] = $arList["ID"];
				if ( ( $arList["ID"] == $arResult["THIS_SECTION"]["ID"] ) && ( count( $arResult["LISTS"] ) > 1 ) ) {
					$arList["NAME"] = "��� � �������";
				} else {
					$arFilter["INCLUDE_SUBSECTIONS"] = true;
				}

				//���������
				//$GLOBALS["NavNum"] = $arList["ID"];
				$arNavParams = array();//**************
				$arNavParams = array(
					"nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
					"bShowAll"  => $arParams["PAGER_SHOW_ALL"],
				);
				if ( (int) $_REQUEST["PAGEN"] && (int) $_REQUEST["CURPAGE"] == $arList["ID"] ) {
					$arNavParams["iNumPage"] = (int) $_REQUEST["PAGEN"];
				} else {
					$arNavParams["iNumPage"] = 1;
				}
			} else { // ���������� �� ���������
				//����������
				$arFilter = array(
//					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
//					"ACTIVE"    => "Y",
					"ID"        => array_keys( $arResult["COMPANY_LIST"] )
				);

		//���������
		$GLOBALS["NavNum"] = $arList["ID"];
		$arNavParams = array();//**************
		$arNavParams = array(
			"nPageSize" => ceil( ( $arParams["PAGE_ELEMENT_COUNT"] / $arParams["COMPANY_ELEMENT_COUNT"] ) * 1 ),
			"bShowAll"  => $arParams["PAGER_SHOW_ALL"],
		);
		if ( (int) $_REQUEST["PAGEN"] ) {
			$arNavParams["iNumPage"] = (int) $_REQUEST["PAGEN"];
		} else {
			$arNavParams["iNumPage"] = 1;
		}

		//��������� ������ ��������
		$arResult["COMPANY_ONPAGE"] = array();
		$rsCompanyOnPage            = CIBlockElement::GetList(
			array(
				"PROPERTY_EXT_SORT" => $sort_type,
				"SORT" => "asc"
			),
			$arFilter,
			false,
			$arNavParams,
			array(
				"ID",
				"NAME"
			)
		);
		$rsCompanyOnPage->SetUrlTemplates( "", $arParams["DETAIL_URL"] );
		while ( $arCompanyOnPage = $rsCompanyOnPage->GetNext() ) {
			$arResult["COMPANY_ONPAGE"][ $arCompanyOnPage["ID"] ] = $arCompanyOnPage;
		}

		//������ ���������
		$rsCompanyOnPage->NavNum   = $arList["ID"];
		$arList["NAV_STRING"]      = $rsCompanyOnPage->GetPageNavStringEx( $navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"] );
	    $arList["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();

				//���������� ������
				$arResult["SORT_FOR_ITEMS"] = array(
					"PROPERTY_FIRM.PROPERTY_EXT_SORT" => $sort_type,
					"SORT"                  => "ASC",
					"NAME"                  => "ASC"
				);

				//���������� ������
				$arFilter                  = array();
				$arFilter["IBLOCK_ID"]     = $arParams["IBLOCK_ID"];
				$arFilter["ACTIVE"]        = "Y";
				$arFilter["!=PROPERTY_ACTIVE"] = "N";

				$arFilter["SECTION_ID"]    = $arList["ID"];
				$arFilter["PROPERTY_FIRM"] = array_keys( $arResult["COMPANY_ONPAGE"] );
				if ( ( $arList["ID"] == $arResult["THIS_SECTION"]["ID"] ) && ( count( $arResult["LISTS"] ) > 1 ) ) {
					$arList["NAME"] = "��� � �������";
				} else {
					$arFilter["INCLUDE_SUBSECTIONS"] = true;
				}

				//��������� ������
				$arNavParams = array();//**************
				$arNavParams = false;
			}

			$CM_SORTED = array_keys($arResult["COMPANY_ONPAGE"]);

			//��������� ������� � �������
			$arList["GROUPS"] = array();
			$groupIndex       = - 1;//����� ��������������� �� ��������� ������� � ������

			$rsElements = CIBlockElement::GetList(
				$arResult["SORT_FOR_ITEMS"],
				array_merge( $arrFilter, $arFilter ),
				false,
				$arNavParams,
				array(
					"ID",
					"NAME",
					"PREVIEW_PICTURE",
					"PREVIEW_TEXT",
					"DETAIL_PICTURE",
					"DETAIL_TEXT",
					"DETAIL_PAGE_URL",
					"PROPERTY_COST",
					"PROPERTY_CURRENCY",
					"PROPERTY_unit_tov",
					"PROPERTY_FIRM.ID",
					"PROPERTY_FIRM.IBLOCK_ID",
					"PROPERTY_FIRM.NAME",
					"PROPERTY_FIRM.PREVIEW_PICTURE",
					"PROPERTY_FIRM.DETAIL_PICTURE",
					"PROPERTY_FIRM.DETAIL_PAGE_URL",
					"PROPERTY_FIRM.PROPERTY_USER",
					"PROPERTY_FIRM.PROPERTY_EMAIL",
					"PROPERTY_FIRM.PROPERTY_MANAGER",
					"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
					"PROPERTY_FIRM.PROPERTY_LIQUIDATED",
					"PROPERTY_FIRM.PROPERTY_PREMIUM"
				)
			);
			$rsElements->SetUrlTemplates( "", $arParams["DETAIL_URL"] );
			while ( $arElements = $rsElements->GetNext() ) {
				//PrintAdmin($arElements);die();
				//�������� ������������� ��������
				$arMultiPropList = array( "FAVORITES" );
				foreach ( $arMultiPropList as $multiPropName ) {
					$arElements[ "PROPERTY_" . $multiPropName ] = array();
					$rsMultiProp                                = CIBlockElement::GetProperty(
						$arElements["IBLOCK_ID"],
						$arElements["ID"],
						array(),
						array( "CODE" => $multiPropName )
					);
					while ( $arMultiProp = $rsMultiProp->GetNext() ) {
						$arElements[ "PROPERTY_" . $multiPropName ][]            = $arMultiProp;
						$arElements[ "PROPERTY_" . $multiPropName . "_VALUE" ][] = $arMultiProp["VALUE"];
					}
				}

				//������ �����
				if ( ! strlen( $arElements["PREVIEW_TEXT"] ) ) {
					$arElements["PREVIEW_TEXT"] = strip_tags( $obParser->html_cut( $arElements["DETAIL_TEXT"], $arParams["DETAIL_TEXT_CUT"] ) );
				}

				//������
				$arElements["PREVIEW_PICTURE"] = Resizer(
					array( $arElements["PREVIEW_PICTURE"], $arElements["DETAIL_PICTURE"] ),
					array( "width" => 55, "height" => 55 ),
					BX_RESIZE_IMAGE_EXACT
				);
				if ( ! strlen( $arElements["PREVIEW_PICTURE"]["SRC"] ) ) {
					$arElements["PREVIEW_PICTURE"]["SRC"] = "/images/withoutphoto54x44.png";
				}

				//�������������� ������/������ ���������
				$arElements["PROPERTY_CURRENCY_FORMATED"] = StrUnion( array(
					$arElements["PROPERTY_CURRENCY_VALUE"],
					$arElements["PROPERTY_unit_tov_VALUE"]
				), "/" );

				//���� �������� ���������, �� ������� ���� � ��������
				if ( $arElements["PROPERTY_FIRM_PROPERTY_REMOVE_REL_ENUM_ID"] != false ) {
					$arElements["PROPERTY_FIRM_ID"] = 0;
				}

				//��������� ���� � ��������
				if ( ! isset( $arResult["COMPANY_DETAIL"][ $arElements["PROPERTY_FIRM_ID"] ] ) ) {
					//TODO: ������ ���������� �������� �������
					$arCompany = array(
						"ID"               => $arElements["PROPERTY_FIRM_ID"],
						"IBLOCK_ID"        => $arElements["PROPERTY_FIRM_IBLOCK_ID"],
						"NAME"             => $arElements["PROPERTY_FIRM_NAME"],
						"PREVIEW_PICTURE"  => $arElements["PROPERTY_FIRM_PREVIEW_PICTURE"],
						"DETAIL_PICTURE"   => $arElements["PROPERTY_FIRM_DETAIL_PICTURE"],
						"DETAIL_PAGE_URL"  => $arElements["PROPERTY_FIRM_DETAIL_PAGE_URL"],
						"PROPERTY_USER"    => $arElements["PROPERTY_FIRM_PROPERTY_USER_VALUE"],
						"PROPERTY_EMAIL"   => $arElements["PROPERTY_FIRM_PROPERTY_EMAIL_VALUE"],
						"PROPERTY_MANAGER" => $arElements["PROPERTY_FIRM_PROPERTY_MANAGER_VALUE"],
						"PREMIUM"          => $arElements["PROPERTY_FIRM_PROPERTY_PREMIUM_VALUE"],
						"LIQUIDATED"          => $arElements["PROPERTY_FIRM_PROPERTY_LIQUIDATED_VALUE"],
						"ITEMS_COUNT"      => array( $arList["ID"] => 1 )
					);
					//������� �������� ��������
					if ( $arParams["COMPANY_NAME_CUT"] && ( strlen( html_entity_decode( $arCompany["NAME"] ) ) > ( $arParams["COMPANY_NAME_CUT"] + 2 ) ) ) {
						$arCompany["NAME_CUT"] = $obParser->html_cut( html_entity_decode( $arCompany["NAME"] ), $arParams["COMPANY_NAME_CUT"] );
					} else {
						$arCompany["NAME_CUT"] = $arCompany["NAME"];
					}
					//������
					$arCompany["PREVIEW_PICTURE"] = Resizer(
						array( $arCompany["PREVIEW_PICTURE"], $arCompany["DETAIL_PICTURE"] ),
						array( "width" => 148, "height" => 40 ),
						BX_RESIZE_IMAGE_PROPORTIONAL_ALT
					);
					//�������� ������������� ��������
					$arMultiPropList = array( "phone", "URL", "FAVORITES" );
					foreach ( $arMultiPropList as $multiPropName ) {
						$arCompany[ "PROPERTY_" . $multiPropName ] = array();
						$rsMultiProp                               = CIBlockElement::GetProperty(
							$arCompany["IBLOCK_ID"],
							$arCompany["ID"],
							array(),
							array( "CODE" => $multiPropName )
						);
						while ( $arMultiProp = $rsMultiProp->GetNext() ) {
							$arCompany[ "PROPERTY_" . $multiPropName ][]            = $arMultiProp;
							$arCompany[ "PROPERTY_" . $multiPropName . "_VALUE" ][] = $arMultiProp["VALUE"];
						}
					}
					//������������ ��� ����� ��������
					foreach ( $arCompany["PROPERTY_URL"] as &$url ) {
						if ( ( strpos( $url["VALUE"], 'http://' ) === false ) && ( strpos( $url["VALUE"], 'https://' ) === false ) ) {
							$url["VALUE_HREF"] = 'http://' . $url["VALUE"];
						} else {
							$url["VALUE_HREF"] = $url["VALUE"];
						}
					}
					//�������� �������������
					if ( ENABLE_PREDSTAVITEL_MODE ) {
						$arPredstavitelInfo         = PredstavitelGetByCompany( $arCompany["ID"] );
						$arCompany["PROPERTY_USER"] = $arPredstavitelInfo["RELATED"];
					}
					if ( ! $arCompany["PROPERTY_USER"] ) {
						$arCompany["PROPERTY_USER"] = $arCompany["PROPERTY_MANAGER"];
					}
					if ( $arCompany["PROPERTY_USER"] ) {
						$arCompany["PREDSTAVITEL"]               = $USER->GetByID( $arCompany["PROPERTY_USER"] )->Fetch();
						$arCompany["PREDSTAVITEL_MESSAGE_LINK"]  = "/personal/messages/{$arCompany["PREDSTAVITEL"]["ID"]}/";
						$arElements["PREDSTAVITEL_MESSAGE_LINK"] = "{$arCompany["PREDSTAVITEL_MESSAGE_LINK"]}?product={$arElements["ID"]}";
					} else {
						$arCompany["PREDSTAVITEL_MESSAGE_LINK"]  = "mailto:{$arCompany["PROPERTY_EMAIL"]}";
						$arElements["PREDSTAVITEL_MESSAGE_LINK"] = $arCompany["PREDSTAVITEL_MESSAGE_LINK"];
					}

					//���������� ��������
					$arResult["COMPANY_DETAIL"][ $arElements["PROPERTY_FIRM_ID"] ] = $arCompany;
				} else {
					$arResult["COMPANY_DETAIL"][ $arElements["PROPERTY_FIRM_ID"] ]["ITEMS_COUNT"][ $arList["ID"] ] ++;
				}
				$company = $arResult["COMPANY_DETAIL"][ $arElements["PROPERTY_FIRM_ID"] ];

				//������ �� ������� ������
				$arElements["PREDSTAVITEL_MESSAGE_LINK"] = $company["PREDSTAVITEL_MESSAGE_LINK"];
				if ( strpos( $company["PREDSTAVITEL_MESSAGE_LINK"], "mailto" ) === false ) {
					$arElements["PREDSTAVITEL_MESSAGE_LINK"] .= "?product={$arElements["ID"]}";
				}

				if ( $arResult["SORT_BY"] != "company" ) { // �� �� ���������
					//����������� �� ���������
					if (
						( $groupIndex < 0 )//� ������ ��� ��� �� ����� ������
						|| ! $arElements["PROPERTY_FIRM_ID"]//����� �� ����������� ��������
						|| ( $arElements["PROPERTY_FIRM_ID"] != $arList["GROUPS"][ $groupIndex ]["COMPANY"] )//�������� ����� ������ ���������� �� �������� ���� ������
					) {
						$groupIndex ++;
					}
					$arList["GROUPS"][ $groupIndex ]["COMPANY"] = $arElements["PROPERTY_FIRM_ID"];
					$arList["GROUPS"][ $groupIndex ]["ITEMS"][] = $arElements;
				} else { // �� ���������
					//������ ������� ���� � �������� �������� ������ ������
					if ( $company["ITEMS_COUNT"][ $arList["ID"] ] > $arParams["COMPANY_ELEMENT_COUNT"] ) {
						$arElements["HIDE"] = true;
					}
					//�������� ������ ������ ������� ���������
					if ( ! ( ( $company["ITEMS_COUNT"][ $arList["ID"] ] - $arParams["COMPANY_ELEMENT_COUNT"] ) % $arParams["COMPANY_ELEMENT_MORE_COUNT"] ) ) {
						$arElements["HIDE_PANEL"] = true;
					}
					//����������� �� ���������
					//$groupKey  = array_search( $arElements["PROPERTY_FIRM_ID"], array_keys( $arResult["COMPANY_ONPAGE"] ) );

			         //����������� �� ���������
				      if ($arElements["PROPERTY_FIRM_ID"] == 0 ) {
					      $groupKey = 999999;
				      } else {

					      // ����� �� �������� ������ � ���������
					      $groupKey = array_search($arElements["PROPERTY_FIRM_ID"], $CM_SORTED);
				      }

					$arList["GROUPS"][ $groupKey ]["COMPANY"] = $arElements["PROPERTY_FIRM_ID"];
					$arList["GROUPS"][ $groupKey ]["ITEMS"][] = $arElements;
				}

			}

			if ( $arResult["SORT_BY"] != "company" ) {
				//������ ���������

				$rsElements->NavNum        = $arList["ID"];
				$arList["NAV_STRING"]      = $rsElements->GetPageNavStringEx( $navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"] );
				$arList["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
				//$arList["NAV_RESULT"] = $rsElements;
			} else {
				//���������� ����� ��� ����������� ������� ��������
				ksort( $arList["GROUPS"] );
			}

			//������ ���������� ������� � �������
			$arList["ITEMS_COUNT"] = CIBlockElement::GetList( Array(), array_merge( $arrFilter, $arFilter ), Array() );

			//����������
			$arResult["LISTS"][ $arListKey ] = $arList;
		}
	}
//�������� ������ �������� � ����������� ������������� �������
	$arTemp = array();
	foreach ( $arResult["LISTS"] as $arList ) {
		if ( $arList["ITEMS_COUNT"] ) {
			//����������� ������� ����
			if (
				( ( $arParams["CURPAGE"] ) && ( $arParams["CURPAGE"] == $arList["ID"] ) )
				||
				( ( ! $arParams["CURPAGE"] ) && ( ! count( $arTemp ) ) )
			) {
				$arList["DISPLAYED"]          = true;
				$arResult["ACCORDION_ACTIVE"] = count( $arTemp );
			}
			$arTemp[] = $arList;
		}
	}

	if ( count( $arResult["LISTS"] ) != 1 ) {
		//$arResult["LISTS"] = $arTemp;
	}
}

//*****************************************************************************************************************
//
//          ����
//
//*****************************************************************************************************************

if ( strlen( $arResult["THIS_SECTION"][ $arParams["BROWSER_TITLE"] ] ) ) {
	$APPLICATION->SetTitle( $arResult["THIS_SECTION"][ $arParams["BROWSER_TITLE"] ] );
}
if ( strlen( $arResult["THIS_SECTION"][ $arParams["META_DESCRIPTION"] ] ) ) {
	$APPLICATION->SetPageProperty( "description", $arResult["THIS_SECTION"][ $arParams["META_DESCRIPTION"] ] );
}
if ( strlen( $arResult["THIS_SECTION"][ $arParams["META_KEYWORDS"] ] ) ) {
	$APPLICATION->SetPageProperty( "keywords", $arResult["THIS_SECTION"][ $arParams["META_KEYWORDS"] ] );
}


//TODO: ������ ����� �� ������ ������
//TODO: ������ premium
//TODO: ��������� ������ ���� � ������ ����������, ����� ��� �� ��������� ���������� �������
//TODO: �����������


//��������� ������
if ( count( $arResult["ERROR"] ) > 0 ) {
	ShowError( implode( "<br>", $arResult["ERROR"] ) );

	return;
}

//PrintAdmin($arResult);

$this->IncludeComponentTemplate();