//���������� ������
function ClearAndReset($select, $chozen, $input){                       
   $select.find("option[value=0]").attr("selected", "selected");
   $select.attr("disabled", "disabled");
   $chozen.trigger("chosen:updated");
   $input.addClass("hidden").val("");
}

function SetManual($select, $chozen, $input){
   $select.find("option[value=-1]").attr("selected", "selected");
   $select.attr("disabled", "disabled");
   $chozen.trigger("chosen:updated");
   $input.val("").removeClass("hidden");
}

$(document).ready(function(){

   var $selectRegionChosen = $(".js_country_sel").change(function(){
      var   $this = $(this),
      val = $this.val(),
      inputVal = $this.find(":selected").html(),
      $input = $(".js_country"),
      $selectChild = $(".js_region_sel");   

      ClearAndReset($selectChild, $selectCityChosen, $(".js_region"));
      ClearAndReset($(".js_city_sel"), $selectIndexChosen, $(".js_city"));

      if(val > 0)
      {
         $input.addClass("hidden");
         $.ajax({
            type: "POST",
            url: "/_ajax/getPlace.php", 
            data: "ext_id=" + val + "&type=region",
            dataType: "json",
            success: function(data){
               $selectChild.html('');
               $.each(data.listOptionHTML, function(key, value){
                  $selectChild.append(value);               
               }); 
               $selectChild.removeAttr("disabled");
               $selectCityChosen.trigger("chosen:updated");
               $input.val(inputVal);
            }
         }); 
      } else if(val == -1)
      {
         $input.removeClass("hidden");
         $input.val("");

         SetManual($selectChild, $selectCityChosen, $(".js_region"));
         SetManual($(".js_city_sel"), $selectIndexChosen, $(".js_city"));                              
      } else
      {
         $input.val("");   
      }
   });

   var $selectCityChosen = $(".js_region_sel").chosen().change(function(){
      var   $this = $(this),
      val = $this.val(),
      inputVal = $this.find(":selected").html(),
      $input = $(".js_region"),
      $selectChild = $(".js_city_sel");                                  

      ClearAndReset($selectChild, $selectIndexChosen, $(".js_city"));

      if(val > 0)
      {
         $input.addClass("hidden");
         $.ajax({
            type: "POST",
            url: "/_ajax/getPlace.php", 
            data: "ext_id=" + val + "&type=city",
            dataType: "json",
            success: function(data){
               $.each(data.listOptionHTML, function(key, value){
                  $selectChild.html('');
                  $.each(data.listOptionHTML, function(key, value){
                     $selectChild.append(value);               
                  }); 
                  $selectChild.removeAttr("disabled");
                  $selectIndexChosen.trigger("chosen:updated");
               }); 
               $input.val(inputVal);
            }
         }); 
      } else if(val == -1)
      {
         $input.removeClass("hidden");
         $input.val("");

         SetManual($selectChild, $selectIndexChosen, $(".js_city"));  
      } else
      {
         $input.val("");   
      }
   });

   var $selectIndexChosen = $(".js_city_sel").change(function(){
      var   $this = $(this),
      val = $this.val(),
      inputVal = $this.find(":selected").html(),
      $input = $(".js_city");   

      if(val > 0)
      {
         $input.addClass("hidden");
         $input.val(inputVal);
         hideErrorFunc($this);
      } else if(val == -1)
      {
         $input.removeClass("hidden");
         $input.val("");
      } else
      {
         $input.val("");   
      }
   });
});


//���������
//������
function showErrorFunc($element, text){
   var $tr = $element.closest('tr');
   $tr.addClass("error");
   $tr.find('.errortext').text(text);
}
function hideErrorFunc($element){
   var $tr = $element.closest('tr');
   if($tr.hasClass("error")){
      $tr.removeClass("error");
      $tr.find('.errortext').text("");
   }
}
//��������
function fieldEmptyFunc($element){
   return $element.val().length
}
function fieldMinMaxFunc($element){
   return (($element.val().length >= 3) && ($element.val().length <= 50))
}
function fieldSymbolsFunc($element){
   return checkStringJS($element.val())
}
function fieldEmailFunc($element){
   return checkEmailJS($element.val())
}
function fieldShortPassFunc($element){
   return ($element.val().length >= 6)
}
function fieldConfirmPassFunc($element){
   return ($element.val() == $('#register-form-password').val())
}
function fieldContractFunc($element){
   return ($element.prop("checked"))
}
//����������
validName = validation({//���
      selector: '#register-form-name',
      require: [
      {
         compare: fieldEmptyFunc,
         errortext: "��� �� ���������"
      },{
         compare: fieldMinMaxFunc,
         errortext: "��� ������ ��������� �� 3 �� 50 ��������"
      },{
         compare: fieldSymbolsFunc,
         errortext: "������ ������� �������� � ����������� �������� � �����"
      }
      ],
      showError: showErrorFunc,
      hideError: hideErrorFunc
   })
validLastname = validation({//�������
   selector: "#register-form-lastname",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "������� �� ���������"
   },{
      compare: fieldMinMaxFunc,
      errortext: "������� ������ ��������� �� 3 �� 50 ��������"
   },{
      compare: fieldSymbolsFunc,
      errortext: "������ ������� �������� � ����������� �������� � �����"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
})
validEmail = validation({//�����
   selector: "#register-form-email",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "����� ����������� ����� �� ��������"
   },{
      compare: fieldEmailFunc,
      errortext: "������������ ����� ����������� �����"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
})
validPassword = validation({
   selector: "#register-form-password",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "������ �� ��������"
   },{
      compare: fieldShortPassFunc,
      errortext: "����������� ����� ������ - 6 ��������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
})
validConfirm = validation({
   selector: "#register-form-confirm",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "��������� ������"
   },{
      compare: fieldConfirmPassFunc,
      errortext: "������ �� ���������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
})
validCaptcha = validation({
   selector: '#register-form-captcha',
   require: [
      {
         compare: fieldEmptyFunc,
         errortext: "������� ���"
      }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
})
validContract = validation({
   selector: '#register-form-contract',
   require: [
      {
         compare: fieldContractFunc,
         errortext: "��������� ������"
      }
   ],
   showError: function(){},
   hideError: function(){}
})

$(document).ready(function(){
   //���������
   //���
   $('#register-form-name').live("focusout", function(){
      //console.log("validName", validName);
      validName();
   })
   //�������
   $('#register-form-lastname').live("focusout", function(){
      validLastname();
   })
   //�����
   $('#register-form-email').live("focusout", function(){
      validEmail();
   })
   //Password
   $('#register-form-password').live("focusout", function(){
      validPassword();
   })
   //Password
   $('#register-form-confirm').live("focusout", function(){
      validConfirm();
   })
   //Captcha
   $('#register-form-captcha').live("focusout", function(){
      validCaptcha();
   })
   /*$('#register-form-captcha').live("focusin", function(){
      $(this).closest('tr').removeClass("error");
      $('#register-errortext-captcha').val();
   })*/
            
   //������
   $('#register-form').live("submit", function(){
      var $this = $(this);
      var $form = $this.closest('form');
      //�������� ������
      validName();
      validLastname();
      validEmail();
      validPassword();
      validConfirm();
      validCaptcha();
      if($form.find('.error').size() || !validContract()) return false;
   })
            
   /*RELOAD CAPTCHA*/
   $(".captchab").live("click", function(){
      var symbols ="abcdefghjklmnopqrstuvwxyz0123456789"
      var length = 32;
      var captcha_code = generatePassword(symbols, length);
      var c = "/bitrix/tools/captcha.php?captcha_sid=" + captcha_code ;
      $(".captcha img").attr("src", c)
      $(".captcha_sid_register").attr("value", captcha_code)
   })
})