<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(is_array($arResult["SECTIONS"]) && count($arResult["SECTIONS"]) > 0):?>
   <div class="gray_box">
      <div class="market_sect_l">
         <ul class="comp_market_sect">
            <?foreach($arResult["SECTIONS"] as $arSection):?>
               <li style="<?=($arSection["ID"] == $arParams['SECTION_P_ID'])?'font-weight:bold;':''?>">
	               <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
	               <?if($arSection["ELEMENT_CNT"] > 0):?><span>&nbsp;<?=$arSection["ELEMENT_CNT"]?></span><?endif;?>
               </li>
            <?endforeach;?>
         </ul>
      </div>
      <div style="clear:both;"></div>
   </div>
<?endif;