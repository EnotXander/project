<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
require_once( $_SERVER["DOCUMENT_ROOT"].$component->__path."/classes.php");

    $main_photo_value = $arResult["ELEMENT_PROPERTIES"][418][0]["VALUE"]; // ?
	$has_photo = 0;
	$pc = 0;
$_REGION = GetPropertyId("pr2", IBLOCK_ADVARE);
$_CITY = GetPropertyId("CITY", IBLOCK_ADVARE);
/*
							GetPropertyId("TYPE", IBLOCK_ADVARE),
                           GetPropertyId("pr2", IBLOCK_ADVARE),//REGION
                           GetPropertyId("CITY", IBLOCK_ADVARE),
                           GetPropertyId("ADDRESS", IBLOCK_ADVARE),
                           "NAME",
                        GetPropertyId("COST", IBLOCK_ADVARE),//����
                           GetPropertyId("CURRENCY", IBLOCK_ADVARE),//CURRENCY
                           GetPropertyId("UNITS", IBLOCK_ADVARE),
                           //GetPropertyId("TORG", IBLOCK_ADVARE),//TORG
                        GetPropertyId("more_photo", IBLOCK_ADVARE),//more_photo
                           "PREVIEW_PICTURE",
                        GetPropertyId("LOAD", IBLOCK_ADVARE), // LOAD GetPropertyId("LOAD", IBLOCK_ADVARE)
                           "DETAIL_TEXT",
                           "PREVIEW_TEXT",
                           GetPropertyId("CONTACT", IBLOCK_ADVARE),
                           GetPropertyId("pr1", IBLOCK_ADVARE),
                           GetPropertyId("SKYPE", IBLOCK_ADVARE)
*/
?>
<? ?>
<??>

<script type="text/javascript" >
    $(document).ready(function(){
        $(".buttons-group input[name=iblock_submit]").click(function(){
            $('div.redactor_editor a').each(function(index) {
                var curSite = '<?=$_SERVER["HTTP_HOST"]?>';
                var href = $(this).attr('href');

                if(!(href.indexOf(curSite) + 1)) {
                    $(this).attr('rel', 'nofollow');
                }
            });
            $('#PROPERTYDETAILTEXT0').val($('div.redactor_editor').html());
        });
		
		$("#div_more_photo .photo-wrap img").click(function(){
			$('input#hidden_main_photo').val($(this).attr('rel'));
			$("#div_more_photo .photo-wrap img.active").removeClass("active");
			$(this).toggleClass("active");
		});
    });
</script>
<span id="page_data" data-citycode="<?= $_CITY ?>" data-regioncode="<?= $_REGION ?>"></span>
<form name="iblock_add" id="add-adv-form" enctype="multipart/form-data">
	<?= bitrix_sessid_post() ?>

	<? if ($arParams["MAX_FILE_SIZE"] > 0): ?>
		<input type="hidden" name="MAX_FILE_SIZE" value="<?= $arParams["MAX_FILE_SIZE"] ?>"/>
	<? endif ?>
	
	<ul class="mb_menu">
		<?$firstTab = true;
		foreach ($arResult["PROPERTY_LIST"] as $tabKey => $arTab):
			if (!$arResult["ELEMENT_EXISTS"] && !$firstTab) break;?>
			<li<?= $firstTab ? ' class="mb_title sel" id="mb_title_f"' : '' ?>><a
					href="javascript:void(0)"><?= $tabKey ?></a>

				<div class="sclew"></div>
			</li>
			<?$firstTab = false;
		endforeach;?>
	</ul>

	<? $firstTab = true; ?>
	<? foreach ($arResult["PROPERTY_LIST"] as $arTab): ?>
		<? if (!$arResult["ELEMENT_EXISTS"] && !$firstTab) break; ?>
		<div class="add-adv-form">

			<? if (is_array($arTab) && !empty($arTab)): ?>
				<? foreach ($arTab as $propertyID): ?>
					<? if ($propertyID == 'EMPTY') continue; ?>

					<?
					if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
					{
						$inputNum = (($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) && $arResult["ELEMENT_PROPERTIES"][$propertyID][0]["VALUE"]) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
						$inputNum += 2;//$arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
					} else
						$inputNum = 1;
					?>

					<? if(!(int)$propertyID): ?>

						<? switch ($propertyID):
							case "IBLOCK_SECTION": ?>
								<? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C"): ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio"; ?>
								<? else: ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown"; ?>
								<? endif; ?>
								<div>

									<label>
										<? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
											<span class="starrequired">*</span>
										<? endif ?>
										<?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : (intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] : GetMessage("IBLOCK_FIELD_" . $propertyID)) ?>:
									</label>

									<select name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>" style="max-width: 610px;">
										<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT"; ?>

										<? foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum): ?>
											<?
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
													if ($key == $arElEnum["VALUE"]) {
														$checked = true;
														break;
													}
												}
											} elseif ($arEnum["DEF"] == "Y") {
												$checked = true;
											}
											?>
											<option
												value="<?= $key; ?>" <?= $checked ? " selected=\"selected\"" : ""; ?>><?= $arEnum["VALUE"]; ?></option>
										<? endforeach; ?>
									</select>

								</div>
								<? break; ?>
							<?case "NAME": ?>
								<div>
									<div class="min">
										<label>
											<? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
												<span class="starrequired">*</span>
											<? endif ?>
											���������:
										</label>
										<? for ($i = 0; $i < $inputNum; $i++): ?>
											<?
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
											} elseif ($i == 0) {
												$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
											} else {
												$value = "";
											}
											?>
											<textarea name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
											          size="200"
											          maxlength="200"
											          rows="2"
											          style="min-height: 24px;"
											          class="inp-title"
												><?= $value ?></textarea>
										<? endfor; ?>
									</div>
									<div class="min"><span class="t-tip">�� ����� 200 ��������</span></div>
								</div>
								<? break; ?>
							<? case "DETAIL_TEXT": ?>
								<div>
									<label class="accent"><span class="starrequired">*</span>��������� ��������:</label>
								<? $formId = preg_replace("/[^a-z0-9]/i", '', "PROPERTY[" . $propertyID . "][0]"); ?>
								<textarea id="<?= $formId ?>" name="PROPERTY[<?= $propertyID ?>][0]" rows="4"
							         cols="5"><?= $arResult["ELEMENT"][$propertyID] ?></textarea>
								<script type="text/javascript">
									$(document).ready(function () {
										/*$('#<?=$formId?>').redactor({
											resize: true,
											lang: "ru",
											buttons: ['bold', 'italic', 'underline', 'RemoveFormat', 'link', 'image', 'orderedlist', 'unorderedlist'],
											imageUpload: '/_ajax/personal/image_upload.php'
										});*/
										$('#<?=$formId?>').redactor({
											lang: "ru",
											buttons: ['bold', 'italic', 'underline', 'RemoveFormat', 'link', 'image', 'orderedlist', 'unorderedlist'],
											imageUpload: '/_ajax/personal/image_upload.php'
										});
									});
								</script>
								</div>
								<? break; ?>
							<? case "PREVIEW_TEXT": ?>
								<div>
								<label class="accent">������� ��������:</label>
								<? $formId = preg_replace("/[^a-z0-9]/i", '', "PROPERTY[" . $propertyID . "][0]"); ?>
								<textarea id="<?= $formId ?>" name="PROPERTY[<?= $propertyID ?>][0]" rows="5"
								          cols="5" class="low" maxlength="130"><?= $arResult["ELEMENT"][$propertyID] ?></textarea>
								</div>
								<? break; ?>
							<? default: ?>
								<? break; ?>
						<? endswitch; ?>

					<? else: ?>
						<??>
						<? switch ($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]):
							case "pr2": ?>
								<? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C"): ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio"; ?>
								<? else: ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown"; ?>
								<? endif; ?>
								<div>
									<div class="min">
									<label>
										<? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
											<span class="starrequired">*</span>
										<? endif ?>
										<?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : (intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] : GetMessage("IBLOCK_FIELD_" . $propertyID)) ?>:
									</label>

									<select name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>">
										<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT"; ?>

										<? foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum): ?>
											<?
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
													if ($key == $arElEnum["VALUE"]) {
														$checked = true;
														break;
													}
												}
											} elseif ($arEnum["DEF"] == "Y") {
												$checked = true;
											}
											?>
											<option
												value="<?= $arEnum["ID"]; ?>" <?= $checked ? " selected=\"selected\"" : ""; ?>><?= $arEnum["NAME"]; ?></option>
										<? endforeach; ?>
									</select>
									</div>
								<? break; ?>
							<? case "CITY": ?>
								<? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C"): ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio"; ?>
								<? else: ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown"; ?>
								<? endif; ?>

									<div class="min">
									<label class="row">
										<? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
											<span class="starrequired">*</span>
										<? endif ?>
										<?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : (intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] : GetMessage("IBLOCK_FIELD_" . $propertyID)) ?>:
									</label>

									<select data-value="<?= $arResult['ELEMENT_PROPERTIES'][$_CITY][0]['VALUE'] ?>" id="sel_cities" name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>">
										<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT"; ?>

										<? foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum): ?>
											<?
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
													if ($key == $arElEnum["VALUE"]) {
														$checked = true;
														break;
													}
												}
											} elseif ($arEnum["DEF"] == "Y") {
												$checked = true;
											}
											?>
											<option     value="<?= $arEnum["ID"]; ?>" <?= $checked ? " selected=\"selected\"" : ""; ?>>
												<?= $arEnum["NAME"]; ?>
											</option>
										<? endforeach; ?>
									</select>
									</div>
								</div>
								<? break; ?>
							<? case "ADDRESS": ?>
								<div>
								<div class="min">
								<label>
									<? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
										<span class="starrequired">*</span>
									<? endif ?>
									<?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : (intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] : GetMessage("IBLOCK_FIELD_" . $propertyID)) ?>:
								</label>
								<? for ($i = 0; $i<$inputNum; $i++): ?>
								<?
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									} elseif ($i == 0)
									{
										$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
									} else
									{
										$value = "";
									}
								?>
									<input type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" value="<?=$value?>" />
								<? endfor; ?>
								</div>
								</div>
								<? break; ?>
							
							<? case "COST": ?>
								<div>
								<div class="min">
									<label>
										<? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
											<span class="starrequired">*</span>
										<? endif ?>
										<?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : (intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] : GetMessage("IBLOCK_FIELD_" . $propertyID)) ?>:
									</label>
									<? for ($i = 0; $i<$inputNum; $i++): ?>
										<?
										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
										{
											$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
										} elseif ($i == 0)
										{
											$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
										} else
										{
											$value = "";
										}
										?>
										<input type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" value="<?=$value?>" maxlength="11"/>
									<? endfor; ?>
								</div>
								<? break; ?>
							<? case "CURRENCY": ?>
								<? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C"): ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio"; ?>
								<? else: ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown"; ?>
								<? endif; ?>
								<div class="min">
									<select name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>">
										<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT"; ?>

										<? foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum): ?>
											<?
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
													if ($key == $arElEnum["VALUE"]) {
														$checked = true;
														break;
													}
												}
											} elseif ($arEnum["DEF"] == "Y") {
												$checked = true;
											}
											?>
											<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
										<? endforeach; ?>
									</select>
								</div>
								<? break; ?>
							<?case "UNITS": ?>
								<? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C"): ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio"; ?>
								<? else: ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown"; ?>
								<? endif; ?>
								<span class="span-za">��</span>
								<div class="min">
									<select name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>">
										<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT"; ?>

										<? foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum): ?>
											<?
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
													if ($key == $arElEnum["VALUE"]) {
														$checked = true;
														break;
													}
												}
											} elseif ($arEnum["DEF"] == "Y") {
												$checked = true;
											}
											?>
											<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
										<? endforeach; ?>
									</select>
								</div>
								</div>
								<? break; ?>
							<? case "more_photo": ?>
								<div>
									<div class="min">
										<label>����:</label>
									</div>
									<div id="div_more_photo" style="display: inline-block; vertical-align: top;">
										<?if(count($arResult["ELEMENT_PROPERTIES"][$propertyID]) > 0):?>
											<?for ($i = 0; $i<count($arResult["ELEMENT_PROPERTIES"][$propertyID]); $i++): ?>
												<?$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];?>
													<?if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])):?>
													<div class="min photo-wrap">
														<?if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]):?>
															<img src="<?=$arResult["ELEMENT_FILES"][$value]["THUMB_PICTURE"]["SRC"]?>"
															     style="width: 52px; height: 52px;" class="<?if($pc == $main_photo_value):?>active<?endif;?>" rel="<?=$pc?>"/><br />
															<?$pc++;?>
															<input type="hidden" name="MORE_PHOTO[]" value="<?=$arResult["ELEMENT_FILES"][$value]["THUMB_PICTURE"]["SRC"]?>">
														<?else:?>
															<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
															<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
															[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
														<?endif;?>
														<span class="t-tip del" onclick="$(this).parent().html('<input type=\'hidden\' name=\'DELETE_FILE[<?= $propertyID ?>][]\' value=\'<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ?>\' />').hide();">�������</span>
													</div>
													<?endif;?>
											<? endfor; ?>
											<?$has_photo = 1;?>
										<? endif; ?>

                                        <?if($_POST["iblock_submit"] == '�������� ����������'):?>
											<?if(count($_POST["MORE_PHOTO"]) > 0):?>
												<?foreach($_POST["MORE_PHOTO"] as $photo):?>
													<div class="min photo-wrap">
														<img src="<?=$photo?>"  rel="<?=$pc?>" style="width: 52px; height: 52px;"/><br />
														<?$pc++;?>
														<input type="hidden" name="MORE_PHOTO[]" value="<?=$photo?>">
														<span class="t-tip del" onclick="$(this).parent().remove();">�������</span>
													</div>
												<?endforeach?>
												<?$has_photo = 1;?>
											<?endif;?>
                                        <?endif?>
										<div class="min photo-wrap"> 
											<span class="add-btn act"></span>
											<input type="file" style="display:none" name="user_file" id="MORE_PHOTO" onchange=""><br />
										</div>
								    </div>
									
									<span class="t-tip photo-comment">
										������ ����������� ���������� ����� ���������<br>
										����������� � ������� JPG, GIF, PNG �� 3 ��
									</span>
								</div>
								<? break; ?>
								
								<? case "HIDDEN_MAIN_PHOTO": ?>
										<?$hidden_main_photo = $arResult["ELEMENT_PROPERTIES"][$propertyID]["VALUE"];?>
									<? break; ?>
							
							<? case "LOAD": ?>
								<div class="file">
									<div class="min" >
										<label>������������:</label>
									</div>
                                    <div id="div_more_files">
									<?for ($i = 0; $i<count($arResult["ELEMENT_PROPERTIES"][$propertyID]); $i++): ?>
											<?$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];?>
											
											<?if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])):?>
												<div class="min " style="margin-right: 5px">
													<span class="t-tip"><?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?></span><br />
													<span class="t-tip del" onclick="$(this).parent().html('<input type=\'hidden\' name=\'DELETE_FILE[<?= $propertyID ?>][]\' value=\'<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ?>\' />').hide();">�������</span>
													<input type="hidden" name="MORE_FILE[]" value="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>">
												</div>
											<?endif;?>
									<? endfor; ?>

                                    <?if($_POST["iblock_submit"] == '�������� ����������'):?>
                                        <?foreach($_POST["MORE_FILE"] as $file):?>
                                            <div class="min">
                                                <span class="t-tip file-name"><?=basename($file)?></span><br />
                                                <span class="t-tip del" onclick="$(this).parent().remove();">�������</span>
                                                <input type="hidden" name="MORE_FILE[]" value="<?=$file?>">
                                            </div>
                                        <?endforeach;?>
                                    <?endif;?>
										<div class="min add">
											<span class="add-file">���������� �����</span><input type="file" name="user_file_up" id="MORE_FILE" onchange="" style="display: none;"><br />
										</div>
									</div>
									<span class="t-tip file-comment">������������ .pdf, .xls(x), .doc(x), .rtf, .ppt, .jpg, .jpeg, .png, .gif&nbsp; �� 3��</span>
								</div>
								<? break; ?>
							<? case "CONTACT": ?>
								<div>
									<div class="min">
										<label>
											<? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
												<span class="starrequired">*</span>
											<? endif ?>
											���������� ����:
										</label>
										<? for ($i = 0; $i < $inputNum; $i++): ?>
											<?
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
											} elseif ($i == 0) {
												$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
											} else {
												$value = "";
											}
											?>
											<input type="text" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
											       value="<?= $value ?>" class="inp-name" maxlength="50"/>
										<? endfor; ?>
									</div>
									<div class="min">
										<span class="t-tip">��� �������� � ����� � ���������� �����������</span>
									</div>
								</div>
								<? break; ?>
							<? case "pr1": ?>
								<? $inputNum--; ?>
									<? for ($i = 0; $i<$inputNum; $i++): ?>
									<div class="phone<?= $i == 0 ? '-first' : ''; ?>">
										<div class="min">
											<label>
												<? if ($i == 0): ?>
													<span>*</span>�������
												<? endif; ?>
											</label>
										</div>
										<?

                                        $value = $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"]
										?>
										<div class="min">
											<span></span>
											<input type="text" class="extPhone" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?= $value; ?>" />

										</div>
										<div class="min">
											<span class="t-tip">c ����� ������ +</span>
										</div>

									</div>
									<? endfor; ?>
								<div class="div-more-numbers">
									<span class="more-numbers">������ �������</span>
								</div>
                                <input type="hidden" id="phone_input_num" value="<?= $inputNum; ?>">
								<? break; ?>
							<? case "SKYPE": ?>
								<div>
									<div class="min">
										<label>
											<? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
												<span class="starrequired">*</span>
											<? endif ?>
											<?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : (intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] : GetMessage("IBLOCK_FIELD_" . $propertyID)) ?>
											:
										</label>
										<? for ($i = 0; $i < $inputNum; $i++): ?>
											<?
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
											} elseif ($i == 0) {
												$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
											} else {
												$value = "";
											}
											?>
											<input type="text" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]" value="<?= $value ?>" maxlength="40"/>
										<? endfor; ?>
									</div>
								</div>
								<? break; ?>
							<? case "TYPE": ?>
								<div>
								<? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C"): ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio"; ?>
								<? else: ?>
									<? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown"; ?>
								<? endif; ?>
								<div class="min">
									<label><span>*</span>���:</label>
									<select name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>">
										<?
										 $sKey = (intval($propertyID) > 0)?"ELEMENT_PROPERTIES":"ELEMENT";

										 foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum): ?>
											<?
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
													if ($key == $arElEnum["VALUE"] ) {
														$checked = true;
														break;
													}
												}
											} elseif ($arEnum["DEF"] == "Y") {
												$checked = true;
											}
											?>
											<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
										<? endforeach; ?>
									</select>
								  </div>
								</div>
								<? break; ?>
							<? default: ?>
								<? break; ?>
						<? endswitch; ?>
					<? endif; ?>
					<?
						if (isset($arResult["ERRORS"][$propertyID])) {
					?>
						<div style="color: red; margin-top: -15px; margin-left: 111px;">
						<?
							echo $arResult["ERRORS"][$propertyID];
						?>
						</div>
					<?
						}
					?>

				<? endforeach; ?>
			<? endif; ?>
		</div>
		<? $firstTab = false; ?>
	<? endforeach; ?>
	<br/>

	<input type="hidden" id="hidden_main_photo" name="HIDDEN_MAIN_PHOTO" value="<?=$main_photo_value;?>" />
	
	<div class="buttons-group">
		<input type="submit" formaction="<?= POST_FORM_ACTION_URI ?>" formmethod="post" formenctype="multipart/form-data" onclick="$(this).attr({'onclick':'return false;'})" value="<?if(empty($_GET["PRODUCT_ID"])):?>�������� ����������<?else:?>��������� ���������<?endif?>" name="iblock_submit" class="adv-add-btn">
		<input type="submit" formtarget="_blank" name="preview" formaction="?preview=Y" formmethod="get" onclick="" value="������������" class="adv-preview">
	</div>
	<script type="application/javascript">
		$(function(){

			var region = $('#page_data').data('regioncode');
		    var city = $('#page_data').data('citycode');
			$('select[name="PROPERTY['+region+']"]').change();
		})
	</script>
</form>
