$(document).ready(function(){
    var region = $('#page_data').data('regioncode');
    var city = $('#page_data').data('citycode');
    var phoneInputCount = parseInt($('#phone_input_num').val()) + 1;
    $('select[name="PROPERTY['+region+']"]').change(function(){
        var regionId = $(this).val();

        $.ajax({
            type: 'POST',
            url: '/bitrix/components/whipstudio/product.add.form/templates/advert/get_cities.php',
            data: {"regionID": regionId},
            success: function(data) {
                $('#sel_cities').html(data);
                $('#sel_cities').val($('#sel_cities').data('value'));
            }
        });
    });


    $('.add-btn.act').click(function(){
        $(this).parent().find('input[type="file"]').click();
    });
    $('.add-file').click(function(){
        $(this).parent().find('input[type="file"]').click();
    });
    $('#PROPERTYPREVIEWTEXT0').click(function(){

        var previewText = $('#PROPERTYDETAILTEXT0').val();
        previewText = previewText.replace(/<\/?[^>]+>/g, '');
        previewText = escapeHtml(previewText);
        if (previewText.length > 130){
            previewText = previewText.substr(0, 127) + '...';
        }

        $(this).val(previewText);

    });

    var maskList = $.masksSort($.masksLoad("/bitrix/templates/.default/js/phone/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
        var maskOpts = {
            inputmask: {
                definitions: {
                    '#': {
                        validator: "[0-9]",
                        cardinality: 1
                    }
                },
                //clearIncomplete: true,
                showMaskOnHover: false,
                autoUnmask: true
            },
            match: /[0-9]/,
            replace: '#',
            list: maskList,
            listKey: "mask",
        };

    $('.more-numbers').click(function(){
        $(this).parent().before('<div class="phone">'+
        '<div class="min">' +
        '<label>' +
        '</label>' +
        '</div>' +
        '<div class="min" style="padding-left: 3px;">' +
        '<span></span>' +
        '<input type="text" class="extPhone" name="PROPERTY[249]['+phoneInputCount+']" size="25" value="">' +
        '</div>' +
        '<div class="min">' +
        '<span class="t-tip"></span>'+
        '</div>' +
        '' +
        '</div>');
        phoneInputCount++;

        $('.extPhone').inputmasks(maskOpts);

    });



    $('.extPhone').inputmasks(maskOpts);


});

function escapeHtml(text) {
    return text.replace(/&amp;/g, "&")
        .replace(/&lt;/g, "<")
        .replace(/&gt;/g, ">")
        .replace(/&quot;/g, '"')
        .replace(/&nbsp;/g, ' ')
        .replace(/&#039;/g, "'");
}

function CheckForm(){
    var   $inpName = $("#insertNameThere"),
        $inpName1 = $(".js_inp_name_1"),
        $inpName2 = $(".js_inp_name_2"),
        $inpName3 = $(".js_inp_name_3");

    if($inpName.size() > 0)
    {
        if($inpName1.val().length > 0)
        {
            if($inpName2.val().length > 0)
                $inpName.val($inpName1.val() + " " + $inpName2.val());
            else if($inpName3.val().length > 0)
                $inpName.val($inpName1.val() + " " + $inpName3.val());
        }
    }

    return true;
}

function insertRow($block, defVal){
    var $el = $block.find(".elements input"),
        count = $el.size(),
        $newEl = $el.first().clone();
    $newEl.attr("name", $newEl.attr("name").replace(/PROPERTY\[(\d+)\]\[(\d+)\]/gi, 'PROPERTY[$1][' + count + ']'));
    $newEl.val(defVal);
    $block.find(".elements").append($newEl);
}

function insertPhoneRow($block, defVal){
    var $el = $block.find(".elements .row"),
        count = $el.size(),
        $newEl = $el.first().clone();

    $newEl.find("input").eq(0).attr("name", $newEl.find("input").eq(0).attr("name").replace(/PROPERTY\[(\d+)\]\[(\d+)\]/gi, 'PROPERTY[$1][' + count + ']'));
    $newEl.find("input").eq(1).attr("name", $newEl.find("input").eq(1).attr("name").replace(/DESCRIPTION\[(\d+)\]\[(\d+)\]/gi, 'DESCRIPTION[$1][' + count + ']'));
    $newEl.find("input").eq(0).val(defVal);
    $newEl.find("input").eq(1).val("");
    $block.find(".elements").append($newEl);
}

function showOnMap(){
    var string = [];
    if(~~$('.js_country_sel').val()) string.push($('.js_country_sel option[value='+$('.js_country_sel').val()+']').text());
    if(~~$('.js_city_sel').val()) string.push($('.js_city_sel option[value='+$('.js_city_sel').val()+']').text());
    if($('.js-address-input').val().length) string.push($('.js-address-input').val());
    if(string.length == 3){
        string = string.join(", ");
        if(string.length){
            var myGeocoder = ymaps.geocode(string);
            myGeocoder.then(
                function (res) {
                    $('.js-coords-input').val(res.geoObjects.get(0).geometry.getCoordinates());
                    if(typeof myPlacemark != 'undefined'){
                        myMapForCompany.geoObjects.remove(myPlacemark);
                    }
                    myPlacemark = new ymaps.Placemark(res.geoObjects.get(0).geometry.getCoordinates(), {
                        hintContent: '�������� �����'
                    }, {
                        draggable: true // ����� ����� �������������, ����� ����� ������ ����.
                    });
                    myPlacemark.events.add("dragend", function (event) {
                        $('.js-coords-input').val(myPlacemark.geometry.getCoordinates());
                    });
                    myMapForCompany.geoObjects.add(myPlacemark);
                    myMapForCompany.setCenter(myPlacemark.geometry.getCoordinates());
                },
                function (err) {
                    console.log("���������� �� �������");
                }
            );
        }
    }
}

function AddMorePhoto(){
    fd = new FormData();
    fd.append('user_file', $('#MORE_PHOTO')[0].files[0]);
    if ($('input[name="MORE_PHOTO\[\]"]').length == 10){
        alert('���������� ����������� �� ������ ���� ������ 10');
        return;
    }
    if ($('#div_more_photo span.act').hasClass('add-btn-dis')){
        return;
    }
    $('#MORE_PHOTO').attr('onclick', 'return false;');
    $('#div_more_photo span.add-btn').removeClass('add-btn').addClass('add-btn-dis');
    $.ajax({
        type: 'POST',
        url: '/bitrix/components/whipstudio/product.add.form/templates/advert/add_more_image.php',
        data: fd,
        processData: false,
        contentType: false,
        success: function(data) {
            var result = jQuery.parseJSON(data);
            if (result.success)
            {
                $('#div_more_photo input[type="file"]').parent().before(result.message);
                $('#div_more_photo span.add-btn-dis').removeClass('add-btn-dis').addClass('add-btn');
                $('#MORE_PHOTO').attr('onclick', '');
                $('#MORE_PHOTO').val('');
                //$(result.message).prependTo($('#div_more_photo input[type="file"]').parent());
            }
            else
            {
                $('#div_more_photo span.add-btn-dis').removeClass('add-btn-dis').addClass('add-btn');
                $('#MORE_PHOTO').attr('onclick', '');
                alert(result.message);
            }
        },
        error: function(data) {
            $('#div_more_photo span.add-btn-dis').removeClass('add-btn-dis').addClass('add-btn');
            $('#MORE_PHOTO').attr('onclick', '');
            //alert('Unknown error');
        }
    });
}

function AddMoreFile(){
    fd = new FormData();
    fd.append('user_file_up', $('#MORE_FILE')[0].files[0]);

    $('#MORE_FILE').attr('onclick', 'return false;');
    $('#div_more_files span.add-file').removeClass('add-file').addClass('add-file-dis');

    $.ajax({
        type: 'POST',
        url: '/bitrix/components/whipstudio/product.add.form/templates/advert/add_more_file.php',
        data: fd,
        processData: false,
        contentType: false,
        success: function(data) {
            var result = jQuery.parseJSON(data);
            if (result.success)
            {
                $(result.message).prependTo($("#div_more_files"));
                $('#MORE_FILE').attr('onclick', '');
                $('#MORE_FILE').val('');
                $('#div_more_files span.add-file-dis').removeClass('add-file-dis').addClass('add-file');
            }
            else
            {
                $('#MORE_FILE').attr('onclick', '');
                $('#div_more_files span.add-file-dis').removeClass('add-file-dis').addClass('add-file');
                alert(result.message);
            }
        },
        error: function(data) {
            $('#MORE_FILE').attr('onclick', '');
            $('#div_more_files span.add-file-dis').removeClass('add-file-dis').addClass('add-file');
            //alert('Unknown error');
        }
    });
}


//���������� ������
function ClearAndReset($select, $chozen){                       
   $select.find("option[value=0]").attr("selected", "selected");
   $select.attr("disabled", "disabled");
   $chozen.trigger("liszt:updated");
}

function SetManual($select, $chozen, $input){
   $select.find("option[value=-1]").attr("selected", "selected");
   $select.attr("disabled", "disabled");
   $chozen.trigger("liszt:updated");
   $input.val("").removeClass("hidden");
}

function SyncFindOnMapLink(){
   if(~~$(".js_country_sel").val() && ~~$(".js_region_sel").val() && ~~$(".js_city_sel").val() && $(".js-address-input").val().length)
      $('.js-address-showonmap').show();
   else
      $('.js-address-showonmap').hide();
}

$(document).ready(function(){

    $(".buttons-group input[name=preview]").click(function(){//�������� ��� �������������, ���� �� ��������� ���� ����
        var detailText = escapeHtml($('div.redactor_editor').html().replace(/<\/?[^>]+>/g, ''));
        var titleText = $('textarea[name="PROPERTY[NAME][0]"]').val();
        $('#PROPERTYDETAILTEXT0').val($('div.redactor_editor').html());
        var error = '';
        var bPhoneError = true;
        $('input[name^="PROPERTY[249]"], input[name^="PROPERTY[734]"]').each(function(index, element){
            if (element.value != ""){
                bPhoneError = false;
                return true;
            }
        });
        if (bPhoneError){
            error += '�� ��������� ������������ ���� �������';
        }
        if (titleText.length == 0){
            error += '\n�� ��������� ������������ ���� ���������';
        }
        if (detailText.length == 0){
            error += '\n�� ��������� ������������ ���� ��������� ��������';
        }
        if (error.length == 0)
        {
            CheckForm();
            return true;
        }
        else
        {
            alert(error);
            return false;
        }
    });

    $("#MORE_FILE").change(function(evt){  //�������� �� ������ �����
        var files = evt.target.files;
        var fileExt = files[0].name.substring(files[0].name.lastIndexOf('.') + 1).toLowerCase();
        if (fileExt != 'pdf' && fileExt != 'xls' && fileExt != 'xlsx' && fileExt != 'doc' && fileExt != 'docx' && fileExt != 'rtf'
            && fileExt != 'ppt' && fileExt != 'pptx' && fileExt != 'jpg' && fileExt != 'jpeg' && fileExt != 'png' && fileExt != 'gif')
        {
            $(this).val('');
            alert('������������ ��� �����');
            evt.stopPropagation();
            return false;
        }
        if (files[0].size <= 3145728)
        {
            AddMoreFile('#add-adv-form');
        }
        else
        {
            $(this).val('');
            alert("�� ��������� ���������� ������ �����");
            evt.stopPropagation();
            return false;
        }
    });


    $("#MORE_PHOTO").change(function(evt){    //  �������� �� ������ ����������
        var files = evt.target.files;
        var fileExt = files[0].name.substring(files[0].name.lastIndexOf('.') + 1).toLowerCase();
        if (fileExt != 'jpg' && fileExt != 'jpeg' && fileExt != 'png' && fileExt != 'gif')
        {
            $(this).val('');
            alert('������������ ��� �����');
            evt.stopPropagation();
            return false;
        }
        if (files[0].size<=3145728)
        {
            AddMorePhoto('#add-adv-form');
        }
        else
        {
            $(this).val('');
            alert("�� ��������� ���������� ������ �����");
            evt.stopPropagation();
            return false;
        }
    });

   //�� ���������� ����� �� enter
   $('.bx-company-table input').live("keydown", function(event){
      if(13==event.keyCode)
         return false;
   });
});


//���������
//������
function showErrorFunc($element, text){
   var $tr = $element.closest('tr');
   $tr.addClass("error");
   $tr.find('.errortext').text(text);
}
function hideErrorFunc($element){
   var $tr = $element.closest('tr');
   if($tr.hasClass("error")){
      $tr.removeClass("error");
      $tr.find('.errortext').text("");
   }
}
//��������
function fieldEmptyFunc($element){
   return $element.val().length
}
function fieldMinMaxFunc($element){
   return (($element.val().length >= 3) && ($element.val().length <= 50))
}
function fieldSymbolsFunc($element){
   return checkStringJS($element.val())
}
function fieldEmailFunc($element){
   return checkEmailJS($element.val())
}
function fieldShortPassFunc($element){
   return ($element.val().length >= 6)
}
function fieldConfirmPassFunc($element){
   return ($element.val() == $('#register-form-password').val())
}
function fieldContractFunc($element){
   return ($element.prop("checked"))
}

//����������
validName = validation({//���
   selector: '#register-form-name',
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "��� �� ���������"
   },{
      compare: fieldMinMaxFunc,
      errortext: "��� ������ ��������� �� 3 �� 50 ��������"
   },{
      compare: fieldSymbolsFunc,
      errortext: "������ ������� �������� � ����������� �������� � �����"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});

validLastname = validation({//�������
   selector: "#register-form-lastname",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "������� �� ���������"
   },{
      compare: fieldMinMaxFunc,
      errortext: "������� ������ ��������� �� 3 �� 50 ��������"
   },{
      compare: fieldSymbolsFunc,
      errortext: "������ ������� �������� � ����������� �������� � �����"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});

validEmail = validation({//�����
   selector: "#register-form-email",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "����� ����������� ����� �� ��������"
   },{
      compare: fieldEmailFunc,
      errortext: "������������ ����� ����������� �����"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});

validPassword = validation({
   selector: "#register-form-password",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "������ �� ��������"
   },{
      compare: fieldShortPassFunc,
      errortext: "����������� ����� ������ - 6 ��������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});

validConfirm = validation({
   selector: "#register-form-confirm",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "��������� ������"
   },{
      compare: fieldConfirmPassFunc,
      errortext: "������ �� ���������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});

validCaptcha = validation({
   selector: '#register-form-captcha',
   require: [
      {
         compare: fieldEmptyFunc,
         errortext: "������� ���"
      }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
});

validContract = validation({
   selector: '#register-form-contract',
   require: [
      {
         compare: fieldContractFunc,
         errortext: "��������� ������"
      }
   ],
   showError: function(){},
   hideError: function(){}
});