<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;

$resize_w = 100;
$resize_h = 100;


if(strlen($arResult["SECTION"]["PATH"][0]["NAME"]))
{
	$i = count($arResult["SECTION"]["PATH"])-1;// �������� ��������� ������
   SetSectionMeta($arResult["SECTION"]["PATH"][$i], array(
       "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
       "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
       "META_KEYWORDS" => $arParams["META_KEYWORDS"]
   ));
}

/*if(strlen($arResult["SECTION"]["PATH"][0]["NAME"]))
{
   $titleSection = ": {$arResult["SECTION"]["PATH"][0]["NAME"]}";
   $descriptionSection = $arResult["SECTION"]["PATH"][0]["NAME"];
   $APPLICATION->SetTitle("�������� ��������{$titleSection} - ������, ����, ����� � �������� ����������� -EnergoBelarus.by");
   $APPLICATION->SetPageProperty("description", "{$descriptionSection} - ��� �������� � �������� � ������, ������� � ���������� � �������� EnergoBelarus.by"); 
   $APPLICATION->AddHeadString('<meta name="abstract" content="'.$descriptionSection.' - ��� �������� � �������� � ������, ������� � ���������� � �������� EnergoBelarus.by">', true);
   $APPLICATION->SetPageProperty("keywords", "{$descriptionSection}, ���������� �������������, ��������, �����, ���������, ����������, ��������");
}*/

foreach ($arResult["ITEMS"] as $key => $arItem)
{
   if(is_array($arItem["PREVIEW_PICTURE"]) && count($arItem["PREVIEW_PICTURE"]))
   {
      if(($arItem["PREVIEW_PICTURE"]["WIDTH"] > $resize_w) || ($arElement["PREVIEW_PICTURE"]["HEIGHT"] > $resize_h))
      {
         $arFileTmp = CFile::ResizeImageGet(
               $arItem["PREVIEW_PICTURE"],
               array("width" => $resize_w, "height" => $resize_h),
               BX_RESIZE_IMAGE_PROPORTIONAL,
               false
          );
         $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
                  "SRC" => $arFileTmp["src"],
                  "WIDTH" => $resize_w,
                  "HEIGHT" => $resize_h,
          );
      }
   }
   if(!strlen($arItem["PREVIEW_TEXT"]))
      $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = $obParser->html_cut($arItem["DETAIL_TEXT"], 100);
}