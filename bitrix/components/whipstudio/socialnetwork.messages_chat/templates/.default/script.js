function ShowError(msg)
{
   $(".comments_box .js-send-good").hide(); 

   $(".js-send-box .js-send-textarea").addClass("error-textarea");//.val("");
   $(".js-send-box .js-send-error").text(msg).show();
   $(".js-send-box .js-send-textarea").removeAttr("disabled");

}
function HideError()
{
   $(".js-send-box .error").hide().text("");   
   $(".js-send-box .js-send-textarea").removeClass("error-textarea").val("");
}
function ShowGood(msg)
{
   HideError();
   $(".comments_box .js-send-good").text(msg).show();
   setTimeout(HideGood, 2000);
}
function HideGood()
{
   $(".comments_box .js-send-good").fadeOut('slow', function(){
      //$(".right-block-content .comments_box #COMMENT_TEXT").val("");
      //$(".right-block-content .comments_box #COMMENT_TEXT").removeAttr("disabled");
      });  
}
function HideAll()
{
   HideGood();
   HideError();
}

var sendTimer = null; // ������ �������� �������������
var sendArr = new Array(); // ������ id ��� ��������� �������������

$(document).ready(function(){
   $(".right-block-content .messages-block .message-control .delete").live("click", function(){
      if(confirm("Are you sure you want to delete this message?")){
         var row = $(this).closest(".message-row");
         var id = row.attr("id");
         id = ~~id.replace("rowMessage", "");
         $.ajax({
            url: '/_ajax/messages/remove_message_list.php',
            data: 'MESSAGE_ID=' + id,
            dataType: 'json',
            success: function (data){
               if(!data.ERROR){
                  row.fadeOut(400).remove(); 
                  if(!$(".messages-block .message-row").size() > 0) // ���������� �����������
                     $(".messages-block .placeholder").show();
               }else{ // ������
                  alert("What is happend?");
               }
            }
         });
      }
   });
    
   //������� ������ � �������� �����
   $('.js-send-textarea').keypress(function(key){
      if(key.keyCode == 13){
         if(key.shiftKey == false){
            $(key.target).closest('.js-send-box').find('.js-send-button').trigger('click');
         }else{
            var textarea = $('.js-send-textarea');
            textarea.val(textarea.val() + '\n');
         }
         return false;
      }
   });
    
   $('.message-row.take.no-read').live("mouseover", function(){ 
      var $row = $(this);
      var id = $row.attr("id");
      id = ~~id.replace("rowMessage", "");
      if($.inArray(id, sendArr) == -1){
         sendArr.push(id);        
         if(typeof(sendTimer) != "undefined")
            clearTimeout(sendTimer);
         sendTimer = setTimeout(setReadStatus, 1000);
      }
   });

});

function setReadStatus(){    
   if(sendArr.length > 0){
      $.post('/_ajax/messages/read_message.php',
         'MESSAGE_IDs=' + sendArr,
         function (data){
            for(var i in sendArr) {
               if (!sendArr.hasOwnProperty(i)) continue;
               $('#rowMessage' + sendArr[i]).removeClass("no-read");
            }  
            //setGlobalCounter(--GLOBALfavCounter);
            sendArr = new Array();                
         }
         );        
   }   
}
