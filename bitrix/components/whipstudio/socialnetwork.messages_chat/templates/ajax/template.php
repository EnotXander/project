<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$isError = false;
$arIDs = array();

if ((!$arResult["ERROR"]) && ($arResult["NEED_AUTH"] != "Y") && is_array($arResult["MESSAGES"]) && count($arResult["MESSAGES"]))
{
   ob_start();
   foreach ($arResult["MESSAGES"] as $key => $event)
   {
      $arIDs[] = $event["ID"];
      ?><div class = "message-row <?= $event["FROM_USER_ID"] != $arResult["arUser"]["ID"] ? 'take' : ''; ?> <?= $event["IS_READ"] ? "" : "no-read" ?>" id = "rowMessage<?= $event["ID"] ?>">
         <div class = "photo">
            <? if ($event["FROM_USER_ID"] != $arResult["arUser"]["ID"]):
               ?>
               <? if (is_array($arResult["PARTNER_INFO"]["PERSONAL_PHOTO"])): ?>
                  <img src="<?=$arResult["PARTNER_INFO"]["PERSONAL_PHOTO"]["SRC"] ?>" alt="<?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>" title="<?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>" />
               <? else: ?>
                  <img src="/images/search_placeholder.png" width="48" height="42" alt="<?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>" title="<?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>" />
               <? endif; ?> 
            <? else: ?>
               <? if (is_array($arResult["arUser"]["PERSONAL_PHOTO"])): ?>
                  <img src="<?=$arResult["arUser"]["PERSONAL_PHOTO"]["SRC"] ?>" alt="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" title="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" />
               <? else: ?>
                  <img src="/images/search_placeholder.png" width="48" height="42" alt="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" title="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" />
               <? endif; ?>                                 
            <? endif; ?>
         </div>
         <div class="message-control">
            <div class="delete" title="Delete message">X</div>
         </div>
         <div class="info">
            <div>
               <? if ($event["FROM_USER_ID"] != $arResult["arUser"]["ID"]): ?>
                  <?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>
               <? else: ?>
                  <?= $arResult["arUser"]["FORMATED_NAME"] ?>
               <? endif; ?>

               <div class="date">
                  <?= $event["DATE_CREATE"] ?>
               </div>
            </div>
            <div class="message-text">
               <?= $event["MESSAGE"]; ?>
            </div>
         </div>                    
      </div><?
   }
   $HTML = ob_get_clean();
   $COUNT = count($arResult["MESSAGES"]);
   $LAST = $arResult["MESSAGES"][count($arResult["MESSAGES"])-1]["ID"];
}

$HTML = iconv("cp1251", "UTF-8", $HTML);
echo json_encode(array(
    "ERROR" => $isError,
    "HTML" => $HTML,
    "COUNT" => $COUNT,
    "LAST" => $LAST,
));