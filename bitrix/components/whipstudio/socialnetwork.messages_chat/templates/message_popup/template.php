<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<div id="rubrikator_section" class="popup" style="display: none; top:20%; left:35%">
    <div class="popup_mask" style="display: block;">&nbsp;</div>
    <div class="popup_header">
        <span class="alt_h2">����� ���������</span>
        <span class="sprite close_btn" href="javascript:void()">X</span>
    </div>
    <div class="rubrikator_body">
        <div class="friends_box">
           <select class="select_location js_user_sel" name="PARTNER">
               <option value="0">�������� �����������</option>
               <? foreach ($arResult["USERS"] as $key => $val): ?>
                  <option value = "<?= $val["ID"] ?>"><?= $val["NAME"]." ".$val["LAST_NAME"] ?></option>
               <? endforeach; ?>
            </select>
        </div>
        <div class="add-more"><a>+ Add more</a></div>
        <div class="comments_box">
            <div>
                <textarea id="COMMENT_TEXT" rows="14" cols="55" class="post_message gray-text" placeholder="������� ���� ���������"></textarea>
            </div>
            <a class="btn-style orange send-button" href="javascript: void(0);"><i></i>Send</a>
            <div class="inputnote">shift+enter - line break<br />enter - send</div>
            <div class="error">Testiruem</div>
            <div class="good">Testiruem</div>
        </div>
    </div>
</div>

<script type="text/javascript">
function ShowError(msg)
{
    $(".comments_box .good").hide(); 
    
    $(".rubrikator_body .comments_box #COMMENT_TEXT").addClass("error-textarea");//.val("");
    $(".comments_box .error").text(msg).show();
    $(".rubrikator_body .comments_box #COMMENT_TEXT").removeAttr("disabled");
    
}

function HideError()
{
    $(".comments_box .error").hide().text("");   
    $(".rubrikator_body .comments_box #COMMENT_TEXT").removeClass("error-textarea").val("");
}

function ShowGood(msg)
{
    HideError();
    $(".comments_box .good").text(msg).show();
    setTimeout(HideGood, 2000);
}

function HideGood()
{
    $(".comments_box .good").fadeOut('slow', function(){
        $(".rubrikator_body .comments_box #COMMENT_TEXT").val("");
        $(".rubrikator_body .comments_box #COMMENT_TEXT").removeAttr("disabled");
    });  
}

function HideAll()
{
    HideGood();
    HideError();
}

$(document).ready(function(){
    textarea = $(".rubrikator_body .comments_box #COMMENT_TEXT");
    //������� ������ � �������� �����
    $('#COMMENT_TEXT').keypress(function(key)
    {
        if(key.which == 13)
        {
            if(key.shiftKey == false) $('.send-button').trigger('click');
            else
            {
                textarea = $('#COMMENT_TEXT');
                textarea.val(textarea.val() + '\n');
            }
            return false;
        }
    });
    // �������� ��������� ������������
    $(".rubrikator_body .comments_box .send-button").live("click", function(){
        jsonPartners = '';
        partners = ~~$(this).closest('.rubrikator_body').find('.js_user_sel').val();
        if(partners)
        {
            message = textarea.val();
            if(message.length > 0)
            {
                textarea.attr("disabled","disabled");
                $.ajax({
                    type: "POST",
                    url: '/_ajax/messages/send_message.php',
                    data: 'PARTNERS_ID=[' + partners + ']&MSG=' + message,
                    dataType: 'json',
                    success: function (data)
                    {
                        if(!data.ERROR)
                        {
                            HideError();
                            textarea.addClass('gray-text');
                            document.location = '/personal/messages/' + partners + '/';
                        } else // ������
                        {
                            ShowError("Error: " + data.ERROR_TEXT);
                        }
                    },
                    complete: function()
                    {
                        //textarea.removeAttr("disabled");
                    }
                });
            } else // ������� �������� ���������
            {
                ShowError("Error: Enter the message text");
            }
        }
        else // ������� ��� ����������� �� ������
            {
                ShowError("Error: Enter the name of partner");
            }
    });
});
</script>