<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

    <div class="main_box_planka">
        <h1>Chat with <?=$arResult["PARTNER_INFO"]["FORMATED_NAME"]?></h1>
        <div class="head-button">
            <a href="/social_network/personal/messages/" class="btn-style back"><i></i><span></span>Back to messages</a>
        </div>
    </div>




    <div class="right-block-content">
        <div class="messages-block">
            <!--<input id="partner_id" type="text"></input>-->
            <?$APPLICATION->IncludeComponent("whipstudio:user.suggest.input", "", Array(
                    "DROPDOWN_SIZE" => 10,
                    "MIN_LETTER_IN_QUERY" => 2,
                    "SHOW_ADD_BUTTON" => "Y",
                    "ACTION" => "SUGGEST"
                )
            );?>
        </div>

        <div class="comments_box">
            <div>
                <div class="send-img">
                    <?if(isset($arResult["arUser"]["PERSONAL_PHOTO"])):?>
                        <img src="<?=resizer("/thumb/74x55xin".$arResult["arUser"]["PERSONAL_PHOTO"]["SRC"])?>" alt="<?=$arResult["arUser"]["FORMATED_NAME"]?>" title="<?=$arResult["arUser"]["FORMATED_NAME"]?>" />
                    <?else:?>
                        <img src="http://dummyimage.com/74x55/777777/FFFFFF.png&text=no+photo" alt="<?=$arResult["arUser"]["FORMATED_NAME"]?>" title="<?=$arResult["arUser"]["FORMATED_NAME"]?>" />
                    <?endif;?>
                </div>
                <textarea id="COMMENT_TEXT" rows="14" cols="55" class="post_message"></textarea>
            </div>
            
            <a class="btn-style send-button" href="javascript: void(0);"><i></i>Send</a>
            <div class="inputnote" style="float: left; margin-left: 15px;">shift+enter - line break<br />enter - send</div>
            <div class="error">Testiruem</div>
            <div class="good">Testiruem</div>
        </div>
        
        <div style="clear:both"></div>
    </div>

<script type="text/javascript">
function ShowError(msg)
{
    $(".comments_box .good").hide(); 
    
    $(".right-block-content .comments_box #COMMENT_TEXT").addClass("error-textarea").val("");
    $(".comments_box .error").text(msg).show();
    $(".right-block-content .comments_box #COMMENT_TEXT").removeAttr("disabled");
    
}

function HideError()
{
    $(".comments_box .error").hide().text("");   
    $(".right-block-content .comments_box #COMMENT_TEXT").removeClass("error-textarea").val("");
}

function ShowGood(msg)
{
    HideError();
    $(".comments_box .good").text(msg).show();
    setTimeout(HideGood, 2000);
}

function HideGood()
{
    $(".comments_box .good").fadeOut('slow', function(){
        $(".right-block-content .comments_box #COMMENT_TEXT").val("");
        $(".right-block-content .comments_box #COMMENT_TEXT").removeAttr("disabled");
    });  
}

function HideAll()
{
    HideGood();
    HideError();
}

$(document).ready(function(){
    textarea = $(".right-block-content .comments_box #COMMENT_TEXT");
    //перенос строки и отправка формы
    $('#COMMENT_TEXT').keypress(function(key)
    {
        if(key.which == 13)
        {
            if(key.shiftKey == false) $('.send-button').trigger('click');
            else
            {
                textarea = $('#COMMENT_TEXT');
                textarea.val(textarea.val() + '\n');
            }
            return false;
        }
    });
    // отправка сообщения пользователю
    $(".right-block-content .comments_box .send-button").live("click", function(){
        partnerID = $('.right-block-content').find('.partnerID').text();
        message = textarea.val();
        if(partnerID.length > 0)
        {
            if(message.length > 0)
            {
                textarea.attr("disabled","disabled");
                $.ajax({
                    url: '/_ajax/social/send_message.php',
                    data: 'PARTNER_ID=' + partnerID + '&MSG=' + message,
                    dataType: 'json',
                    success: function (data)
                    {
                        if(!data.ERROR)
                        {
                            HideError();
                            document.location = '/social_network/personal/messages/chat.php?user_id=' + partnerID;
                        } else // ошибка
                        {
                            ShowError("Error: " + data.ERROR_TEXT);
                        }
                    },
                    complete: function()
                    {
                        //textarea.removeAttr("disabled");
                    }
                });
            } else // слишком короткое сообщение
            {
                ShowError("Error: Enter the message text");
            }
        }
        else // слишком имя отправителя не задано
            {
                ShowError("Error: Enter the name of partner");
            }
    });
});
</script>