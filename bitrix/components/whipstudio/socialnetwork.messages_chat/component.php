<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("socialnetwork"))
{
   ShowError(GetMessage("SONET_MODULE_NOT_INSTALL"));
   return;
}


$arParams["USER_ID"] = (int) $arParams["USER_ID"];

$arParams["PATH_TO_SMILE"] = trim($arParams["PATH_TO_SMILE"]);


if (!$USER->IsAuthorized())
   $arResult["NEED_AUTH"] = "Y";
else
{
   if ($arParams["USER_ID"] == $USER->GetID())
      LocalRedirect("/personal/messages/");

   // �������� ���������� � ����
   $result = $USER->GetList(($by = "personal_country"), ($order = "desc"), array("ID" => $USER->GetID()), array("SELECT" => array("PERSONAL_PHOTO", "NAME", "LAST_NAME", "LOGIN")));
   $arResult["arUser"] = $result->Fetch();
   if ($arResult["arUser"]["PERSONAL_PHOTO"] > 0)
      $arResult["arUser"]["PERSONAL_PHOTO"] = CFile::GetFileArray($arResult["arUser"]["PERSONAL_PHOTO"]);
   if (strlen($arResult["arUser"]["NAME"]) || strlen($arResult["arUser"]["LAST_NAME"]))
   {
      $arResult["arUser"]["FORMATED_NAME"] = $arResult["arUser"]["NAME"];
      if (strlen($arResult["arUser"]["FORMATED_NAME"]))
         $arResult["arUser"]["FORMATED_NAME"] .=" ";
      $arResult["arUser"]["FORMATED_NAME"] .= $arResult["arUser"]["LAST_NAME"];
   }
   else
      $arResult["arUser"]["FORMATED_NAME"] = $arResult["arUser"]["LOGIN"];

   if ($arParams["USER_ID"] > 0 && $arParams["USER_ID"] != $USER->GetID())//���� ����� ��������� ����������� ������������
   {
      // �������� ���������� � ��������
      $result = $USER->GetList(($by = "personal_country"), ($order = "desc"), array("ID" => $arParams["USER_ID"]), array("SELECT" => array("PERSONAL_PHOTO", "NAME", "LAST_NAME", "LOGIN")));
      $arResult["PARTNER_INFO"] = $result->Fetch();
      if ($arResult["PARTNER_INFO"]["PERSONAL_PHOTO"] > 0)
         $arResult["PARTNER_INFO"]["PERSONAL_PHOTO"] = CFile::GetFileArray($arResult["PARTNER_INFO"]["PERSONAL_PHOTO"]);
      if (strlen($arResult["PARTNER_INFO"]["NAME"]) || strlen($arResult["PARTNER_INFO"]["LAST_NAME"]))
      {
         $arResult["PARTNER_INFO"]["FORMATED_NAME"] = $arResult["PARTNER_INFO"]["NAME"];
         if (strlen($arResult["PARTNER_INFO"]["FORMATED_NAME"]))
            $arResult["PARTNER_INFO"]["FORMATED_NAME"] .=" ";
         $arResult["PARTNER_INFO"]["FORMATED_NAME"] .= $arResult["PARTNER_INFO"]["LAST_NAME"];
      }
      else
         $arResult["arUser"]["FORMATED_NAME"] = $arResult["arUser"]["LOGIN"];
      $parser = new CSocNetTextParser(LANGUAGE_ID, $arParams["PATH_TO_SMILE"]);
      $arResult["MESSAGES"] = false;

      $strSql = "SELECT  `ID`, `FROM_USER_ID`, `TO_USER_ID`, `TITLE`, `MESSAGE`, `DATE_CREATE`, `DATE_VIEW`, `MESSAGE_TYPE`, `FROM_DELETED`, `TO_DELETED`, `SEND_MAIL`, `EMAIL_TEMPLATE`, `IS_LOG`";
      $strSql .= "FROM `b_sonet_messages`";
      $strSql .= "WHERE ((`FROM_USER_ID` = {$DB->ForSql($USER->GetID())} AND `FROM_DELETED` LIKE 'N' AND `TO_USER_ID` = {$DB->ForSql($arResult["PARTNER_INFO"]["ID"])}) OR (`TO_USER_ID` = {$DB->ForSql($USER->GetID())} AND `TO_DELETED` LIKE 'N' AND `FROM_USER_ID` = {$DB->ForSql($arResult["PARTNER_INFO"]["ID"])}))";
      if($arParams["FROM"])
         $strSql .= " AND (`ID` > {$DB->ForSql($arParams["FROM"])})";
      $strSql .= "ORDER BY `ID` ASC";

      $rsMes = $DB->Query($strSql, true, $err_mess . __LINE__);

      if ($rsMes)
      {
         while ($mess = $rsMes->Fetch())
         {
            $strTime = GetMyTimeIntervalInSec(strtotime($mess["DATE_CREATE"]));
            $mess["TODAY"] = FormatDate("d", $strTime) == date("d") ? "Y" : "N";
            if ($mess["TODAY"] == "Y")
               $mess["DATE_CREATE"] = FormatDate("H:i:s", $strTime);
            else
               $mess["DATE_CREATE"] = FormatDate("M, d H:i:s", $strTime);

            $mess["MESSAGE"] = FormatText($mess["MESSAGE"], "text");

            // ����������� ������ ���������
            if (strlen($mess["DATE_VIEW"]) > 0)
               $mess["IS_READ"] = true;
            else
            {
               $mess["IS_READ"] = false;
               //CSocNetMessages::MarkMessageRead($USER->GetID(), $mess["ID"]);
            }

            $arResult["MESSAGES"][] = $mess;
         }
      } else
      {
         $arResult["ERROR"] = true;
      }

      if ($arParams["SET_TITLE"] == "Y")
         $APPLICATION->SetTitle("��� � {$arResult["PARTNER_INFO"]["FORMATED_NAME"]}");
         
      //������������� ��������� � textarea
      if((int)$_REQUEST["advert"])
      {
         $rsElement = CIBlockElement::GetByID((int)$_REQUEST["advert"]);
         if($arElement = $rsElement->GetNext())
            $arResult["COMMENT_TEXT"] = "����� �� ���������� {$arElement["NAME"]} :\n";
      }
      //������������� ��������� � textarea
      if((int)$_REQUEST["product"])
      {
         $rsElement = CIBlockElement::GetByID((int)$_REQUEST["product"]);
         if($arElement = $rsElement->GetNext())
            $arResult["COMMENT_TEXT"] = "{$arElement["NAME"]} :\n";
      }
   }
}

$this->IncludeComponentTemplate();
?>