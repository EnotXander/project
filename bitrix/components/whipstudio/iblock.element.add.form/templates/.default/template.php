<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
require_once( $_SERVER["DOCUMENT_ROOT"].$component->__path."/classes.php");
?>

<?if (count($arResult["ERRORS"]["MAIN"])):?>
   <?=ShowError(implode("<br />", $arResult["ERRORS"]["MAIN"]))?>
   <?//PrintObject($arResult["ERRORS"])?>
<?endif?>

<?if (strlen($arResult["MESSAGE"]) > 0):?>
   <?=ShowNote($arResult["MESSAGE"])?>
   <br /><br />
<?endif?>

<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
   <?=bitrix_sessid_post()?>
   
   <?if ($arParams["MAX_FILE_SIZE"] > 0):?>
      <input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" />
   <?endif?>

      
   <div class="mc_block company-tabs js-company-tabs">
      <ul class="mb_menu">
         <?$firstTab = true;
         foreach($arResult["PROPERTY_LIST"] as $tabKey => $arTab):
            if(!$arResult["ELEMENT_EXISTS"] && !$firstTab) break;?>
            <li<?=$firstTab ? ' class="mb_title sel" id="mb_title_f"' : ''?>><a href="javascript:void(0)"><?=$tabKey?></a><div class="sclew"></div></li>
            <?$firstTab = false;
         endforeach;?>
      </ul>
      <?$firstTab = true;
      foreach($arResult["PROPERTY_LIST"] as $arTab):
         if(!$arResult["ELEMENT_EXISTS"] && !$firstTab) break;?>
         <div class="news_comp mc_block tab">
            <table class="data-table bx-company-table">
               <?if (is_array($arTab) && !empty($arTab)):?>
                  <tbody class="bodyground">
                     <?//PrintObject($arTab);?>
                     <?foreach ($arTab as $propertyID):?>
                        <?if ($propertyID == 'EMPTY'):?>
                           <tr style="background-color:#ffffff;"><td colspan="3">&nbsp;</td></tr>
                           <?continue;
                        endif;?>
                        <tr>
                           <td class="txt_right" width="188" style="font-weight:bold;">
                              <?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?>
                                 <span class="starrequired">*</span>
                              <?endif?>

                              <?=!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : (intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] : GetMessage("IBLOCK_FIELD_".$propertyID))?>:                     
                           </td>

                           <td width="523" class="<?=isset($arResult["ERRORS"][$propertyID]) ? "error" : ""?>" style="padding-bottom: 10px;">
                              <?
                              if (intval($propertyID) > 0)
                              {
                                 if (
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                                    &&
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                                 )
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                                 elseif (
                                    (
                                       $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                                       ||
                                       $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                                    )
                                    &&
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                                 )
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
                              } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                                 $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

                              if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
                              {
                                 $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                                 $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
                              } else
                                 $inputNum = 1;


                              if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "HTML")
                                 $INPUT_TYPE = "HTML";
                              elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                                 $INPUT_TYPE = "USER_TYPE";
                              else
                                 $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

                              //PrintObject($arResult["PROPERTY_LIST_FULL"][$propertyID]);
                              //PrintObject($arResult["PROPERTY_LIST_FULL"][$propertyID]);
                              switch ($INPUT_TYPE):
                                 case "L":
                                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
                                       $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                                    else
                                       $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";  
                                    ?>
                                    <select class="js_section_sel main_select" name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
                                       <?if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                                          else $sKey = "ELEMENT";

                                       foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum):?>
                                          <?$checked = false;
                                          if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                          {
                                             foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
                                             {
                                                if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
                                             }
                                          } else
                                          {
                                             if ($arEnum["DEF"] == "Y") $checked = true;
                                          }?>
                                          <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
                                       <?endforeach;?>
                                    </select>
                                    <? 
                                    break;


                                 case "USER_TYPE":
                                    for ($i = 0; $i<$inputNum; $i++)
                                    {
                                       if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                       {
                                          $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
                                          $description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
                                       } elseif ($i == 0)
                                       {
                                          $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                          $description = "";
                                       } else
                                       {
                                          $value = "";
                                          $description = "";
                                       }
                                       //PrintAdmin($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"]);
                                       echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"], array(
                                          $arResult["PROPERTY_LIST_FULL"][$propertyID],
                                          array(
                                              "VALUE" => $value,
                                              "DESCRIPTION" => $description),
                                          array(
                                             "VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
                                             "DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
                                             "FORM_NAME"=>"iblock_add",
                                          ),
                                          ($propertyID == $arResult['id_const']['COUNTRY'] ? "js_country_sel" : ($propertyID ==  $arResult['id_const']['REGION'] ? "js_region_sel" : ($propertyID ==  $arResult['id_const']['CITY'] ? "js_city_sel" : "") )),
                                          !strlen($value) > 0 && $propertyID != $arResult['id_const']['COUNTRY']
                                       ));
                                    }
                                    break;

                                 case "TAGS":
                                    $APPLICATION->IncludeComponent("bitrix:search.tags.input", "", array(
                                          "VALUE" => $arResult["ELEMENT"][$propertyID],
                                          "NAME" => "PROPERTY[".$propertyID."][0]",
                                          "TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
                                       ), null, array("HIDE_ICONS"=>"Y")
                                    );
                                    break;
                                 case "HTML":
                                    $formId = preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]");
                                    ?><textarea id="<?=$formId?>" name="PROPERTY[<?=$propertyID?>][0]" rows="4" cols="5"><?=strlen($arResult["ELEMENT"][$propertyID]) ? $arResult["ELEMENT"][$propertyID] : $arResult["ELEMENT_PROPERTIES"][$propertyID][0]["VALUE"]["TEXT"]?></textarea>
                                    <script type="text/javascript">
                                       $(document).ready(function (){
                                           $('#<?=$formId?>').redactor({
                                              resize: true,
                                              lang: "ru",
                                              buttons: ['bold', 'italic', 'underline', 'RemoveFormat', 'link', 'orderedlist', 'unorderedlist'],
                                              //articleId: '<?=$arResult["REAL_ELEMENT"]["ID"]?>'
                                           });
                                           $('.redactor_btn_link').live("click", function(){
                                              console.log("fff");
                                           })
                                           console.log("redactor_modal", $('#redactor_modal').get())
                                       }); 
                                    </script>
                                    <?
                                    /*$LHE = new CLightHTMLEditor;
                                    $LHE->Show(array(
                                       'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
                                       'width' => '650px',
                                       'height' => '300px',
                                       'inputName' => "PROPERTY[".$propertyID."][0]",
                                       'content' => $arResult["ELEMENT"][$propertyID],
                                       'bUseFileDialogs' => false,
                                       'bFloatingToolbar' => false,
                                       'bArisingToolbar' => false,
                                       'toolbarConfig' => array(
                                          'Bold', 'Italic', 'Underline', 'RemoveFormat',
                                          'CreateLink',
                                          //'DeleteLink', 'Image', 'Video',
                                          //'BackColor', 'ForeColor',
                                          //'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
                                          'InsertOrderedList', 'InsertUnorderedList',
                                          //'Outdent', 'Indent',
                                          //'StyleList', 'HeaderList',
                                          //'FontList', 'FontSizeList',
                                       ),
                                    ));*/
                                    break;
                                 case "T":
                                    for ($i = 0; $i<$inputNum; $i++)
                                    {
                                       if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                       {
                                          $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                       } elseif ($i == 0)
                                       {
                                          $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                       } else
                                       {
                                          $value = "";
                                       } ?>
                                       <textarea cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
                                    <?
                                    }
                                    break;
                                    case "E":
                              if ($propertyID == GetPropertyId( "REGION_DOST", IBLOCK_COMPANY )) {
                                                                  echo 'ok';
                                 // REGION
                                       if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C"): ?>
                                          <? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio"; ?>
                                      <? else: ?>
                                          <? $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown"; ?>
                                      <? endif; ?>

                                              <?
                                              if (intval($propertyID) > 0)
                                                 $sKey = "ELEMENT_PROPERTIES";
                                              else
                                                 $sKey = "ELEMENT"; ?>

                                              <? foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum): ?>
                                                  <?
                                                  $checked = false;
                                                  if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                                      foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
                                                          if ($key == $arElEnum["VALUE"]) {
                                                              $checked = true;
                                                              break;
                                                          }
                                                      }
                                                  } elseif ($arEnum["DEF"] == "Y") {
                                                      $checked = true;
                                                  }
                                                  ?><div>
                                                  <input name="PROPERTY[<?= $propertyID ?>]" type="checkbox" value="<?= $arEnum["ID"]; ?>" <?= $checked ? " checked=\"checked\"" : ""; ?> id="reg<?= $arEnum["ID"]; ?>"/>
                                                  <label for="reg<?= $arEnum["ID"]; ?>"><?= $arEnum["NAME"]; ?></label>
                                                   </div>
                                              <? endforeach; ?>

                                      <?
                                    } else {
                                              }
                                    break;
                                 case "S":
                                 case "N":

                                    {

                                    if($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y"):?>
                                       <?if($propertyID == $arResult['id_const']['phone']):?>
                                          <div class="wrapper_<?=$propertyID?>">
                                             <div class="elements">
                                                ������ ��������� � ������� +�(���) ���-��-��
                                                <div class="row">
                                                   <?
                                                   for ($i = 0; $i<$inputNum; $i++)
                                                   {
                                                      if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                                      {
                                                         $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                                      } elseif ($i == 0)
                                                      {
                                                         $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                                      } else
                                                      {
                                                         $value = "";
                                                      } ?>
                                                      <input style="display: inline-block;" type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" class="extPhone" />
                                                      ��������:
                                                      <input style="display: inline-block; width: 215px;" type="text" name="DESCRIPTION[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=strlen($value) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : ""?>" />
                                                   <? } ?> 
                                                </div>
                                             </div> 
                                             <a href="javascript: void(0)" onclick="insertPhoneRow($('.wrapper_<?=$propertyID?>'), '<?=$value?>')">�������� ���</a>
                                          </div>
                                       <?else:?>                              
                                          <div class="wrapper_<?=$propertyID?>">
                                             <div class="elements">
                                                <?
                                                for ($i = 0; $i<$inputNum; $i++)
                                                {
                                                   if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                                   {
                                                      $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                                   } elseif ($i == 0)
                                                   {
                                                      $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                                   } else
                                                   {
                                                      $value = "";
                                                   } ?>

                                                   <input type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" />
                                                <? } ?>                               
                                             </div> 
                                             <a href="javascript: void(0)" onclick="insertRow($('.wrapper_<?=$propertyID?>'), '<?=$value?>')">�������� ���</a>
                                          </div>
                                       <?endif;?>
                                    <?else:?>
                                       <?
                                       for ($i = 0; $i<$inputNum; $i++)
                                       {
                                          if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                          {
                                             $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                          } elseif ($i == 0)
                                          {
                                             $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                          } else
                                          {
                                             $value = "";
                                          } ?>

                                          <?if($propertyID == "NAME" && !strlen($value) > 0):?>
                                             <input id="insertNameThere" type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="" />

                                             <input class="js_inp_name_1" type="text" />
                                             <? // �������� ������ ���
                                             $opf = companyForms();
                                             ?>
                                             <select class="js_inp_name_2" style="position: relative; top: 1px; height: 20px;">
                                                <?foreach($opf as $el):?>
                                                   <option value="<?=$el?>"><?=$el?></option>
                                                <?endforeach;?>
                                                <option value="">������ �������</option>
                                             </select>
                                             <input class="js_inp_name_3 " style="display: none; width: 120px;" type="text" />
                                                <div class="alert_exist js-make-predstavitel">

                                                </div>

                                          <?elseif($propertyID == $arResult['id_const']['MAP']): // �����?>
                                             <? // �������� �������� � ������ ������
                                             if(strlen($value) > 0)
                                             {
                                                $coord = explode(",", $value);
                                             } else
                                             {
                                                $coord = array(
                                                    53.90109,
                                                    27.547773
                                                );
                                             } ?>

                                             <?//PrintObject($arResult["PROPERTY_LIST_FULL"][$propertyID])?>
                                             <input class="js-coords-input" style="display: none;" type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" value="<?=$value?>" />
                                             <div id="yandex-map"></div>
                                             <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
                                             <script type="text/javascript">
                                             // ��� ������ ����� �������� API � ����� DOM, ��������� �������������
                                             ymaps.ready(init);
                                             function init(){
                                                myMapForCompany = new ymaps.Map('yandex-map', {                                         
                                                   center: [<?=$coord[0]?>, <?=$coord[1]?>], // �����
                                                   zoom: 11,
                                                   behaviors: ['default', 'scrollZoom'],
                                                });
                                                myMapForCompany.controls.add('zoomControl', {right: '5px', top: '5px'});
																myMapForCompany.events.add('dblclick', function (e) {
																	//console.log( e.get('coordPosition') );
																	if (myPlacemark != undefined){
																		myPlacemark.geometry.setCoordinates(e.get('coordPosition'));
																	}
																	$('.js-coords-input').val(e.get('coordPosition'));
																	e.preventDefault(); // ��� ������� ������ ���� �� �����.
																});

                                                <?//if(strlen($value) > 0):?>
                                                   myPlacemark = new ymaps.Placemark([<?=$coord[0]?>, <?=$coord[1]?>], {
                                                      hintContent: '�������� �����'
                                                   }, {
                                                      draggable: true // ����� ����� �������������, ����� ����� ������ ����.
                                                   });
                                                   myPlacemark.events.add("dragend", function (event) {
                                                      $('.js-coords-input').val(myPlacemark.geometry.getCoordinates());
                                                   });
                                                   myMapForCompany.geoObjects.add(myPlacemark);
                                                <?//endif;?>
                                             }
                                             </script>
                                          <?elseif($propertyID == $arResult['id_const']['adress']): // �����?>
                                            <input class="js-address-input" type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" />&nbsp;<a class="js-address-showonmap" href="javascript:void(0);" onclick="javascript: showOnMap(myMapForCompany);"<?=($arResult["ELEMENT_PROPERTIES"][123][0]["VALUE"] && $arResult["ELEMENT_PROPERTIES"][84][0]["VALUE"] && $arResult["ELEMENT_PROPERTIES"][94][0]["VALUE"] && strlen($arResult["ELEMENT_PROPERTIES"][99][0]["VALUE"])) ? '' : ' style="display: none;"'?>>�������� �� �����</a>
                                          <?//elseif($propertyID == 360): // ����������� �����?>
                                            <!--<input type="file"  />   --> 
                                          <?elseif($propertyID == $arResult['id_const']['USER']): // ������������� ��������?>
                                             <input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" value="<?=$value?>" />
                                             <?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"]?>
                                          <?elseif($propertyID == $arResult['id_const']['EMAIL']):?>
                                             <?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"]?>
                                          <?elseif($propertyID == $arResult['id_const']['METRIKA_ID']): // ������ �������?>
                                             <?if($value):?>
                                                <?//YandexMetrika::showCounter($value);?>
                                                <?//����������
                                                //$APPLICATION->IncludeComponent("whipstudio:metrika.statistic", "", array("COUNTER_ID" => $value));
                                                ?> 
                                             <?endif;?>
                                          <?else:?> 
                                             <? // ������������� �������� �� ���������, ���� ��� �� ����������� ��� email                                    
                                             if($propertyID == $arResult['id_const']['EMAIL_REQUEST'] && !strlen($value) > 0)
                                                $value = $arResult["PROPERTY_LIST_FULL"][$arResult['id_const']['EMAIL']]["DEFAULT_VALUE"]; ?>
                                             <input<?=(($propertyID == 'NAME') && strlen($value)) ? ' readonly="readonly"' : ''?> type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" />
                                             <?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?>
                                                <?$APPLICATION->IncludeComponent('bitrix:main.calendar', '', array(
                                                      'FORM_NAME' => 'iblock_add',
                                                      'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
                                                      'INPUT_VALUE' => $value,
                                                   ),
                                                   null,
                                                   array('HIDE_ICONS' => 'Y')
                                                );?>
                                                <small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small>
                                             <?endif?>
                                          <?endif;
                                       }
                                    endif;
                                          }// else region
                                    break;
                                 case "F":
                                          $i=0;

                                          ?>
                                    <!--����� �������� ������-->
                                    <input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" /><br />
                                    <?for ($i = 0; $i<$inputNum; $i++)
                                    {
                                       $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];?>
                                       <input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
                                       <?if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])):?>
                                          <input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
                                          <?if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]):?>
                                             <img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
                                          <?else:?>
                                             <?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
                                             <?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
                                             [<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
                                          <?endif;
                                       endif;
                                    }
                                    break;


                                 case "L":
                                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
                                       $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                                    else
                                       $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

                                    switch ($type):
                                       case "checkbox":
                                       case "radio":
                                          foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
                                          {
                                             $checked = false;
                                             if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                             {
                                                if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
                                                {
                                                   foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
                                                   {
                                                      if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
                                                   }
                                                }
                                             } else
                                             {
                                                if ($arEnum["DEF"] == "Y") $checked = true;
                                             } ?>
                                             <input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br />
                                             <?
                                          }
                                          break;

                                       case "dropdown":
                                       case "multiselect": ?>
                                          <select class="main_select" name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
                                             <?if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                                                else $sKey = "ELEMENT";

                                             foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum):?>
                                                <?$checked = false;
                                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                                {
                                                   foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
                                                   {
                                                      if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
                                                   }
                                                } else
                                                {
                                                   if ($arEnum["DEF"] == "Y") $checked = true;
                                                }?>
                                                <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
                                             <?endforeach;?>
                                          </select>
                                          <?
                                          break;
                                    endswitch;
                                    break;?>
                              <?endswitch;?>

                              <?if(isset($arResult["ERRORS"][$propertyID])):?>
                                 <div class="errortext"><?=$arResult["ERRORS"][$propertyID]?></div>
                              <?endif;?>
                           </td>
                           <td width="309"  class="bordered">
                              <div> <!-- <?= $propertyID ?> -->

                                 <?if($propertyID == "NAME" && $arResult['ELEMENT']['IBLOCK_ID'] == IBLOCK_COMPANY ):?>
                                    <p>
                                       �������� �������� � ����� ������������� ���������� ��������� �
                                       ������ ������������ � ��������� � ������������� � �����������.
                                    </p>
                                 <?endif;?>
                                 <?if($propertyID == "IBLOCK_SECTION"  && $arResult['ELEMENT']['IBLOCK_ID'] == IBLOCK_COMPANY):?>
                                    <p>
                                       �������� ���� ��� ��������� ����, � ������� �������������� ��������
                                       ������������ ��������.
                                    </p>
                                 <?endif;?>
                                 <?if($propertyID == "99"):?>
                                    <p>
                                       �����, ������� ����� ������������ � ��������� ��� ����� � ���������

                                    </p>
                                 <?endif;?>
                                 <?if($propertyID == "100"):?>
                                    <p>
                                       ����� �������� ����������� � ����� ������ � ������ � ������������� �������, ��������, +37517 222 22 22, +375 29 222 22 22
                                    </p>
                                 <?endif;?>

                              </div>
                           </td>   
                        </tr>

                        <!--<?if(in_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"], array("DETAIL_TEXT", "SKYPE", "UNN", "ENGLISH_DESCRIPTION"))):?>
                           <tr style="background-color:#FFFFFF;">
                              <td colspan="3">&nbsp;</td>
                           </tr>
                        <?endif;?>-->
                     <?endforeach;?>

                     <?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
                        <tr>
                           <td><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?></td>
                           <td>
                              <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                              <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                           </td>
                           <td></td>
                        </tr>
                        <tr>
                           <td><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</td>
                           <td><input type="text" name="captcha_word" maxlength="50" value=""></td>
                           <td></td>
                        </tr>
                     <?endif?>

                     <tr>
                        <td colspan="2">&nbsp;</td>  
                        <td class="bordered">&nbsp;</td>         
                     </tr>
                  </tbody>
               <?endif?>

               <tfoot>
                  <tr>
                     <td colspan="3">
                        <input class="button_v2" onclick="return CheckForm();" type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
                        <?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?>
                           <input class="button_v2" type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
                        <?endif;?>
                     </td>
                  </tr>
               </tfoot>
            </table>
         </div>
         <?$firstTab = false;
      endforeach;?>
   </div>
   <br />
   <?if (strlen($arParams["LIST_URL"]) > 0):?><a href="<?=$arParams["LIST_URL"]?>"><?=GetMessage("IBLOCK_FORM_BACK")?></a><?endif?>
</form>

<script>
$(document).ready(function(){
   $(".js_inp_name_2,.js_inp_name_1  ").change(function(){
      var   $this = $(this),
            $inpName3 = $(".js_inp_name_3");
            
      if($this.val().length > 0)
      {
         $inpName3.val("").hide();
      } else
      {
         $inpName3.val("").show();
      }


      var   $inpName = $("#insertNameThere"),
              $inpName1 = $(".js_inp_name_1"),
              $inpName2 = $(".js_inp_name_2"),
              $inpName3 = $(".js_inp_name_3");

        if($inpName.size() > 0)
        {
           if($inpName1.val().length > 0)
           {
              if($inpName2.val().length > 0)
                 $inpName.val($inpName1.val() + " " + $inpName2.val());
              else if($inpName3.val().length > 0)
                 $inpName.val($inpName1.val() + " " + $inpName3.val());
           }
        }

      $.getJSON('/_ajax/_help/getCompanyByName.php', {'NAME': $inpName.val()}, function(data){
         console.log(data);
         if ( data.URL.length > 0){
            // ���� ��������
            var _id = data.ID;
            var html = '�������� � ����� ��������� ��� ����������, <a href="" data-company-id="'+_id+'" >��������� ������ �� �����������������</a> ?'

            $('.alert_exist').html(html);

         } else {
            // ��� ��������
            $('.alert_exist').html('');
         }
      });

   });
});  

$('.js-make-predstavitel a').live("click", function () {
      var $button = $(this).closest('.js-make-predstavitel');
      var companyId = $(this).attr("data-company-id");
      $button.html("�������� ������...");
      $.ajax({
          type: "POST",
          url: "/_ajax/personal/make_predstavitel.php",
          data: "companyId=" + companyId,
          dataType: "json",
          success: function (data) {
              if (!data.error.length) {
                  if (data.answer.REQUEST_CREATE)
                      $button.html("���� ������ �� ������������");
              } else {
                  $button.html("");
                  for (var index in data.error) {
                      $button.append(data.error[index] + "<br>");
                  }
              }
          }
      });

   return false;
  });

function CheckForm(){
   var   $inpName = $("#insertNameThere"),
         $inpName1 = $(".js_inp_name_1"),
         $inpName2 = $(".js_inp_name_2"),
         $inpName3 = $(".js_inp_name_3");
   
   if($inpName.size() > 0)
   {      
      if($inpName1.val().length > 0)
      {
         if($inpName2.val().length > 0)
            $inpName.val($inpName1.val() + " " + $inpName2.val());
         else if($inpName3.val().length > 0)
            $inpName.val($inpName1.val() + " " + $inpName3.val());
      }
   }    
   
   return true;
}   

function insertRow($block, defVal){
   var $el = $block.find(".elements input"),
       count = $el.size(),
       $newEl = $el.first().clone();     
   $newEl.attr("name", $newEl.attr("name").replace(/PROPERTY\[(\d+)\]\[(\d+)\]/gi, 'PROPERTY[$1][' + count + ']'));
   $newEl.val(defVal);
   $block.find(".elements").append($newEl);
}

function insertPhoneRow($block, defVal){
  var $el = $block.find(".elements .row"),
      count = $el.size(),
      $newEl = $el.first().clone();
      
   $newEl.find("input").eq(0).attr("name", $newEl.find("input").eq(0).attr("name").replace(/PROPERTY\[(\d+)\]\[(\d+)\]/gi, 'PROPERTY[$1][' + count + ']'));
   $newEl.find("input").eq(1).attr("name", $newEl.find("input").eq(1).attr("name").replace(/DESCRIPTION\[(\d+)\]\[(\d+)\]/gi, 'DESCRIPTION[$1][' + count + ']'));
   $newEl.find("input").eq(0).val(defVal);
   $newEl.find("input").eq(1).val("");
   $block.find(".elements").append($newEl);



   var maskList = $.masksSort($.masksLoad("/bitrix/templates/.default/js/phone/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
           var maskOpts = {
               inputmask: {
                   definitions: {
                       '#': {
                           validator: "[0-9]",
                           cardinality: 1
                       }
                   },
                   //clearIncomplete: true,
                   showMaskOnHover: false,
                   autoUnmask: true
               },
               match: /[0-9]/,
               replace: '#',
               list: maskList,
               listKey: "mask",
           };

           $('.extPhone').inputmasks(maskOpts);
}

function showOnMap(){
   var string = [];
   if(~~$('.js_country_sel').val()) string.push($('.js_country_sel option[value='+$('.js_country_sel').val()+']').text());
   if(~~$('.js_city_sel').val()) string.push($('.js_city_sel option[value='+$('.js_city_sel').val()+']').text());
   if($('.js-address-input').val().length) string.push($('.js-address-input').val());
   if(string.length == 3){
      string = string.join(", ");
      if(string.length){
         var myGeocoder = ymaps.geocode(string);
         myGeocoder.then(
            function (res) {
               $('.js-coords-input').val(res.geoObjects.get(0).geometry.getCoordinates());
               if(typeof myPlacemark != 'undefined'){
                  myMapForCompany.geoObjects.remove(myPlacemark);
               }
               myPlacemark = new ymaps.Placemark(res.geoObjects.get(0).geometry.getCoordinates(), {
                  hintContent: '�������� �����'
               }, {
                  draggable: true // ����� ����� �������������, ����� ����� ������ ����.
               });
               myPlacemark.events.add("dragend", function (event) {
                  $('.js-coords-input').val(myPlacemark.geometry.getCoordinates());
               });
               myMapForCompany.geoObjects.add(myPlacemark);
               myMapForCompany.setCenter(myPlacemark.geometry.getCoordinates());
            },
            function (err) {
               console.log("���������� �� �������");
            }
         );
      }
   }
}
</script>



<?$APPLICATION->IncludeComponent("bitrix:main.file.input", "",
   array(
      "INPUT_NAME"=>"TEST_NAME_INPUT",
      "MULTIPLE"=>"Y",
      "MODULE_ID"=>"main",
      "MAX_FILE_SIZE"=>"",
      "ALLOW_UPLOAD"=>"A", 
      "ALLOW_UPLOAD_EXT"=>""
   ),
   false
);?>
