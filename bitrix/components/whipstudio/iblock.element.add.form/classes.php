<?class MyPropertyElementList extends CIBlockPropertyElementList {
   function GetPropertyFieldHtmlRegion($arProperty, $value, $strHTMLControlName, $class, $disabled)
   {
       $settings = CIBlockPropertyElementList::PrepareSettings($arProperty);
       if($settings["size"] > 1)
           $size = ' size="'.$settings["size"].'"';
       else
           $size = '';

       $width = ' style="width:218px"';

       $bWasSelect = false;


       if($value["VALUE"] > 0 || $arProperty["CODE"] == 'COUNTRY')
       {
          if(!$value["VALUE"] > 0 && $arProperty["CODE"] == 'COUNTRY'):?>
            <script>
            $(document).ready(function(){
               $(".js_country_sel").val(438).change();
               $selectRegionChosen.trigger("liszt:updated");
            });
            </script>
          <?endif;
          $options = CIBlockPropertyElementList::GetOptionsHtml($arProperty, array($value["VALUE"]), $bWasSelect);
       }
          
         
       
       $html = '<select '.($disabled ? "disabled" : "").' class="'.$class.'" name="'.$strHTMLControlName["VALUE"].'"'.$size.$width.'>';
       if($arProperty["IS_REQUIRED"] != "Y")
           $html .= '<option value=""'.(!$bWasSelect? ' selected': '').'>'.GetMessage("IBLOCK_PROP_ELEMENT_LIST_NO_VALUE").'</option>';
       $html .= $options;
       $html .= '</select>';
       return  $html;
   }
   
   function GetPropertyFieldHtmlSimple($arProperty, $value, $strHTMLControlName, $class, $disabled)
   {
      $html = '<input type="hidden" name="'.$strHTMLControlName["VALUE"].'[TYPE]" value="text">';
      $html .= '<input type="text" class="'.$class.'" name="'.$strHTMLControlName["VALUE"].'[TEXT]" value="'.$value["VALUE"]["TEXT"].'">';
      return  $html;
   }
}