<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?$APPLICATION->AddHeadScript('/js/jquery.ui.widget.js');
$APPLICATION->AddHeadScript('/js/jquery.iframe-transport.js');
$APPLICATION->AddHeadScript('/js/jquery.fileupload.js');?>

<?if(count($arResult["SECTIONS"])):?>
   <div>
      <strong>��������� � ����������:</strong>
      <select class="js-section-id" name="section_id">
         <?foreach($arResult["SECTIONS"] as $key => $section):?>
            <option value="<?=$section["ID"]?>"<?=$key ? '' : ' selected'?>><?=$section["NAME"]?></option>
         <?endforeach;?>
      </select>
      <p>���� <a target="_blank" href="/bitrix/admin/iblock_section_edit.php?IBLOCK_SECTION_ID=0&from=iblock_list_admin&type=content&lang=ru&IBLOCK_ID=<?=$arParams["IBLOCK_ID"]?>&find_section_section=0">��������</a> ����� ���������</p>
   </div>
   <div>
      <input class="js-fileupload" type="file" name="files[]" data-url="<?=$arParams['AJAX_PATH']?>" data-iblock="<?=$arParams['IBLOCK_ID']?>" multiple>
   </div>
   <div id="progress">
       <div class="bar" style="width: 0%;"></div>
   </div>
<?else:?>
   <strong>��� ���������� ���������� <a target="_blank" href="/bitrix/admin/iblock_section_edit.php?IBLOCK_SECTION_ID=0&from=iblock_list_admin&type=content&lang=ru&IBLOCK_ID=<?=$arParams["IBLOCK_ID"]?>&find_section_section=0">��������</a> ���������</strong>
<?endif;?>