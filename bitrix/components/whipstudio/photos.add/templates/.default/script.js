$(document).ready(function() {
   $(function() {
      $('.js-fileupload').fileupload({
         dataType: 'json',
         formData: function(form){
            return [{
               name: 'section_id',
               value: $(".js-section-id").val()
            },
            {
               name: 'iblock_id',
               value: $('.js-fileupload').attr('data-iblock')
            }];
         },
         sequentialUploads: false,
         done: function(e, data) {
             console.log("Завершено", e, data);
         },
         progressall: function(e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                    'width',
                    progress + '%'
                    );
         }
      });
   });
});