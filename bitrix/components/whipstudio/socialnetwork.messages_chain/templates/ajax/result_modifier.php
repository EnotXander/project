<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


if ($arResult["USER"]["PERSONAL_PHOTO"])
{
   $arFileTmp = CFile::ResizeImageGet(
         $arResult["USER"]["PERSONAL_PHOTO"],
         array("width" => 74, "height" => 55),
         BX_RESIZE_IMAGE_PROPORTIONAL,
         true
   );
   $arResult["USER"]["PERSONAL_PHOTO"] = array(
         "SRC" => $arFileTmp["src"],
         "WIDTH" => $arFileTmp["width"],
         "HEIGHT" => $arFileTmp["height"],
     );
}

foreach($arResult["Events"] as $eventKey => $event)
{
   if (is_array($event["PARTNER_INFO"]["PERSONAL_PHOTO"]))
   {
      $arFileTmp = CFile::ResizeImageGet(
            $event["PARTNER_INFO"]["PERSONAL_PHOTO"],
            array("width" => 74, "height" => 55),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
      );
      $arResult["Events"][$eventKey]["PARTNER_INFO"]["PERSONAL_PHOTO"] = array(
          "SRC" => $arFileTmp["src"],
          "WIDTH" => $arFileTmp["width"],
          "HEIGHT" => $arFileTmp["height"],
      );
   }
}