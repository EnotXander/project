<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?// �� ������������
if ($arResult["NEED_AUTH"] == "Y"):
   $APPLICATION->AuthForm("");
else:?>
   <div class="write-message-block">
      <a id="write-message-btn" href="javascript:void(0)">�������� ���������</a>
   </div>
   <div class="messages-block js-messages-box" rel="<?= $arParams["PAGE_SIZE"] ?>" data-last="<?=(is_array($arResult["Events"]) && count($arResult["Events"])) ? $arResult["Events"][0]["ID"] : 0?>">
      <?if(is_array($arResult["Events"]) && count($arResult["Events"])):
         foreach ($arResult["Events"] as $key => $event):
            $event["MESSAGE_SAFE"] = FormatText($event["MESSAGE"], "text");
            ?>
            <div class="message-row <?= $event["UNREAD_MESSAGE"] > 0 ? "no-read" : "" ?>" id="rowMessage<?=$event["PARTNER_INFO"]["ID"]?>">
               <div class="photo">
                  <? if (is_array($event["PARTNER_INFO"]["PERSONAL_PHOTO"])): ?>
                     <img src="<?=$event["PARTNER_INFO"]["PERSONAL_PHOTO"]["SRC"] ?>" alt="<?= $event["PARTNER_INFO"]["FORMATED_NAME"] ?>" title="<?= $event["PARTNER_INFO"]["FORMATED_NAME"] ?>" />
                  <? else: ?>
                     <img src="/images/search_placeholder.png" alt="<?= $event["PARTNER_INFO"]["FORMATED_NAME"] ?>" width="74" height="55" title="<?= $event["PARTNER_INFO"]["FORMATED_NAME"] ?>" />
                  <? endif; ?>  
               </div>
               <div class="info">
                  <?= $event["PARTNER_INFO"]["FORMATED_NAME"] ?>
                  <? if ($USER->IsOnLine($event["PARTNER_INFO"]["ID"])): ?>
                     <div>Online</div>
                  <? endif; ?> 
                  <div style="margin-top: 5px;">
                     <?= $event["DATE_CREATE"] ?>
                  </div>
               </div>
               <div class="message-control">
                  <div id="deleteM<?= $event["PARTNER_INFO"]["ID"] ?>" class="delete" title="Delete chain">X</div>
                  <? if ($event["MESSAGE_TYPE"] == "S"): ?>
                     <div class="status status-in" title="An incoming message"></div>
                  <? else: ?>
                     <div class="status status-out" title="An outcoming message"></div>
                  <? endif; ?>
                  <? if ($event["UNREAD_MESSAGE"] > 0): ?>
                     <div class="unread"><?= $event["UNREAD_MESSAGE"] ?></div>
                  <? endif; ?>
               </div>
               <? // PrintObject($event["UNREAD_MESSAGE_SELF"])?>
               <div class="message-text<?= (!$event["MESSAGE_INCOME"] && $event["UNREAD_MESSAGE_SELF"]) ? " no-read" : "" ?>">
                  <? if ($event["MESSAGE_INCOME"]): ?>
                     <?= $event["MESSAGE_SAFE"] ?>
                  <? else: ?>
                     <img class="message-picture-self" src="<?=$arResult["USER"]["PERSONAL_PHOTO"]["SRC"] ?>" alt="<?= $arResult["USER"]["FORMATED_NAME"] ?>" title="<?= $arResult["USER"]["FORMATED_NAME"] ?>" />
                     <div><?= $event["MESSAGE_SAFE"] ?></div>
                  <? endif; ?>
               </div>
               <div class="user-meta" style="display: none;">
                  <span class="message_id"><?= $event["ID"] ?></span>
                  <span class="user_id"><?= $event["PARTNER_INFO"]["ID"] ?></span>
                  <span class="user_name"><?= $event["PARTNER_INFO"]["NAME"] ?></span>
                  <span class="message_type"><?= $event["MESSAGE_TYPE"] ?></span>
                  <span class="message_unread"><?= $event["UNREAD_MESSAGE"] ?></span>
               </div>
            </div>
         <? endforeach; ?>
      <?else:?>
         <div class="message-row" style="padding: 25px;">������� ����� ��������� �����.</div>
      <? endif; ?>
   
   </div>
<? endif; ?>