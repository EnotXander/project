<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
}

foreach ( $arResult["ITEMS"] as $elementKey => &$element ) {
	if (  $element["~PREVIEW_PICTURE"]> 0 ) {


			$arFilterImg = array();
			$arFileTmp   = CFile::ResizeImageGet(
				$element["~PREVIEW_PICTURE"],
				array(
						"width"  => 100,
				       "height" => 100
				),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true,
				$arFilterImg
			);

			$FILE = array(
				'SRC'         => $arFileTmp["src"],
				'WIDTH'       => $arFileTmp["width"],
				'HEIGHT'      => $arFileTmp["height"],
				'DESCRIPTION' =>  $element["NAME"]
			);

			$element["PREVIEW_PICTURE"] = $FILE;


	} else {
		$FILE             = array(
			'SRC'         => "/images/search_placeholder.png",
			'WIDTH'       => 48,
			'HEIGHT'      => 42,
			'DESCRIPTION' => $element["NAME"]
		);
		$element["PREVIEW_PICTURE"] = $FILE;
	}
	$arResult["ITEMS"][ $elementKey ] = $element;
}
