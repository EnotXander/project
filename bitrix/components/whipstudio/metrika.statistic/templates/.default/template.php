<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");?>

<?
global $APPLICATION;
//$APPLICATION->AddHeadString('<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />', true);

?>

<script type="text/template" id="grafic-sources-template">
   <table>
      <thead>
         <tr>
            <th>���������</th>
            <th>������</th>
            <th>���������</th>
            <th>������</th>
            <th>������� ���������</th>
            <th>����� �� �����</th>
         </tr>
      <thead>
      <tbody>
         <%for(var index in data){%>
            <tr>
               <td><%=data[index]['name']%></td>
               <td><%=data[index]['visits']%></td>
               <td><%=data[index]['page_views']%></td>
               <td><%=data[index]['denial']%></td>
               <td><%=data[index]['depth']%></td>
               <td><%=data[index]['visit_time']%></td>
            </tr>
         <%}%>
          <tr>
              <th>�����</th>
              <th><%=summary['visits']%></th>
              <th><%=summary['page_views']%></th>
              <th><%=summary['denial']%></th>
              <th><%=summary['depth']%></th>
              <th><%=summary['visit_time']%></th>
          </tr>
      </tbody>
   </table>
</script>
<script type="text/template" id="grafic-countries-template">
   <table>
      <thead>
         <tr>
            <th>���������</th>
            <th>������</th>
            <th>���������</th>
            <th>������</th>
            <th>������� ���������</th>
            <th>����� �� �����</th>
         </tr>
      <thead>
      <tbody>
         <%for(var index in data){%>
            <tr>
               <td><%=data[index]['name']%></td>
               <td><%=data[index]['visits']%></td>
               <td><%=data[index]['page_views']%></td>
               <td><%=data[index]['denial']%></td>
               <td><%=data[index]['depth']%></td>
               <td><%=data[index]['visit_time']%></td>
            </tr>
         <%}%>
          <tr>
              <th>�����</th>
              <th><%=summary['visits']%></th>
              <th><%=summary['page_views']%></th>
              <th><%=summary['denial']%></th>
              <th><%=summary['depth']%></th>
              <th><%=summary['visit_time']%></th>
          </tr>
      </tbody>
   </table>
</script>
<script type="text/template" id="grafic-search-template">
   <table>
      <thead>
         <tr>
            <th>���������</th>
            <th>������</th>
            <th>���������</th>
            <th>������</th>
            <th>������� ���������</th>
            <th>����� �� �����</th>
         </tr>
      <thead>
      <tbody>
         <%for(var index in data){%>
            <tr>
               <td><%=data[index]['name']%></td>
               <td><%=data[index]['visits']%></td>
               <td><%=data[index]['page_views']%></td>
               <td><%=data[index]['denial']%></td>
               <td><%=data[index]['depth']%></td>
               <td><%=data[index]['visit_time']%></td>
            </tr>
         <%}%>
          <tr>
              <th>�����</th>
              <th><%=summary['visits']%></th>
              <th><%=summary['page_views']%></th>
              <th><%=summary['denial']%></th>
              <th><%=summary['depth']%></th>
              <th><%=summary['visit_time']%></th>
          </tr>
      </tbody>
   </table>
</script>
<script type="text/template" id="grafic-phrases-template">
   <table>
      <thead>
         <tr>
            <th>���������</th>
            <th>������</th>
            <th>���������</th>
            <th>������</th>
            <th>������� ���������</th>
            <th>����� �� �����</th>
         </tr>
      <thead>
      <tbody>
         <%for(var index in data){%>
            <tr<%if(~~index >= ~~opt.visible){%> style="display: none;"<%}%>>
               <td><%=data[index]['phrase']%></td>
               <td><%=data[index]['visits']%></td>
               <td><%=data[index]['page_views']%></td>
               <td><%=data[index]['denial']%></td>
               <td><%=data[index]['depth']%></td>
               <td><%=data[index]['visit_time']%></td>
            </tr>
         <%}%>
          <tr>
              <th>�����</th>
              <th><%=summary['visits']%></th>
              <th><%=summary['page_views']%></th>
              <th><%=summary['denial']%></th>
              <th><%=summary['depth']%></th>
              <th><%=summary['visit_time']%></th>
          </tr>
      </tbody>
   </table>
</script>

<?if($arResult['COUNTER_ID']):?>
   <div class="statistic-wrapper">

      <div class="mc_block">
         <h2>������</h2>
         <div class="dp-wrapper" style="margin-bottom: 0px;">
            <div type="text" class="js-datepicker js-datepicker-from" data-input="js-dateinput-from" style="float: left"></div>
            <div type="text" class="js-datepicker js-datepicker-to" data-input="js-dateinput-to"  style="margin-left: 209px;"></div>
            <input type="hidden" class="js-dateinput js-dateinput-from" />
            <input type="hidden" class="js-dateinput js-dateinput-to" />
         </div>
         <div class="clear" style="margin-top: 24px;"></div>
         <input type="button" class="js-statistic-reload"
                data-counter-id="<?=$arResult['COUNTER_ID']?>"
                data-phone-click-id="<?= $arResult['PHONE_CLICK_ID'] ?>" data-oauth-token="<?=$arResult['TOKEN']?>" value="��������" />
      </div>
      <div class="mc_block js-grafic-visits" style="height: 340px;">
         <h2>������ � ���������</h2>
         <div class="statistic-chart-control">
            <input type="radio" name="group" value="day" checked onclick="statisticHandler.reload('getVisits')">�� ����<Br>
            <input type="radio" name="group" value="week" onclick="statisticHandler.reload('getVisits')">�� �������<Br>
            <input type="radio" name="group" value="month" onclick="statisticHandler.reload('getVisits')">�� �������<Br>
            <br>
            <input type="checkbox" name="visit" value="Y" checked onclick="statisticHandler.reloadLocal('getVisits')">������<Br>
            <input type="checkbox" name="view" value="Y" checked onclick="statisticHandler.reloadLocal('getVisits')">���������<Br>
         </div>
         <div id="grafic_visits_chart" style="width: 680px"></div>
      </div>
      <div class="mc_block js-grafic-goals" style="height: 340px; ">
         <h2>������� ���������� ��������� - <span id="call_summ_count"></span> (�� ������)</h2>
         <div id="goal_control" class="statistic-chart-control">
            <input type="radio" name="group" value="day" checked onclick="statisticHandler.reload('getGoals')">�� ����<Br>
            <input type="radio" name="group" value="week" onclick="statisticHandler.reload('getGoals')">�� �������<Br>
            <input type="radio" name="group" value="month" onclick="statisticHandler.reload('getGoals')">�� �������<Br>
            <br>
            <input type="checkbox" name="goal" value="Y" checked onclick="statisticHandler.reloadLocal('getGoals')">�������<Br>
            <input type="checkbox" name="visits" value="Y" checked onclick="statisticHandler.reloadLocal('getGoals')">������<Br>
         </div>
         <div id="grafic_visits_goals" style="width: 680px"></div>
      </div>
      <div class="mc_block js-grafic-sources" style="">
         <h2>��������� ���������</h2>
         <div id="grafic_sources_chart" class="statistic-bubble"></div>
         <div id="grafic_sources_list" class="statistic-table" style="max-width: 1000px;"></div>
      </div>
      <div class="mc_block js-grafic-countries" style="">
         <h2>��������� �� �������</h2>
         <div id="grafic_countries_chart" class="statistic-bubble"></div>
         <div id="grafic_countries_list" class="statistic-table" style=""></div>
      </div>
      <div class="mc_block js-grafic-search" style="">
         <h2>��������� �������</h2>
         <div id="grafic_search_chart" class="statistic-bubble"></div>
         <div id="grafic_search_list" class="statistic-table" style=""></div>
      </div>
      <div class="mc_block js-grafic-phrases" style="">
         <h2>��������� �����</h2>
         <div id="grafic_phrases_chart" class="statistic-bubble"></div>
         <div class="statistic-table">
            <div id="grafic_phrases_list" class="" style=""></div>
            <button class="js-grafic-phrases-more" data-per-page="10" data-more="10" onclick="if(!statistic.getSearchPhrases.showMore($(this).attr('data-more'))) $(this).hide();">�������� ���</button>
         </div>
      </div>
   </div>
<?else:?>
   <div>��� ���� ����� ������ ���������� �� ������ ���� �������������� ��������</div>
<?endif;?>
