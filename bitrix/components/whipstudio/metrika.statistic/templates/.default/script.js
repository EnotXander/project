google.load("visualization", "1", {packages: ["corechart"]});
console.log('script!!!');

var statistic = {
   //������ � ���������
   getVisits: {
      method: 'GET',
      url: '/stat/traffic/summary',
      dataFunc: function(request) {
         return $.extend({}, request, {
            group: $('.js-grafic-visits input[name=group]:checked').val()
         });
      },
      grafic: {
         chart: function(objData) {
            var visibleStat = {
               visit: $('.js-grafic-visits input[name=visit]:checked').length ? true : false,
               view: $('.js-grafic-visits input[name=view]:checked').length ? true : false
            }

            var objData = objData.data;
            //console.log('draw', objData);

            var dataTable = new google.visualization.DataTable();//������������� ������ ������� ������
            dataTable.addColumn('date', 'Date');
            if (visibleStat.visit)
               dataTable.addColumn('number', '������');
            if (visibleStat.view)
               dataTable.addColumn('number', '���������');

            //���������� ������ � �������//
            for (i = 0; i < (objData.length); i++)
            {
               var added = [new Date(objData[i].date.substr(0, 4), objData[i].date.substr(4, 2)-1, objData[i].date.substr(6, 2))];
               if (visibleStat.visit)
                  added.push(objData[i].visits);
               if (visibleStat.view)
                  added.push(objData[i].page_views);
               dataTable.addRow(added);
            }

            var dataView = new google.visualization.DataView(dataTable);
            //dataView.setColumns([{calc: function(data, row) { return data.getFormattedValue(row, 0); }, type:'string'}, 1]);
            var chart = new google.visualization.LineChart(document.getElementById('grafic_visits_chart'));
            var options = {//��������� ���������
               //title: '����������', //��������� �������
               fontSize: 12,
               height: 300,
               width: 680,
               lineWidth: 2,
               chartArea: {height: 200, width: 460},
               axisTitlesPosition: 'out', // ��������� �������� ����
               backgroundColor: 'white', // ���� ����
               curveType: 'none', //������������ �����. �� ���������: 'none'
               hAxis: {format: 'MMM d, y', direction: 1, title: '����', titleTextStyle: {color: 'black', fontSize: 14, bold: 'true'}}, //��������� �������������� ���
               vAxis: {title: '����������', titleTextStyle: {color: 'black', fontSize: 14, bold: 'true'}},
               legend: {position: 'right', textStyle: {fontSize: 12}}
            };

            chart.draw(dataTable, options);
            //console.log('draw end');
         }
      }
   },

   // �������
   getGoals: {
      method: 'GET',
      em_id : 'grafic_visits_goals',
      url: '/stat/traffic/summary',
       //params: 'goal_id='+,
      dataFunc: function(request) {
         return $.extend({}, request, {
            group: $('#goal_control input[name=group]:checked').val(),
            goal_id: $('.js-statistic-reload').data('phone-click-id')
         });
      },
      grafic: {
         chart: function(objData) {
            var visibleStat = {
                goal: $('#goal_control input[name=goal]:checked').length ? true : false,
                visits: $('#goal_control input[name=visits]:checked').length ? true : false
            };
            var total = objData.totals.goal_reaches;
            var objData = objData.data;
            //console.log('draw', objData);

            var dataTable = new google.visualization.DataTable();//������������� ������ ������� ������
            dataTable.addColumn('date', 'Date');
            if (visibleStat.goal)
               dataTable.addColumn('number', '�������');
            if (visibleStat.visits)
               dataTable.addColumn('number', '������');

            //���������� ������ � �������//
            for (i = 0; i < (objData.length); i++)
            {
               var added = [new Date(objData[i].date.substr(0, 4), objData[i].date.substr(4, 2)-1, objData[i].date.substr(6, 2))];
               if (visibleStat.goal)
                  added.push(objData[i].goal_reaches);
               if (visibleStat.visits)
                  added.push(objData[i].visits_all);
               dataTable.addRow(added);
            }

            var dataView = new google.visualization.DataView(dataTable);
            //dataView.setColumns([{calc: function(data, row) { return data.getFormattedValue(row, 0); }, type:'string'}, 1]);
            var chart = new google.visualization.LineChart(document.getElementById('grafic_visits_goals'));
            var options = {//��������� ���������
               //title: '����������', //��������� �������
               fontSize: 12,
               height: 300,
               width: 680,
               lineWidth: 2,
               chartArea: {height: 200, width: 460},
               axisTitlesPosition: 'out', // ��������� �������� ����
               backgroundColor: 'white', // ���� ����
               curveType: 'none', //������������ �����. �� ���������: 'none'
               hAxis: {format: 'MMM d, y', direction: 1, title: '����', titleTextStyle: {color: 'black', fontSize: 14, bold: 'true'}}, //��������� �������������� ���
               vAxis: {title: '����������', titleTextStyle: {color: 'black', fontSize: 14, bold: 'true'}},
               legend: {position: 'right', textStyle: {fontSize: 12}}
            };
            $('#call_summ_count').text(total);

            chart.draw(dataTable, options);
            //console.log('draw end');
         }
      }
   },
   //��������� ���������
   getSources: {
      method: 'GET',
      url: '/stat/sources/summary',
      //template: _.template($('#grafic-sources-template').html()),
      templateSelector: '#grafic-sources-template',
      grafic: {
         list: function(objData) {
            //console.log('list', this, objData, $('#grafic-sources-template').html());
            $('#grafic_sources_list').html(this.template({
                data: objData.data,
                summary: statisticHandler.getSummary(objData.data)
            }));
         },
         drawChart: function(objData) {
            objData = objData.data;
            var dataArray = [['System', 'Percent']];

            for(i = 0; i < objData.length; i++){  
               dataArray.push([objData[i].name, objData[i].visits]);
               /*dataArray.push([objData[1].name, objData[1].visits]);
               dataArray.push([objData[2].name, objData[2].visits]);
               dataArray.push([objData[4].name, objData[4].visits]);*/
            }
            var dataTable = google.visualization.arrayToDataTable(dataArray);
            var options = {
               //title: '��������� ���������',
               pieHole: 0.4,
               sliceVisibilityThreshold: 0//�������, ���� �������� ����� ������������ � Other
            };

            var chart = new google.visualization.PieChart(document.getElementById('grafic_sources_chart'));
            chart.draw(dataTable, options);
         }
      }
   },
   //��������� �� �������
   getCountries: {
      method: 'GET',
      url: '/stat/geo',
      //template: _.template($('#grafic-sources-template').html()),
      templateSelector: '#grafic-countries-template',
      grafic: {
         list: function(objData) {
            //console.log('list', this, objData, $('#grafic-countries-template').html());
            $('#grafic_countries_list').html(this.template({
                data: objData.data,
                summary: statisticHandler.getSummary(objData.data)
            }));
         },
         drawChart: function(objData) {
            var objData = objData.data;
            var dataArray = [['Country', 'Percent']];

            for (i = 0; i < objData.length; i++) {
               dataArray.push([objData[i].name, objData[i].visits]);
            }
            var dataTable = google.visualization.arrayToDataTable(dataArray);
            var options = {
               //title: '��������� �� �������',
               pieHole: 0.4,
               sliceVisibilityThreshold: 0.003//�������, ���� �������� ����� ������������ � Other
            };

            var chart = new google.visualization.PieChart(document.getElementById('grafic_countries_chart'));
            chart.draw(dataTable, options);
         }
      }
   },
   //��������� �������
   getSearchSystem: {
      method: 'GET',
      url: '/stat/sources/search_engines',
      //template: _.template($('#grafic-sources-template').html()),
      templateSelector: '#grafic-search-template',
      grafic: {
         list: function(objData) {
            //console.log('list', objData.data);
            $('#grafic_search_list').html(this.template({
                data: objData.data,
                summary: statisticHandler.getSummary(objData.data)
            }));
         },
         drawChart: function(objData) {
            objData = objData.data;
            var dataArray = [['System', 'Percent']];

            for (i = 0; i < objData.length; i++) {
               dataArray.push([objData[i].name, objData[i].visits]);
            }
            var dataTable = google.visualization.arrayToDataTable(dataArray);
            var options = {
               //title: '��������� �������',
               pieHole: 0.4,
               sliceVisibilityThreshold: 0.03//�������, ���� �������� ����� ������������ � Other
            };

            var chart = new google.visualization.PieChart(document.getElementById('grafic_search_chart'));
            chart.draw(dataTable, options);
         }
      }
   },
   //��������� �����
   getSearchPhrases: {
      method: 'GET',
      url: '/stat/sources/phrases',
      dataFunc: function(request) {
         return $.extend({}, request, {
            per_page: 1000//$('.js-grafic-phrases-more').attr('data-per-page')
         });
      },
      templateSelector: '#grafic-phrases-template',
      grafic: {
         list: function(objData) {
            //console.log('list', this, objData, $('#grafic-phrases-template').html());
            $('#grafic_phrases_list').html(this.template({
               data: objData.data,
               opt: {
                  visible: $('.js-grafic-phrases-more').attr('data-per-page')
               },
                summary: statisticHandler.getSummary(objData.data)
            }));
         },
         drawChart: function(objData) {
            objData = objData.data;
            var dataArray = [['Phrase', 'Visits']];

            for (i = 0; i < objData.length; i++) {
               dataArray.push([objData[i].phrase, objData[i].visits]);
            }
            var dataTable = google.visualization.arrayToDataTable(dataArray);
            var options = {
               //title: '��������� �����',
               pieHole: 0.4,
               sliceVisibilityThreshold: 0.003//�������, ���� �������� ����� ������������ � Other
            };

            var chart = new google.visualization.PieChart(document.getElementById('grafic_phrases_chart'));
            chart.draw(dataTable, options);
         },
         moreControl: function(objData){
            var current = $('.js-grafic-phrases-more').attr('data-per-page');
            //console.log(objData.data.length, current);
            if(objData.data.length < current) $('.js-grafic-phrases-more').hide();
         }
      },
      showMore: function(added){
         $('#grafic_phrases_list tr:hidden').slice(0, $('.js-grafic-phrases-more').attr('data-more')).show();
         return ($('#grafic_phrases_list tr:hidden').length > 0);
      }
   }
}

//��������
var statisticHandler = {
   counter: {},
   onload: function() {


       var counter = statisticHandler.counter;

      //datepickers
      var datePickerOptions = {
         dateFormat: 'dd MM yy',
         closeText: '�������', 
         prevText: '����������', 
         nextText: '���������', 
         currentText: '�������', 
         monthNames: ['������','�������','����','������','���','����','����','������','��������','�������','������','�������'], 
         monthNamesShort: ['���','���','���','���','���','���','���','���','���','���','���','���'], 
         dayNames: ['�����������','�����������','�������','�����','�������','�������','�������'], 
         dayNamesShort: ['���','���','���','���','���','���','���'], 
         dayNamesMin: ['��','��','��','��','��','��','��'],
         altFormat: 'yymmdd',
         minDate: new Date(2013, 10, 1),
         maxDate: +1, // ������� -1
         firstDay: 1, 
         isRTL: false,
         showOptions: {
            direction: "up"
         }
         //onSelect: statisticHandler.reloadAll
      }
      datePickerOptions.altField = '.js-dateinput-from';
      datePickerOptions.defaultDate = '-1m';
      datePickerOptions.setDate = '-1m';
      datePickerOptions.onSelect = function(selectedDate){
         $( ".js-datepicker-to" ).datepicker( "option", "minDate", selectedDate );
      };
      $('.js-datepicker-from').datepicker(datePickerOptions).datepicker("setDate", datePickerOptions.setDate);
      datePickerOptions.altField = '.js-dateinput-to';
      datePickerOptions.defaultDate = -1;
      datePickerOptions.setDate = -1;
      datePickerOptions.onSelect = function(selectedDate){
         $( ".js-datepicker-from" ).datepicker( "option", "maxDate", selectedDate );
      };
      $('.js-datepicker-to').datepicker(datePickerOptions).datepicker("setDate", datePickerOptions.setDate);
      $('.js-statistic-reload').live('click', statisticHandler.reloadAll);
      for (var index in statistic) {
         //console.log('search template in', index);
         if (typeof statistic[index].templateSelector != 'undefined') {
            var htmlTemplate = $(statistic[index].templateSelector).html();
            //console.log('html', htmlTemplate);
            statistic[index].template = _.template(htmlTemplate);
         }
      }
      statisticHandler.reloadAll();
   },
   reloadAll: function() { // ���������� ������
      statisticHandler.counter = {
         date1: $('.js-dateinput-from').val(),
         date2: $('.js-dateinput-to').val(),
         id: $('.js-statistic-reload').attr('data-counter-id'),
         id: $('.js-statistic-reload').attr('data-counter-id'),
         oauth_token: $('.js-statistic-reload').attr('data-oauth-token')
      }
      for (var statName in statistic) {
         statisticHandler.reload(statName);
      }
   },
   reloadLocal: function(statName) {
      var stat = statistic[statName];
      for (var index2 in stat.grafic) {
         //console.log('local', index2, stat.cachedData);
         stat.grafic[index2](stat.cachedData);
      }
   },
   reload: function(statName) {
      var counter = statisticHandler.counter;
      if (counter.date1.length && counter.date2.length) {
         var stat = statistic[statName];
         var params = '';
         var dataFunc = function(cnt) {
            return cnt;
         }
         if (typeof stat.dataFunc == 'function')
            dataFunc = stat.dataFunc
         if (typeof stat.params !== 'undefined')
             params = '?'+stat.params;
         //console.log('ajax', stat.url, dataFunc(counter));
         $.ajax({
            type: stat.method,
            url: 'https://api-metrika.yandex.ru' + stat.url + '.json'+params,
            data: dataFunc(counter),
            dataType: 'jsonp',
            success: function(responce) {
               if ($.isArray(responce.errors) && responce.errors.length) {
                   if (typeof stat.em_id !== 'undefined') {
                       $('#'+stat.em_id).append('<b style="color:red">'+responce.errors[0].text+'</b>');
                   }
                  console.log('������ �� �������:', responce.errors);
               } else {
                  stat.cachedData = responce;
                  for (var index2 in stat.grafic) {
                     $.proxy(stat.grafic[index2], stat)(responce);
                  }
               }
            },
            error: function(error) {
               console.error(error);
            }
         });
      }
   },
    getSummary: function(rows) {
        var summary = {}

        for (var index in rows) {
            var row = rows[index];

            if (typeof row === 'object') {
                for (var key in row) {
                    if (row.hasOwnProperty(key)) {
                        if ((typeof row[key] !== 'number') || (key === 'id')) {
                            if (key === 'name') {
                                summary[key] = '�����';
                            } else {
                                summary[key] = '';
                            }
                        } else {
                            if (summary.hasOwnProperty(key)) {
                                summary[key] = (~~((row[key] + summary[key]) * 1000) / 1000);
                            } else {
                                summary[key] = (~~(row[key] * 1000) / 1000);
                            }
                        }
                    }
                }
            }
        }

        if (rows.length) {
            for (var key in summary) {
                if ((key === 'visits') || (key === 'page_views')) {
                    continue;
                }

                summary[key] = (~~((summary[key] / rows.length) * 1000) / 1000);
            }
        }

        return summary;
    }
}

$(statisticHandler.onload);