<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult = array();

if($arParams['COUNTER_ID'])
{
   $arResult['COUNTER_ID'] = $arParams['COUNTER_ID'];

    if ($arParams['COMPANY_ID'])
    {
        $arResult['COMPANY']['ID'] = $arParams['COMPANY_ID'];
    }
}
else
{
   if($arParams['COMPANY_ID'])
   {
      if (!CModule::IncludeModule("iblock")) die();
      $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_COMPANY, "ID" => $arParams['COMPANY_ID']), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_PHONE_CLICK_COUNT', 'PROPERTY_METRIKA_ID'));
      if ($element = $res->GetNext())
      {
         $arResult['COMPANY'] = $element;
         $arResult['COUNTER_ID'] = $element['PROPERTY_METRIKA_ID_VALUE'];
      }
   }
}
if (LANG == 's1') {

    $arResult['METRIKA'] = YandexMetrika::getInstance(array(
        "ID" => METRIKA_CLIENT_ID,
        "SECRET" => METRIKA_CLIENT_SECRET,
        "USERNAME" => METRIKA_USERNAME,
        "PASSWORD" => METRIKA_PASSWORD
      ));
} else {

    $arResult['METRIKA'] = YandexMetrika::getInstance(array(
        "ID" => METRIKA_CLIENT_ID_AGRO,
        "SECRET" => METRIKA_CLIENT_SECRET_AGRO,
        "USERNAME" => METRIKA_USERNAME_AGRO,
        "PASSWORD" => METRIKA_PASSWORD_AGRO
    ));

}
// ������� ������ GET
function _get($method, $token, $arPost = array()) {
	;
  $path = "https://api-metrika.yandex.ru{$method}?";
  foreach ($arPost as $key => $value)
     $path .= "{$key}={$value}&";
  $path .= "oauth_token=" . $token;
  if (!$result = file_get_contents($path)) {

  }
  return $result;
}
//PrintAdmin($arResult);
//PrintAdmin($arResult['METRIKA']->token);
//PrintAdmin($arResult['METRIKA']->error);

if(strlen($arResult['METRIKA']->token))
{
    $arResult['TOKEN'] = $arResult['METRIKA']->token;

    $ar = json_decode(_get('/management/v1/counter/'.$arResult['COUNTER_ID'].'/goals',  $arResult['TOKEN']));
   // PrintAdmin($ar);
    foreach ($ar->goals as $goal){

        if ($goal->name == 'PHONE_CLICK')
            $arResult['PHONE_CLICK_ID'] = $goal->id;// ��� ������� ��� ��������� ����������
    }

}
else
{
   $arResult['ERROR'] = $arResult['METRIKA']->error;
}

$this->IncludeComponentTemplate();