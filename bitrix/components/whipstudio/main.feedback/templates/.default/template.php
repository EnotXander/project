<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true) die();?> 

<div class="popup_mask"></div>
<?if(strlen($arResult["OK_MESSAGE"]) > 0):?>
   <div class="popup fast_order_status_popup content_popup">
      <i class="ico_pop_close icons" href="javascript: void(0)"></i>
      <div class="fast_order_form">
         <div class="title">���� ������ �������</div>
         <div class="good">
            �� ������� ��������� ������ �� ����� <?=$arResult["PRODUCT"]["NAME"]?>. � ��������� �����, �� ��������� ���������� ������ � ���� �������� ������������� ��������.
         </div>
      </div>
   </div>
   <script>
      $(document).ready(function(){

         var popup = $('.fast_order_status_popup');
         $('.popup_mask').show().bind("click", function(){
            closePopup($('.popup'));
         });
         popup.find(".ico_pop_close").bind("click", function(){
            closePopup($('.popup'));
         });
         $('body').css({'overflow':'hidden'});
         popup.show();
         popupPosition(popup);
      })
   </script>
<?endif;?>
 <!--
<?PrintObject($arResult["ERROR_MESSAGE"])?>
-->

<div class="popup fast_order_popup content_popup">
   <i class="ico_pop_close icons" href="javascript: void(0)"></i>
   <div class="fast_order_form">
      <div class="title">��������� ������</div>
      <form action="<?=$APPLICATION->GetCurPage()?>" method="post">
         <?=bitrix_sessid_post()?>
         <table class="form-table email-form left-form">
            <tbody>
               

               <tr>			
                  <th>Email: <span class="req">*</span></th>
                  <td class="right">
                     <input type="text" value="<?=$arResult["AUTHOR_EMAIL"]?>" name="EMAIL">
                  </td>
               </tr>
               <tr>			
                  <th>��� ��� �����: <span class="req">*</span></th>
                  <td class="right">
                     <input type="text" value="<?=$arResult["AUTHOR_NAME"]?>" name="NAME">
                  </td>
               </tr>
               <tr>
                  <th>����� ��������: <span class="req">*</span></th>
                  <td class="right">
                     <input type="text" value="<?=$arResult["AUTHOR_PHONE"]?>" name="PHONE">
                  </td>
               </tr>
               <tr>
                  <th>�����: <span class="req">*</span></th>
                  <td class="right">
                     <input type="text" value="<?=$arResult["AUTHOR_ADDRESS"]?>" name="ADDRESS">
                  </td>
               </tr>
               <tr>
                  <th>�����������:</th>
                  <td class="right">
                     <textarea name="MESSAGE"><?=$arResult["MESSAGE"]?></textarea>
                  </td>
               </tr>
               <tr class="product">
                  <th></th>
                  <td></td>
               </tr>
               <tr>
                  <th>����������: <span class="req">*</span></th>
                  <td class="right">
                     <input style="width: 50px;" type="text" value="<?=$arResult["QUANTITY"] > 0 ? $arResult["QUANTITY"] > 0 : 1?>" name="QUANTITY">
                  </td>
               </tr>
               <tr>
                  <th>&nbsp;</th>
                  <td>
                     <input type="hidden" name="PRODUCT_ID" value="" />
                     <input style="display: none;" type="submit" name="submitFastOrder" value="go" />
							<div class="send_r-wrap">
								<a onclick="javascript:oneClickCheckMake($(this).closest('form')); yaCounter32687100.reachGoal('Send_Form');"
                                   class="button_v2" href="javascript: void(0);">��������� ������</a>
								<div class="send_r-overflow"></div>
							</div>
                  </td>
               </tr>
            </tbody>
         </table>
      </form>
   </div>
</div>

<script>
function setError($this){
   $this.addClass("error_brd");
}

function removeError($this){
   $this.removeClass("error_brd");
}

function oneClickCheckMake(form){
	$('.send_r-overflow').show();
	
   var   $form = $(form),
         $inputEmail = $form.find("input[name=EMAIL]"),  
         $inputName = $form.find("input[name=NAME]"),
         $inputAddress = $form.find("input[name=ADDRESS]"),
         $inputPhone = $form.find("input[name=PHONE]"),
         $inputQuantity = $form.find("input[name=QUANTITY]"),         
         regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
         error = false;

   if(!$inputEmail.val().length > 0 || !regex.test($inputEmail.val())) // email
   {
      error = true;
      setError($inputEmail);
   } else
      removeError($inputEmail);

   if($inputName.val().length > 3) // ���
      removeError($inputName);      
   else
   {
      error = true;
      setError($inputName);
   }

   if(!$inputPhone.val().length > 0) // �������
   {
      error = true;
      setError($inputPhone);
   } else
      removeError($inputPhone);

   if($inputAddress.val().length > 5) // �����
      removeError($inputAddress);
   else
   {
      error = true;
      setError($inputAddress);
   }

   if(!$inputQuantity.val().length > 0 || !~~$inputQuantity.val() > 0) // ����������
   {
      error = true;
      setError($inputQuantity);
   }         
   else
      removeError($inputQuantity);   

   if(!error)
      $form.find("input[type=submit]").click();
	
	if(error){
		$('.send_r-overflow').hide();
       yaCounter32687100.reachGoal('Error_Send');
	}
}

function showPopup(data){
   var popup = $(data.id),
       $productTr = popup.find(".product");

   $productTr.find("th, td").html("");

   if(data.productID > 0)
   {
      var productID = data.productID,
          $img = $(".catalog-element .lightbox img"),
          productName = $(".catalog-element h1").text(),
          $span = $("<span>").html(productName);
		
		if (data.sender){
			var senderTable = data.sender.closest('table');
			$img = senderTable.find('.image_box img');
			productName = senderTable.find('.ads_title').text();
			$span = $("<span>").html(productName);
		}

      if($img.size() > 0)
      {
         var $productImg = $("<img>", {src: $img.attr("src"), width: "30px"});
         $productTr.find("td").append($productImg);
      }   
      $productTr.find("td").append($span);

      popup.find("input[name=PRODUCT_ID]").val(productID);
   }

   if (popup.length && productID > 0)
   {
      $('.popup_mask').show().bind("click", function(){
         closePopup($('.popup'));
      });
      popup.find(".ico_pop_close").bind("click", function(){
         closePopup($('.popup'));
      });
      $('body').css({'overflow':'hidden'});
      popup.show();
      popupPosition(popup);
      //$(window).bind("resize", {name : name}, popupPosition);
   }
};

// ��������� ����� ������ � �����
function closePopup(popup){
   var popup = $(popup);
   if (popup.length)
   {
      $('.popup_mask').hide().unbind("click");
      popup.find('.ico_pop_close').unbind("click");
      $('body').css({'overflow':''});
      popup.hide();
      popup.css({'top':'', 'left':''});
      //$(window).unbind("resize", "popupPosition");
   }
}; 


// ���������� �����
function popupPosition(popup){
   var ppTop = 120;//($(window).height()- popup.height())/2 + $(window).scrollTop();
   var ppleft = ($(window).width()- popup.width())/2 + $(window).scrollLeft();
   popup.css({'top':ppTop, 'left':ppleft, 'position':'fixed'});
};

$(document).ready(function(){
   $('.ico_pop_close').click(function(){
      closePopup($(this).closest('.popup'));
   });    

   $(".fast_order_popup input[name=PHONE]").mask("+999 (99) 999-99-99", {
      completed:function(){
         $(".fast_order_popup input[name=ADDRESS]").focus();
      }
   });

   if ( !window.yaCounter32687100 ) {
   window.yaCounter32687100 = new Ya.Metrika({
  	                                    id:32687100,
  	                                    clickmap:true,
  	                                    trackLinks:true,
  	                                    accurateTrackBounce:true
  	                                });
   }
});
</script>
