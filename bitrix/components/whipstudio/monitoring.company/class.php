<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main;

require_once 'company/Company.php';
require_once 'tests/TestInterface.php';
require_once 'test_result/TestResult.php';

require_once 'tests/MetrikaTest.php';
require_once 'tests/IncorrectSiteTest.php';
require_once 'tests/PhoneTest.php';
require_once 'tests/SectionTest.php';
require_once 'tests/UnnTest.php';

class CompanyMonitoring extends CBitrixComponent
{
    const COMPONENT_NAME = 'whipstudio:monitoring.company';
    const IBLOCK_ID = IBLOCK_COMPANY;

    /** @var array */
    public $arResult;

    /** @var array[TestInterface] */
    public $tests;

    public function __construct()
    {
        if(!CModule::IncludeModule("iblock")) die();
        $this->arResult = array();
    }

    private function getRsCompanies()
    {
        return CIBlockElement::GetList(
            array("PROPERTY_FIRM.NAME" => "ASC"),
            array(
                "IBLOCK_ID" => self::IBLOCK_ID,
                "ACTIVE" => "Y"
            )
        );
    }

    /**
     * @param TestInterface $test
     */
    private function registerTest(TestInterface $test)
    {
        $this->tests[] = $test;
    }

    /**
     * @param Company $company
     * @return array[TestResult]
     */
    private function getFailedTests(Company $company)
    {
        $failedTests = array();

        /** @var TestInterface $test */
        foreach ($this->tests as $test) {
            /** @var  $testResult */
            $testResult = $test->testCompany($company);

            if ($testResult) {
                $failedTests = $testResult;
                $this->arResult['failedTests'][] = $testResult;
            }
        }

        return $failedTests;
    }

    public function executeComponent()
    {
        $this->initComponent(self::COMPONENT_NAME);

        $this->registerTest(new MetrikaTest());
        $this->registerTest(new IncorrectSiteTest());

        if ( LANG == 's1')
            $this->registerTest(new PhoneTest());

        $this->registerTest(new SectionTest());
        $this->registerTest(new UnnTest());

        $rsCompanies = $this->getRsCompanies();
        while ($obCompany = $rsCompanies->getNextElement()) {
            $company = new Company($obCompany);

            $this->getFailedTests($company);
        }

        $this->includeComponentTemplate();
    }
}