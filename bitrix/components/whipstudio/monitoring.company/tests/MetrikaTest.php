<?php

class MetrikaTest implements TestInterface
{
    const METRIKA_PROPERTY_NAME = 'METRIKA_ID';

    /**
     * @param Company $company
     * @return null|TestResult
     */
    public function testCompany(Company $company)
    {
        $metrikaCounter = $company->getPropertyValue(self::METRIKA_PROPERTY_NAME);

        if (!strlen($metrikaCounter)) {
            $testResult =  new TestResult();

            $testResult
                ->setCompany($company)
                ->setTest($this)
                ->setMessage("�������� {$company->getLinkAdmin()} �� ����� ������������ �������� ������.������� {$metrikaCounter}")
            ;

            return $testResult;
        }

        return null;
    }
} 