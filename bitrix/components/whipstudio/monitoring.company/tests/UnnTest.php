<?php

class UnnTest implements TestInterface
{
    const CHECK_PROPERTY_TARIF = 'TARIF';
    const CHECK_PROPERTY_UNN = 'UNN';

    /**
     * @param Company $company
     * @return null|TestResult
     */
    public function testCompany(Company $company)
    {
        $tarif = $company->getPropertyValue(self::CHECK_PROPERTY_TARIF);
        $unn = $company->getPropertyValue(self::CHECK_PROPERTY_UNN);

        if (!empty($tarif) && empty($unn)) {
            $testResult =  new TestResult();

            $testResult
                ->setCompany($company)
                ->setTest($this)
                ->setMessage("�������� {$company->getLinkAdmin()} �� ����� ��� �� ���� �������� ���� ")
            ;

            return $testResult;
        }

        return null;
    }
} 