<?php

class SectionTest implements TestInterface
{
    const CHECK_PROPERTY_NAME = 'IBLOCK_SECTION_ID';

    /**
     * @param Company $company
     * @return null|TestResult
     */
    public function testCompany(Company $company)
    {
        $checkValue = $company->getField(self::CHECK_PROPERTY_NAME);

        if (empty($checkValue)) {
            $testResult =  new TestResult();

            $testResult
                ->setCompany($company)
                ->setTest($this)
                ->setMessage("�������� {$company->getLinkAdmin()} �� ����� ������ ")
            ;

            return $testResult;
        }

        return null;
    }
} 