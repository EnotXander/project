<?php

class PhoneTest implements TestInterface
{
    const CHECK_PROPERTY_NAME = 'PHONE';

    /**
     * @param Company $company
     * @return null|TestResult
     */
    public function testCompany(Company $company)
    {
        $checkValue = $company->getPropertyValue(self::CHECK_PROPERTY_NAME);

        if (empty($checkValue)) {
            $testResult =  new TestResult();

            $testResult
                ->setCompany($company)
                ->setTest($this)
                ->setMessage("�������� {$company->getLinkAdmin()} �� ����� ��������� ")
            ;

            return $testResult;
        }

        return null;
    }
} 