<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (isset($arResult['failedTests'])) {
    foreach ($arResult['failedTests'] as $testResult) {
        /** @var TestResult $testResult */
        echo '<p>' . $testResult->getMessage() . '</p>';
    }
}

//PrintAdmin(124);
//PrintAdmin($this);
//PrintAdmin($arResult);
