<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (isset($arResult['messages'])) {
    ?>
    <div class="mc_block company-tabs js-company-tabs">
        <ul class="mb_menu">
            <li class="mb_title sel" id="mb_title_f"><a href="javascript:void(0)">Bitrix -> CRM</a><div class="sclew"></div></li>
            <li><a href="javascript:void(0)">CRM -> Bitrix</a><div class="sclew"></div></li>
        </ul>
        <div class="news_comp mc_block tab">
            <?
            if (count($arResult['messages']['in'])) {
                $messageDays = $arResult['messages']['in'];
                ?>
                <table class="monitoring_table">
                    <thead>
                        <tr>
                            <th>�������</th>
                            <th>��������</th>
                            <th>����</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($messageDays as $dateDay => $messages) {
                            ?>
                                <tr class="monitoring_table_date"><th colspan="3"><?=$dateDay?></th></tr>
                            <?
                            foreach ($messages as $message) {
                                ?>
                                    <tr>
                                        <td>������� 'a_crm_in' ������� ���������</td>
                                        <td>�������� � �������: <b><?=$message->getCount()?></b></td>
                                        <td><?=$message->getDate()->format('d.m.Y H.i.s')?></td>
                                    </tr>
                                <?
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <?
            }
            ?>
        </div>
        <div class="news_comp mc_block tab">
            <?
            if (count($arResult['messages']['out'])) {
                $messageDays = $arResult['messages']['out'];
                ?>
                <table class="monitoring_table">
                    <thead>
                    <tr>
                        <th>�������</th>
                        <th>��������</th>
                        <th>����</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($messageDays as $dateDay => $messages) {
                        ?>
                        <tr class="monitoring_table_date"><th colspan="3"><?=$dateDay?></th></tr>
                        <?
                        foreach ($messages as $message) {
                            ?>
                            <tr>
                                <td>�������� c ID <?=$message->getCompanyId()?> ������� ���������</td>
                                <td>
                                    <?
                                        $fieldsInfo = $message->getUpdatedFields();
                                        $fieldsInfo = array_map(function($element) {
                                                foreach ($element as $field => $value) {
                                                    return $field.' -> '.$value;
                                                }
                                            }, $fieldsInfo);
                                        $fieldsString = implode('<br>', $fieldsInfo);
                                    ?>
                                    <?=$fieldsString?>
                                </td>
                                <td><?=$message->getDate()->format('d.m.Y H.i.s')?></td>
                            </tr>
                        <?
                        }
                    }
                    ?>
                    </tbody>
                </table>
            <?
            }
            ?>
            </div>
        </div>
    <?
}

//PrintAdmin(124);
//PrintAdmin($this);
//PrintAdmin($arResult);
