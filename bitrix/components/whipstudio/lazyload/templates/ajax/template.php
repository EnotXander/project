<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


foreach($arResult["COMPONENTS"] as $arComponent)
{
   $APPLICATION->IncludeComponent($arComponent["NAME"], $arComponent["TEMPLATE"], $arComponent["PARAMETERS"]);
}