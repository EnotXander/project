<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult = array();
foreach($arParams["COMPONENTS"] as $arComponent)
{
    $componentInfo = array();
    $componentInfo = $arComponent;
    $componentInfo["MD5"] = md5($arParams["SALT"].serialize($arComponent));
    $arrayForPOST = $componentInfo;
    $arrayForPOST["PARAMETERS"] = serialize($arComponent["PARAMETERS"]);
    $componentInfo["POST"] = http_build_query($arrayForPOST);

    if (is_array($_GET) && count($_GET)) {
        $componentInfo["GET"] = http_build_query($_GET);
    }

    $arResult["COMPONENTS"][] = $componentInfo;
}

$this->IncludeComponentTemplate();