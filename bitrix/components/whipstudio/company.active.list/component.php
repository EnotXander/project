<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule( 'highloadblock' ); //������ highload ����������
CModule::IncludeModule( 'iblock' ); //������ highload ����������

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$hlblock_requests           = HL\HighloadBlockTable::getById( 5 )->fetch();  //requests
$entity_requests            = HL\HighloadBlockTable::compileEntity( $hlblock_requests );
$entity_requests_data_class = $entity_requests->getDataClass();
$main_query_requests = new Entity\Query( $entity_requests_data_class );//

//�������� ����
$arAccessGroups = array(1, 14);//�����, ��������
$arUserGroups = $USER->GetUserGroup($USER->GetID());
global $USER;
if($USER->IsAuthorized())
{
    $redirect = true;
    foreach($arAccessGroups as $group)
    {
        if(in_array($group, $arUserGroups))
            $redirect = false;
    }
    if($redirect)
        LocalRedirect("/");
} else {
    LocalRedirect("/");
}


if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
$firmArr = array();
$fullCompaniesCnt = 0;


//��������� �������
$arFilter = array(
    "IBLOCK_ID" => IBLOCK_PRODUCTS,
    "!PROPERTY_FIRM" => false
);
$res = $objElement->GetList(array(), $arFilter, array("PROPERTY_FIRM"));

$fullTovarsCnt = 0;
while($element = $res->GetNext())
{
    if (!isset($firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]]))
    {
        $fullCompaniesCnt++;
        $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] = 0;
    }
    $fullTovarsCnt += $element['CNT'];
    $firmArr[IBLOCK_PRODUCTS][$element["PROPERTY_FIRM_VALUE"]] = $element['CNT'];
    $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] += $element['CNT'];
}


//��������� �����
$arFilter = array(
    "IBLOCK_ID" => IBLOCK_USLUGI,
    "!PROPERTY_FIRM" => false
);
$res = $objElement->GetList(array(), $arFilter, array("PROPERTY_FIRM"));

$fullServicesCnt = 0;
while($element = $res->GetNext())
{
    if (!isset($firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]]))
    {
        $fullCompaniesCnt++;
        $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] = 0;
    }
    $fullServicesCnt += $element['CNT'];
    $firmArr[IBLOCK_USLUGI][$element["PROPERTY_FIRM_VALUE"]] = $element['CNT'];
    $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] += $element['CNT'];
}

if ($arParams['FILTER'] == 'OFF') {

	//��������� ��������
	$arFilter = array(
	    "IBLOCK_ID" => IBLOCK_COMPANY,
	    "ID" => array_keys($firmArr['ALL']),
	    "!PROPERTY_REMOVE_REL" => false
	);
} else {

	//��������� ��������
	$arFilter = array(
	    "IBLOCK_ID" => IBLOCK_COMPANY,
	    "ID" => array_keys($firmArr['ALL']),
	    "=PROPERTY_REMOVE_REL" => false
	);
}

$res = $objElement->GetList(
    array("NAME"=>"ASC", "ID"=>"ASC"),
    $arFilter,
    false,
    false,
    array('ID', 'IBLOCK_ID', 'PROPERTY_TARIF','PROPERTY_TARIF_DATE_START','PROPERTY_TARIF_DATE_STOP',
        'PROPERTY_MANAGER', 'PROPERTY_METRIKA_ID', 'DETAIL_PAGE_URL')
);


// ������ �� ���������
// $col = '-';
//
$main_query_requests = new Entity\Query( $entity_requests_data_class );// JuristAnsverLike
      $main_query_requests->setSelect( array( '*' ) );
      $result_requests = $main_query_requests->exec();
      $result_requests = new CDBResult( $result_requests );
// ��������� ���������

while ($item = $result_requests->getNext()) {
    $arStat[$item['UF_FIRM_ID']] = $item;
}


$fullTovarsActiveLinkCnt = 0;
$arCompanies = array();
$arTarifs = array();
$arManagers = array();
$arViews   = array();


while($element = $res->GetNext())
{

    $arCompanies[$element['ID']] = array(
        'ID'       => $element['ID'],
        'NAME'     => $element['NAME'],
        'LINK'     => $element['DETAIL_PAGE_URL'],
        'PRODUCTS' => $firmArr[IBLOCK_PRODUCTS][$element['ID']],
        'SERVICES' => $firmArr[IBLOCK_USLUGI][$element['ID']],
        'VIEWED'   => $arStat[$element['ID']]['UF_VIEW'],
        'PHONE'    => $arStat[$element['ID']]['UF_PHONE'],
        'TARIF'    => $element['PROPERTY_TARIF_VALUE'],
        'START'    => $element['PROPERTY_TARIF_DATE_START_VALUE'],
        'STOP'     => $element['PROPERTY_TARIF_DATE_STOP_VALUE'],
        'MANAGER'  => $element['PROPERTY_MANAGER_VALUE']
    );

    if ($element['PROPERTY_TARIF_VALUE']) {
        $arTarifs[$element['PROPERTY_TARIF_VALUE']] = $element['PROPERTY_TARIF_VALUE'];
    }

    if ($element['PROPERTY_MANAGER_VALUE']) {
        $arManagers[$element['PROPERTY_MANAGER_VALUE']] = $element['PROPERTY_MANAGER_VALUE'];
    }

    $fullTovarsActiveLinkCnt   += $firmArr[IBLOCK_PRODUCTS][$element['ID']];
    $fullServicesActiveLinkCnt += $firmArr[IBLOCK_USLUGI][$element['ID']];
}


//��������� �������
$arFilter = array(
    "IBLOCK_ID" => IBLOCK_TARIF,
    "ID" => $arTarifs,
);
$res = $objElement->GetList(
    array("NAME"=>"ASC", "ID"=>"ASC"),
    $arFilter
);

while($element = $res->GetNext())
{
    $arTarifs[$element['ID']] = $element;
}

//��������� ����������
$res = $USER->GetList(($by="personal_country"), ($order="desc"), array('ID' => implode('|', $arManagers)));

while($element = $res->GetNext())
{
    $arManagers[$element['ID']] = $element;
};

$file =$_SERVER['DOCUMENT_ROOT'].'/../energo/personal/control/'.LANG.'_company_list.csv';

//������������ csv
$fp = fopen($file, 'w');
$element = ['��', '��������', 'Url',
    '�������', '�����', '��������� �� 30����','������� �� ��� �� 30����',
    '�����', '������ ������', '���������� ������','��������'];
fputcsv($fp, $element, ";");
foreach($arCompanies as $key=>&$element)
{
	$mid = $element['MANAGER'];
	$element['VIEWED']  = $arStat[$element['ID']]['UF_VIEW'];
	$element['PHONE']   = $arStat[$element['ID']]['UF_PHONE'];
	$element['TARIF']   = $arTarifs[$element['TARIF']]['NAME'];
	//$element['START']   = $element['START'];
	//$element['STOP']    = $element['STOP'];
	$element['MANAGER'] = $arManagers[$mid]['NAME'].' '.$arManagers[$mid]['LAST_NAME'];

    foreach ($element as $elKey=>$el)
	    $element[$elKey] = str_replace('"', "", html_entity_decode($el));

    fputcsv($fp, $element, ";");
}
fclose($fp);
// ��������� $arCompanies

function cmp($a, $b)
{
    if ($a['PRODUCTS'] == $b['PRODUCTS']) {
        return 0;
    }
    return ($a['PRODUCTS'] < $b['PRODUCTS']) ? 1 : -1;
}


usort($arCompanies, "cmp");

//������
$arResult['COMPANIES'] = $arCompanies;
$arResult['STAT']['fullTovarsActiveLinkCnt'] = $fullTovarsActiveLinkCnt;
$arResult['STAT']['fullServicesActiveLinkCnt'] = $fullServicesActiveLinkCnt;
$arResult['STAT']['fullTovarsCnt'] = $fullTovarsCnt;
$arResult['STAT']['fullServicesCnt'] = $fullServicesCnt;
$arResult['STAT']['fullCompaniesCnt'] = $fullCompaniesCnt;
//$arResult['TARIFS'] = $arTarifs;
//$arResult['MANAGERS'] = $arManagers;
$arResult['VIEW'] = $arViews;

$this->IncludeComponentTemplate();