<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<? if ($arParams['FILTER'] == 'OFF') {
 ?>
<h1>������ �������� � ������������ ��������</h1>
<? } else {?>
    <h1>������ �������� � ��������� ��������</h1>
<? } ?>
<div class="paging" style="margin-bottom: 10px;">
   <span>�������� � �������� � � �������� ������: <b><?=count($arResult['COMPANIES'])?></b>. <a href="<?=LANG?>_company_list.csv">������� CSV</a></span>
</div>

<table class="simple-table" width="100%" style="margin-right: 30px;">
    <thead>
    <tr>
        <th>ID</th>
        <th>��������</th>
        <th>�������</th>
        <th>�����</th>
        <th>��������� �� 30�</th>
        <th>������� ���. �� 30�</th>
        <th>�����</th>
        <th>��������</th>
    </tr>
    </thead>
    <tbody>
    <?foreach($arResult['COMPANIES'] as $arCompany):?>
        <tr>
            <td style="padding: 5px;"><?=$arCompany['ID']?></td>
            <td style="padding: 5px;"><a target="_blank" href="<?=$arCompany['LINK']?>"><?=$arCompany['NAME']?></a></td>
            <td style="padding: 5px;"><?=$arCompany['PRODUCTS']?></td>
            <td style="padding: 5px;"><?=$arCompany['SERVICES']?></td>
            <td style="padding: 5px;"><?= $arCompany['VIEWED'] ?></td>
            <td style="padding: 5px;"><?= $arCompany['PHONE'] ?></td>
            <?if($arCompany['TARIF']):?>
                <td style="padding: 5px;">
                    <?=$arCompany['TARIF']?><br/>
                    <?=$arCompany['START']?>-<?=$arCompany['STOP']?>
                </td>
            <?else:?>
                <td style="padding: 5px;">-</td>
            <?endif;?>
            <?if( $arCompany['MANAGER'] ):?>
                <td style="padding: 5px;"><?= $arCompany['MANAGER'] ?></td>
            <?else:?>
                <td style="padding: 5px;">-</td>
            <?endif;?>
        </tr>
    <?endforeach;?>
    </tbody>
</table>

<div class="paging" style="margin-bottom: 10px;">
    <div>����� �������, ����������� � �������� � �������� ������: <b><?=$arResult['STAT']['fullTovarsActiveLinkCnt']?></b></div>
    <div>����� �����, ����������� � �������� � �������� ������: <b><?=$arResult['STAT']['fullServicesActiveLinkCnt']?></b></div>
    <div>����� �������, ����������� � ����� ���������: <b><?=$arResult['STAT']['fullTovarsCnt']?></b></div>
    <div>����� �����, ����������� � ����� ���������: <b><?=$arResult['STAT']['fullServicesCnt']?></b></div>
    <div>����� �������� � ��������: <b><?=$arResult['STAT']['fullCompaniesCnt']?></b></div>
</div>