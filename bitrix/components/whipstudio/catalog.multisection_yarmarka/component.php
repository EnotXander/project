<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
}
if ( ! CModule::IncludeModule( "iblock" ) ) ;


include ( $_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/tools/companies.php');
include ( 'functions.php');

global $USER;
global $APPLICATION;
global $CACHE_MANAGER;
$obParser = new CTextParser;


//���������� ������
global ${$arParams["FILTER_NAME"]};
$arrFilter = ${$arParams["FILTER_NAME"]};
if ( ! is_array( $arrFilter ) ) {
	$arrFilter = array();
}
$arParams["COMPANY_ELEMENT_MORE_COUNT"] = (int) $arParams["COMPANY_ELEMENT_MORE_COUNT"];
$arParams["CURPAGE"]                    = (int) $_REQUEST["CURPAGE"] ? (int) $_REQUEST["CURPAGE"] : 0;

$arResult = array();

//*****************************************************************************************************************
//
//          �������
//
//*****************************************************************************************************************

//�������� ���������� �� ������� �������
if ( ! $arParams["SECTION_ID"] && ! strlen( $arParams["SECTION_CODE"] ) ) {
	//ShowError( "������ �� ������" );

	//return;
}
$thisSectionFilter              = array();
$thisSectionFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
if ( $arParams["SECTION_ID"] ) {
	$thisSectionFilter["ID"] = $arParams["SECTION_ID"];
} else {
	$thisSectionFilter["=CODE"] = $arParams["SECTION_CODE"];
}
$rsThisSection = CIBlockSection::GetList(
	array(),
	$thisSectionFilter,
	false,
	array(
		$arParams["BROWSER_TITLE"],
		$arParams["META_DESCRIPTION"],
		$arParams["META_KEYWORDS"]
	)
);
$rsThisSection->SetUrlTemplates( "", $arParams["SECTION_URL"] );
if ( ! $arResult["THIS_SECTION"] = $rsThisSection->GetNext() ) {
	//ShowError( "������ {$arParams["SECTION_ID"]} �� ������" );

	//return;
}

//�������� ��������� �������

$arResult["LISTS"] = cacheFunction( 36000, 'LISTS' . $arResult["THIS_SECTION"]["ID"] . $arParams["IBLOCK_ID"], function () use ( $arParams, $arResult ) {
	$res = array();
	if ( $arParams["INCLUDE_SUBLISTS"] ) {
		$rsSublistSection = CIBlockSection::GetList(
			array( "SORT" => "ASC", "NAME" => "ASC" ),
			array(
				"IBLOCK_ID"  => $arParams["IBLOCK_ID"],
				"ACTIVE"     => "Y",
				"SECTION_ID" => $arResult["THIS_SECTION"]["ID"]
			), false, array( 'NAME', 'ID', 'SECTION_PAGE_URL' )
		);
		$rsSublistSection->SetUrlTemplates( "", $arParams["SECTION_URL"] );
		while ( $arSublistSection = $rsSublistSection->GetNext() ) {
			$res[] = $arSublistSection;
		}
	}
	if (empty($res))
		$res[] = array("ID"=>$arResult["THIS_SECTION"]["ID"]);


	return $res;
} );
$hash = 'REGION_COUNT' . $arResult["THIS_SECTION"]["ID"] . $arParams["IBLOCK_ID"].md5(serialize($arrFilter));
$arResult["REGION_COUNT"] = cacheFunction( 0, $hash, function () use ($arrFilter, $arParams, $arResult ) {

	$res =[ ];
	$ff =  array_merge($arrFilter, array(
				"IBLOCK_ID"  => $arParams["IBLOCK_ID"],
				"ACTIVE"     => "Y",
				"INCLUDE_SUBSECTIONS" =>'Y',
				"SECTION_ID" => $arResult["THIS_SECTION"]["ID"]
			));
	// ��������� �������� � ������
	$rsElements = CIBlockElement::GetList(
		array(),
		$ff,
		array('PROPERTY_REGION')
	);

	while ( $arElements = $rsElements->GetNext() ) {
		if (!empty($arElements['PROPERTY_FIRM_PROPERTY_REGION_VALUE'])) {
			$res [ $arElements['PROPERTY_REGION_VALUE'] ] = $arElements['CNT'];
		}
	}

	//$res =[ ];
	// ��������� �������� � ������
	$rsElements = CIBlockElement::GetList(
		array(),
		$ff,
		array("PROPERTY_FIRM.PROPERTY_REGION")
	);

	while ( $arElements = $rsElements->GetNext() ) {

		if (!empty($arElements['PROPERTY_FIRM_PROPERTY_REGION_VALUE'])) {
			$res [$arElements['PROPERTY_FIRM_PROPERTY_REGION_VALUE']] = 1;
		}

	}

	return $res;
} );

//*****************************************************************************************************************
//
//          ����������
//
//*****************************************************************************************************************

$arResult["SORT"] = array();
//�� ���������
$sortDefaultBy = $arParams["SORT_DEFAULT"];
$arSortDefault = $arParams["SORT"];
//��������� �� ����
$sort_by = $_REQUEST["sort_by"];
if ( ! strlen( $sort_by ) ) {
	$sort_by = $sortDefaultBy;
}
$sort_type = urldecode( $_REQUEST["sort_type"] );
if ( ! strlen( $sort_type ) ) {
	$sort_type = $arSortDefault[ $sortDefaultBy ]["TYPE"];
}
//���������
$arSortArrow   = array(
	"asc"  => true,
	"desc" => false,
);
$arSortReverse = array(
	"asc"        => "desc",
	"desc"       => "asc",
	"asc,nulls"  => "desc,nulls",
	"desc,nulls" => "asc,nulls"
);



//������� ���������� ��� GetList
$arResult["SORT_BY"] = $sort_by;
foreach ( $arSortDefault as $code => $value ) {
	if ( $sort_by == $code ) {
		$sort_by = $value["CODE"];
	}
}
$arResult["SORT_FOR_ITEMS"] = array(
	$sort_by => $sort_type,
	//"SORT" => "ASC",
	//"NAME" => "ASC"
);


//*****************************************************************************************************************
//
//          ��������
//
//*****************************************************************************************************************

//��������� ��������, ������� ������� � ���������
$arResult["COMPANY_LIST"] = array();
$arResult["BRANDS_LIST"]  = array();
$arResult["SORT_COMPANY"] = array(
	"ITEMS_IDS"   => array(),
	"COMPANY_IDS" => array()
);
$arParams["REGION"] = $_GET['region'];

$arResult["COMPANY_LIST"] = cacheFunction( 36000, 'COMPANY_LIST' . $arResult["THIS_SECTION"]["ID"].$arParams['REGION'], function () use ( $arParams, $arResult ) {
	$res           = [ ];
    $filter = array(
		"IBLOCK_ID"           => $arParams["IBLOCK_ID"],
		"ACTIVE"              => "Y",
		"SECTION_ID"          => $arResult["THIS_SECTION"]["ID"],
		"INCLUDE_SUBSECTIONS" => "Y"
	);
	if (!empty($arParams['REGION'])) {
		$arFilter["PROPERTY_REGION"] = intval($arParams['REGION']);
	}

	$rsAllElements = CIBlockElement::GetList(
		array( "PROPERTY_FIRM.NAME" => "ASC" ),
		$filter,
		false,
		false,
		array(
			"ID",
			"NAME",
			"IBLOCK_SECTION_ID",
			"PROPERTY_FIRM.ID",
			"PROPERTY_FIRM.NAME",
			"PROPERTY_FIRM.DETAIL_PAGE_URL",
			"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
			"PROPERTY_FIRM.PROPERTY_YARMARKA"
		)
	);
	while ( $arAllElements = $rsAllElements->GetNext() ) {
		//TODO: ������
		if ( empty($arAllElements["PROPERTY_FIRM_ID"]) ) {
			continue;
		}
		if ( empty($arAllElements["PROPERTY_FIRM_PROPERTY_YARMARKA_VALUE"]) ) {
			continue;
		}
		//PrintAdmin($arAllElements);

		//���������� � ��������
		if ( ! isset( $res[ $arAllElements["PROPERTY_FIRM_ID"] ] ) ) {
			//���� �������� ���������, �� ������� ���� � ��������
			if ( $arAllElements["PROPERTY_FIRM_PROPERTY_REMOVE_REL_ENUM_ID"] != false ) {
				//continue;
			}

			$res[ $arAllElements["PROPERTY_FIRM_ID"] ] = array(
				"ID"              => $arAllElements["PROPERTY_FIRM_ID"],
				"NAME"            => $arAllElements["PROPERTY_FIRM_NAME"],
				"DETAIL_PAGE_URL" => $arAllElements["PROPERTY_FIRM_DETAIL_PAGE_URL"],
				"ITEMS_COUNT"     => 1
			);
		} else {
			$res[ $arAllElements["PROPERTY_FIRM_ID"] ]["ITEMS_COUNT"] ++;
		}

	}

	return $res;
} );

//*****************************************************************************************************************
//
//          ��������
//
//*****************************************************************************************************************

//�������� �������� ������� �������� �� ������� ������
$arResult["COMPANY_DETAIL"] = array();

if ( count( $arResult["LISTS"] ) == 1 ) {

	unset( $arParams["SHOW_GOODS"] );

}

//$arResult["LISTS"] ������
if ( count($arResult["LISTS"]) == 1 ) { // ���� ��������� �������
	foreach ( $arResult["LISTS"] as $arListKey => $arList ) {
		if ( ( $arList["ID"] == $arResult["THIS_SECTION"]["ID"] ) && ( count( $arResult["LISTS"] ) > 1 ) ) {
			$arList["NAME"] = "��� � �������";
		}
		if ( $arList["NAME"] == "��� � �������" || count( $arResult["LISTS"] ) == 1 ) {
			if ( $arResult["SORT_BY"] != "company" ) {
				//����������

				$arFilter               = array();
				$arFilter["IBLOCK_ID"]  = $arParams["IBLOCK_ID"];
				$arFilter["ACTIVE"]     = "Y";
				$arFilter["=PROPERTY_ACTIVE"] = false;
				$arFilter["SECTION_ID"] = $arList["ID"];
				if ( ( $arList["ID"] == $arResult["THIS_SECTION"]["ID"] ) && ( count( $arResult["LISTS"] ) > 1 ) ) {
					$arList["NAME"] = "��� � �������";
				} else {
					$arFilter["INCLUDE_SUBSECTIONS"] = true;
				}
				// $_GET['region']
				if (!empty($_GET['region'])) {
					$arFilter["PROPERTY_REGION"] = intval($_GET['region']);
				}
				// $arFilter["PROPERTY_FIRM.PROPERTY_YARMARKA_VALUE"] = '��';

				//���������
				//$GLOBALS["NavNum"] = $arList["ID"];
				$arNavParams = array();//**************
				$arNavParams = array(
					"nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
					"bShowAll"  => $arParams["PAGER_SHOW_ALL"],
				);
				if ( (int) $_REQUEST["PAGEN"] && (int) $_REQUEST["CURPAGE"] == $arList["ID"] ) {
					$arNavParams["iNumPage"] = (int) $_REQUEST["PAGEN"];
				} else {
					$arNavParams["iNumPage"] = 1;
				}
			} else { // ���������� �� ���������
				//����������
				if (empty($arResult["COMPANY_LIST"])) {
						$arFilter = array( // ������ ��������
							"ID"        => -1
						);
			   } else {
						$arFilter = array( // ������ ��������
							"ID"        => array_keys( $arResult["COMPANY_LIST"] )
						);
			   }
				if (!empty($_GET['region'])) {
					$arFilter["PROPERTY_REGION"] = intval($_GET['region']);
				}


				//���������
				//$GLOBALS["NavNum"] = $arList["ID"];
				$arNavParams = array();//**************
				$arNavParams = array(
					"nPageSize" => ceil( ( $arParams["PAGE_ELEMENT_COUNT"] / $arParams["COMPANY_ELEMENT_COUNT"] ) * 2 ),
					"bShowAll"  => $arParams["PAGER_SHOW_ALL"],
				);
				if ( (int) $_REQUEST["PAGEN"] ) {
					$arNavParams["iNumPage"] = (int) $_REQUEST["PAGEN"];
				} else {
					$arNavParams["iNumPage"] = 1;
				}

				//��������� ������ ��������
				$arResult["COMPANY_ONPAGE"] = array();
				$rsCompanyOnPage            = CIBlockElement::GetList(
					array(
						"PROPERTY_EXT_SORT" => $sort_type,
						"SORT" => "asc"
					),
					$arFilter,
					false,
					$arNavParams,
					array(
						"ID",
						"NAME"
					)
				);
				$rsCompanyOnPage->SetUrlTemplates( "", $arParams["DETAIL_URL"] );
				while ( $arCompanyOnPage = $rsCompanyOnPage->GetNext() ) {
					$arResult["COMPANY_ONPAGE"][ $arCompanyOnPage["ID"] ] = $arCompanyOnPage;
				}

				//������ ���������
				$rsCompanyOnPage->NavNum   = $arList["ID"];
				$arList["NAV_STRING"]      = $rsCompanyOnPage->GetPageNavStringEx( $navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"] );
				$arList["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
				//$arList["NAV_RESULT"] = $rsElements;

				//���������� ������
				$arResult["SORT_FOR_ITEMS"] = array(
					"PROPERTY_EXT_SORT" => "DESC",
					"SORT"                  => "ASC",
					"NAME"                  => "ASC"
				);

				//���������� ������
				$arFilter                  = array();
				$arFilter["IBLOCK_ID"]     = $arParams["IBLOCK_ID"];
				$arFilter["ACTIVE"]        = "Y";
				$arFilter["SECTION_ID"]    = $arList["ID"];
				$arFilter["PROPERTY_FIRM"] = array_keys( $arResult["COMPANY_ONPAGE"] );
				if ( ( $arList["ID"] == $arResult["THIS_SECTION"]["ID"] ) && ( count( $arResult["LISTS"] ) > 1 ) ) {
					$arList["NAME"] = "��� � �������";
				} else {
					$arFilter["INCLUDE_SUBSECTIONS"] = true;
				}
			/*	foreach ($_REQUEST['filter'] as $code=>$f1){ // �����������
					foreach ($f1 as $value){
						if (substr($code, 0,3) == 'F__'){
							$arFilter["=PROPERTY_".$code.''][] = $value;
						} else {
							$arFilter["=PROPERTY_".$code.'_VALUE'][] = $value;
						}

					}
				}


				PrintAdmin($arFilter);*/

				//��������� ������
				$arNavParams = array();//**************
				$arNavParams = false;
			}

			//��������� ������� � �������
			$arList["GROUPS"] = array();
			$groupIndex       = - 1;//����� ��������������� �� ��������� ������� � ������
			//PrintAdmin(array_merge( $arrFilter, $arFilter ));
			$rsElements = CIBlockElement::GetList(
				$arResult["SORT_FOR_ITEMS"],
				array_merge( $arrFilter, $arFilter ),
				false,
				$arNavParams,
				array(
					"ID",
					"NAME",
					"PREVIEW_PICTURE",
					"PREVIEW_TEXT",
					"DETAIL_PICTURE",
					"DETAIL_TEXT",
					"DETAIL_PAGE_URL","SHOW_COUNTER","DATE_CREATE","PROPERTY_OPT","PROPERTY_ROZNICA",
					"PROPERTY_COST",
					"PROPERTY_CURRENCY",
					"PROPERTY_unit_tov",
					"PROPERTY_FIRM.ID",
					"PROPERTY_FIRM.IBLOCK_ID",
					"PROPERTY_FIRM.NAME",
					"PROPERTY_FIRM.PREVIEW_PICTURE",
					"PROPERTY_FIRM.DETAIL_PICTURE",
					"PROPERTY_FIRM.DETAIL_PAGE_URL",
					"PROPERTY_FIRM.PROPERTY_USER",
					"PROPERTY_FIRM.PROPERTY_EMAIL",
					"PROPERTY_FIRM.PROPERTY_MANAGER",
					"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
					"PROPERTY_FIRM.PROPERTY_YARMARKA",
					"PROPERTY_FIRM.PROPERTY_PREMIUM"
				)
			);
			$rsElements->SetUrlTemplates( "", $arParams["DETAIL_URL"] );
			while ( $arElements = $rsElements->GetNext() ) {
				//PrintAdmin($arElements);die();
				//�������� ������������� ��������
				$arMultiPropList = array( "FAVORITES" );
				foreach ( $arMultiPropList as $multiPropName ) {
					$arElements[ "PROPERTY_" . $multiPropName ] = array();
					$rsMultiProp                                = CIBlockElement::GetProperty(
						$arElements["IBLOCK_ID"],
						$arElements["ID"],
						array(),
						array( "CODE" => $multiPropName )
					);
					while ( $arMultiProp = $rsMultiProp->GetNext() ) {
						$arElements[ "PROPERTY_" . $multiPropName ][]            = $arMultiProp;
						$arElements[ "PROPERTY_" . $multiPropName . "_VALUE" ][] = $arMultiProp["VALUE"];
					}
				}

				//������ �����
				if ( ! strlen( $arElements["PREVIEW_TEXT"] ) ) {
					$arElements["PREVIEW_TEXT"] = strip_tags( $obParser->html_cut( $arElements["DETAIL_TEXT"], $arParams["DETAIL_TEXT_CUT"] ) );
				}

				//������
				$arElements["PREVIEW_PICTURE"] = Resizer(
					array( $arElements["PREVIEW_PICTURE"], $arElements["DETAIL_PICTURE"] ),
					array( "width" => 100, "height" => 100 ),
					BX_RESIZE_IMAGE_EXACT
				);
				if ( ! strlen( $arElements["PREVIEW_PICTURE"]["SRC"] ) ) {
					$arElements["PREVIEW_PICTURE"]["SRC"] = "/images/withoutphoto54x44.png";
				}

				//�������������� ������/������ ���������
				$arElements["PROPERTY_CURRENCY_FORMATED"] = StrUnion( array(
					$arElements["PROPERTY_CURRENCY_VALUE"],
					$arElements["PROPERTY_UNIT_TOV_VALUE"]
				), "/" );

				//���� �������� ���������, �� ������� ���� � ��������
				if ( $arElements["PROPERTY_FIRM_PROPERTY_REMOVE_REL_ENUM_ID"] != false ) {
					//$arElements["PROPERTY_FIRM_ID"] = 0;
				}
				//���� ��������� ������� �� �� ����������
				if ( $arElements["PROPERTY_FIRM_PROPERTY_YARMARKA_ENUM_ID"] == false ) {
					$arElements["PROPERTY_FIRM_ID"] = 0;
				}

				//��������� ���� � ��������
				if ( ! isset( $arResult["COMPANY_DETAIL"][ $arElements["PROPERTY_FIRM_ID"] ] ) ) {
					//TODO: ������ ���������� �������� �������
					$arCompany = array(
						"ID"               => $arElements["PROPERTY_FIRM_ID"],
						"IBLOCK_ID"        => $arElements["PROPERTY_FIRM_IBLOCK_ID"],
						"NAME"             => $arElements["PROPERTY_FIRM_NAME"],
						"PREVIEW_PICTURE"  => $arElements["PROPERTY_FIRM_PREVIEW_PICTURE"],
						"DETAIL_PICTURE"   => $arElements["PROPERTY_FIRM_DETAIL_PICTURE"],
						"DETAIL_PAGE_URL"  => $arElements["PROPERTY_FIRM_DETAIL_PAGE_URL"],
						"PROPERTY_USER"    => $arElements["PROPERTY_FIRM_PROPERTY_USER_VALUE"],
						"PROPERTY_EMAIL"   => $arElements["PROPERTY_FIRM_PROPERTY_EMAIL_VALUE"],
						"PROPERTY_MANAGER" => $arElements["PROPERTY_FIRM_PROPERTY_MANAGER_VALUE"],
						"PREMIUM"          => $arElements["PROPERTY_FIRM_PROPERTY_PREMIUM_VALUE"],
						"ITEMS_COUNT"      => array( $arList["ID"] => 1 )
					);
					//������� �������� ��������
					if ( $arParams["COMPANY_NAME_CUT"] && ( strlen( html_entity_decode( $arCompany["NAME"] ) ) > ( $arParams["COMPANY_NAME_CUT"] + 2 ) ) ) {
						$arCompany["NAME_CUT"] = $obParser->html_cut( html_entity_decode( $arCompany["NAME"] ), $arParams["COMPANY_NAME_CUT"] );
					} else {
						$arCompany["NAME_CUT"] = $arCompany["NAME"];
					}
					//������
					$arCompany["PREVIEW_PICTURE"] = Resizer(
						array( $arCompany["PREVIEW_PICTURE"], $arCompany["DETAIL_PICTURE"] ),
						array( "width" => 148, "height" => 48 ),
						BX_RESIZE_IMAGE_PROPORTIONAL_ALT
					);
					//�������� ������������� ��������
					$arMultiPropList = array( "phone", "URL", "FAVORITES" );
					foreach ( $arMultiPropList as $multiPropName ) {
						$arCompany[ "PROPERTY_" . $multiPropName ] = array();
						$rsMultiProp                               = CIBlockElement::GetProperty(
							$arCompany["IBLOCK_ID"],
							$arCompany["ID"],
							array(),
							array( "CODE" => $multiPropName )
						);
						while ( $arMultiProp = $rsMultiProp->GetNext() ) {
							$arCompany[ "PROPERTY_" . $multiPropName ][]            = $arMultiProp;
							$arCompany[ "PROPERTY_" . $multiPropName . "_VALUE" ][] = $arMultiProp["VALUE"];
						}
					}
					//������������ ��� ����� ��������
					foreach ( $arCompany["PROPERTY_URL"] as &$url ) {
						if ( ( strpos( $url["VALUE"], 'http://' ) === false ) && ( strpos( $url["VALUE"], 'https://' ) === false ) ) {
							$url["VALUE_HREF"] = 'http://' . $url["VALUE"];
						} else {
							$url["VALUE_HREF"] = $url["VALUE"];
						}
					}
					//�������� �������������
					if ( ENABLE_PREDSTAVITEL_MODE ) {
						$arPredstavitelInfo         = PredstavitelGetByCompany( $arCompany["ID"] );
						$arCompany["PROPERTY_USER"] = $arPredstavitelInfo["RELATED"];
					}
					if ( ! $arCompany["PROPERTY_USER"] ) {
						$arCompany["PROPERTY_USER"] = $arCompany["PROPERTY_MANAGER"];
					}
					if ( $arCompany["PROPERTY_USER"] ) {
						$arCompany["PREDSTAVITEL"]               = $USER->GetByID( $arCompany["PROPERTY_USER"] )->Fetch();
						$arCompany["PREDSTAVITEL_MESSAGE_LINK"]  = "/personal/messages/{$arCompany["PREDSTAVITEL"]["ID"]}/";
						$arElements["PREDSTAVITEL_MESSAGE_LINK"] = "{$arCompany["PREDSTAVITEL_MESSAGE_LINK"]}?product={$arElements["ID"]}";
					} else {
						$arCompany["PREDSTAVITEL_MESSAGE_LINK"]  = "mailto:{$arCompany["PROPERTY_EMAIL"]}";
						$arElements["PREDSTAVITEL_MESSAGE_LINK"] = $arCompany["PREDSTAVITEL_MESSAGE_LINK"];
					}

					//���������� ��������
					$arResult["COMPANY_DETAIL"][ $arElements["PROPERTY_FIRM_ID"] ] = $arCompany;
				} else {
					$arResult["COMPANY_DETAIL"][ $arElements["PROPERTY_FIRM_ID"] ]["ITEMS_COUNT"][ $arList["ID"] ] ++;
				}
				$company = $arResult["COMPANY_DETAIL"][ $arElements["PROPERTY_FIRM_ID"] ];

				//������ �� ������� ������
				$arElements["PREDSTAVITEL_MESSAGE_LINK"] = $company["PREDSTAVITEL_MESSAGE_LINK"];
				if ( strpos( $company["PREDSTAVITEL_MESSAGE_LINK"], "mailto" ) === false ) {
					$arElements["PREDSTAVITEL_MESSAGE_LINK"] .= "?product={$arElements["ID"]}";
				}

				if ( $arResult["SORT_BY"] != "company" ) {
					//����������� �� ���������
					if (
						( $groupIndex < 0 )//� ������ ��� ��� �� ����� ������
						|| ! $arElements["PROPERTY_FIRM_ID"]//����� �� ����������� ��������
						|| ( $arElements["PROPERTY_FIRM_ID"] != $arList["GROUPS"][ $groupIndex ]["COMPANY"] )//�������� ����� ������ ���������� �� �������� ���� ������
					) {
						$groupIndex ++;
					}
					$arList["GROUPS"][ $groupIndex ]["COMPANY"] = $arElements["PROPERTY_FIRM_ID"];
					$arList["GROUPS"][ $groupIndex ]["ITEMS"][] = $arElements;
				} else { // �� ���������
					//������ ������� ���� � �������� �������� ������ ������
					if ( $company["ITEMS_COUNT"][ $arList["ID"] ] > $arParams["COMPANY_ELEMENT_COUNT"] ) {
						$arElements["HIDE"] = true;
					}
					//�������� ������ ������ ������� ���������
					if ( ! ( ( $company["ITEMS_COUNT"][ $arList["ID"] ] - $arParams["COMPANY_ELEMENT_COUNT"] ) % $arParams["COMPANY_ELEMENT_MORE_COUNT"] ) ) {
						$arElements["HIDE_PANEL"] = true;
					}
					//����������� �� ���������
					$groupKey                                 = array_search( $arElements["PROPERTY_FIRM_ID"], array_keys( $arResult["COMPANY_ONPAGE"] ) );
					$arList["GROUPS"][ $groupKey ]["COMPANY"] = $arElements["PROPERTY_FIRM_ID"];
					$arList["GROUPS"][ $groupKey ]["ITEMS"][] = $arElements;
				}

			}

			if ( $arResult["SORT_BY"] != "company" ) {
				//������ ���������

				$rsElements->NavNum        = $arList["ID"];
				$arList["NAV_STRING"]      = $rsElements->GetPageNavStringEx( $navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"] );
				$arList["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
				//$arList["NAV_RESULT"] = $rsElements;
			} else {
				// ���������� ����� ��� ����������� ������� ��������
				// PrintAdmin($arList);
				ksort( $arList["GROUPS"] );
			}

			//������ ���������� ������� � �������
			$arList["ITEMS_COUNT"] = CIBlockElement::GetList( Array(), array_merge( $arrFilter, $arFilter ), Array() );

			//      PrintAdmin($arList);
			//����������
			$arResult["LISTS"][ $arListKey ] = $arList;
		}
	}
//�������� ������ �������� � ����������� ������������� �������
	$arTemp = array();
	foreach ( $arResult["LISTS"] as $arList ) {
		if ( $arList["ITEMS_COUNT"] ) {
			//����������� ������� ����
			if (
				( ( $arParams["CURPAGE"] ) && ( $arParams["CURPAGE"] == $arList["ID"] ) )
				||
				( ( ! $arParams["CURPAGE"] ) && ( ! count( $arTemp ) ) )
			) {
				$arList["DISPLAYED"]          = true;
				$arResult["ACCORDION_ACTIVE"] = count( $arTemp );
			}
			$arTemp[] = $arList;
		}
	}

	if ( count( $arResult["LISTS"] ) != 1 ) {
		//$arResult["LISTS"] = $arTemp;
	}
}

//*****************************************************************************************************************
//
//          ����
//
//*****************************************************************************************************************

if ( strlen( $arResult["THIS_SECTION"][ $arParams["BROWSER_TITLE"] ] ) ) {
	$APPLICATION->SetTitle( $arResult["THIS_SECTION"][ $arParams["BROWSER_TITLE"] ] );
}
if ( strlen( $arResult["THIS_SECTION"][ $arParams["META_DESCRIPTION"] ] ) ) {
	$APPLICATION->SetPageProperty( "description", $arResult["THIS_SECTION"][ $arParams["META_DESCRIPTION"] ] );
}
if ( strlen( $arResult["THIS_SECTION"][ $arParams["META_KEYWORDS"] ] ) ) {
	$APPLICATION->SetPageProperty( "keywords", $arResult["THIS_SECTION"][ $arParams["META_KEYWORDS"] ] );
}


//TODO: ������ ����� �� ������ ������
//TODO: ������ premium
//TODO: ��������� ������ ���� � ������ ����������, ����� ��� �� ��������� ���������� �������
//TODO: �����������


//��������� ������
if ( count( $arResult["ERROR"] ) > 0 ) {
	ShowError( implode( "<br>", $arResult["ERROR"] ) );

	return;
}

//PrintAdmin($arResult);

$this->IncludeComponentTemplate();