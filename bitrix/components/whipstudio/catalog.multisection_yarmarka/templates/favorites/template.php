<div class="left_col">
   <?if(count($arResult["LISTS"])):?>
      <div class="lc_block">
         <div class="title">���������</div>
         <ul class="left_menu">
            <?foreach ($arResult["LISTS"] as $arList):?>
               <li><a href="<?=$arList["SECTION_PAGE_URL"]?>"><?=$arList["NAME"]?></a></li>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
   <?if(count($arResult["COMPANY_LIST"])):?>
      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?foreach ($arResult["COMPANY_LIST"] as $arCompany): ?>
               <li><a href="<?=$arCompany["DETAIL_PAGE_URL"]?>"><?=$arCompany["NAME"]?></a></li>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
   <?if(count($arResult["BRANDS_LIST"])):?>
      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?foreach ($arResult["BRANDS_LIST"] as $arBrands):?>
               <li><a href="<?=$arBrands["DETAIL_PAGE_URL"]?>"><?=$arBrands["NAME"]?></a> <img src="<?=$arBrands["PREVIEW_PICTURE"]?>"> </li>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
   <div class="lc_block">
      <div class="left_banner_185">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "A8",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
   <div class="lc_block">
      <div class="left_banner_185">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "A8",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
</div>

<div class="middle_col">
   <h1><?=$arResult["THIS_SECTION"]["NAME"]?></h1>
   <div class="mc_block">
      <div class="mc_block_banner">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "MARKET_LIST",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>

      <ul class="mb_menu ml">
         <li>
            <a href="<?=$APPLICATION->GetCurPage()?>"><i class="icons icon_list list_sel"></i>������</a>
            <div class="sclew"></div>
         </li>
         <!--<li>
            <a href="#"><i class="icons icon_gallery gallery_sel"></i>�������</a>
            <div class="sclew"></div>
         </li>
         <li>
            <a href="#"><i class="icons icon_map map_sel"></i>�� �����</a>
            <div class="sclew"></div>
         </li>-->
         <li class="sel">
            <a href="#"><i class="icons icon_star star_sel"></i>���������</a>
            <div class="sclew"></div>
         </li>
      </ul>
      <!--<div class="search-box">
         <input type="text" name="" id="" />
         <a href="#">�����</a>
      </div>-->
      <div class="clear"></div>
      
      <?=$arResult["FAVORITES"]?>
      
      <div class="mc_block_banner bottom">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "MARKET_LIST",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
   <?if(($arParams['SHOW_SEO_TEXT'] == 'Y') && strlen($arResult["THIS_SECTION"]["DESCRIPTION"])):?>
      <div class="rc_block">
         <div class="rc_block_info rc_block_description">
            <p><?=$arResult["THIS_SECTION"]["DESCRIPTION"]?></p>
         </div>
      </div>
   <?endif;?>
</div>
<div class="content_right_col">
   <div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "market-rightcol", Array(
               "TYPE" => "MARKET_LIST_RIGHT",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
   <div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "market-rightcol", Array(
               "TYPE" => "MARKET_LIST_RIGHT",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
</div>