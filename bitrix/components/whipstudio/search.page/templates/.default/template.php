<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
   die(); ?>

<?$APPLICATION->SetTitle("�������������� - �����.");?>

<? if ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false): ?>
<? elseif ($arResult["ERROR_CODE"] != 0): ?>
   <p><?= GetMessage("SEARCH_ERROR") ?></p>
   <? ShowError($arResult["ERROR_TEXT"]); ?>
   <p><?= GetMessage("SEARCH_CORRECT_AND_CONTINUE") ?></p>
   <br /><br />
<? elseif (count($arResult["SEARCH"]) > 0): ?>
   <ul class="mb_menu" style="margin-top: 15px;">
      <li<?=$arResult["SORT"]["TYPE"]=="rank" ? ' class="sel"' : ''?>><a href="<?=$arResult["SORT"]["RELEV"]?>">�������������</a><div class="sclew"></div></li>
      <li<?=$arResult["SORT"]["TYPE"]=="date" ? ' class="sel"' : ''?>><a href="<?=$arResult["SORT"]["DATE"]?>">����</a><div class="sclew"></div></li>
   </ul>
   <? if ($arParams["DISPLAY_TOP_PAGER"] != "N"):// echo $arResult["NAV_STRING"]; ?>
      <?= $arResult["NAV_STRING"] ?>
   <? endif; ?>
   <div class="search_results">
      <? foreach ($arResult["SEARCH"] as $arItem):?>
         <div class="search_row">
            <? if (is_array($arItem["REAL_ELEMENT"]["PREVIEW_PICTURE"])): ?>
               <a class="image_box" href="<?= $arItem["REAL_ELEMENT"]["DETAIL_PAGE_URL"] ?>">
                  <img src="<?= $arItem["REAL_ELEMENT"]["PREVIEW_PICTURE"]["SRC"] ?>" alt="">
               </a>
            <? endif; ?>
            <div class="row"<?=($arParams["IMAGE_MARGIN"]) ? '' : ' style="margin-left: 110px"'?>>
               <a class="title" href="<?= $arItem["REAL_ELEMENT"]["DETAIL_PAGE_URL"] ?>"><?= $arItem["TITLE_FORMATED"] ?></a>
               <div class="descr">
                  <?if(is_array($arItem["RIBBON"]) && count($arItem["RIBBON"])):?>
                     <div class="inline">
                        <a href="<?=$arItem["RIBBON"]["URL"]?>">
                           <div class="news_section <?=$arItem["RIBBON"]["CLASS"]?>">
                              <p><?=$arItem["RIBBON"]["TITLE"]?></p>
                           </div>
                        </a>
                     </div>
                  <?endif;?>
                  <?= $arItem["BODY_FORMATED"] ?>
                  <?if($arItem["REAL_ELEMENT"]["SHOW_DATE"]):?>
                     <span class="date">\ <?echo $arItem["REAL_ELEMENT"]["DATE_ACTIVE_FROM"]?></span>
                  <?endif;?>
               </div>
            </div>
         </div>
      <? endforeach; ?>
   </div>
   <? if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N"):?>
      <?= $arResult["NAV_STRING"] ?>
   <? endif; ?>
<? else: ?>
   <p><?= GetMessage("SEARCH_NOTHING_TO_FOUND"); ?></p>
<?endif;/*
PrintObject($arResult);

$www = 0;
$rsWww = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 27));
while($rsWww->GetNextElement())
{
   $www++;
}
PrintObject("-------------------------------");
PrintObject($www);*/