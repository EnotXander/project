<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$response = array();
if(count($arResult["SEARCH"]) > 0)
{
   foreach($arResult["SEARCH"] as $arItem)
   {      
      $json = array();
      $json['value'] = iconv("cp1251", "UTF-8", $arItem["BODY_FORMATED"]);

      $json['id'] = $arItem["REAL_ELEMENT"]["ID"];
      $json['name'] = iconv("cp1251", "UTF-8", $arItem["TITLE_FORMATED"]);
      $json['type'] = iconv("cp1251", "UTF-8", $arItem["BODY_FORMATED"]);
      $json['image'] = iconv("cp1251", "UTF-8", $arItem["REAL_ELEMENT"]["PREVIEW_PICTURE"]["SRC"]);
      $json['image_h'] = $arItem["REAL_ELEMENT"]["PREVIEW_PICTURE"]["HEIGHT"];
      $json['link'] = $arItem["REAL_ELEMENT"]["DETAIL_PAGE_URL"];
      $json['ribbon_class'] = $arItem["RIBBON"]["CLASS"];
      $json['ribbon_url'] = $arItem["RIBBON"]["URL"];
      $json['ribbon_title'] = iconv("cp1251", "UTF-8", $arItem["RIBBON"]["TITLE"]);
      if($arItem["REAL_ELEMENT"]["SHOW_DATE"])
      {
          $timestamp = BXToTimestamp($arItem["REAL_ELEMENT"]["DATE_ACTIVE_FROM"]);
          $dateFormat = date('d.m.Y H:i', $timestamp);
         $json['date'] = "\ ".$dateFormat;
      }
      else $json['date'] = "";
      $json['is_section'] = $arItem["REAL_ELEMENT"]["IS_SECTION"];
      if($arItem["REAL_ELEMENT"]["IS_SECTION"])
      {
         $sub = array();
         foreach($arItem["REAL_ELEMENT"]["SUB"] as $subItem)
         {
            $sub[] = array(
                "name" => iconv("cp1251", "UTF-8", $subItem["NAME"]),
                "url" => $subItem["URL"]
            );
         }
         $json['sub'] = $sub;
      }
      if(is_array($arItem["REAL_ELEMENT"]["FIRM"]))
      {
         $arItem["REAL_ELEMENT"]["FIRM"]["LOGO"]["DESCRIPTION"] = html_entity_decode(iconv("cp1251", "UTF-8", $arItem["REAL_ELEMENT"]["FIRM"]["LOGO"]["DESCRIPTION"]));
         $json['firm'] = array(
             "LINK" => $arItem["REAL_ELEMENT"]["FIRM"]["LINK"],
             "LOGO" => $arItem["REAL_ELEMENT"]["FIRM"]["LOGO"]
         );
      }
      $json['image_margin'] = $arParams["IMAGE_MARGIN"];
      $response[] = $json; 
   }
}
header("Content-type: application/json");
echo json_encode($response, JSON_ERROR_UTF8);

return false;
/*
<p>
	 �� ����� ������������ ������ ����������� �� ������� � �����-���� �������� 2015� ��������� ������ ����� ���
</p>
 <a class="border_button" href="/action/paplavi/">��������� ����� ������</a>
*/
?>
<ul class="as-list" style="width: 560px;">
	<li class="as-result-item active" id="as-result-item-0">
		<div class="search_row">
			<a class="image_box" title="����������� ���" style="float: right; margin-top:15px;">
				<img src="/upload/resize_cache/iblock/815/52_45_040cd750bba9870f18aada2478b24840a/logo.jpg">
			</a>
			<a class="image_box" href="/market/svetilniki_i_ikh_komplektuyushchie/svetilnik_npo_1408_dlya_lamp_nakalivaniya/" style="margin-top: 20.5px;">
				<img src="/upload/resize_cache/iblock/c5c/c30/52_45_040cd750bba9870f18aada2478b24840a/1_.jpg">
			</a>

			<div class="row" style="margin-left: 72px;">
				<a class="title" href="/market/svetilniki_i_ikh_komplektuyushchie/svetilnik_npo_1408_dlya_lamp_nakalivaniya/">
					<div class="title_gradient"></div>
					<span class="title_fullwidth"><b>����������</b> ��� <b>1408</b> ��� ���� �����������</span>
				</a>

				<div class="descr"><b>����������</b> ��� <b>1408</b> ��� ���� ����������� <b>�����������</b>
					��� ��� ���� ����������� (����� – <b>�����������</b> ... ������������ �
					�������� ����������������� �������. <b>�����������</b> ��� ������������� ������
					������ I ... ��������������

					������������ ��������� ����������� ���������� <b>�����������</b> ��� ����� ����
					«� ...

					���������� ������� (��)

					����������� ������� <b>�����������</b> ��� ��������������� ���������� � ��������
					... ������������ ������. <b>����������</b> ��� <b>1408</b> ��� ���� �����������,
					���������������� ���������, <b>�����������</b> � �� ...
					<a href="/market/svetilniki_i_ikh_komplektuyushchie/svetilnik_npo_1408_dlya_lamp_nakalivaniya/" style="margin-left: 7px; text-decoration: none !important;">�����...</a>
				</div>
				<div class="inline"><a href="/market/svetilniki_i_ikh_komplektuyushchie/">
						<div class="news_section rib_default"><p>����������� � �� �������������</p>
						</div>
					</a></div>
				<span class="date"></span></div>
		</div>
	</li>
	<li class="as-result-item" id="as-result-item-1">
		<div class="search_row">
			<a class="image_box" title="����������� ���" style="float: right; margin-top:15px;"><img src="/upload/resize_cache/iblock/815/52_45_040cd750bba9870f18aada2478b24840a/logo.jpg"></a><a class="image_box" href="/market/nizkovoltnaya_apparatura_nva/svetilniki_npo/" style="margin-top: 15px;"><img src="/upload/resize_cache/iblock/d29/52_45_040cd750bba9870f18aada2478b24840a/npo.jpg"></a>

			<div class="row" style="margin-left: 72px;">
				<a class="title" href="/market/nizkovoltnaya_apparatura_nva/svetilniki_npo/">
					<div class="title_gradient"></div>
					<span class="title_fullwidth"><b>�����������</b> ���</span></a>

				<div class="descr">... .

					<b>����������</b> ��� 1409 ��� ���� �����������


					<b>����������</b> ��� <b>1408</b> ��� ���� �����������


					<b>����������</b> ��� 1407-100 ��� ���� �����������


					<b>����������</b>
					...<a href="/market/nizkovoltnaya_apparatura_nva/svetilniki_npo/" style="margin-left: 7px; text-decoration: none !important;">�����...</a>
				</div>
				<div class="inline"><a href="/market/nizkovoltnaya_apparatura_nva/">
						<div class="news_section rib_default"><p>������������� ���������� (���)</p>
						</div>
					</a></div>
				<span class="date"></span></div>
		</div>
	</li>
</ul>