<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//$objMultilevel = new multilevel($arParams);

if(!CModule::IncludeModule("search"))
{
	ShowError(GetMessage("SEARCH_MODULE_UNAVAILABLE"));
	return;
}
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if($arParams["BODY_LENGTH"]<=0)
   $arParams["BODY_LENGTH"] = 250;
$GLOBALS["BODY_LENGTH"] = $arParams["BODY_LENGTH"];
class MySearch extends CSearch
{
   function PrepareSearchResult($str)
    {
        $obParser = new CTextParser;
        $globalBodyLength = $GLOBALS["BODY_LENGTH"];
        $w = array();
        foreach($this->Query->m_words as $k=>$v)
        {
            $v = ToUpper($v);
            $w[$v] = "KAV";
            if(strpos($v, "\"")!==false)
                $w[str_replace("\"", "&QUOT;", $v)] = "KAV";
        }

        foreach($this->Query->m_stemmed_words as $k=>$v)
            $w[ToUpper($v)]="STEM";

        if($this->Query->bStemming)
        {
            $arStemInfo = stemming_init($this->Query->m_lang);
            $letters = $arStemInfo["letters"];
            $strUpp = stemming_upper($str, $this->Query->m_lang);
        }
        else
        {
            $strUpp = ToUpper($str);
        }

        $arPos = Array();
        foreach($w as $search=>$type)
        {
            $p = strpos($strUpp, $search);

            while($p!==false)
            {
                //Check if we are in the middle of the numeric entity
                if(
                    preg_match("/^[0-9]+;/", substr($str, $p)) &&
                    preg_match("/^[0-9]+#&/", strrev(substr($str, 0, $p+strlen($search))))
                )
                {
                    $p = strpos($strUpp, $search, $p+1);
                }
                elseif($type=="STEM")
                {
                    $l = strlen($strUpp);
                    for($s=$p; $s>=0 && strpos($letters, substr($strUpp, $s, 1))!==false;$s--){}
                    $s++;
                    for($e=$p; $e<$l && strpos($letters, substr($strUpp, $e, 1))!==false;$e++){}
                    $e--;
                    $a = array_keys(stemming(substr($strUpp,$s,$e-$s+1), $this->Query->m_lang));

                    foreach($a as $stem)
                    {
                        if($stem == $search)
                        {
                            $arPos[] = $p;
                            $p = false;
                            break;
                        }
                    }
                    if($p !== false)
                        $p = strpos($strUpp, $search, $p+1);
                }
                else
                {
                    $arPos[] = $p;
                    $p=false;
                }
            }
        }

        if(count($arPos)<=0)
        {
            $str_len = strlen($str);
            $pos_end = 500;
            while(($pos_end < $str_len) && (strpos(" ,.\n\r", substr($str, $pos_end, 1)) === false))
                $pos_end++;
            return substr($str, 0, $pos_end).($pos_end < $str_len? "...": "");
        }

        sort($arPos);

        $str_result = "";
        $last_pos = -1;
        $delta = ($globalBodyLength/1)/count($arPos);
        $str_len = strlen($str);
        foreach($arPos as $pos_mid)
        {
            //Find where word begins
            $pos_beg = $pos_mid - $delta;
            if($pos_beg <= 0)
                $pos_beg = 0;
            while(($pos_beg > 0) && (strpos(" ,.\n\r", substr($str, $pos_beg, 1)) === false))
                $pos_beg--;

            //Find where word ends
            $pos_end = $pos_mid + $delta;
            if($pos_end > $str_len)
                $pos_end = $str_len;
            while(($pos_end < $str_len) && (strpos(" ,.\n\r", substr($str, $pos_end, 1)) === false))
                $pos_end++;

            if($pos_beg <= $last_pos)
                $arOtr[count($arOtr)-1][1] = $pos_end;
            else
                $arOtr[] = Array($pos_beg, $pos_end);

            $last_pos = $pos_end;
        }

        for($i=0; $i<count($arOtr); $i++)
        {
            $substr = html_entity_decode(substr($str, $arOtr[$i][0]));
            $substr = $obParser->html_cut($substr, $arOtr[$i][1]-$arOtr[$i][0]);
            $str_result .= ($arOtr[$i][0]<=0?"":" ...").
                    $substr.
                    ($arOtr[$i][1]>=strlen($str)?"":"... ");
        }

        foreach($w as $search=>$type)
            $str_result=$this->repl($search, $type, $str_result);

        $str_result = str_replace("%/^%", "</b>", str_replace("%^%","<b>", $str_result));
        if(strlen($str_result) > round($globalBodyLength*1.4))
        {
           $str_result = $obParser->html_cut($str_result, round($globalBodyLength*1.4))."...";
        }
        return $str_result;
    }
}

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["SHOW_WHEN"] = $arParams["SHOW_WHEN"]=="Y";
$arParams["SHOW_WHERE"] = $arParams["SHOW_WHERE"]!="N";
if(!is_array($arParams["arrWHERE"]))
	$arParams["arrWHERE"] = array();
$arParams["PAGE_RESULT_COUNT"] = intval($arParams["PAGE_RESULT_COUNT"]);
if($arParams["PAGE_RESULT_COUNT"]<=0)
	$arParams["PAGE_RESULT_COUNT"] = 50;

$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
if(strlen($arParams["PAGER_TITLE"]) <= 0)
	$arParams["PAGER_TITLE"] = GetMessage("SEARCH_RESULTS");
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["USE_TITLE_RANK"] = $arParams["USE_TITLE_RANK"]=="Y";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);

if($arParams["DEFAULT_SORT"] !== "date")
	$arParams["DEFAULT_SORT"] = "rank";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
	$arFILTERCustom = array();
else
{
	$arFILTERCustom = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arFILTERCustom))
		$arFILTERCustom = array();
}

$exFILTER = CSearchParameters::ConvertParamsToFilter($arParams, "arrFILTER");

$arParams["CHECK_DATES"]=$arParams["CHECK_DATES"]=="Y";

//options
if(isset($_REQUEST["tags"]))
	$tags = trim($_REQUEST["tags"]);
else
	$tags = false;
if(isset($_REQUEST["q"]))
	$q = trim($_REQUEST["q"]);
else
	$q = false;

if(
	$arParams["SHOW_WHEN"]
	&& isset($_REQUEST["from"])
	&& is_string($_REQUEST["from"])
	&& strlen($_REQUEST["from"])
	&& CheckDateTime($_REQUEST["from"])
)
	$from = $_REQUEST["from"];
else
	$from = "";

if(
	$arParams["SHOW_WHEN"]
	&& isset($_REQUEST["to"])
	&& is_string($_REQUEST["to"])
	&& strlen($_REQUEST["to"])
	&& CheckDateTime($_REQUEST["to"])
)
	$to = $_REQUEST["to"];
else
	$to = "";

$where = $arParams["SHOW_WHERE"]? trim($_REQUEST["where"]): "";

$how = trim($_REQUEST["how"]);
if($how == "d")
	$how = "d";
elseif($how == "r")
	$how = "";
elseif($arParams["DEFAULT_SORT"] == "date")
	$how = "d";
else
	$how = "";

if($arParams["USE_TITLE_RANK"])
{
	if($how=="d")
		$aSort=array("DATE_CHANGE"=>"DESC", "CUSTOM_RANK"=>"DESC", "TITLE_RANK"=>"DESC", "RANK"=>"DESC");
	else
		$aSort=array("CUSTOM_RANK"=>"DESC", "TITLE_RANK"=>"DESC", "RANK"=>"DESC"/*, "DATE_CHANGE"=>"DESC"*/);
}
else
{
	if($how=="d")
		$aSort=array("DATE_CHANGE"=>"DESC", "CUSTOM_RANK"=>"DESC", "RANK"=>"DESC");
	else
		$aSort=array("CUSTOM_RANK"=>"DESC", "RANK"=>"DESC"/*, "DATE_CHANGE"=>"DESC"*/);
}
/*************************************************************************
			Operations with cache
*************************************************************************/
$arrDropdown = array();

$obCache = new CPHPCache;

if(
	$arParams["CACHE_TYPE"] == "N"
	|| (
		$arParams["CACHE_TYPE"] == "A"
		&& COption::GetOptionString("main", "component_cache_on", "Y") == "N"
	)
)
	$arParams["CACHE_TIME"] = 0;

if($obCache->StartDataCache($arParams["CACHE_TIME"], $this->GetCacheID(), "/".SITE_ID.$this->GetRelativePath()))
{
	// Getting of the Information block types
	$arIBlockTypes = array();
	if(!CModule::IncludeModule("iblock")) die();
        if(!CModule::IncludeModule("blog")) die();
        if(!CModule::IncludeModule("forum")) die();
        
        $rs = CIBlock::GetByID(17);
        if($iblock = $rs->GetNext())
        {
            $arIBlockTypes[$iblock["ID"]] = $iblock;
        }

	// Creating of an array for drop-down list
	foreach($arParams["arrWHERE"] as $code)
	{
            list($module_id, $part_id) = explode("_", $code, 2);
            if(strlen($module_id)>0)
            {
                if(strlen($part_id)<=0)
                {
                    switch($module_id)
                    {
                        case "forum":           $arrDropdown[$code] = GetMessage("SEARCH_FORUM");
                                                break;
                                            
                        case "blog":            $arrDropdown[$code] = GetMessage("SEARCH_BLOG");
                                                break;
                                            
                        case "socialnetwork":   $arrDropdown[$code] = GetMessage("SEARCH_SOCIALNETWORK");
                                                break;
                                            
                        case "intranet":        $arrDropdown[$code] = GetMessage("SEARCH_INTRANET");
                                                break;
                                            
                        case "sections":        $objSection = new CIBlockSection();
                                                $rs = $objSection->GetList(
                                                        array("SORT" => "ASC"),
                                                        array("IBLOCK_ID" => 11, "DEPTH_LEVEL" => 1, "GLOBAL_ACTIVE" => "Y")
                                                );
                                                while($section = $rs->GetNext())
                                                {
                                                    $arrDropdown['sec-'.$section["ID"]] = $section["NAME"];
                                                }
                                                
                                                break;
                    }
                }
                else
                {
                        // if there is additional information specified besides ID then
                        switch($module_id)
                        {
                            case "iblock":  if(isset($arIBlockTypes[$part_id]))
                                                $arrDropdown[$code] = $arIBlockTypes[$part_id]["NAME"];
                                            break;
                        }
                }
            }
	}
	$obCache->EndDataCache($arrDropdown);
}
else
{
	$arrDropdown = $obCache->GetVars();
}

$arResult["DROPDOWN"] = htmlspecialcharsex($arrDropdown);
$arResult["REQUEST"]["HOW"] = htmlspecialchars($how);
$arResult["REQUEST"]["~FROM"] = $from;
$arResult["REQUEST"]["FROM"] = htmlspecialchars($from);
$arResult["REQUEST"]["~TO"] = $to;
$arResult["REQUEST"]["TO"] = htmlspecialchars($to);

if($q!==false)
{
    if($arParams["USE_LANGUAGE_GUESS"] == "N" || isset($_REQUEST["spell"]))
    {
        $arResult["REQUEST"]["~QUERY"] = $q;
        $arResult["REQUEST"]["QUERY"] = htmlspecialcharsex($q);
    }else
    {
        $arLang = CSearchLanguage::GuessLanguage($q);
        if(is_array($arLang) && $arLang["from"] != $arLang["to"])
        {
            $arResult["REQUEST"]["~ORIGINAL_QUERY"] = $q;
            $arResult["REQUEST"]["ORIGINAL_QUERY"] = htmlspecialcharsex($q);
            $arResult["REQUEST"]["~QUERY"] = CSearchLanguage::ConvertKeyboardLayout($arResult["REQUEST"]["~ORIGINAL_QUERY"], $arLang["from"], $arLang["to"]);
            $arResult["REQUEST"]["QUERY"] = htmlspecialcharsex($arResult["REQUEST"]["~QUERY"]);
        } else
        {
            $arResult["REQUEST"]["~QUERY"] = $q;
            $arResult["REQUEST"]["QUERY"] = htmlspecialcharsex($q);
        }
    }
} else
{
    $arResult["REQUEST"]["~QUERY"] = false;
    $arResult["REQUEST"]["QUERY"] = false;
}

if($tags!==false)
{
    $arResult["REQUEST"]["~TAGS_ARRAY"] = array();
    $arTags = explode(",", $tags);
    foreach($arTags as $tag)
    {
        $tag = trim($tag);
        if(strlen($tag) > 0)
            $arResult["REQUEST"]["~TAGS_ARRAY"][$tag] = $tag;
    }
    $arResult["REQUEST"]["TAGS_ARRAY"] = htmlspecialcharsex($arResult["REQUEST"]["~TAGS_ARRAY"]);
    $arResult["REQUEST"]["~TAGS"] = implode(",", $arResult["REQUEST"]["~TAGS_ARRAY"]);
    $arResult["REQUEST"]["TAGS"] = htmlspecialcharsex($arResult["REQUEST"]["~TAGS"]);
} else
{
    $arResult["REQUEST"]["~TAGS_ARRAY"] = array();
    $arResult["REQUEST"]["TAGS_ARRAY"] = array();
    $arResult["REQUEST"]["~TAGS"] = false;
    $arResult["REQUEST"]["TAGS"] = false;
}

$arResult["REQUEST"]["WHERE"] = htmlspecialchars($where);

$arResult["URL"] = $APPLICATION->GetCurPage()
	."?q=".urlencode($q)
	.(isset($_REQUEST["spell"])? "&amp;spell=1": "")
	."&amp;where=".urlencode($where)
	.($tags!==false? "&amp;tags=".urlencode($tags): "");

if(isset($arResult["REQUEST"]["~ORIGINAL_QUERY"]))
{
    $arResult["ORIGINAL_QUERY_URL"] = $APPLICATION->GetCurPage()
            ."?q=".urlencode($arResult["REQUEST"]["~ORIGINAL_QUERY"])
            ."&amp;spell=1"
            ."&amp;where=".urlencode($arResult["REQUEST"]["WHERE"])
            .($arResult["REQUEST"]["HOW"]=="d"? "&amp;how=d": "")
            .($arResult["REQUEST"]["FROM"]? '&amp;from='.urlencode($arResult["REQUEST"]["~FROM"]): "")
            .($arResult["REQUEST"]["TO"]? '&amp;to='.urlencode($arResult["REQUEST"]["~TO"]): "")
            .($tags!==false? "&amp;tags=".urlencode($tags): "");
}
//������
$arResult["TARIFS"] = GetTarifs();


if($this->InitComponentTemplate($templatePage))
{
	$template = &$this->GetTemplate();
	$arResult["FOLDER_PATH"] = $folderPath = $template->GetFolder();

	if(strlen($folderPath) > 0)
	{
		$arFilter = array(
			"SITE_ID" => SITE_ID,
			"QUERY" => $arResult["REQUEST"]["~QUERY"],
			"TAGS" => $arResult["REQUEST"]["~TAGS"],
		);
		$arFilter = array_merge($arFILTERCustom, $arFilter);
		if(strlen($where) > 0)
		{
                    list($module_id, $part_id) = explode("_",$where,2);
                    $arFilter["MODULE_ID"] = $module_id;
                    if(strlen($part_id)>0) 
                        $arFilter["PARAM2"] = $part_id;
                    
                    list($module_id, $part_id) = explode("-",$where,2);
                    if($module_id == 'sec')
                    {
                        $arFilter["MODULE_ID"] = 'iblock';
                        $arFilter["PARAM2"] = 11;
                        
                        $part_id = (int)$part_id;
                        if($part_id > 0)
                        {
                            $objElement = new CIBlockElement();
                            
                            $rs = $objElement->GetList(
                                    array("SORT" => "ASC"), 
                                    array(
                                        "IBLOCK_ID" => 11, 
                                        "SECTION_ID" => $part_id, 
                                        "INCLUDE_SUBSECTIONS" => "Y",
                                        "ACTIVE" => "Y"
                                    )
                            );
                            $elements = array();
                            while($el = $rs->GetNext())
                            {
                                $elements[] = $el["ID"];
                            }
                            $exFILTER[] = array("ITEM_ID" => $elements);
                        }                            
                    }                        
		} else {
            $arFilter["!MODULE_ID"] = 'main';
        }
                
		if($arParams["CHECK_DATES"])
			$arFilter["CHECK_DATES"]="Y";
		if($from)
			$arFilter[">=DATE_CHANGE"] = $from;
		if($to)
			$arFilter["<=DATE_CHANGE"] = $to;

		$obSearch = new MySearch();

		//When restart option is set we will ignore error on query with only stop words
		$obSearch->SetOptions(array(
			"ERROR_ON_EMPTY_STEM" => $arParams["RESTART"] != "Y",
			"NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"] == "Y",
		));
 
            if(preg_match("#^(\S{2,}) (\S{1,1}) (\S{2,})$#", $arFilter["QUERY"], $match))
            {
               $arFilter["QUERY"] = "\"{$match[1]} {$match[2]} {$match[3]}\"";
            } else if(preg_match("#^(\S{2,}) (\S{1,1})$#", $arFilter["QUERY"], $match))
            {
               $arFilter["QUERY"] = "\"{$match[1]} {$match[2]}\"";
            } else if(preg_match("#^(\S{1,1}) (\S{2,})$#", $arFilter["QUERY"], $match))
            {
               $arFilter["QUERY"] = "\"{$match[1]} {$match[2]}\"";
            }
            //$arFilter["QUERY"] = "\"������������ �\"";
            //PrintObject($arFilter);
            //PrintObject($exFILTER);
                
		$obSearch->Search($arFilter, $aSort, $exFILTER);

		$arResult["ERROR_CODE"] = $obSearch->errorno;
		$arResult["ERROR_TEXT"] = $obSearch->error;

		$arResult["SEARCH"] = array();
		if($obSearch->errorno==0)
		{
                    $obSearch->NavStart($arParams["PAGE_RESULT_COUNT"], false);
                    $ar = $obSearch->GetNext();
                    //Search restart
                    if(!$ar && ($arParams["RESTART"] == "Y") && $obSearch->Query->bStemming)
                    {
                            $exFILTER["STEMMING"] = false;
                            $obSearch = new MySearch();
                            $obSearch->Search($arFilter, $aSort, $exFILTER);

                            $arResult["ERROR_CODE"] = $obSearch->errorno;
                            $arResult["ERROR_TEXT"] = $obSearch->error;

                            if($obSearch->errorno == 0)
                            {
                                    $obSearch->NavStart($arParams["PAGE_RESULT_COUNT"], false);
                                    $ar = $obSearch->GetNext();
                            }
                    }

                    $objElement = new CIBlockElement();
                    $objSection = new CIBlockSection();
                    while($ar)
                    {
                        $ar["CHAIN_PATH"] = $APPLICATION->GetNavChain($ar["URL"], 0, $folderPath."/chain_template.php", true, false);
                        $ar["URL"] = htmlspecialchars($ar["URL"]);
                        $ar["TAGS"] = array();
                        if (!empty($ar["~TAGS_FORMATED"]))
                        {
                            foreach ($ar["~TAGS_FORMATED"] as $name => $tag)
                            {
                                if($arParams["TAGS_INHERIT"] == "Y")
                                {
                                    $arTags = $arResult["REQUEST"]["~TAGS_ARRAY"];
                                    $arTags[$tag] = $tag;
                                    $tags = implode("," , $arTags);
                                } else
                                    $tags = $tag;

                                $ar["TAGS"][] = array(
                                    "URL" => $APPLICATION->GetCurPageParam("tags=".urlencode($tags), array("tags")),
                                    "TAG_NAME" => htmlspecialcharsex($name),
                                );
                            }
                        }
                        
                        //��������
                        if(strlen($ar["ITEM_ID"]) > 0 && $ar["MODULE_ID"] == 'iblock')
                        {
                           if(substr($ar["ITEM_ID"], 0, 1) != "S")//�������
                           {
                               $resElement = $objElement->GetByID($ar["ITEM_ID"]);
                               if($obj = $resElement->GetNextElement())
                               {
                                    $element = $obj->GetFields();
                                    $element["IS_SECTION"] = false;
                                    //$element["DETAIL_PAGE_URL"] = $objMultilevel->get_element_url($element);

                                    $element["PROPERTIES"] = $obj->GetProperties();
                                    
                                    //�������� �������� ����
                                    $element["SEARCH_SORT"] = 0;
                                    if($element["IBLOCK_ID"] == 17)//��������
                                    {
                                       $tarifId = $element["PROPERTIES"]["TARIF"]["VALUE"];
                                    }
                                    elseif($element["IBLOCK_ID"] == 27)//������
                                    {
                                       $resTarif = $objElement->GetProperty(17, $element["PROPERTIES"]["FIRM"]["VALUE"], array(), array("CODE" => "TARIF"));
                                       if($arTarif = $resTarif->GetNext())
                                          $tarifId = $arTarif["VALUE"];
                                    }
                                    if($tarifId)
                                       $element["TARIF_SORT"] = $arResult["TARIFS"]["TARIFS"][$tarifId]["PROP"]["SEARCH_SORT"]["VALUE"];

                                    /*if($element["PROPERTIES"]["FIRM"]["VALUE"])
                                    {
                                       $resManager = $objElement->GetProperty(17, $element["PROPERTIES"]["FIRM"]["VALUE"], array(), array("CODE" => "MANAGER"));
                                       if($arManager = $resManager->GetNext())
                                       {
                                          $element["CREATOR_FIRM"] = $element["PROPERTIES"]["FIRM"]["VALUE"];
                                          $element["CREATOR_MANAGER"] = $arManager["VALUE"];
                                          $element["CREATOR_GROUP"] = $USER->GetUserGroupArray($arManager["VALUE"]);
                                          
                                          $arTarif = array(19, 18, 23, 17, 16, 15);
                                          $tarifIndex = 100;//�� ����������� �� ������ ��������� �����
                                          foreach ($element["CREATOR_GROUP"] as $tarif){
                                             $newTarifIndex = array_search($tarif, $arTarif);
                                             if(($newTarifIndex !== false) && ($newTarifIndex < $tarifIndex))
                                                $tarifIndex = $newTarifIndex;
                                          }
                                          $element["CREATOR_TARIF"] = $tarifIndex;
                                          
                                       }
                                    }*/

                                    $element["IMAGES"] = array();
                                    if($element["PREVIEW_PICTURE"])
                                    {
                                       $FILE = CFile::GetFileArray($element["PREVIEW_PICTURE"]);
                                       if (is_array($FILE))
                                       {                  
                                          $arFilterImg = array();
                                          $arFileTmp = CFile::ResizeImageGet(
                                              $FILE,
                                              array("width" => $arParams["THUMB_WIDTH"], "height" => $arParams["THUMB_HEIGHT"]),
                                              BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                                              true, 
                                              $arFilterImg
                                          );

                                          $FILE = array(
                                              'SRC' => $arFileTmp["src"],
                                              'WIDTH' => $arFileTmp["width"],
                                              'HEIGHT' => $arFileTmp["height"],
                                              'DESCRIPTION' => strlen($FILE["DESCRIPTION"]) > 0 ? $FILE : $element["NAME"]
                                          );

                                          $element["PREVIEW_PICTURE"] = $FILE;
                                       }
                                    }
                                    else
                                    {
                                       
                                       //$FILE = CFile::MakeFileArray("/images/search_placeholder.png");
                                       $FILE = array(
                                              'SRC' => "/images/search_placeholder.png",
                                              'WIDTH' => 48,
                                              'HEIGHT' => 42,
                                              'DESCRIPTION' => $element["NAME"]
                                          );
                                       $element["PREVIEW_PICTURE"] = $FILE;
                                    }

                                    $rsNav = $objSection->GetNavChain($element["IBLOCK_ID"], $element["IBLOCK_SECTION_ID"]);
                                    while($navSec = $rsNav->GetNext())
                                    {
                                       //$navSec["SECTION_PAGE_URL"] = $objMultilevel->get_section_url_by_id($navSec["ID"]);
                                       $element["NAV"][] = $navSec;
                                    }
                                    //������� ��� ������ �� ����������������� ����������
                                    if($element["IBLOCK_ID"] == 39) $element["DETAIL_PAGE_URL"] = "/terminology/".$element["IBLOCK_SECTION_ID"]."/".$element["ID"]."/";
                                       
                               }

                               if(array_search($element["IBLOCK_ID"], array(3,41,36,2)) !== false)
                                   $element["SHOW_DATE"] = true;
                           }
                           else//������
                           {
                               $resSection = $objSection->GetByID(substr($ar["ITEM_ID"], 1));
                               if($element = $resSection->GetNext())
                               {
                                  $element["DETAIL_PAGE_URL"] = $element["SECTION_PAGE_URL"];
                                  $element["IS_SECTION"] = true;
                                  //picture
                                  if($element["PICTURE"])
                                  {
                                    $FILE = CFile::GetFileArray($element["PICTURE"]);
                                    if (is_array($FILE))
                                    {                  
                                       $arFilterImg = array();
                                       $arFileTmp = CFile::ResizeImageGet(
                                           $FILE,
                                           array("width" => $arParams["THUMB_WIDTH"], "height" => $arParams["THUMB_HEIGHT"]),
                                           BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                                           true, 
                                           $arFilterImg
                                       );

                                       $FILE = array(
                                           'SRC' => $arFileTmp["src"],
                                           'WIDTH' => $arFileTmp["width"],
                                           'HEIGHT' => $arFileTmp["height"],
                                           'DESCRIPTION' => strlen($FILE["DESCRIPTION"]) > 0 ? $FILE : $element["NAME"]
                                       );

                                       $element["PREVIEW_PICTURE"] = $FILE;
                                    }
                                  }
                                  //����� �����������
                                  $element["SUB"] = array();
                                  $resSubSection = $objSection->GetList(
                                    array("NAME" => "ASC"),
                                    array("IBLOCK_ID" => $element["IBLOCK_ID"], "DEPTH_LEVEL" => $element["DEPTH_LEVEL"]+1, "SECTION_ID" => $element["ID"], "ACTIVE" => "Y"),
                                          false,
                                          false,
                                          array("nPageSize" => 4)
                                  );
                                  while($arSubSection = $resSubSection->GetNext())
                                  {
                                     $arSubSection["URL"] = $arSubSection["SECTION_PAGE_URL"];
                                     $element["SUB"][] = $arSubSection;
                                  }
                                  
                                  //����� ���������
                                  if(!count($element["SUB"]))
                                  {
                                     $resSubElement = $objElement->GetList(
                                          array("SORT" => "ASC"), 
                                          array("IBLOCK_ID" => $element["IBLOCK_ID"], "SECTION_ID" => $element["ID"], "ACTIVE" => "Y"),
                                          false,
                                          array("nPageSize" => 4)
                                     );
                                     while($arSubElement = $resSubElement->GetNext())
                                     {
                                        $arSubElement["URL"] = $arSubElement["DETAIL_PAGE_URL"];
                                        $element["SUB"][] = $arSubElement;
                                     }
                                  }
                               }
                               
                           }
                           if(!isset($element["TARIF_SORT"])) $element["TARIF_SORT"] = 0;
                            $ar["REAL_ELEMENT"] = $element;
                        }
                        //����
                        if(strlen($ar["ITEM_ID"]) > 0 && $ar["MODULE_ID"] == 'blog')
                        {
                           $ar["REAL_ID"] = substr($ar["ITEM_ID"], 1);
                           $element = CBlog::GetByID($ar["REAL_ID"] );
                           if(is_array($element) && count($element))
                           {
                              $element["DETAIL_PAGE_URL"] = "/blogs/".$element["URL"]."/";
                           }
                           if(!isset($element["TARIF_SORT"])) $element["TARIF_SORT"] = 100;
                           $ar["REAL_ELEMENT"] = $element;
                        }
                        //�����
                        if(strlen($ar["ITEM_ID"]) > 0 && $ar["MODULE_ID"] == 'forum')
                        {
                           $ar["REAL_ELEMENT"]["DETAIL_PAGE_URL"] = $ar["URL"];
                        }
                        
                        if(!isset($ar["REAL_ELEMENT"]["PREVIEW_PICTURE"]))
                        {
                           $FILE = array(
                                 'SRC' => "/images/search_placeholder.png",
                                 'WIDTH' => $arParams["THUMB_WIDTH"],
                                 'HEIGHT' => $arParams["THUMB_HEIGHT"],
                                 'DESCRIPTION' => $element["NAME"]
                             );
                           $ar["REAL_ELEMENT"]["PREVIEW_PICTURE"] = $FILE;
                        }
                        if(!strlen($ar["BODY_FORMATED"]))
                           $ar["BODY_FORMATED"] = "&nbsp;";

                        $arResult["SEARCH"][] = $ar;
                        $ar = $obSearch->GetNext();
                    }
                                        
                    $arResult["NAV_STRING"] = $obSearch->GetPageNavStringEx($navComponentObject,  $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
                    $arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
                    $arResult["NAV_RESULT"] = $obSearch;
		}
                

		$arResult["TAGS_CHAIN"] = array();
		$url = array();
		foreach ($arResult["REQUEST"]["~TAGS_ARRAY"] as $key => $tag)
		{
			$url_without = $arResult["REQUEST"]["~TAGS_ARRAY"];
			unset($url_without[$key]);
			$url[$tag] = $tag;
			$result = array(
				"TAG_NAME" => $tag,
				"TAG_PATH" => $APPLICATION->GetCurPageParam("tags=".urlencode(implode(",", $url)), array("tags")),
				"TAG_WITHOUT" => $APPLICATION->GetCurPageParam("tags=".urlencode(implode(",", $url_without)), array("tags")),
			);
			$arResult["TAGS_CHAIN"][] = $result;
		}
                    
                //$APPLICATION->AddChainItem("Поиск");
		$this->ShowComponentTemplate();
	}
}
else
{
    $this->__ShowError(str_replace("#PAGE#", $templatePage, str_replace("#NAME#", $this->__templateName, "Can not find '#NAME#' template with page '#PAGE#'")));
}