<?
if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
}
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentName */
/** @var string $componentPath */
/** @var string $componentTemplate */
/** @var string $parentComponentName */
/** @var string $parentComponentPath */
/** @var string $parentComponentTemplate */
$this->setFrameMode( false );

if ( ! CModule::IncludeModule( "iblock" ) ) {}
if ( ! CModule::IncludeModule( "search" ) ) {
	ShowError( GetMessage( "SEARCH_MODULE_UNAVAILABLE" ) );

	return;
}
CPageOption::SetOptionString( "main", "nav_page_in_session", "N" );

if ( ! isset( $arParams["CACHE_TIME"] ) ) {
	$arParams["CACHE_TIME"] = 3600;
}

// activation rating
CRatingsComponentsMain::GetShowRating( $arParams );

$arParams["SHOW_WHEN"]  = $arParams["SHOW_WHEN"] == "Y";
$arParams["SHOW_WHERE"] = $arParams["SHOW_WHERE"] != "N";
if ( ! is_array( $arParams["arrWHERE"] ) ) {
	$arParams["arrWHERE"] = array();
}
$arParams["PAGE_RESULT_COUNT"] = intval( $arParams["PAGE_RESULT_COUNT"] );
if ( $arParams["PAGE_RESULT_COUNT"] <= 0 ) {
	$arParams["PAGE_RESULT_COUNT"] = 60;
}

$arParams["PAGER_TITLE"] = trim( $arParams["PAGER_TITLE"] );
if ( strlen( $arParams["PAGER_TITLE"] ) <= 0 ) {
	$arParams["PAGER_TITLE"] = GetMessage( "SEARCH_RESULTS" );
}
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] != "N";
$arParams["USE_TITLE_RANK"]    = $arParams["USE_TITLE_RANK"] == "Y";
$arParams["PAGER_TEMPLATE"]    = trim( $arParams["PAGER_TEMPLATE"] );

if ( $arParams["DEFAULT_SORT"] !== "date" ) {
	$arParams["DEFAULT_SORT"] = "rank";
}

if ( strlen( $arParams["FILTER_NAME"] ) <= 0 || ! preg_match( "/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"] ) ) {
	$arFILTERCustom = array();
} else {
	$arFILTERCustom = $GLOBALS[ $arParams["FILTER_NAME"] ];
	if ( ! is_array( $arFILTERCustom ) ) {
		$arFILTERCustom = array();
	}
}
$exFILTER = CSearchParameters::ConvertParamsToFilter( $arParams, "arrFILTER" );

$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"] == "Y";

//options
if ( isset( $_REQUEST["tags"] ) ) {
	$tags = trim( $_REQUEST["tags"] );
} else {
	$tags = false;
}
if ( isset( $_REQUEST["q"] ) ) {
	$q = trim( $_REQUEST["q"] );
} else {
	$q = false;
}

if (
	$arParams["SHOW_WHEN"]
	&& isset( $_REQUEST["from"] )
	&& is_string( $_REQUEST["from"] )
	&& strlen( $_REQUEST["from"] )
	&& CheckDateTime( $_REQUEST["from"] )
) {
	$from = $_REQUEST["from"];
} else {
	$from = "";
}

if (
	$arParams["SHOW_WHEN"]
	&& isset( $_REQUEST["to"] )
	&& is_string( $_REQUEST["to"] )
	&& strlen( $_REQUEST["to"] )
	&& CheckDateTime( $_REQUEST["to"] )
) {
	$to = $_REQUEST["to"];
} else {
	$to = "";
}

$where = $arParams["SHOW_WHERE"] ? trim( $_REQUEST["where"] ) : "";

$how = trim( $_REQUEST["how"] );
if ( $how == "d" ) {
	$how = "d";
} elseif ( $how == "r" ) {
	$how = "";
} elseif ( $arParams["DEFAULT_SORT"] == "date" ) {
	$how = "d";
} else {
	$how = "";
}


$aSort = array( "CUSTOM_RANK" => "ASC", "RANK" => "DESC", "DATE_CHANGE" => "DESC" );

/*************************************************************************
 * Operations with cache
 *************************************************************************/
$arrDropdown = array();

$obCache = new CPHPCache;

if (
	$arParams["CACHE_TYPE"] == "N"
	|| (
		$arParams["CACHE_TYPE"] == "A"
		&& COption::GetOptionString( "main", "component_cache_on", "Y" ) == "N"
	)
) {
	$arParams["CACHE_TIME"] = 0;
}

if ( $obCache->StartDataCache( $arParams["CACHE_TIME"], $this->GetCacheID(), "/" . SITE_ID . $this->GetRelativePath() ) ) {
	// Getting of the Information block types
	$arIBlockTypes = array();
	if ( CModule::IncludeModule( "iblock" ) ) {
		$rsIBlockType = CIBlockType::GetList( array( "sort" => "asc" ), array( "ACTIVE" => "Y" ) );
		while ( $arIBlockType = $rsIBlockType->Fetch() ) {
			if ( $ar = CIBlockType::GetByIDLang( $arIBlockType["ID"], LANGUAGE_ID ) ) {
				$arIBlockTypes[ $arIBlockType["ID"] ] = $ar["~NAME"];
			}
		}
	}

	// Creating of an array for drop-down list
	foreach ( $arParams["arrWHERE"] as $code ) {
		list( $module_id, $part_id ) = explode( "_", $code, 2 );
		if ( strlen( $module_id ) > 0 ) {
			if ( strlen( $part_id ) <= 0 ) {
				switch ( $module_id ) {
					case "forum":
						$arrDropdown[ $code ] = GetMessage( "SEARCH_FORUM" );
						break;
					case "blog":
						$arrDropdown[ $code ] = GetMessage( "SEARCH_BLOG" );
						break;
					case "socialnetwork":
						$arrDropdown[ $code ] = GetMessage( "SEARCH_SOCIALNETWORK" );
						break;
					case "intranet":
						$arrDropdown[ $code ] = GetMessage( "SEARCH_INTRANET" );
						break;
					case "crm":
						$arrDropdown[ $code ] = GetMessage( "SEARCH_CRM" );
						break;
				}
			} else {
				// if there is additional information specified besides ID then
				switch ( $module_id ) {
					case "iblock":
						if ( isset( $arIBlockTypes[ $part_id ] ) ) {
							$arrDropdown[ $code ] = $arIBlockTypes[ $part_id ];
						}
						break;
				}
			}
		}
	}
	$obCache->EndDataCache( $arrDropdown );
} else {
	$arrDropdown = $obCache->GetVars();
}

$arResult["DROPDOWN"]         = htmlspecialcharsex( $arrDropdown );
$arResult["REQUEST"]["HOW"]   = htmlspecialcharsbx( $how );
$arResult["REQUEST"]["~FROM"] = $from;
$arResult["REQUEST"]["FROM"]  = htmlspecialcharsbx( $from );
$arResult["REQUEST"]["~TO"]   = $to;
$arResult["REQUEST"]["TO"]    = htmlspecialcharsbx( $to );

if ( $q !== false ) {
	if ( $arParams["USE_LANGUAGE_GUESS"] == "N" || isset( $_REQUEST["spell"] ) ) {
		$arResult["REQUEST"]["~QUERY"] = $q;
		$arResult["REQUEST"]["QUERY"]  = htmlspecialcharsex( $q );
	} else {
		$arLang = CSearchLanguage::GuessLanguage( $q );
		if ( is_array( $arLang ) && $arLang["from"] != $arLang["to"] ) {
			$arResult["REQUEST"]["~ORIGINAL_QUERY"] = $q;
			$arResult["REQUEST"]["ORIGINAL_QUERY"]  = htmlspecialcharsex( $q );

			$arResult["REQUEST"]["~QUERY"] = CSearchLanguage::ConvertKeyboardLayout( $arResult["REQUEST"]["~ORIGINAL_QUERY"], $arLang["from"], $arLang["to"] );
			$arResult["REQUEST"]["QUERY"]  = htmlspecialcharsex( $arResult["REQUEST"]["~QUERY"] );
		} else {
			$arResult["REQUEST"]["~QUERY"] = $q;
			$arResult["REQUEST"]["QUERY"]  = htmlspecialcharsex( $q );
		}
	}

} else {
	$arResult["REQUEST"]["~QUERY"] = false;
	$arResult["REQUEST"]["QUERY"]  = false;
}

if ( $tags !== false ) {
	$arResult["REQUEST"]["~TAGS_ARRAY"] = array();
	$arTags                             = explode( ",", $tags );
	foreach ( $arTags as $tag ) {
		$tag = trim( $tag );
		if ( strlen( $tag ) > 0 ) {
			$arResult["REQUEST"]["~TAGS_ARRAY"][ $tag ] = $tag;
		}
	}
	$arResult["REQUEST"]["TAGS_ARRAY"] = htmlspecialcharsex( $arResult["REQUEST"]["~TAGS_ARRAY"] );
	$arResult["REQUEST"]["~TAGS"]      = implode( ",", $arResult["REQUEST"]["~TAGS_ARRAY"] );
	$arResult["REQUEST"]["TAGS"]       = htmlspecialcharsex( $arResult["REQUEST"]["~TAGS"] );
} else {
	$arResult["REQUEST"]["~TAGS_ARRAY"] = array();
	$arResult["REQUEST"]["TAGS_ARRAY"]  = array();
	$arResult["REQUEST"]["~TAGS"]       = false;
	$arResult["REQUEST"]["TAGS"]        = false;
}
$arResult["REQUEST"]["WHERE"] = htmlspecialcharsbx( $where );

$arResult["URL"] = $APPLICATION->GetCurPage()
                   . "?q=" . urlencode( $q )
                   . ( isset( $_REQUEST["spell"] ) ? "&amp;spell=1" : "" )
                   . "&amp;where=" . urlencode( $where )
                   . ( $tags !== false ? "&amp;tags=" . urlencode( $tags ) : "" );

if ( isset( $arResult["REQUEST"]["~ORIGINAL_QUERY"] ) ) {
	$arResult["ORIGINAL_QUERY_URL"] = $APPLICATION->GetCurPage()
	                                  . "?q=" . urlencode( $arResult["REQUEST"]["~ORIGINAL_QUERY"] )
	                                  . "&amp;spell=1"
	                                  . "&amp;where=" . urlencode( $arResult["REQUEST"]["WHERE"] )
	                                  . ( $arResult["REQUEST"]["HOW"] == "d" ? "&amp;how=d" : "" )
	                                  . ( $arResult["REQUEST"]["FROM"] ? '&amp;from=' . urlencode( $arResult["REQUEST"]["~FROM"] ) : "" )
	                                  . ( $arResult["REQUEST"]["TO"] ? '&amp;to=' . urlencode( $arResult["REQUEST"]["~TO"] ) : "" )
	                                  . ( $tags !== false ? "&amp;tags=" . urlencode( $tags ) : "" );
}

$templatePage = "";
$arReturn     = false;
if ( $this->InitComponentTemplate( $templatePage ) ) {
	$template                = &$this->GetTemplate();
	$arResult["FOLDER_PATH"] = $folderPath = $template->GetFolder();

	if ( strlen( $folderPath ) > 0 ) {
		$arFilter = array(
			"SITE_ID" => SITE_ID,
			"QUERY"   => trim($arResult["REQUEST"]["~QUERY"]),
			"TAGS"    => $arResult["REQUEST"]["~TAGS"],
		);
		$arFilter = array_merge( $arFILTERCustom, $arFilter );
		if ( strlen( $where ) > 0 ) {
			list( $module_id, $part_id ) = explode( "_", $where, 2 );
			$arFilter["MODULE_ID"] = $module_id;
			if ( strlen( $part_id ) > 0 ) {
				$arFilter["PARAM1"] = $part_id;
			}
		}
		if ( $arParams["CHECK_DATES"] ) {
			$arFilter["CHECK_DATES"] = "Y";
		}
		if ( $from ) {
			$arFilter[">=DATE_CHANGE"] = $from;
		}
		if ( $to ) {
			$arFilter["<=DATE_CHANGE"] = $to;
		}

		$obSearch = new CSearch();
		$exFILTER["STEMMING"] = false;
		//When restart option is set we will ignore error on query with only stop words
		$obSearch->SetOptions( array(
			"ERROR_ON_EMPTY_STEM" => $arParams["RESTART"] != "Y",
			"NO_WORD_LOGIC"       => $arParams["NO_WORD_LOGIC"] == "Y",
		) );
		//$arFilter['QUERY'] = $arFilter['QUERY'].'*';

		$obSearch->Search( $arFilter, $aSort, $exFILTER );

		$arResult["ERROR_CODE"] = $obSearch->errorno;
		$arResult["ERROR_TEXT"] = $obSearch->error;

		$arResult["SEARCH"] = array();
		if ( $obSearch->errorno == 0 ) {
			$obSearch->NavStart( $arParams["PAGE_RESULT_COUNT"], false );
			$ar    = $obSearch->GetNext();
			$allId = [ ];

			$objSection = new CIBlockSection();
			$objElement = new CIBlockElement();

			$arReturn = array();
			while ( $ar ) {

				if ( substr( $ar["ITEM_ID"], 0, 1 ) != "S" ) {
					$arReturn[ $ar["ID"] ] = $ar["ITEM_ID"];
					//$allId[IBLOCK_PRODUCTS] []= $ar['ITEM_ID'];
					 $allId [ $ar['PARAM2']] []= $ar['ITEM_ID'];
				}
				$ar["URL"]  = htmlspecialcharsbx( $ar["URL"] );

				// ������
				if ( substr( $ar["ITEM_ID"], 0, 1 ) == "S" ) {
					$arResult['SECTION_IDS'] [substr( $ar["ITEM_ID"], 1 )]= substr( $ar["ITEM_ID"], 1 );
				} else {
					$arResult["SEARCH"][ $ar['ITEM_ID'] ] = $ar;
				}

				$ar = $obSearch->GetNext();
			}
			$arNavParams = array('nTopCount'=>500);
			global $USER;


			// ������ � ������ �������
			$tarifs = [];
			if ( ! empty( $allId[IBLOCK_PRODUCTS] ) ) { // ������

				$arFilter   = array( 'ID' => $allId[IBLOCK_PRODUCTS] );//
				$rsElements = CIBlockElement::GetList(
					array('PROPERTY_FIRM.PROPERTY_EXT_SORT' => 'desc', 'PROPERTY_FIRM.PROPERTY_rating' => 'desc' ),//
					$arFilter,
					false,
					$arNavParams,
					array(
						"ID",
						"NAME",
						"PREVIEW_PICTURE",
						"PREVIEW_TEXT", "DETAIL_TEXT",
						"IBLOCK_SECTION_ID",
						"IBLOCK_ID",
						"PROPERTY_FIRM.ID",
						"PROPERTY_FIRM.IBLOCK_ID",
						"PROPERTY_FIRM.NAME",
						"PROPERTY_FIRM.PREVIEW_PICTURE",
						"PROPERTY_FIRM.DETAIL_PAGE_URL",
						"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
						"PROPERTY_FIRM.PROPERTY_rating",
						"PROPERTY_FIRM.PROPERTY_TARIF",
						"PROPERTY_FIRM.PROPERTY_PREMIUM",
						"PROPERTY_FIRM.PROPERTY_EXT_SORT"
					)
				);

				// rating TARIF PREMIUM
				while ( $arElements = $rsElements->GetNext() ) {
					if ( $arElements["PROPERTY_FIRM_PROPERTY_REMOVE_REL_VALUE"] == '��' ) {
						continue;
					}
					if ( !empty($arElements["PROPERTY_FIRM_PROPERTY_TARIF_VALUE"] ) ) {
						$tarifs []= $arElements["PROPERTY_FIRM_PROPERTY_TARIF_VALUE"];
					}

					$picture_id                                  = $arElements['DETAIL_PICTURE'] == '' ? $arElements['PREVIEW_PICTURE'] : $arElements['DETAIL_PICTURE'];
					$arElements['PREVIEW_PICTURE']               = Resizer( $picture_id, array(
						'width'  => 50,
						'height' => 50
					), BX_RESIZE_IMAGE_PROPORTIONAL, true );
					$arElements['PROPERTY_FIRM_PREVIEW_PICTURE'] = Resizer( $arElements['PROPERTY_FIRM_PREVIEW_PICTURE'], array(
						'width'  => 50,
						'height' => 50
					), BX_RESIZE_IMAGE_PROPORTIONAL, true );
					$arResult["SEARCH"][ $arElements['ITEM_ID'] ] ['NAME'] = $arElements['NAME'];
					$arResult["SEARCH"][ $arElements['ITEM_ID'] ] ['EX_DETAIL'] = $arElements['DETAIL_TEXT'];
					$arResult["SEARCH_ITEMS"][ $arElements['ID'] ] = $arElements;
					$arResult['SECTION_IDS'] [$arElements['IBLOCK_SECTION_ID']]= $arElements['IBLOCK_SECTION_ID'];
				}
			}

			if (!empty($tarifs)){

				$arFilter   = array( 'ID' => $tarifs);//
				$rsElements = CIBlockElement::GetList(
					array(),//
					$arFilter,
					false,
					$arNavParams,
					array(
						"ID",
						"NAME"
					)
				);
				// rating TARIF PREMIUM
				while ( $arElements = $rsElements->GetNext() ) {
					$arResult['TARIFS'] [$arElements['ID']] = $arElements['NAME'];
				}
			}

			if ( ! empty( $allId[IBLOCK_USLUGI] ) ) { // ������
				$arFilter   = array( 'ID' => $allId[IBLOCK_USLUGI] );//
				$rsElements = CIBlockElement::GetList(
					array('PROPERTY_FIRM.PROPERTY_EXT_SORT' => 'desc', 'PROPERTY_FIRM.PROPERTY_rating' => 'desc' ),//
					$arFilter,
					false,
					$arNavParams,
					array(
						"ID",
						"NAME",
						"PREVIEW_PICTURE",
						"PREVIEW_TEXT",
						"IBLOCK_SECTION_ID",
						"IBLOCK_ID",
						"PROPERTY_FIRM.ID",
						"PROPERTY_FIRM.IBLOCK_ID",
						"PROPERTY_FIRM.NAME",
						"PROPERTY_FIRM.PREVIEW_PICTURE",
						"PROPERTY_FIRM.DETAIL_PAGE_URL",
						"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
						"PROPERTY_FIRM.PROPERTY_rating",
						"PROPERTY_FIRM.PROPERTY_TARIF",
						"PROPERTY_FIRM.PROPERTY_PREMIUM",
						"PROPERTY_FIRM.PROPERTY_EXT_SORT"
					)
				);

				// rating TARIF PREMIUM
				while ( $arElements = $rsElements->GetNext() ) {
					if ( $arElements["PROPERTY_FIRM_PROPERTY_REMOVE_REL_VALUE"] == '��' ) {
						continue;
					}
					if ( !empty($arElements["PROPERTY_FIRM_PROPERTY_TARIF_VALUE"] ) ) {
						$tarifs []= $arElements["PROPERTY_FIRM_PROPERTY_TARIF_VALUE"];
					}

					$picture_id                                  = $arElements['DETAIL_PICTURE'] == '' ? $arElements['PREVIEW_PICTURE'] : $arElements['DETAIL_PICTURE'];
					$arElements['PREVIEW_PICTURE']               = Resizer( $picture_id, array(
						'width'  => 50,
						'height' => 50
					), BX_RESIZE_IMAGE_PROPORTIONAL, true );
					$arElements['PROPERTY_FIRM_PREVIEW_PICTURE'] = Resizer( $arElements['PROPERTY_FIRM_PREVIEW_PICTURE'], array(
						'width'  => 50,
						'height' => 50
					), BX_RESIZE_IMAGE_PROPORTIONAL, true );

					$arResult["SEARCH_ITEMS"][ $arElements['ID'] ] = $arElements;
					$arResult['SECTION_IDS'] [$arElements['IBLOCK_SECTION_ID']]= $arElements['IBLOCK_SECTION_ID'];
				}
			}

			if (!empty($tarifs)){

				$arFilter   = array( 'ID' => $tarifs);//
				$rsElements = CIBlockElement::GetList(
					array(),//
					$arFilter,
					false,
					$arNavParams,
					array(
						"ID",
						"NAME"
					)
				);
				// rating TARIF PREMIUM
				while ( $arElements = $rsElements->GetNext() ) {
					$arResult['TARIFS'] [$arElements['ID']] = $arElements['NAME'];
				}
			}

			if ( ! empty( $allId[IBLOCK_COMPANY] ) ) { // ��������

				$arFilter   = array( 'ID' => $allId[IBLOCK_COMPANY]);
				$rsElements = CIBlockElement::GetList(
					array('PROPERTY_EXT_SORT' => 'DESC' ),//
					$arFilter,
					false,
					$arNavParams,
					array(
						"ID",
						"NAME",
						"PREVIEW_PICTURE", "PREVIEW_TEXT",
						"IBLOCK_SECTION_ID",
						"IBLOCK_ID",

					)
				);

				while ( $arElements = $rsElements->GetNext() ) {


					$picture_id                                  = $arElements['DETAIL_PICTURE'] == '' ? $arElements['PREVIEW_PICTURE'] : $arElements['DETAIL_PICTURE'];
					$arElements['PREVIEW_PICTURE']               = Resizer( $picture_id, array(
						'width'  => 50,
						'height' => 50
					), BX_RESIZE_IMAGE_PROPORTIONAL, true );
					$arElements['PROPERTY_FIRM_PREVIEW_PICTURE'] = Resizer( $arElements['PROPERTY_FIRM_PREVIEW_PICTURE'], array(
						'width'  => 50,
						'height' => 50
					), BX_RESIZE_IMAGE_PROPORTIONAL, true );

					$arResult["SEARCH_ITEMS"][ $arElements['ID'] ] = $arElements;
					$arResult['SECTION_IDS'] [$arElements['IBLOCK_SECTION_ID']]= $arElements['IBLOCK_SECTION_ID'];
				}
			}
			$call = count($allId[IBLOCK_COMPANY])+count($allId[IBLOCK_PRODUCTS]);
			unset( $allId[IBLOCK_COMPANY]);
			unset( $allId[IBLOCK_PRODUCTS]);

			if ($call < 4) {
				$az = [];
				foreach ($allId as $it){
					$az = array_merge($it, $az);
				}
			}
			if ( ! empty( $az ) ) { // ���������

						$arFilter   = array( 'ID' =>$az);
						$rsElements = CIBlockElement::GetList(
							array(),//
							$arFilter,
							false,
							$arNavParams,
							array(
								"ID",
								"NAME",
								"DETAIL_PICTURE",
								"PREVIEW_PICTURE", "PREVIEW_TEXT",
								"IBLOCK_SECTION_ID",
								"IBLOCK_ID",
								"PROPERTY_FIRM.ID",
								"PROPERTY_FIRM.IBLOCK_ID",
								"PROPERTY_FIRM.NAME",
								"PROPERTY_FIRM.PREVIEW_PICTURE",
								"PROPERTY_FIRM.DETAIL_PAGE_URL",
								"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
								"PROPERTY_FIRM.PROPERTY_EXT_SORT",
							)
						);

						while ( $arElements = $rsElements->GetNext() ) {

							$picture_id                                  = $arElements['DETAIL_PICTURE'] == '' ? $arElements['PREVIEW_PICTURE'] : $arElements['DETAIL_PICTURE'];
							$arElements['PREVIEW_PICTURE']               = Resizer( $picture_id, array(
								'width'  => 50,
								'height' => 50
							), BX_RESIZE_IMAGE_PROPORTIONAL, true );
							$arElements['PROPERTY_FIRM_PREVIEW_PICTURE'] = Resizer( $arElements['PROPERTY_FIRM_PREVIEW_PICTURE'], array(
								'width'  => 50,
								'height' => 50
							), BX_RESIZE_IMAGE_PROPORTIONAL, true );

							$arResult["SEARCH_ITEMS"][ $arElements['ID'] ] = $arElements;
						}
					}

			if (true) {
				// ������� ������ 2 �������
				//PrintAdmin($arResult['SECTION_IDS']);
				$top2 = [];
				$top3 = [];
				foreach ($arResult['SECTION_IDS'] as $id) {
					$resSection = $objSection->GetByID( $id );
					if ( $element = $resSection->GetNext() ) {
						//
						if ($element["DEPTH_LEVEL"] ==  2) {
							$top2 []= $element['ID'];
						} elseif ($element["DEPTH_LEVEL"] > 2) {
							$resSection_v = $objSection->GetByID( $element['IBLOCK_SECTION_ID'] );
							if ( $element_v = $resSection_v->GetNext() ) {

								if ($element_v["DEPTH_LEVEL"] == 2) {
									$top2 []= $element_v['ID'];
								} elseif ($element_v["DEPTH_LEVEL"] > 2) {
									$resSection_2 = $objSection->GetByID( $element_v['IBLOCK_SECTION_ID'] );
									if ( $element_2 = $resSection_2->GetNext() ) {
										$top2 []= $element_2['ID'];
									}
								}
							}
							$top3 []=$element['ID'];
						}
					}
				}
				$arResult['SECTION_IDS'] = array_unique($top2);

			    $arResult['SECTION_IDS'] = array_slice($arResult['SECTION_IDS'], 0, $arParams["PAGE_RESULT_COUNT_DISPLAYED"]);

					foreach ($arResult['SECTION_IDS'] as $id) {
						$resSection = $objSection->GetByID( $id );
						if ( $element = $resSection->GetNext() ) {
							$element["DETAIL_PAGE_URL"] = $element["SECTION_PAGE_URL"];
							$element["IS_SECTION"]      = true;
							//picture
							if ( $element["PICTURE"] ) {
								$FILE = CFile::GetFileArray( $element["PICTURE"] );
								if ( is_array( $FILE ) ) {
									$arFilterImg = array();
									$arFileTmp   = CFile::ResizeImageGet(
										$FILE,
										array(
											"width"  => $arParams["THUMB_WIDTH"],
											"height" => $arParams["THUMB_HEIGHT"]
										),
										BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
										true,
										$arFilterImg
									);

									$FILE = array(
										'SRC'         => $arFileTmp["src"],
										'WIDTH'       => $arFileTmp["width"],
										'HEIGHT'      => $arFileTmp["height"],
										'DESCRIPTION' => strlen( $FILE["DESCRIPTION"] ) > 0 ? $FILE : $element["NAME"]
									);

									$element["PREVIEW_PICTURE"] = $FILE;
								}
							}
							//����� �����������
							$element["SUB"] = array();
							$resSubSection  = $objSection->GetList(
								array( "NAME" => "ASC" ),
								array(
									"IBLOCK_ID"   => $element["IBLOCK_ID"],
									"DEPTH_LEVEL" => $element["DEPTH_LEVEL"] + 1,
									"SECTION_ID"  => $element["ID"],
									"ID" =>$top3,
									"ACTIVE"      => "Y"
								),
								false,
								false,
								array( "nPageSize" => 4 )
							);
							while ( $arSubSection = $resSubSection->GetNext() ) {
								$arSubSection["URL"] = $arSubSection["SECTION_PAGE_URL"];
								$element["SUB"][]    = $arSubSection;
							}

							//����� ���������
							if ( empty( $element["SUB"] ) ) {
								$sort = array( "SORT" => "ASC" );
								if ( $element["IBLOCK_ID"] == IBLOCK_COMPANY){
									$sort = array( "PROPERTY_EXT_SORT" => "DESC" );
								}

								$resSubElement = $objElement->GetList(
									array( "SORT" => "ASC" ),
									array(
										"IBLOCK_ID"  => $element["IBLOCK_ID"],
										"SECTION_ID" => $element["ID"],
										"INCLUDE_SUBSECTIONS" => "Y",
										"ACTIVE"     => "Y",
										"!PROPERTY_REMOVE_REL" => '��'
									),
									false,
									array( "nTopCount" => 4 )
								);
								while ( $arSubElement = $resSubElement->GetNext() ) {
									$arSubElement["URL"] = $arSubElement["DETAIL_PAGE_URL"];
									$element["SUB"][]    = $arSubElement;
								}
							}
						}

						$ar['ITEM_ID'] = $element["ID"];
						$ar['PARAM2']  = $element["IBLOCK_ID"];
						$ar['URL']     = $element["DETAIL_PAGE_URL"];
						$ar['NAME']    = $element["NAME"];

						$ar["REAL_ELEMENT"]                           = $element;
						$arResult["SEARCH_SECTION"][ $ar['ITEM_ID'] ] = $ar;

					}
			}

			$navComponentObject          = null;
			$arResult["NAV_STRING"]      = $obSearch->GetPageNavStringEx( $navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"] );
			$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
			$arResult["NAV_RESULT"]      = $obSearch;
		}

		$arResult["TAGS_CHAIN"] = array();
		$url                    = array();
		foreach ( $arResult["REQUEST"]["~TAGS_ARRAY"] as $key => $tag ) {
			$url_without = $arResult["REQUEST"]["~TAGS_ARRAY"];
			unset( $url_without[ $key ] );
			$url[ $tag ]              = $tag;
			$result                   = array(
				"TAG_NAME"    => $tag,
				"TAG_PATH"    => $APPLICATION->GetCurPageParam( "tags=" . urlencode( implode( ",", $url ) ), array( "tags" ) ),
				"TAG_WITHOUT" => $APPLICATION->GetCurPageParam( "tags=" . urlencode( implode( ",", $url_without ) ), array( "tags" ) ),
			);
			$arResult["TAGS_CHAIN"][] = $result;
		}

		$this->ShowComponentTemplate();
	}
} else {
	$this->__ShowError( str_replace( "#PAGE#", $templatePage, str_replace( "#NAME#", $this->__templateName, "Can not find '#NAME#' template with page '#PAGE#'" ) ) );
}
return $arReturn;
