<?
$MESS["SEARCH_FONT_MAX"] = "Tama�o largo de la fuente (px)";
$MESS["SEARCH_FONT_MIN"] = "Tama�o corto de la fuente (px)";
$MESS["SEARCH_COLOR_OLD"] = "Ultima etiqueta de color (ex. \"FEFEFE\")";
$MESS["SEARCH_COLOR_NEW"] = "(ex. \"C0C0C0\")";
$MESS["SEARCH_PERIOD_NEW_TAGS"] = "Considerar la nueva etiqueta durante (d�as)";
$MESS["SEARCH_SHOW_CHAIN"] = "Mostrar ruta de exploraci�n de navegaci�n.";
$MESS["SEARCH_COLOR_TYPE"] = "Usar colores degradados";
$MESS["SEARCH_WIDTH"] = "Anchura de la etiqueta nube (ex. \"100%\", \"100px\", \"100pt\" or \"100in\")";
?>