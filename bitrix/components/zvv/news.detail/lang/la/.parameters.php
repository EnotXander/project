<?
$MESS["T_IBLOCK_DESC_LIST_ID"] = "C�digo del Block de Informaci�n";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Tipo del Block de Informaci�n (usado s�lo para verificaci�n)";
$MESS["T_IBLOCK_DESC_LIST_PAGE_URL"] = "Lista URL de la p�gina (desde los ajustes por defecto del Block de Informaci�n)";
$MESS["T_IBLOCK_DESC_INCLUDE_IBLOCK_INTO_CHAIN"] = "Incluye Block de Informaci�n en la cadena de navegaci�n";
$MESS["T_IBLOCK_PROPERTY"] = "Propiedades";
$MESS["T_IBLOCK_DESC_KEYWORDS"] = "Ajuste de palabras clave desde la propiedad";
$MESS["T_IBLOCK_DESC_DESCRIPTION"] = "Ajuste de descripci�n de p�gina desde la propiedad";
$MESS["IBLOCK_FIELD"] = "Campos";
$MESS["T_IBLOCK_DESC_ACTIVE_DATE_FORMAT"] = "Formato para mostrar la fecha";
$MESS["T_IBLOCK_DESC_USE_PERMISSIONS"] = "Usar restricci�n adicional del acceso";
$MESS["T_IBLOCK_DESC_GROUP_PERMISSIONS"] = "Grupo de usuarios permitidos para ver descripci�n detallada";
$MESS["T_IBLOCK_DESC_PAGER_PAGE"] = "P�gina";
$MESS["T_IBLOCK_DESC_ADD_SECTIONS_CHAIN"] = "Agregar nombre de secci�n para breadcrumb de navegaci�n";
$MESS["T_IBLOCK_DESC_CHECK_DATES"] = "Mostrar solo elementos activos actuales";
$MESS["CP_BND_ELEMENT_CODE"] = "C�digo de noticias";
$MESS["CP_BND_ELEMENT_ID"] = "ID de las noticias";
$MESS["CP_BND_SET_STATUS_404"] = "Establecer el estado 404 si no se encontr� ning�n elemento o secci�n";
$MESS["CP_BND_BROWSER_TITLE"] = "Establecer t�tulo de la ventana de navegaci�n desde el valor de la propiedad";
$MESS["CP_BND_CACHE_GROUPS"] = "Respetar los permisos de acceso";
$MESS["T_IBLOCK_DESC_ADD_ELEMENT_CHAIN"] = "Agregar el nombre del elemento de breadcrumbs";
$MESS["CP_BND_SET_META_KEYWORDS"] = "Establecer palabras claves de la p�gina";
$MESS["CP_BND_SET_META_DESCRIPTION"] = "Conjunto de descripci�n de p�gina";
$MESS["CP_BND_SET_BROWSER_TITLE"] = "Establecer t�tulo de la ventana del navegador";
?>