<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

if($this->StartResultCache(false, array($USER->GetGroups(), $arParams)))
{
	if(!CModule::IncludeModule("forum"))
	{
		$this->AbortResultCache();
		return;
	}
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		return;
	}

	if(!CModule::IncludeModule("firstit.lasttopics"))
	{
		$this->AbortResultCache();
		return;
	}
	
	if(empty($arParams["SORT_BY"])) $arParams["SORT_BY"]="ID";
	if(empty($arParams["SORT_ORDER"])) $arParams["SORT_ORDER"]="DESC";
	if(empty($arParams["NUM_MESSAGES"])) $arParams["NUM_MESSAGES"]=50;
	if(empty($arParams["FORUM_TOPIC"])) $arParams["FORUM_TOPIC"]="/forum#FID#/topic#TID#/";
	if(empty($arParams["FORUM_MESSAGE"])) $arParams["FORUM_MESSAGE"]="/messages/forum#FID#/topic#TID#/message#MID#/#message#MID#";
	if(empty($arParams["FORUM_MAIN"])) $arParams["FORUM_MAIN"]="/forum";
	if(empty($arParams["myDATE_FORMAT"])) $arParams["myDATE_FORMAT"]="d.m";

	$FilterGet=Array();
	if(is_array($arParams["CHOOSE_FORUM"]) && count($arParams["CHOOSE_FORUM"])>0) $FilterGet["@FORUM_ID"]=$arParams["CHOOSE_FORUM"];
	elseif($arParams["CHOOSE_FORUM"]>0) $FilterGet["FORUM_ID"]=$arParams["CHOOSE_FORUM"];
	$FilterGet["APPROVED"]="Y";

	//debug($FilterGet);

	$db_res = CForumMessage::GetList(array($arParams["SORT_BY"]=>$arParams["SORT_ORDER"]), $FilterGet, false, 50);
	while ($ar_res = $db_res->Fetch())
	{
		if ($ar_res['NEW_TOPIC'] == 'Y') continue; /// ���������� ������ ����

		$parser = new textParser();
		$arAllow = $Forum->ALLOW;                  
		$arAllow["SMILES"] = "Y";
		$arAllow["HTML"] = "Y";
		$arAllow["LIST"] = "Y";
		$arAllow["FONT"] = "Y";
		$arAllow["CODE"] = "Y";
		$arAllow["QUOTE"] = "Y";
		$arAllow["ANCHOR"] = "Y";
		$arAllow["BIU"] = "Y";
		$arAllow["IMG"] = "Y";
		$ar_res["POST_MESSAGE"]=$parser->convert($ar_res["POST_MESSAGE"], $arAllow);

		$ar_res["TOPIC_INFO"]=CFirstitLasttopics::get_topic($ar_res["TOPIC_ID"]);
		
		$DataGet["AUTHOR_NAME"]=$ar_res["AUTHOR_NAME"];
		
		if ($ar_res["AUTHOR_ID"]){
			$rsUser = CUser::GetByID($ar_res["AUTHOR_ID"]);
			$arUser = $rsUser->Fetch();
			$DataGet["AUTHOR_PARAMS"] = $arUser;
			if ($arUser['PERSONAL_PHOTO']){
				$arUserPhoto = CFile::GetFileArray($arUser['PERSONAL_PHOTO']);
				$DataGet["AUTHOR_PARAMS"]['PERSONAL_PHOTO_ARRAY'] = $arUserPhoto;
			}
		}
		
		$unixtime = MakeTimeStamp($ar_res["POST_DATE"], CSite::GetDateFormat());
		$DataGet["POST_DATE"] = date($arParams['myDATE_FORMAT'], $unixtime);
		
		$DataGet["TITLE"]=CFirstitLasttopics::better_truncate(strip_tags($ar_res["TOPIC_INFO"]["TITLE"]),31);
		$DataGet["MESSAGE"]=CFirstitLasttopics::better_truncate(strip_tags($ar_res["POST_MESSAGE"]),$arParams["NUM_MESSAGES_TRUN"]);
		
		/// #1

		$ELEMENT_ID = $ar_res['PARAM2'];
		$DataGet["TOPIC_URL"] = $arParams["FORUM_TOPIC"];
		$DataGet["MESSAGE_URL"] = $arParams["FORUM_MESSAGE"];
		
		$DataGet["TOPIC_URL"] = str_replace('#ELEMENTID#', $ELEMENT_ID, $DataGet["TOPIC_URL"]);
		$DataGet["MESSAGE_URL"] = str_replace(
			array('#ELEMENTID#','#MID#'),
			array($ELEMENT_ID,$ar_res['ID']),
			$DataGet["MESSAGE_URL"]
		);
		
		$resElem = CIBlockElement::GetByID($ELEMENT_ID);
		$arrElem = $resElem->GetNext();
		$DataGet["TOPIC_URL"] = $arrElem['DETAIL_PAGE_URL'];
		$DataGet["IBLOCK_ID"] = $arrElem['IBLOCK_ID'];
		if (LANG == $arrElem['LID']){
			$DataGet["LID"] = $arrElem['LID'];

			//$DataGet["MESSAGE_URL"] = str_replace('#ELEMENT_CODE#', $arrElem['CODE'], $DataGet["MESSAGE_URL"]);

			//$resSect = CIBlockSection::GetByID($arrElem['IBLOCK_SECTION_ID']);
			//$arrSect = $resSect->GetNext();
			//$DataGet["TOPIC_URL"] = str_replace('#SECTION_CODE#', $arrSect['CODE'], $DataGet["TOPIC_URL"]);
			//$DataGet["MESSAGE_URL"] = str_replace('#SECTION_CODE#', $arrSect['CODE'], $DataGet["MESSAGE_URL"]);
			$DataGet["MESSAGE_URL"] = $arrElem['DETAIL_PAGE_URL'].'#message'.$ar_res['ID'];
			/// -#1

			$arResult["ITEMS"][]=$DataGet;
			if (count ($arResult["ITEMS"]) >= $arParams["NUM_MESSAGES"]) break;
		}
		$DataGet=Array();
	}

	$this->SetResultCacheKeys(array(
		"ITEMS"
	));
	$this->IncludeComponentTemplate();
}
?>