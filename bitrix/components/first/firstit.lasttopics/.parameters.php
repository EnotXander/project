<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

if(!CModule::IncludeModule("forum"))
	return;

$Forums["all"] = GetMessage("FORUM_ALL");
$MainQuery = CForumNew::GetList(array($arParams["SORT_BY"]=>$arParams["SORT_ORDER"]), array());
while ($MainQueryGet = $MainQuery->Fetch())
{
	$Forums[$MainQueryGet["ID"]] = $MainQueryGet["NAME"];
}

$arSorts = Array("ASC"=>GetMessage("T_IBLOCK_DESC_ASC"), "DESC"=>GetMessage("T_IBLOCK_DESC_DESC"),"RAND"=>GetMessage("T_IBLOCK_DESC_RAND"));
$arSortFields = Array(
		"ID"=>GetMessage("T_IBLOCK_DESC_FID"),
		"NAME"=>GetMessage("T_IBLOCK_DESC_FNAME"),
		"ACTIVE_FROM"=>GetMessage("T_IBLOCK_DESC_FACT"),
		"SORT"=>GetMessage("T_IBLOCK_DESC_FSORT"),
		"TIMESTAMP_X"=>GetMessage("T_IBLOCK_DESC_FTSAMP")
	);

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"SORT_ORDER" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("T_IBLOCK_DESC_IBBY1"),
			"TYPE" => "LIST",
			"DEFAULT" => "DESC",
			"VALUES" => $arSorts,
		),
		"SORT_BY" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("T_IBLOCK_DESC_IBORD2"),
			"TYPE" => "LIST",
			"DEFAULT" => "SORT",
			"VALUES" => $arSortFields,
			"ADDITIONAL_VALUES" => "Y",
		),
		"CHOOSE_FORUM" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("FORUM_CHOOSE"),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"DEFAULT" => "all",
			"VALUES" => $Forums,
		),
		"FORUM_MAIN" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("FORUM_MAIN"),
			"TYPE" => "STRING",
			"DEFAULT" => '/forum',
		),			
		"FORUM_TOPIC" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("FORUM_TOPIC"),
			"TYPE" => "STRING",
			"DEFAULT" => '/forum#FID#/topic#TID#/',
		),		
		"FORUM_MESSAGE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("FORUM_MESSAGE"),
			"TYPE" => "STRING",
			"DEFAULT" => '/messages/forum#FID#/topic#TID#/message#MID#/#message#MID#',
		),
		"NUM_MESSAGES" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("NUM_MESSAGES"),
			"TYPE" => "STRING",
			"DEFAULT" => '5',
		),
		"NUM_MESSAGES_TRUN" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("NUM_MESSAGES_TRUN"),
			"TYPE" => "STRING",
			"DEFAULT" => '100',
		),		
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);
?>
