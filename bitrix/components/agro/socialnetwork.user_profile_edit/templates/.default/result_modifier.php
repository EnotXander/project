<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arResult['USER_PROP'] = array();
$arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", 0, LANGUAGE_ID);
if (!empty($arRes))
{
   foreach ($arRes as $key => $val)
   {
      $arResult['USER_PROP'][$val["FIELD_NAME"]] = (strLen($val["EDIT_FORM_LABEL"]) > 0 ? $val["EDIT_FORM_LABEL"] : $val["FIELD_NAME"]);
      $val['ENTITY_VALUE_ID'] = $arResult['User']['ID'];
      $val['VALUE'] = $arResult['User'][$val['FIELD_NAME']];
      $arResult['USER_PROPERTY_ALL'][$val['FIELD_NAME']] = $val;
   }
}

//����� ����������
if(!CModule::IncludeModule("iblock")) return;
$rsPlace = CIBlockElement::GetList(
   array("NAME" => "ASC", "SORT" => "ASC"),
   array(
      "IBLOCK_ID" => 26,
      "ACTIVE" => "Y"
   ),
   false,
   false,
   array("ID", "NAME"));
while($arPlace = $rsPlace->GetNext())
{
   $arResult["PLACE"]["COUNTRY"][] = $arPlace;
}
if(!CModule::IncludeModule("iblock")) return;
$rsPlace = CIBlockElement::GetList(
   array("NAME" => "ASC", "SORT" => "ASC"),
   array(
      "IBLOCK_ID" => 22,
      "ACTIVE" => "Y",
      "PROPERTY_COUNTRY" => $arResult["USER_PROPERTY_ALL"]["UF_COUNTRY"]["VALUE"]
   ),
   false,
   false,
   array("ID", "NAME"));
while($arPlace = $rsPlace->GetNext())
{
   $arResult["PLACE"]["REGION"][] = $arPlace;
}
if(!CModule::IncludeModule("iblock")) return;
$rsPlace = CIBlockElement::GetList(
   array("NAME" => "ASC", "SORT" => "ASC"),
   array(
      "IBLOCK_ID" => 23,
      "ACTIVE" => "Y",
      "PROPERTY_COUNTRY" => $arResult["USER_PROPERTY_ALL"]["UF_COUNTRY"]["VALUE"],
      "PROPERTY_REGION" => $arResult["USER_PROPERTY_ALL"]["UF_REGION"]["VALUE"]
   ),
   false,
   false,
   array("ID", "NAME"));
while($arPlace = $rsPlace->GetNext())
{
   $arResult["PLACE"]["CITY"][] = $arPlace;
}
//����������
$arResult["EMPLOYEE"] = EmployeeGetByUserAgro();
$arResult["PREDSTAVITEL"] = PredstavitelGetByUserAgro();

//��������
if(!CModule::IncludeModule("iblock")) return;
$rsCompany = CIBlockElement::GetList(
   array("NAME" => "ASC", "SORT" => "ASC"),
   array(
      "IBLOCK_ID" => 65,
      "ACTIVE" => "Y"
   ),
   false,
   false,
   array("ID", "NAME", "DETAIL_PAGE_URL"));
while($arCompany = $rsCompany->GetNext())
{
   $arResult["COMPANY"][$arCompany["ID"]] = $arCompany;
   $companyIndex = array_search($arCompany["ID"], $arResult["EMPLOYEE"]["COMPANIES"]);
   if($companyIndex !== false)
   {
      $arResult["EMPLOYEE"]["COMPANIES"][$companyIndex] = $arCompany;
   }
   $companyIndex = array_search($arCompany["ID"], $arResult["PREDSTAVITEL"]["COMPANIES"]);
   if($companyIndex !== false)
   {
      $arResult["PREDSTAVITEL"]["COMPANIES"][$companyIndex] = $arCompany;
   }
}
//���������
if(!CModule::IncludeModule("iblock")) return;
$rsCompany = CIBlockElement::GetList(
   array("NAME" => "ASC", "SORT" => "ASC"),
   array(
      "IBLOCK_ID" => 30,
      "ACTIVE" => "Y"
   ),
   false,
   false,
   array("ID", "NAME"));
while($arCompany = $rsCompany->GetNext())
{
   $arResult["POSITION"][$arCompany["ID"]] = $arCompany;
   $companyIndex = array_search($arCompany["ID"], $arResult["EMPLOYEE"]["POSITIONS"]);
   if($companyIndex !== false)
   {
      $arResult["EMPLOYEE"]["POSITIONS"][$companyIndex] = $arCompany;
   }
}
//���� ��������, � ������� �������������� ������� �
$rsCompany = CIBlockElement::GetList(
   array("NAME" => "ASC", "SORT" => "ASC"),
   array(
      "IBLOCK_ID" => 65,
      "ACTIVE" => "Y",
      "PROPERTY_USER" => $USER->GetID()
   )
);
if($arCompany = $rsCompany->GetNext())
{
   $arResult["MANAGED_COMPANY"] = $arCompany;
}
//PrintObject($_REQUEST);
//PrintObject($arResult["EMPLOYEE_EDIT"]);
//PrintObject($arResult["EMPLOYEE_POST"]);
?>