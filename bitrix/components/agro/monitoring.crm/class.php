<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main;

class CrmMonitoring extends CBitrixComponent
{
    const COMPONENT_NAME = 'whipstudio:monitoring.crm';

    private $messageRepo;

    public function __construct()
    {
        $this->messageRepo = new \Glifery\Crm\MessageRepository();

    }

    private function getMessages()
    {
        $inMEssages = $this->messageRepo->findInMessages();
        $outMEssages = $this->messageRepo->findOutMessages();

        $this->arResult = array(
            'messages' => array(
                'in'  => $this->splitMessagesByDate($inMEssages),
                'out' => $this->splitMessagesByDate($outMEssages)
            )
        );
    }

    private function splitMessagesByDate(array $messages)
    {
        $dailyMessages = array();

        foreach ($messages as $message) {
            /** @var \Glifery\Crm\AbstractMessage $message */
            $messageDate = $message->getDate();
            $dateDay = $messageDate->format('d.m.Y');

            if (!isset($dailyMessages[$dateDay])) {
                $dailyMessages[$dateDay] = array();
            }

            $dailyMessages[$dateDay][] = $message;
        }

        return $dailyMessages;
    }

    public function executeComponent()
    {
        $this->initComponent(self::COMPONENT_NAME);

        $this->getMessages();

        $this->includeComponentTemplate();
    }
}