<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $USER;
$dbUser = $USER->GetList(
        ($by = "NAME"),
        ($order = "asc"),
        array(
            "!ID" => $USER->GetID(),
            "!NAME" => false,
            "!LAST_NAME" => false
         ),
        array("FIELDS" => array("ID", "PERSONAL_PHOTO", "NAME", "LAST_NAME", "LOGIN"))
   );

while($arUser = $dbUser->GetNext())
{
   $arResult["USERS"][] = $arUser;
}
//PrintObject($arResult["USERS"]);