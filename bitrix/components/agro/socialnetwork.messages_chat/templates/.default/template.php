<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->AddChainItem("��� � {$arResult["PARTNER_INFO"]["FORMATED_NAME"]}");
?>
<script type="text/javascript" src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/jquery.scrollTo.js"></script>

<!-- ������������� �� PUSHER -->
<script type="text/javascript">  
   //�������������� �����
   //energoPusher = energoPusher(<?= $arResult["arUser"]["ID"] ?>);
   //������������� �� ����� ���������
   energoPusher.messageCannel.bind('incoming_message', function(data) {
      var textarea = $(this).closest('.js-send-box').find('.js-send-textarea');
      var parentsID = <?= $arResult["PARTNER_INFO"]["ID"] ?>;
      var lastMessage = $('.js-messages-box').attr("data-last");
      var post = {user_id: parentsID, from: lastMessage};
      $.ajax({
         type: "POST",
         url: '/_ajax/messages/get_message.php',
         data: post,
         dataType: 'json',
         success: function (data){
            if(!data.ERROR){
               if(data.COUNT){
                  if($(".js-messages-box .js-messages-placeholder").is(':visible')) // ������ �����������
                     $(".js-messages-box .js-messages-placeholder").hide();

                  //ShowGood("Message send!");
                  $(".js-messages-box").attr("data-last", data.LAST)
                  $(".js-messages-box").append(data.HTML);
                  $(".js-messages-box").scrollTo($('.message-row:last'), 100);
               }
            }else{ // ������
               ShowError("Error: " + data.ERROR_TEXT);
            }
         },
         complete: function(){
            textarea.val("").removeAttr("disabled");
         }
      });
   });
   //������������� �� ���������� ���������
   energoPusher.messageCannel.bind('read_message', function(data) {
      for(var index in data.messageId){
         $('#rowMessage'+data.messageId[index]).removeClass("no-read");
      }
   });
</script>

<div class="right-block-content">
   <? if ($arResult["ERROR"]): ?>
      <div>A user with this ID is not found</div>
   <? else: ?>
      <div class="messages-block js-messages-box" data-last="<?=(is_array($arResult["MESSAGES"]) && count($arResult["MESSAGES"])) ? $arResult["MESSAGES"][count($arResult["MESSAGES"])-1]["ID"] : 0?>">
         <?
         // �� ������������
         if ($arResult["NEED_AUTH"] == "Y"):
            $APPLICATION->AuthForm("");
         else:
            ?>
            <div class="placeholder js-messages-placeholder" style="<? if (is_array($arResult["MESSAGES"])): ?>display: none;<? endif; ?>">
               <div>No message in a chat!</div>
            </div>                
            <? foreach ($arResult["MESSAGES"] as $key => $event): ?>
               <div class="message-row <?= $event["FROM_USER_ID"] != $arResult["arUser"]["ID"] ? 'take' : ''; ?> <?= $event["IS_READ"] ? "" : "no-read" ?>" id="rowMessage<?= $event["ID"] ?>">
                  <div class="photo">
                     <? if ($event["FROM_USER_ID"] != $arResult["arUser"]["ID"]): ?>
                        <? if (is_array($arResult["PARTNER_INFO"]["PERSONAL_PHOTO"])): ?>
                           <img src="<?=$arResult["PARTNER_INFO"]["PERSONAL_PHOTO"]["SRC"] ?>" alt="<?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>" title="<?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>" />
                        <? else: ?>
                           <img src="/images/search_placeholder.png" width="48" height="42" alt="<?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>" title="<?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>" />
                        <? endif; ?> 
                     <? else: ?>
                        <? if (is_array($arResult["arUser"]["PERSONAL_PHOTO"])): ?>
                           <img src="<?=$arResult["arUser"]["PERSONAL_PHOTO"]["SRC"] ?>" alt="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" title="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" />
                        <? else: ?>
                           <img src="/images/search_placeholder.png" width="48" height="42" alt="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" title="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" />
                        <? endif; ?>                                 
                     <? endif; ?>
                  </div>
                  <div class="message-control">
                     <div class="delete" title="Delete message">X</div>
                  </div>
                  <div class="info">
                     <div>
                        <? if ($event["FROM_USER_ID"] != $arResult["arUser"]["ID"]): ?>
                           <?= $arResult["PARTNER_INFO"]["FORMATED_NAME"] ?>
                        <? else: ?>
                           <?= $arResult["arUser"]["FORMATED_NAME"] ?>
                        <? endif; ?>

                        <div class="date">
                           <?= $event["DATE_CREATE"] ?>
                        </div>
                     </div>

                     <div class="message-text">
                        <?= $event["MESSAGE"]; ?>
                     </div>
                  </div>                    
                  
               </div>
            <? endforeach; ?>
         <? endif; ?>
      </div>
      <div class="comments_box js-send-box">
         <div>
            <div class="send-img">
               <? if (isset($arResult["arUser"]["PERSONAL_PHOTO"])): ?>
                  <img src="<?= $arResult["arUser"]["PERSONAL_PHOTO"]["SRC"] ?>" alt="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" title="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" />
               <? else: ?>
                  <img src="/images/search_placeholder.png" width="48" height="42" alt="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" title="<?= $arResult["arUser"]["FORMATED_NAME"] ?>" />
               <? endif; ?>
            </div>
            <textarea id="COMMENT_TEXT" rows="14" cols="55" class="post_message js-send-textarea"><?=$arResult["COMMENT_TEXT"]?></textarea>
         </div>
         <a class="btn-style js-send-button" href="javascript: void(0);"><i></i>Send</a>
         <div class="inputnote" style="float: left; margin-left: 15px;">shift+enter - line break<br />enter - send</div>
         <div class="error js-send-error">Testiruem</div>
         <div class="good js-send-good">Testiruem</div>
      </div>
      <div style="clear:both"></div>
   <? endif; ?>
</div>

<script type="text/javascript">
   $(document).ready(function(){
      if($('.message-row').size())
         $(".messages-block").scrollTo($('.message-row:last'), 100);
      
      // �������� ��������� ������������
      $(".js-send-button").live("click", function(){
         var textarea = $(this).closest('.js-send-box').find('.js-send-textarea');
         var partnersID = '["<?= $arResult["PARTNER_INFO"]["ID"] ?>"]';
         var regex = /\S+/;
         var message = textarea.val();
         if(message.length > 0){
            if(regex.test(message)){
               ShowGood("Sending....");
               textarea.attr("disabled","disabled");
               $.ajax({
                  type: "POST",
                  url: '/_ajax/messages/send_message.php',
                  data: 'PARTNERS_ID=' + partnersID + '&MSG=' + message,
                  dataType: 'json',
                  success: function (data){
                     if(!data.ERROR){
                        if(data.CHAT.COUNT){
                           if($(".js-messages-box .js-messages-placeholder").is(':visible')) // ������ �����������
                              $(".js-messages-box .js-messages-placeholder").hide();
                           ShowGood("Message send!");
                           $(".js-messages-box").attr("data-last", data.LAST)
                           $(".js-messages-box").append(data.CHAT.HTML);
                           $(".js-messages-box").scrollTo($('.message-row:last'), 100);
                        }
                     }else{ // ������
                        ShowError("Error: " + data.ERROR_TEXT);
                     }
                  },
                  complete: function(){
                     textarea.val("").removeAttr("disabled");
                  }
               });
            }else{
               ShowError("Error: Enter the message text");
            }
         }else{ // ������� �������� ���������
            ShowError("Error: Enter the message text");
         }
      });
   });
</script>