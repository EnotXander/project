<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$resizeArray = array("arUser", "PARTNER_INFO");
foreach($resizeArray as $fieldKey => $field)
{
   if (is_array($arResult[$field]["PERSONAL_PHOTO"]))
   {
      $arFileTmp = CFile::ResizeImageGet(
            $arResult[$field]["PERSONAL_PHOTO"],
            array("width" => 74, "height" => 55),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
      );
      $arResult[$field]["PERSONAL_PHOTO"] = array(
          "SRC" => $arFileTmp["src"],
          "WIDTH" => $arFileTmp["width"],
          "HEIGHT" => $arFileTmp["height"],
      );
   }
}