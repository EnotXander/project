<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (is_array($arResult["SECTIONS"]))
{
   foreach ($arResult["SECTIONS"] as $key => $arSection) 
   {
      $arSection["SECTION_PAGE_URL"] = "?{$arParams["SECTION_VAR"]}={$arSection["ID"]}#tabs";
      $arSection["ELEMENT_CNT"] = $arParams["COUNT_INFO"][$arSection["ID"]]["CNT"];
      $arResult["SECTIONS"][$key] = $arSection;
   }
}