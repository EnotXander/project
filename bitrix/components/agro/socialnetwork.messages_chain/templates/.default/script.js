function UpdateMessageChainFrom(messageId){
   if(messageId){
      var post = {from: messageId};
      $.ajax({
         type: "POST",
         url: '/_ajax/messages/get_message_chain.php',
         data: post,
         dataType: 'json',
         success: function (data){
            if(!data.ERROR){
               if(data.COUNT){
                  $(".js-messages-box").attr("data-last", data.LAST);
                  for(var index in data.USERS){
                     $('#rowMessage'+data.USERS[index]).remove();
                  }
                  $(".js-messages-box").prepend(data.HTML);
                  $(".js-messages-box").scrollTo($('.message-row:first'), 100);
               }
            }
         }
      });
      return true;
   }else
      return false;
}



$(document).ready(function(){
   //������������� �� ����� ���������
   console.log('call', energoPusher.messageCannel);
   energoPusher.messageCannel.bind('new_message', function(data) {
      UpdateMessageChainFrom($('.js-messages-box').attr("data-last"));
   });
   //������������� �� ���������� ���������
   energoPusher.messageCannel.bind('read_message', function(data) {
      UpdateMessageChainFrom(Math.max.apply(Math, data.messageId)-1);
   });

   // ������� � ������ �������
   $(".message-row").live("click", function(){
      idPartner = $(this).find(".message-control .delete").attr("id");
      idPartner = ~~idPartner.replace("deleteM", "");
      window.document.location.href = "/personal/messages/"+idPartner+"/";
   });
   //�������� ������� � ����� ����
   $(".message-row .photo img").live('click', function()
   {
      window.open($(this).closest('.message-row').find(".info a").attr('href'));
      event.stopPropagation();
      return false;
   });
   $(".message-row .info a").live('click', function()
   {
      event.stopPropagation();
      return false;
   })
    
   // �������� ������� ���������
   $(".message-row .message-control .delete").live("click", function(){    
      var size = $(".messages-block").attr("rel");
      var page = ~~size;
      size = 1;
      if(confirm("Are you sure you want to delete this chain?")){
         var row = $(this).parents(".message-row");
         var idPartner = $(this).attr("id");
         idPartner = idPartner.replace("deleteM", "");
         $.ajax({
            url: '/_ajax/messages/remove_message_list.php',
            data: 'PARTNER_ID=' + idPartner,
            dataType: 'json',
            success: function (dataD){
               if(!dataD.ERROR){
                  row.remove();
               }else{ // ������
                  alert("What is happend?");
               }
            }
         });
      }
      return false;
   });
});