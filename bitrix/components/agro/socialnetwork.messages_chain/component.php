<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("socialnetwork"))
{
    ShowError(GetMessage("SONET_MODULE_NOT_INSTALL"));
    return;
}

$arParams["USER_ID"] = $USER->GetID();


$arNavParams = false;
$page = (int)$arParams["PAGE"];
if($page > 0)
    $arNavParams["iNumPage"] = $page;

$arParams["PAGE_SIZE"] = (int)$arParams["PAGE_SIZE"];
if ($arParams["PAGE_SIZE"] > 0)
    $arNavParams["nPageSize"] = $arParams["PAGE_SIZE"];

$arParams["PATH_TO_SMILE"] = trim($arParams["PATH_TO_SMILE"]);

if (!$GLOBALS["USER"]->IsAuthorized())
{	
	$arResult["NEED_AUTH"] = "Y";
} else
{

	if ($arParams["SET_TITLE"]=="Y")
		$APPLICATION->SetTitle("���������");

        $parser = new CSocNetTextParser(LANGUAGE_ID, $arParams["PATH_TO_SMILE"]);
	$arResult["Events"] = false;

	$strSql = "SELECT  `ID`, `FROM_USER_ID`, `TO_USER_ID`, `TITLE`, `MESSAGE`, `DATE_CREATE`,
                            `DATE_VIEW`, `MESSAGE_TYPE`, `FROM_DELETED`, `TO_DELETED`, `SEND_MAIL`, `EMAIL_TEMPLATE`, `IS_LOG`,
                            LEAST(`FROM_USER_ID`, `TO_USER_ID`) AS `LEAST_VALUE`, 
                            GREATEST(`FROM_USER_ID`, `TO_USER_ID`) AS `GREATEST_VALUE`,
                            MAX(`ID`) AS `MAX_MESS_ID` 
                    FROM `b_sonet_messages`";
        $strSql .= "WHERE ((`FROM_USER_ID` = {$DB->ForSql($arParams["USER_ID"])} AND `FROM_DELETED` LIKE 'N') OR (`TO_USER_ID` = {$DB->ForSql($arParams["USER_ID"])} AND `TO_DELETED` LIKE 'N'))";
        if($arParams["FROM"])
            $strSql .= " AND (`ID` > {$DB->ForSql($arParams["FROM"])})";
        $strSql .= "GROUP BY `LEAST_VALUE`, `GREATEST_VALUE`
                    ORDER BY `MAX_MESS_ID` DESC";
        
        $rsMes = $DB->Query($strSql, false, $err_mess.__LINE__);
        
        $messagesIDs = array();
        while($mess = $rsMes->Fetch())
            $messagesIDs[] = $mess["MAX_MESS_ID"];
        
        if (count($messagesIDs) > 0)
        {
            $res = CSocNetMessages::GetList(array("ID" => "DESC"), array("ID" => $messagesIDs), false, $arNavParams);
            while($event = $res->Fetch())
            {
                if($event["FROM_USER_ID"] == $arParams["USER_ID"])
                {
                    $partnerUserID = $event["TO_USER_ID"];
                    $event["MESSAGE_INCOME"] = false;//���������
                }
                else 
                {
                    $partnerUserID = $event["FROM_USER_ID"];
                    $event["MESSAGE_INCOME"] = true;//��������
                }

                // �������� ���������� � ��������
                $result = $USER->GetList(($by="personal_country"), ($order="desc"), array("ID" => $partnerUserID), array("SELECT" => array("PERSONAL_PHOTO", "NAME", "LAST_NAME", "UF_NICK_NAME")) );
                $event["PARTNER_INFO"] = $result->Fetch(); 
                
                /* �������� ���������� � ������������� ���������� */
                $resUnReadMes = CSocNetMessages::GetList(
                    array("ID" => "DESC"), 
                    array("FROM_USER_ID" => $partnerUserID, "DATE_VIEW" => "", "TO_USER_ID" => $arParams["USER_ID"], "MESSAGE_TYPE" => SONET_MESSAGE_PRIVATE, "TO_DELETED" => "N")
                );
                $event["UNREAD_MESSAGE"] = 0;
                while($mesUnRead = $resUnReadMes->GetNext())
                    $event["UNREAD_MESSAGE"]++;
                $event["UNREAD_MESSAGE_SELF"] = strlen($event["DATE_VIEW"]) ? false : true;
                /*$resUnReadMes = CSocNetMessages::GetList(
                    array("ID" => "DESC"), 
                    array("FROM_USER_ID" => $arParams["USER_ID"], "DATE_VIEW" => "", "TO_USER_ID" => $partnerUserID, "MESSAGE_TYPE" => SONET_MESSAGE_PRIVATE, "TO_DELETED" => "N")
                );
                $event["UNREAD_MESSAGE_SELF"] = 0;
                while($mesUnRead = $resUnReadMes->GetNext())
                    $event["UNREAD_MESSAGE_SELF"] = $mesUnRead;*/
                
                
                if($event["PARTNER_INFO"]["PERSONAL_PHOTO"] > 0)
                    $event["PARTNER_INFO"]["PERSONAL_PHOTO"] = CFile::GetFileArray($event["PARTNER_INFO"]["PERSONAL_PHOTO"]);
                if(strlen($event["PARTNER_INFO"]["UF_NICK_NAME"]) > 0)
                    $event["PARTNER_INFO"]["FORMATED_NAME"] = $event["PARTNER_INFO"]["UF_NICK_NAME"];
                else
                    $event["PARTNER_INFO"]["FORMATED_NAME"] = $event["PARTNER_INFO"]["NAME"] ." ". $event["PARTNER_INFO"]["LAST_NAME"];
                
                $strTime = GetMyTimeIntervalInSec(strtotime($event["DATE_CREATE"]));
                $event["TODAY"] = FormatDate("d", $strTime) == date("d") ? "Y" : "N";
                if($event["TODAY"] == "Y")
                    $event["DATE_CREATE"] = FormatDate("H:i:s", $strTime);
                else 
                    $event["DATE_CREATE"] = FormatDate("M, d H:i:s", $strTime);
                
                $arResult["Events"][] = $event;
            }
            
            $arResult["NAV_STRING"] = $res->GetPageNavString("", "news_more", false);
        }
	
        
        $arResult["USER"] = $USER->GetByID($USER->GetID())->Fetch();
        
}

//FB::info($arResult,'$arResult');
$this->IncludeComponentTemplate();
?>