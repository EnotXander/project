<?php

class IncorrectSiteTest implements TestInterface
{
    const SITE_PROPERTY_NAME = 'URL';
    const INCORRECT_SITE = 'http://';

    /**
     * @param Company $company
     * @return null|TestResult
     */
    public function testCompany(Company $company)
    {
        $sites = $company->getPropertyValue(self::SITE_PROPERTY_NAME);

        if (is_array($sites)) {
            foreach ($sites as $site) {
                if ($site == self::INCORRECT_SITE) {
                    $testResult =  new TestResult();

                    $testResult
                        ->setCompany($company)
                        ->setTest($this)
                        ->setMessage("�������� {$company->getLinkAdmin()} - ������������ ����: {$site}")
                    ;

                    return $testResult;
                }
            }
        }

        return null;
    }
} 