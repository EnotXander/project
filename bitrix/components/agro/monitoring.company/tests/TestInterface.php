<?php

//require_once '../company/Company.php';
//require_once '../test_result/TestResult.php';

interface TestInterface
{
    /**
     * @param Company $company
     * @return TestResult
     */
    public function testCompany(Company $company);
} 