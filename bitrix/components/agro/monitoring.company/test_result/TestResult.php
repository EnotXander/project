<?php

//require_once '../company/Company.php';
//require_once '../test_result/TestResult.php';

class TestResult
{
    /** @var Company */
    private $company;

    /** @var string */
    private $message;

    /** @var TestInterface */
    private $test;

    /**
     * @param Company $company
     * @return $this
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param TestInterface $test
     * @return $this
     */
    public function setTest(TestInterface $test)
    {
        $this->test = $test;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }
} 