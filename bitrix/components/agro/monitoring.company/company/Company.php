<?php

class Company
{
    /**
     * @var _CIBElement
     */
    private $element;

    /**
     * @var array
     */
    private $fields;

    /**
     * @var array
     */
    private $properties;

    /**
     * @param _CIBElement $obCompany
     */
    public function __construct(_CIBElement $obCompany)
    {
        $this->element = $obCompany;
        $this->fields = array();
        $this->properties = array();
    }

    /**
     * @param string $fieldCode
     * @return mixed
     */
    public function getField($fieldCode)
    {
        if (!count($this->fields)) {
            $this->fields = $this->element->getFields();
        }

        return $this->fields[$fieldCode];
    }

    /**
     * @param string $propertyCode
     * @return mixed
     */
    public function getPropertyValue($propertyCode)
    {
        $this->loadProperty($propertyCode);

        return $this->properties[$propertyCode]['VALUE'];
    }

    public function getLinkAdmin()
    {
        $name = $this->getField('NAME');
        $id = $this->getField('ID');
        $iblockId = $this->getField('IBLOCK_ID');
        $iblockType = $this->getField('IBLOCK_TYPE_ID');
        $link = "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID={$iblockId}&type={$iblockType}&ID={$id}";

        return '<a href="'.$link.'" target="_blanc">'.$name.'</a>';
    }

    public function getAllFields()
    {
        if (!count($this->fields)) {
            $this->fields = $this->element->getFields();
        }

        return $this->fields;
    }

    /**
     * @param string $propertyCode
     */
    private function loadProperty($propertyCode)
    {
        if (!isset($this->properties[$propertyCode])) {
            $this->properties[$propertyCode] = $this->element->getProperty($propertyCode);
        }
    }
} 