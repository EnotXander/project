<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

foreach($arResult["ITEMS"] as $elementKey => $element)
{
   if(!strlen($arItem["PREVIEW_PICTURE"]["SRC"]))
   {
      if($element["PREVIEW_PICTURE"])
      {
         $FILE = $element["PREVIEW_PICTURE"];
         if (is_array($FILE))
         {                  
            $arFilterImg = array();
            $arFileTmp = CFile::ResizeImageGet(
                $FILE,
                array("width" => $arParams["THUMB_WIDTH"], "height" => $arParams["THUMB_HEIGHT"]),
                BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                true, 
                $arFilterImg
            );

            $FILE = array(
                'SRC' => $arFileTmp["src"],
                'WIDTH' => $arFileTmp["width"],
                'HEIGHT' => $arFileTmp["height"],
                'DESCRIPTION' => strlen($FILE["DESCRIPTION"]) > 0 ? $FILE : $element["NAME"]
            );

            $element["PREVIEW_PICTURE"] = $FILE;
         }
      }
      else
      {
         $FILE = array(
                'SRC' => "/images/search_placeholder.png",
                'WIDTH' => 48,
                'HEIGHT' => 42,
                'DESCRIPTION' => $element["NAME"]
            );
         $element["PREVIEW_PICTURE"] = $FILE;
      }
      $arResult["ITEMS"][$elementKey] = $element;
   }
}
?>
