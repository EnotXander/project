<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//printAdmin($arResult);
if(is_array($arResult["ITEMS"]) && count($arResult["ITEMS"]) > 0):?>
   <div class="mc_block">
      <div class="news_list">
         <?foreach($arResult["ITEMS"] as $arItem):?>
            <div class="item_row">
               <div class="image_box" style="display: inline-block; text-align: center;">
                  <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                     <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="preview_picture" border="0" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                     <?else:?>
                        <img class="preview_picture" border="0" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" />
                     <?endif;?>
                       
                  <?endif?>
                  <? // PrintObject($arItem["PREVIEW_PICTURE"]);?>
               </div>
               <div class="row">
                  <table width="100%">
                  <tr>
                     <td width="50%">
                        <div class="title_c"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                        <p><?=$arItem["PREVIEW_TEXT"];?></p>
                     </td>
                     <?if($arItem['DISPLAY_PROPERTIES']['COST']['VALUE']):?>
                        <td>
                           <div class="title"><?=$arItem['DISPLAY_PROPERTIES']['COST']['VALUE']?><?=strlen($arItem['PROPERTIES']['CURRENCY']['VALUE']) ? " ".$arItem['PROPERTIES']['CURRENCY']['VALUE'] : ""?></div>
                        </td>
                     <?else:?>
                        <td>
                           <div class="title">���� ���������</div> 
                        </td>
                     <?endif;?>
                  </tr>
                  </table>
               </div>
            </div><!--item_row-->
         <?endforeach;?>
      </div>
   </div>

   <div class="paging mar_20_bot">
      <div class="show_col">
         <form method="get" name="bot_f<?=$arParams['IBLOCK_ID']?>">
            <span class="meta">���������� ��:</span>
            <select name="per_page" onchange="document.bot_f<?=$arParams['IBLOCK_ID']?>.submit()">
               <option value="20" <?if ($arParams['PAGE_ELEMENT_COUNT'] == 20) echo "selected";?>>20</option>
               <option value="50" <?if ($arParams['PAGE_ELEMENT_COUNT'] == 50) echo "selected";?>>50</option>
               <option value="100" <?if ($arParams['PAGE_ELEMENT_COUNT'] == 100) echo "selected";?>>100</option>
            </select>
         </form>
      </div>
      <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
         <?=$arResult["NAV_STRING"]?>
      <?endif;?>
   </div>
<?endif;