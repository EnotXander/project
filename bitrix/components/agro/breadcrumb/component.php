<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//Params
$arParams["START_FROM"] = (isset($arParams["START_FROM"]) && intval($arParams["START_FROM"]) > 0 ? intval($arParams["START_FROM"]) : 0);
$arParams["PATH"] = (isset($arParams["PATH"]) && strlen($arParams["PATH"]) > 0 ? htmlspecialcharsbx($arParams["PATH"]) : false);
$arParams["SITE_ID"] = (isset($arParams["SITE_ID"]) && strlen($arParams["SITE_ID"]) == 2 ? htmlspecialcharsbx($arParams["SITE_ID"]) : false);
if ($arParams["SITE_ID"] === false)
	$path = $arParams["PATH"];
else
	$path = Array($arParams["SITE_ID"], $arParams["PATH"]);

//main
if (!CModule::IncludeModule("iblock")){
	return;
}

$arResult[] = array('LINK'=>'/', 'TITLE'=>'�������');

$arResult[] = array('LINK'=>'/blogs/', 'TITLE'=>'�����');

$filterElem = array();
if ($_REQUEST["id"]) $filterElem['ID'] = $_REQUEST["id"];
if ($_REQUEST["ELEMENT_CODE"]) $filterElem['CODE'] = $_REQUEST['ELEMENT_CODE'];
if ($filterElem){
	$resElem = CIBlockElement::GetList(array(),$filterElem);
	$arrElem = $resElem->GetNext();
	//$resSect = CIBlockSection::GetByID($arrElem['IBLOCK_SECTION_ID']);
	$resSect = CIBlockSection::GetList(array(), array(
		'IBLOCK_ID' => '50',
		'ID' => $arrElem['IBLOCK_SECTION_ID']
	), false, array('ID','NAME','SECTION_PAGE_URL','UF_USER_ID'));
	$arrSect = $resSect->GetNext();
	if ($arrSect['SECTION_PAGE_URL']){
		$arrSect['SECTION_PAGE_URL'] = str_replace('#UF_USER_ID#', $arrSect['UF_USER_ID'], $arrSect['SECTION_PAGE_URL']);
	}
	$arResult[] = array('LINK'=>$arrSect['SECTION_PAGE_URL'], 'TITLE'=>$arrSect['NAME']);

	$arResult[] = array('TITLE'=>$arrElem['NAME']);
}

$this->IncludeComponentTemplate();
?>