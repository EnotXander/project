<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$APPLICATION->SetTitle("���������� �������� �� �����������������");

//�������� ����
$arAccessGroups = array(1, 14);//�����, ��������
$arUserGroups = $USER->GetUserGroup($USER->GetID());
global $USER;
if($USER->IsAuthorized())
{
    $redirect = true;
    foreach($arAccessGroups as $group)
    {
        if(in_array($group, $arUserGroups))
            $redirect = false;
    }
    if($redirect)
        LocalRedirect("/");
} else {
    LocalRedirect("/");
}


if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
$firmArr = array();
$fullCompaniesCnt = 0;


//��������� �������
$arFilter = array(
    "IBLOCK_ID" => 27,
    "!PROPERTY_FIRM" => false
);
$res = $objElement->GetList(array(), $arFilter, array("PROPERTY_FIRM"));

$fullTovarsCnt = 0;
while($element = $res->GetNext())
{
    if (!isset($firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]]))
    {
        $fullCompaniesCnt++;
        $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] = 0;
    }
    $fullTovarsCnt += $element['CNT'];
    $firmArr[27][$element["PROPERTY_FIRM_VALUE"]] = $element['CNT'];
    $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] += $element['CNT'];
}


//��������� �����
$arFilter = array(
    "IBLOCK_ID" => 28,
    "!PROPERTY_FIRM" => false
);
$res = $objElement->GetList(array(), $arFilter, array("PROPERTY_FIRM"));

$fullServicesCnt = 0;
while($element = $res->GetNext())
{
    if (!isset($firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]]))
    {
        $fullCompaniesCnt++;
        $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] = 0;
    }
    $fullServicesCnt += $element['CNT'];
    $firmArr[28][$element["PROPERTY_FIRM_VALUE"]] = $element['CNT'];
    $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] += $element['CNT'];
}


//��������� ��������
$arFilter = array(
    "IBLOCK_ID" => 17,
    "ID" => array_keys($firmArr['ALL']),
    "PROPERTY_REMOVE_REL" => false
);
$res = $objElement->GetList(
    array("NAME"=>"ASC", "ID"=>"ASC"),
    $arFilter,
    false,
    false,
    array('ID', 'IBLOCK_ID', 'PROPERTY_TARIF', 'PROPERTY_MANAGER', 'DETAIL_PAGE_URL')
);

$fullTovarsActiveLinkCnt = 0;
$arCompanies = array();
$arTarifs = array();
$arManagers = array();
while($element = $res->GetNext())
{
    $arCompanies[$element['ID']] = array(
        'ID' => $element['ID'],
        'NAME' => $element['NAME'],
        'LINK' => $element['DETAIL_PAGE_URL'],
        'PRODUCTS' => $firmArr[27][$element['ID']],
        'SERVICES' => $firmArr[28][$element['ID']],
        'TARIF' => $element['PROPERTY_TARIF_VALUE'],
        'MANAGER' => $element['PROPERTY_MANAGER_VALUE']
    );
    if ($element['PROPERTY_TARIF_VALUE']) {
        $arTarifs[$element['PROPERTY_TARIF_VALUE']] = $element['PROPERTY_TARIF_VALUE'];
    }
    if ($element['PROPERTY_MANAGER_VALUE']) {
        $arManagers[$element['PROPERTY_MANAGER_VALUE']] = $element['PROPERTY_MANAGER_VALUE'];
    }
    $fullTovarsActiveLinkCnt += $firmArr[27][$element['ID']];
    $fullServicesActiveLinkCnt += $firmArr[28][$element['ID']];
}

//PrintAdmin($arTarifs);
//��������� �������
$arFilter = array(
    "IBLOCK_ID" => 47,
    "ID" => $arTarifs,
);
$res = $objElement->GetList(
    array("NAME"=>"ASC", "ID"=>"ASC"),
    $arFilter
);

while($element = $res->GetNext())
{
    $arTarifs[$element['ID']] = $element;
}
//PrintAdmin($arTarifs);


//��������� ����������
$res = $USER->GetList(($by="personal_country"), ($order="desc"), array('ID' => implode('|', $arManagers)));

while($element = $res->GetNext())
{
    $arManagers[$element['ID']] = $element;
}


//������������ csv
$fp = fopen('company_list.csv', 'w');
foreach($arCompanies as $key=>$element)
{
    foreach ($element as $elKey=>$el) $element[$elKey] = str_replace('"', "", html_entity_decode($el));
    fputcsv($fp, $element, ";");
}
fclose($fp);

//������
$arResult['COMPANIES'] = $arCompanies;
$arResult['STAT']['fullTovarsActiveLinkCnt'] = $fullTovarsActiveLinkCnt;
$arResult['STAT']['fullServicesActiveLinkCnt'] = $fullServicesActiveLinkCnt;
$arResult['STAT']['fullTovarsCnt'] = $fullTovarsCnt;
$arResult['STAT']['fullServicesCnt'] = $fullServicesCnt;
$arResult['STAT']['fullCompaniesCnt'] = $fullCompaniesCnt;
$arResult['TARIFS'] = $arTarifs;
$arResult['MANAGERS'] = $arManagers;

$this->IncludeComponentTemplate();