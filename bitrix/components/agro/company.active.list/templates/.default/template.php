<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>


<h1>������ �������� � ��������� ��������</h1>

<div class="paging" style="margin-bottom: 10px;">
   <span>�������� � �������� � � �������� ������: <b><?=count($arResult['COMPANIES'])?></b>. <a href="company_list.csv">������� CSV</a></span>
</div>

<table class="simple-table" style="margin-right: 30px;">
    <thead>
    <tr>
        <th>ID</th>
        <th>��������</th>
        <th>�������</th>
        <th>�����</th>
        <th>�����</th>
        <th>��������</th>
    </tr>
    </thead>
    <tbody>
    <?foreach($arResult['COMPANIES'] as $arCompany):?>
        <tr>
            <td style="padding: 5px;"><?=$arCompany['ID']?></td>
            <td style="padding: 5px;"><a target="_blank" href="<?=$arCompany['LINK']?>"><?=$arCompany['NAME']?></a></td>
            <td style="padding: 5px;"><?=$arCompany['PRODUCTS']?></td>
            <td style="padding: 5px;"><?=$arCompany['SERVICES']?></td>
            <?if($arResult['TARIFS'][$arCompany['TARIF']]):?>
                <td style="padding: 5px;"><?=$arResult['TARIFS'][$arCompany['TARIF']]['NAME']?></td>
            <?else:?>
                <td style="padding: 5px;">-</td>
            <?endif;?>
            <?if($arResult['MANAGERS'][$arCompany['MANAGER']]):?>
                <td style="padding: 5px;"><?=$arResult['MANAGERS'][$arCompany['MANAGER']]['NAME'].' '.$arResult['MANAGERS'][$arCompany['MANAGER']]['LAST_NAME']?></td>
            <?else:?>
                <td style="padding: 5px;">-</td>
            <?endif;?>
        </tr>
    <?endforeach;?>
    </tbody>
</table>

<div class="paging" style="margin-bottom: 10px;">
    <div>����� �������, ����������� � �������� � �������� ������: <b><?=$arResult['STAT']['fullTovarsActiveLinkCnt']?></b></div>
    <div>����� �����, ����������� � �������� � �������� ������: <b><?=$arResult['STAT']['fullServicesActiveLinkCnt']?></b></div>
    <div>����� �������, ����������� � ����� ���������: <b><?=$arResult['STAT']['fullTovarsCnt']?></b></div>
    <div>����� �����, ����������� � ����� ���������: <b><?=$arResult['STAT']['fullServicesCnt']?></b></div>
    <div>����� �������� � ��������: <b><?=$arResult['STAT']['fullCompaniesCnt']?></b></div>
</div>