<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult = array();



if($arParams['COUNTER_ID'])
{
   $arResult['COUNTER_ID'] = $arParams['COUNTER_ID'];

    if ($arParams['COMPANY_ID'])
    {
        $arResult['COMPANY']['ID'] = $arParams['COMPANY_ID'];
    }
}
else
{
   if($arParams['COMPANY_ID'])
   {
      if (!CModule::IncludeModule("iblock")) die();
      $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 65, "ID" => $arParams['COMPANY_ID']), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_PHONE_CLICK_COUNT', 'PROPERTY_METRIKA_ID'));
      if ($element = $res->GetNext())
      {
         $arResult['COUNTER_ID'] = $element['PROPERTY_METRIKA_ID_VALUE'];
//PrintAdmin($element);
      }
   }
}

$arResult['METRIKA'] = YandexMetrika::getInstance(array(
	"ID" => METRIKA_CLIENT_ID_AGRO,
	"SECRET" => METRIKA_CLIENT_SECRET_AGRO,
	"USERNAME" => METRIKA_USERNAME_AGRO,
	"PASSWORD" => METRIKA_PASSWORD_AGRO
));

//PrintAdmin($arResult);
//PrintAdmin($arResult['METRIKA']->token);
//PrintAdmin($arResult['METRIKA']->error);

if(strlen($arResult['METRIKA']->token))
{
   $arResult['TOKEN'] = $arResult['METRIKA']->token;
}
else
{
   $arResult['ERROR'] = $arResult['METRIKA']->error;
}

$this->IncludeComponentTemplate();