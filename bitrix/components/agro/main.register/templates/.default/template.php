<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
   die();
?>

<div class="bx-auth">

   <? if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) && $arParams["AUTH_RESULT"]["TYPE"] === "OK"): ?>
      <p><? echo GetMessage("AUTH_EMAIL_SENT") ?></p>
   <? else: ?>
      <noindex>
         <form id="register-form" method="post" action="<?= $arResult["AUTH_URL"] ?>" name="bform">
            <?
            if (strlen($arResult["BACKURL"]) > 0)
            {
               ?>
               <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
               <?
            }
            ?>
         <!--<input type="hidden" name="AUTH_FORM" value="Y" />
         <input type="hidden" name="TYPE" value="REGISTRATION" />-->

            <table class="data-table bx-registration-table">
               <thead>
                  <tr>
                     <td colspan="3" style="color:red;"><b>��������������� ������</b></td>
                  </tr>
                  <tr>
                     <td colspan="3">����, ���������� <span style="color:red">*</span>, ������������ ��� ����������.<br><br></td>
                  </tr>
               </thead>
               <tbody class="bodyground">
                  <tr class="<?= strlen($arResult["VALID"]["EMAIL"]) ? 'error' : '' ?>">
                     <td><span class="starrequired" style="color:red">*</span>��� ����� ����������� �����:</td>
                     <td>
                        <input id="register-form-email" type="text" name="REGISTER[EMAIL]" maxlength="255" value="<?= $arResult["VALUES"]["EMAIL"] ?>" class="bx-auth-input" />
                        <div id="register-errortext-email" class="errortext"><?= $arResult["VALID"]["EMAIL"] ?></div>
                     </td>
                     <td rowspan="2" class="bordered">
                        <div>
                           <p>��� ����� ����� �������������� <b>��� ����� �� ����</b></p>
                           <br>
                           <p>�� ������� �������� �� ���������� ����� ������ ��������� ����������:<br>
                              - ��������� �� ��������� ��������� ���� �������.<br>
                              - ����������� � ������������ ��� ����� ������� � �������.<br>
                              - �������������� �������� ������� EnergoBelarus.by.
                           </p>
                        </div>
                     </td>
                     <td rowspan="3" class="register-cell-info" style="padding: 13px 15px 12px 15px !important;">
                        <b>����� ����� �����������?</b><br />
                        ����� ����������� �� ������� ������������ �������� ������� ��������-�������.<br /><br />
                        <b>�������� ��������!</b><br />
                        ���������� ������ ����������� ����������.
                        ��� ���������� � ��� �������� ���������������� � ������������ ��� "������� ������� �����" ������������� � ����� ��������� �������� � ������������� ������ � ��������� ������� EnergoBelarus.by.
                     </td>
                  </tr>
                  <!--<tr>
                          <td><span class="starrequired" style="color:red;">*</span><?= GetMessage("AUTH_LOGIN_MIN") ?></td>
                          <td><input id="register-form-login" type="text" name="REGISTER[LOGIN]" maxlength="50" value="<?= $arResult["USER_LOGIN"] ?>" class="bx-auth-input" /></td>
                          
                  </tr>-->
                  <tr class="<?= strlen($arResult["VALID"]["PASSWORD"]) ? 'error' : '' ?>">
                     <td><span class="starrequired" style="color:red">*</span><?= GetMessage("AUTH_PASSWORD_REQ") ?></td>
                     <td>
                        <input id="register-form-password" type="password" name="REGISTER[PASSWORD]" maxlength="50" value="" class="bx-auth-input" />
                        <div id="register-errortext-password" class="errortext"><?= $arResult["VALID"]["PASSWORD"] ?></div>
                     </td>

                  </tr>

                  <tr class="auth-last <?= strlen($arResult["VALID"]["CONFIRM_PASSWORD"]) ? 'error' : '' ?>">
                     <td><span class="starrequired" style="color:red">*</span>��������� ������:</td>
                     <td>
                        <input id="register-form-confirm" type="password" name="REGISTER[CONFIRM_PASSWORD]" maxlength="50" value="" class="bx-auth-input" />
                        <div id="register-errortext-confirm" class="errortext"><?= $arResult["VALID"]["CONFIRM_PASSWORD"] ?></div>
                     </td>
                     <td class="bordered">
                        <div>
                           <p>
                              <b>�����:</b> ��� ����� ������ ���� ������� ��������� ��������� � �������� �����. ����������� ����� ������ - 6 ��������. 
                           </p>
                        </div>
                     </td>
                  </tr>
                  <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
                  <tr class="<?= strlen($arResult["VALID"]["NAME"]) ? 'error' : '' ?>">
                     <td><span class="starrequired" style="color:red;">*</span><?= GetMessage("AUTH_NAME") ?></td>
                     <td>
                        <input id="register-form-name" type="text" name="REGISTER[NAME]" maxlength="50" value="<?= $arResult["VALUES"]["NAME"] ?>" class="bx-auth-input" />
                        <div id="register-errortext-name" class="errortext"><?= $arResult["VALID"]["NAME"] ?></div>
                     </td>
                     <td rowspan="3" class="bordered"><div>
                           <p>
                              <b>������ ������</b><br>��� ������ �������� ��� ������������� ��������� �������� ���������� ��� ��� �������������� ����������. 
                              ��� � ������� ������������ ����� ��� �������� ����� � � ��������� ������ �������. 
                           </p>
                        </div>
                     </td>
                  </tr>
                  <tr class="auth-last <?= strlen($arResult["VALID"]["LAST_NAME"]) ? 'error' : '' ?>">
                     <td><span class="starrequired" style="color:red;">*</span><?= GetMessage("AUTH_LAST_NAME") ?></td>
                     <td>
                        <input id="register-form-lastname" type="text" name="REGISTER[LAST_NAME]" maxlength="50" value="<?= $arResult["VALUES"]["LAST_NAME"] ?>" class="bx-auth-input" />
                        <div id="register-errortext-lastname" class="errortext"><?= $arResult["VALID"]["LAST_NAME"] ?></div>
                     </td>

                  </tr>
                  <tr class="auth-last <?= strlen($arResult["VALID"]["PLACE"]) ? 'error' : '' ?>">
                     <td><span class="starrequired" style="color:red;">*</span><?= GetMessage("AUTH_PLACE") ?></td>
                     <td>
                        <select class="select_location js_country_sel" name="REGISTER[UF_COUNTRY]">
                           <option value="0">�������� ������</option>
                           <? foreach ($arResult["PLACE"]["COUNTRY"] as $key => $val): ?>
                              <option value = "<?= $val["ID"] ?>" <?= $val["ID"]==$arResult["VALUES"]["UF_COUNTRY"] ? 'selected' : '' ?> ><?= $val["NAME"] ?></option>
                           <? endforeach; ?>
                        </select>

                        <select<?=(!$arResult["VALUES"]["UF_REGION"]) ? " disabled" : ""?> class="select_location js_region_sel" name="REGISTER[UF_REGION]">
                           <option value="0">�������� ������</option>
                           <?if($arResult["VALUES"]["UF_COUNTRY"]):?>
                              <? foreach ($arResult["PLACE"]["REGION"] as $key => $val): ?>
                                 <option value = "<?= $val["ID"] ?>" <?= $val["ID"]==$arResult["VALUES"]["UF_REGION"] ? 'selected' : '' ?> ><?= $val["NAME"] ?></option>
                              <? endforeach; ?>
                           <?endif;?>
                        </select>

                        <select<?=(!$arResult["VALUES"]["UF_CITY"]) ? " disabled" : ""?> class="select_location js_city_sel" name="REGISTER[UF_CITY]">
                           <option value="0">�������� �����</option>
                           <?if($arResult["VALUES"]["UF_REGION"]):?>
                              <? foreach ($arResult["PLACE"]["CITY"] as $key => $val): ?>
                                 <option value = "<?= $val["ID"] ?>" <?= $val["ID"]==$arResult["VALUES"]["UF_CITY"] ? 'selected' : '' ?> ><?= $val["NAME"] ?></option>
                              <? endforeach; ?>
                           <?endif;?>
                        </select>

                        <div id="register-errortext-place" class="errortext"><?= $arResult["VALID"]["PLACE"] ?></div>
                     </td>

                  </tr>

                  <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
                  <tr class="auth-last <?= strlen($arResult["VALID"]["UF_COMPANY"]) ? 'error' : '' ?>">
                     <td><?= GetMessage("AUTH_UF_COMPANY") ?></td>
                     <td>
                        <select class="select_location js_company_sel" name="REGISTER[UF_COMPANY]">
                           <option value="0">�������� ��������</option>
                           <? foreach ($arResult["COMPANY"] as $key => $val): ?>
                              <option value = "<?= $val["ID"] ?>" <?= $val["ID"]==$arResult["VALUES"]["UF_COMPANY"] ? 'selected' : '' ?> ><?= $val["NAME"] ?></option>
                           <? endforeach; ?>
                        </select>
                     </td>
                     <td rowspan="2" class="bordered"><div>
                           <p>
                              <b>���� ��������</b><br>�������� �� ������ ��������, ����������� ������� �� ��������� � ������� ���������.
                              ���� ����� �������� ��� � ������, �� ������ �������� ������ �� ���������� � <a href="/company/">����������</a> ����� �����������.
                           </p>
                        </div>
                     </td>
                  </tr>
                  <tr class="auth-last <?= strlen($arResult["VALID"]["WORK_POSITION"]) ? 'error' : '' ?>">
                     <td><?= GetMessage("AUTH_WORK_POSITION") ?></td>
                     <td>
                        <input id="register-form-position" type="text" name="REGISTER[WORK_POSITION]" maxlength="50" value="<?= $arResult["VALUES"]["WORK_POSITION"] ?>" class="bx-auth-input" />
                        <div id="register-errortext-position" class="errortext"><?= $arResult["VALID"]["WORK_POSITION"] ?></div>
                     </td>

                  </tr>

                  <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>		

                  <? // ********************* User properties ***************************************************?>
                  <? if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y"): ?>
                     <tr><td colspan="2"><?= strLen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB") ?></td></tr>
                     <? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField): ?>
                        <tr><td><? if ($arUserField["MANDATORY"] == "Y"): ?><span class="required">*</span><? endif; ?>
                              <?= $arUserField["EDIT_FORM_LABEL"] ?>:</td><td>
                              <?
                              $APPLICATION->IncludeComponent(
                                      "bitrix:system.field.edit", $arUserField["USER_TYPE"]["USER_TYPE_ID"], array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "bform"), null, array("HIDE_ICONS" => "Y"));
                              ?></td></tr>
                        <? endforeach; ?>
                     <? endif; ?>
                     <?
                     // ******************** /User properties ***************************************************

                     /* CAPTCHA */
                     if ($arResult["USE_CAPTCHA"] == "Y")
                     {
                        ?>

                     <tr>
                        <td></td>
                        <td>
                           <input class="captcha_sid_register" type="hidden" name="captcha_sid_register" value="<?= $arResult["CAPTCHA_CODE"] ?>" />
                           <div class="captcha"> <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" alt="CAPTCHA" /></div>
                        </td>
                        <td rowspan="2" class="bordered"><b><?= GetMessage("CAPTCHA_REGF_TITLE") ?></b><p>
                              ���� �� �� ������ ��������� �������, ������� �������� ��� ��������� ����� "������!", <a href="javascript:void(0)" class="captchab">������� �����</a> � �������� ���������.	
                           </p></td>
                     </tr>
                     <tr class="auth-last <?= strlen($arResult["VALID"]["CAPTCHA"]) ? 'error' : '' ?>">
                        <td><span class="starrequired"  style="color:red;">*</span>��� � �����������:</td>
                        <td>
                           <input id="register-form-captcha" type="text" autocomplete="off" name="captcha_word_register" maxlength="50" value="" />
                           <div id="register-errortext-captcha" class="errortext"><?= $arResult["VALID"]["CAPTCHA"] ?></div>
                        </td>
                     </tr>
                     <?
                  }
                  /* CAPTCHA */
                  ?>
                  <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
               </tbody>
               <tfoot>
                  <tr>
                     <td></td><td></td>
                     <td>
                        <input type="submit" id="register-form-submit" name="register_submit_button" value="<?= GetMessage("AUTH_REGISTER") ?>" style="display: none;"/>
                        <a class="btns_big" style="float: right;" href="javascript:void(0);" onclick="$('#register-form').submit();"><i></i>������������������</a>
                        <div style="margin-top: 20px;">
                           <input id="register-form-contract" type="checkbox" style="width: auto;" />
                           <span>� ��������(�) � </span>
                           <a href="/public-contract/user.doc">��������� �������.</a>
                        </div>
                     </td>
                  </tr>
               </tfoot>
            </table>
            <!--<p><? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>
            <p><span class="starrequired">*</span><?= GetMessage("AUTH_REQ") ?></p>-->


         </form>
      </noindex>
   <? endif ?>
</div>
<?//PrintObject($_REQUEST);
//PrintObject($arResult);?>