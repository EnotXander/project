<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arParams["USE_CAPTCHA"] = (($arParams["USE_CAPTCHA"] != "N" && !$USER->IsAuthorized()) ? "Y" : "N");
$arParams["EVENT_NAME"] = trim($arParams["EVENT_NAME"]);
if (strlen($arParams["EVENT_NAME"]) <= 0)
   $arParams["EVENT_NAME"] = "FEEDBACK_FORM";
$arParams["EMAIL_TO"] = trim($arParams["EMAIL_TO"]);
if (strlen($arParams["EMAIL_TO"]) <= 0)
   $arParams["EMAIL_TO"] = COption::GetOptionString("main", "email_from");
$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if (strlen($arParams["OK_TEXT"]) <= 0)
   $arParams["OK_TEXT"] = GetMessage("MF_OK_MESSAGE");

if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();

if ($_SERVER["REQUEST_METHOD"] == "POST" && strlen($_POST["submitFastOrder"]) > 0) {
   if (check_bitrix_sessid()) {
      if (empty($arParams["REQUIRED_FIELDS"]) || !in_array("NONE", $arParams["REQUIRED_FIELDS"])) 
      {
         if ((empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])) && !strlen($_POST["NAME"]) > 0)
            $arResult["ERROR_MESSAGE"]["NAME"] = GetMessage("MF_REQ_NAME");         
         if (!strlen($_POST["PHONE"]) > 0)
            $arResult["ERROR_MESSAGE"]["PHONE"] = GetMessage("MF_REQ_PHONE");         
         if (!strlen($_POST["ADDRESS"]) > 0)
            $arResult["ERROR_MESSAGE"]["ADDRESS"] = GetMessage("MF_REQ_ADDRESS");         
         if (!(int)$_POST["PRODUCT_ID"] > 0)
            $arResult["ERROR_MESSAGE"]["PRODUCT_ID"] = GetMessage("MF_REQ_PRODUCT_ID");
         if (!(int)$_POST["QUANTITY"] > 0)
            $arResult["ERROR_MESSAGE"]["QUANTITY"] = GetMessage("MF_REQ_QUANTITY");
         if ((empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])) && strlen($_POST["MESSAGE"]) <= 3)
            $arResult["ERROR_MESSAGE"]["MESSAGE"] = GetMessage("MF_REQ_MESSAGE");         
      }
      if (strlen($_POST["EMAIL"]) > 1 && !check_email($_POST["EMAIL"]))
         $arResult["ERROR_MESSAGE"]["EMAIL"] = GetMessage("MF_EMAIL_NOT_VALID");

      if (empty($arResult)) 
      {
         $prop = array();
         $arFields = Array(
            "EMAIL" => $_POST["EMAIL"],
            "NAME" => $_POST["NAME"],
            "PHONE" => $_POST["PHONE"],
            "ADDRESS" => $_POST["ADDRESS"],
            "MESSAGE" => $_POST["MESSAGE"]     
         );
          $dateTime = new DateTime();
          $arFieldsToCompany = Array(
              'LK_LINK' => $arParams['LK_LINK'],
              'DATE' => $dateTime->format('d.m.Y H:i:s')
          );


         $resEl = $objElement->GetByID((int)$_POST["PRODUCT_ID"]);
         if($obj = $resEl->GetNextElement())
         {
            $el = $obj->GetFields();     
            $arFields["PRODUCT_LINK"] = "<a href='http://".SITE_SERVER_NAME."{$el["DETAIL_PAGE_URL"]}'>{$el["NAME"]} [{$el["ID"]}] ({$_POST["QUANTITY"]} ��.)</a>";
             $arFieldsToCompany['PRODUCT_LINK'] = $arFields["PRODUCT_LINK"];
            $company = $obj->GetProperty("FIRM");
            if($company["VALUE"] > 0)
            {               
               $resEl = $objElement->GetByID($company["VALUE"]);
               if($obj = $resEl->GetNextElement())
               {
                  $company = $obj->GetFields();   
                  $company["PROP"] = $obj->GetProperties();   
                  
                  $prop["COMPANY"] = $company["ID"];

                   $arFieldsToCompany['NAME'] = $company['NAME'];

                     if($company["PROP"]["USER"]["VALUE"] > 0)
                     {
                        $resUser = $USER->GetByID($company["PROP"]["USER"]["VALUE"]);
                        if($userInfo = $resUser->GetNext())
                        {
                            $arFieldsToCompany['NAME'] = $userInfo["NAME"];
                            $arFieldsToCompany['EMAIL_COMPANY'] = $userInfo["EMAIL"];
                        }
                     }

                   if(strlen($company["PROP"]["EMAIL_REQUEST"]["VALUE"]) > 0) {
                       $arFieldsToCompany['EMAIL_COMPANY'] = $company["PROP"]["EMAIL_REQUEST"]["VALUE"];
                   }
               }
            }
         }

         //������ �������� �� email ��� �������� (��� �������������)
         if(strlen($arFieldsToCompany['EMAIL_COMPANY']) > 0) {
             CEvent::Send($arParams["EVENT_NAME_TO_COMPANY"], SITE_ID, $arFieldsToCompany);
         }

         //������ ����������
         if(strlen($arFields["EMAIL"]) > 0)
         {
            if (!empty($arParams["EVENT_MESSAGE_ID"])) {
               foreach ($arParams["EVENT_MESSAGE_ID"] as $v) {
                  if (IntVal($v) > 0) {
                     CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));
                  }
                }
            } else {
               CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields);
            }
         }
         
         $prop["PHONE"] = $_POST["PHONE"];
         $prop["NAME"] = $_POST["NAME"];
         $prop["EMAIL"] = $_POST["EMAIL"];
         $prop["ADDRESS"] = $_POST["ADDRESS"];
         $prop["PRODUCT"] = (int)$_POST["PRODUCT_ID"];
         $prop["QUANTITY"] = (int)$_POST["QUANTITY"];
         
         $time = time();
         $arFiedsToSave = array(
             "NAME" => "������ �� ����� �� ".ConvertTimeStamp($time, "FULL"),
             "ACTIVE" => "Y",
             "IBLOCK_ID" => 46,
             "DETAIL_TEXT" => $_POST["MESSAGE"],
             "DETAIL_TEXT_TYPE" => "html",
             "PROPERTY_VALUES" => $prop,
             "DATE_ACTIVE_FROM" => ConvertTimeStamp($time, "FULL"),             
         );
         $objElement->Add($arFiedsToSave);
         
         $_SESSION["MF_EMAIL"] = htmlspecialcharsEx($_POST["EMAIL"]);
         $_SESSION["MF_PHONE"] = htmlspecialcharsEx($_POST["PHONE"]);
         $_SESSION["MF_NAME"] = htmlspecialcharsEx($_POST["NAME"]);
         $_SESSION["MF_ADDRESS"] = htmlspecialcharsEx($_POST["ADDRESS"]);
         $_SESSION["MF_PRODUCT_ID"] = htmlspecialcharsEx($_POST["PRODUCT_ID"]);
         $_SESSION["MF_MESSAGE"] = htmlspecialcharsEx($_POST["MESSAGE"]);
         $_SESSION["MF_QUANTITY"] = htmlspecialcharsEx($_POST["QUANTITY"]);
         
         LocalRedirect($APPLICATION->GetCurPageParam("success=Y&product_id={$prop["PRODUCT"]}", Array("success", "SECTION_CODE", "SECTION_CODE_2")));
      }

      $arResult["ADDRESS"] = htmlspecialcharsEx($_POST["ADDRESS"]);
      $arResult["AUTHOR_PHONE"] = htmlspecialcharsEx($_POST["PHONE"]);
      $arResult["AUTHOR_NAME"] = htmlspecialcharsEx($_POST["NAME"]);
      $arResult["AUTHOR_EMAIL"] = htmlspecialcharsEx($_POST["EMAIL"]);
      $arResult["MESSAGE"] = htmlspecialcharsEx($_POST["MESSAGE"]);
      $arResult["PRODUCT_ID"] = htmlspecialcharsEx($_POST["PRODUCT_ID"]);     
      $arResult["QUANTITY"] = htmlspecialcharsEx($_POST["QUANTITY"]);      
   } else
      $arResult["ERROR_MESSAGE"][] = GetMessage("MF_SESS_EXP");
} elseif ($_REQUEST["success"] == "Y")
{
   $arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
   $rsProduct = CIBlockElement::GetByID($_REQUEST["product_id"]);
   if($arProduct = $rsProduct->Fetch())
   {
      $arResult["PRODUCT"] = $arProduct;
   }
}

if (empty($arResult["ERROR_MESSAGE"])) 
{
   if ($USER->IsAuthorized()) 
   {
      global $USER_FIELD_MANAGER;
      $arResult["AUTHOR_INFO"] = $USER_FIELD_MANAGER->GetUserFields("USER", $USER->GetID(), "ru");
      $resUser = $USER->GetByID($USER->GetID());
      if($userInfo = $resUser->GetNext())
         $arResult["AUTHOR_INFO"] = array_merge($userInfo, $arResult["AUTHOR_INFO"]);
      
      if (strlen($arResult["AUTHOR_INFO"]["PHONE"]) > 0)
        $arResult["AUTHOR_PHONE"] = htmlspecialcharsEx($arResult["AUTHOR_INFO"]["PHONE"]);
      if (strlen($arResult["AUTHOR_INFO"]["NAME"]) > 0)
        $arResult["AUTHOR_NAME"] = htmlspecialcharsEx($arResult["AUTHOR_INFO"]["NAME"]);
      if (strlen($arResult["AUTHOR_INFO"]["EMAIL"]) > 0)
        $arResult["AUTHOR_EMAIL"] = htmlspecialcharsEx($arResult["AUTHOR_INFO"]["EMAIL"]);
   } else {
      if (strlen($_SESSION["MF_PHONE"]) > 0)
         $arResult["AUTHOR_PHONE"] = htmlspecialcharsEx($_SESSION["MF_PHONE"]);
      if (strlen($_SESSION["MF_NAME"]) > 0)
         $arResult["AUTHOR_NAME"] = htmlspecialcharsEx($_SESSION["MF_NAME"]);  
      if (strlen($_SESSION["MF_EMAIL"]) > 0)
         $arResult["AUTHOR_EMAIL"] = htmlspecialcharsEx($_SESSION["MF_EMAIL"]);  
      if (strlen($_SESSION["MF_ADDRESS"]) > 0)
         $arResult["AUTHOR_ADDRESS"] = htmlspecialcharsEx($_SESSION["MF_ADDRESS"]); 
   }
}

if ($arParams["USE_CAPTCHA"] == "Y")
   $arResult["capCode"] = htmlspecialchars($APPLICATION->CaptchaGetCode());

$this->IncludeComponentTemplate();