<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 


<? foreach ($arResult["COMPONENTS"] as $arComponent): ?>
   <div
       class="lazyload_component lazyload_component_<?= $arComponent["MD5"] ?>"
       data-post="<?= $arComponent["POST"] ?>"
       <?if (isset($arComponent["GET"])):?>data-get="<?= $arComponent["GET"] ?>"<?endif;?>
       >
      ��������...
   </div>
<? endforeach; ?>

<script>
    $(document).ready(function(){
        var $this = $('.lazyload_component_<?= $arComponent["MD5"] ?>'),
            getQuery = $this.attr("data-get"),
            url = '/_ajax/lazyload.php';

        if (getQuery) {
            url += '?' + getQuery;
        }

        $.ajax({
            type: "POST",
            url: url,
            data: $this.attr("data-post"),
            dataType: "json",
            success: function(data){
                if(!data.error){
                    $this.replaceWith(data.html);
                    <?= strlen($arParams["ONLOAD_SCRIPT"]) ? $arParams["ONLOAD_SCRIPT"] : ""?>
                }else{
                    $this.html("�� ������� �������� ����");
                }
            },
            error: function(){
                $this.html("�� ������� �������� ����");
            }
        });
    })
</script>