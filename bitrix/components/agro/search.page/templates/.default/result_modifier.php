<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arRibbon = array(
    "5" => array(
        "CLASS" => "rib_red",
        "TITLE" => "���",
        "IDD" => 5,
        "URL" => "/news/5/"
    ),
    "6" => array(
        "CLASS" => "rib_purple",
        "TITLE" => "���� ���",
        "IDD" => 6,
        "URL" => "/news/6/"
    ),
    "149" => array(
        "CLASS" => "rib_yellow",
        "TITLE" => "��������",
        "IDD" => 149,
        "URL" => "/news/149/"
    ),
    "152" => array(
        "CLASS" => "rib_blue",
        "TITLE" => "���",
        "IDD" => 152,
        "URL" => "/news/152/"
    ),
    "2695" => array(
        "CLASS" => "rib_green",
        "TITLE" => "��������",
        "IDD" => 2695,
        "URL" => "/news/2695/"
    )
);

$arRibbonKeys = array_keys($arRibbon);

foreach ($arResult["SEARCH"] as $arKey => $arItem)
{
	// ��������� "������ ����������� ��������"
	if ($arItem["REAL_ELEMENT"]['IBLOCK_ID'] == 49){
		unset($arResult["SEARCH"][$arKey]);
		continue;
	}
	// ��������� ������ ����
	if ($arItem['MODULE_ID'] == 'blog'){
		unset($arResult["SEARCH"][$arKey]);
		continue;
	}
	// ��������� ����������� � ������
	if ($arItem['MODULE_ID'] == 'forum' && strpos($arItem['URL'], 'communication/forum') !== false){
		unset($arResult["SEARCH"][$arKey]);
		continue;
	}
	
   $arItem = $arItem["REAL_ELEMENT"];
   $index = array_search($arItem["IBLOCK_SECTION_ID"], $arRibbonKeys);
   if($index !== false)
   {
      $arResult["SEARCH"][$arKey]["RIBBON"] = $arRibbon[$arRibbonKeys[$index]];
   }
   else
   {
      if($arResult["SEARCH"][$arKey]["MODULE_ID"] == "blog")
      {
         $arResult["SEARCH"][$arKey]["RIBBON"] = array(
              "CLASS" => "rib_default",
              "TITLE" => "�����",
              "URL" => "/blogs/"
            );
      }
      elseif($arResult["SEARCH"][$arKey]["MODULE_ID"] == "forum")
      {
         $arResult["SEARCH"][$arKey]["RIBBON"] = array(
              "CLASS" => "rib_default",
              "TITLE" => "������",
              "URL" => "/forum/"
            );
      }
      else
      {
         $rsSection = CIBlockSection::GetList(
				array("SORT"=>"ASC"),
				array(
					'ID' => $arItem["IBLOCK_SECTION_ID"],
					'IBLOCK_ID' => $arItem['IBLOCK_ID']
				),
				false,
				array('ID','NAME','SECTION_PAGE_URL','UF_USER_ID')
			);
         if($arSection = $rsSection->GetNext())
         {
            $arResult["SEARCH"][$arKey]["RIBBON"] = array(
              "CLASS" => "rib_default",
              "TITLE" => $arSection["NAME"],
              "IDD" => $arItem["IBLOCK_SECTION_ID"],
              "URL" => $arSection["SECTION_PAGE_URL"]
            );
            if($arResult["SEARCH"][$arKey]["REAL_ELEMENT"]["IBLOCK_ID"] == 3)
               $arResult["SEARCH"][$arKey]["RIBBON"]["URL"] .= $arResult["SEARCH"][$arKey]["REAL_ELEMENT"]["IBLOCK_SECTION_ID"]."/";
				// ������ ������ �� ������ �����
				if ($arResult["SEARCH"][$arKey]["REAL_ELEMENT"]["IBLOCK_ID"] == 50){
					$arResult["SEARCH"][$arKey]["RIBBON"]["URL"] = str_replace('#UF_USER_ID#', $arSection['UF_USER_ID'], $arResult["SEARCH"][$arKey]["RIBBON"]["URL"]);
				}
         }
      }
   }
   $arResult["SEARCH"][$arKey]["BODY_FORMATED"] = html_entity_decode(trim($arResult["SEARCH"][$arKey]["BODY_FORMATED"]));
	
}

//sort
$arResult["SORT"]["RELEV"] = $APPLICATION->GetCurPageParam("sort=rank", array("PAGEN_2", "sort"));
$arResult["SORT"]["DATE"] = $APPLICATION->GetCurPageParam("sort=date", array("PAGEN_2", "sort"));
if($_REQUEST['sort'] == "date")
{
   $arResult["SORT"]["TYPE"] = "date";
   $arResult["SORT"]["DATE"] = "javascript:void(0)";
}
else
{
   $arResult["SORT"]["TYPE"] = "rank";
   $arResult["SORT"]["RELEV"] = "javascript:void(0)";
}