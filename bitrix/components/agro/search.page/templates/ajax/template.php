<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$response = array();
if(count($arResult["SEARCH"]) > 0)
{
   foreach($arResult["SEARCH"] as $arItem)
   {      
      $json = array();
      $json['value'] = iconv("cp1251", "UTF-8", $arItem["BODY_FORMATED"]);
      
      $json['id'] = $arItem["REAL_ELEMENT"]["ID"];
      $json['name'] = iconv("cp1251", "UTF-8", $arItem["TITLE_FORMATED"]);
      $json['type'] = iconv("cp1251", "UTF-8", $arItem["BODY_FORMATED"]);
      $json['image'] = iconv("cp1251", "UTF-8", $arItem["REAL_ELEMENT"]["PREVIEW_PICTURE"]["SRC"]);
      $json['image_h'] = $arItem["REAL_ELEMENT"]["PREVIEW_PICTURE"]["HEIGHT"];
      $json['link'] = $arItem["REAL_ELEMENT"]["DETAIL_PAGE_URL"];
      $json['ribbon_class'] = $arItem["RIBBON"]["CLASS"];
      $json['ribbon_url'] = $arItem["RIBBON"]["URL"];
      $json['ribbon_title'] = iconv("cp1251", "UTF-8", $arItem["RIBBON"]["TITLE"]);
      if($arItem["REAL_ELEMENT"]["SHOW_DATE"])
      {
          $timestamp = BXToTimestamp($arItem["REAL_ELEMENT"]["DATE_ACTIVE_FROM"]);
          $dateFormat = date('d.m.Y H:i', $timestamp);
         $json['date'] = "\ ".$dateFormat;
      }
      else $json['date'] = "";
      $json['is_section'] = $arItem["REAL_ELEMENT"]["IS_SECTION"];
      if($arItem["REAL_ELEMENT"]["IS_SECTION"])
      {
         $sub = array();
         foreach($arItem["REAL_ELEMENT"]["SUB"] as $subItem)
         {
            $sub[] = array(
                "name" => iconv("cp1251", "UTF-8", $subItem["NAME"]),
                "url" => $subItem["URL"]
            );
         }
         $json['sub'] = $sub;
      }
      if(is_array($arItem["REAL_ELEMENT"]["FIRM"]))
      {
         $arItem["REAL_ELEMENT"]["FIRM"]["LOGO"]["DESCRIPTION"] = html_entity_decode(iconv("cp1251", "UTF-8", $arItem["REAL_ELEMENT"]["FIRM"]["LOGO"]["DESCRIPTION"]));
         $json['firm'] = array(
             "LINK" => $arItem["REAL_ELEMENT"]["FIRM"]["LINK"],
             "LOGO" => $arItem["REAL_ELEMENT"]["FIRM"]["LOGO"]
         );
      }
      $json['image_margin'] = $arParams["IMAGE_MARGIN"];
      $response[] = $json; 
   }
}
header("Content-type: application/json");
echo json_encode($response, JSON_ERROR_UTF8);