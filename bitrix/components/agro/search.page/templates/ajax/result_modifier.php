<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arRibbon = array(
    "5" => array(
        "CLASS" => "rib_red",
        "TITLE" => "���",
        "IDD" => 5,
        "URL" => "/news/5/"
    ),
    "6" => array(
        "CLASS" => "rib_purple",
        "TITLE" => "���� ���",
        "IDD" => 6,
        "URL" => "/news/6/"
    ),
    "149" => array(
        "CLASS" => "rib_yellow",
        "TITLE" => "��������",
        "IDD" => 149,
        "URL" => "/news/149/"
    ),
    "152" => array(
        "CLASS" => "rib_blue",
        "TITLE" => "���",
        "IDD" => 152,
        "URL" => "/news/152/"
    ),
    "2695" => array(
        "CLASS" => "rib_green",
        "TITLE" => "��������",
        "IDD" => 2695,
        "URL" => "/news/2695/"
    )
);
$arRibbonKeys = array_keys($arRibbon);

//����������
if($arParams["IS_COMPANY_UP"]){
   //���������� �� ���������
   $sortedByCompany = array();
   foreach ($arResult["SEARCH"] as $arKey => $arItem){
      if($arItem["REAL_ELEMENT"]["IBLOCK_ID"] == 17)
      {
         $sortedByCompany[] = $arItem;
         unset($arResult["SEARCH"][$arKey]);
      }
   }
   foreach ($arResult["SEARCH"] as $arKey => $arItem) $sortedByCompany[] = $arItem;
   $arResult["SEARCH"] = $sortedByCompany;
}
if($arParams["IS_TOVAR_UP"]){
   //���������� �� �������
   $sortedByTovar = array();
   foreach ($arResult["SEARCH"] as $arKey => $arItem){
      if($arItem["REAL_ELEMENT"]["IBLOCK_ID"] == 27)
      {
         $sortedByTovar[] = $arItem;
         unset($arResult["SEARCH"][$arKey]);
      }
   }
   foreach ($arResult["SEARCH"] as $arKey => $arItem) $sortedByTovar[] = $arItem;
   $arResult["SEARCH"] = $sortedByTovar;
}

if($arParams["IS_SORTING"])
{
   //���������� �� ������
   $t = true;
   while ($t) {
       $t = false;
       for ($i = 0; $i < count($arResult["SEARCH"]) - 1; $i++) {
           if ($arResult["SEARCH"][$i]["REAL_ELEMENT"]["TARIF_SORT"] < $arResult["SEARCH"][$i + 1]["REAL_ELEMENT"]["TARIF_SORT"]) {
               $temp = $arResult["SEARCH"][$i + 1];
               $arResult["SEARCH"][$i + 1] = $arResult["SEARCH"][$i];
               $arResult["SEARCH"][$i] = $temp;
               $t = true;
           }
       }
   }
}
 if($arParams["IS_SECTION_UP"]){
   //���������� ������
   $sortedBySection = array();
   foreach ($arResult["SEARCH"] as $arKey => $arItem){
      if($arItem["REAL_ELEMENT"]["IS_SECTION"])
      {
         $sortedBySection[] = $arItem;
         unset($arResult["SEARCH"][$arKey]);
      }
   }
   $t = true;
   while ($t) {
       $t = false;
       for ($i = 0; $i < count($sortedBySection) - 1; $i++) {
           if ($sortedBySection[$i]["DEPTH_LEVEL"] > $sortedBySection[$i + 1]["DEPTH_LEVEL"]) {
               $temp = $sortedBySection[$i + 1];
               $sortedBySection[$i + 1] = $sortedBySection[$i];
               $sortedBySection[$i] = $temp;
               $t = true;
           }
       }
   }
   foreach ($arResult["SEARCH"] as $arKey2 => $arItem2) $sortedBySection[] = $arItem2;
   $arResult["SEARCH"] = $sortedBySection;
}


foreach ($arResult["SEARCH"] as $arKey => $arItem)
{
	// ��������� "������ ����������� ��������"
	if ($arItem["REAL_ELEMENT"]['IBLOCK_ID'] == 49){
		unset($arResult["SEARCH"][$arKey]);
		continue;
	}
	// ��������� ������ ����
	if ($arItem['MODULE_ID'] == 'blog'){
		unset($arResult["SEARCH"][$arKey]);
		continue;
	}
	// ��������� ����������� � ������
	if ($arItem['MODULE_ID'] == 'forum' && strpos($arItem['URL'], 'communication/forum') !== false){
		unset($arResult["SEARCH"][$arKey]);
		continue;
	}
	
   $arItem = $arItem["REAL_ELEMENT"];
   $index = array_search($arItem["IBLOCK_SECTION_ID"], $arRibbonKeys);
   if($index !== false)
   {
      $arResult["SEARCH"][$arKey]["RIBBON"] = $arRibbon[$arRibbonKeys[$index]];
   }
   else
   {
      if($arResult["SEARCH"][$arKey]["MODULE_ID"] == "blog")
      {
         //����
         $arResult["SEARCH"][$arKey]["RIBBON"] = array(
              "CLASS" => "rib_default",
              "TITLE" => "�����",
              "URL" => "/blogs/"
            );
      }
      elseif($arResult["SEARCH"][$arKey]["MODULE_ID"] == "forum")
      {
         //�����
         $arResult["SEARCH"][$arKey]["RIBBON"] = array(
              "CLASS" => "rib_default",
              "TITLE" => "������",
              "URL" => "/forum/"
            );
      }
      else
      {
         $rsSection = CIBlockSection::GetList(
				array("SORT"=>"ASC"),
				array(
					'ID' => $arItem["IBLOCK_SECTION_ID"],
					'IBLOCK_ID' => $arItem['IBLOCK_ID']
				),
				false,
				array('ID','NAME','SECTION_PAGE_URL','UF_USER_ID')
			);
         if($arSection = $rsSection->GetNext())
         {
            //������
            $arResult["SEARCH"][$arKey]["RIBBON"] = array(
              "CLASS" => "rib_default",
              "TITLE" => $arSection["NAME"],
              "IDD" => $arItem["IBLOCK_SECTION_ID"],
              "URL" => $arSection["SECTION_PAGE_URL"]
            );
				// ������ ������ �� ������ �����
				if ($arResult["SEARCH"][$arKey]["REAL_ELEMENT"]["IBLOCK_ID"] == 50){
					$arResult["SEARCH"][$arKey]["RIBBON"]["URL"] = str_replace('#UF_USER_ID#', $arSection['UF_USER_ID'], $arResult["SEARCH"][$arKey]["RIBBON"]["URL"]);
				}
         }
         
         //�������
         if($arItem["IBLOCK_ID"] == 27)
         {
            //�����
            $rsFirm = CIBlockElement::GetList(
                array(),
                array('ID' => $arItem["PROPERTIES"]["FIRM"]["VALUE"]),
                false,
                false,
                array(
                    'ID',
                    'IBLOCK_ID',
                    'NAME',
                    'PREVIEW_PICTURE',
                    'PROPERTY_REMOVE_REL'
                )
            );
            //GetByID($arItem["PROPERTIES"]["FIRM"]["VALUE"]);
            if($arFirm = $rsFirm->GetNext())
            {
                if (!strlen($arFirm['PROPERTY_REMOVE_REL_ENUM_ID'])) {
                   if($arFirm["PREVIEW_PICTURE"])
                   {
                      $FILE = CFile::GetFileArray($arFirm["PREVIEW_PICTURE"]);
                      if (is_array($FILE))
                      {
                         $arFilterImg = array();
                         $arFileTmp = CFile::ResizeImageGet(
                             $FILE,
                             array("width" => $arParams["THUMB_WIDTH"], "height" => $arParams["THUMB_HEIGHT"]),
                             BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                             true,
                             $arFilterImg
                         );

                         $FILE = array(
                             'SRC' => $arFileTmp["src"],
                             'WIDTH' => $arFileTmp["width"],
                             'HEIGHT' => $arFileTmp["height"],
                             'DESCRIPTION' => strlen($FILE["DESCRIPTION"]) > 0 ? $FILE : $arFirm["NAME"]
                         );
                      }
                   }
                   else
                   {
                      $FILE = array(
                            'SRC' => "/images/search_placeholder.png",
                            'WIDTH' => 48,
                            'HEIGHT' => 42,
                            'DESCRIPTION' => $arFirm["NAME"]
                      );
                   }
                   $arResult["SEARCH"][$arKey]["REAL_ELEMENT"]["FIRM"]["LOGO"] = $FILE;
                   $arResult["SEARCH"][$arKey]["REAL_ELEMENT"]["FIRM"]["LINK"] = $arFirm["DETAIL_PAGE_URL"];
                   $arResult["SEARCH"][$arKey]["REAL_ELEMENT"]["FIRM"]["LOG11"] = $arFirm;
                }
            }
         }
      }
   }
   $arResult["SEARCH"][$arKey]["BODY_FORMATED"] = html_entity_decode(trim($arResult["SEARCH"][$arKey]["BODY_FORMATED"]));
}

//��������� ������ ������������ ���������� ��������� � �������
$arResult["SEARCH"] = array_slice($arResult["SEARCH"], 0, $arParams["PAGE_RESULT_COUNT_DISPLAYED"]);