<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
require_once( $_SERVER["DOCUMENT_ROOT"].$component->__path."/classes.php");

//$APPLICATION->AddHeadScript("http://".SITE_SERVER_NAME.SITE_TEMPLATE_PATH."/js/redactor2/redactor.js");
//$APPLICATION->AddHeadScript("http://".SITE_SERVER_NAME.SITE_TEMPLATE_PATH."/js/redactor2/ru.js");
//$APPLICATION->AddHeadString('<link href="http://'.SITE_SERVER_NAME.SITE_TEMPLATE_PATH.'/js/redactor2/redactor.css" type="text/css" rel="stylesheet">', true);
?>

<?if (count($arResult["ERRORS"]["MAIN"])):?>
   <?=ShowError(implode("<br />", $arResult["ERRORS"]["MAIN"]))?>
<?endif?>

<?if (strlen($arResult["MESSAGE"]) > 0):?>
   <?=ShowNote($arResult["MESSAGE"])?>
   <br /><br />
<?endif?>

<?if(strlen($arResult["ERRORS"]["LIMIT"])):?>
   <div style="color: red;"><?=$arResult["ERRORS"]["LIMIT"]?></div>
<?endif?>

<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
   <?=bitrix_sessid_post()?>
   
   <?if ($arParams["MAX_FILE_SIZE"] > 0):?>
      <input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" />
   <?endif?>

      
   <div class="mc_block company-tabs js-company-tabs">
      <ul class="mb_menu">
         <?$firstTab = true;
         foreach($arResult["PROPERTY_LIST"] as $tabKey => $arTab):
            if(!$arResult["ELEMENT_EXISTS"] && !$firstTab) break;?>
            <li<?=$firstTab ? ' class="mb_title sel" id="mb_title_f"' : ''?>><a href="javascript:void(0)"><?=$tabKey?></a><div class="sclew"></div></li>
            <?$firstTab = false;
         endforeach;?>
      </ul>
      
      <?$firstTab = true;
      foreach($arResult["PROPERTY_LIST"] as $arTab):
         if(!$arResult["ELEMENT_EXISTS"] && !$firstTab) break;?>
         <div class="news_comp mc_block tab">
            <table class="data-table bx-company-table">
               <?if (is_array($arTab) && !empty($arTab)):?>
                  <tbody class="bodyground">
                     <?foreach ($arTab as $propertyID):?>
                        <?if ($propertyID == 'EMPTY'):?>
                           <tr style="background-color:#ffffff;"><td colspan="3">&nbsp;</td></tr>
                           <?continue;
                        elseif($propertyID == "IBLOCK_SECTION"):?>
                           <tr style="display: none"><td colspan="3"><input type="hidden" name="PROPERTY[IBLOCK_SECTION]" value="149"></td></tr>
                           <?continue;
                        endif;?>
                        <tr>
                           <td class="txt_right" style="font-weight:bold;">
                              <?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?>
                                 <span class="starrequired">*</span>
                              <?endif?>

                              <?=!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : (intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] : GetMessage("IBLOCK_FIELD_".$propertyID))?>:                     
                           </td>

                           <td class="<?=isset($arResult["ERRORS"][$propertyID]) ? "error" : ""?>" style="padding-bottom: 10px;">
                              <?
                              if (intval($propertyID) > 0)
                              {
                                 if (
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                                    &&
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                                 )
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                                 elseif (
                                    (
                                       $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                                       ||
                                       $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                                    )
                                    &&
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                                 )
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
                              } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                                 $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

                              if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
                              {
                                 $inputNum = (($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) && $arResult["ELEMENT_PROPERTIES"][$propertyID][0]["VALUE"]) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                                 $inputNum += 2;//$arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
                              } else
                                 $inputNum = 1;


                              if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "HTML")
                                 $INPUT_TYPE = "HTML";
                              elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                                 $INPUT_TYPE = "USER_TYPE";
                              else
                                 $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

                              //PrintObject($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"]);
                              //PrintObject($INPUT_TYPE);
                              switch ($INPUT_TYPE):
                                 case "L":
                                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
                                       $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                                    else
                                       $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";  
                                    ?>
                                    <select class="js_section_sel main_select" name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
                                       <?if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                                          else $sKey = "ELEMENT";

                                       foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum):?>
                                          <?$checked = false;
                                          if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                          {
                                             foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
                                             {
                                                if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
                                             }
                                          } else
                                          {
                                             if ($arEnum["DEF"] == "Y") $checked = true;
                                          }?>
                                          <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
                                       <?endforeach;?>
                                    </select>
                                    <? 
                                    break;


                                 case "USER_TYPE":
                                    for ($i = 0; $i<$inputNum; $i++)
                                    {
                                       if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                       {
                                          $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
                                          $description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
                                       } elseif ($i == 0)
                                       {
                                          $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                          $description = "";
                                       } else
                                       {
                                          $value = "";
                                          $description = "";
                                       }

                                       echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"], array(
                                          $arResult["PROPERTY_LIST_FULL"][$propertyID],
                                          array(
                                              "VALUE" => $value,
                                              "DESCRIPTION" => $description),
                                          array(
                                             "VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
                                             "DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
                                             "FORM_NAME"=>"iblock_add",
                                          ),
                                          ($propertyID == 123 ? "js_country_sel" : ($propertyID == 84 ? "js_region_sel" : ($propertyID == 94 ? "js_city_sel" : "") )),
                                          !strlen($value) > 0 && $propertyID != 123  
                                       ));
                                    }
                                    break;

                                 case "TAGS":
                                    $APPLICATION->IncludeComponent("bitrix:search.tags.input", "", array(
                                          "VALUE" => $arResult["ELEMENT"][$propertyID],
                                          "NAME" => "PROPERTY[".$propertyID."][0]",
                                          "TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
                                       ), null, array("HIDE_ICONS"=>"Y")
                                    );
                                    break;
                                 case "HTML":
                                    $formId = preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]");
                                    ?><textarea id="<?=$formId?>" name="PROPERTY[<?=$propertyID?>][0]" rows="4" cols="5"><?=$arResult["ELEMENT"][$propertyID]?></textarea>
                                    <script type="text/javascript">
                                       $(document).ready(function (){
                                           $('#<?=$formId?>').redactor({
                                              resize: true,
                                              lang: "ru",
                                              buttons: ['bold', 'italic', 'underline', 'RemoveFormat', 'link', 'orderedlist', 'unorderedlist'],
                                              //articleId: '<?=$arResult["REAL_ELEMENT"]["ID"]?>'
                                           });
                                           $('.redactor_btn_link').live("click", function(){
                                              console.log("fff");
                                           })
                                           console.log("redactor_modal", $('#redactor_modal').get())
                                       }); 
                                    </script>
                                    <?
                                    /*$LHE = new CLightHTMLEditor;
                                    $LHE->Show(array(
                                       'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
                                       'width' => '650px',
                                       'height' => '300px',
                                       'inputName' => "PROPERTY[".$propertyID."][0]",
                                       'content' => $arResult["ELEMENT"][$propertyID],
                                       'bUseFileDialogs' => false,
                                       'bFloatingToolbar' => false,
                                       'bArisingToolbar' => false,
                                       'toolbarConfig' => array(
                                          'Bold', 'Italic', 'Underline', 'RemoveFormat',
                                          'CreateLink',
                                          //'DeleteLink', 'Image', 'Video',
                                          //'BackColor', 'ForeColor',
                                          //'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
                                          'InsertOrderedList', 'InsertUnorderedList',
                                          //'Outdent', 'Indent',
                                          //'StyleList', 'HeaderList',
                                          //'FontList', 'FontSizeList',
                                       ),
                                    ));*/
                                    break;
                                 case "T":
                                    for ($i = 0; $i<$inputNum; $i++)
                                    {
                                       if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                       {
                                          $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                       } elseif ($i == 0)
                                       {
                                          $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                       } else
                                       {
                                          $value = "";
                                       } ?>
                                       <textarea cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
                                    <?
                                    }
                                    break;

                                 case "S":
                                 case "N":
                                    if($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y"):?>
                                                                    
                                       <div class="wrapper_<?=$propertyID?>">
                                          <div class="elements">
                                             <?
                                             for ($i = 0; $i<$inputNum; $i++)
                                             {
                                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                                {
                                                   $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                                } elseif ($i == 0)
                                                {
                                                   $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                                } else
                                                {
                                                   $value = "";
                                                } ?>

                                                <input type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" />
                                             <? } ?>                               
                                          </div> 
                                          <a href="javascript: void(0)" onclick="insertRow($('.wrapper_<?=$propertyID?>'), '<?=$value?>')">�������� ���</a>
                                       </div>
                                    <?else:?>
                                       <?
                                       for ($i = 0; $i<$inputNum; $i++)
                                       {
                                          if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                          {
                                             $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                          } elseif ($i == 0)
                                          {
                                             $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                          } else
                                          {
                                             $value = "";
                                          } ?>

                                          <input type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" />
                                          <?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?>
                                             <?$APPLICATION->IncludeComponent('bitrix:main.calendar', '', array(
                                                   'FORM_NAME' => 'iblock_add',
                                                   'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
                                                   'INPUT_VALUE' => $value,
                                                ),
                                                null,
                                                array('HIDE_ICONS' => 'Y')
                                             );?>
                                             <small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small>
                                          <?endif;
                                       }
                                    endif;
                                    break;
                                 case "F":
                                    for ($i = 0; $i<$inputNum; $i++)
                                    {
                                       $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];?>
                                       <div class="img_block" data-prop-id="<?=$propertyID?>">
                                          <input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
                                          <input type="file" name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" /><br />
                                          <?if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])):?>
                                             <input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
                                             <?if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]):?>
                                                <img src="<?=$arResult["ELEMENT_FILES"][$value]["THUMB_PICTURE"]["SRC"]?>"  border="0" /><br />
                                             <?else:?>
                                                <?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
                                                <?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
                                                [<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
                                             <?endif;
                                          endif;?>
                                       </div>      
                                       <?      
                                    }
                                    if($inputNum > 1):?>
                                       <a href="javascript: void(0)" class="js-add-input-more">�������� ���</a>
                                    <?endif;
                                    break;


                                 case "L":
                                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
                                       $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                                    else
                                       $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

                                    switch ($type):
                                       case "checkbox":
                                       case "radio":
                                          foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
                                          {
                                             $checked = false;
                                             if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                             {
                                                if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
                                                {
                                                   foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
                                                   {
                                                      if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
                                                   }
                                                }
                                             } else
                                             {
                                                if ($arEnum["DEF"] == "Y") $checked = true;
                                             } ?>
                                             <input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br />
                                             <?
                                          }
                                          break;

                                       case "dropdown":
                                       case "multiselect": ?>
                                          <select class="main_select" name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
                                             <?if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                                                else $sKey = "ELEMENT";

                                             foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum):?>
                                                <?$checked = false;
                                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                                {
                                                   foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
                                                   {
                                                      if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
                                                   }
                                                } else
                                                {
                                                   if ($arEnum["DEF"] == "Y") $checked = true;
                                                }?>
                                                <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
                                             <?endforeach;?>
                                          </select>
                                          <?
                                          break;
                                    endswitch;
                                    break;?>
                              <?endswitch;?>

                              <?if(isset($arResult["ERRORS"][$propertyID])):?>
                                 <div class="errortext"><?=$arResult["ERRORS"][$propertyID]?></div>
                              <?endif;?>
                           </td>
                           <td class="bordered">
                              <div>
                                 <!--<p>����� ����� ���� �����-�� �����!</p>-->
                              </div>
                           </td>   
                        </tr>

                        <!--<?if(in_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"], array("DETAIL_TEXT", "SKYPE", "UNN", "ENGLISH_DESCRIPTION"))):?>
                           <tr style="background-color:#FFFFFF;">
                              <td colspan="3">&nbsp;</td>
                           </tr>
                        <?endif;?>-->
                     <?endforeach;?>

                     <?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
                        <tr>
                           <td><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?></td>
                           <td>
                              <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                              <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                           </td>
                           <td></td>
                        </tr>
                        <tr>
                           <td><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</td>
                           <td><input type="text" name="captcha_word" maxlength="50" value=""></td>
                           <td></td>
                        </tr>
                     <?endif?>

                     <tr>
                        <td colspan="2">&nbsp;</td>  
                        <td class="bordered">&nbsp;</td>         
                     </tr>
                  </tbody>
               <?endif?>

               <tfoot>
                  <tr>
                     <td colspan="3">
                        <input onclick="return CheckForm();" type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
                        <input onclick="return CheckForm();" type="submit" name="iblock_submit_inactive" value="<?=GetMessage("IBLOCK_FORM_SUBMIT_INACTIVE")?>" />
                        <!--<?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?>
                           <input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
                        <?endif;?>--->
                        <!--�����������
                        <?if($arResult["ELEMENT_EXISTS"]):?>
                           <input onclick="return CheckForm();" type="submit" name="iblock_submit_copy" value="<?=GetMessage("IBLOCK_FORM_SUBMIT_COPY")?>" />
                        <?endif;?>-->
                     </td>
                  </tr>
               </tfoot>
            </table>
         </div>
         <?$firstTab = false;
      endforeach;?>
   </div>
   <br />
   <?if (strlen($arParams["LIST_URL"]) > 0):?><a href="<?=$arParams["LIST_URL"]?>"><?=GetMessage("IBLOCK_FORM_BACK")?></a><?endif?>
</form>

<script>
$(document).ready(function(){
   $(".js_inp_name_2").change(function(){
      var   $this = $(this),
            $inpName3 = $(".js_inp_name_3");
            
      if($this.val().length > 0)
      {
         $inpName3.val("").hide();
      } else
      {
         $inpName3.val("").show();
      }
   });
});  
   
function CheckForm(){
   var   $inpName = $("#insertNameThere"),
         $inpName1 = $(".js_inp_name_1"),
         $inpName2 = $(".js_inp_name_2"),
         $inpName3 = $(".js_inp_name_3");
   
   if($inpName.size() > 0)
   {      
      if($inpName1.val().length > 0)
      {
         if($inpName2.val().length > 0)
            $inpName.val($inpName1.val() + " " + $inpName2.val());
         else if($inpName3.val().length > 0)
            $inpName.val($inpName1.val() + " " + $inpName3.val());
      }
   }    
   
   return true;
}   

function insertRow($block, defVal){
   var $el = $block.find(".elements input"),
       count = $el.size(),
       $newEl = $el.first().clone();     
   $newEl.attr("name", $newEl.attr("name").replace(/PROPERTY\[(\d+)\]\[(\d+)\]/gi, 'PROPERTY[$1][' + count + ']'));
   $newEl.val(defVal);
   $block.find(".elements").append($newEl);
}

function insertPhoneRow($block, defVal){
  var $el = $block.find(".elements .row"),
      count = $el.size(),
      $newEl = $el.first().clone();
      
   $newEl.find("input").eq(0).attr("name", $newEl.find("input").eq(0).attr("name").replace(/PROPERTY\[(\d+)\]\[(\d+)\]/gi, 'PROPERTY[$1][' + count + ']'));
   $newEl.find("input").eq(1).attr("name", $newEl.find("input").eq(1).attr("name").replace(/DESCRIPTION\[(\d+)\]\[(\d+)\]/gi, 'DESCRIPTION[$1][' + count + ']'));
   $newEl.find("input").eq(0).val(defVal);
   $newEl.find("input").eq(1).val("");
   $block.find(".elements").append($newEl); 
}

function showOnMap(){
   var string = [];
   if(~~$('.js_country_sel').val()) string.push($('.js_country_sel option[value='+$('.js_country_sel').val()+']').text());
   if(~~$('.js_city_sel').val()) string.push($('.js_city_sel option[value='+$('.js_city_sel').val()+']').text());
   if($('.js-address-input').val().length) string.push($('.js-address-input').val());
   if(string.length == 3){
      string = string.join(", ");
      if(string.length){
         var myGeocoder = ymaps.geocode(string);
         myGeocoder.then(
            function (res) {
               $('.js-coords-input').val(res.geoObjects.get(0).geometry.getCoordinates());
               if(typeof myPlacemark != 'undefined'){
                  myMapForCompany.geoObjects.remove(myPlacemark);
               }
               myPlacemark = new ymaps.Placemark(res.geoObjects.get(0).geometry.getCoordinates(), {
                  hintContent: '�������� �����'
               }, {
                  draggable: true // ����� ����� �������������, ����� ����� ������ ����.
               });
               myPlacemark.events.add("dragend", function (event) {
                  $('.js-coords-input').val(myPlacemark.geometry.getCoordinates());
               });
               myMapForCompany.geoObjects.add(myPlacemark);
               myMapForCompany.setCenter(myPlacemark.geometry.getCoordinates());
            },
            function (err) {
               console.log("���������� �� �������");
            }
         );
      }
   }
}
</script>   
