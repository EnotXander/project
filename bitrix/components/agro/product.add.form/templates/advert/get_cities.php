<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$regionID = $_POST["regionID"];
$arFilter = array(
	"ACTIVE" => "Y",
	"IBLOCK_ID" => 23,
	"PROPERTY_REGION" => $regionID
);
$arOrder = array(
	"SORT" => "ASC",
	"NAME" => "ASC"
);
$arSelect = array(
	"ID",
	"IBLOCK_ID",
	"NAME",
);

$rsElements = CIBlockElement::GetList($arOrder, $arFilter, FALSE, FALSE, $arSelect);

while ($arElement = $rsElements->GetNext()) 
{
	echo '<option value="' . $arElement["ID"] . '">' . $arElement["NAME"] . '</option>';
}
?>