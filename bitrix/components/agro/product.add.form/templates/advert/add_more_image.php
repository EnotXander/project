<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',  'ы' => 'y',   'ъ' => '',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

	    '\'' => ''
    );
    return strtr($string, $converter);
}
//global $USER;
$whiteList = array("gif","jpeg","jpg","png");
$data = array();
$error = true;
$temp=rus2translit($_FILES['user_file']['name']);
$_FILES['user_file']['name']=$temp;
$fileInfo = pathinfo($_FILES['user_file']['name']);
if (!in_array($fileInfo['extension'], $whiteList))
{
	$data['errors'] = 'Вы загружаете запрещенный тип файла';
	$arResult = array(
		'success' => false,
		'message' => $data['errors'],
		"site_template_path" => SITE_TEMPLATE_PATH
	);
	echo json_encode($arResult);
}
elseif($_FILES['user_file_up']['size'] > 3145728)
{
	$data['errors'] = 'Вы превысили допустимый размер файла';
	$arResult = array(
		'success' => false,
		'message' => $data['errors'],
		"site_template_path" => SITE_TEMPLATE_PATH
	);
	echo json_encode($arResult);
}
else
{
	$error = false;
}
//если нет ошибок, грузим файл
if(!$error)
{
    $uploadedFileSRC =  $_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$_FILES['user_file']['name'];
    if(is_uploaded_file($_FILES['user_file']['tmp_name']))
    {
        if(move_uploaded_file($_FILES['user_file']['tmp_name'], $uploadedFileSRC))
        {
            $arResult = array(
                "success" => true,
                "message" => '<div class="min photo-wrap"><img src="/upload/tmp/'.$_FILES['user_file']['name'].'"  border="0" style="width: 52px; height: 52px;"/><br /><span class="t-tip del" onclick="$(this).parent().remove();">Удалить</span><input type="hidden" name="MORE_PHOTO[]" value="/upload/tmp/'.$_FILES['user_file']['name'].'"></div>',
                "site_template_path" => SITE_TEMPLATE_PATH,
                "file_name" => $_FILES['user_file']['name']
            );
            echo json_encode($arResult);
        }
											
        else
        {
            $data['errors'] = "Во время загрузки файла произошла ошибка";
            $arResult = array(
                'success' => false,
                'message' => $data['errors'],
                "site_template_path" => SITE_TEMPLATE_PATH
            );
            echo json_encode($arResult);
        }
    }
    else {
        $data['errors'] = "Файл не  загружен";
        $arResult = array(
            'success' => false,
            'message' => $data['errors'],
            "site_template_path" => SITE_TEMPLATE_PATH
        );
        echo json_encode($arResult);
    }
}
?> 
