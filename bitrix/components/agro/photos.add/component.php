<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(strlen($arParams['SCRIPT_INCLUDE'])){
   global $APPLICATION;
   $APPLICATION->AddHeadString('<script type="text/javascript" src="'.$arParams['SCRIPT_INCLUDE'].'"></script>', true);
}

if(!CModule::IncludeModule("iblock")) die();
//$objElement = new CIBlockElement;
$objSection = new CIBlockSection;

$arResult["SECTIONS"] = array();
//$lastSection = array("ID" => 0);
$rsSection = $objSection->GetList(
        array("timestamp_x" => "DESC"),
        array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "")
);
while ($section = $rsSection->GetNext())
{
   $arResult["SECTIONS"][] = $section;
}

$this->IncludeComponentTemplate();