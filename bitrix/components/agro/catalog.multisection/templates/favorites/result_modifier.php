<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


global $USER;
if(!$USER->IsAuthorized()) LocalRedirect($APPLICATION->GetCurPage());

global $arrFilterMarketFavorites;
$arrFilterMarketFavorites["PROPERTY_FAVORITES"] = $USER->GetID();
$arrFilterMarketFavorites["SECTION_ID"] = $arResult["THIS_SECTION"]["ID"];
$arrFilterMarketFavorites["INCLUDE_SUBSECTIONS"] = "Y";

ob_start();
$APPLICATION->IncludeComponent("bitrix:news.list", "favorites", array(
                  "IBLOCK_TYPE" => "services",
                  "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                  "NEWS_COUNT" => $perPage,
                  "SORT_BY1" => "ACTIVE_FROM",
                  "SORT_ORDER1" => "DESC",
                  "SORT_BY2" => "SORT",
                  "SORT_ORDER2" => "ASC",
                  "FILTER_NAME" => "arrFilterMarketFavorites",
                  "FIELD_CODE" => array("DETAIL_PICTURE", "PREVIEW_PICTURE", "DATE_CREATE"),
                  "PROPERTY_CODE" => array("COST", "FIRM"),
                  "CHECK_DATES" => "N",
                  "DETAIL_URL" => "",
                  "AJAX_MODE" => "N",
                  "AJAX_OPTION_JUMP" => "N",
                  "AJAX_OPTION_STYLE" => "Y",
                  "AJAX_OPTION_HISTORY" => "N",
                  "CACHE_TYPE" => "N",
                  "CACHE_TIME" => 3600*24,
                  "CACHE_FILTER" => "N",
                  "CACHE_GROUPS" => "Y",
                  "PREVIEW_TRUNCATE_LEN" => "",
                  "ACTIVE_DATE_FORMAT" => "d.m H:i",
                  "SET_TITLE" => "N",
                  "SET_STATUS_404" => "N",
                  "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                  "ADD_SECTIONS_CHAIN" => "N",
                  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                  "PARENT_SECTION" => "",
                  "PARENT_SECTION_CODE" => "",
                  "DISPLAY_TOP_PAGER" => "N",
                  "DISPLAY_BOTTOM_PAGER" => "N",
                  "PAGER_TITLE" => "�������",
                  "PAGER_SHOW_ALWAYS" => "Y",
                  "PAGER_TEMPLATE" => "",
                  "PAGER_DESC_NUMBERING" => "N",
                  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                  "PAGER_SHOW_ALL" => "Y",
                  "DISPLAY_DATE" => "N",
                  "DISPLAY_NAME" => "N",
                  "DISPLAY_PICTURE" => "Y",
                  "DISPLAY_PREVIEW_TEXT" => "N",
                  "AJAX_OPTION_ADDITIONAL" => "",
                  "PAGE_ELEMENT_COUNT" => $perPage,
                  "COMPANY_NAME_CUT" => $arParams["COMPANY_NAME_CUT"],
                  "ROOT_SECTION" => $arResult["THIS_SECTION"]["ID"]
                      )
        );
$arResult["FAVORITES"] = ob_get_clean();