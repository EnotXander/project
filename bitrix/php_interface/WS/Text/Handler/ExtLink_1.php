<?php

namespace WS\Handler;

use WS\Debug;
use WS\Handler\ITextHandler;

class ExtLink implements ITextHandler
{
    /**
     * @var array
     */
    private $allowDomains;

    /**
     * @param array $allowDomains
     * @return $this
     */
    public function setAllowDomains($allowDomains)
    {
        $this->allowDomains = $allowDomains;

        return $this;
    }

    /**
     * @return array
     */
    public function getAllowDomains()
    {
        return $this->allowDomains;
    }

    /**
     * @param string $allowDomain
     * @return $this
     */
    public function addAllowDomain($allowDomain)
    {
        $this->allowDomains[] = $allowDomain;

        return $this;
    }

    private function generateRegExp()
    {
        $reg = '/((?<!(<noindex>))\<a[^>]*href=[\"\\\']?http[s]?:\/\/[^>\/\"\\\' ]*';
        foreach($this->allowDomains as $domain) {
            $reg .= "(?<!({$domain}))";
        }
        $reg .= '[\/| |\"|\\\'][^>]*\>.*<\/a>(?!(<\/noindex>)))/isU';

        return $reg;
    }

    private function addNoindex($text, $reg)
    {
        $newText = preg_replace($reg, "<noindex>$1</noindex>", $text);

        if($newText !== null) {
            return $newText;
        } else {
            return $text;
        }
    }

    public function handle($text)
    {
        if (!count($this->allowDomains)) return $text;

        $reg = $this->generateRegExp();

        $newText = $this->addNoindex($text, $reg);

        return $newText;
    }
} 