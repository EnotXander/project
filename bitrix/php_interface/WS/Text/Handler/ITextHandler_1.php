<?php

namespace WS\Handler;

interface ITextHandler
{
    public function handle($text);
}