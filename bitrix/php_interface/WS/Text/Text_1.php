<?php

namespace WS;

use WS\Handler\ITextHandler;

interface IText
{
    public function setText($text);
    public function getText();
    public function handle();
    public function addTextHandler(ITextHandler $textHandler);
}

class Text implements IText
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var array
     */
    private $textHandlers;

    public function __construct($textOrArray)
    {
        return $this->setText($textOrArray);
    }

    /**
     * @param $textOrArray
     * @internal param string|array $text
     * @return $this
     */
    public function setText($textOrArray)
    {
        if (is_array($textOrArray)) {
            foreach ($textOrArray as $text) {
                if (strlen((string)$text)) {
                    $this->text = $text;

                    return $text;
                }
            }
        } else {
            $this->text = $text;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    public function addTextHandler(ITextHandler $textHandler)
    {
        $this->textHandlers[] = $textHandler;

        return $this;
    }

    public function handle()
    {
        foreach ($this->textHandlers as $textHandler) {
            $this->text = $textHandler->handle($this->text);
        }

        return $this;
    }

    public function handleAndGet()
    {
        return $this->handle()->getText();
    }
}