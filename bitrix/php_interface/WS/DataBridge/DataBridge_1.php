<?php

namespace WS;

class DataBridge {
   protected static $secureKey = 'BdgXahPDZmth6GlEpQSG';
   protected static $bridgeAdress = null;
   
   public static function connect($bridgeAdress){
      if(strlen($bridgeAdress)){
         self::$bridgeAdress = $bridgeAdress;
      }else{
         PrintAdmin ("����� ��� DataBridge �� ������");
         return null;
      }
      return self;
   }
   protected static function send($type, $bxId, $arItem, $additionalData){
      $content = array(
          'sequreKey' => self::$secureKey,
          'type' => $type,
          'bxId' => $bxId,
          'item' => $arItem,
          'additional' => $additionalData
      );
      PrintAdmin($content);
      /*$context = stream_context_create(array(
          'http' => array(
              'method' => 'POST',
              'header' => "Content-Type: application/json\r\n",
              'content' => json_encode($content),
          ),
      ));
      if (!$result = @file_get_contents("{$bridgeAdress}/update/{$type}/$bxId", false, $context))
      {
         PrintAdmin ("������ ���������� � �������");
         return null;
      }*/
      return true;
   }

   public static function updateElement($bxId, $additionalData = null){
      if(self::$bridgeAdress === null){
         PrintAdmin ("����� ��� DataBridge �� ������");
         return null;
      }
      if(!(int)$bxId){
         PrintAdmin ("�������� id ��������: {$bxId}");
         return null;
      }
      $type = 'element';//--------
      \CModule::IncludeModule("iblock");
      $rsElement = \CIBlockElement::GetByID($bxId);
      if($obElement = $rsElement->GetNextElement()){
         $arElement = $obElement->GetFields();
         $arElement['PROP'] = $obElement->GetProperties();
         self::send($type, $bxId, $arElement, $additionalData);
      }else{
         PrintAdmin ("�� ������� ����� ������� � id: {$bxId}");
         return null;
      }
   }
}
