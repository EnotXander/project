<?php
//Функция рекурсивно ищет файлы версий нужного класса//
function globRecursiveForWS($dir,$mask,$path)
{
   $cur_ver = false;
   $class_path = '';

   foreach(glob($dir."/*") as $filepath):
      if(strtolower(substr($filepath, strlen($filepath)-strlen($mask), strlen($mask)))==strtolower($mask)):
         $filename = substr($filepath,strrpos($filepath,"/")+1);
         $filename = explode("_",$filename);
         $f = explode("/",$path);
         if($filename[0] == end($f)):
            $file_version = explode($mask,end($filename));
               if($file_version[0] > $cur_ver):
                  $cur_ver = $file_version[0];
                  $class_path = $filepath;
               endif;
         endif;
      endif;
      
      if(is_dir($filepath)):
         globRecursiveForWS($filepath,$mask,$path);
      endif;
   endforeach;
   
   if($cur_ver):
      include $class_path;//подключение файла с последней версией класса
   endif;
} 

//Функция автозагрузки классов
function myAutoloaderForWS($path){
   $path=str_replace("\\","/",$path);
   //echo "11111111111111";
   $path_WS=__DIR__;//Путь от корня до папок с классами
   globRecursiveForWS($path_WS,".php",$path);
}
//регистрация методов в стеке//
spl_autoload_register('myAutoloaderForWS');