<?php

namespace WS;

class Debug {
   //общие настройки
   private static $cookieName = 'debug';//имя куки, которая отображает дебаг
   private static $trackCount = 0;
   
   //angular
   private static $useAngularView = false;
   private static $angularAppName = 'AngApp';
   public static function includeAngular(){
      global $APPLICATION;
      $APPLICATION->AddHeadString('<script src="http://code.angularjs.org/1.2.0-rc.2/angular.min.js"></script>', true);
      return self;
   }
   public static function angular($angularName = 'AngApp'){
      self::$angularAppName = $angularName;
      self::$useAngularView = true;
      return self;
   }
   
   //pusher
   private static $pusherId = '57480';
   private static $pusherKey = '4b0661b2da376a974324';
   private static $pusherSecret = '0ca7a524e7ca1e127f83';
   private static $pusher = null;
   public static function includePusherServer($app_key = null, $app_secret = null, $app_id = null){
      if(is_null($app_key)) $app_key = self::$pusherKey;
      if(is_null($app_secret)) $app_secret = self::$pusherSecret;
      if(is_null($app_id)) $app_id = self::$pusherId;
      //require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/Pusher.php');
      self::$pusher = new Pusher($app_key, $app_secret, $app_id);
      return self;
   }
   public static function includePusherClient($app_key = null, $app_secret = null, $app_id = null){
      if(!is_null($app_key)) self::$pusherKey = $app_key;
      if(!is_null($app_secret)) self::$pusherSecret = $app_secret;
      if(!is_null($app_id)) self::$pusherId = $app_id;
      global $APPLICATION;
      $APPLICATION->AddHeadString('<script src="http://js.pusher.com/2.1/pusher.min.js" type="text/javascript"></script>', true);
      return self;
   }

   //remote print
   private static $useRemotePrint = false;
   private static $remoteName = 'debug';
   private static $remoteValueDefault = 'all';
   private static $remoteIterationHash = null;
   public static function remote($remoteName = null){
      self::includePusherServer();
      if(!is_null($remoteName)) self::$remoteName = $remoteName;
      self::$useRemotePrint = true;
      self::$remoteIterationHash = rand (0, 100000);
   }
   public static function getRemote(){
      self::includePusherClient();
      $arSubscribe = array();
      if(count($_GET)){
         foreach ($_GET as $getKey => $getVal){
            $getVal = strtolower($getVal);
            if(!strlen($getVal) || in_array($getVal, array('all', 'y'))){
               $getVal = 'all';
            }
            $arSubscribe[$getKey] = $getVal;
         }
      }else{
         $arSubscribe[self::$remoteName] = 'all';
      }
      $jsonSubscribe = self::convertToJSON($arSubscribe);
      ?><script>
         var pusher = new Pusher('<?=self::$pusherKey?>');
         var subscribe = <?=$jsonSubscribe?>;
         var iterationHash = null;
         var counter = 1;
         $(function(){
            for(var index in subscribe){
               console.warn('Подключено к каналу "'+index+'" с фильром "'+subscribe[index]+'"');
               pusher
                  .subscribe(index)
                  .bind(subscribe[index], function(data){
                     if((iterationHash === null) || (data.pusher.ih !== iterationHash)){
                        if(counter) console.groupEnd();
                        $('#js-debug-<?=self::$remoteName?>').html('');
                        console.group("iteration" + counter++, '( '+index+' '+subscribe[index]+' )', '( '+data.time+' )');
                        iterationHash = data.pusher.ih;
                     }
                     console.info(data.name, '('+data.section+')', data);
                     <?if(self::$useAngularView):?>
                        //angular
                     <?else:?>
                        
                        var print = '<pre style=\"background-color:#FFF;color:#000;font-size:10px;\"><span style=\"color: red;\">'+data.name+'</span><br>'+JSON.stringify(data, null, 3)+'</pre>';
                        $('#js-debug-<?=self::$remoteName?>').append(print);
                     <?endif;?>
                  }
               );
            }
         });
      </script>
      <button onclick="$('#js-debug-<?=self::$remoteName?>').html('');">Очистить</button>
      <div id="js-debug-<?=self::$remoteName?>"></div><?
      return self;
   }

   //проверка на отображение
   public static function checkDisplayMode($section = 'all'){
      if(isset($_COOKIE[self::$cookieName])){
         $cookie = strtolower($_COOKIE[self::$cookieName]);
         if(in_array($cookie, array('all', 'y'))){
            return true;//без указания конкретного имени
         }else{
            if($cookie === $section){
               return true;//именно это имя указано
            }else{
               return false;//указано другое имя
            }
         }
      }else{
         return false;//кука не установлена
      }
   }

   //вывод
   private static function convertToJSON($data){
      return json_encode($data, JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE);
   }
   public static function printAdmin($obj, $name = 'PrintAdmin:', $section = 'all'){
      if(self::checkDisplayMode($section)){
         echo "<pre style=\"background-color:#FFF;color:#000;font-size:10px;\"><span style=\"color: red;\">{$name}</span>\n";
         print_r($obj);
         echo "</pre>";
         return self;
      }
   }
   private static function printEx($obj, $name = 'PrintEx:', $section = 'all', $order = true){
      if(self::checkDisplayMode($section)){
         $json = self::convertToJSON($obj);
         $appName = 'DebugCtrl_'.rand(0, 1000);
         ob_start();?>
         <script>
            angular.module('<?=self::$angularAppName?>', []).directive('tree', function($compile) {
               return {
                  restrict: 'E',
                  terminal: true,
                  scope: {val: '=', parentData: '=', key: '@', first: '@', order: '@'},
                  link: function(scope, element, attrs) {
                     scope.vis = true;
                     scope.toggle = function(key) {
                        scope.vis = !scope.vis;
                        return false;
                     };
                     if(scope.first) var first = 'color: red;';
                     else var first = 'color: blue;';
                     if (angular.isObject(scope.val)) {
                        if(scope.order){
                           scope.keys = [];
                           for(index in scope.val) scope.keys.push(index);
                           repeatPart = '<div ng-repeat="key in keys">\n\
                                             <tree val="val[key]" parent-data="val" order="{{order}}" key="[ {{key}} ]"></tree>\n\
                                          </div>';
                        }else{
                           repeatPart = '<div ng-repeat="(key, item) in val">\n\
                                             <tree val="item" parent-data="val" order="{{order}}" key="[ {{key}} ]"></tree>\n\
                                          </div>';
                        }
                        template = '<span style="cursor: pointer; '+first+'" ng-click="toggle(); $event.stopPropagation()">\n\
                                       <span style="color: red;" ng-hide="vis"> + </span>{{key}}\n\
                                    </span>\n\
                                    <span style="color: gray;">&nbsp; = &nbsp;</span>\n\
                                    <div ng-show="vis" style="margin-left: 30px;">\n\
                                       '+repeatPart+'\n\
                                    </div>';
                     } else {
                        template = '<span style="'+first+'">{{key}}</span><span style="color: gray;">&nbsp; = &nbsp;</span><span>{{val}}</span>';
                     }
                     var newElement = angular.element(template);
                     $compile(newElement)(scope);
                     element.replaceWith(newElement);
                  }
               }
            });
         </script>
         <?$script = ob_get_clean();
         global $APPLICATION;
         $APPLICATION->AddHeadString($script, true);?>
         <script>
            function <?=$appName?>($scope) {
               var DebugCtrl = {
                  data: <?= $json ?>
               }
               $scope.model = DebugCtrl;
            }
         </script>
         <div style="background-color:#FFF;color:#000;font-size:10px;line-height:20px;">
            <div ng-controller="<?=$appName?>">
               <tree val="model.data" first="true" order="<?=$order?>" key="<?=$name?>"></tree>
            </div>
         </div>
         <?
      }
      return self;
   }
   public static function printRemote($obj, $name = 'PrintRemote:', $section = 'all'){
      $section = (string)$section;
      if(self::checkDisplayMode($section)){
         $data = array(
             'pusher' => array(),
             'address' => $_SERVER['REQUEST_URI'],
             'time' => date("Y-m-d H:i:s", $_SERVER['REQUEST_TIME']),
             'channel' => self::$remoteName,
             'section' => $section,
             'name' => $name,
             'data' => $obj
         );
         $data['pusher'] = array(
             'ih' => self::$remoteIterationHash,
             'channel' => self::$remoteName,
             'event' => self::$remoteValueDefault
         );
         self::$pusher->trigger($data['pusher']['channel'], $data['pusher']['event'], $data);
         if($section !== self::$remoteValueDefault){
            $data['pusher'] = array(
                'ih' => self::$remoteIterationHash,
                'channel' => self::$remoteName,
                'event' => $section
            );
            self::$pusher->trigger($data['pusher']['channel'], $data['pusher']['event'], $data);
         }
      }
      return self;
   }
   public static function pr($obj, $name = 'Print:', $section = 'all') {
      if(self::$useRemotePrint){
         self::printRemote($obj, $name, $section);
      }else{
         if(self::$useAngularView){
            self::printEx($obj, $name, $section);
         }else{
            self::printAdmin($obj, $name, $section);
         }
      }
      return self;
   }
   public static function displayError(){
      if(self::checkDisplayMode()){
         ini_set('display_startup_errors',1);
         ini_set('display_errors',1);
         error_reporting(E_ERROR | E_WARNING | E_PARSE);
      }
   }

   //параметры объектов трекинга
   private $name = null;
   private $arVars = array();
   private $arLastState = array();
   private $arTracks = array();
   public function __construct($name = null) {
      if(is_null($name)) $name = 'tracker '.self::$trackCount;
      self::$trackCount++;
      $this->name = $name;
      return $this;
   }
   public function watch(&$var, $name = null) {
      if(is_null($name)) $name = 'variable '.count($this->arVars);
      $this->arVars[] = array(
          'name' => $name,
          'var' => &$var
      );
      return $this;
   }
   public function track($name = null) {
      if(is_null($name)) $name = 'track '.count($this->arTracks);
      $state = array();
      foreach ($this->arVars as $key => $var) {
         $state[] = array(
             'name' => $var['name'],
             'changed' => ($var['var'] !== $this->arLastState[$key]['var']),
             'var' => $var['var']
         );
      }
      $this->arTracks[] = array(
          'name' => $name,
          'vars' => $state
      );
      $this->arLastState = $state;
      return $this;
   }
   public function log() {
      if(!count($this->arTracks)) $this->track('_BEFORE_SHOW');
      self::pr($this->arTracks, $this->name, $this->name);
      return $this;
   }
}
