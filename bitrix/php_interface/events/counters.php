<?php
/**
 * User: ZvvAsyA
 * Date: 01.06.15 -  14:31
 */



//Метрика: создание счетчика
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "EventIblockMetrikaCreate");
function EventIblockMetrikaCreate(&$arFields)
{

    if ($arFields['WF_PARENT_ELEMENT_ID'] > 0) return true;

    $json_g = json_decode('{
                  "name": "PHONE_CLICK",
                  "type": "action",
                  "is_retargeting": 0,
                  "prev_goal_id": 0,
                  "conditions": [
                    {
                      "type": "exact",
                      "url": "PHONE_CLICK"
                    }
                  ],
                  "class": 0
                }');


   if ($arFields["IBLOCK_ID"] == 17)//только для компаний
   {

      $yaMetrika = YandexMetrika::getInstance(array(
          "ID" => METRIKA_CLIENT_ID,
          "SECRET" => METRIKA_CLIENT_SECRET,
          "USERNAME" => METRIKA_USERNAME,
          "PASSWORD" => METRIKA_PASSWORD
      ));


      $yaMetrika->post("/counters", [ "counter" =>array(
          "name" => $arFields["CODE"],
          "site" => 'energobelarus.by',
          "goals" => [$json_g],
          "code_options" => array(
              "clickmap" => 0,
              //"external_links" => 1,
              "async" => 1,
              "informer" => array(
                  "enabled" => 1,
                  "type" => "ext",
                 // "allowed" => 1
              ),
             // "denial" => 1,

          )
      )]);

      if(!strlen($yaMetrika->error))
      {

         $arFields["PROPERTY_VALUES"][GetPropertyId("METRIKA_ID", 17)] = $yaMetrika->result["counter"]["id"];

      } else {

         // file_put_contents('log_metrika.log', $yaMetrika->error);
      }
   }
   if ($arFields["IBLOCK_ID"] == 65)//только для компаний
   {
      $yaMetrika = YandexMetrika::getInstance(array(
          "ID" => METRIKA_CLIENT_ID_AGRO,
          "SECRET" => METRIKA_CLIENT_SECRET_AGRO,
          "USERNAME" => METRIKA_USERNAME_AGRO,
          "PASSWORD" => METRIKA_PASSWORD_AGRO
      ));

       $yaMetrika->post("/counters", [ "counter" =>array(
                 "name" => $arFields["CODE"],
                 "site" => 'agrobelarus.by',
                 "goals" => [$json_g],
                 "code_options" => array(
                     "clickmap" => 0,
                     //"external_links" => 1,
                     "async" => 1,
                     "informer" => array(
                         "enabled" => 1,
                         "type" => "ext",
                        // "allowed" => 1
                     ),
                    // "denial" => 1,

                 )
             )]);


      if(!strlen($yaMetrika->error))
      {
         $arFields["PROPERTY_VALUES"][GetPropertyId("METRIKA_ID", 65)] = $yaMetrika->result["counter"]["id"];

         // mail('zvvasya@yesweinc.com', 'debug agro counter 3', $yaMetrika->result["counter"]["id"]);

      } else {
          mail('zvvasya@yesweinc.com', 'debug agro counter', $yaMetrika->error);
      }
   }
}

//Метрика: удаление счетчика
// AddEventHandler("iblock", "OnBeforeIBlockElementDelete", "EventIblockMetrikaDelete"); /// врменно закоментено
function EventIblockMetrikaDelete($ID)
{

  if(!CModule::IncludeModule('iblock')) die();
  $objElement = new CIBlockElement();
  $res = $objElement->GetList(
          array("SORT" => "ASC"),
          array("IBLOCK_ID" => 17, "ID" => $ID),
          false,
          false,
          array("PROPERTY_METRIKA_ID")
  );
  if($arItem = $res->GetNext())
  {
     if($arItem["PROPERTY_METRIKA_ID_VALUE"])
     {
        $yaMetrika = YandexMetrika::getInstance(array(
            "ID" => METRIKA_CLIENT_ID,
            "SECRET" => METRIKA_CLIENT_SECRET,
            "USERNAME" => METRIKA_USERNAME,
            "PASSWORD" => METRIKA_PASSWORD
        ));
        $yaMetrika->delete("/counter/{$arItem["PROPERTY_METRIKA_ID_VALUE"]}");
     }
  }

  $objElement = new CIBlockElement();
  $res = $objElement->GetList(
          array("SORT" => "ASC"),
          array("IBLOCK_ID" => 65, "ID" => $ID),
          false,
          false,
          array("PROPERTY_METRIKA_ID")
  );
  if($arItem = $res->GetNext())
  {
     if($arItem["PROPERTY_METRIKA_ID_VALUE"])
     {
        $yaMetrika = YandexMetrika::getInstance(array(
            "ID" => METRIKA_CLIENT_ID_AGRO,
            "SECRET" => METRIKA_CLIENT_SECRET_AGRO,
            "USERNAME" => METRIKA_USERNAME_AGRO,
            "PASSWORD" => METRIKA_PASSWORD_AGRO
        ));
        $yaMetrika->delete("/counter/{$arItem["PROPERTY_METRIKA_ID_VALUE"]}");
     }
  }
}
