<?
/**
 * ������� ������ �������� ��� �������
 * mixed $obj - ������ ��� ������ ��� ������
 * boolean $hidden - ���� ���������� �� � ������� ��� ������� ������� �����
 * @return mixed;
 */
function PrintObject($obj, $hidden = false)
{
    $hiddenStyle = "";

    if($hidden)
         $hiddenStyle = "display:none;";

    echo "<pre style=\"background-color:#FFF;color:#000;font-size:10px;".$hiddenStyle."\">";
    print_r($obj);
    echo "</pre>";
}

function PrintAdmin($obj)
{
    global  $USER;
    if($USER->IsAdmin())
    {
      echo "<pre style=\"background-color:#FFF;color:#000;font-size:10px;\"><span style=\"color: red;\">PrintAdmin:</span>\n";
      print_r($obj);
      echo "</pre>";
    }
}

function GetFileImageClass($extension) {  
   switch(strtolower($extension))
   {
      case "doc":      $filename      = "icon_doc";
                              break;
      case "docx":     $filename      = "icon_docx";
                              break;
      case "pdf":      $filename      = "icon_pdf";
                              break;
      case "djvu":     $filename      = "icon_djvu";
                              break;
      case "ppt":      $filename      = "icon_ppt";
                              break;
      case "pptx":     $filename      = "icon_ppt";
                              break;
      case "xls":      $filename      = "icon_xls";
                              break;
      case "xlsx":     $filename      = "icon_xlsx";
                              break;
      /*case "gif":      $filename      = "icon_gif";
                              break;*/
      case "jpg":      $filename      = "icon_jpg";
                              break;
      case "jpeg":     $filename      = "icon_jpeg";
                              break;
      /*case "rar":      $filename      = "icon_rar";
                              break;
      case "zip":      $filename      = "icon_rar";
                              break;*/
                        
      default: $filename      = "icon_def";
   }
   
   return $filename;
}
// ������ ���������
function e_ucfirst($text) {
    return  mb_convert_case((substr($text, 0, 1)), MB_CASE_UPPER, "windows-1251") . substr($text, 1);
}
// ���������� ��������� �� ������� �� �����
function cmpName($a, $b)
{

    if ($a['NAME'] == $b['NAME']) {
        return 0;
    }
    return ($a['NAME'] < $b['NAME']) ? -1 : 1;
}

function GetExtension($filename) {
   $path_info = pathinfo($filename);
   return $path_info['extension'];
}

function FormatBytes($bytes, $precision = 1) {
    $units = array('�', '��', '��', '��', '��');
  
    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);
  
    $bytes /= pow(1024, $pow);
  
    return round($bytes, $precision) . ' ' . $units[$pow];
}

function checkStringPHP($str) {
   $regex="/^[a-zA-Z�-��-�0-9]*$/";
   if(!preg_match($regex,$str,$match))
      return false;
   else
      return true;
}

function checkEmailPHP($email) {
   //$regex = "/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/";
   //$regex = "^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$";
   //if(!preg_match($regex,$email,$match))
   
   $email = strtolower($email);

   if(!filter_var($email, FILTER_VALIDATE_EMAIL))
      return false;
   else
      return true;
   /*$checkedEmail=filter_var($email, FILTER_VALIDATE_EMAIL);
   if($checkedEmail!=$email)
      return false;
   else
      return true;*/
}

//�������� ��� �������� �������� ������
function GetTarifs() {
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24 * 30; // 30 ����
   $cacheID = "�ARIFS";
   if ($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp[$CACHE_ID];
   }
   else
   {
      if(!CModule::IncludeModule("iblock")) die();
      $rsTarifs = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => 47, "ACTIVE" => "Y"));
      while($obTarifs = $rsTarifs->GetNextElement())
      {
         $arTarifs = $obTarifs->GetFields();
         $arTarifs["PROP"] = $obTarifs->GetProperties();
         $result["TARIFS_ID"][] = $arTarifs["ID"];
         $result["GROUPS_ID"][] = $arTarifs["PROP"]["USER_GROUP_ID"]["VALUE"];
         $result["TARIFS"][$arTarifs["ID"]] = $arTarifs;
      }
      if(is_array($result["GROUPS_ID"]) && count($result["GROUPS_ID"]))
      {
         $strGroups = implode($result["GROUPS_ID"], " | ");
         $rsGroups = CGroup::GetList(($by="c_sort"), ($order="asc"), array("ID" => $strGroups));
         while($arGroups = $rsGroups->GetNext())
         {
            $result["GROUPS"][$arGroups["ID"]] = $arGroups;
         }
      }

      $obCache->StartDataCache();
      $obCache->EndDataCache(array($CACHE_ID => $result));
   }
   return $result;
}

function PageInWork()
{
   global $USER;
   if(true/*!$USER->IsAdmin()*/)
   {
      require $_SERVER['DOCUMENT_ROOT'].'/work.php';
      exit();
   }
}

/*function GetComponentPath($componentName, $componentTemplate)
{
   $component = new CBitrixComponent();
   $component->InitComponent($componentName, $componentTemplate);
   $component->initComponentTemplate();
   PrintObject($component->__template);
   return $component->__template->GetFolder();
}

GetComponentPath("bitrix:news.list", "main_p");*/

function SiteRating()
{
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 30 ����
   $cacheID = "SiteRating_".mktime(0, 0, 0, date("n"), date("j"), date("Y"));
   if ($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp[$CACHE_ID];
   }
   else
   {
      ob_start();?>
         <div class="mc_block withoutTitleM">
            <ul class="mb_menu">
               <li class="sel"><a>�� �����</a><div class="sclew"></div></li>
               <li><a href="#3">�����</a><div class="sclew"></div></li>
               <!--<li><a href="#3">�������</a><div class="sclew"></div></li>-->
            </ul>
            <div class="white_box tab">
               <div class="wrap">
                  <? $fp = fopen("http://stat.energobelarus.by/p/export/main_page_rating_block.php", "r");
                  while ($str = fgets($fp)) {
                     echo $str;
                  }//while ($str=fgets($fp)){  
                  fclose($fp);?>
               </div>
            </div>
            <div class="white_box tab">
               <div class="wrap">
                  <? $fp = fopen("http://stat.energobelarus.by/p/export/main_page_rating_block.php?var=yesterday", "r");
                  while ($str = fgets($fp)) {
                     echo $str;
                  }//while ($str=fgets($fp)){  
                  fclose($fp); ?>
               </div>
            </div>
            <div class="white_box tab">
               <div class="wrap">
                  <? $fp = fopen("http://stat.energobelarus.by/p/export/main_page_rating_block.php?var=today", "r");
                  while ($str = fgets($fp)) {
                     echo $str;
                  }//while ($str=fgets($fp)){  
                  fclose($fp);?>
               </div>
            </div>
         </div>
      <?$result = ob_get_clean();

      $obCache->StartDataCache();
      $obCache->EndDataCache(array($CACHE_ID => $result));
   }
   echo $result;
}


function GetMyTimeIntervalInSec ($time)
{
    global $USER;
    
    if ($USER->IsAuthorized())
    {
        $rsUser = $USER->GetByID($USER->GetID());
        $arUser = $rsUser->Fetch(); // ������ ���������������� �����
        $rsRes= CUserFieldEnum::GetList(array(), array("ID" => $arUser["UF_TIME_ZONE"]));
        if ($arResult = $rsRes->GetNext()) $temp = $arResult["VALUE"];
        
        $temp = substr($temp, 1, 10);
        $temp = str_replace("GMT", "", $temp);
        $znak = substr ($temp,0,1);
        $temp = substr ($temp,1);

        $arTime = explode (":",$temp);
        if (is_array($arTime)) 
        {
            $secH = $arTime[0]*60*60; // ���� � �������� 
            $secM = $arTime[1]*60; // ������ � �������� 
            $result = $secH+$secM;
        } else $result = $temp*60*60;

        if ($znak == "+") return $time + $result;
        else return $time - $result;
    } else
    {
        return $time;
    }
}


function companyForms(){
   return array(
      "���",
      "���",
      "���", 
      "���",
      "��",
      "��"
   );
}


//���������� ��������
//�������� ��� �����
function EmployeeClearByUser($userId)
{
   if(!$userId)
   {
      global $USER;
      $userId = $USER->GetID();
   }
   $obCache = new CPHPCache;
   $cacheID = md5("Employee_User_".$userId);
   $obCache->Clean($cacheID, "/");
}
function EmployeeClearByUserAgro($userId)
{
   if(!$userId)
   {
      global $USER;
      $userId = $USER->GetID();
   }
   $obCache = new CPHPCache;
   $cacheID = md5("Employee_User_Agro_".$userId);
   $obCache->Clean($cacheID, "/");
}
//�������� ��� ��������
function EmployeeClearByCompany($companyId)
{
   $obCache = new CPHPCache;
   $cacheID = md5("Employee_Company_".$companyId);
   $obCache->Clean($cacheID, "/");
}
function EmployeeClearByCompanyAgro($companyId)
{
   $obCache = new CPHPCache;
   $cacheID = md5("Employee_Company_Agro_".$companyId);
   $obCache->Clean($cacheID, "/");
}
//�������� �������� �����
function EmployeeGetByUser($userId)
{
   if(!$userId)
   {
      global $USER;
      $userId = $USER->GetID();
   }
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 30 ����
   $cacheID = md5("Employee_User_".$userId);
   if($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
       $result = $obCache->GetVars();
      //$result = $tmp[$CACHE_ID];
   }
   else
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsEmployee = CIBlockElement::GetList(
         array("created" => "ASC", "NAME" => "ASC"),
         array(
            "IBLOCK_ID" => IBLOCK_PREPSTAVITEL,
            "ACTIVE" => "Y",
            "PROPERTY_USER" => $userId,
            "!PROPERTY_PREDSTAVITEL_VALUE" => "Y"
         )
      );
      while($obEmployee = $rsEmployee->GetNextElement())
      {
         $arEmployee = $obEmployee->GetFields();
         $arEmployee["PROP"] = $obEmployee->GetProperties();
         $result[] = $arEmployee;
      }
      $obCache->StartDataCache();
      $obCache->EndDataCache($result);
   }
   if(count($result))
   {
      $arResult = array();
      $arResult["RAW"] = $result;
      $arResult["LIST"] = array();
      foreach($result as $element)
      {
         $arResult["LIST"][$element["ID"]] = array(
             "ID" => $element["ID"],
             "COMPANY" => $element["PROP"]["COMPANY"]["VALUE"],
             "USER" => $element["PROP"]["USER"]["VALUE"],
             "POSITION" => $element["PROP"]["POSITION"]["VALUE"],
             "POSITION_MANUAL" => $element["PROP"]["POSITION_MANUAL"]["VALUE"],
             "STATUS" => $element["PROP"]["STATUS"]["VALUE_ENUM_ID"],
             "STATUS_TEXT" => $element["PROP"]["STATUS"]["VALUE"],
             "MANUAL" => (strlen($element["PROP"]["POSITION_MANUAL"]["VALUE"]) ? true : false)
         );
         $arResult["COMPANIES"][$element["PROP"]["COMPANY"]["VALUE"]] = $element["PROP"]["COMPANY"]["VALUE"];
         $arResult["POSITIONS"][$element["PROP"]["POSITION"]["VALUE"]] = $element["PROP"]["POSITION"]["VALUE"];
      }
      return $arResult;
   }
}
function EmployeeGetByUserAgro($userId)
{
   if(!$userId)
   {
      global $USER;
      $userId = $USER->GetID();
   }
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 30 ����
   $cacheID = md5("Employee_User_Agro_".$userId);
   if($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp[$CACHE_ID];
   }
   else
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsEmployee = CIBlockElement::GetList(
         array("created" => "ASC", "NAME" => "ASC"),
         array(
            "IBLOCK_ID" => 66,
            "ACTIVE" => "Y",
            "PROPERTY_USER" => $userId,
            "!PROPERTY_PREDSTAVITEL_VALUE" => "Y"
         )
      );
      while($obEmployee = $rsEmployee->GetNextElement())
      {
         $arEmployee = $obEmployee->GetFields();
         $arEmployee["PROP"] = $obEmployee->GetProperties();
         $result[] = $arEmployee;
      }
      $obCache->StartDataCache();
      $obCache->EndDataCache(array($CACHE_ID => $result));
   }
   if(count($result))
   {
      $arResult = array();
      $arResult["RAW"] = $result;
      $arResult["LIST"] = array();
      foreach($result as $element)
      {
         $arResult["LIST"][$element["ID"]] = array(
             "ID" => $element["ID"],
             "COMPANY" => $element["PROP"]["COMPANY"]["VALUE"],
             "USER" => $element["PROP"]["USER"]["VALUE"],
             "POSITION" => $element["PROP"]["POSITION"]["VALUE"],
             "POSITION_MANUAL" => $element["PROP"]["POSITION_MANUAL"]["VALUE"],
             "STATUS" => $element["PROP"]["STATUS"]["VALUE_ENUM_ID"],
             "STATUS_TEXT" => $element["PROP"]["STATUS"]["VALUE"],
             "MANUAL" => (strlen($element["PROP"]["POSITION_MANUAL"]["VALUE"]) ? true : false)
         );
         $arResult["COMPANIES"][$element["PROP"]["COMPANY"]["VALUE"]] = $element["PROP"]["COMPANY"]["VALUE"];
         $arResult["POSITIONS"][$element["PROP"]["POSITION"]["VALUE"]] = $element["PROP"]["POSITION"]["VALUE"];
      }
      return $arResult;
   }
}
//�������� ������ ��������
function EmployeeGetByCompany($companyId)
{
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 30 ����
   $cacheID = md5("Employee_Company_".$companyId);
   if($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp[$CACHE_ID];
   }
   else
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsEmployee = CIBlockElement::GetList(
         array("created" => "ASC", "NAME" => "ASC"),
         array(
            "IBLOCK_ID" => 49,
            "ACTIVE" => "Y",
            "PROPERTY_COMPANY.ID" => $companyId,
            "!PROPERTY_PREDSTAVITEL_VALUE" => "Y"
         )
      );
      while($obEmployee = $rsEmployee->GetNextElement())
      {
         $arEmployee = $obEmployee->GetFields();
         $arEmployee["PROP"] = $obEmployee->GetProperties();
         $result[] = $arEmployee;
      }
      $obCache->StartDataCache();
      $obCache->EndDataCache(array($CACHE_ID => $result));
   }
   if(count($result))
   {
      $arResult = array();
      $arResult["RAW"] = $result;
      $arResult["LIST"] = array();
      foreach($result as $element)
      {
         $arResult["LIST"][$element["ID"]] = array(
             "ID" => $element["ID"],
             "COMPANY" => $element["PROP"]["COMPANY"]["VALUE"],
             "USER" => $element["PROP"]["USER"]["VALUE"],
             "POSITION" => $element["PROP"]["POSITION"]["VALUE"],
             "POSITION_MANUAL" => $element["PROP"]["POSITION_MANUAL"]["VALUE"],
             "STATUS" => $element["PROP"]["STATUS"]["VALUE_ENUM_ID"],
             "STATUS_TEXT" => $element["PROP"]["STATUS"]["VALUE"],
             "MANUAL" => (strlen($element["PROP"]["POSITION_MANUAL"]["VALUE"]) ? true : false)
         );
         $arResult["USERS"][$element["PROP"]["USER"]["VALUE"]] = $element["PROP"]["USER"]["VALUE"];
         $arResult["POSITIONS"][$element["PROP"]["POSITION"]["VALUE"]] = $element["PROP"]["POSITION"]["VALUE"];
      }
      return $arResult;
   }
}
function EmployeeGetByCompanyAgro($companyId)
{
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 30 ����
   $cacheID = md5("Employee_Company_Agro_".$companyId);
   if($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp[$CACHE_ID];
   }
   else
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsEmployee = CIBlockElement::GetList(
         array("created" => "ASC", "NAME" => "ASC"),
         array(
            "IBLOCK_ID" => 66,
            "ACTIVE" => "Y",
            "PROPERTY_COMPANY.ID" => $companyId,
            "!PROPERTY_PREDSTAVITEL_VALUE" => "Y"
         )
      );
      while($obEmployee = $rsEmployee->GetNextElement())
      {
         $arEmployee = $obEmployee->GetFields();
         $arEmployee["PROP"] = $obEmployee->GetProperties();
         $result[] = $arEmployee;
      }
      $obCache->StartDataCache();
      $obCache->EndDataCache(array($CACHE_ID => $result));
   }
   if(count($result))
   {
      $arResult = array();
      $arResult["RAW"] = $result;
      $arResult["LIST"] = array();
      foreach($result as $element)
      {
         $arResult["LIST"][$element["ID"]] = array(
             "ID" => $element["ID"],
             "COMPANY" => $element["PROP"]["COMPANY"]["VALUE"],
             "USER" => $element["PROP"]["USER"]["VALUE"],
             "POSITION" => $element["PROP"]["POSITION"]["VALUE"],
             "POSITION_MANUAL" => $element["PROP"]["POSITION_MANUAL"]["VALUE"],
             "STATUS" => $element["PROP"]["STATUS"]["VALUE_ENUM_ID"],
             "STATUS_TEXT" => $element["PROP"]["STATUS"]["VALUE"],
             "MANUAL" => (strlen($element["PROP"]["POSITION_MANUAL"]["VALUE"]) ? true : false)
         );
         $arResult["USERS"][$element["PROP"]["USER"]["VALUE"]] = $element["PROP"]["USER"]["VALUE"];
         $arResult["POSITIONS"][$element["PROP"]["POSITION"]["VALUE"]] = $element["PROP"]["POSITION"]["VALUE"];
      }
      return $arResult;
   }
}


//������������� ��������
//��������� ������� ��������
function PredstavitelSetStatus($companyId, $userId, $status = 65)
{
   //64 - ���������������
   //65 - �������
   //66 - ���������
   $errors = false;
   $test = false;
   
   //���������
   if(!$companyId || !$userId)
   {
      if($errors) PrintAdmin ('�� ������ �������� ���� ����');
      return false;
   }
   if(!in_array($status, array(64, 65, 66)))
   {
      if($errors) PrintAdmin ("�� ������ ������: {$status}");
      return false;
   }
   
   //�������������
   if(!CModule::IncludeModule("iblock")) return;
   $objElement = new CIBlockElement();
   global $USER;
   
   //�������� ��������, ������ ������� ����� ������
   $arTarget = array();
   $rsTargetEmployee = CIBlockElement::GetList(
           array("id" => "DESC", "created" => "DESC"),
           array(
               "IBLOCK_ID" => 49,
               //"ACTIVE" => "Y",
               "PROPERTY_COMPANY" => $companyId,
               "PROPERTY_USER" => $userId,
               "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
           ),
           false,
           false,
           array('ID', 'IBLOCK_ID', 'PROPERTY_STATUS')
   );
   while($arTargetEmployee = $rsTargetEmployee->GetNext())
   {
      $arTarget[$arTargetEmployee['ID']] = $arTargetEmployee;
   }
   
   //�������� �������� ���� ��������
   $arCompany = array();
   $arFilter = array(
         "IBLOCK_ID" => 49,
         //"ACTIVE" => "Y",
         "PROPERTY_COMPANY" => $companyId,
         "!PROPERTY_STATUS" => 66,//�� ���������
         "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
      );
   if(count($arTarget)) $arFilter["!ID"] = array_keys($arTarget);
   $rsCompanyEmployee = CIBlockElement::GetList(
           array("id" => "DESC", "created" => "DESC"),
           $arFilter,
           false,
           false,
           array('ID', 'IBLOCK_ID', 'PROPERTY_USER')
   );
   while($arCompanyEmployee = $rsCompanyEmployee->GetNext())
   {
      $arCompany[$arCompanyEmployee['ID']] = $arCompanyEmployee;
   }
   //PrintAdmin($arCompany);
   
   //�������� �������� ����� �����
   $arUser = array();
   $arFilter = array(
         "IBLOCK_ID" => 49,
         //"ACTIVE" => "Y",
         "PROPERTY_USER" => $userId,
         "!PROPERTY_STATUS" => 66,//�� ���������
         "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
      );
   if(count($arTarget)) $arFilter["!ID"] = array_keys($arTarget);
   $rsUserEmployee = CIBlockElement::GetList(
           array("id" => "DESC", "created" => "DESC"),
           $arFilter,
           false,
           false,
           array('ID', 'IBLOCK_ID', 'PROPERTY_COMPANY')
   );
   while($arUserEmployee = $rsUserEmployee->GetNext())
   {
      $arUser[$arUserEmployee['ID']] = $arUserEmployee;
   }
   //PrintAdmin($arUser);
   
   //��������� ������� ������
   $arGeneralId = null;
   if(!count($arTarget))//������ �� ����������
   {
      //�������� ������, ���� �� �� ����������
      $props = array(
          "COMPANY" => $companyId,
          "USER" => $userId,
          "STATUS" => $status,
          "PREDSTAVITEL" => 71//"Y"
      );
      //���������� ������ ��� ���������� � �������
      if(!$userName = $USER->GetByID($userId)->Fetch())
      {
         if($errors) PrintAdmin ("�� ������ ���� ID: {$userId}");
         return false;
      }
      $userName = $userName['NAME'].' '.$userName['LAST_NAME'];
      //���������� ������ ��� ���������� � �������
      $rsCompanyName = CIBlockElement::GetByID($companyId);
      if(!$arCompanyName = $rsCompanyName->GetNext())
      {
         if($errors) PrintAdmin ("�� ������� �������� ID: {$companyId}");
         return false;
      }
      $companyName = htmlspecialchars_decode($arCompanyName["NAME"]);
      $arToSave = array(
          "MODIFIED_BY" => $USER->GetID(), // ������� ������� ������� �������������
          "PROPERTY_VALUES" => $props,
          "IBLOCK_ID" => 49,
          "NAME" => "{$userName} - {$companyName}",
          "ACTIVE" => "Y", // �������
      );
      if(!$test)
      {
         $newID = $objElement->Add($arToSave);
      }
      else
      {
         PrintAdmin("TEST: Add:");
         PrintAdmin($arToSave);
         $newID = 12345;
      }
      if (intval($newID) > 0)
      {
         $arGeneralId = $newID;
         PredstavitelClearByUser($userId);
         PredstavitelClearByCompany($companyId);
      }
      else
      {
         if($errors) PrintAdmin ('������ �������� ��������: '.$objElement->LAST_ERROR);
         return false;
      }
   }
   else//������ ����������
   {
      //��������� ������ ������
      foreach($arTarget as $targetId => $targetArray)
      {
         $arGeneralId = $targetId;
         if($targetArray['PROPERTY_STATUS_ENUM_ID'] != $status)
         {
            if(!$test)
            {
               $objElement->SetPropertyValues($arGeneralId, 49, $status, "STATUS");
            }
            else
            {
               PrintAdmin("TEST2: SetProperty ID {$arGeneralId}: 'STATUS' = {$status}");
            }
            PredstavitelClearByUser($userId);
            PredstavitelClearByCompany($companyId);
         }
         unset($arTarget[$targetId]);//������������ ������ ������ ������
         break;
      }
      //��������� ������ �������
      foreach($arTarget as $targetId => $targetArray)
      {
         if(!$test)
         {
            $objElement->Delete($targetId);
         }
         else
         {
            PrintAdmin("TEST: Delete ID {$targetId}");
         }
      }
   }
   
   //��������� ������� ������ ������, ���� ������ �������
   if($status == 65)//�������
   {
      foreach($arCompany as $predstavitelId => $companyArray)
      {
         if(!$test)
         {
            $objElement->SetPropertyValues($predstavitelId, 49, 66, "STATUS");//��������
         }
         else
         {
            PrintAdmin("TEST: SetProperty ID {$predstavitelId}: 'STATUS' = 66");
         }
         PredstavitelClearByUser($companyArray['PROPERTY_USER_VALUE']);
      }
      foreach($arUser as $predstavitelId => $userArray)
      {
         if(!$test)
         {
            $objElement->SetPropertyValues($predstavitelId, 49, 66, "STATUS");//��������
         }
         else
         {
            PrintAdmin("TEST: SetProperty ID {$predstavitelId}: 'STATUS' = 66");
         }
         PredstavitelClearByCompany($userArray['PROPERTY_COMPANY_VALUE']);
      }
   }
   
   return $arGeneralId;
}

function PredstavitelSetStatusAgro($companyId, $userId, $status = 65)
{
   //143 - ���������������
   //144 - �������
   //145 - ���������
   $errors = false;
   $test = false;
   
   //���������
   if(!$companyId || !$userId)
   {
      if($errors) PrintAdmin ('�� ������ �������� ���� ����');
      return false;
   }
   if(!in_array($status, array(143, 144, 145)))
   {
      if($errors) PrintAdmin ("�� ������ ������: {$status}");
      return false;
   }
   
   //�������������
   if(!CModule::IncludeModule("iblock")) return;
   $objElement = new CIBlockElement();
   global $USER;
   
   //�������� ��������, ������ ������� ����� ������
   $arTarget = array();
   $rsTargetEmployee = CIBlockElement::GetList(
           array("id" => "DESC", "created" => "DESC"),
           array(
               "IBLOCK_ID" => 66,
               //"ACTIVE" => "Y",
               "PROPERTY_COMPANY" => $companyId,
               "PROPERTY_USER" => $userId,
               "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
           ),
           false,
           false,
           array('ID', 'IBLOCK_ID', 'PROPERTY_STATUS')
   );
   while($arTargetEmployee = $rsTargetEmployee->GetNext())
   {
      $arTarget[$arTargetEmployee['ID']] = $arTargetEmployee;
   }
   
   //�������� �������� ���� ��������
   $arCompany = array();
   $arFilter = array(
         "IBLOCK_ID" => 66,
         //"ACTIVE" => "Y",
         "PROPERTY_COMPANY" => $companyId,
         "!PROPERTY_STATUS" => 145,//�� ���������
         "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
      );
   if(count($arTarget)) $arFilter["!ID"] = array_keys($arTarget);
   $rsCompanyEmployee = CIBlockElement::GetList(
           array("id" => "DESC", "created" => "DESC"),
           $arFilter,
           false,
           false,
           array('ID', 'IBLOCK_ID', 'PROPERTY_USER')
   );
   while($arCompanyEmployee = $rsCompanyEmployee->GetNext())
   {
      $arCompany[$arCompanyEmployee['ID']] = $arCompanyEmployee;
   }
   //PrintAdmin($arCompany);
   
   //�������� �������� ����� �����
   $arUser = array();
   $arFilter = array(
         "IBLOCK_ID" => 66,
         //"ACTIVE" => "Y",
         "PROPERTY_USER" => $userId,
         "!PROPERTY_STATUS" => 145,//�� ���������
         "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
      );
   if(count($arTarget)) $arFilter["!ID"] = array_keys($arTarget);
   $rsUserEmployee = CIBlockElement::GetList(
           array("id" => "DESC", "created" => "DESC"),
           $arFilter,
           false,
           false,
           array('ID', 'IBLOCK_ID', 'PROPERTY_COMPANY')
   );
   while($arUserEmployee = $rsUserEmployee->GetNext())
   {
      $arUser[$arUserEmployee['ID']] = $arUserEmployee;
   }
   //PrintAdmin($arUser);
   
   //��������� ������� ������
   $arGeneralId = null;
   if(!count($arTarget))//������ �� ����������
   {
      //�������� ������, ���� �� �� ����������
      $props = array(
          "COMPANY" => $companyId,
          "USER" => $userId,
          "STATUS" => $status,
          "PREDSTAVITEL" => 146//"Y"
      );
      //���������� ������ ��� ���������� � �������
      if(!$userName = $USER->GetByID($userId)->Fetch())
      {
         if($errors) PrintAdmin ("�� ������ ���� ID: {$userId}");
         return false;
      }
      $userName = $userName['NAME'].' '.$userName['LAST_NAME'];
      //���������� ������ ��� ���������� � �������
      $rsCompanyName = CIBlockElement::GetByID($companyId);
      if(!$arCompanyName = $rsCompanyName->GetNext())
      {
         if($errors) PrintAdmin ("�� ������� �������� ID: {$companyId}");
         return false;
      }
      $companyName = htmlspecialchars_decode($arCompanyName["NAME"]);
      $arToSave = array(
          "MODIFIED_BY" => $USER->GetID(), // ������� ������� ������� �������������
          "PROPERTY_VALUES" => $props,
          "IBLOCK_ID" => 66,
          "NAME" => "{$userName} - {$companyName}",
          "ACTIVE" => "Y", // �������
      );
      if(!$test)
      {
         $newID = $objElement->Add($arToSave);
      }
      else
      {
         PrintAdmin("TEST: Add:");
         PrintAdmin($arToSave);
         $newID = 12345;
      }
      if (intval($newID) > 0)
      {
         $arGeneralId = $newID;
         PredstavitelClearByUserAgro($userId);
         PredstavitelClearByCompanyAgro($companyId);
      }
      else
      {
         if($errors) PrintAdmin ('������ �������� ��������: '.$objElement->LAST_ERROR);
         return false;
      }
   }
   else//������ ����������
   {
      //��������� ������ ������
      foreach($arTarget as $targetId => $targetArray)
      {
         $arGeneralId = $targetId;
         if($targetArray['PROPERTY_STATUS_ENUM_ID'] != $status)
         {
            if(!$test)
            {
               $objElement->SetPropertyValues($arGeneralId, 66, $status, "STATUS");
            }
            else
            {
               PrintAdmin("TEST2: SetProperty ID {$arGeneralId}: 'STATUS' = {$status}");
            }
            PredstavitelClearByUserAgro($userId);
            PredstavitelClearByCompanyAgro($companyId);
         }
         unset($arTarget[$targetId]);//������������ ������ ������ ������
         break;
      }
      //��������� ������ �������
      foreach($arTarget as $targetId => $targetArray)
      {
         if(!$test)
         {
            $objElement->Delete($targetId);
         }
         else
         {
            PrintAdmin("TEST: Delete ID {$targetId}");
         }
      }
   }
   
   //��������� ������� ������ ������, ���� ������ �������
   if($status == 144)//�������
   {
      foreach($arCompany as $predstavitelId => $companyArray)
      {
         if(!$test)
         {
            $objElement->SetPropertyValues($predstavitelId, 66, 145, "STATUS");//��������
         }
         else
         {
            PrintAdmin("TEST: SetProperty ID {$predstavitelId}: 'STATUS' = 145");
         }
         PredstavitelClearByUserAgro($companyArray['PROPERTY_USER_VALUE']);
      }
      foreach($arUser as $predstavitelId => $userArray)
      {
         if(!$test)
         {
            $objElement->SetPropertyValues($predstavitelId, 66, 145, "STATUS");//��������
         }
         else
         {
            PrintAdmin("TEST: SetProperty ID {$predstavitelId}: 'STATUS' = 145");
         }
         PredstavitelClearByCompanyAgro($userArray['PROPERTY_COMPANY_VALUE']);
      }
   }
   
   return $arGeneralId;
}

//��������� ������� �������� �� ID ��������
function PredstavitelSetStatusByID($predstavitelId, $status = 65)
{
   $rsEmployee = CIBlockElement::GetList(
           array("id" => "DESC", "created" => "DESC"),
           array(
               "IBLOCK_ID" => 49,
               "ID" => $predstavitelId,
           ),
           false,
           false,
           array('ID', 'IBLOCK_ID', 'PROPERTY_COMPANY', 'PROPERTY_USER')
   );
   if($arEmployee = $rsEmployee->GetNext())
   {
      return PredstavitelSetStatus($arEmployee['PROPERTY_COMPANY_VALUE'], $arEmployee['PROPERTY_USER_VALUE'], $status);
   }
   else return false;
}
function PredstavitelSetStatusByIDAgro($predstavitelId, $status = 144)
{
   $rsEmployee = CIBlockElement::GetList(
           array("id" => "DESC", "created" => "DESC"),
           array(
               "IBLOCK_ID" => 66,
               "ID" => $predstavitelId,
           ),
           false,
           false,
           array('ID', 'IBLOCK_ID', 'PROPERTY_COMPANY', 'PROPERTY_USER')
   );
   if($arEmployee = $rsEmployee->GetNext())
   {
      return PredstavitelSetStatusAgro($arEmployee['PROPERTY_COMPANY_VALUE'], $arEmployee['PROPERTY_USER_VALUE'], $status);
   }
   else return false;
}

//�������� ��� �����
function PredstavitelClearByUser($userId)
{
   if(!$userId)
   {
      global $USER;
      $userId = $USER->GetID();
   }
   $obCache = new CPHPCache;
   $cacheID = md5("Predstavitel_User_".$userId);
   $obCache->Clean($cacheID, '/');
}
function PredstavitelClearByUserAgro($userId)
{
   if(!$userId)
   {
      global $USER;
      $userId = $USER->GetID();
   }
   $obCache = new CPHPCache;
   $cacheID = md5("Predstavitel_User_Agro_".$userId);
   $obCache->Clean($cacheID, '/');
}
//�������� ��� ��������
function PredstavitelClearByCompany($companyId)
{
   $obCache = new CPHPCache;
   $cacheID = md5("Predstavitel_Company_".$companyId);
   $obCache->Clean($cacheID, '/');
}
function PredstavitelClearByCompanyAgro($companyId)
{
   $obCache = new CPHPCache;
   $cacheID = md5("Predstavitel_Company_Agro_".$companyId);
   $obCache->Clean($cacheID, '/');
}
//�������� �������� �����
function PredstavitelGetByUser($userId)
{
   if(!$userId)
   {
      global $USER;
      $userId = $USER->GetID();
   }
   $result = array();
// ���� ����������� �����, �� ����� � ����� ��� ���������
      if(!CModule::IncludeModule("iblock")) return;
      $rsEmployee = CIBlockElement::GetList(
         array("created" => "ASC", "NAME" => "ASC"),
         array(
            "IBLOCK_ID" => IBLOCK_PREPSTAVITEL,
            "ACTIVE" => "Y",
            "=PROPERTY_USER" => $userId,
            "=PROPERTY_PREDSTAVITEL_VALUE" => "Y"
         )
      );
      while($obEmployee = $rsEmployee->GetNextElement())
      {
         $arEmployee = $obEmployee->GetFields();
         $arEmployee["PROP"] = $obEmployee->GetProperties();
         $result[] = $arEmployee;
      }

   if(count($result))
   {
      $arResult = array();
      $arResult["RAW"] = $result;
      $arResult["LIST"] = array();
      foreach($result as $element)
      {
          $VALUES = false;
          if ($element["PROP"]["COMPANY"]["VALUE"] != '') {
             $res = CIBlockElement::GetProperty(IBLOCK_COMPANY, $element["PROP"]["COMPANY"]["VALUE"], "sort", "asc", array("CODE" => "YARMARKA"));
             if ($ob = $res->GetNext())
             {
                  $VALUES = $ob['VALUE_ENUM'];
             }
          }
         $arResult["LIST"][$element["ID"]] = array(
             "ID" => $element["ID"],
             "COMPANY" => $element["PROP"]["COMPANY"]["VALUE"],
             "YARMARKA" => $VALUES,
             "USER" => $element["PROP"]["USER"]["VALUE"],
             "STATUS" => $element["PROP"]["STATUS"]["VALUE_ENUM_ID"],
             "STATUS_TEXT" => $element["PROP"]["STATUS"]["VALUE"],
         );
         if ($element["PROP"]["COMPANY"]["VALUE"] != '')
           $arResult["COMPANIES"][$element["PROP"]["COMPANY"]["VALUE"]] = $element["PROP"]["COMPANY"]["VALUE"];



         if($VALUES == '��')//�������
            $arResult["YARMARKA"] = '��';

         if($element["PROP"]["STATUS"]["VALUE_ENUM_ID"] == (LANG == 's1' ? 65 : 144))//�������
            $arResult["RELATED"] = $element["PROP"]["COMPANY"]["VALUE"];
      }
      return $arResult;
   }
    return false;
};

//�������� �������� ����� // ����� ��������� ����� ��� ������� ��������
function PredstavitelGetByUserAgro($userId)
{
   
   if(!$userId)
   {
      global $USER;
      $userId = $USER->GetID();
   }
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 1 ����
   $cacheID = md5("Predstavitel_User_Agro_n".$userId);
   if($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp;
   }
   else
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsEmployee = CIBlockElement::GetList(
         array("created" => "ASC", "NAME" => "ASC"),
         array(
            "IBLOCK_ID" => 66,
            "ACTIVE" => "Y",
            "PROPERTY_USER" => $userId,
            "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
         )
      );
      while($obEmployee = $rsEmployee->GetNextElement())
      {

         $arEmployee = $obEmployee->GetFields();
         $arEmployee["PROP"] = $obEmployee->GetProperties();
         $result[] = $arEmployee;
      }
      $obCache->StartDataCache();
      $obCache->EndDataCache($result);
   }
   if(count($result))
   {
      $arResult = array();
      $arResult["RAW"] = $result;
      $arResult["LIST"] = array();
      foreach($result as $element)
      {
         $arResult["LIST"][$element["ID"]] = array(
             "ID" => $element["ID"],
             "COMPANY" => $element["PROP"]["COMPANY"]["VALUE"],
             "USER" => $element["PROP"]["USER"]["VALUE"],
             "STATUS" => $element["PROP"]["STATUS"]["VALUE_ENUM_ID"],
             "STATUS_TEXT" => $element["PROP"]["STATUS"]["VALUE"],
         );
         $arResult["COMPANIES"][$element["PROP"]["COMPANY"]["VALUE"]] = $element["PROP"]["COMPANY"]["VALUE"];
         if($element["PROP"]["STATUS"]["VALUE_ENUM_ID"] == 144)//�������
            $arResult["RELATED"] = $element["PROP"]["COMPANY"]["VALUE"];
      }
      return $arResult;
   }
}
//�������� ������ ��������
function PredstavitelGetByCompany($companyId)
{
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 30 ����
   $cacheID = md5("Predstavitel_Company_".$companyId);
   if($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp;
   }
   else
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsEmployee = CIBlockElement::GetList(
         array("created" => "ASC", "NAME" => "ASC"),
         array(
            "IBLOCK_ID" => IBLOCK_PREPSTAVITEL,
            "ACTIVE" => "Y",
            "PROPERTY_COMPANY.ID" => (int)$companyId,
            "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
         )
      );
      while($obEmployee = $rsEmployee->GetNextElement())
      {
         $arEmployee = $obEmployee->GetFields();
         $arEmployee["PROP"] = $obEmployee->GetProperties();
         $result[] = $arEmployee;
      }
      $obCache->StartDataCache();
      $obCache->EndDataCache($result);
   }
   if(count($result))
   {
      $arResult = array();
      $arResult["RAW"] = $result;
      $arResult["LIST"] = array();
      foreach($result as $element)
      {
         $arResult["LIST"][$element["ID"]] = array(
             "ID" => $element["ID"],
             "COMPANY" => $element["PROP"]["COMPANY"]["VALUE"],
             "USER" => $element["PROP"]["USER"]["VALUE"],
             "STATUS" => $element["PROP"]["STATUS"]["VALUE_ENUM_ID"],
             "STATUS_TEXT" => $element["PROP"]["STATUS"]["VALUE"],
         );
         $arResult["USERS"][$element["PROP"]["USER"]["VALUE"]] = $element["PROP"]["USER"]["VALUE"];
         if($element["PROP"]["STATUS"]["VALUE_ENUM_ID"] == (LANG == 's1' ? 65 : 144))//�������
            $arResult["RELATED"] = $element["PROP"]["USER"]["VALUE"];
      }
      return $arResult;
   }
}

//�������� ������ ��������
function PredstavitelGetByCompanyAgro($companyId)
{
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 30 ����
   $cacheID = md5("Predstavitel_Company_Agro_".$companyId);
   if($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp[$CACHE_ID];
   }
   else
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsEmployee = CIBlockElement::GetList(
         array("created" => "ASC", "NAME" => "ASC"),
         array(
            "IBLOCK_ID" => 66,
            "ACTIVE" => "Y",
            "PROPERTY_COMPANY.ID" => (int)$companyId,
            "PROPERTY_PREDSTAVITEL_VALUE" => "Y"
         )
      );
      while($obEmployee = $rsEmployee->GetNextElement())
      {
         $arEmployee = $obEmployee->GetFields();
         $arEmployee["PROP"] = $obEmployee->GetProperties();
         $result[] = $arEmployee;
      }
      $obCache->StartDataCache();
      $obCache->EndDataCache(array($CACHE_ID => $result));
   }
   if(count($result))
   {
      $arResult = array();
      $arResult["RAW"] = $result;
      $arResult["LIST"] = array();
      foreach($result as $element)
      {
         $arResult["LIST"][$element["ID"]] = array(
             "ID" => $element["ID"],
             "COMPANY" => $element["PROP"]["COMPANY"]["VALUE"],
             "USER" => $element["PROP"]["USER"]["VALUE"],
             "STATUS" => $element["PROP"]["STATUS"]["VALUE_ENUM_ID"],
             "STATUS_TEXT" => $element["PROP"]["STATUS"]["VALUE"],
         );
         $arResult["USERS"][$element["PROP"]["USER"]["VALUE"]] = $element["PROP"]["USER"]["VALUE"];
         if($element["PROP"]["STATUS"]["VALUE_ENUM_ID"] == 144)//�������
            $arResult["RELATED"] = $element["PROP"]["USER"]["VALUE"];
      }
      return $arResult;
   }
}


/**
 * ���������, �������� �� $password ������� ������� ������������.
 *
 * @param int $userId
 * @param string $password
 *
 * @return bool
 */
function isUserPassword($userId, $password)
{
    $userData = CUser::GetByID($userId)->Fetch();
    $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));
    $realPassword = substr($userData['PASSWORD'], -32);
    $password = md5($salt.$password);
    return ($password == $realPassword);
}

/**
* ������� �������� URL
*
* @param  string $url
* @return boolean
*/
function check_url($url){
   if (empty($url))
      return false;    
   
   if (!preg_match("~^(?:f|ht)tps?://~i", $url))
      $url = "http://" . $url;
   
   if (preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url))
       return @get_headers($url);

   return false;
}

function GetPropertyId($propCode, $iblockId = 0)
{
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 30 ����
   $cacheID = "PropertyList_".$iblockId;
   if($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp[$cacheID];
   }
   else
   {
      $result = array();
      if(!CModule::IncludeModule("iblock")) return;
      $arFilter = array("ACTIVE" => "Y");
      if($iblockId)
         $arFilter["IBLOCK_ID"] = $iblockId;
      $rsProperty = CIBlockProperty::GetList(array("iblock_id" => "ASC", "id" => "ASC"), $arFilter);
      while($arProperty = $rsProperty->GetNext())
      {
         $result[$arProperty["CODE"]] = $arProperty["ID"];
      }
      $obCache->StartDataCache();
      $obCache->EndDataCache(array($cacheID => $result));
   }
   if(!empty($result) && isset($result[$propCode]))
   {
      return $result[$propCode];
   }

	return false;
}
function GetPropertyVariantId($variantCode, $propCode, $iblockId = 0)
{
   $result = array();
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24; // 30 ����
   $cacheID = "PropertyVariantList_".$propCode."_".$iblockId;
   if($obCache->InitCache($lifeTime, $cacheID, '/')) // �������� � �����
   {
      $tmp = $obCache->GetVars();
      $result = $tmp[$cacheID];
   }
   else
   {
      $result = array();
      if(!CModule::IncludeModule("iblock")) return;
      $rsVariant = CIBlockPropertyEnum::GetList(
              array("SORT"=>"ASC", "VALUE"=>"ASC"),
              array("PROPERTY_ID" => GetPropertyId($propCode, $iblockId)/*, "VALUE" => $variantCode*/)
      );
      while($arVariant = $rsVariant->GetNext())
      {
         $result[$arVariant["VALUE"]] = $arVariant["ID"];
      }
      $obCache->StartDataCache();
      $obCache->EndDataCache(array($cacheID => $result));
   }
   if(count($result))
   {
      return $result[$variantCode];
   }
}
function GetUserFieldVariantId($variantCode, $propCode, $iblockId)
{
   if(!CModule::IncludeModule("iblock")) return;
   $rsUserField = CUserTypeEntity::GetList(
           array(),
           array("ENTITY_ID" => "IBLOCK_{$iblockId}_SECTION", "FIELD_NAME" => $propCode)
   );
   if($arUserField = $rsUserField->Fetch())
   {
      $rsVariant = CUserFieldEnum::GetList(
            array(),
            array("USER_FIELD_ID" => $arUserField["ID"], "VALUE" => $variantCode)
      );
      if($arVariant = $rsVariant->GetNext())
      {
         return $arVariant["ID"];
      }
   }
   return 0;
   
}

//���������, �������� �� 404 ��� ���������� (���� ��������� �� �������� �����)
function Check404OutOfComponent()
{
   //PrintAdmin($_SERVER["REDIRECT_URL"]);
   /*if(strlen($_SERVER["REDIRECT_URL"]))
      define("ERROR_404_OUT", "Y");*/
}

//���������, ������������� ������� � ����������� ���������� bitrix:news

function Check404SectionPage($component)
{
   if(!CModule::IncludeModule("iblock")) return;
   $arFilter = array("IBLOCK_ID" => $component->arParams["IBLOCK_ID"]);
   if($component->arResult["VARIABLES"]["SECTION_ID"])
      $arFilter["ID"] = $component->arResult["VARIABLES"]["SECTION_ID"];
   if(strlen($component->arResult["VARIABLES"]["SECTION_CODE"]))
      $arFilter["CODE"] = $component->arResult["VARIABLES"]["SECTION_CODE"];
   $res = CIBlockSection::GetList(array(), $arFilter);
   if(!($element = $res->GetNext()) && (count($arFilter) > 1))
   {
      define("ERROR_404_OUT", "Y");
   }
}


//��������� ���������� �������
function TovarsResort($pagen, $sizen, $companyId, $iblockId, $arElements)
{
   if (!CModule::IncludeModule("iblock")) die();
   $objElement = new CIBlockElement();

   global $USER;
   $isError = array();

   $arElementFilter = [];

   if(!$USER->IsAuthorized())
      $isError[] = iconv("cp1251", "UTF-8", "��������� �����������");
   if(!$pagen)
      $isError[] = iconv("cp1251", "UTF-8", "������������ ����� ��������: {$pagen}");
   if(!$sizen)
      $isError[] = iconv("cp1251", "UTF-8", "������������ ������ ��������: {$sizen}");
   if($companyId)
   {
      $rsCompany = $objElement->GetList(array(), array("IBLOCK_ID" => IBLOCK_COMPANY, "ACTIVE" => "Y", "ID" => $companyId, "PROPERTY_USER" => $USER->GetID()));
      if($rsCompany->FieldsCount())
         $arElementFilter = array("IBLOCK_ID" => $iblockId, "PROPERTY_FIRM" => $companyId);
      else
         $isError[] = iconv("cp1251", "UTF-8", "�������� {$companyId} ��� ����� {$USER->GetID()} �� �������");
   }
   else
      $arElementFilter = array("IBLOCK_ID" => $iblockId, "PROPERTY_AUTHOR" => $USER->GetID());
   if(!$iblockId)
      $isError[] = iconv("cp1251", "UTF-8", "������������ ID ���������: {$iblockId}");

   if(!count($isError))
   {
      $arElementOldList = array();
      $arElementOldIDs = array();
      $rsElement = $objElement->GetList(
              array("PROPERTY_SORT_COMPANY" => "DESC", "ACTIVE_FROM" => "ASC"),
              $arElementFilter,
              false,
              false,
              array("ID", "NAME", "PROPERTY_SORT_COMPANY")
       );
      while($arElement = $rsElement->GetNext())
      {
         $arElement["NAME"] = iconv("cp1251", "UTF-8", $arElement["NAME"]);
         $arElement["PROPERTY_SORT_COMPANY"] = $arElement["PROPERTY_SORT_COMPANY_VALUE"];
         $arElementOldList[$arElement["ID"]] = $arElement;
         $arElementOldIDs[] = $arElement["ID"];
      }
      if(count($arElementOldList))
      {
         //��������� ��������� ���������
         $arElementUpdateList = array();
         foreach($arElements as $elem)
         {
            $elem = (int)$elem;
            if($elem && in_array($elem, $arElementOldIDs))
            {
               $arElementUpdateList[$elem] = array(
                  "ID" => $elem
               );
            }
         }
         if(count($arElementUpdateList))
         {
            $arElementNewList = $arElementOldList;
            //������� �� ������ ������� ��������� ��������
            foreach($arElementUpdateList as $updateId => $arUpdate)
            {
               unset($arElementNewList[$updateId]);
            }
            $wwwCleanArr = $arElementNewList;//*******
            //��������� � ����� ������ ��������� �������� � ����� �������
            $arTemp = array();
            if($pagen > 1)
               $arTemp = array_slice($arElementNewList, 0, ($pagen-1)*$sizen, true);
            $arTemp += $arElementUpdateList;
            if((count($arElementNewList)+count($arElementUpdateList)) > $pagen*$sizen)
               $arTemp += array_slice($arElementNewList, $pagen*$sizen-count($arElementUpdateList), null, true);
            $arElementNewList = $arTemp;
            $wwwEndArr = $arTemp;//*******
            if(count($arElementNewList) == count($arElementOldList))
            {
               //������������ ������ ������� ����������
               $multiplier = 2;
               $index = count($arElementNewList);
               foreach($arElementNewList as $itemId => $arItem)
               {
                  $arElementNewList[$itemId]["PROPERTY_SORT_COMPANY"] = $index*$multiplier;
                  $index--;
               }
               //����� ��������� � ������������ �������� ����������
               $arElementToSort = array();
               foreach($arElementOldList as $itemId => $arItem)
               {
                  if($arElementNewList[$itemId]["PROPERTY_SORT_COMPANY"] != $arElementOldList[$itemId]["PROPERTY_SORT_COMPANY"])
                     $arElementToSort[$itemId] = $arElementNewList[$itemId]["PROPERTY_SORT_COMPANY"];
               }
               if(count($arElementToSort))
               {
                  foreach($arElementToSort as $itemId => $itemSort)
                     $objElement->SetPropertyValues($itemId, $iblockId, $itemSort, "SORT_COMPANY");
               }
               else
                  $isError[] = iconv("cp1251", "UTF-8", "�� ���� �� ��������� �� ������� ������ ����������");
            }
            else
            {
               $oldCount = count($arElementOldList);
               $newCount = count($arElementNewList);
               $isError[] = iconv("cp1251", "UTF-8", "������ ({$oldCount}) � ����� ({$newCount}) ������� ������ �����!");
            }

         }
         else
            $isError[] = iconv("cp1251", "UTF-8", "�������� ������ � ������� ���������� ���������");
      }
      else
         $isError[] = iconv("cp1251", "UTF-8", "��� �������� {$companyId} �� ������� �� ������ �������� ��������� {$iblockId}");

   }

   return array('error' => $isError, "sort" => $arElementToSort, "LOG" => array(
       '$arElementOldList' => array_keys($arElementOldList),
       '$arElementUpdateList' => array_keys($arElementUpdateList),
       'old without this page' => array_keys($wwwCleanArr),
       'new order' => array_keys($wwwEndArr),
       '$arElementNewList' => array_keys($arElementNewList),
       '$arElementToSort' => $arElementToSort,
   ));
}


//������� ����� ����� (�������� � ��.) ��� ����������� ��������� ���������
function CopyFileField($fileId)
{
   if((int)$fileId)
   {
      $copyId = CFile::CopyFile($fileId);
      $arFile = CFile::GetFileArray($copyId);
      return CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$arFile["SRC"]);
   }
   else
      return false;
}

//��������� �������� �� ����� ��������� �����
function CheckTarifLimit($companyId, $iblock_id)
{
   //�������� ���������� � ��������
   $rsCompany = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_COMPANY, "ID" => $companyId));
   if($obCompany = $rsCompany->GetNextElement())
   {
      //$arCompany = $obCompany->GetFields();
      $arCompany["TARIF"] = $obCompany->GetProperty("TARIF");
      $arCompany["TARIF_DATE_START"] = $obCompany->GetProperty("TARIF_DATE_START");
      //�������� ����
      if($arCompany["TARIF"]["VALUE"])
      {
         $rsTarif = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_TARIF, "ID" => $arCompany["TARIF"]["VALUE"]));
         if($obTarif = $rsTarif->GetNextElement())
         {
            //$arResult["TARIF"] = $obTarif->GetFields();
            $arCompany["LIMIT"]= $obTarif->GetProperty("LIMIT_IBLOCK_{$iblock_id}");
            //�������� �� �����
            if($arCompany["LIMIT"]["VALUE"])
            {
               $arCountFilter = array("IBLOCK_ID" => $iblock_id, "ACTIVE" => "Y", "PROPERTY_FIRM" => $companyId);
               if(($iblock_id == 3) && ($arCompany["TARIF_DATE_START"]["VALUE"]))//��� �������� ��������� ����
               {
                  $arCompany["CHECK_DATE"] = true;
                  $arCountFilter[">ACTIVE_FROM"] = $arCompany["TARIF_DATE_START"]["VALUE"];
               }
               $rsIBlockElements = CIBlockElement::GetList(array(), $arCountFilter);
               $elements_count = $rsIBlockElements->SelectedRowsCount();
               return array(
                   "TARIF" => $arCompany["TARIF"],
                   "TARIF_DATE_START" => $arCompany["TARIF_DATE_START"],
                   "CHECK_DATE" => $arCompany["CHECK_DATE"],
                   "LIMIT" => $arCompany["LIMIT"]["VALUE"],
                   "COUNT" => $elements_count,
                   "ADDABLE" => ($arCompany["LIMIT"]["VALUE"] > $elements_count)
               );// ($arCompany["LIMIT"]["VALUE"] > $elements_count);
                  //$arResult["ERRORS"]["LIMIT"] = "�������� ����� ��������� ��� ������ ��������� �����";
            }
            else return array("ADDABLE" => true, 'TARIF'=>$arCompany["TARIF"]["VALUE"], 'LIMIT'=>$arCompany["LIMIT"]["VALUE"]);
         }
         else return array("ADDABLE" => true);
      }
      else return array("ADDABLE" => true);
   }
   else return array("ADDABLE" => true);
}

//������� ������� ��������
function ResizeImage($file,$arSize,$resizeType,$bInitSizes)
{
   $arFileTmp = CFile::ResizeImageGet(
      $file,
      $arSize,
      $resizeType,
      $bInitSizes
      );
   
   $result=array(
      "SRC" => $arFileTmp["src"],
      "WIDTH" => $arFileTmp["width"],
      "HEIGHT" => $arFileTmp["height"],
      );
      
   return $result;
}


//������� �������
function TimestampToBX($timestamp, $format = "FULL")
{
   global $DB;
   return date($DB->DateFormatToPHP(CLang::GetDateFormat($format)), $timestamp);
}
function TimestampToDB($timestamp, $format = "FULL")
{
   global $DB;
   return str_replace("'", "", $DB->CharToDateFunction(date($DB->DateFormatToPHP(CLang::GetDateFormat($format)), $timestamp)));
}
function BXToTimestamp($time, $format = "FULL")
{
   return MakeTimeStamp($time, CLang::GetDateFormat($format));
}
function BXToDB($strTime)
{
   global $DB;
   return str_replace("'", "", $DB->CharToDateFunction($strTime));
}
function DBToTimestamp($strTime, $format = "FULL")
{
   return MakeTimeStamp($strTime, "YYYY-MM-DD HH:MI:SS");
}
function DBToBX($strTime, $format = "FULL")
{
   global $DB;
   return date($DB->DateFormatToPHP(CLang::GetDateFormat($format)), MakeTimeStamp($strTime, "YYYY-MM-DD HH:MI:SS"));
}

//���������� �����
function StrUnion($arUnion, $separator = " ")
{
   $arFormated = array();
   foreach($arUnion as $element)
   {
      if(strlen($element))
         $arFormated[] = $element;
   }
   if(count($arFormated) > 1)
      return implode($separator, $arFormated);
   elseif(count($arFormated) == 1)
      return $arFormated[0];
   else
      return "";
}

//��������
function Resizer($source, $arSize, $resizeType)
{
   $file = 0;
   if(is_array($source))
   {
      foreach($source as $candidate)
      {
         if($candidate)
         {
            $file = $candidate;
            break;
         }
      }
   }
   else $file = $source;
   if($file)
   {
      $arFileTmp = CFile::ResizeImageGet(
              $file,
              $arSize,
              $resizeType,
              true
      );
      return array(
              "SRC" => $arFileTmp["src"],
              "WIDTH" => $arFileTmp["width"],
              "HEIGHT" => $arFileTmp["height"],
      );
   }
   else return;
}


//���������� ������ ��������� ��������
function NotEmptyStringOf($arString)
{
   if(is_array($arString))
   {
      foreach($arString as $string)
      {
         if(strlen($string)) return $string;
      }
   }
   elseif(is_string($arString))
   {
      if(strlen($string)) return $string;
   }
   return null;
}


//������� � ������ �������
function unquot($str)
{
   $str = str_replace('"', '', html_entity_decode($str));
   return str_replace('&quot;', '', html_entity_decode($str));
}


//������������� �� �������� ���� �������� �������
function SetSectionMeta($arSection, $arMetaFields = array("BROWSER_TITLE" => "UF_SEO_TITLE", "META_DESCRIPTION" => "UF_DESCRIPTION", "META_KEYWORDS" => "UF_KEYWORDS"))
{
   if($arSection["IBLOCK_ID"] && $arSection["ID"])
   {
      global $APPLICATION;
      if(!CModule::IncludeModule("iblock")) die();
      $rsSectionMeta = CIBlockSection::GetList(
              array(),
              array("IBLOCK_ID" => $arSection["IBLOCK_ID"], "ID" => $arSection["ID"]),
              false,
              $arMetaFields
      );
      if($arSectionMeta = $rsSectionMeta->GetNext())
      {
         $APPLICATION->SetTitle($arSectionMeta[$arMetaFields["BROWSER_TITLE"]]);
         $APPLICATION->SetPageProperty("description", $arSectionMeta[$arMetaFields["META_DESCRIPTION"]]); 
         $APPLICATION->SetPageProperty("keywords", $arSectionMeta[$arMetaFields["META_KEYWORDS"]]);
      }
   }
}

      
function Superprint($array)
{
   $i=0;
   echo "<div class='click_span' style='width:20px;'>+</div><ul class='active'>(";
   echo "<div style='margin:0px 0px 0px 30px;'>";

   foreach($array as $key => $arr)
   {
      echo '<li style="list-style-type:none;">['.$key.']';
      echo '&nbsp=>&nbsp';
      if (is_array($arr))
      {
         Superprint($arr);
      }
      else
      {
         echo $arr.'</li>';
      }
   $i++; 
   }
echo '</div>)</ul>';
}

function itemFieldsValues( $id, $iblock, $field ) {
	$db_props = CIBlockElement::GetProperty($iblock, $id, "sort", "asc", Array("CODE"=>$field)); // XXX - ������������� �������� ���� "������"
	if($ar_props = $db_props->Fetch())
		return $ar_props;

	return array();
}


function cacheFunction($CACHE_TIME, $CACHE_ID, $callback){
	$obCache = new CPHPCache;

	if ($obCache->InitCache($CACHE_TIME, $CACHE_ID.$_SERVER['SERVER_NAME'])) {
	    $vars = $obCache->GetVars();
		return $vars["result"];
	} else {

	$obCache->StartDataCache();

	$ar_result = $callback();

	$obCache->EndDataCache(
			array(
				"result" => $ar_result
			)
		);
	}

	return $ar_result;
};

function getUniField($filter, $code){
    			 //  ���������� ���� ��������������
    $ar_dev = CIBlockElement::GetList(array("PROPERTY_".$code => "ASC"), //arOrder =
                                      $filter, //arFilter =
                                      array('PROPERTY_'.$code), //arGroupBy =
                                      false, //arNavStartParams =
                                      Array('PROPERTY_'.$code)//arSelectFields =
    );

    $result =[];
    while ($ar_result = $ar_dev->Fetch()){
        if ( $ar_result['PROPERTY_'.$code.'_VALUE'] == '' ) continue;
        $result []= $ar_result['PROPERTY_'.$code.'_VALUE'];
    }

    return $result;
}

function phone($phone = '', $convert = true, $trim = true)
{
    //$phoneCodes = array('+375'=>'BY'); // ������ ��� �������! ��� ���������� ���������� �� ���������� ����������.

    $phoneCodes = array('375'=>Array(
            'name'=>'Belarus',
            'cityCodeLength'=>2,
            'zeroHack'=>false,
            'exceptions'=>Array(),
            'exceptions_max'=>2,
            'exceptions_min'=>2
        ),'7'=>Array(
            'name'=>'Russia',
            'cityCodeLength'=>3,
            'zeroHack'=>false,
            'exceptions'=>Array(),
            'exceptions_max'=>2,
            'exceptions_min'=>2
        ),'380'=>Array(
            'name'=>'Ukraine',
            'cityCodeLength'=>2,
            'zeroHack'=>false,
            'exceptions'=>Array(),
            'exceptions_max'=>2,
            'exceptions_min'=>2
        ),'359'=>Array(
            'name'=>'Bulgaria',
            'cityCodeLength'=>2,
            'zeroHack'=>false,
            'exceptions'=>Array(),
            'exceptions_max'=>2,
            'exceptions_min'=>2
        ),'370'=>Array(
            'name'=>'Lithuania',
            'cityCodeLength'=>3,
            'zeroHack'=>false,
            'exceptions'=>Array(),
            'exceptions_max'=>2,
            'exceptions_min'=>2
        ),'371'=>Array(
            'name'=>'Latvia',
            'cityCodeLength'=>2,
            'zeroHack'=>false,
            'exceptions'=>Array(),
            'exceptions_max'=>2,
            'exceptions_min'=>2
        ),'373'=>Array(
            'name'=>'Moldova',
            'cityCodeLength'=>3,
            'zeroHack'=>false,
            'exceptions'=>Array(),
            'exceptions_max'=>2,
            'exceptions_min'=>2
        ),'381'=>Array(
            'name'=>'Yugoslavia',
            'cityCodeLength'=>2,
            'zeroHack'=>false,
            'exceptions'=>Array(),
            'exceptions_max'=>2,
            'exceptions_min'=>2
        ),'8'=>Array(
            'name'=>'MEz',
            'cityCodeLength'=>3,
            'zeroHack'=>false,
            'exceptions'=>Array(),
            'exceptions_max'=>2,
            'exceptions_min'=>2
        ));

    if (empty($phone)) {
        return '';
    }
    // ������� �� ������� ������ � ����������� ���������� � ������ � ������ ������
    $phone=trim($phone);
    $plus = ($phone[ 0] == '+');

    $phone = preg_replace("/[^0-9A-Za-z]/", "", $phone);
    $OriginalPhone = $phone;

    // ������������ ��������� ����� � ��������
    if ($convert == true && !is_numeric($phone)) {
        $replace = array('2'=>array('a','b','c'),
        '3'=>array('d','e','f'),
        '4'=>array('g','h','i'),
        '5'=>array('j','k','l'),
        '6'=>array('m','n','o'),
        '7'=>array('p','q','r','s'),
        '8'=>array('t','u','v'),
        '9'=>array('w','x','y','z'));

        foreach($replace as $digit=>$letters) {
            $phone = str_ireplace($letters, $digit, $phone);
        }
    }

    // �������� 00 � ������ ������ �� +
    if (substr($phone,  0, 2)== "00")
    {
        $phone = substr($phone, 2, strlen($phone)-2);
        $plus=true;
    }

    // ���� ������� ������� 7 ��������, �������� ����� ������
    if (strlen($phone)>7)
    foreach ($phoneCodes as $countryCode=>$data)
    {
        $codeLen = strlen($countryCode);
        if (substr($phone,  0, $codeLen)==$countryCode)
        {
            // ��� ������ ������ ����������, ������� ������� �� ������ ���� ������
            $phone = substr($phone, $codeLen, strlen($phone)-$codeLen);
            $zero=false;
            // ��������� �� ������� ����� � ���� ������
            if ($data['zeroHack'] && $phone[ 0]=='0')
            {
                $zero=true;
                $phone = substr($phone, 1, strlen($phone)-1);
            }

            $cityCode=NULL;
            // ������� ���������� � ��������-������������
            if ($data['exceptions_max']!= 0)
            for ($cityCodeLen=$data['exceptions_max']; $cityCodeLen>=$data['exceptions_min']; $cityCodeLen--)
            if (in_array(intval(substr($phone,  0, $cityCodeLen)), $data['exceptions']))
            {
                $cityCode = ($zero? "": "").substr($phone,  0, $cityCodeLen);
                $phone = substr($phone, $cityCodeLen, strlen($phone)-$cityCodeLen);
                break;
            }
            // � ������ ������� � ������������ �������� ��� ������ � ������������ � ������ �� ���������
            if (is_null($cityCode))
            {
                $cityCode = substr($phone,  0, $data['cityCodeLength']);
                $phone = substr($phone, $data['cityCodeLength'], strlen($phone)-$data['cityCodeLength']);
            }
            if ($countryCode == '375') $plus = true;
            // ���������� ���������
            return ($plus? "+": "").$countryCode.' ('.$cityCode.') '.phoneBlocks($phone);
        }
    }
    // ���������� ��������� ��� ���� ������ � ������
    return ($plus? "+": "").phoneBlocks($phone);
}

// ������� ���������� ����� ����� � ������ ������� XX-XX-... ��� XXX-XX-XX-... � ����������� �� �������� ���-�� ����
function phoneBlocks($number){
    $add='';
    if (strlen($number)%2)
    {
        $add = $number[ 0];
        $add .= (strlen($number)<=5? "-": "");
        $number = substr($number, 1, strlen($number)-1);
    }
    return $add.implode("-", str_split($number, 2));
}
function html_img_src($html) {
    $matches = [];
    if (stripos($html, '<img') !== false) {
        $imgsrc_regex = '#<\s*img[^\>]*src\s*=\s*(["\'])(.*?)\1#im';
        preg_match_all($imgsrc_regex, $html, $matches);
        unset($imgsrc_regex);
        unset($html);
        if (is_array($matches) && !empty($matches)) {
           // print_r($matches);
            return $matches[2];
        } else {
            return [];
        }
    } else {
        return [];
    }
}
// �����
/*
echo phone("+38 (044) 226-22-04")."<br />";
echo phone(�0038 (044) 226-22-04�)."<br />";
echo phone("+79263874814")."<br />";
echo phone(�4816145�)."<br />";
echo phone("+44 (0) 870 770 5370")."<br />";
echo phone(�0044 (0) 870 770 5370�)."<br />";
echo phone("+436764505509")."<br />";
echo phone("(+38-048) 784-15-46 ")."<br />";
echo phone("(38-057) 706-34-03 ")."<br />";
echo phone("+38 (044) 244 12 01 ")."<br />";
*/


function ShowCanonical(){
   global $APPLICATION;

   $APPLICATION->AddBufferContent("ShowCanonicalHandle");
}
function ShowCanonicalHandle(){
   global $APPLICATION;
   $href = $APPLICATION->GetProperty('canonical');
   $dir = siteURL().$APPLICATION->GetCurDir();

  if($href <> '')
     if( $dir != $href ){
         return '<link rel="canonical" data-dir="'.$dir.'" href="'.$href.'">'."\n";
     }

}

    function IncViewCounter($ID){
        global $DB;
        $ID = (int)$ID;
        if ($ID <= 0)
            return;

        $strSql =
            "UPDATE b_iblock_element SET ".
            "	TIMESTAMP_X = ".($DB->type=="ORACLE"?" NULL":"TIMESTAMP_X").", ".
            "	SHOW_COUNTER_START = ".$DB->IsNull("SHOW_COUNTER_START", $DB->CurrentTimeFunction()).", ".
            "	SHOW_COUNTER =  ".$DB->IsNull("SHOW_COUNTER", 0)." + 1 ".
            "WHERE ID=".$ID;
        $DB->Query($strSql, false, "", array("ignore_dml"=>true));
    }

function siteURL() {
  $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ||
    $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
  $domainName = $_SERVER['HTTP_HOST'];
  return $protocol.$domainName;
}

function SetWatermarkImg($text) {
      $list = html_img_src($text);
  	  // file_put_contents(SITE_DIR.'/evn.log', print_r($list, true));
  	  $template = LANG == 's1'? 'energo3.0' : 'agrorb';
  	  foreach ($list as $url)
  	  {
  		    $ext =  pathinfo(parse_url($url, PHP_URL_PATH), PATHINFO_EXTENSION);

  		    $from =  $_SERVER['DOCUMENT_ROOT'].$url;
  		    $fto  =  str_replace('.'.$ext, '.wt.'.$ext, $from);

  		    $arSize = ["width"=>0, "height"=>0];
  			$arFileTmp = CFile::ResizeImageFile(
  				$from,
                  $fto,
  				$arSize,
  				false,
                  false,
                  false,
                      [[
                         "name"       => "watermark",
                         "position"   => "bottomright", //"size"=>"real",
                         "file"       => $_SERVER['DOCUMENT_ROOT']."/bitrix/templates/$template/images/watermark.png"
                     ]]
              );

  		  $fto  =  str_replace($_SERVER['DOCUMENT_ROOT'], '', $fto);

  		  if ( !empty($arFileTmp))
             $text = str_replace($url, $fto, $text );

  	  }

      return $text;
}


function en_getName ($_id){
      $id = intval($_id);
      if ( $id > 0 ){

            $ob = CIBlockElement::GetByID($id);
            if ( $item = $ob->GetNext() ){

                return $item['NAME'];
            } else {
                return '';
            }

      } else {
         return $_id;
      }
   }