<?

class YandexMetrika {
   protected static $_instance;
   private $client_id; // id ����������
   private $client_secret; // ������ ����������
   private $username;// ����������� ����� �����
   private $password;// ������ �����
   public $token; // �����, ��������� � ���������� �����������
   public $success = true; // bool ��������� ���������� ���������� ��������� ��������
   public $error = ''; // �������� ��������� ������
   public $result = array(); // ��������� �������

   public static function getInstance($arAuth) {
      // ��������� ������������ ����������
      if (null === self::$_instance)
      {
         // ������� ����� ���������
         self::$_instance = new self($arAuth);
      }
      // ���������� ��������� ��� ������������ ���������
      return self::$_instance;
   }
    
   private function YandexMetrika($arAuth) {
      $this->client_id = $arAuth["ID"];
      $this->client_secret = $arAuth["SECRET"];
      $this->username = $arAuth["USERNAME"];
      $this->password = $arAuth["PASSWORD"];
      $this->login($this->username, $this->password);
      /*$this->client_id = $client_id;
      $this->client_secret = $client_secret;*/
   }

   // ����������� �� ������� ����� �����-������ ������������
   private function login($username, $password) {
      $url = 'https://oauth.yandex.ru/token';
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
      curl_setopt($ch, CURLOPT_FAILONERROR, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
      curl_setopt($ch, CURLOPT_TIMEOUT, 9);
      curl_setopt($ch, CURLOPT_POST, 1); // set POST method
      curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=password&username={$username}&password={$password}&client_id={$this->client_id}&client_secret={$this->client_secret}"); // add POST fields
      $result = curl_exec($ch); // run the whole process 
      $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);

      if ($status != 200)
      {
         $this->_error($result);
         return false;
      }

      $this->_success($result);
      $this->token = $this->result['access_token'];
      return true;
   }
   
   public function getToken(){
      return $this->token;
   }

   // ������� ������ GET
   public function get($method, $arPost = array()) {
      $path = "http://api-metrika.yandex.ru{$method}.json?";
      foreach ($arPost as $key => $value)
         $path .= "{$key}={$value}&";
      $path .= "oauth_token=" . $this->token;
      if (!$result = @file_get_contents($path))
      {
         $this->_error();
         return false;
      }
      $this->_success($result);
      return true;
   }
   
   // ������� ������ POST
   public function post($method, $arPost) {
      // ������� �������� � ���������������� POST ������
      $context = stream_context_create(array(
          'http' => array(
              'method' => 'POST',
              'ignore_errors' => true,
              'header' => ""
                  ."Authorization: OAuth {$this->token}\r\n"
                  ."Accept: application/x-yametrika+json\r\n"
                  ."Content-Type: application/x-yametrika+json\r\n",
              'content' => json_encode($arPost),
          ),
      ));

      ;      // ��������� ������ �� ����, ����� ��������� �����
      // � �������� ��������� ���������� ������
      $res = file_get_contents("http://api-metrika.yandex.ru/management/v1{$method}", false, $context);

      $result = json_decode($res);

      if ($result && !empty($result->errors))
      {
         $this->_error($res);
         return false;
      }
      $this->_success($res);

      return true;
   }
   
   // ������� ������ POST
   public function delete($method, $arPost=null) {
      // ������� �������� � ���������������� POST ������
      $context = stream_context_create(array(
          'http' => array(
              'method' => 'DELETE',
              'header' => "Content-Type: application/x-www-form-urlencoded\r\n"
                  ."Authorization: OAuth {$this->token}\r\n"
                  ."Accept: application/x-yametrika+json\r\n"
                  ."Content-type: application/x-yametrika+json\r\n",
              'content' => json_encode($arPost),
          ),
      ));
      // ��������� ������ �� ����, ����� ��������� �����
      // � �������� ��������� ���������� ������
      if (!$result = @file_get_contents("http://api-metrika.yandex.ru{$method}", false, $context))
      {
         $this->_error();
         return false;
      }
      $this->_success($result);
      return true;
   }
   
   // ���������� ����� ������ �������� ��������
   private function _success($result) {
      $this->result = json_decode($result, true);
      $this->success = true;
      $this->error = '';
   }

   // ���������� ����� ������ ���������� ��������
   private function _error($desc = '') {
      $this->success = false;
      $this->error = json_decode($desc, true);
   }
   
   //���������� �������� �� ��������
   /*public static function setCounter($counter_id){
      if($counter_id):?>
         <script>
            $(document).ready(function(){
               new Ya.Metrika({id:<?= $counter_id ?>, enableAll:true});
            });
         </script>
      <?endif;
   }*/
   public static function setCounter($counter_id){
      if($counter_id):?>
         <script>
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter<?= $counter_id ?> = new Ya.Metrika({id:<?= $counter_id ?>,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
         </script>
      <?endif;
   }
   
   //����������� �������� �� ��������
   public static function showCounter($counter_id){
      if($counter_id):?>
         <a href="http://metrika.yandex.ru/stat/?id=<?= $counter_id ?>&amp;from=informer" target="_blank" rel="nofollow">
            <img src="//bs.yandex.ru/informer/<?= $counter_id ?>/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" alt="������.�������" title="������.�������: ������ �� ������� (���������, ������ � ���������� ����������)" onclick="try{Ya.Metrika.informer({i:this,id:<?= $counter_id ?>,lang:'ru'});return false}catch(e){}"/>
         </a>
      <?endif;
   }
   
   public function oldPost($method, $arPost){
//      $fp = fsockopen("api-metrika.yandex.ru", 80, &$errno, $errstr, 30);
      if (!$fp)
      {
         echo "$errstr ($errno)<br />\n";
         return false;
      } else
      {
         $data = json_encode($arPost);
         $out = "POST http://api-metrika.yandex.ru{$method} HTTP/1.0\r\n";
         $out .= "Host: metrika.yandex.ru\r\n";
         $out .= "Authorization: OAuth {$this->token}\r\n";
         $out .= "Accept: application/x-yametrika+json\r\n";
         $out .= "Content-type: application/x-yametrika+json\r\n";
         $out .= "Content-length: " . strlen($data) . "\r\n";
         $out .= "Connection: Close\r\n\r\n";
         $out .= $data;
         $html = '';
         fwrite($fp, $out);
         while (!feof($fp))
         {
            $html .= fgets($fp, 128);
         }
         fclose($fp);
         $pos = strpos($html, "\r\n\r\n");
         $html = substr($html, $pos + 4);
         return $html;
      }
   }
}