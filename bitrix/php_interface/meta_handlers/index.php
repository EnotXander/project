<?require_once 'core.php';


//����������
class MetaHandlerArticles extends MetaHandler
{
   private $iblock_id = 2;
   protected function GetByID($arElement)
   {
      $arElement = parent::GetByID($arElement);
      if($arElement["PROP"]["FIRM"]["VALUE"])
      {
         if(!CModule::IncludeModule("iblock")) return;
         $rsCompany = CIBlockElement::GetByID($arElement["PROP"]["FIRM"]["VALUE"]);
         if($arCompany = $rsCompany->GetNext())
         {
            $arElement["COMPANY_NAME"] = trim(unquot($arCompany["NAME"]));
         }
      }
      return $arElement;
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $obParser = new CTextParser;
      if(!strlen($arElement["COMPANY_NAME"])) $browserTitle = "{$arParams["NAME"]} - ��������������";
      else $browserTitle = "{$arParams["NAME"]} ������� �������� {$arParams["COMPANY_NAME"]} �� EnergoBelarus.by";
      return array(
          "BROWSER_TITLE" => $browserTitle,
          "DESCRIPTION" => $obParser->html_cut(NotEmptyStringOf(array(
              trim(unquot(strip_tags($arParams["PREVIEW_TEXT"]))),
              trim(unquot(strip_tags($arParams["DETAIL_TEXT"])))
          )), 200),
          "KEYWORDS" => $arParams["TAGS"],
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerArticlesAgro extends MetaHandler
{
   private $iblock_id = 55;
   protected function GetByID($arElement)
   {
      $arElement = parent::GetByID($arElement);
      if($arElement["PROP"]["FIRM"]["VALUE"])
      {
         if(!CModule::IncludeModule("iblock")) return;
         $rsCompany = CIBlockElement::GetByID($arElement["PROP"]["FIRM"]["VALUE"]);
         if($arCompany = $rsCompany->GetNext())
         {
            $arElement["COMPANY_NAME"] = trim(unquot($arCompany["NAME"]));
         }
      }
      return $arElement;
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $obParser = new CTextParser;
      if(!strlen($arElement["COMPANY_NAME"])) $browserTitle = "{$arParams["NAME"]} - ������������";
      else $browserTitle = "{$arParams["NAME"]} ������� �������� {$arParams["COMPANY_NAME"]} �� AgroBelarus.by";
      return array(
          "BROWSER_TITLE" => $browserTitle,
          "DESCRIPTION" => $obParser->html_cut(NotEmptyStringOf(array(
              trim(unquot(strip_tags($arParams["PREVIEW_TEXT"]))),
              trim(unquot(strip_tags($arParams["DETAIL_TEXT"])))
          )), 200),
          "KEYWORDS" => $arParams["TAGS"],
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerNews extends MetaHandler
{
   private $iblock_id = 3;
   protected function GetByID($arElement)
   {
      $arElement = parent::GetByID($arElement);
      if($arElement["PROP"]["FIRM"]["VALUE"])
      {
         if(!CModule::IncludeModule("iblock")) return;
         $rsCompany = CIBlockElement::GetByID($arElement["PROP"]["FIRM"]["VALUE"]);
         if($arCompany = $rsCompany->GetNext())
         {
            $arElement["COMPANY_NAME"] = trim(unquot($arCompany["NAME"]));
         }
      }
      return $arElement;
   }

   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $obParser = new CTextParser;
      if(!strlen($arElement["COMPANY_NAME"])) $browserTitle = "{$arParams["NAME"]} - ��������������";
      else $browserTitle = "{$arParams["NAME"]} ������� �������� {$arParams["COMPANY_NAME"]} �� EnergoBelarus.by";
      return array(
          "BROWSER_TITLE" => $browserTitle,
          "DESCRIPTION" => $obParser->html_cut(NotEmptyStringOf(array(
              trim(unquot(strip_tags($arParams["PREVIEW_TEXT"]))),
              trim(unquot(strip_tags($arParams["DETAIL_TEXT"])))
          )), 200),
          "KEYWORDS" => $arParams["TAGS"],
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerNewsAgro extends MetaHandler
{
   private $iblock_id = 52;
   protected function GetByID($arElement)
   {
      $arElement = parent::GetByID($arElement);
      if($arElement["PROP"]["FIRM"]["VALUE"])
      {
         if(!CModule::IncludeModule("iblock")) return;
         $rsCompany = CIBlockElement::GetByID($arElement["PROP"]["FIRM"]["VALUE"]);
         if($arCompany = $rsCompany->GetNext())
         {
            $arElement["COMPANY_NAME"] = trim(unquot($arCompany["NAME"]));
         }
      }
      return $arElement;
   }

   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $obParser = new CTextParser;
      if(!strlen($arElement["COMPANY_NAME"])) $browserTitle = "{$arParams["NAME"]} - ������������";
      else $browserTitle = "{$arParams["NAME"]} ������� �������� {$arParams["COMPANY_NAME"]} �� AgroBelarus.by";
      return array(
          "BROWSER_TITLE" => $browserTitle,
          "DESCRIPTION" => $obParser->html_cut(NotEmptyStringOf(array(
              trim(unquot(strip_tags($arParams["PREVIEW_TEXT"]))),
              trim(unquot(strip_tags($arParams["DETAIL_TEXT"])))
          )), 200),
          "KEYWORDS" => $arParams["TAGS"],
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerInterview extends MetaHandler
{
   private $iblock_id = 29;
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => StrUnion(array($arParams["PROP"]["FIO"]["VALUE"], $arParams["NAME"]), ": ")." ���������� ��� EnergoBelarus.by",
          "DESCRIPTION" => $obParser->html_cut(NotEmptyStringOf(array(
              trim(unquot(strip_tags($arParams["PREVIEW_TEXT"]))),
              trim(unquot(strip_tags($arParams["DETAIL_TEXT"])))
          )), 200),
          "KEYWORDS" => StrUnion(array(
              $arParams["PROP"]["FIO"]["VALUE"],
              $arParams["TAGS"]
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerInterviewAgro extends MetaHandler
{
   private $iblock_id = 56;
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => StrUnion(array($arParams["PROP"]["FIO"]["VALUE"], $arParams["NAME"]), ": ")." ���������� ��� AgroBelarus.by",
          "DESCRIPTION" => $obParser->html_cut(NotEmptyStringOf(array(
              trim(unquot(strip_tags($arParams["PREVIEW_TEXT"]))),
              trim(unquot(strip_tags($arParams["DETAIL_TEXT"])))
          )), 200),
          "KEYWORDS" => StrUnion(array(
              $arParams["PROP"]["FIO"]["VALUE"],
              $arParams["TAGS"]
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerCompany extends MetaHandler
{
   private $iblock_id = 17;
   protected function GetByID($elementId)
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsElement = CIBlockElement::GetList(
              array(),
              array("ID" => $elementId),
              false,
              false,
              array(
                  "ID",
                  "NAME",
                  "IBLOCK_SECTION_ID",
                  "PROPERTY_CITY",
                  "PROPERTY_CITY.ID",
                  "PROPERTY_CITY.NAME"
              )
      );
      if($arElement = $rsElement->GetNext())
      {
         //���������
         $arElement["CATEGORIES"] = array();
         $rsGroups = CIBlockElement::GetElementGroups($arElement["ID"]);
         while($group = $rsGroups->GetNext())
            $arElement["CATEGORIES"][] = $group["NAME"];
         $arElement["CATEGORIES_STRING"] = StrUnion($arElement["CATEGORIES"], ", ");
         //������ ������
         $arSectionTree = array();
         $rsNavChain = CIBlockSection::GetNavChain($arElement["IBLOCK_ID"], $arElement["IBLOCK_SECTION_ID"]);
         while($arNavChain = $rsNavChain->GetNext())
         {
            if(!in_array($arNavChain["NAME"], $arElement["CATEGORIES"]))
               $arSectionTree[] = $arNavChain["NAME"];
            if($arNavChain["ID"] == $arElement["IBLOCK_SECTION_ID"])
               $arElement["SECTION_NAME"] = $arNavChain["NAME"];
         }
         $arElement["SECTION_STRING"] = StrUnion($arSectionTree, ", ");
      }
      else $this->AddError("������� � ID {$elementId} �� ������");
      return $arElement;
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $city = NotEmptyStringOf(array($arParams["PROPERTY_CITY_NAME"], "��������"));
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => "�������� {$arParams["NAME"]}: {$city}, ���������� ������",
          "DESCRIPTION" => "�������� {$arParams["NAME"]} ������������ � �������� � ����������: {$arParams["CATEGORIES_STRING"]}. �����, �������, ���� ��������� ������� �������� {$arParams["NAME"]}.",
          "KEYWORDS" => StrUnion(array(
              "��������",
              $arParams["NAME"],
              $arParams["SECTION_STRING"],
              $arParams["CATEGORIES_STRING"],
              "�����, ����, ��������, ��������, �����",
              $arParams["PROPERTY_CITY_NAME"],
              "��������, ������������, �������, ������, ������"
              
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerCompanyAgro extends MetaHandler
{
   private $iblock_id = 65;
   protected function GetByID($elementId)
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsElement = CIBlockElement::GetList(
              array(),
              array("ID" => $elementId),
              false,
              false,
              array(
                  "ID",
                  "NAME",
                  "IBLOCK_SECTION_ID",
                  "PROPERTY_CITY",
                  "PROPERTY_CITY.ID",
                  "PROPERTY_CITY.NAME"
              )
      );
      if($arElement = $rsElement->GetNext())
      {
         //���������
         $arElement["CATEGORIES"] = array();
         $rsGroups = CIBlockElement::GetElementGroups($arElement["ID"]);
         while($group = $rsGroups->GetNext())
            $arElement["CATEGORIES"][] = $group["NAME"];
         $arElement["CATEGORIES_STRING"] = StrUnion($arElement["CATEGORIES"], ", ");
         //������ ������
         $arSectionTree = array();
         $rsNavChain = CIBlockSection::GetNavChain($arElement["IBLOCK_ID"], $arElement["IBLOCK_SECTION_ID"]);
         while($arNavChain = $rsNavChain->GetNext())
         {
            if(!in_array($arNavChain["NAME"], $arElement["CATEGORIES"]))
               $arSectionTree[] = $arNavChain["NAME"];
            if($arNavChain["ID"] == $arElement["IBLOCK_SECTION_ID"])
               $arElement["SECTION_NAME"] = $arNavChain["NAME"];
         }
         $arElement["SECTION_STRING"] = StrUnion($arSectionTree, ", ");
      }
      else $this->AddError("������� � ID {$elementId} �� ������");
      return $arElement;
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $city = NotEmptyStringOf(array($arParams["PROPERTY_CITY_NAME"], "��������"));
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => "�������� {$arParams["NAME"]}: {$city}, ���������� ������",
          "DESCRIPTION" => "�������� {$arParams["NAME"]} ������������ � �������� � ����������: {$arParams["CATEGORIES_STRING"]}. �����, �������, ���� ��������� ������� �������� {$arParams["NAME"]}.",
          "KEYWORDS" => StrUnion(array(
              "��������",
              $arParams["NAME"],
              $arParams["SECTION_STRING"],
              $arParams["CATEGORIES_STRING"],
              "�����, ����, ��������, ��������, �����",
              $arParams["PROPERTY_CITY_NAME"],
              "��������, ������������, �������, ������, ������"
              
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerTovars extends MetaHandler
{
   private $iblock_id = 27;
   protected function GetByID($elementId)
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsElement = CIBlockElement::GetList(
              array(),
              array("ID" => $elementId),
              false,
              false,
              array(
                  "ID",
                  "NAME",
                  "IBLOCK_SECTION_ID",
                  "PROPERTY_FIRM.ID",
                  "PROPERTY_FIRM.NAME",
                  "PROPERTY_FIRM.PROPERTY_CITY",
              )
      );
      if($arElement = $rsElement->GetNext())
      {
         //������ ������
         $arSectionTree = array();
         $rsNavChain = CIBlockSection::GetNavChain($arElement["IBLOCK_ID"], $arElement["IBLOCK_SECTION_ID"]);
         while($arNavChain = $rsNavChain->GetNext())
         {
            $arSectionTree[] = $arNavChain["NAME"];
            if($arNavChain["ID"] == $arElement["IBLOCK_SECTION_ID"])
               $arElement["SECTION_NAME"] = $arNavChain["NAME"];
         }
         $arElement["SECTION_STRING"] = StrUnion($arSectionTree, ", ");
         //����� ��������
         if($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"])
         {
            $rsCity = CIBlockElement::GetByID($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"]);
            if($arCity = $rsCity->GetNext())
            {
               $arElement["COMPANY_CITY"] = $arCity["NAME"];
            }
         }
      }
      else $this->AddError("������� � ID {$elementId} �� ������");
      return $arElement;
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $arParams["PROPERTY_FIRM_NAME"] = unquot($arParams["PROPERTY_FIRM_NAME"]);
      $city = NotEmptyStringOf(array($arParams["COMPANY_CITY"], "��������"));
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => "������ {$arParams["NAME"]} � {$city}: ����, ��������������",
          "DESCRIPTION" => "����������� �������������� � �������� {$arParams["NAME"]}. ������ ���� � ������ {$arParams["NAME"]} � {$city} � �������� {$arParams["PROPERTY_FIRM_NAME"]}. ������� � ��������� �������",
          "KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              $arParams["SECTION_STRING"],
              $arParams["PROPERTY_FIRM_NAME"],
              "������",
              $arParams["COMPANY_CITY"],
              "��������", "������", "������", "����������", "�������", "��������", "�����������", "��������", "��������������", "���", "�������"
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerTovarsAgro extends MetaHandler
{
   private $iblock_id = 58;
   protected function GetByID($elementId)
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsElement = CIBlockElement::GetList(
              array(),
              array("ID" => $elementId),
              false,
              false,
              array(
                  "ID",
                  "NAME",
                  "IBLOCK_SECTION_ID",
                  "PROPERTY_FIRM.ID",
                  "PROPERTY_FIRM.NAME",
                  "PROPERTY_FIRM.PROPERTY_CITY",
              )
      );
      if($arElement = $rsElement->GetNext())
      {
         //������ ������
         $arSectionTree = array();
         $rsNavChain = CIBlockSection::GetNavChain($arElement["IBLOCK_ID"], $arElement["IBLOCK_SECTION_ID"]);
         while($arNavChain = $rsNavChain->GetNext())
         {
            $arSectionTree[] = $arNavChain["NAME"];
            if($arNavChain["ID"] == $arElement["IBLOCK_SECTION_ID"])
               $arElement["SECTION_NAME"] = $arNavChain["NAME"];
         }
         $arElement["SECTION_STRING"] = StrUnion($arSectionTree, ", ");
         //����� ��������
         if($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"])
         {
            $rsCity = CIBlockElement::GetByID($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"]);
            if($arCity = $rsCity->GetNext())
            {
               $arElement["COMPANY_CITY"] = $arCity["NAME"];
            }
         }
      }
      else $this->AddError("������� � ID {$elementId} �� ������");
      return $arElement;
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $arParams["PROPERTY_FIRM_NAME"] = unquot($arParams["PROPERTY_FIRM_NAME"]);
      $city = NotEmptyStringOf(array($arParams["COMPANY_CITY"], "��������"));
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => "������ {$arParams["NAME"]} � {$city}: ����, ��������������",
          "DESCRIPTION" => "����������� �������������� � �������� {$arParams["NAME"]}. ������ ���� � ������ {$arParams["NAME"]} � {$city} � �������� {$arParams["PROPERTY_FIRM_NAME"]}. ������� � ��������� �������",
          "KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              $arParams["SECTION_STRING"],
              $arParams["PROPERTY_FIRM_NAME"],
              "������",
              $arParams["COMPANY_CITY"],
              "��������", "������", "������", "����������", "�������", "��������", "�����������", "��������", "��������������", "���", "�������"
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerYarmarka extends MetaHandler
{
   private $iblock_id = 81;
   protected function GetByID($elementId)
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsElement = CIBlockElement::GetList(
              array(),
              array("ID" => $elementId),
              false,
              false,
              array(
                  "ID",
                  "NAME",
				  "DETAIL_TEXT",
                  "IBLOCK_SECTION_ID",
                  "PROPERTY_FIRM.ID",
                  "PROPERTY_FIRM.NAME",
                  "PROPERTY_FIRM.PROPERTY_CITY",
              )
      );
      if($arElement = $rsElement->GetNext())
      {
         //������ ������
         $arSectionTree = array();
         $rsNavChain = CIBlockSection::GetNavChain($arElement["IBLOCK_ID"], $arElement["IBLOCK_SECTION_ID"]);
         while($arNavChain = $rsNavChain->GetNext())
         {
            $arSectionTree[] = $arNavChain["NAME"];
            if($arNavChain["ID"] == $arElement["IBLOCK_SECTION_ID"])
               $arElement["SECTION_NAME"] = $arNavChain["NAME"];
         }
         $arElement["SECTION_STRING"] = StrUnion($arSectionTree, ", ");
         //����� ��������
         if($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"])
         {
            $rsCity = CIBlockElement::GetByID($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"]);
            if($arCity = $rsCity->GetNext())
            {
               $arElement["COMPANY_CITY"] = $arCity["NAME"];
            }
         }
      }
      else $this->AddError("������� � ID {$elementId} �� ������");
      return $arElement;
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $arParams["PROPERTY_FIRM_NAME"] = unquot($arParams["PROPERTY_FIRM_NAME"]);
      $city = NotEmptyStringOf(array($arParams["COMPANY_CITY"], "��������"));
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => "{$arParams["NAME"]} � {$city} - �������� ���� � ������",
          "DESCRIPTION" => "{$arParams["DETAIL_TEXT"]}. ������ ����������� �� {$arParams["PROPERTY_FIRM_NAME"]}.",
          "KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              $arParams["SECTION_STRING"],
              $arParams["PROPERTY_FIRM_NAME"],
              "������",
              $arParams["COMPANY_CITY"],
              ��������, ������, ������, �������, ��������, ��������������, ���, �������,
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerServices extends MetaHandler
{
   private $iblock_id = 28;
   protected function GetByID($elementId)
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsElement = CIBlockElement::GetList(
              array(),
              array("ID" => $elementId),
              false,
              false,
              array(
                  "ID",
                  "NAME",
                  "IBLOCK_SECTION_ID",
                  "PROPERTY_FIRM.ID",
                  "PROPERTY_FIRM.NAME",
                  "PROPERTY_FIRM.PROPERTY_CITY",
              )
      );
      if($arElement = $rsElement->GetNext())
      {
         //������ ������
         $arSectionTree = array();
         $rsNavChain = CIBlockSection::GetNavChain($arElement["IBLOCK_ID"], $arElement["IBLOCK_SECTION_ID"]);
         while($arNavChain = $rsNavChain->GetNext())
         {
            $arSectionTree[] = $arNavChain["NAME"];
            if($arNavChain["ID"] == $arElement["IBLOCK_SECTION_ID"])
               $arElement["SECTION_NAME"] = $arNavChain["NAME"];
         }
         $arElement["SECTION_STRING"] = StrUnion($arSectionTree, ", ");
         //����� ��������
         if($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"])
         {
            $rsCity = CIBlockElement::GetByID($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"]);
            if($arCity = $rsCity->GetNext())
            {
               $arElement["COMPANY_CITY"] = $arCity["NAME"];
            }
         }
      }
      else $this->AddError("������� � ID {$elementId} �� ������");
      return $arElement;
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $arParams["PROPERTY_FIRM_NAME"] = unquot($arParams["PROPERTY_FIRM_NAME"]);
      $city = NotEmptyStringOf(array($arParams["COMPANY_CITY"], "��������"));
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => "{$arParams["NAME"]}, {$city}: �������� {$arParams["PROPERTY_FIRM_NAME"]}",
          "DESCRIPTION" => "{$arParams["SECTION_NAME"]}. ������ ����������� � �������� ����������� {$arParams["NAME"]} � ��������.",
          "KEYWORDS" => StrUnion(array(
              $arParams["SECTION_NAME"],
              $arParams["NAME"],
              "��������, ������, �������, �������, �����, ��������, �����������",
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerServicesAgro extends MetaHandler
{
   private $iblock_id = 64;
   protected function GetByID($elementId)
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsElement = CIBlockElement::GetList(
              array(),
              array("ID" => $elementId),
              false,
              false,
              array(
                  "ID",
                  "NAME",
                  "IBLOCK_SECTION_ID",
                  "PROPERTY_FIRM.ID",
                  "PROPERTY_FIRM.NAME",
                  "PROPERTY_FIRM.PROPERTY_CITY",
              )
      );
      if($arElement = $rsElement->GetNext())
      {
         //������ ������
         $arSectionTree = array();
         $rsNavChain = CIBlockSection::GetNavChain($arElement["IBLOCK_ID"], $arElement["IBLOCK_SECTION_ID"]);
         while($arNavChain = $rsNavChain->GetNext())
         {
            $arSectionTree[] = $arNavChain["NAME"];
            if($arNavChain["ID"] == $arElement["IBLOCK_SECTION_ID"])
               $arElement["SECTION_NAME"] = $arNavChain["NAME"];
         }
         $arElement["SECTION_STRING"] = StrUnion($arSectionTree, ", ");
         //����� ��������
         if($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"])
         {
            $rsCity = CIBlockElement::GetByID($arElement["PROPERTY_FIRM_PROPERTY_CITY_VALUE"]);
            if($arCity = $rsCity->GetNext())
            {
               $arElement["COMPANY_CITY"] = $arCity["NAME"];
            }
         }
      }
      else $this->AddError("������� � ID {$elementId} �� ������");
      return $arElement;
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $arParams["PROPERTY_FIRM_NAME"] = unquot($arParams["PROPERTY_FIRM_NAME"]);
      $city = NotEmptyStringOf(array($arParams["COMPANY_CITY"], "��������"));
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => "{$arParams["NAME"]}, {$city}: �������� {$arParams["PROPERTY_FIRM_NAME"]}",
          "DESCRIPTION" => "{$arParams["SECTION_NAME"]}. ������ ����������� � �������� ����������� {$arParams["NAME"]} � ��������.",
          "KEYWORDS" => StrUnion(array(
              $arParams["SECTION_NAME"],
              $arParams["NAME"],
              "��������, ������, �������, �������, �����, ��������, �����������",
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerTermin extends MetaHandler
{
   private $iblock_id = 39;
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => "{$arParams["NAME"]} - ���������� �������� �� EnrgoBelarus.by",
          "DESCRIPTION" => "����������� {$arParams["NAME"]} � ����������������� ������� �� �����������, ���������� ��������.",
          "KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              "������, ����������, �����������"
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerVideo extends MetaHandler
{
   private $iblock_id = 34;
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = unquot($arParams["NAME"]);
      $obParser = new CTextParser;
      return array(
          "BROWSER_TITLE" => "{$arParams["NAME"]}, ���� � ����� �� EnergoBelarus.by",
          "DESCRIPTION" => "������������ � ���� � ������������� {$arParams["NAME"]}",
          "KEYWORDS" => StrUnion(array(
              "����, �����, �����",
              $arParams["NAME"]
          ), ", "),
          "META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}


///////////////////////////////////////////////////////////////////////////////////////




class MetaHandlerSectionCompany extends MetaHandlerSection
{
   protected $iblock_id = 17;
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = trim(unquot($arParams["NAME"]));
      $obParser = new CTextParser;
      return array(
          "UF_SEO_TITLE" => "������� �������� �������� � �����: {$arParams["NAME"]} � ��������, ������, ������",
          "UF_DESCRIPTION" => "������� �������� ����������� ������ �/��� ��������� ������ � ����� {$arParams["NAME"]}. ��������, ������, ������, �������� �������� � ������� {$arParams["NAME"]}.",
          "UF_KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              "���������� �������������, ��������, �����, ���������, ����������, ��������, ��������, ����, �����, �������, �����, �������, ������, ������, ������, ��������, ��������, ��������, ����������, ����������, �������, �������, ��������������",
          ), ", "),
          "UF_META_AUTO" => GetUserFieldVariantId("Y", "UF_META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerSectionCompanyAgro extends MetaHandlerSection
{
   protected $iblock_id = 65;
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = trim(unquot($arParams["NAME"]));
      $obParser = new CTextParser;
      return array(
          "UF_SEO_TITLE" => "������� �������� �������� � �����: {$arParams["NAME"]} � ��������, ������, ������",
          "UF_DESCRIPTION" => "������� �������� ����������� ������ �/��� ��������� ������ � ����� {$arParams["NAME"]}. ��������, ������, ������, �������� �������� � ������� {$arParams["NAME"]}.",
          "UF_KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              "���������� �������������, ��������, �����, ���������, ����������, ��������, ��������, ����, �����, �������, �����, �������, ������, ������, ������, ��������, ��������, ��������, ����������, ����������, �������, �������, ��������������",
          ), ", "),
          "UF_META_AUTO" => GetUserFieldVariantId("Y", "UF_META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerSectionTovars extends MetaHandlerSection
{
   protected $iblock_id = 27;
   protected function GetByID($elementId)
   {
      $arSection = parent::GetByID($elementId);
      if($arSection["DEPTH_LEVEL"] > 0)
      {
         if(!CModule::IncludeModule("iblock")) return;
         $arTags = array();
         $rsElement = CIBlockElement::GetList(
                 array("ID" => "DESC"),
                 array("IBLOCK_ID" => $this->iblock_id, "SECTION_ID" => $arSection["ID"]),
                 false,
                 array("nPageSize" => 100),
                 array(
                     "ID",
                     "NAME",
                     "TAGS",
                 )
         );
         while($arElement = $rsElement->GetNext())
         {
            if(strlen(trim($arElement["TAGS"])))
            {
               $arElement["TAGS"] = explode(", ", $arElement["TAGS"]);
               foreach ($arElement["TAGS"] as $tag) $arTags[$tag] += 1;
            }
         }
         if(count($arTags))
         {
            arsort($arTags);
            array_splice($arTags, 5);
            $arTags = array_keys($arTags);
            $arTags = StrUnion($arTags, ", ");
            $arSection["KEYWORDS_STRING"] = $arTags;
         }
         return $arSection;
      }
      return array();
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = trim(unquot($arParams["NAME"]));
      $arParams["PROPERTY_FIRM_NAME"] = trim(unquot($arParams["PROPERTY_FIRM_NAME"]));
      $city = NotEmptyStringOf(array($arParams["COMPANY_CITY"], "��������"));
      $obParser = new CTextParser;
      return array(
          "UF_SEO_TITLE" => "{$arParams["NAME"]}: �������, ���� � ��������, ������ �� EnergoBelarus",
          "UF_DESCRIPTION" => "������� ����� {$arParams["NAME"]} � �������� EnergoBelarus. ��������� ����������� �������������� � ����. ��������� ��� �� ������ �� ���� ����������� ��������. ������� � ��������� �������.",
          "UF_KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              $arParams["KEYWORDS_STRING"],
              "�������, ������, �������, ����������, ����, ����������, ��������, ������������, ������, ���, �������",
          ), ", "),
          "UF_META_AUTO" => GetUserFieldVariantId("Y", "UF_META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerSectionTovarsAgro extends MetaHandlerSection
{
   protected $iblock_id = 58;
   protected function GetByID($elementId)
   {
      $arSection = parent::GetByID($elementId);
      if($arSection["DEPTH_LEVEL"] > 0)
      {
         if(!CModule::IncludeModule("iblock")) return;
         $arTags = array();
         $rsElement = CIBlockElement::GetList(
                 array("ID" => "DESC"),
                 array("IBLOCK_ID" => $this->iblock_id, "SECTION_ID" => $arSection["ID"]),
                 false,
                 array("nPageSize" => 100),
                 array(
                     "ID",
                     "NAME",
                     "TAGS",
                 )
         );
         while($arElement = $rsElement->GetNext())
         {
            if(strlen(trim($arElement["TAGS"])))
            {
               $arElement["TAGS"] = explode(", ", $arElement["TAGS"]);
               foreach ($arElement["TAGS"] as $tag) $arTags[$tag] += 1;
            }
         }
         if(count($arTags))
         {
            arsort($arTags);
            array_splice($arTags, 5);
            $arTags = array_keys($arTags);
            $arTags = StrUnion($arTags, ", ");
            $arSection["KEYWORDS_STRING"] = $arTags;
         }
         return $arSection;
      }
      return array();
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = trim(unquot($arParams["NAME"]));
      $arParams["PROPERTY_FIRM_NAME"] = trim(unquot($arParams["PROPERTY_FIRM_NAME"]));
      $city = NotEmptyStringOf(array($arParams["COMPANY_CITY"], "��������"));
      $obParser = new CTextParser;
      return array(
          "UF_SEO_TITLE" => "{$arParams["NAME"]}: �������, ���� � ��������, ������ �� AgroBelarus",
          "UF_DESCRIPTION" => "������� ����� {$arParams["NAME"]} � �������� AgroBelarus. ��������� ����������� �������������� � ����. ��������� ��� �� ������ �� ���� ����������� ��������. ������� � ��������� �������.",
          "UF_KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              $arParams["KEYWORDS_STRING"],
              "�������, ������, �������, ����������, ����, ����������, ��������, ������������, ������, ���, �������",
          ), ", "),
          "UF_META_AUTO" => GetUserFieldVariantId("Y", "UF_META_AUTO", $this->iblock_id)
      );
   }
}



class MetaHandlerSectionUslugiAgro extends MetaHandlerSection
{
   protected $iblock_id = 64;
   protected function GetByID($elementId)
   {
      $arSection = parent::GetByID($elementId);
      if($arSection["DEPTH_LEVEL"] > 1)
      {
         if(!CModule::IncludeModule("iblock")) return;
         $arTags = array();
         $rsElement = CIBlockElement::GetList(
                 array("ID" => "DESC"),
                 array("IBLOCK_ID" => $this->iblock_id, "SECTION_ID" => $arSection["ID"]),
                 false,
                 array("nPageSize" => 100),
                 array(
                     "ID",
                     "NAME",
                     "TAGS",
                 )
         );
         while($arElement = $rsElement->GetNext())
         {
            if(strlen(trim($arElement["TAGS"])))
            {
               $arElement["TAGS"] = explode(", ", $arElement["TAGS"]);
               foreach ($arElement["TAGS"] as $tag) $arTags[$tag] += 1;
            }
         }
         if(count($arTags))
         {
            arsort($arTags);
            array_splice($arTags, 5);
            $arTags = array_keys($arTags);
            $arTags = StrUnion($arTags, ", ");
            $arSection["KEYWORDS_STRING"] = $arTags;
         }
         return $arSection;
      }
      return array();
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = trim(unquot($arParams["NAME"]));
      $arParams["PROPERTY_FIRM_NAME"] = trim(unquot($arParams["PROPERTY_FIRM_NAME"]));
      $city = NotEmptyStringOf(array($arParams["COMPANY_CITY"], "��������"));
      $obParser = new CTextParser;
      return array(
          "UF_SEO_TITLE" => "{$arParams["NAME"]}: �������, ���� � ��������, ������ �� AgroBelarus",
          "UF_DESCRIPTION" => "������� ����� {$arParams["NAME"]} � �������� AgroBelarus. ��������� ����������� �������������� � ����. ��������� ��� �� ������ �� ���� ����������� ��������. ������� � ��������� �������.",
          "UF_KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              $arParams["KEYWORDS_STRING"],
              "�������, ������, �������, ����������, ����, ����������, ��������, ������������, ������, ���, �������",
          ), ", "),
          "UF_META_AUTO" => GetUserFieldVariantId("Y", "UF_META_AUTO", $this->iblock_id)
      );
   }
}
class MetaHandlerSectionYarmarka extends MetaHandlerSection
{
   protected $iblock_id = 81;
   protected function GetByID($elementId)
   {
      $arSection = parent::GetByID($elementId);
      if($arSection["DEPTH_LEVEL"] > 1)
      {
         if(!CModule::IncludeModule("iblock")) return;
         $arTags = array();
         $rsElement = CIBlockElement::GetList(
                 array("ID" => "DESC"),
                 array("IBLOCK_ID" => $this->iblock_id, "SECTION_ID" => $arSection["ID"]),
                 false,
                 array("nPageSize" => 100),
                 array(
                     "ID",
                     "NAME",
                     "TAGS",
                 )
         );
         while($arElement = $rsElement->GetNext())
         {
            if(strlen(trim($arElement["TAGS"])))
            {
               $arElement["TAGS"] = explode(", ", $arElement["TAGS"]);
               foreach ($arElement["TAGS"] as $tag) $arTags[$tag] += 1;
            }
         }
         if(count($arTags))
         {
            arsort($arTags);
            array_splice($arTags, 5);
            $arTags = array_keys($arTags);
            $arTags = StrUnion($arTags, ", ");
            $arSection["KEYWORDS_STRING"] = $arTags;
         }
         return $arSection;
      }
      return array();
   }
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = trim(unquot($arParams["NAME"]));
      $arParams["PROPERTY_FIRM_NAME"] = trim(unquot($arParams["PROPERTY_FIRM_NAME"]));
      $city = NotEmptyStringOf(array($arParams["COMPANY_CITY"], "��������"));
      $obParser = new CTextParser;
      return array(
          "UF_SEO_TITLE" => "{$arParams["NAME"]}: ������� ��������, �������� ����, ������ �� AgroBelarus",
          "UF_DESCRIPTION" => "������� ����� {$arParams["NAME"]} � �������� AgroBelarus. ��������� �������� ��������� � ����. ��������� ��� �� ������ �� ���� ����������� ��������.",
          "UF_KEYWORDS" => StrUnion(array(
              $arParams["NAME"],
              $arParams["KEYWORDS_STRING"],
              "{$arParams["NAME"]}, �������, ������, �������, ����, ����������, ��������, ������, ���, �������",
          ), ", "),
          "UF_META_AUTO" => GetUserFieldVariantId("Y", "UF_META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerSectionPhoto extends MetaHandlerSection
{
   protected $iblock_id = 10;
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = trim(unquot($arParams["NAME"]));
      $obParser = new CTextParser;
      return array(
          "UF_SEO_TITLE" => "{$arParams["NAME"]}, ���� � ����� �� EnergoBelarus.by",
          "UF_DESCRIPTION" => "������������ � ���� � ������������� {$arParams["NAME"]}",
          "UF_KEYWORDS" => StrUnion(array(
              "����, �����, �����",
              $arParams["NAME"]
          ), ", "),
          "UF_META_AUTO" => GetUserFieldVariantId("Y", "UF_META_AUTO", $this->iblock_id)
      );
   }
}

class MetaHandlerSectionPhotoAgro extends MetaHandlerSection
{
   protected $iblock_id = 62;
   protected function Algoritm($arParams)
   {
      $arParams["NAME"] = trim(unquot($arParams["NAME"]));
      $obParser = new CTextParser;
      return array(
          "UF_SEO_TITLE" => "{$arParams["NAME"]}, ���� � ����� �� AgroBelarus.by",
          "UF_DESCRIPTION" => "������������ � ���� � ������������� {$arParams["NAME"]}",
          "UF_KEYWORDS" => StrUnion(array(
              "����, �����, �����",
              $arParams["NAME"]
          ), ", "),
          "UF_META_AUTO" => GetUserFieldVariantId("Y", "UF_META_AUTO", $this->iblock_id)
      );
   }
}

//******************** blog ********************//
class MetaHandlerBlog extends MetaHandler
{
   private $iblock_id = 50;
   protected function GetByID($elementId){
      $arElement = parent::GetByID($elementId);
      //�������� ���� � ������ ��������
      return $arElement;
   }

   protected function Algoritm($arParams){
      //$arParams["NAME"] = unquot($arParams["NAME"]);
      //$obParser = new CTextParser;
      return array(
			"BROWSER_TITLE" => $arParams["NAME"].' - ����� - ������ �������������� ��������',
			"DESCRIPTION" => '��������� ���������� �������� ����� �� ���������� �������� � ����������.',
			"KEYWORDS" => '�����, ���������� �������, ����������',
			"META_AUTO" => GetPropertyVariantId("Y", "META_AUTO", $this->iblock_id)
      );
   }
}
