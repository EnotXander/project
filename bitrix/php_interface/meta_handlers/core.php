<?
//����� ������������ ����-������ ���������
abstract class MetaHandler
{
   protected $arElement = null;
   protected $arMeta = null;
   private $arSaveProp = array();
   //������
   public $error = false;
   public $arError = array();
   protected function AddError($message)
   {
      $this->error = true;
      $this->arError = $message;
   }
   public function ShowError()
   {
      PrintAdmin(implode("<br>", $this->arError));
   }
   public function GetElement()
   {
      return $this->arElement;
   }
   public function GetMeta()
   {
      return $this->arMeta;
   }
   public function GetSavedMeta()
   {
      return $this->arSaveProp;
   }

   //�����������
   public function __construct($arElement = null)
   {
      $this->arElement = $arElement;
      //��������� ������� ������
      if(is_array($this->arElement))
      {
         //TODO: �������������� ���������
      }
      elseif($this->arElement > 0)
      {
         $this->arElement = $this->GetByID($this->arElement);
      }
      elseif($this->arElement === null)
      {
         $this->AddError("� ���������� ������� ������ ��������");
      }
      else
      {
         $this->AddError("����������� ������ � �������� �����������: {$this->arElement}");
      }
      if($this->error) $this->ShowError();
   }
   
   //��������� ������ �� ��������. ����� ���� �������������
   protected function GetByID($arElement)
   {
      if(!CModule::IncludeModule("iblock")) return;
      $rsElement = CIBlockElement::GetByID($this->arElement);
      if($obElement = $rsElement->GetNextElement())
      {
         $this->arElement = $obElement->GetFields();
         $this->arElement["PROP"] = $obElement->GetProperties();
      }
      else $this->AddError("������� � ID {$this->arElement} �� ������");
      return $this->arElement;
   }
   //���������� ���� (�����������, ����� �������� �������������, � ����� ���). ������� �� ��������������� GetByID
   protected function FilterMeta($arMeta, $arElement)
   {
      $this->arSaveProp = array();
      foreach($this->arMeta as $propCode => $propVal)
      {
         //Unquot
         $propVal = str_replace('"', '', html_entity_decode($propVal));
         $propVal =  str_replace('&quot;', '', html_entity_decode($propVal));
         if(!isset($this->arElement["PROP"][$propCode]["VALUE"]) || ($this->arElement["PROP"][$propCode]["VALUE"] != $propVal))
            $this->arSaveProp[$propCode] = $propVal;
      }
      return $this->arSaveProp;
   }
   //���������� ������
   protected function UpdateMeta($realSave)
   {
      if(count($this->arSaveProp))
      {
         if(!CModule::IncludeModule("iblock")) return;
         $objElement = new CIBlockElement();
         if($realSave)
            $objElement->SetPropertyValuesEx($this->arElement["ID"], $this->arElement["IBLOCK_ID"], $this->arSaveProp);
         return true;
      }
      else return false;
   }
   
   //������ �����������
   public function Meta($realSave = true)
   {
      if(!$this->error)
      {
         $this->arMeta = $this->Algoritm($this->arElement);
         $this->arSaveProp = $this->FilterMeta($this->arMeta, $this->arElement);
         $this->UpdateMeta($realSave);
      }
      return $this->arSaveProp;
   }
   //����������
   abstract protected function Algoritm($arParams);
}

//��� ��������
abstract class MetaHandlerSection extends MetaHandler
{
   protected $iblock_id = null;
   //���������� ���� (�����������, ����� �������� �������������, � ����� ���). ������� �� ��������������� GetByID
   protected function FilterMeta($arMeta, $arElement)
   {
      $this->arSaveProp = array();
      foreach($this->arMeta as $propCode => $propVal)
      {
         //Unquot
         $propVal = str_replace('"', '', html_entity_decode($propVal));
         $propVal =  str_replace('&quot;', '', html_entity_decode($propVal));
         if(!isset($this->arElement[$propCode]) || ($this->arElement[$propCode] != $propVal))
            $this->arSaveProp[$propCode] = $propVal;
      }
      return $this->arSaveProp;
   }
   //���������� ������
   protected function UpdateMeta($realSave)
   {
      if(count($this->arSaveProp))
      {
         if(!CModule::IncludeModule("iblock")) return;
         $objSection = new CIBlockSection();
         if($realSave)
            $objSection->Update($this->arElement["ID"], $this->arSaveProp);
         return true;
      }
      else return false;
   }
   protected function GetByID($elementId)
   {
      if(!$this->iblock_id)
         $this->AddError("��� ������".__CLASS__."�� ������ �������� iblock_id");
      if(!CModule::IncludeModule("iblock")) return;
      $rsSection = CIBlockSection::GetList(
              array(),
              array("IBLOCK_ID" => $this->iblock_id, "ID" => $this->arElement),
              false,
              array(
                  "ID", "CODE", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID",
                  "EXTERNAL_ID", "TIMESTAMP_X", "SORT", "ACTIVE", "GLOBAL_ACTIVE",
                  "SECTION_PAGE_URL", "MODIFIED_BY", "DATE_CREATE", "CREATED_BY", "DEPTH_LEVEL", "UF_*"
                  )
      );
      if($arSection = $rsSection->GetNext())
      {
         $this->arElement = $arSection;
      }
      else $this->AddError("������� � ID {$this->arElement} �� ������");
      return $this->arElement;
   }
}

//����������
class MetaHandlerTest extends MetaHandler
{
   protected function GetByID($elementId)
   {
      return parent::GetByID($elementId);
   }
   protected function FilterMeta($arMeta, $arElement) {
      return parent::FilterMeta($arMeta, $arElement);
   }

   protected function Algoritm($arParams)
   {
      return array(
          "BROWSER_TITLE" => "BROWSER_TITLE_1111",
          "DESCRIPTION" => "DESCRIPTION_2222",
          "KEYWORDS" => "KEYWORDS_3333"
      );
   }
}