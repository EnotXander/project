<?php

namespace Glifery\Crm;

interface MessageInterface
{
    public function getFlushingArray();
}