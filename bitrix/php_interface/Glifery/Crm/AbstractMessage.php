<?php

namespace Glifery\Crm;

abstract class AbstractMessage
{
    /** @var integer */
    private $id;

    /** @var \DateTime */
    private $date;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setDate($date)
    {
        $this->date = new \DateTime($date);
    }

    public function getDate()
    {
        return $this->date;
    }
}