<?php

namespace Glifery\Crm;

class OutMessage extends AbstractMessage implements MessageInterface
{
    private $companyId = null;

    private $fieldsInfo = array();

    public function getFlushingArray()
    {
        if (is_null($this->companyId)) {
            throw new \Exception('Trying to log empty companyId');
        }
        if (!count($this->fieldsInfo)) {
            throw new \Exception('Trying to log empty fields info');
        }

        return array(
            'DIRECTION' => Logger::DIRECTION_OUT,
            'COMPANY_ID' => $this->companyId,
            'FIELDS_INFO' => json_encode($this->fieldsInfo)
        );
    }

    /**
     * @param mixed $companyId
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * @return null
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    public function addUpdatedField($field, $value)
    {
        if (is_array($value)) {
            $value = serialize($value);
        }

        $info = array($field => $value);

        $this->fieldsInfo[] = $info;
    }

    public function getUpdatedFields()
    {
        return $this->fieldsInfo;
    }
}