<?php

namespace Glifery\Crm;

use Glifery\Crm\AbstractMessage;
use Glifery\Crm\InMessage;

class MessageRepository
{
    const TABLE_NAME = 'a_crm_stat';
    const TABLE_NAME_AGRO = 'a_crm_stat_agro';
    const DIRECTION_IN = 'in';
    const DIRECTION_OUT = 'out';

    private $db;
    private $type;
    private $table;

    public function __construct($type='energo')
    {
        global $DB;
        $this->db = $DB;

        $this->type = $type;

        $this->table = $this->type =='agro' ? self::TABLE_NAME_AGRO : self::TABLE_NAME;

    }

    /**
     * @param array $row
     * @return InMessage
     */
    private function createInMessageFromDbRow(array $row)
    {
        $message = new InMessage();
        $this->fillCommonFieldsFromDbRow($message, $row);

        $message->setCount($row['COUNT']);

        return $message;
    }

    /**
     * @param array $row
     * @return OutMessage
     * @throws \Exception
     */
    private function createOutMessageFromDbRow(array $row)
    {
        $message = new OutMessage();
        $this->fillCommonFieldsFromDbRow($message, $row);

        $message->setCompanyId($row['COMPANY_ID']);
        if (!strlen($row['FIELDS_INFO'])) {
            throw new \Exception('Fields info in message with id '.$row['ID'].' is empty');
        }

        $fieldsInfo = json_decode($row['FIELDS_INFO'], true);
        if (!is_array($fieldsInfo)) {
            throw new \Exception('Can\' decode from JSON field info: '.serialize($row['FIELDS_INFO']));
        }

        foreach ($fieldsInfo as $fieldInfoElement) {
            foreach ($fieldInfoElement as $field => $value) {
                $message->addUpdatedField($field, $value);
            }
        }

        return $message;
    }

    /**
     * @param AbstractMessage $message
     * @param array $row
     */
    private function fillCommonFieldsFromDbRow(AbstractMessage $message, array $row)
    {
        $message->setId($row['ID']);
        $message->setDate($row['DATE']);
    }

    public function addMessage(AbstractMessage $message)
    {
        $fieldsInfo = $message->getFlushingArray();
        if (!count($fieldsInfo)) {
            throw new \Exception('Trying to flush empty message');
        }

        $fieldsArray = array_keys($fieldsInfo);
        $fieldsArray = array_map(function($element) {
                return '`'.$element.'`';
            }, $fieldsArray);
        $fieldsPart = implode(', ',$fieldsArray);

        $db = $this->db;
        $valuesArray = array_map(function($element) use ($db) {
                return '\''.$db->ForSql($element).'\'';
            }, $fieldsInfo);
        $valuesPart = implode(', ',$valuesArray);


        $query = "INSERT INTO  `".$this->table."` (".$fieldsPart.") VALUES (".$valuesPart.");";
        $this->db->Query($query);
    }

    /**
     * @return array
     */
    public function findInMessages()
    {
        $messages = array();

        $query = 'SELECT * FROM '.$this->table.' WHERE `DATE` > (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) and  `DIRECTION` = \''.self::DIRECTION_IN.'\' ORDER BY `ID` DESC';
        $result = $this->db->Query($query);
        while ($row = $result->Fetch()) {
            $message = $this->createInMessageFromDbRow($row);

            $messages[] = $message;
        }

        return $messages;
    }

    /**
     * @return array
     */
    public function findOutMessages()
    {
        $messages = array();

        $query = 'SELECT * FROM '.$this->table.' WHERE `DATE` > (DATE_SUB(CURDATE(), INTERVAL 1 MONTH))  and `DIRECTION` = \''.self::DIRECTION_OUT.'\' ORDER BY `ID` DESC';
        $result = $this->db->Query($query);
        while ($row = $result->Fetch()) {
            $message = $this->createOutMessageFromDbRow($row);

            $messages[] = $message;
        }

        return $messages;
    }
}