<?php

namespace Glifery\Crm;

class InMessage extends AbstractMessage implements MessageInterface
{
    /**
     * @var integer
     */
    private $count = null;

    /**
     * @throws \Exception
     * @return array
     */
    public function getFlushingArray()
    {
        if (is_null($this->count)) {
            throw new \Exception('Trying to log empty message');
        }

        return array(
            'DIRECTION' => Logger::DIRECTION_IN,
            'COUNT' => $this->count
        );
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}