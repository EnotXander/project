<?php

namespace Glifery\Crm;

class Logger
{
    const DIRECTION_IN = 'in';
    const DIRECTION_OUT = 'out';

    private $messageRepo;

    public function __construct($site='energo')
    {
        global $DB;
        $this->db = $DB;

        $this->messageRepo = new MessageRepository($site);
    }

    static public function createMessage($direction)
    {
        switch ($direction) {
            case self::DIRECTION_IN:
                return new InMessage();
                break;
            case self::DIRECTION_OUT:
                return new OutMessage();
                break;
            default:
                throw new \Exception('Unknown logget message type: '.$direction);
        }
    }

    public function log(MessageInterface $message)
    {
        $this->messageRepo->addMessage($message);
    }
}