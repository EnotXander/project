<?php
/**
 * User: ZvvAsyA
 * Date: 13.08.15 -  14:57
 */

class Companies {

	public function CompaniesInSection($iblock, $section){

		// ������ �������� � ������
		return cacheFunction( 36000, 'COMPANY_LIST' . $section, function () use ($iblock, $section ) {
			$res           = [ ];
			$rsAllElements = CIBlockElement::GetList(
				array( "PROPERTY_FIRM.PROPERTY_EXT_SORT" => "DESC" ),
				array(
					"IBLOCK_ID"           => $iblock,
					"ACTIVE"              => "Y",
					"SECTION_ID"          => $section,
					"INCLUDE_SUBSECTIONS" => "Y"
				),
				false,
				false,
				array(
					"ID",
					"NAME",
					"IBLOCK_SECTION_ID",
					"PROPERTY_FIRM.ID",
					"PROPERTY_FIRM.NAME",
					"PROPERTY_FIRM.DETAIL_PAGE_URL",
					"PROPERTY_FIRM.PROPERTY_REMOVE_REL",
					"PROPERTY_FIRM.PROPERTY_YARMARKA",
					"PROPERTY_FIRM.PROPERTY_EXT_SORT"
				)
			);
			while ( $arAllElements = $rsAllElements->GetNext() ) {
				//TODO: ������
				if ( empty($arAllElements["PROPERTY_FIRM_ID"]) ) {
					continue;
				}
				//���������� � ��������
				if ( ! isset( $res[ $arAllElements["PROPERTY_FIRM_ID"] ] ) ) {
					//���� �������� ���������, �� ������� ���� � ��������
					if ($iblock == IBLOCK_YARMARKA){
						if ( $arAllElements["PROPERTY_FIRM_PROPERTY_YARMARKA_ENUM_ID"] == false
							) {
							continue;
						}
					} else {

						if ( $arAllElements["PROPERTY_FIRM_PROPERTY_REMOVE_REL_ENUM_ID"] != false

							) {
							//continue;
						}
					}


					$res[ $arAllElements["PROPERTY_FIRM_ID"] ] = array(
						"ID"                => $arAllElements["PROPERTY_FIRM_ID"],
						"NAME"              => $arAllElements["PROPERTY_FIRM_NAME"],
						"DETAIL_PAGE_URL"   => $arAllElements["PROPERTY_FIRM_DETAIL_PAGE_URL"],
						"EXT_SORT"          => $arAllElements["PROPERTY_FIRM_PROPERTY_EXT_SORT_VALUE"],
						"ITEMS_COUNT"       => 1
					);
				} else {
					$res[ $arAllElements["PROPERTY_FIRM_ID"] ]["ITEMS_COUNT"] ++;
				}

			}

		return $res;
		} );
	}
}