<?
function GA() {
   $obCache = new CPHPCache;
   $lifeTime = 60 * 60 * 24;
   $cacheID = "GA-" . date("m/d/Y");
   $str_visits="";
   $str_pageviews="";
   
	if ($obCache->InitCache($lifeTime, $cacheID, "/") ) {
		$tmp = $obCache->GetVars();
		$res_str  = $tmp[$cacheID];
	}
	else {
		require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/gapi/gapi.class.php');

		$ga = new gapi(ga_email, ga_p12);
		$ga->requestReportData(
			ga_profile_id,
			array('userDefinedValue'),
			array('uniqueEvents', 'pageviews','visits', 'visitors'),
			null, null, date("Y-m-d", strtotime("-1 day", time())), date("Y-m-d", strtotime("-1 day", time())),
			1, 100
		);
		$visits = $ga->getVisits();
		$pageviews = $ga->getPageviews();

		if((int)$visits > 0){
			$str_visits = $visits." ".pluralForm($visits, '���������', '���������', '���������');
		}

		if((int)$pageviews > 0){
			$str_pageviews = $pageviews." ".pluralForm($visits, '��������', '���������', '����������');
		}

		$res_str = "<p>�����: ".$str_visits.", ".$str_pageviews." �������.<br>�� ����������� ������ Google Analytics, ��� ����� ��������� ������� �����������.</p>";

		
		$obCache->StartDataCache();
		$obCache->EndDataCache(array($cacheID => $res_str));
   }
   
   return $res_str;
}

function pluralForm($n, $form1, $form2, $form5) {
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $form5;
    if ($n1 > 1 && $n1 < 5) return $form2;
    if ($n1 == 1) return $form1;
    return $form5;
}
