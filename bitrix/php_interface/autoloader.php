<?php

class autoloader
{
    const PATH_LOCAL = '/bitrix/php_interface/';

    private function getClassPath($classFullName)
    {
        $fileName = str_replace('\\', '/', $classFullName).'.php';
        $fullFilePath = $_SERVER['DOCUMENT_ROOT'].self::PATH_LOCAL.$fileName;

        return $fullFilePath;
    }

    public function loadClass($classFullName)
    {
        $fullFilePath = $this->getClassPath($classFullName);
        if (file_exists($fullFilePath)) {
            require_once($fullFilePath);
        }
    }

    static function init()
    {
        $autoloader = new self;
        spl_autoload_register(array($autoloader, 'loadClass'));
    }
}

autoloader::init();