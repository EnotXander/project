<?
$MESS["CLU_DBNODE_LIST_TITLE"] = "Sharding Vertical: M�dulo de la Base de datos";
$MESS["CLU_DBNODE_LIST_ID"] = "ID";
$MESS["CLU_DBNODE_LIST_FLAG"] = "Estado";
$MESS["CLU_DBNODE_NOCONNECTION"] = "desconectado";
$MESS["CLU_DBNODE_UPTIME"] = "subirhora";
$MESS["CLU_DBNODE_LIST_ACTIVE"] = "Activar";
$MESS["CLU_DBNODE_LIST_STATUS"] = "Estado";
$MESS["CLU_DBNODE_LIST_NAME"] = "Nombre";
$MESS["CLU_DBNODE_LIST_DB_HOST"] = "Servidor";
$MESS["CLU_DBNODE_LIST_DB_NAME"] = "Base de Datos";
$MESS["CLU_DBNODE_LIST_DB_LOGIN"] = "Usuario";
$MESS["CLU_DBNODE_LIST_MODULES"] = "M�dulos";
$MESS["CLU_DBNODE_LIST_DESCRIPTION"] = "Descripci�n";
$MESS["CLU_DBNODE_LIST_ADD"] = "Agregar Nueva Base de Datos";
$MESS["CLU_DBNODE_LIST_ADD_TITLE1"] = "Activar el asistente de conexi�n de la base de datos";
$MESS["CLU_DBNODE_LIST_ADD_TITLE2"] = "Agregar Conexi�n de la Nueva Base de Datos";
$MESS["CLU_DBNODE_LIST_EDIT"] = "Editar";
$MESS["CLU_DBNODE_LIST_START_USING_DB"] = "Usar Base de Datos";
$MESS["CLU_DBNODE_LIST_STOP_USING_DB"] = "Detener Usando Esta Base de Datos";
$MESS["CLU_DBNODE_LIST_NOTE1"] = "Las tablas de datos de ciertos m�dulos pueden ser trasladados a una base de datos aparte, como la distribuci�n de la carga entre varios servidores de base de datos. Para mover las tablas, primero haga clic en \"A�adir Nueva base de datos\". Despu�s de haber agregado una base de datos, usted tiene que transferir los datos del m�dulo usando uno de los m�todos siguientes. </ p><p>La primera manera es aplicable s�lo a MySQL. Buscar la base de datos requeridos en la lista y seleccione la opci�n \"Usar la base de datos\" en la acci�n de men�. A continuaci�n, siga las instrucciones del asistente. </ p>
 <p> Como alternativa, puede desinstalar e instalar el m�dulo de nuevo. En el primer paso del asistente, seleccione la base de datos del m�dulo va a utilizar. Las tablas de bases de datos actualmente existentes no ser�n transferidos. </ p>
Los siguientes m�dulos de apoyo de agrupamiento:";
$MESS["CLU_DBNODE_LIST_DELETE"] = "Eliminar";
$MESS["CLU_DBNODE_LIST_DELETE_CONF"] = "Eliminar conexi�n?";
$MESS["CLU_DBNODE_LIST_REFRESH"] = "Actualizar";
$MESS["CLU_DBNODE_LIST_NOTE2"] = "<p><i> sharding vertical implica delegaci�n de las consultas de los m�dulos seleccionados se divide entre dos o m�s bases de datos. Cada m�dulo seleccionado tiene una conexi�n de base de datos designada. </i> </p>
 <p><i> sharding horizontal implica la distribuci�n uniforme de datos (por ejemplo, perfiles de usuario) a trav�s de bases de datos separadas. </i> </p>";
$MESS["CLU_DBNODE_UPTIME_UNKNOWN"] = "desconocido";
?>