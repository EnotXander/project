<?
$MESS["CLU_SLAVE_LIST_TITLE"] = "Bases de datos Esclavas";
$MESS["CLU_SLAVE_LIST_ID"] = "ID";
$MESS["CLU_SLAVE_LIST_FLAG"] = "Estado";
$MESS["CLU_SLAVE_NOCONNECTION"] = "desconectado";
$MESS["CLU_SLAVE_UPTIME"] = "subirhora";
$MESS["CLU_SLAVE_LIST_BEHIND"] = "Latencia (seg)";
$MESS["CLU_SLAVE_LIST_STATUS"] = "Estado";
$MESS["CLU_SLAVE_LIST_NAME"] = "Nombre";
$MESS["CLU_SLAVE_LIST_DB_HOST"] = "Servidor";
$MESS["CLU_SLAVE_LIST_DB_NAME"] = "Base de datos";
$MESS["CLU_SLAVE_LIST_DB_LOGIN"] = "Usuario";
$MESS["CLU_SLAVE_LIST_WEIGHT"] = "Peso (%)";
$MESS["CLU_SLAVE_LIST_DESCRIPTION"] = "Descripci�n";
$MESS["CLU_SLAVE_LIST_ADD"] = "Agregar Base de datos Esclava";
$MESS["CLU_SLAVE_LIST_ADD_TITLE"] = "Ejecutar Asistentede la Nueva Base de Datos Esclava";
$MESS["CLU_SLAVE_LIST_EDIT"] = "Editar";
$MESS["CLU_SLAVE_LIST_START_USING_DB"] = "Usar Base de Datos";
$MESS["CLU_SLAVE_LIST_SKIP_SQL_ERROR"] = "Ignorar error";
$MESS["CLU_SLAVE_LIST_SKIP_SQL_ERROR_ALT"] = "Ignorar un s�lo error SQL y continuar la replicaci�n";
$MESS["CLU_SLAVE_LIST_DELETE"] = "Borrar";
$MESS["CLU_SLAVE_LIST_DELETE_CONF"] = "Eliminar conexi�n?";
$MESS["CLU_SLAVE_LIST_PAUSE"] = "Pausa";
$MESS["CLU_SLAVE_LIST_RESUME"] = "Resumen";
$MESS["CLU_SLAVE_LIST_REFRESH"] = "Actualizar";
$MESS["CLU_SLAVE_LIST_STOP"] = "No utlizar la Base de Datos";
$MESS["CLU_SLAVE_BACKUP"] = "Backup";
$MESS["CLU_MAIN_LOAD"] = "Carga M�nima";
$MESS["CLU_SLAVE_LIST_NOTE"] = "<p>La replicaci�n de bases de datos es la creaci�n y el mantenimiento de m�ltiples copias de la misma base de datos que ofrece las dos caracter�sticas principales: </ p>
 <p>
 1) distribuir la carga entre una base de datos principal y una o m�s bases de datos de esclavos; <br>
 2) uso de esclavos como una espera activa. <br>
 </ p>
 <p> Importante! <br>
 - Utilice s�lo los servidores independientes con mayor conectividad posible para la replicaci�n <br>.
 - El proceso de replicaci�n se inicia copiando el contenido de bases de datos. Durante dicho per�odo de tiempo, la secci�n p�blica del sitio web no estar� disponible, pero el panel de control se mantiene accesible. Cualquier modificaci�n de datos que ocurren durante la replicaci�n pueda afectar el funcionamiento adecuado del sitio web. <br>
 </ p>
Directrices <p> replicaci�n <br>
Paso 1: Inicie el asistente haciendo clic en \"A�adir Esclavo base de datos \". El asistente comprobar� la configuraci�n del servidor y sugieren que se agrega una base de datos de esclavos. <br>
Paso 2: Encuentra una base de datos requeridos en la lista y seleccione la opci�n \"Usar la base de datos\" en el men� de acciones <br>.
Paso 3: Siga las instrucciones del asistente <br>.
 </ p>";
$MESS["CLU_SLAVE_LIST_MASTER_ADD"] = "Agregar base de datos maestra/esclava";
$MESS["CLU_SLAVE_LIST_MASTER_ADD_TITLE"] = "Ejecutar el asistente de la nueva base de datos maestro/esclava";
?>