<?
$MESS["CLU_DBNODE_EDIT_ERROR"] = "Conexi�n especficada es inv�lida.";
$MESS["CLU_DBNODE_EDIT_TAB"] = "Conexi�n";
$MESS["CLU_DBNODE_EDIT_TAB_TITLE"] = "Editar Par�metros de la Conexi�n de la Base de Datos";
$MESS["CLU_DBNODE_EDIT_SAVE_ERROR"] = "Error al guardar la conexi�n de la base de datos.";
$MESS["CLU_DBNODE_EDIT_EDIT_TITLE"] = "Editar Par�metros de la base de datos";
$MESS["CLU_DBNODE_EDIT_ADD_TITLE"] = "Conex�n de la Nueva Base de Datos";
$MESS["CLU_DBNODE_EDIT_MENU_LIST"] = "Conexiones";
$MESS["CLU_DBNODE_EDIT_MENU_LIST_TITLE"] = "Conexiones";
$MESS["CLU_DBNODE_EDIT_MENU_DELETE"] = "Eliminar";
$MESS["CLU_DBNODE_EDIT_MENU_DELETE_TITLE"] = "Eliminar la Conexi�n";
$MESS["CLU_DBNODE_EDIT_MENU_DELETE_CONF"] = "Est� seguro que desea eliminar esta conexi�n?";
$MESS["CLU_DBNODE_EDIT_ID"] = "ID";
$MESS["CLU_DBNODE_EDIT_ACTIVE"] = "Activar";
$MESS["CLU_DBNODE_EDIT_NAME"] = "Nombre";
$MESS["CLU_DBNODE_EDIT_DB_HOST"] = "Cadena de Conexi�n de la Base de Datos";
$MESS["CLU_DBNODE_EDIT_DB_NAME"] = "Base de datos";
$MESS["CLU_DBNODE_EDIT_DB_LOGIN"] = "Usuario";
$MESS["CLU_DBNODE_EDIT_DB_PASSWORD"] = "Contrase�a";
$MESS["CLU_DBNODE_EDIT_DESCRIPTION"] = "Descripci�n.";
$MESS["CLU_DBNODE_EDIT_ORACLE_DB_NAME"] = "Cadena de conexi�n de la base de datos";
?>