<?
$MESS["CLUWIZ_ERROR_ACCESS_DENIED"] = "Error! Acceso denegado.";
$MESS["CLUWIZ_CONNECTION_ERROR"] = "Error al conectarse a la base de datos.";
$MESS["CLUWIZ_QUERY_ERROR"] = "Error consultar la base de datos.";
$MESS["CLUWIZ_TABLE_PROGRESS"] = "Tabla: #table_name#<br />Filas movidas: #records#<br />Tablas de la derecha: #tables#";
$MESS["CLUWIZ_ALL_DONE"] = "Las tablas han sido transferidas exitosamente. La replicaci�n ahora est� en progreso (ONLINE).";
$MESS["CLUWIZ_INIT"] = "Inicializando...";
$MESS["CLUWIZ_SITE_OPEN"] = "Oprimir \"Terminar\" para hacer la secci�n p�blica del sitio web disponible para los visitantes.";
?>