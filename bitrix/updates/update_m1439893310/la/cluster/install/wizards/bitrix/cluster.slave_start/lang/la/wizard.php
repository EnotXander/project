<?
$MESS["CLUWIZ_NO_MODULE_ERROR"] = "El m�dulo de la Web Cluster no est� instalado. El asistente podr� abortar ahora.";
$MESS["CLUWIZ_STEP2_NO_TABLES"] = "Ninguna de las tablas fue encontrada; la trasferencia de datos es segura para empezar.";
$MESS["CLUWIZ_STEP2_TABLES_EXIST"] = "La base de datos especificada contiene tablas. Esas tabls deben ser borradas para continuar.";
$MESS["CLUWIZ_STEP2_TABLES_LIST"] = "ver tablas";
$MESS["CLUWIZ_STEP2_DELETE_TABLES"] = "Permitir tablas en la base de datos #database# para ser borrada";
$MESS["CLUWIZ_STEP2_WARNING"] = "La secci�n del sitio web p�blico podia no estar disponible durante la transferencia de datos.";
$MESS["CLUWIZ_STEP3_TITLE"] = "Borrando Tablas";
$MESS["CLUWIZ_STEP4_TITLE"] = "Moviendo Tablas";
$MESS["CLUWIZ_FINALSTEP_BUTTONTITLE"] = "Finalizar";
$MESS["CLUWIZ_CANCELSTEP_TITLE"] = "El aistente ha sido cancelado.";
$MESS["CLUWIZ_CANCELSTEP_BUTTONTITLE"] = "Cerrar";
$MESS["CLUWIZ_CANCELSTEP_CONTENT"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_INIT"] = "Inicializaci�n";
$MESS["CLUWIZ_DATABASE_NOT_SUPPORTED"] = "El asistente no soporta el tipo de base especiicada.";
$MESS["CLUWIZ_NO_NODE_ERROR"] = "La base de datos esclava no est� especificada. El asistente podr� anularse ahora.";
$MESS["CLUWIZ_NO_CONN_ERROR"] = "Error al conectar a la base esclava. El asistente podr� anularse ahora.";
$MESS["CLUWIZ_STEP2_TITLE"] = "Comprobaci�n de las tablas de la base de datos Esclava";
$MESS["CLUWIZ_NO_GROUP_ERROR"] = "El grupo del servidor no est� definido.";
$MESS["CLUWIZ_NO_MASTER_ERROR"] = "El grupo del servidor no tiene una base de datos maestra funcionando.";
?>