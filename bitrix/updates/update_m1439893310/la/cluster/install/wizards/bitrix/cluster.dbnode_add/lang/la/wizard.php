<?
$MESS["CLUWIZ_STEP1_TITLE"] = "Asistente del Nuevo M�dulo de la Base de Datos";
$MESS["CLUWIZ_NO_MODULE_ERROR"] = "El m�dulo de la Web Cluster no est� instalado. El asistente podr� anularse.";
$MESS["CLUWIZ_CHEKED"] = "Comprobado.";
$MESS["CLUWIZ_STEP3_TITLE"] = "Par�metros de la Conexi�n de la Nueva Base de Datos";
$MESS["CLUWIZ_STEP3_DB_HOST"] = "Cadena de Conexi�n de la Base de Datos";
$MESS["CLUWIZ_STEP3_DB_NAME"] = "Base de Datos";
$MESS["CLUWIZ_STEP3_DB_LOGIN"] = "Usuario";
$MESS["CLUWIZ_STEP3_DB_PASSWORD"] = "Contrase�a";
$MESS["CLUWIZ_STEP4_TITLE"] = "Verificaci�n de los Par�metros de la Base de Datos";
$MESS["CLUWIZ_FINALSTEP_TITLE"] = "Finalizando el Asistente de la Conexi�n de la Base de Datos";
$MESS["CLUWIZ_FINALSTEP_BUTTONTITLE"] = "Finalizar";
$MESS["CLUWIZ_FINALSTEP_NAME"] = "Nombre de la Conexi�n";
$MESS["CLUWIZ_CANCELSTEP_TITLE"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_CANCELSTEP_BUTTONTITLE"] = "Cerrar";
$MESS["CLUWIZ_CANCELSTEP_CONTENT"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_DATABASE_NOT_SUPPORTED"] = "El asistente no soporta el tipo de base de datos especificada.";
$MESS["CLUWIZ_STEP4_CONN_ERROR"] = "La conexi�n de la base de datos no pudo ser establecida. Por favor regrese hacia el paso previo y modifique los par�metros de la conexi�n.";
$MESS["CLUWIZ_STEP1_CONTENT"] = "El asistente podr� ayudarlo aagregar una nueva base de datos y a verificar los par�metros principales de la base de datos primaria esencial para edecuado agrupamiento de la web.<br />";
?>