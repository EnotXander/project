<?
$MESS["MAIN_RESTORE_DEFAULTS"] = "Prdeterminado";
$MESS["opt_def_from"] = "Direcci�n predeterminada del remitente:";
$MESS["opt_method_agent"] = "Gestor";
$MESS["opt_method_cron"] = "Cron";
$MESS["opt_max_per_hit"] = "M�ximo de envios de e-mails durante una sola ejecuci�n:";
$MESS["opt_interval"] = "Segundos de espera entre el env�o de intentos (0 - mandan todos a la vez):";
$MESS["opt_method"] = "M�todo de env�o autom�tico:";
$MESS["opt_reiterate_method"] = "Compruebe si hay boletines recurrentes utilizados:";
$MESS["opt_reiterate_interval"] = "Compruebe si hay boletines recurrentes cada (segundo):";
$MESS["opt_address_from"] = "Direcciones para el uso en campo Desde:";
$MESS["opt_address_send_to_me"] = "Direcciones para enviar mensajes de prueba a:";
$MESS["opt_sender_cron_support"] = "Cron no se puede utilizar actualmente. Es compatible con la versio 15.0.9. de kernel. Su versi�n kernel es";
$MESS["opt_auto_agent_interval"] = "Demora entre su posterior env�o de trabajos (Seg. 0 - no esperar)";
$MESS["opt_unsub_link"] = "Dejar p�gina de suscripci�n personalizada:";
$MESS["opt_sub_link"] = "P�gina de suscripci�n personalizada:";
?>