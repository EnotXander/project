<?
$MESS ['IBLOCK_TYPE'] = "Tipo de block de informaci�n";
$MESS ['IBLOCK_IBLOCK'] = "Block de informaci�n";
$MESS ['IBLOCK_DETAIL_URL'] = "Ver detalle de la p�gina";
$MESS ['IBLOCK_ELEMENT_ID'] = "ID del elemento";
$MESS ['P_COMMENTS_TYPE'] = "Comentarios del componente";
$MESS ['P_COMMENTS_TYPE_BLOG'] = "Blog";
$MESS ['P_COMMENTS_TYPE_FORUM'] = "Foro";
$MESS ['F_COMMENTS_COUNT'] = "Comentarios por p�gina";
$MESS ['F_BLOG_URL'] = "Blog para comentarios";
$MESS ['P_PATH_TO_USER'] = "Ruta al perfil de los usuarios";
$MESS ['P_PATH_TO_BLOG'] = "Ruta al blog";
$MESS ['F_PATH_TO_SMILE'] = "Ruta a la carpeta de los Smiles (relativo a la ra�z)";
$MESS ['F_FORUM_ID'] = "ID del foro";
$MESS ['F_USE_CAPTCHA'] = "Usar CAPTCHA";
$MESS ['F_READ_TEMPLATE'] = "P�gina de tema le�do";
$MESS ['F_PREORDER'] = "Visualizar mensajes en orden ascendente";
?>