<?
$MESS ['LDAP_USER_CONFIRM_TYPE_NAME'] = "Confirmaci�n de Registro ";
$MESS ['LDAP_USER_CONFIRM_TYPE_DESC'] = "#USER_ID# -  ID del usuario
#EMAIL# - E-mail
#LOGIN# - sesi�n
#XML_ID# -  ID Externo
#BACK_URL# - Returnar al URL";
$MESS ['LDAP_USER_CONFIRM_EVENT_NAME'] = "#SITE_NAME#: Confirmaci�n de Registro ";
$MESS ['LDAP_USER_CONFIRM_EVENT_DESC'] = "Saludos desde #SITE_NAME#!
------------------------------------------
Hola,

usted ha recibido este mensaje porque usted (o alguien m�s) utiliza su direcci�n de e-mail para inscribirse en
#SERVER_NAME#.
Para confirmar la inscripci�n, haga clic en el siguiente enlace e introduzca el nombre y contrase�a que utiliza en la red local:

http://#SERVER_NAME#/bitrix/admin/ldap_user_auth.php?ldap_user_id=#XML_ID#&back_url=#BACK_URL#

Este es un mensaje automatizado.";
?>