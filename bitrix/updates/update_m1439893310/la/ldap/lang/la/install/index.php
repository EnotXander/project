<?
$MESS ['LDAP_MODULE_NAME'] = "Conector AD/LDAP ";
$MESS ['LDAP_MODULE_DESC'] = "M�dulo conector para AD/LDAP";
$MESS ['LDAP_UNINSTALL_TITLE'] = "Desinstalar el m�dulo conector AD/LDAP ";
$MESS ['LDAP_UNINSTALL_WARNING'] = "Peligro� El m�dulo ser� desintalado.";
$MESS ['LDAP_UNINSTALL_SAVEDATA'] = "Para guardar los datos almacenados en la base de datos, haga check en \"guardar tablas\"";
$MESS ['LDAP_UNINSTALL_SAVETABLE'] = "Guardar tabla";
$MESS ['LDAP_UNINSTALL_DEL'] = "Desinstalar";
$MESS ['LDAP_UNINSTALL_ERROR'] = "Error al eliminar:";
$MESS ['LDAP_UNINSTALL_COMPLETE'] = "Desintalaci�n completa.";
$MESS ['LDAP_INSTALL_BACK'] = "Regresar al administrador del m�dulo";
$MESS ['LDAP_MOD_INST_ERROR'] = "Error al instalar el m�dulo conector AD/LDAP";
$MESS ['LDAP_MOD_INST_ERROR_PHP'] = "Para el correcto funcionamiento del m�dulo PHP con el m�dulo de LDAP debe ser instalado. Entrar en contacto con el administrador de sistema por favor . ";
$MESS ['LDAP_INSTALL_TITLE'] = "Instalaci�n del m�dulo conector AD/LDAP";
?>