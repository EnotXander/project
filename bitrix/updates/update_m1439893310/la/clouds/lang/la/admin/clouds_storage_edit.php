<?
$MESS["CLO_STORAGE_EDIT_TAB"] = "Conexi�n";
$MESS["CLO_STORAGE_EDIT_TAB_TITLE"] = "Configurar los par�metros de la conexi�n de almacenamiento en la nube";
$MESS["CLO_STORAGE_EDIT_TAB2"] = "Reglas";
$MESS["CLO_STORAGE_EDIT_TAB2_TITLE"] = "Establecer las reglas de selecci�n de archivos para el almacenamiento en la nube";
$MESS["CLO_STORAGE_EDIT_EDIT_TITLE"] = "Cambia los par�metros de conexi�n del almacenamiento en la nube";
$MESS["CLO_STORAGE_EDIT_ADD_TITLE"] = "Agrega una nueva conexi�n al almacenamiento en la nube";
$MESS["CLO_STORAGE_EDIT_MENU_LIST"] = "Conexiones";
$MESS["CLO_STORAGE_EDIT_MENU_LIST_TITLE"] = "Mostrar las conexiones disponibles para el almacenamiento en la nube";
$MESS["CLO_STORAGE_EDIT_ID"] = "ID";
$MESS["CLO_STORAGE_EDIT_ACTIVE"] = "Activar";
$MESS["CLO_STORAGE_EDIT_SORT"] = "Clasificar";
$MESS["CLO_STORAGE_EDIT_SERVICE_ID"] = "Proveedor";
$MESS["CLO_STORAGE_EDIT_LOCATION"] = "Localizaciones";
$MESS["CLO_STORAGE_EDIT_BUCKET"] = "Cubo";
$MESS["CLO_STORAGE_EDIT_READ_ONLY"] = "S�lo lectura";
$MESS["CLO_STORAGE_EDIT_CNAME"] = "Nombre can�nico del dominio (<a href=\"http://en.wikipedia.org/wiki/CNAME_record\">CNAME</a>)";
$MESS["CLO_STORAGE_EDIT_MODULE"] = "M�dulos";
$MESS["CLO_STORAGE_EDIT_EXTENSIONS"] = "Extensiones";
$MESS["CLO_STORAGE_EDIT_SIZE"] = "Tama�os";
$MESS["CLO_STORAGE_EDIT_ADD_FILE_RULE"] = "Agregar nueva regla";
$MESS["CLO_STORAGE_EDIT_SAVE_ERROR"] = "Error al guardar la conexi�n de almacenamiento en la nube";
$MESS["CLO_STORAGE_EDIT_RULES_NOTE"] = "La regla se activa s�lo si un archivo cumple todas las condiciones de la misma. Si un archivo no coincide con al menos una regla, no ser� puesto en la nube.";
$MESS["CLO_STORAGE_EDIT_RULES_NOTE1"] = "Identificadores del M�dulo. Ejemplo: iblock, advertising. Listado vacio de m�dulos coincide con todos los archivos de todos los m�dulos.";
$MESS["CLO_STORAGE_EDIT_RULES_NOTE2"] = "Las extensiones de los archivos se pueden almacenar en la nube. Ejemplo: gif, png, gif, jpg. La lista de extensiones vac�as coincide con cualquier archivo. Esta lista es case-insensitive.";
$MESS["CLO_STORAGE_EDIT_RULES_NOTE3"] = "El tama�o de los archivos. El uso de los sufijos tama�o de archivo para indicar el tama�o de archivo: K, M o G. rangos de tama�o son posibles, por ejemplo: 1K, 1M, 2,3 millones. Si este campo est� vac�o, cualquier tama�o de la altura del archivo podr� coincidir.";
?>