<?
$MESS["SEC_FILTER_TITLE"] = "Filtro proactivo";
$MESS["SEC_FILTER_MAIN_TAB"] = "Filtro proactivo";
$MESS["SEC_FILTER_MAIN_TAB_TITLE"] = "Habilitar o desactivar el filtro proactivo";
$MESS["SEC_FILTER_ON"] = "Protecci�n Proactiva est� habilitada.";
$MESS["SEC_FILTER_EXCL_FOUND"] = "Existen exclusiones del filtro.";
$MESS["SEC_FILTER_OFF"] = "Protecci�n Proactiva desactivada.";
$MESS["SEC_FILTER_BUTTON_OFF"] = "Protecci�n Proactiva deshabilitada";
$MESS["SEC_FILTER_BUTTON_ON"] = "Habilitar Protecci�n Proactiva";
$MESS["SEC_FILTER_PARAMETERS_TAB"] = "Activar reacci�n";
$MESS["SEC_FILTER_PARAMETERS_TAB_TITLE"] = "Configurar la reacci�n del sistema frente a intrusiones";
$MESS["SEC_FILTER_ACTION"] = "Reacci�n de intrusi�n";
$MESS["SEC_FILTER_ACTION_FILTER"] = "Hacer la base segura";
$MESS["SEC_FILTER_ACTION_CLEAR"] = "Borrar los datos peligrosos";
$MESS["SEC_FILTER_LOG"] = "Agregar intento de intrusi�n a  <a href=\"#HREF#\">Log</a>";
$MESS["SEC_FILTER_EXCEPTIONS_TAB"] = "Excepciones";
$MESS["SEC_FILTER_EXCEPTIONS_TAB_TITLE"] = "Especificar m�scara para las URL�s que no podr� ser rechazadas";
$MESS["SEC_FILTER_SITE"] = "para el sitio:";
$MESS["SEC_FILTER_MASKS"] = "Excepciones:<br>(ejemplo: /bitrix/* o */news/*)";
$MESS["SEC_FILTER_ADD"] = "Agregar";
$MESS["SEC_FILTER_NOTE"] = "<p>El  <b>Filtro Proativo </b> (Web Application Firewall*) previene al sitio de la mayor&iacute;a de ataques web conocidos. El filtro reconoce y bloquea amenazas e ingresos peligrosos.</p>
<p>El Filtro Proactivo es el camino m&aacute;s efectivo de protegerse contra defectos en la implementaci&ccedil;on de un oproyecto web. (XSS, inyecci&oacute;n SQL, inclusiones PHP, etc)</p>
<p>Usted podr&aacute; conseguir r&aacute;pidamente ls &uacute;ltimas actualizaciones de su Filtro din&aacute;mico usando nusra tecnolog&iacute;a SITEUPDAT.</p>
<p>Tenga en cuenta que algunas acciones e intervenciones inofencivas de algunos usuario, pueden ser conideras por el sitema, haciendo reacionar al FiltroProactivo.</p>
<p><b>*WAF (Web Application Firewall)</b> es un sistema especial de seguirdad basado en el filtrado del tr&aacute;fico del sitio.</p>
<p><i>Recomendado para el nivel estandard </i></p>";
$MESS["SEC_FILTER_ACTION_NOTE_1"] = "La data ingresada ser&aacute; modificada. Por ejemplo: &quot;select&quot; ser&aacute; reemplazado con &quot;sel&nbsp;ect&quot;, &quot;&lt;script&gt;&quot;  con &quot;&lt;sc&nbsp;ript&gt;&quot;.";
$MESS["SEC_FILTER_STOP"] = "Agregar IP de atacantes a la Lista de Detenidos";
$MESS["SEC_FILTER_ACTION_NONE"] = "Saltar datos peligrosos";
$MESS["SEC_FILTER_DURATION"] = "Agregar a la lista de detenidos por (min.)";
$MESS["SEC_FILTER_ACTION_NOTE_2"] = "Al no filtrar posibles ataques est� permitiendo que su sitio est� disponible para cualquier persona con malas intenciones.";
?>