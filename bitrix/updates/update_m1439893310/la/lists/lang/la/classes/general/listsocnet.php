<?
$MESS["LISTS_SOCNET_TAB"] = "Listas";
$MESS["LISTS_SOCNET_LOG_GROUP"] = "Listas";
$MESS["LISTS_DEL_SOCNET_LOG_GROUP"] = "Listas (supresión)";
$MESS["LISTS_SOCNET_LOG_GROUP_SETTINGS"] = "Todos los cambios en las listas de este grupo";
$MESS["LISTS_SOCNET_LOG_TITLE"] = "Agregar o actualizar item de lista \"#TITLE#\"";
$MESS["LISTS_SOCNET_LOG_TITLE_MAIL"] = "Agregar o actualizar item de lista \"#TITLE#\" en el grupo \"#ENTITY#\"";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE"] = "Eliminar item de lista \"#TITLE#\"";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE_MAIL"] = "Eliminar item de lista \"#TITLE#\" en el grupo \"#ENTITY#\"";
?>