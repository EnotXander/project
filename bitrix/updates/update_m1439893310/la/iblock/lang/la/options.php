<?
$MESS["IBLOCK_USE_HTMLEDIT"] = "Usar el editor visual";
$MESS["IBLOCK_COMBINED_LIST_MODE"] = "Vista combinada de los elementos de las secciones";
$MESS["IBLOCK_MENU_MAX_SECTIONS"] = "El n�mero m�ximo de secciones en el men� ";
$MESS["IBLOCK_PATH2RSS"] = "Ruta para exportar archivos RSS";
$MESS["IBLOCK_SHOW_LOADING_CODE"] = "Mostrar c�digo desde fuente externa DB";
$MESS["IBLOCK_EVENT_LOG"] = "Registrar los eventos de los Bloques de Informaci�n";
$MESS["IBLOCK_LIST_IMAGE_SIZE"] = "Ver tama�o de la imagen en las listas";
$MESS["IBLOCK_DETAIL_IMAGE_SIZE"] = "Ver tama�o de la imagen en la vista de edici�n";
$MESS["IBLOCK_NUM_CATALOG_LEVELS"] = "N�mero m�ximo de sub-secciones anidadas para la exportaci�n e importaci�n de CSV";
?>