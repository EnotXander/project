<?
$MESS["SONET_ERROR_UPDATE"] = "Error al actualizar par�metros del tema.";
$MESS["SONET_UPDATE_ALT"] = "Editar par�metros del tema";
$MESS["SONET_DELETE_ALT"] = "Borrar un tema";
$MESS["SONET_DELETE_CONF"] = "Est� usted seguro que desea borrar este tema?";
$MESS["SONET_ADD_NEW"] = "Nuevo tema";
$MESS["SONET_ADD_NEW_ALT"] = "Oprimir para agregar a un nuevo tema";
$MESS["SONET_DELETE_ERROR"] = "Error al borrar el tema.";
$MESS["SONET_SUBJECT_NAV"] = "Temas";
$MESS["SONET_SUBJECT_NAME"] = "Nombre";
$MESS["SONET_SUBJECT_SITE_ID"] = "Sitio";
$MESS["SONET_TITLE"] = "Temas del grupo";
$MESS["SONET_FILTER_SITE_ID"] = "Sitio";
$MESS["SONET_SPT_ALL"] = "[todos]";
$MESS["SONET_SUBJECT_SORT"] = "Orden";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Seleccionado:";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Marcado:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "eliminar";
?>