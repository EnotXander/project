<?
$MESS["PATH_TO_USER_TIP"] = "La ruta a la p�gina del perfil del usuario. Ejemplo: sonet_user.php?page=user&user_id=#user_id#.";
$MESS["PATH_TO_USER_EDIT_TIP"] = "La ruta a la p�gina del editor del perfil. Ejemplo:";
$MESS["PAGE_VAR_TIP"] = "Especificar aqu� el nombre de la variable de la p�gina de Social Network";
$MESS["USER_VAR_TIP"] = "Especifique aqu� el nombre de una variable a la que se pasar� el ID de usuario de social network,";
$MESS["ID_TIP"] = "Especificar el c�digo que eval�a el ID de un usuario.";
$MESS["SET_TITLE_TIP"] = "Si la opci�n est� activa, el t�tulo de la p�gina podr�a ser fijado <nobr><b>Perfil para</b> <i>\"user name\"</i>.</nobr>";
$MESS["USER_PROPERTY_TIP"] = "Seleccionar aqu� las propiedades adicionales que podr�n mostrarse en el perfil del usuario.";
?>