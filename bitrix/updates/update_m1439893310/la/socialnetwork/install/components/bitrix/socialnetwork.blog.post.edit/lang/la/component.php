<?
$MESS["BLOG_MODULE_NOT_INSTALL"] = "El m�dulo Blog no est� instalado.";
$MESS["B_B_MES_NO_BLOG"] = "No se encuentra el mensaje ";
$MESS["BLOG_POST_EDIT"] = "Editar mensaje";
$MESS["BLOG_NEW_MESSAGE"] = "Nueva conversaci�n";
$MESS["BLOG_ERR_NO_RIGHTS"] = "Error: permisos insuficientes para escribir mensaje";
$MESS["BLOG_P_INSERT"] = "Haga clic para insertar una imagen";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "Las conversaciones no est�n disponibles para este usuario.";
$MESS["BLG_NAME"] = "Iniciado por";
$MESS["BLG_GRP_NAME"] = "Iniciado el";
$MESS["BPE_HIDDEN_POSTED"] = "Su mensaje ha sido agregado con �xito. Su mensajes en esta conversaci�n est�n pre-moderados, tu mensaje ser� visible tan pronto como el moderador lo haya aprobado.";
$MESS["BPE_SESS"] = "Su sesi�n ha caducado. Por favor, publicar de nuevo el mensaje.";
$MESS["BPE_COPY_DELETE_ERROR"] = "Error al intentar eliminar el mensaje original.<br />";
$MESS["B_B_MES_NO_GROUP"] = "No se encontr� el grupo de Social Network.";
$MESS["B_B_MES_NO_GROUP_ACTIVE"] = "Esta caracter�stica no est� disponible para el grupo de Social Network.";
$MESS["B_B_MES_NO_GROUP_RIGHTS"] = "Permisos suficientes para crear un puesto en el grupo de Social Network";
$MESS["BLOG_BLOG_BLOG_MES_DEL_ERROR"] = "Eliminar mensaje de error";
$MESS["BLOG_BLOG_BLOG_MES_DEL_NO_RIGHTS"] = "No tiene permiso para eliminar este mensaje";
$MESS["BLOG_BLOG_BLOG_MES_DEL_OK"] = "El mensaje fue eliminado";
$MESS["BLOG_BLOG_SESSID_WRONG"] = "Su sesi�n ha caducado. Por favor, int�ntelo de nuevo.";
$MESS["BLOG_BPE_EXTRANET_ERROR"] = "La conversaci�n no puede ser dirigida a todos, por favor, especifique al menos un destinatario.";
$MESS["BLOG_SONET_GROUP_MODULE_NOT_AVAIBLE"] = "Las conversaciones no est�n disponibles para este grupo de usuarios.";
$MESS["BLOG_GRAT_IBLOCKELEMENT_NAME"] = "Apreciaci�n de #GRAT_NAME#";
$MESS["BLOG_EMPTY_TITLE_PLACEHOLDER"] = "Imagen";
$MESS["BLOG_BPE_DESTINATION_EMPTY"] = "Especifique por lo menos a una persona.";
$MESS["B_B_HIDDEN_GROUP"] = "Grupo oculto";
$MESS["B_B_PC_DUPLICATE_POST"] = "Ya hemos agregado su mensaje";
?>