<?
$MESS["BPABL_STATE_MODIFIED"] = "Fecha del estado actual";
$MESS["BPABL_STATE_NAME"] = "Estado actual del proceso de negocios";
$MESS["BPABL_LOG"] = "Historial del proceso de negocios";
$MESS["BPABL_TYPE_1"] = "Acci�n lanzada '#ACTIVITY#'#NOTE#";
$MESS["BPABL_TYPE_2"] = "Acci�n finalizado '#ACTIVITY#', retornar al estado: '#STATUS#', resultado: '#RESULT#'#NOTE#";
$MESS["BPABL_TYPE_3"] = "Acci�n cancelada '#ACTIVITY#'#NOTE#";
$MESS["BPABL_TYPE_4"] = "Error de acci�n '#ACTIVITY#'#NOTE# ";
$MESS["BPABL_TYPE_5"] = "Acci�n '#ACTIVITY#'#NOTE#";
$MESS["BPABL_TYPE_6"] = "Algo se activ� en la acci�n '#ACTIVITY#'#NOTE#";
$MESS["BPABL_STATUS_1"] = "Inicializado";
$MESS["BPABL_STATUS_2"] = "En progreso";
$MESS["BPABL_STATUS_3"] = "Iniciar cancelaci�n";
$MESS["BPABL_STATUS_4"] = "Finalizado";
$MESS["BPABL_STATUS_5"] = "Error";
$MESS["BPABL_STATUS_6"] = "Indefinido";
$MESS["BPABL_RES_1"] = "No";
$MESS["BPABL_RES_2"] = "�xito";
$MESS["BPABL_RES_3"] = "Cancelado";
$MESS["BPABL_RES_4"] = "Error";
$MESS["BPABL_RES_5"] = "No se inicializ�";
$MESS["BPABL_RES_6"] = "Indefinido";
$MESS["BPWC_WLCT_TOTAL"] = "Total";
?>