<?
$MESS["SONET_GCE_ERR_SPAM_PERMS"] = "No se establecen permisos para enviar mensajes a grupos de trabajo";
$MESS["SONET_MODULE_NOT_INSTALL"] = "El m�dulo Social network no est� instalado.";
$MESS["SONET_P_USER_NO_USER"] = "No se encontr� el usuario.";
$MESS["SONET_GCE_GROUP_EDIT"] = "Editar grupo";
$MESS["SONET_GCE_GROUP_CREATE"] = "Crear grupo";
$MESS["SONET_GCE_ERR_NAME"] = "No se especifica el nombre del grupo.";
$MESS["SONET_GCE_ERR_SUBJECT"] = "No se especifica la descripci�n del grupo.";
$MESS["SONET_GCE_ERR_PERMS"] = "No se ha especificado el permiso de la invitaci�n.";
$MESS["SONET_GCE_ERR_SECURITY"] = "No tienes permiso para editar par�metros de grupo.";
$MESS["SONET_GCE_IP_OWNER"] = "S�lo el propietario del grupo";
$MESS["SONET_GCE_IP_MOD"] = "Moderadores y propietario del grupo";
$MESS["SONET_GCE_IP_USER"] = "Todos los miembros del grupo";
$MESS["SONET_GCE_IP_ALL"] = "Todos los usuarios";
$MESS["SONET_GCE_CANNOT_USER_ADD"] = "No tienes permiso para crear nuevos grupos.";
$MESS["SONET_GCE_NO_USERS"] = "Los destinatarios de la invitaci�n no se especifican.";
$MESS["SONET_GCE_ERR_DESCR"] = "No se especifica la descripci�n del grupo.";
$MESS["SONET_GCE_ERR_CANT_CREATE"] = "No tienes permiso para crear nuevos grupos.";
$MESS["SONET_GCE_CANNOT_EMAIL_ADD"] = "Esta direcci�n de correo electr�nico (#EMAIL#) ya est� en uso por otro usuario.";
$MESS["SONET_GCE_CANNOT_EMAILS_ADD"] = "Las direcciones #EMAIL# ya est�n en uso por otros usuarios.";
$MESS["SONET_GCE_USER_BANNED_IN_GROUP"] = "Un usuario con e-mail #EMAIL# est� prohibido para este grupo.";
$MESS["SONET_GCE_USER_REQUEST_SENT"] = "La invitaci�n ya ha sido enviado a un usuario con este correo electr�nico #EMAIL#.";
$MESS["SONET_GCE_USER_IN_GROUP"] = "Un usuario con correo electr�nico #EMAIL# ya es un miembro del grupo.";
?>