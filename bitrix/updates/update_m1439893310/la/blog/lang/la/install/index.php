<?
$MESS["BLOG_INSTALL_NAME"] = "Blog";
$MESS["BLOG_INSTALL_DESCRIPTION"] = "Con este m�dulo usted puede instalar, almacenar y administrar blog desde su sitio web.";
$MESS["BLOG_INSTALL_TITLE"] = "Instalaci�n del m�dulo Blog";
$MESS["BLI_PERM_D"] = "leer blogs";
$MESS["BLI_PERM_K"] = "ver p�ginas publicas";
$MESS["BLI_PERM_N"] = "crear blogs";
$MESS["BLI_PERM_R"] = "ver p�ginas en el Control Panel";
$MESS["BLI_PERM_W"] = "acceso completo";
$MESS["BLI_COPY_PUBLIC_FILES"] = "Instalar la secci�n p�blica";
$MESS["BLI_COPY_FOLDER"] = "Carpeta donde cada archivo ser� copiado (relativo a la raiz del sitio)";
$MESS["BLOG_INSTALL_PUBLIC_REW"] = "Sobreescribir archivos existentes";
$MESS["BLI_INSTALL_EMAIL"] = "Crear plantillas de e-mail";
$MESS["BLI_DELETE_EMAIL"] = "Eliminar plantillas de e-mail";
$MESS["BLI_INSTALL_404"] = "Instalar archivos p�blicos que use 404 error handler.";
$MESS["BLI_INSTALL_SMILES"] = "Instalar smiles";
?>