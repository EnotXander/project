<?
$MESS["ID_TIP"] = "Especificar el c�digo en el cual el ID del blog del env�o podr� ser transmitido.";
$MESS["BLOG_URL_TIP"] = "Especificar el c�digo en el cual el ID del blog podr� ser transmitido.";
$MESS["PATH_TO_BLOG_TIP"] = "La ruta a la p�gina del blog principal. Ejemplo: blog_blog.php?page=blog&blog=#blog#.";
$MESS["PATH_TO_USER_TIP"] = "La ruta a la p�gina del perfil del usuario. Ejemplo: <nobr>blog_user.php?page=user&user_id=#user_id#.</nobr>";
$MESS["PATH_TO_SMILE_TIP"] = "La ruta a la carpeta que contiene emoticons.";
$MESS["BLOG_VAR_TIP"] = "Especificar aqu� el nombre de la variable a la cual el ID del blog podr� ser pasado.";
$MESS["USER_VAR_TIP"] = "Especificar aqu� el nombre de la variable a la cual el ID del blog del usuario podr� ser pasado.";
$MESS["PAGE_VAR_TIP"] = "Especificar aqu� el nombre de la variable a la cual la p�gina del blog de la p�gina podr� ser transmitido.";
$MESS["COMMENTS_COUNT_TIP"] = "N�mero m�ximo de comentarios que pueden ser mostrados en la p�gina. Otros comentarios podr�n estar disponibles a trav�s de los links del breadcrumb de navegaci�n.";
$MESS["CACHE_TIME_TIP"] = "Especificar aqu� el per�odo de tiempo durante el cual el cach� es v�lido.";
$MESS["POST_VAR_TIP"] = "Especificar aqu� el nombre de la variable para la cual el ID del blog del env�o podr� pasar.";
$MESS["SIMPLE_COMMENT_TIP"] = "En el modo simple, comentarios pueden ser agregados a un env�o nulo (usado con la fotogaler�a 2.0).";
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: el cach� es v�lido durante el tiempo predefinido en las configuraciones del cach�;<br /><i>Cache</i>: siempre es el cach� para el tiempo especificado en el siguiente campo;<br /><i> ning�n cach�</i>: no se realiz� el almacenamiento en la memoria.";
?>