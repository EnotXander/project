<?
$MESS["CATALOG_SET_BUY"] = "Comprar";
$MESS["CATALOG_SET_DISCOUNT_DIFF"] = "(Guardar #PRICE#)";
$MESS["CATALOG_SET_BUY_SET"] = "Conjunto de compras";
$MESS["CATALOG_SET_CONSTRUCT"] = "Crear conjunto";
$MESS["CATALOG_SET_POPUP_LOADER"] = "Cargando...";
$MESS["CATALOG_SET_POPUP_TITLE_BAR"] = "Crear su propio conjunto";
$MESS["CATALOG_SET_POPUP_TITLE"] = "Agregar elemento para establecer";
$MESS["CATALOG_SET_POPUP_DESC"] = "Arrastre un producto a una caja vac�a o haga clic en �l";
$MESS["CATALOG_SET_SUM"] = "Total";
$MESS["CATALOG_SET_DISCOUNT"] = "Descuento";
$MESS["CATALOG_SET_WITHOUT_DISCOUNT"] = "Sin descuento";
$MESS["CATALOG_SET_ADDED2BASKET"] = "El conjunto se ha a�adido a la cesta de la compra.";
$MESS["CATALOG_SET_BUTTON_BUY"] = "Ver el carrito de la compras";
?>