<?
$MESS["RSRV_INCORRECT_ID"] = "ID de producto no v�lida.";
$MESS["RSRV_ID_NOT_FOUND"] = "El producto ##PRODUCT_ID# no se ha encontrado.";
$MESS["RSRV_UNKNOWN_ERROR"] = "Error desconocido al reservar producto #PRODUCT_NAME# (##PRODUCT_ID#).";
$MESS["RSRV_QUANTITY_NEGATIVE_ERROR"] = "El almac�n muestra cantidad negativa para el producto #PRODUCT_NAME# (##PRODUCT_ID#).";
$MESS["DDCT_UNKNOWN_ERROR"] = "Error desconocido al enviar el producto #PRODUCT_NAME# (##PRODUCT_ID#).";
$MESS["DDCT_DEDUCTION_QUANTITY_ERROR"] = "Cantidad insuficiente para enviar el producto #PRODUCT_NAME# (##PRODUCT_ID#).";
$MESS["DDCT_DEDUCTION_STORE_ERROR"] = "Los almacenes no est�n especificados a enviar producto #PRODUCT_NAME# (##PRODUCT_ID#).";
$MESS["DDCT_DEDUCTION_SAVE_ERROR"] = "Ha ocurrido un error desconocido al guardar la cantidad de #PRODUCT_NAME# (##PRODUCT_ID#) en el almac�n.";
$MESS["DDCT_DEDUCTION_WRITE_ERROR"] = "Error al actualizar informaci�n de env�o de producto #PRODUCT_NAME# (##PRODUCT_ID#).";
$MESS["DDCT_DEDUCTION_QUANTITY_STORE_ERROR"] = "La cantidad de producto #PRODUCT_NAME# (##PRODUCT_ID#) en el almac�n ##STORE_ID# es insuficiente para el env�o.";
$MESS["RSRV_QUANTITY_NOT_ENOUGH_ERROR"] = "Producto #PRODUCT_NAME# (##PRODUCT_ID#) tiene stock suficiente.";
$MESS["DDCT_DEDUCTION_BARCODE_ERROR"] = "Barcode \"#BARCODE#\" no fue encontrado para el producto \"#PRODUCT_NAME#\" (##PRODUCT_ID#).";
$MESS["CATALOG_QUANTITY_NOT_ENOGH"] = "Usted quiere comprar #QUANTITY# pcs. de #NAME#; sin embargo, s�lo #CATALOG_QUANTITY# pcs. est�n disponibles.";
$MESS["CATALOG_NO_QUANTITY_PRODUCT"] = "#NAME# est� fuera de stock.";
$MESS["CATALOG_ERR_NO_PRODUCT"] = "El producto no ha sido encontrado.";
$MESS["DDCT_DEDUCTION_MULTI_BARCODE_EMPTY"] = "C�digo de barras del producto \"#PRODUCT_NAME# en almacen ##STORE_ID# no ha sido especificado";
?>