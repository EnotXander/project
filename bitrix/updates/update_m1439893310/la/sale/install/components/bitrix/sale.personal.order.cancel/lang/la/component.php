<?
$MESS["SALE_MODULE_NOT_INSTALL"] = "El m�dulo e-Store no est� instalado";
$MESS["SALE_ACCESS_DENIED"] = "Necesitas autorizarse para anular la orden ";
$MESS["SPOC_TITLE"] = "Cancelar Orden # #ID#";
$MESS["SPOC_NO_ORDER"] = "Orden # #ORDER_ID# no fue encontrada.";
$MESS["SPOC_CANCEL_ORDER"] = "Las ordenes entregadas y/o pagadas no pueden ser anuladas. ";
?>