<?
$MESS["PATH_TO_DETAIL_TIP"] = "La ruta a la p�gina de detalles del pedido.";
$MESS["PATH_TO_COPY_TIP"] = "La ruta a la p�gina de duplicaci�n. Usted puede especificar la ruta al carrito de compras aqu� en el cual podr� copiar el pedido y tenerlo listo para el check out.";
$MESS["PATH_TO_CANCEL_TIP"] = "La ruta a la p�gina de cancelaci�n del pedido.";
$MESS["PATH_TO_BASKET_TIP"] = "La ruta a la p�gina del carrito de compras del cliente.";
$MESS["ORDERS_PER_PAGE_TIP"] = "Especifica la cantidad de pedidos por p�gina. Otros pedidos estar�n disponibles a trav�s de los v�nculos del breadcrumb de navegaci�n.";
$MESS["SET_TITLE_TIP"] = "Comprobar esta opci�n para fijar el t�tulo para \"Mis Pedidos\".";
$MESS["ID_TIP"] = "El ID del pedido que ser� usado para cosntruir links para ver p�gina del pedido etc.";
$MESS["HISTORIC_STATUSES_TIP"] = "Estados que se pueden utilizar para filtrar el historial. Si el par�metro \"filter_history\" no es Y sino una orden est� en uno de estos estados, ser� rechazada y no se muestra en los resultados de b�squeda.";
?>