<?
$MESS["TSBS_READY"] = "Elementos en su carrito de compras:";
$MESS["TSBS_PRICE"] = "Precio:";
$MESS["TSBS_QUANTITY"] = "Cantidad:";
$MESS["TSBS_2BASKET"] = "Cambiar la cantidad";
$MESS["TSBS_2ORDER"] = "Orden";
$MESS["TSBS_DELAY"] = "Mantenerlo durante:";
$MESS["TSBS_UNAVAIL"] = "Actualmente no disponible:";
$MESS["TSBS_SUBSCRIBE"] = "Suscripción:";
?>