<?
$MESS ['SPCAT3_AFF_REG'] = "Link del texto del registro del afiliado";
$MESS ['SPCAT3_UNACTIVE_AFF'] = "Eres el afiliado registrado pero tu cuenta est� inactiva. Entrar en contacto por favor la administraci�n para m�s informaci�n. ";
$MESS ['SPCAT3_HTML'] = "C�digo del HTML: ";
$MESS ['SPCAT3_LINK_TEXT'] = "Afiliado convertido del &quot; #NAME#&quot;";
$MESS ['SPCAT3_TEXT_LINK'] = "Link del texto para el almac�n";
$MESS ['SPCAT3_VIEW'] = "Aspecto: ";
$MESS ['SPCAT3_PARTNER_ID'] = "- Tu identificaci�n de la sociedad. Tener esta identificaci�n en el link nos informa que el cliente vino de tu sitio. ";
$MESS ['SPCAT3_NOTE'] = "Un acoplamiento del texto no es la �nica opci�n. Puedes utilizar una imagen, un logo � un banner, con � sin los comentarios - cualquier cosa que persuada a tus visitantes en oprimir en �l para ganar un poco de dinero para t�. ";
?>