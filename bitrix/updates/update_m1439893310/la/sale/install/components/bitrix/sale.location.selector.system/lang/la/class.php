<?
$MESS["SALE_SLSS_ENTITY_PRIMARY_NOT_SET"] = "ID de la entidad no especificada";
$MESS["SALE_SLSS_LINK_ENTITY_CLASS_NAME_NOT_SET"] = "No se especifica el nombre de clase de la entidad";
$MESS["SALE_SLSS_LINK_ENTITY_CLASS_UNKNOWN"] = "Clase de entidad desconocido";
$MESS["SALE_SLSS_WRONG_LINK_CLASS"] = "La clase de entidad especificada no se deriva de conector";
?>