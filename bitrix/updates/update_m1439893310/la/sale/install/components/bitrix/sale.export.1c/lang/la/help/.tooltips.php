<?
$MESS ['SITE_LIST_TIP'] = "Especificar aqu� el sitio en el cual las ordenes ser�n exportadas.";
$MESS ['GROUP_PERMISSIONS_TIP'] = "Seleccione aqu� el grupo de usuarios cuyos miembros est�n permitidos para exportar 1C.";
$MESS ['USE_ZIP_TIP'] = "Especificar el uso de la compresi�n ZIP si es posible.";
$MESS ['EXPORT_PAYED_ORDERS_TIP'] = "Si comprob�, s�lo pague las ordenes que ser�n exportadas para 1C.";
$MESS ['EXPORT_ALLOW_DELIVERY_ORDERS_TIP'] = "Si comprob�, s�lo pague las ordenes que ser�n permitidas para env�o podr�n ser exportadas a IC.";
$MESS ['EXPORT_FINAL_ORDERS_TIP'] = "Especificar aqu� el estado m�nimo que una orden deber� tewner para ser exportada.";
$MESS ['FINAL_STATUS_ON_DELIVERY_TIP'] = "Especificar aqu� el estado para aplicar a una orden si la exportaci�n a 1C fue exitosa y el sitio se notific�.";
$MESS ['REPLACE_CURRENCY_TIP'] = "El art�culo actual puede ser reemplazado con una especificaci�n en este campo. Este no podr� convertir al precio actual.";
?>