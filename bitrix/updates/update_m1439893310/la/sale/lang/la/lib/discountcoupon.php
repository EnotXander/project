<?
$MESS["BX_SALE_DCM_ERR_BAD_MODE"] = "Modo de administrador inadecuado";
$MESS["BX_SALE_DCM_ERR_BAD_USER_ID"] = "ID de cliente no se encuentra";
$MESS["BX_SALE_DCM_ERR_ORDER_ID_EXIST"] = "Solicitar ID especificado en el nuevo modo de creaci�n de la orden";
$MESS["BX_SALE_DCM_ERR_ORDER_ID_ABSENT"] = "No hay ID de la orden especificado en el modo de edici�n";
$MESS["BX_SALE_DCM_ERR_BAD_FUSER_ID"] = "No puede inicializar ID de cliente";
$MESS["BX_SALE_DCM_STATUS_NOT_FOUND"] = "no se encuentra";
$MESS["BX_SALE_DCM_STATUS_ENTERED"] = "adicional";
$MESS["BX_SALE_DCM_STATUS_NOT_APPLYED"] = "nunca utilizado";
$MESS["BX_SALE_DCM_STATUS_APPLYED"] = "usado";
$MESS["BX_SALE_DCM_STATUS_FREEZE"] = "no se puede utilizar";
$MESS["BX_SALE_DCM_COUPON_CHECK_OK"] = "activo y v�lido";
$MESS["BX_SALE_DCM_COUPON_CHECK_NOT_FOUND"] = "no se encuentra";
$MESS["BX_SALE_DCM_COUPON_CHECK_NO_ACTIVE"] = "inactivo";
$MESS["BX_SALE_DCM_COUPON_CHECK_RANGE_ACTIVE_FROM"] = "fecha de inicio del cup�n es posterior a la actual";
$MESS["BX_SALE_DCM_COUPON_CHECK_RANGE_ACTIVE_TO"] = "fecha de finalizaci�n del cup�n es posterior a la actual";
$MESS["BX_SALE_DCM_COUPON_CHECK_NO_ACTIVE_DISCOUNT"] = "Estado inactivo";
$MESS["BX_SALE_DCM_COUPON_CHECK_RANGE_ACTIVE_FROM_DISCOUNT"] = "Estado de inicio del cup�n es posterior a la actual";
$MESS["BX_SALE_DCM_COUPON_CHECK_RANGE_ACTIVE_TO_DISCOUNT"] = "Estado de finalizaci�n del cup�n es posterior a la actual";
$MESS["BX_SALE_DCM_COUPON_CHECK_BAD_USER_ID"] = "titular del cup�n incorrecto";
$MESS["BX_SALE_DCM_COUPON_CHECK_ALREADY_MAX_USED"] = "usos m�ximos alcanzados";
$MESS["BX_SALE_DCM_COUPON_CHECK_UNKNOWN_TYPE"] = "tipo de cup�n desconocido";
$MESS["BX_SALE_DCM_COUPON_CHECK_CORRUPT_DATA"] = "Datos del cup�n no v�lido";
$MESS["BX_SALE_DCM_COUPON_CHECK_NOT_APPLIED"] = "cup�n no utilizado";
?>