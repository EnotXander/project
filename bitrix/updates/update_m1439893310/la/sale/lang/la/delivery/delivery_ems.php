<?
$MESS["SALE_DH_EMS_DESCRIPTION"] = "Entrega Express";
$MESS["SALE_DH_EMS_DELIVERY_TITLE"] = "entrega express";
$MESS["SALE_DH_EMS_CONFIG_TITLE"] = "Par�metros";
$MESS["SALE_DH_EMS_CONFIG_CATEGORY"] = "Tipo de env�o (entrega en todo el mundo)";
$MESS["SALE_DH_EMS_CONFIG_CATEGORY_att"] = "productos";
$MESS["SALE_DH_EMS_CONFIG_CATEGORY_doc"] = "documentos (arriba de 2 kilos)";
$MESS["SALE_DH_EMS_ERROR_CONNECT"] = "No puede calcular el costo de entrega: error de conexi�n.";
$MESS["SALE_DH_EMS_CORRECT_CITIES"] = "Actualizaci�n de ciudades.";
$MESS["SALE_DH_EMS_CORRECT_COUNTRIES"] = "Actualizaci�n de pa�ses.";
$MESS["SALE_DH_EMS_ERROR_RESPONSE"] = "No puede calcular el costo de la entrega: respuesta errada del servidor.";
$MESS["SALE_DH_EMS_ERROR_NO_CITY_TO"] = "El servicio de entrega no reparte a  #CITY#.";
$MESS["SALE_DH_EMS_ERROR_NO_CITY_FROM"] = "El servicio de entrega no entrega de #CITY#.";
$MESS["SALE_DH_EMS_ERROR_NO_COUNTRY_TO"] = "El servicio de entrega no reparte a  #COUNTRY#.";
$MESS["SALE_DH_EMS_NAME"] = "Correo EMS de Rusia";
$MESS["SALE_DH_EMS_DESCRIPTION_INNER"] = "Controlador para El Correo EMS de Rusia. S�lo env�os de Rusia. Sobre la base de<a href=\"http://www.emspost.ru/service/online/api\" target=\"_blank\">public EMS API</a>.<br /> Requiere la direcci�n de la tienda para ser especificada en el <a href=\"/bitrix/admin/settings.php?mid=sale&lang=ru\">configuraciones del m�odulo</a>.";
?>