<?
$MESS["SALE_LOCATION_MIGRATION_TITLE"] = "Migrar a ubicaciones 2.0";
$MESS["SALE_LOCATION_MIGRATION_TAB_MIGRATION_TITLE"] = "Migrar a ubicaciones 2.0";
$MESS["SALE_LOCATION_MIGRATION_START"] = "Iniciar migraci�n";
$MESS["SALE_LOCATION_MIGRATION_NEUTRAL_ERROR"] = "Se ha producido un error";
$MESS["SALE_LOCATION_MIGRATION_ERROR"] = "Error";
$MESS["SALE_LOCATION_MIGRATION_ALREADY_DONE"] = "La migraci�n a nuevas ubicaciones ya se ha realizado. No hay necesidad de migrar de nuevo.";
$MESS["SALE_LOCATION_MIGRATION_STAGE_CREATE_TYPES"] = "Crear tipos de ubicaci�n";
$MESS["SALE_LOCATION_MIGRATION_STAGE_CONVERT_TREE"] = "Convertir �rbol de ubicaci�n";
$MESS["SALE_LOCATION_MIGRATION_STAGE_CONVERT_ZONES"] = "Convertir las �reas de servicio de fabricaci�n";
$MESS["SALE_LOCATION_MIGRATION_STAGE_CONVERT_LINKS"] = "Convertir enlaces de entidad";
$MESS["SALE_LOCATION_MIGRATION_STAGE_COPY_DEFAULT_LOCATIONS"] = "Copia ubicaciones seleccionadas";
$MESS["SALE_LOCATION_MIGRATION_STAGE_COPY_ZIP_CODES"] = "Copie los c�digos postales";
$MESS["SALE_LOCATION_MIGRATION_STAGE_COMPLETE"] = "Migraci�n completa, redireccionando...";
$MESS["SALE_LOCATION_MIGRATION_STATUS"] = "Estado";
?>