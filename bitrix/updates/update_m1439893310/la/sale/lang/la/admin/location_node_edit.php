<?
$MESS["SALE_MODULE_ACCES_DENIED"] = "Se le neg� acceso al m�dulo";
$MESS["SALE_LOCATION_E_ITEM_EDIT"] = "Editar ubicaci�n: #ITEM_NAME#";
$MESS["SALE_LOCATION_E_ITEM_NEW"] = "Nueva ubicaci�n";
$MESS["SALE_LOCATION_E_CANNOT_SAVE_ITEM"] = "Error al guardar la ubicaci�n";
$MESS["SALE_LOCATION_E_CANNOT_UPDATE_ITEM"] = "Error al actualizar ubicaci�n";
$MESS["SALE_LOCATION_E_MAIN_TAB"] = "Ubicaci�n";
$MESS["SALE_LOCATION_E_MAIN_TAB_TITLE"] = "Par�metros de ubicaci�n";
$MESS["SALE_LOCATION_E_ITEM_NOT_FOUND"] = "La ubicaci�n no fue encontrado";
$MESS["SALE_LOCATION_E_GO_BACK"] = "Volver";
$MESS["SALE_LOCATION_E_GEODATA"] = "Datos de geoubicaci�n";
$MESS["SALE_LOCATION_E_EXTERNAL_TAB"] = "Datos externos";
$MESS["SALE_LOCATION_E_EXTERNAL_TAB_TITLE"] = "Tabla de datos externo";
$MESS["SALE_LOCATION_E_NO_SERVICES"] = "No hay servicios externos. Puede crear una en el";
$MESS["SALE_LOCATION_E_NO_SERVICES_LIST_PAGE"] = "p�gina de servicios externos";
$MESS["SALE_LOCATION_E_HEADING_EXTERNAL"] = "Datos externos";
$MESS["SALE_LOCATION_E_HEADER_EXT_REMOVE"] = "Del.";
$MESS["SALE_LOCATION_E_HEADER_EXT_MORE"] = "M�s...";
$MESS["SALE_LOCATION_E_HEADING_NAME_ALL"] = "T�tulos dependientes del idioma";
$MESS["SALE_LOCATION_E_HEADING_NAME"] = "T�tulos dependientes del idioma (#LANGUAGE_ID#)";
?>