<?
$MESS["SALE_MODULE_ACCES_DENIED"] = "Se le neg� acceso al m�dulo";
$MESS["SALE_LOCATION_E_ITEM_EDIT"] = "Editar grupo: #ITEM_NAME#";
$MESS["SALE_LOCATION_E_ITEM_NEW"] = "Nuevo grupo de ubicaci�n";
$MESS["SALE_LOCATION_E_CANNOT_SAVE_ITEM"] = "Error al guardar el grupo";
$MESS["SALE_LOCATION_E_CANNOT_UPDATE_ITEM"] = "Error al actualizar el grupo";
$MESS["SALE_LOCATION_E_MAIN_TAB"] = "Grupo";
$MESS["SALE_LOCATION_E_MAIN_TAB_TITLE"] = "Par�metros de grupo";
$MESS["SALE_LOCATION_E_ITEM_NOT_FOUND"] = "No se encontr� grupo.";
$MESS["SALE_LOCATION_E_GO_BACK"] = "Volver";
$MESS["SALE_LOCATION_E_HEADING_LOCATION"] = "Seleccione las ubicaciones";
$MESS["SALE_LOCATION_E_HEADING_NAME_ALL"] = "T�tulos dependientes del idioma";
$MESS["SALE_LOCATION_E_HEADING_NAME"] = "T�tulos dependientes del idioma (#LANGUAGE_ID#)";
?>