<?
$MESS["SALE_EDIT_RECORD"] = "Editar grupo de ubicaci�n #ID#";
$MESS["SALE_NEW_RECORD"] = "Nuevo grupo de ubicaci�n";
$MESS["SALE_RECORDS_LIST"] = "Grupos de ubicaci�n";
$MESS["SALE_SAVE"] = "Guardar";
$MESS["SALE_ADD"] = "Agregar";
$MESS["SALE_APPLY"] = "Aplicar";
$MESS["SALE_RESET"] = "Reajustar";
$MESS["ERROR_EMPTY_LOCATION"] = "Por favor, enlace al menos una ubicaci�n.";
$MESS["ERROR_EMPTY_NAME"] = "No se ha especificado el nombre de grupo de idioma";
$MESS["ERROR_EDIT_GROUP"] = "Error al modificar el grupo de ubicaci�n.";
$MESS["ERROR_ADD_GROUP"] = "Error al a�adir un grupo ubicaci�n ..";
$MESS["SALE_NEW"] = "nuevo";
$MESS["SALE_SORT"] = "�ndice de clasificaci�n";
$MESS["SALE_LOCATIONS"] = "Ubicaciones en el grupo";
$MESS["SALE_NAME"] = "Nombre";
$MESS["SALE_PT_PROPS"] = "Par�metros";
$MESS["SLGEN_2FLIST"] = "Lista de grupo de ubicaci�n ";
$MESS["SLGEN_NEW_LGROUP"] = "Agregar un nuevo grupo de ubicaci�n";
$MESS["SLGEN_DELETE_LGROUP"] = "Eliminar el grupo ubicaci�n";
$MESS["SLGEN_DELETE_LGROUP_CONFIRM"] = "�Seguro que quieres borrar grupo ubicaci�n?";
$MESS["SLGEN_TAB_LGROUP"] = "Grupo de ubicaci�n";
$MESS["SLGEN_TAB_LGROUP_DESCR"] = "Grupo de ubicaci�n";
?>