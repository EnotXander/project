<?
$MESS["SALE_1C_GROUP_PERMISSIONS"] = "Grupos de usuarios permitidos para exportar";
$MESS["SALE_1C_SITE_LIST"] = "Exportar pedidos de este sitio para 1C";
$MESS["SALE_1C_USE_ZIP"] = "Usar compresi�n ZIP si est� disponible";
$MESS["SALE_1C_ALL_SITES"] = "Todos los sitios";
$MESS["SALE_1C_EXPORT_PAYED_ORDERS"] = "Exportar s�lo pago de pedidos";
$MESS["SALE_1C_EXPORT_ALLOW_DELIVERY_ORDERS"] = "Exportar s�lo pedidos entregables";
$MESS["SALE_1C_EXPORT_FINAL_ORDERS"] = "Exportar estado de pedidos ";
$MESS["SALE_1C_FINAL_STATUS_ON_DELIVERY"] = "Estado de pedidos una vez adquiridos del delivery desde 1C";
$MESS["SALE_1C_REPLACE_CURRENCY"] = "Cuando exporta a 1C, cambiar la moneda a ";
$MESS["SALE_1C_NO"] = "<Ninguna selecci�n>";
$MESS["SALE_1C_RUB"] = "rub.";
$MESS["SALE_1C_SALE_ACCOUNT_NUMBER_SHOP_PREFIX"] = "Prefijo del n�mero de pedido (para exportaci�n)";
$MESS["SALE_1C_INTERVAL"] = "Duraci�n del paso de importaci�n, seg. (0- importar todo de una)";
$MESS["SALE_1C_FILE_SIZE_LIMIT"] = "Tama�o m�ximo para importar un pedazo de archivo (bytes)";
$MESS["SALE_1C_SITE_NEW_ORDERS"] = "Sitio para importar nuevas �rdenes y contratistas a";
$MESS["SALE_1C_IMPORT_NEW_ORDERS"] = "Crear nuevas �rdenes y contratistas de 1C";
$MESS["SALE_1C_CHANGE_STATUS_FROM_1C"] = "Utilice los datos 1C para establecer el estado del pedido";
?>