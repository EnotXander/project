<?
$MESS["NO_LOC_FILE"] = "Archivo de datos de ubicaci�n no est� cargado";
$MESS["ERROR_LOC_FORMAT"] = "Formato de archivo de ubicaci�n incorrecto: idioma principal no est� configurado";
$MESS["NO_MAIN_LANG"] = "Idioma principal de la ubicaci�n del archivo no se encuentra en el sitio";
$MESS["OMLOADED1"] = "Cargado";
$MESS["OMLOADED2"] = "pa�ses";
$MESS["OMLOADED3"] = "ciudades";
$MESS["OMLOADED4"] = "Ubicaciones creadas";
$MESS["LOCA_LOADING"] = "Asistente para importar ubicaciones";
$MESS["LOCA_LOADING_OLD"] = "Cargar ubicaciones de archivo";
$MESS["LOCA_FILE"] = "Archivo";
$MESS["LOCA_DEL_OLD"] = "Ubicaci�n de los sitios antes de la carga";
$MESS["LOCA_DO_LOAD"] = "Cargar";
$MESS["location_admin_import"] = "Importar ubicaciones";
$MESS["location_admin_import_tab"] = "Importar ";
$MESS["location_admin_import_tab_old"] = "Importar desde archivo";
$MESS["LOCA_LOADING_WIZARD"] = "Ejecutar asistente para importar ubicaciones";
$MESS["LOCA_SAVE"] = "Cargar";
$MESS["LOCA_LOCATIONS_STATS"] = "Estad�sticas de ubicaci�n";
$MESS["LOCA_LOCATIONS_COUNTRY_STATS"] = "pa�ses";
$MESS["LOCA_LOCATIONS_CITY_STATS"] = "ciudades";
$MESS["LOCA_LOCATIONS_LOC_STATS"] = "total de ubicaciones";
$MESS["LOCA_LOCATIONS_GROUP_STATS"] = "Grupos de ubicaci�n";
$MESS["LOCA_LOCATIONS_REGION_STATS"] = "regiones";
$MESS["LOCATION_CLEAR"] = "Borrar";
$MESS["LOCATION_CLEAR_DESC"] = "Elimina todas las ubicaciones";
$MESS["LOCATION_CLEAR_BTN"] = "Borrar";
$MESS["LOCATION_CLEAR_OK"] = "Se han eliminado las ubicaciones.";
$MESS["LOCATION_CLEAR_CONFIRM"] = "�Seguro que quieres eliminar todos las ubicaciones?";
$MESS["LOCA_HINT"] = "Usted puede descargar los archivos de ubicaciones desde el sitio de Bitrix:<ul style=\"font-size: 100%\">
	<li><a href=\"http://www.1c-bitrix.ru/download/files/locations/loc_cntr.csv\">Pa�ses del mundo</a></li>
	<li><a href=\"http://www.1c-bitrix.ru/download/files/locations/loc_usa.csv\">USA</a></li>
</ul>";
?>