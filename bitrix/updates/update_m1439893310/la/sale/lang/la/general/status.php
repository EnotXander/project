<?
$MESS["SKGS_ORDER_ID"] = "ID de la orden";
$MESS["SKGS_ORDER_DATE"] = "Fecha de la orden ";
$MESS["SKGS_ORDER_STATUS"] = "Estatus de la orden";
$MESS["SKGS_ORDER_EMAIL"] = "E-Mail del cliente";
$MESS["SKGS_STATUS_DESCR"] = "descripci�n del estatus de la orden";
$MESS["SKGS_STATUS_TEXT"] = "texto";
$MESS["SKGS_CHANGING_STATUS_TO"] = "Cambiando estatus de la orden a";
$MESS["SKGS_STATUS_MAIL_SUBJ"] = "#SERVER_NAME#: Cambiando estatus de la orden N� #ORDER_ID#";
$MESS["SKGS_STATUS_MAIL_BODY1"] = "Mensaje desde #SITE_NAME#";
$MESS["SKGS_STATUS_MAIL_BODY2"] = "El estatus de la orden N� #ORDER_ID#  del #ORDER_DATE# fue cambiado";
$MESS["SKGS_STATUS_MAIL_BODY3"] = "Nuevo estatus de orden";
$MESS["SKGS_ERROR_DELETE"] = "No puede borrar el estado por que hay pedidos en este estado.";
$MESS["SKGS_ID_NOT_SYMBOL"] = "C�digo del estado puede ser letra";
$MESS["SIM_ACCEPTED"] = "Aceptado";
$MESS["SIM_ACCEPTED_DESCR"] = "Orden ha sido aceptada, pero no se est� tramitando todav�a (por ejemplo: el pedido puede haber sido creado reci�n, o est� en espera de pago)";
$MESS["SIM_FINISHED"] = "Finalizado";
$MESS["SIM_FINISHED_DESCR"] = "Orden ha sido entregada y pagada";
$MESS["SKGS_SALE_EMAIL"] = "E-mail del departamento de ventas";
$MESS["SKGS_STATUS_MAIL_BODY4"] = "Para ver los detalles del pedido, por favor haga clic aqu�: #SERVER_NAME#/personal/order/#ORDER_ID#/

Gracias por hacer compras!";
$MESS["SKGS_SALE_STATUS_ALREADY_EXISTS"] = "Este ID del estado ya existe.";
?>