<?
$MESS["SKGO_EMPTY_SITE"] = "El sitio para �rdenes no se ha especificado";
$MESS["SKGO_EMPTY_PERS_TYPE"] = "El tipo de pagador no se ha especificado";
$MESS["SKGO_EMPTY_USER_ID"] = "El ID del cliente no se ha especificado";
$MESS["SKGO_EMPTY_CURRENCY"] = "Moneda no especificada";
$MESS["SKGO_WRONG_CURRENCY"] = "No se puede ubicar la moneda ##ID";
$MESS["SKGO_WRONG_SITE"] = "No se puede ubicar el sitio ##ID#";
$MESS["SKGO_WRONG_USER"] = "No se puede ubicar al usuario ##ID#";
$MESS["SKGO_WRONG_PERSON_TYPE"] = "No se puede ubicar al tipo de pagador ##ID#";
$MESS["SKGO_WRONG_PS"] = "No se puede ubicar al sistema de pago ##ID#";
$MESS["SKGO_WRONG_DELIVERY"] = "No se puede ubicar al servicio de entrega ##ID#";
$MESS["SKGO_WRONG_STATUS"] = "No se puede ubicar el estatus ##ID#";
$MESS["SKGO_NO_ORDER_ID"] = "La ID de la orden no se ha especificado";
$MESS["SKGO_NO_ORDER"] = "La orden ##ID# no se puede encontrar";
$MESS["SKGO_DUB_PAY"] = "La orden ##ID# ya se asign� a otra insidencia pagada";
$MESS["SKGO_NO_ORDER_ID1"] = "La ID de la orden no se ha especificado";
$MESS["SKGO_DUB_CANCEL"] = "La orden ##ID# ya se asign� a otra insidencia cancelada";
$MESS["SKGO_DUB_DELIVERY"] = "La orden ##ID# ya se asign� a otra insidencia entregada";
$MESS["SKGO_ERROR_ORDERS"] = "El usuario ##USER_ID# cuenta con algunas �rdenes en el e-Store y no podr� ser eliminado.";
$MESS["SKGO_ERROR_ORDERS_CURRENCY"] = "El sistema contiene pedidos en la #MONEDA#.";
$MESS["SKGO_CALC_PARAM_ERROR"] = "Error al llamar la orden de calcular: no se ha especificado el ID del sitio Web.";
$MESS["SKGO_SHOPPING_CART_EMPTY"] = "Carrito de compras esta vacio";
$MESS["SKGO_DUB_DEDUCTION"] = "La orden ##ID# ya tiene el valor necesario \"Enviado\".";
$MESS["SKGO_DUB_RESERVATION"] = "La orden ##ID# ya tiene el valor necesario \"Reservado\".";
$MESS["SKGO_EMPTY_ACCOUNT_NUMBER"] = "N�mero de pedido no puede estar en blanco";
$MESS["SKGO_EXISTING_ACCOUNT_NUMBER"] = "El n�mero de orden introducido ya est� en uso para una orden diferente";
?>