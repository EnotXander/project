<?
$MESS["SMILE_TITLE"] = "Emoticonos";
$MESS["SMILE_DEL_CONF"] = "Est� seguro que desea eliminar este emoticono?";
$MESS["SMILE_NAV"] = "Emoticono";
$MESS["SMILE_ID"] = "ID";
$MESS["SMILE_SORT"] = "Clasificar";
$MESS["SMILE_SET_NAME"] = "Fijar";
$MESS["SMILE_TYPE"] = "Tipo";
$MESS["SMILE_TYPING"] = "Digitar";
$MESS["SMILE_ICON"] = "Imagen";
$MESS["SMILE_IMAGE_FILE"] = "Archivo";
$MESS["SMILE_TYPE_ICON"] = "Icono";
$MESS["SMILE_TYPE_SMILE"] = "Emoticono";
$MESS["SMILE_NAME"] = "Nombre";
$MESS["SMILE_SET_NO_NAME"] = "Fijar: #ID#";
$MESS["SMILE_NO_NAME"] = "Sin t�tulo";
$MESS["SMILE_DELETE_DESCR"] = "Eliminar";
$MESS["SMILE_EDIT"] = "Editar";
$MESS["SMILE_EDIT_DESCR"] = "Editar";
$MESS["ERROR_DEL_SMILE"] = "Error al eliminar el emoticono.";
$MESS["SMILE_BTN_IMPORT"] = "Importar";
$MESS["SMILE_BTN_CLEAR_FILTER"] = "Reconfigurar filtro";
$MESS["SMILE_BTN_ADD_NEW"] = "Nuevo emoticono";
$MESS["SMILE_BTN_ADD_NEW_ALT"] = "Haga clic para a�adir un nuevo emoticono";
?>