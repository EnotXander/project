<?
$MESS["TITLE"] = "Usuarios";
$MESS["SAVE_ERROR"] = "Error al guardar el usuario #";
$MESS["DELETE_ERROR"] = "Error al borrar usuario. Posiblemente hay un usuario conectado con otro objeto.";
$MESS["PAGES"] = "Usuarios";
$MESS["TIMESTAMP"] = "Modificado";
$MESS["LAST_LOGIN"] = "�ltima conexi�n";
$MESS["ACTIVE"] = "Activo";
$MESS["LOGIN"] = "Login";
$MESS["EMAIL"] = "E-mail";
$MESS["NAME"] = "Primer nombre";
$MESS["ACTION"] = "Acciones";
$MESS["LAST_NAME"] = "Apellido";
$MESS["SECOND_NAME"] = "Segundo Nombre";
$MESS["CHANGE_USER"] = "Modifcar configuraci�n de usuarios";
$MESS["CHANGE"] = "Modificar";
$MESS["DELETE_USER"] = "Borrar usuario";
$MESS["CONFIRM_DEL_USER"] = "�Est� seguro que desa borrar este usuario?";
$MESS["DELETE"] = "Borrar";
$MESS["F_LOGIN"] = "Login:";
$MESS["F_NAME"] = "Nombre o Apellido:";
$MESS["F_ACTIVE"] = "Activo:";
$MESS["F_GROUP"] = "Grupos:";
$MESS["MAIN_WRONG_TIMESTAMP_FROM"] = "Ingrese correctamente la fecha de modificaci�n \"desde\" en el filtro";
$MESS["MAIN_WRONG_TIMESTAMP_TILL"] = "Ingrese correctamente la fecha de modificaci�n \"hasta\" en el filtro";
$MESS["MAIN_FROM_TILL_TIMESTAMP"] = "La fecha de modifcaci�n en �hasta� debe ser mayor que la de �desde� en  el filtro ";
$MESS["MAIN_WRONG_LAST_LOGIN_FROM"] = "Escriba correctamente la fecha de autorizaci�n \"desde\"";
$MESS["MAIN_WRONG_LAST_LOGIN_TILL"] = "Escriba correctamente la fecha de autorizaci�n \"hasta\"";
$MESS["MAIN_FROM_TILL_LAST_LOGIN"] = "La fecha de conexi�n en �hasta� debe ser mayor que la de �desde� ";
$MESS["MAIN_F_ID"] = "ID:";
$MESS["MAIN_F_LAST_LOGIN"] = "�ltima Modificaci�n";
$MESS["MAIN_F_TIMESTAMP"] = "Modificado";
$MESS["MAIN_F_EMAIL"] = "E-mail:";
$MESS["MAIN_F_KEYWORDS"] = "Campos del perfil: ";
$MESS["DATE_REGISTER"] = "Fecha de Resgistro";
$MESS["PERSONAL_BIRTHDAY"] = "Fecha de cumplea�os";
$MESS["PERSONAL_PROFESSION"] = "Cargo en el trabajo";
$MESS["PERSONAL_WWW"] = "P�gina WEB";
$MESS["PERSONAL_ICQ"] = "ICQ";
$MESS["PERSONAL_GENDER"] = "Sexo";
$MESS["PERSONAL_PHONE"] = "Tel�fono privado";
$MESS["PERSONAL_MOBILE"] = "Tel�fono celular";
$MESS["PERSONAL_CITY"] = "Ciudad";
$MESS["PERSONAL_STREET"] = "Calle";
$MESS["WORK_COMPANY"] = "Compa��a";
$MESS["WORK_DEPARTMENT"] = "Dept.";
$MESS["WORK_POSITION"] = "Cargo";
$MESS["WORK_WWW"] = "WEB de la compa��a";
$MESS["WORK_PHONE"] = "Tel�fonos del Trabajo";
$MESS["WORK_CITY"] = "Ciudad del trabajo";
$MESS["XML_ID"] = "C�digo externo";
$MESS["EXTERNAL_AUTH_ID"] = "Autorizaci�n de fuente externa (ID)";
$MESS["MAIN_EDIT_TITLE"] = "Editar perfil de usuario";
$MESS["MAIN_ADD_USER"] = "Agregar usuario";
$MESS["MAIN_ADD_USER_TITLE"] = "Agregar un nuevo usuario";
$MESS["MAIN_FLT_USER_ID"] = "ID de usuario";
$MESS["MAIN_FLT_MOD_DATE"] = "Modificado ";
$MESS["MAIN_FLT_AUTH_DATE"] = "Autorizado";
$MESS["MAIN_FLT_ACTIVE"] = "Activo";
$MESS["MAIN_FLT_LOGIN"] = "Login";
$MESS["MAIN_FLT_EMAIL"] = "E-Mail";
$MESS["MAIN_FLT_FIO"] = "Apellido, nombre y segundo nombre";
$MESS["MAIN_FLT_PROFILE_FIELDS"] = "Campos del perfil ";
$MESS["MAIN_FLT_USER_GROUP"] = "Grupo de usuario";
$MESS["MAIN_FLT_SEARCH_TITLE"] = "Ingrese el texto a buscar";
$MESS["MAIN_FLT_SEARCH"] = "Buscar:";
$MESS["MAIN_EDIT_ERROR"] = "Error al actualizar el registro:";
$MESS["MAIN_ADMIN_MENU_VIEW"] = "Ver";
$MESS["F_FIND_INTRANET_USERS"] = "Usuarios de Intranet:";
$MESS["MAIN_ADMIN_LIST_INTRANET_DEACTIVATE"] = "Limpiar el campo de '�ltima autorizaci�n' ";
$MESS["MAIN_ADMIN_LIST_ADD_GROUP"] = "agregar al grupo";
$MESS["MAIN_ADMIN_LIST_REM_GROUP"] = "quitar del grupo";
$MESS["MAIN_ADMIN_LIST_GROUP"] = "(seleccionar grupo)";
$MESS["MAIN_ADMIN_LIST_ADD_STRUCT"] = "agregar a la estructura de la compa��a";
$MESS["MAIN_ADMIN_LIST_REM_STRUCT"] = "quitar de la estructura de la compa��a";
$MESS["MAIN_ADMIN_AUTH"] = "Autorizar";
$MESS["MAIN_ADMIN_AUTH_TITLE"] = "Autorizar como este usuario";
$MESS["MAIN_ALL"] = "(todos)";
$MESS["MAIN_YES"] = "si";
$MESS["MAIN_ADMIN_MENU_EDIT"] = "Editar";
$MESS["MAIN_ADMIN_ADD_COPY"] = "Agregar copia";
$MESS["MAIN_ADMIN_MENU_DELETE"] = "Eliminar";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "activar";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "desactivar";
$MESS["USER_ADMIN_TITLE"] = "Saludo";
?>