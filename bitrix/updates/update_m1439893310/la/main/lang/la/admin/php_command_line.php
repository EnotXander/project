<?
$MESS["php_cmd_title"] = "L�nea de comando PHP ";
$MESS["php_cmd_confirm"] = "Este comando podr� ejecutarse en el servidor como un escrito PHP. Continuar?";
$MESS["php_cmd_input"] = "Comando PHP";
$MESS["php_cmd_php"] = "Script arbitrario PHP para ser ejecutado en el servidor";
$MESS["php_cmd_button"] = "Ejecutar";
$MESS["php_cmd_button_clear"] = "Limpiar";
$MESS["php_cmd_result"] = "Resultado de Ejecuci�n de Mando";
$MESS["php_cmd_error"] = "Error al ejecutar un comando.";
$MESS["php_cmd_text_result"] = "Mostrar resultado del comando como texto";
?>