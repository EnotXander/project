<?
$MESS["SMILE_TITLE"] = "Fijar emoticonos";
$MESS["SMILE_DEL_CONF"] = "Est� seguro que desea eliminar este grupo? Esto tambi�n eliminar� todos los emoticonos que contiene el conjunto.";
$MESS["SMILE_NAV"] = "Fijar";
$MESS["SMILE_ID"] = "ID";
$MESS["SMILE_SORT"] = "Clasificar";
$MESS["SMILE_NAME"] = "Nombre";
$MESS["SMILE_SET_NAME"] = "Fijar: #ID#";
$MESS["SMILE_STRING_ID"] = "Fijar ID";
$MESS["SMILE_SMILE_COUNT"] = "Emoticonos";
$MESS["SMILE_DELETE_DESCR"] = "Eliminar";
$MESS["SMILE_EDIT"] = "Editar";
$MESS["SMILE_EDIT_DESCR"] = "Editar";
$MESS["ERROR_DEL_SMILE"] = "Error al eliminar el conjunto.";
$MESS["SMILE_BTN_ADD_NEW"] = "Nuevo conjunto";
$MESS["SMILE_BTN_ADD_NEW_ALT"] = "Haga clic para a�adir una nueva serie";
?>