<?
$MESS["BCLMMD_PARAM_MONITORING_PERIOD"] = "Rango de tiempo";
$MESS["BCLMMD_PARAM_DOMAIN_REGISTRATION"] = "El dominio expirar� en ";
$MESS["BCLMMD_PARAM_LICENSE"] = "La licencia expirar� en";
$MESS["BCLMMD_PARAM_MONITORING_SSL"] = "El certificado SSL expirar� en";
$MESS["BCLMMD_EDIT"] = "Editar sitio";
$MESS["BCLMMD_DELETE"] = "Eliminar sitio";
$MESS["BCLMMD_DELETE_CONFIRM"] = "�Est� seguro que usted quiere remover este sitio web de su lista?";
$MESS["BCLMMD_BUTT_CANCEL"] = "Cancelar";
$MESS["BCLMMD_BUTT_OK"] = "Remover";
$MESS["BCLMMD_TITLE"] = "Inspector Cloud";
$MESS["BCLMMD_NO_DATA"] = "Sin datos.";
$MESS["BCLMMD_PARAM_HTTP_RESPONSE_TIME"] = "Respuesta del Sitio Web";
$MESS["BCLMMD_PARAM_FAILED_PERIOD"] = "Inactividad";
?>