<?
$MESS["AD_USE_HTML_EDIT"] = "Use el editor HTML (s�lo para IE 5.0 y superior):";
$MESS["AD_DONT_USE_CONTRACT"] = "No aplicar restricciones de contrato para BANNERS:";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Dejar por defecto";
$MESS["AD_BANNER_DAYS"] = "�D�as a mantener el banner din�mico?";
$MESS["AD_CLEAR"] = "limpiar";
$MESS["AD_RECORDS"] = "registros:";
$MESS["AD_BANNER_GRAPH_WEIGHT"] = "Tama�o del gr�fico:";
$MESS["AD_BANNER_GRAPH_HEIGHT"] = "Altura del gr�fico:";
$MESS["AD_BANNER_DIAGRAM_DIAMETER"] = "Diametro del gr�fico circular:";
$MESS["AD_COOKIE_DAYS"] = "�Cuanto tiempo guardar la informaci�n sobre visitantes que ven los banners?:";
$MESS["AD_REDIRECT_FILENAME"] = "Nombre completo del archivo que maneja el registro de clicks en los banners:";
$MESS["AD_UPLOAD_SUBDIR"] = "Subcarpeta para cargar banners:";
$MESS["AD_OPT_DONT_FIX_BANNER_SHOWS"] = "No hacer seguimiento de las impresiones de banner para ning�n banner:";
?>