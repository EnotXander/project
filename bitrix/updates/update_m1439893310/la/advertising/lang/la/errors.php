<?
$MESS ['AD_ERROR_FORGOT_SID'] = "Error! Usted fall� al ingresar el s�mbolo ifdentificador (ID). ";
$MESS ['AD_ERROR_INCORRECT_SID'] = "Error! s�mbolo identificador incorrecto (s�lo letras latinas , n�meros y el gui�n de abajo \"_\" est�n permitidos)";
$MESS ['AD_ERROR_SID_EXISTS'] = "Error! El identificador \"#SID#\" ya est� en uso.";
$MESS ['AD_ERROR_NOT_ENOUGH_PERMISSIONS_CONTRACT'] = "Error! Acceso al contrato denengado.";
$MESS ['AD_ERROR_NOT_ENOUGH_PERMISSIONS_BANNER'] = "Error! Acceso al banner denengado.";
$MESS ['AD_ERROR_NOT_ENOUGH_PERMISSIONS_FOR_NEW_CONTRACT'] = "Error! No cuenta con suficientes permisos para crear un contrato.";
$MESS ['AD_ERROR_NOT_ENOUGH_PERMISSIONS_TYPE'] = "Error! Acceso a un tipo denegado.";
$MESS ['AD_ERROR_NOT_ENOUGH_PERMISSIONS_FOR_CREATE_TYPE'] = "Error! No cuenta con permisos para crear un tipo";
$MESS ['AD_ERROR_WRONG_DATE_MODIFY_FROM'] = "Error! Ingrese correctamente la modificaci�n de la fecha \"desde\".";
$MESS ['AD_ERROR_WRONG_DATE_MODIFY_TILL'] = "Error! Ingrese correctamente la modificaci�n de la fecha \"hasta\".";
$MESS ['AD_ERROR_FROM_TILL_DATE_MODIFY'] = "Error! La modificaci�n de la fecha \"desde\" debe ser superior a la de \"desde\".";
$MESS ['AD_ERROR_WRONG_DATE_SHOW_FROM_CONTRACT'] = "Error! Fecha de inicio incorrecto en el campo \"Periodo de muestra de banners\".";
$MESS ['AD_ERROR_WRONG_DATE_SHOW_TO_CONTRACT'] = "Error! Fecha final incorrecta en el campo \"Periodo de muestra de banners\".";
$MESS ['AD_ERROR_WRONG_DATE_SHOW_FROM_BANNER'] = "Error! Fecha de inicio incorrecto en el campo \"Periodo de muestra\".";
$MESS ['AD_ERROR_WRONG_DATE_SHOW_TO_BANNER'] = "Error! Fecha final incorrecta en el campo \"Periodo de muestra\".";
$MESS ['AD_ERROR_INCORRECT_CONTRACT_ID'] = "Error! ID de contrato incorrecto";
$MESS ['AD_ERROR_INCORRECT_BANNER_ID'] = "Error! ID de banner incorrecto";
$MESS ['AD_ERROR_WRONG_PERIOD_FROM'] = "Error! Ingrese correctamente la fecha \"desde\" para el \"Periodo\"";
$MESS ['AD_ERROR_WRONG_PERIOD_TILL'] = "Error! Ingrese correctamente la fecha \"hasta\" para el \"Periodo\"";
$MESS ['AD_ERROR_FROM_TILL_PERIOD'] = "Error! La fecha \"hasta\" debe ser mayor a la fecha \"desde\" en el filtro";
$MESS ['AD_CONTRACT_DISABLE'] = "Atenci�n! Las restriciones de Contrato est�n desabilitados en el m�dulo de configuraci�n.";
$MESS ['AD_ERROR_FROMTO_DATE_HAVETOBE_SET'] = "Error! El intervalo a mostrar debe ser definido.";
$MESS ['AD_ERROR_MAX_SHOW_COUNT_HAVETOBE_SET'] = "�Error! Este par�metro tiene que ser definido:\"m�ximo de muestras\" ";
$MESS ['AD_ERROR_FIXSHOW_HAVETOBE_SET'] = "Error! Esta opci�n debe ser habilitada: \"Seguimiento de impresiones del banner\"";
?>