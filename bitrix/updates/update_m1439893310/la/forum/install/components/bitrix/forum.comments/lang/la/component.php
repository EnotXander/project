<?
$MESS["F_NO_MODULE"] = "El m�dulo Foro no est� instalado";
$MESS["F_ERR_FID_EMPTY"] = "No se encuentran opiniones en este foro";
$MESS["F_ERR_FID_IS_NOT_EXIST"] = "No hay opiniones para el foro #FORUM#";
$MESS["F_ERR_EID_EMPTY"] = "El elemento del Block de Informaci�n no se ha especificado";
$MESS["COMM_COMMENT_OK"] = "El comentario fue guardado exitosamente";
$MESS["COMM_COMMENT_OK_AND_NOT_APPROVED"] = "El comentario ha sido agregado exitosamente. Este se mostrar� despu�s que el moderador lo apruebe.";
$MESS["NAV_OPINIONS"] = "Opiniones";
$MESS["F_MESSAGE_TEXT"] = "Texto del mensaje ";
$MESS["F_MINIMIZE_TEXT"] = "Ocultar formulario de comentarios";
$MESS["F_EXPAND_TEXT"] = "Agregar un Nuevo Comentario";
$MESS["F_ERR_ENT_EMPTY"] = "El tipo de entidad no est� especificado.";
$MESS["F_ERR_ENT_INVALID"] = "El tipo de entidad es incorrecto.";
?>