<?
$MESS["F_URL_TEMPLATES"] = "Proceso URLs";
$MESS["F_SET_NAVIGATION"] = "Muestra Breadcrumb de navegaci�n";
$MESS["F_TOPICS_PER_PAGE"] = "N�mero de temas por p�gina";
$MESS["F_SHOW_FORUM_ANOTHER_SITE"] = "Muestra foros desde otro sitios internos";
$MESS["F_INDEX_TEMPLATE"] = "P�gina de lista de foros";
$MESS["F_READ_TEMPLATE"] = "P�gina de tema le�do";
$MESS["F_LIST_TEMPLATE"] = "P�gina de lista de temas";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "Perfil de la p�gina";
$MESS["F_DEFAULT_FID"] = "ID del foro";
$MESS["F_SHOW_TITLE"] = "T�tulo";
$MESS["F_SHOW_USER_START_NAME"] = "Autor";
$MESS["F_SHOW_POSTS"] = "Respuestas";
$MESS["F_SHOW_VIEWS"] = "Leer";
$MESS["F_SHOW_LAST_POST_DATE"] = "Ultimo mensaje";
$MESS["F_SORTING_ORD"] = "Clasificar por campo";
$MESS["F_SORTING_BY"] = "Clasificar en orden";
$MESS["F_DESC_ASC"] = "ascendente";
$MESS["F_DESC_DESC"] = "descendente";
$MESS["F_DATE_TIME_FORMAT"] = "Formato de fecha y hora";
$MESS["F_TOPICS"] = "Temas";
$MESS["F_DISPLAY_PANEL"] = "Visualizar panel de botones para este componente";
$MESS["F_MESSAGE_TEMPLATE"] = "P�agina de mensaje visto";
$MESS["F_SORT_BY_SORT_FIRST"] = "Mostrar temas depositado en la parte superior";
?>