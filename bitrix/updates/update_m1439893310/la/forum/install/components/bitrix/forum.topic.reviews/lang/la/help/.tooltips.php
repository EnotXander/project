<?
$MESS["FORUM_ID_TIP"] = "ID del Foro de discusi�n ";
$MESS["IBLOCK_TYPE_TIP"] = "Tipo del Block de Informaci�n (s�lo para la verificaci�n)";
$MESS["IBLOCK_ID_TIP"] = "ID del Block de Informaci�n";
$MESS["ELEMENT_ID_TIP"] = "ID del Elemento";
$MESS["URL_TEMPLATES_READ_TIP"] = "P�gina de Vista de Tema del Foro";
$MESS["URL_TEMPLATES_DETAIL_TIP"] = "P�gina del Block de informaci�n del Elemento";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "P�gina del Perfil del Usuario";
$MESS["MESSAGES_PER_PAGE_TIP"] = "N�mero de mensajes por P�gina";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "Plantilla del Breadrumb de navegaci�n";
$MESS["DATE_TIME_FORMAT_TIP"] = "Formato de Fecha y Hora";
$MESS["PATH_TO_SMILE_TIP"] = "Ruta a la Carpeta de Smiley (relativo a la ra�z del sitio)";
$MESS["USE_CAPTCHA_TIP"] = "Utilizar CAPTCHA";
$MESS["PREORDER_TIP"] = "Mostrar las Primeras Publicaciones";
$MESS["CACHE_TYPE_TIP"] = "Tipo del Cache";
$MESS["CACHE_TIME_TIP"] = "Tiempo del Vida del Cache (seg.)";
$MESS["EDITOR_CODE_DEFAULT_TIP"] = "Prdeterminar el modo del editor para el texto plano";
$MESS["SHOW_AVATAR_TIP"] = "Mostrar Avatars";
$MESS["SHOW_RATING_TIP"] = "Mostrar calificaci�n";
$MESS["SHOW_MINIMIZED_TIP"] = "El formulario de respuesta colaps�";
?>