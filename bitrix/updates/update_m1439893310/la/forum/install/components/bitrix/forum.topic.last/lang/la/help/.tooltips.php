<?
$MESS["FID_TIP"] = "ID del Foro";
$MESS["SORT_BY_TIP"] = "Campo de ordenamiento";
$MESS["SORT_ORDER_TIP"] = "Orden";
$MESS["SORT_BY_SORT_FIRST_TIP"] = "Mostrar temas fijados en la parte superior";
$MESS["URL_TEMPLATES_INDEX_TIP"] = "P�gina d Foros";
$MESS["URL_TEMPLATES_LIST_TIP"] = "P�gina de Temas";
$MESS["URL_TEMPLATES_READ_TIP"] = "P�gina de vista de Temas";
$MESS["URL_TEMPLATES_MESSAGE_TIP"] = "P�gina de Vista de env�os";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "Plantilla del URL del perfil de usuario";
$MESS["TOPICS_PER_PAGE_TIP"] = "Temas por P�gina";
$MESS["DATE_TIME_FORMAT_TIP"] = "Formato de D�a y Hora";
$MESS["SHOW_FORUM_ANOTHER_SITE_TIP"] = "Mostrar Foros de Otros Sitios";
$MESS["SET_NAVIGATION_TIP"] = "Mostrar Controles de Navegaci�n";
$MESS["DISPLAY_PANEL_TIP"] = "El Panel de Control muestra los botones para este componente";
$MESS["CACHE_TYPE_TIP"] = "Tipo de Cach�";
$MESS["CACHE_TIME_TIP"] = "Tiempo de Duraci�n del Cach� (sec.)";
$MESS["SET_TITLE_TIP"] = "Establecer T�tulo de la P�gina";
$MESS["PAGER_DESC_NUMBERING_TIP"] = "Usar Breadcrumbs Inverso";
$MESS["PAGER_SHOW_ALWAYS_TIP"] = "Mostrar Siempre";
$MESS["PAGER_TITLE_TIP"] = "Nombres de la Categor�a";
$MESS["PAGER_TEMPLATE_TIP"] = "T�tulo de la Plantilla";
?>