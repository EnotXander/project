<?
$MESS["FSL_SUBSCR_MANAGE"] = "Suscripci�n";
$MESS["FSL_FORUM_NAME"] = "T�tulo del foro";
$MESS["FSL_TOPIC_NAME"] = "T�tulo de los temas";
$MESS["FSL_SUBSCR_DATE"] = "Fecha de suscripci�n";
$MESS["FSL_LAST_SENDED_MESSAGE"] = "�ltimo mensaje enviado";
$MESS["FSL_NEW_TOPICS"] = "Nuevos temas";
$MESS["FSL_ALL_MESSAGES"] = "Todos los temas";
$MESS["FSL_HERE"] = "Aqu�";
$MESS["FSL_NOT_SUBCRIBED"] = "No est� suscrito a ning�n foro todav�a. Para empezar a recibir mensajes del foro por favor use los botones del men� en el foro o a trav�s de intereses.";
$MESS["JS_NO_SUBSCRIBE"] = "Suscripciones no se han seleccionado.";
$MESS["JS_DEL_SUBSCRIBE"] = "�Est� seguro que desea eliminar las suscripciones?";
$MESS["F_DELETE_SUBSCRIBES"] = "Eliminar las suscripciones";
?>