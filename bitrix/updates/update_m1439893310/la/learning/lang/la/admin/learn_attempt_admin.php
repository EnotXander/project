<?
$MESS["LEARNING_ADMIN_TITLE"] = "Alternativas";
$MESS["LEARNING_ADMIN_RESULTS"] = "Alternativas";
$MESS["LEARNING_ADMIN_MENU_RESULTS"] = "Resultados";
$MESS["LEARNING_ERROR"] = "Error al guardar la aternativa";
$MESS["SAVE_ERROR"] = "Error al actualizar la alternativa #";
$MESS["LEARNING_ADMIN_DATE_START"] = "Fecha de inicio";
$MESS["LEARNING_ADMIN_DATE_END"] = "Fecha final";
$MESS["LEARNING_ADMIN_STUDENT"] = "Estudiante";
$MESS["LEARNING_ADMIN_STATUS"] = "Estatus";
$MESS["LEARNING_ADMIN_SCORE"] = "Puntaje";
$MESS["LEARNING_ADMIN_MAX_SCORE"] = "M�x. Puntaje";
$MESS["LEARNING_ADMIN_TEST"] = "Prueba";
$MESS["LEARNING_ADMIN_QUESTIONS"] = "Preguntas";
$MESS["LEARNING_ADMIN_COMPLETED"] = "La prueba fue completada";
?>