<?
$MESS["BPATL_DESCR"] = "Descripción";
$MESS["BPATL_DESCR_FULL"] = "Descripción total";
$MESS["BPATL_F_MODIFIED"] = "Modificado";
$MESS["BPATL_MODIFIED"] = "Modificado";
$MESS["BPATL_F_NAME"] = "Nombre";
$MESS["BPATL_NAME"] = "Nombre";
$MESS["BPATL_NAV"] = "Tareas";
$MESS["BPATL_TITLE"] = "Tareas";
$MESS["BPATL_VIEW"] = "Ver tarea";
$MESS["BPATL_WORKFLOW_NAME"] = "Proceso de negocio";
$MESS["BPATL_WORKFLOW_STATE"] = "Estado";
$MESS["BPATL_USER"] = "Usuario";
$MESS["BPATL_USER_ID"] = "ID del usuario";
$MESS["BPATL_USER_NOT_FOUND"] = "El usuraio ##USER_ID# no fue encontrado.";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Seleccionado:";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Comprobado:";
?>