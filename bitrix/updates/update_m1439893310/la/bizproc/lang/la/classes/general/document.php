<?
$MESS["BPCGDOC_ADD"] = "Agregar";
$MESS["BPCGDOC_AUTO_EXECUTE_CREATE"] = "Crear";
$MESS["BPCGDOC_AUTO_EXECUTE_DELETE"] = "Eliminar";
$MESS["BPCGDOC_AUTO_EXECUTE_NONE"] = "No";
$MESS["BPCGDOC_NO"] = "No";
$MESS["BPCGDOC_INVALID_WF"] = "Ning�n trabajo de proceso de negocios puede ser encontrado para este documento. ";
$MESS["BPCGDOC_EMPTY_WD_ID"] = "El ID de la plantilla de proceso de negocios esta desaparecido.";
$MESS["BPCGDOC_INVALID_TYPE"] = "El tipo de par�metro esta indefinido";
$MESS["BPCGDOC_AUTO_EXECUTE_EDIT"] = "Actualizar";
$MESS["BPCGDOC_WAIT"] = "espere...";
$MESS["BPCGDOC_YES"] = "Si";
$MESS["BPCGDOC_INVALID_WF_ID"] = "No se pudo encontrar una plantilla para procesos de negocio #ID#.";
$MESS["BPCGDOC_ERROR_DELEGATE"] = "La tarea \"#NAME#\" no puede ser delegada porque el empleado seleccionado ya est� asignado a la tarea.";
$MESS["BPCGDOC_ERROR_ACTION"] = "Tarea \"#NAME#\": #ERROR#";
$MESS["BPCGDOC_ERROR_TASK_IS_NOT_INLINE"] = "La tarea \"#NAME#\" no funciona de esta manera.";
?>