<?
$MESS["BPAA_PD_TYPE_ALL"] = "Todas las personas";
$MESS["BPAA_PD_TYPE_ANY"] = "Cualquier persona";
$MESS["BPAA_PD_APPROVERS"] = "Electores";
$MESS["BPAA_PD_PERCENT"] = "Porcentaje m�nimo requerido de participaci�n de usuarios para la aceptaci�n";
$MESS["BPAA_PD_DESCR"] = "Descripci�n de la tarea";
$MESS["BPAA_PD_NAME"] = "Nombre de la tarea";
$MESS["BPAA_PD_TYPE"] = "Tipo de aprobaci�n";
$MESS["BPAA_PD_TYPE_VOTE"] = "Voto";
$MESS["BPAA_PD_WAIT"] = "Esperar por las respuestas de todos los participantes";
$MESS["BPAA_PD_SET_STATUS_MESSAGE"] = "Establecer Estado del mensaje";
$MESS["BPAA_PD_YES"] = "Si";
$MESS["BPAA_PD_NO"] = "No";
$MESS["BPAA_PD_STATUS_MESSAGE"] = "Estado del texto";
$MESS["BPAA_PD_STATUS_MESSAGE_HINT"] = "Los siguientes macros est�n habilitados: #PERC# - porcentaje, #REV# - personas actualmente familiarizadas, #TOT# - Total de personas familiarizadas.";
$MESS["BPAA_PD_TIMEOUT_DURATION"] = "Tiempo l�mite";
$MESS["BPAA_PD_TIMEOUT_DURATION_HINT"] = "Si un documento no es aprobado o rechazado antes de la expiraci�n del tiempo l�mite, ser� rechazado autom�ticamente. Un valor vac�o o 0 especifica no se aplicar� ning�n tiempo l�mite.";
$MESS["BPAA_PD_TIME_D"] = "d�as";
$MESS["BPAA_PD_TIME_H"] = "horas";
$MESS["BPAA_PD_TIME_M"] = "minutos";
$MESS["BPAA_PD_TIME_S"] = "segundos";
$MESS["BPAA_PD_STATUS_MESSAGE_HINT1"] = "Los siguientes macros son posible: #PERCENT# - promedio de personas familiarizadas, #VOTED# - personas actualmente informadas, #TOTAL# - total de personas requeridas, #APPROVERS# - usuarios que aprobaron el item, #REJECTERS# - usuarios que rechazaron el item";
$MESS["BPAR_PD_SHOW_COMMENT"] = "Mostrar campo de ingreso de comentarios";
$MESS["BPAR_PD_COMMENT_LABEL_MESSAGE"] = "Etiqueta para el campo de ingreso de comentarios";
$MESS["BPAR_PD_TASK_BUTTON1_MESSAGE"] = "Bot�n de texto para Aceptar";
$MESS["BPAR_PD_TASK_BUTTON2_MESSAGE"] = "Bot�n de texto para rechazar";
$MESS["BPAR_PD_ACCESS_CONTROL"] = "Mostrar la descripci�n de la tarea s�lo a la persona responsable. ";
?>