<?
$MESS["BPAA_ACT_BUTTON1"] = "Aceptar";
$MESS["BPAA_LOG_Y"] = "Aprobado";
$MESS["BPAA_ACT_COMMENT"] = "Comentarios";
$MESS["BPAA_LOG_COMMENTS"] = "Comentarios";
$MESS["BPAA_ACT_BUTTON2"] = "Rechazar";
$MESS["BPAA_LOG_N"] = "Rechazado";
$MESS["BPAA_ACT_NO_ACTION"] = "Sin acci�n especificada.";
$MESS["BPAA_ACT_INFO"] = "Participo #PERC#% (#REV# para #TOT#)";
$MESS["BPAA_ACT_APPROVE"] = "El documento ha sido aprobado";
$MESS["BPAA_ACT_NONAPPROVE"] = "El documento no ha sido aprobado";
$MESS["BPAA_ACT_TRACK2"] = "El documento debe ser aprobado por todos #VAL#";
$MESS["BPAA_ACT_TRACK1"] = "El documento debe ser aprobado por cualquiera #VAL#";
$MESS["BPAA_ACT_TRACK3"] = "El documento sera aprobado por el voto #VAL#";
$MESS["BPAA_ACT_PROP_EMPTY4"] = "La propiedad 'Nombre' esta desaparecida.";
$MESS["BPAA_ACT_PROP_EMPTY2"] = "La propiedad 'Tipo de aprobaci�n' no esta especificada.";
$MESS["BPAA_ACT_APPROVE_TRACK"] = "El usuario #PERSON# ha aprobado el documento #COMMENT#";
$MESS["BPAA_ACT_NONAPPROVE_TRACK"] = "El usuario #PERSON# no ha aprobado el documento #COMMENT#";
$MESS["BPAA_ACT_PROP_EMPTY1"] = "La propiedad 'Usuario' no esta especificada.";
$MESS["BPAA_ACT_PROP_EMPTY3"] = "El valor de la propiedad 'Tipo de aprobaci�n' es invalida.";
$MESS["BPAA_ACT_APPROVERS_NONE"] = "ninguno";
?>