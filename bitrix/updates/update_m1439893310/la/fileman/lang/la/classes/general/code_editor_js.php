<?
$MESS["GoToLine"] = "Ir a la l�nea";
$MESS["Line"] = "L�nea";
$MESS["Char"] = "car�cter";
$MESS["Total"] = "Total";
$MESS["Lines"] = "l�neas";
$MESS["Chars"] = "caracteres";
$MESS["LineTitle"] = "L�nea actual";
$MESS["CharTitle"] = "Car�cter actual";
$MESS["EnableHighlight"] = "resaltado de sintaxis";
$MESS["EnableHighlightTitle"] = "Alternar resaltado de sintaxis";
$MESS["DarkTheme"] = "fondo oscuro";
$MESS["LightTheme"] = "fondo claro";
$MESS["HighlightWrongwarning"] = "Este navegador no admitan resaltado de sintaxis en medida completa.";
?>