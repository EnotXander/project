<?
if(IsModuleInstalled('advertising'))
{
	$updater->CopyFiles("install/admin", "admin");
	$updater->CopyFiles("install/components", "components");
	//Following copy was parsed out from module installer
	$updater->CopyFiles("install/js", "js");
	$updater->CopyFiles("install/themes", "themes");
}
if ($updater->CanUpdateDatabase() && $updater->TableExists('b_adv_banner'))
{
	if ($DB->type == "MSSQL")
	{
		if ($updater->TableExists("b_adv_banner"))
		{
			if (!$DB->Query("SELECT TEMPLATE FROM b_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_adv_banner ADD TEMPLATE text NULL");
			}
			if (!$DB->Query("SELECT TEMPLATE_FILES FROM b_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_adv_banner ADD TEMPLATE_FILES varchar(1000) NULL");
			}
		}
	}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_adv_banner'))
{
	if ($DB->type == "MYSQL")
	{
		if ($updater->TableExists("b_adv_banner"))
		{
			if (!$DB->Query("SELECT TEMPLATE FROM b_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_adv_banner ADD TEMPLATE text");
			}
			if (!$DB->Query("SELECT TEMPLATE_FILES FROM b_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_adv_banner ADD TEMPLATE_FILES varchar(1000)");
			}
		}
	}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_adv_banner'))
{
	if ($DB->type == "ORACLE")
	{
		if ($updater->TableExists("b_adv_banner"))
		{
			if (!$DB->Query("SELECT TEMPLATE FROM b_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_adv_banner ADD (TEMPLATE CLOB NULL)");
			}
			if (!$DB->Query("SELECT TEMPLATE_FILES FROM b_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_adv_banner ADD (TEMPLATE_FILES VARCHAR2(1000 CHAR) NULL)");
			}
		}
	}
}


if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/advertising/install/components/bitrix/advertising.banner/component.php",
		"components/bitrix/advertising.banner/component.php",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
?>
