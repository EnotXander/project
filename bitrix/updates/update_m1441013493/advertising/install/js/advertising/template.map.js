{"version":3,"file":"template.min.js","sources":["template.js"],"names":["BXBannerTemplate","oConfig","this","MESS","lang","templates","templatesExtended","oldTemplates","tPosition","tCount","canAdd","nType","BX","nComponentHead","nComponent","nAddTemplate","nTemplateProperties","nBannerContainer","nExtMode","prototype","show","name","ind","template","checked","settings","oSettings","MULTIPLE","window","changeTemplateNodes","style","display","a","curTemplateBanner","oTProps","nProps","oTPropsHead","nPropsHead","cleanNode","create","props","className","children","width","marginBottom","targetNode","firstChild","appendChild","nextSibling","i","hasOwnProperty","appendJS","innerHTML","addDraggableItem","sortItems","addListenerControlItem","oTParams","oParams","TYPE","parseInt","positions","easing","duration","start","height","opacity","finish","transition","transitions","quart","step","state","complete","animate","insBefore","insertBefore","oVal","tNew","data","action","properties","mode","bCopy","sessid","bitrix_sessid","_this","parameters","MODE","showWait","ajax","post","curPage","res","closeWait","eval","curValue","propValueHead","propHiddenName","createTemplatePropHead","setAttribute","createHiddenNameInput","BANNER_NAME","PROPS","PARAMETERS","propValueNode","valProp","propValue","buildProperty","Object","toString","call","ar","cnt","length","text","NAME","id","SETTINGS","close","createFromDB","getName","params","val","EXTENDED_MODE","refresh","curValues","getCurValues","index","oldTemplate","allTemplateProps","curOldTemplate","curAllTemplateProps","mergeObjects","addNewTBanner","colSpan","float","cursor","title","DELETE","marginRight","HIDE","refreshAll","templateForCurVal","curTemplate","fromObject","difference","countObject","push","newTemplate","obj","count","k","type","value","target","source","prop","select","options","findChild","tagName","selected","getAttribute","ext","oTemplate","node","findChildren","tag","opt","checkb","childNodes","propObj","toUpperCase","createListInput","createCheckboxInput","createStringInput","createColorpicker","createHTMLString","createPresetSelection","multiple","size","SIZE","events","REFRESH","change","input","DEFAULT","VALUES","option","inputHid","inputCheck","IMAGES","eventInput","eventLabel","click","selectPreset","arr","pad","adminMode","radio","label","src","padding","addClass","div","margin","td","parentNode","labels","removeClass","cols","COLS","rows","ROWS","color","minWidth","script","HTML","html","patt1","RegExp","s","code1","match","substring","setTimeout","patt2","code2","createCPObject","BXColorPicker","Create","zIndex","WindowManager","GetZIndex","fid","e","OnKeyPress","OnDocumentClick","pColCont","document","body","arColors","row","cell","colorCell","tbl","l","insertRow","insertCell","defBut","jsColorPickerMess","DefaultColor","onmouseover","backgroundColor","onmouseout","onclick","Select","Math","round","oPar","bCreated","SELECT_COLOR","OnSelect","delegate","pWnd","backgroundPosition","pCont","initDraggableItems","dragdrop","DragDrop","dragItemClassName","dragItemControlClassName","sortable","rootElem","gagClass","gagHtml","dragEnd","eventObj","dragElement","event","repairEditor","item","addSortableItem","bindDragItem","removeDraggableItem","removeSortableItem","itemList","elementPosition","elementSortText","elementNameText","sort","message","querySelectorAll","querySelector","indexOf","SLIDE","container","attr","attributes","nodeName","nodeValue","messageHtmlEditor","BXHtmlEditor","Get","CheckAndReInit","buttonToggleShow","contToggleShow","bind","toggleShow","buttonDeleteItem","deleteItem","showRenameDialogButton","showRenameDialog","button","isShow","SHOW","elementDelete","remove","popupWindow","PopupWindowManager","darkMode","closeIcon","content","autoHide","setBindElement","btnRenameCancel","btnRenameSave","unbindAll","setNameItem","setNameText","setNameToDialog","dialogNameValue","util","trim","nameInput","nameText"],"mappings":"AAAA,QAASA,kBAAiBC,GAEtBC,KAAKD,QAAUA,CACfC,MAAKC,KAAOF,EAAQG,IACpBF,MAAKG,YACLH,MAAKI,oBACLJ,MAAKK,eACLL,MAAKM,YACLN,MAAKO,SACLP,MAAKQ,OAAS,IACdR,MAAKS,MAAQC,GAAG,cAChBV,MAAKW,eAAiBD,GAAG,yBACzBV,MAAKY,WAAaF,GAAG,qBACrBV,MAAKa,aAAeH,GAAG,qBACvBV,MAAKc,oBAAsBJ,GAAG,sBAC9BV,MAAKe,iBAAmBL,GAAG,kCAC3BV,MAAKgB,SAAWN,GAAG,iBAGvBZ,iBAAiBmB,WAEbC,KAAM,SAAUC,EAAMC,GAElB,GAAIC,KAAarB,KAAKgB,SAASM,QAAUtB,KAAKI,kBAAkBe,GAAQnB,KAAKG,UAAUgB,GACnFI,EAAWF,EAAS,GAAGG,SAC3BxB,MAAKQ,OAASe,GAAYA,EAASE,UAAYF,EAASE,UAAY,IAAM,MAAQ,IAClF,UAAWL,KAAQ,YACnB,CACIM,OAAOC,oBAAoB,QAC3B3B,MAAKY,WAAWgB,MAAMC,QAAU,EAChC,IAAI7B,KAAKQ,OACLR,KAAKa,aAAae,MAAMC,QAAU,OAElC7B,MAAKa,aAAae,MAAMC,QAAU,MACtC7B,MAAKc,oBAAoBc,MAAMC,QAAU,EACzC,KAAK,GAAIC,KAAKT,GACd,CACI,GAAIU,GAAoBV,EAASS,GAC7BE,EAAUD,EAAkBE,OAC5BC,EAAcH,EAAkBI,WAChCC,EAAY1B,GAAG2B,OAAO,OAClBC,OACIC,UAAW,iCAEfC,UACI9B,GAAG2B,OAAO,SACNC,OACIC,UAAW,YAEfX,OACIa,MAAO,OACPC,aAAc,QAElBF,UACI9B,GAAG2B,OAAO,SACNC,OACIC,UAAW,sBAGnB7B,GAAG2B,OAAO,SACNC,OACIC,UAAW,2BAOvC,IAAII,GAAaP,EAAUQ,WAAWA,UACtCD,GAAWE,YAAYX,EACvBS,GAAaA,EAAWG,WACxB,KAAK,GAAIC,KAAKf,GACd,CACI,GAAIA,EAAQgB,eAAeD,GAC3B,CACIJ,EAAWE,YAAYb,EAAQe,GAC/B/C,MAAKiD,SAASjB,EAAQe,GAAGG,YAGjClD,KAAKe,iBAAiB8B,YAAYT,EAClCpC,MAAKmD,iBAAiBf,EACtBpC,MAAKoD,WACLpD,MAAKqD,uBAAuBjB,QAG/B,IAAIhB,IAAQ,MACjB,CACIM,OAAOC,oBAAoB,QAC3B3B,MAAKY,WAAWgB,MAAMC,QAAU,EAChC,IAAI7B,KAAKQ,OACLR,KAAKa,aAAae,MAAMC,QAAU,OAElC7B,MAAKa,aAAae,MAAMC,QAAU,MACtC7B,MAAKc,oBAAoBc,MAAMC,QAAU,EACzC,KAAK,GAAIC,KAAKT,GACd,CACI,GAAIU,GAAoBV,EAASS,GAC7BE,EAAUD,EAAkBE,OAC5BC,EAAcH,EAAkBI,WAChCmB,EAAWvB,EAAkBwB,QAC7BnB,EAAY1B,GAAG2B,OAAO,OAClBC,OACIC,UAAW,iCAEfC,UACI9B,GAAG2B,OAAO,SACNC,OACIC,UAAW,YAEfX,OACIa,MAAO,OACPC,aAAc,QAElBF,UACI9B,GAAG2B,OAAO,SACNC,OACIC,UAAW,sBAGnB7B,GAAG2B,OAAO,SACNC,OACIC,UAAW,2BAOvC,IAAII,GAAaP,EAAUQ,WAAWA,UACtCD,GAAWE,YAAYX,EACvBS,GAAaA,EAAWG,WACxB,KAAK,GAAIC,KAAKf,GACd,CACI,GAAIA,EAAQgB,eAAeD,GAC3B,CACIJ,EAAWE,YAAYb,EAAQe,GAC/B,IAAIO,EAASP,IAAMO,EAASP,GAAGS,MAAQ,OACnCxD,KAAKiD,SAASjB,EAAQe,GAAGG,YAGrClD,KAAKe,iBAAiB8B,YAAYT,EAClCpC,MAAKmD,iBAAiBf,EACtBpC,MAAKoD,WACLpD,MAAKqD,uBAAuBjB,QAG/B,IAAIqB,SAASrC,IAAQ,EAC1B,CACI,KAAMC,EAASD,GACf,CACI,GAAIW,GAAoBV,EAASD,GAC7BY,EAAUD,EAAkBE,OAC5BC,EAAcH,EAAkBI,WAChCmB,EAAWvB,EAAkBwB,QAC7BnB,EAAY1B,GAAG2B,OAAO,OAClBC,OACIC,UAAW,iCAEfX,OACIC,QAAS,QAEbW,UACY9B,GAAG2B,OAAO,SACNC,OACIC,UAAW,YAEfX,OACIa,MAAO,OACPC,aAAc,QAElBF,UACI9B,GAAG2B,OAAO,SACNC,OACIC,UAAW,sBAGnB7B,GAAG2B,OAAO,SACNC,OACIC,UAAW,2BAQ/C,IAAII,GAAaP,EAAUQ,WAAWA,WAClCc,EAAY1D,KAAKM,UAAUa,EAE/BwB,GAAWE,YAAYX,EACvBS,GAAaA,EAAWG,WACxB,KAAK,GAAIC,KAAKf,GACd,CACI,GAAIA,EAAQgB,eAAeD,GAC3B,CACIJ,EAAWE,YAAYb,EAAQe,GAC/B,IAAIO,EAASP,GAAGS,MAAQ,OACpBxD,KAAKiD,SAASjB,EAAQe,GAAGG,YAGrC,IAAKQ,EAAUtC,GACf,CACI,GAAIuC,GAAS,GAAIjD,IAAGiD,QAChBC,SAAW,IACXC,OAAUC,OAAS,EAAGC,QAAU,GAChCC,QAAWF,OAAS,IAAKC,QAAS,KAClCE,WAAavD,GAAGiD,OAAOO,YAAYC,MACnCC,KAAO,SAASC,GACZjC,EAAUR,MAAMmC,QAAUM,EAAMN,QAAQ,GACxC3B,GAAUR,MAAMC,QAAU,SAE9ByC,SAAW,cAGf5C,QAAOC,oBAAoB,QAASP,EACpCpB,MAAKe,iBAAiB8B,YAAYT,EAClCuB,GAAOY,cAGX,CACI7C,OAAOC,oBAAoB,QAAS+B,EAAUtC,GAAK,EACnD,IAAIoD,GAAYxE,KAAKe,iBAAiByB,SAASkB,EAAUtC,GAAK,EAC9DpB,MAAKe,iBAAiB0D,aAAarC,EAAWoC,EAC9CpC,GAAUR,MAAMC,QAAU,QAE9B7B,KAAKmD,iBAAiBf,EACtBpC,MAAKoD,WACLpD,MAAKqD,uBAAuBjB,MAIxCC,OAAQ,SAAUlB,KAAMuD,KAAMC,MAE1B,GAAIC,OACAC,OAAQ,cACR1D,KAAMA,KACN2D,aAAeJ,KAAQA,KAAO,GAC9BK,KAAM/E,KAAKgB,SAASM,QAAU,IAAM,IACpC0D,MAAOhF,KAAKD,QAAQiF,MAAQ,IAAM,IAClCC,OAAQvE,GAAGwE,gBAEf,IAAIC,OAAQnF,IACZ,IAAI0E,MAAQA,KAAKU,WAAWC,MAAQ,IACpC,CACIF,MAAMnE,SAASM,QAAU,KAE7BZ,GAAG4E,UACH5E,IAAG6E,KAAKC,KACJxF,KAAKD,QAAQ0F,QAASb,KACtB,SAAUc,KACNhF,GAAGiF,WACH,MAAKD,IACL,CACIA,IAAME,KAAK,IAAMF,IAAM,IACvB,IAAIvF,aAAcgF,MAAMnE,SAASM,QAAU6D,MAAM/E,kBAAkBe,SAAagE,MAAMhF,UAAUgB,SAAY0E,QAC5GV,OAAM5E,OAAOY,MAAQ,CACrBgE,OAAM9E,aAAac,QACnB,IAAI2D,YAAYgB,cAAeC,cAC/B,KAAK,GAAIjE,KAAK4D,KACd,CAEIZ,aACAgB,eAAgBX,MAAMa,uBAAuBlE,EAC7CgE,eAAclD,WAAWqD,aAAa,QAAS,6BAC/CF,gBAAiBZ,MAAMe,sBAAsBR,IAAI5D,GAAGqE,YAAarE,EACjEgE,eAAcjD,YAAYkD,eAC1BF,YAAanB,KAAOA,KAAKU,WAAWgB,MAAMtE,KAC1C,KAAK,GAAIiB,KAAK2C,KAAI5D,GAAGuE,WACrB,CAEI,GAAIX,IAAI5D,GAAGuE,WAAWrD,eAAeD,GACrC,CACI,GAAIuD,eACA5F,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfX,OACIa,MAAO,QAGnB,IAAI8D,WAAaV,SAAS9C,GAAM8C,SAAS9C,GAAK,KAC9C,IAAIyD,WAAYrB,MAAMsB,cAAcf,IAAI5D,GAAGuE,WAAWtD,GAAIwD,QAASxD,EAAG4B,KAAMQ,MAAM5E,OAAOY,MACzF,IAAIuF,OAAOzF,UAAU0F,SAASC,KAAKJ,aAAe,iBAClD,CACI,IAAK,GAAIK,IAAK,EAAGC,IAAMN,UAAUO,OAAQF,GAAKC,IAAKD,KAC/CP,cAAczD,YAAY2D,UAAUK,SAEvC,UAAWL,aAAc,SAC1BF,cAAcpD,UAAYsD,cAE1BF,eAAczD,YAAY2D,UAE9B1B,YAAW/B,GAAKrC,GAAG2B,OAAO,MACtBG,UACI9B,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfyE,KAAMtB,IAAI5D,GAAGuE,WAAWtD,GAAGkE,KAAO,IAClCrF,OACIa,MAAO,SAGf6D,eAEJhE,OACI4E,GAAI,kBAAoBpF,EAAI,IAAMiB,IAG1C+B,YAAW/B,GAAGkD,aAAa,SAAU,QAG7C,GAAI1E,UAAWmE,IAAI5D,GAAGqF,UAAY,KAClChH,WAAU2B,IAAMG,OAAQ6C,WAAYvB,QAASmC,IAAI5D,GAAGuE,WAAYlE,WAAY2D,cAAetE,UAAWD,SACtG4D,OAAM5E,OAAOY,QAEjBgE,MAAMjE,KAAKC,UAGXgE,OAAMiC,WAGtBC,aAAc,SAAU3B,GAEpB,KAAKA,EACL,CACI,GAAIZ,GAAYgB,EAAeC,EAC3B5E,EAAOnB,KAAKsH,UACZC,EAAS7B,EAAI6B,OACbC,EAAM9B,EAAI8B,GACd,IAAI9B,GAAOA,EAAI8B,KAAO9B,EAAI8B,IAAI,IAAM9B,EAAI8B,IAAI,GAAGC,eAAiB,IAC5DzH,KAAKgB,SAASM,QAAU,IAC5B,IAAInB,KAAcH,KAAKgB,SAASM,QAAUtB,KAAKI,kBAAkBe,MAAanB,KAAKG,UAAUgB,MAAY0E,CACzG7F,MAAKO,OAAOY,GAAQ,CACpBnB,MAAKK,aAAac,KAClB,KAAK,GAAIW,KAAKyF,GACd,CAEIzC,IACAgB,GAAgB9F,KAAKgG,uBAAuBlE,EAC5CgE,GAAclD,WAAWqD,aAAa,QAAS,6BAC/CF,GAAiB/F,KAAKkG,sBAAsBqB,EAAOzF,GAAGqE,YAAarE,EACnEgE,GAAcjD,YAAYkD,EAC1BF,KAAa2B,EAAMA,EAAI1F,KACvB,KAAK,GAAIiB,KAAKwE,GAAOzF,GAAGuE,WACxB,CAEI,GAAIkB,EAAOzF,GAAGuE,WAAWrD,eAAeD,GACxC,CACI,GAAIuD,GACA5F,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfX,OACIa,MAAO,QAGnB,IAAI8D,KAAaV,EAAS9C,GAAM8C,EAAS9C,GAAK,KAC9C,IAAIyD,GAAYxG,KAAKyG,cAAcc,EAAOzF,GAAGuE,WAAWtD,GAAIwD,EAASxD,EAAG,MAAO/C,KAAKO,OAAOY,GAC3F,IAAIuF,OAAOzF,UAAU0F,SAASC,KAAKJ,KAAe,iBAClD,CACI,IAAK,GAAIK,GAAK,EAAGC,EAAMN,EAAUO,OAAQF,EAAKC,EAAKD,IAC/CP,EAAczD,YAAY2D,EAAUK,QAEvC,UAAWL,KAAc,SAC1BF,EAAcpD,UAAYsD,MAE1BF,GAAczD,YAAY2D,EAE9B1B,GAAW/B,GAAKrC,GAAG2B,OAAO,MACtBG,UACI9B,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfyE,KAAMO,EAAOzF,GAAGuE,WAAWtD,GAAGkE,KAAO,IACrCrF,OACIa,MAAO,SAGf6D,GAEJhE,OACI4E,GAAI,kBAAoBpF,EAAI,IAAMiB,IAG1C+B,GAAW/B,GAAGkD,aAAa,SAAU,QAG7C,GAAI1E,GAAWgG,EAAOzF,GAAGqF,UAAY,KACrChH,GAAU2B,IAAMG,OAAQ6C,EAAYvB,QAASgE,EAAOzF,GAAGuE,WAAYlE,WAAY2D,EAAetE,UAAWD,EACzGvB,MAAKO,OAAOY,KAEhBnB,KAAKkB,KAAKC,OAGVnB,MAAKoH,SAEbM,QAAS,SAAUtG,KAEf,GAAIuG,WAAY3H,KAAK4H,aAAaxG,IAAKpB,KAAKgB,SAASM,SACjDH,KAAOnB,KAAKsH,UACZnC,MAAQnF,KACR4E,MACIC,OAAQ,kBACR1D,KAAMA,KACNwG,YAAcA,UAAaA,UAAY,GACvC5C,KAAM/E,KAAKgB,SAASM,QAAU,IAAM,IACpCuG,MAAOzG,IACP6D,OAAQvE,GAAGwE,gBAEnBxE,IAAG4E,UACH5E,IAAG6E,KAAKC,KACJxF,KAAKD,QAAQ0F,QAASb,KACtB,SAAUc,KACNhF,GAAGiF,WACH,MAAKD,IACL,CACIA,IAAME,KAAK,IAAMF,IAAM,IACvB,IAAIoC,eAAgB3C,MAAMnE,SAASM,QAAU6D,MAAM/E,kBAAkBe,MAAQgE,MAAMhF,UAAUgB,MACzF4G,iBAAmB5C,MAAM9E,aAAac,MACtC6G,eAAiBF,YAAY1G,KAC7B6G,oBAAsBF,iBAAiB3G,SACvC0D,cACAM,WAAaM,IAAI,GAAGW,UACxB,KAAK,GAAItD,KAAKqC,YACd,CACI,GAAIA,WAAWpC,eAAeD,GAC9B,CACI,IAAKiF,eAAe/F,OAAOc,KAAOkF,oBAAoBlF,GACtD,CACI,GAAIuD,eACA5F,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfX,OACIa,MAAO,QAGnB,IAAI+D,WAAYrB,MAAMsB,cAAcrB,WAAWrC,GAAI,MAAOA,EAAG,KAAM3B,IACnE,IAAIsF,OAAOzF,UAAU0F,SAASC,KAAKJ,aAAe,iBAClD,CACI,IAAK,GAAIK,IAAK,EAAGC,IAAMN,UAAUO,OAAQF,GAAKC,IAAKD,KAC/CP,cAAczD,YAAY2D,UAAUK,SAEvC,UAAWL,aAAc,SAC9B,CACIF,cAAcpD,UAAYsD,SAC1B,IAAIpB,WAAWrC,GAAGS,MAAQ,OACtB2B,MAAMlC,SAASuD,eAGnBF,eAAczD,YAAY2D,UAE9B1B,YAAW/B,GAAKrC,GAAG2B,OAAO,MACtBG,UACI9B,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfyE,KAAM5B,WAAWrC,GAAGkE,KAAO,IAC3BrF,OACIa,MAAO,SAGf6D,eAEJhE,OACI4E,GAAI,kBAAoB9F,IAAM,IAAM2B,IAG5C+B,YAAW/B,GAAGkD,aAAa,SAAU,WAEpC,IAAI+B,eAAe/F,OAAOc,GAC3B+B,WAAW/B,GAAKiF,eAAe/F,OAAOc,OAEtC+B,YAAW/B,GAAKkF,oBAAoBlF,IAGhDgF,iBAAiB3G,KAAO+D,MAAM+C,aAAaF,eAAe/F,OAAQgG,oBAClED,gBAAe/F,OAAS6C,UACxBkD,gBAAezE,QAAU6B,UAEzBD,OAAMjE,KAAKC,KAAMC,SAGjB+D,OAAMiC,WAGtBe,cAAe,WAEX,GAAIhH,MAAOnB,KAAKsH,UACZ1C,MACIC,OAAQ,mBACR1D,KAAMA,KACN2D,WAAY,GACZC,KAAM/E,KAAKgB,SAASM,QAAU,IAAM,IACpCuG,MAAO7H,KAAKO,OAAOY,MACnB8D,OAAQvE,GAAGwE,gBAEnB,IAAIC,OAAQnF,IACZU,IAAG4E,UACH5E,IAAG6E,KAAKC,KACJxF,KAAKD,QAAQ0F,QAASb,KACtB,SAAUc,KACNhF,GAAGiF,WACH,MAAKD,IACL,CACIA,IAAME,KAAK,IAAMF,IAAM,IACvB,IAAIZ,eAAiBgB,cAAeC,eAChC1E,WAAa8D,MAAMnE,SAASM,QAAU6D,MAAM/E,kBAAkBe,MAAQgE,MAAMhF,UAAUgB,KAC1F2E,eAAgBX,MAAMa,uBAAuBb,MAAM5E,OAAOY,MAC1D2E,eAAclD,WAAWqD,aAAa,QAAS,6BAC/CF,gBAAiBZ,MAAMe,sBAAsB,GAAIf,MAAM5E,OAAOY,MAC9D2E,eAAcjD,YAAYkD,eAC1B,KAAK,GAAIhD,KAAK2C,KAAI,GAAGW,WACrB,CACI,GAAIX,IAAI,GAAGW,WAAWrD,eAAeD,GACrC,CACI,GAAIuD,eACA5F,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfX,OACIa,MAAO,QAGnB,IAAI+D,WAAYrB,MAAMsB,cAAcf,IAAI,GAAGW,WAAWtD,GAAI,MAAOA,EAAG,KAAMoC,MAAM5E,OAAOY,MACvF,IAAIuF,OAAOzF,UAAU0F,SAASC,KAAKJ,aAAe,iBAClD,CACI,IAAK,GAAIK,IAAK,EAAGC,IAAMN,UAAUO,OAAQF,GAAKC,IAAKD,KAC/CP,cAAczD,YAAY2D,UAAUK,SAEvC,UAAWL,aAAc,SAC9B,CACIF,cAAcpD,UAAYsD,SAC1B,IAAId,IAAI,GAAGW,WAAWtD,GAAGS,MAAQ,OAC7B2B,MAAMlC,SAASuD,eAGnBF,eAAczD,YAAY2D,UAE9B1B,YAAW/B,GAAKrC,GAAG2B,OAAO,MACtBG,UACI9B,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfyE,KAAMtB,IAAI,GAAGW,WAAWtD,GAAGkE,KAAO,IAClCrF,OACIa,MAAO,SAGf6D,eAEJhE,OACI4E,GAAI,kBAAoB/B,MAAM5E,OAAOY,MAAQ,IAAM4B,IAG3D+B,YAAW/B,GAAGkD,aAAa,SAAU,QAG7C5E,SAAS8D,MAAM5E,OAAOY,QAAUc,OAAQ6C,WAAYvB,QAASmC,IAAI,GAAGW,WAAYlE,WAAY2D,cAC5FX,OAAMjE,KAAKC,KAAMgE,MAAM5E,OAAOY,MAC9BgE,OAAM5E,OAAOY,YAI7B6E,uBAAwB,SAAUlE,GAE9B,MAAOpB,IAAG2B,OAAO,MACbC,OACI4E,GAAI,qBAAuBpF,EAC3BS,UAAW,WAEfC,UACI9B,GAAG2B,OAAO,MACNC,OACI8F,QAAS,GAEb5F,UACI9B,GAAG2B,OAAO,QACV3B,GAAG2B,OAAO,QACNC,OACIC,UAAW,wBAEfX,OACIyG,QAAO,OACPvE,OAAQ,OACRwE,OAAQ,UAGhB5H,GAAG2B,OAAO,QACNC,OACIC,UAAW,2BACXgG,MAAOvI,KAAKC,KAAKuI,QAErB5G,OACIyG,QAAO,QACPI,YAAa,OACbH,OAAQ,WAEZtB,KAAM,MAEVtG,GAAG2B,OAAO,QACNC,OACIC,UAAW,iDAEfX,OACIyG,QAAO,QACPI,YAAa,OACbH,OAAQ,WAEZtB,KAAMhH,KAAKC,KAAKyI,cAOxCC,WAAY,WAER,GAAIxH,MAAOnB,KAAKsH,UACZsB,oBAAsB5I,KAAKgB,SAASM,QAAUtB,KAAKI,kBAAkBe,MAAQnB,KAAKG,UAAUgB,MAC5F0H,aAAe7I,KAAKgB,SAASM,QAAUtB,KAAKI,kBAAkBe,MAAQnB,KAAKG,UAAUgB,MACrFwG,aACA5C,OAAS/E,KAAKgB,SAASM,QACvBwH,WAAa,KACbC,WAAa/I,KAAKgJ,YAAYH,aAAe7I,KAAKgJ,YAAYJ,kBAClE,KAAKA,kBACL,CACIA,kBAAoBC,WACpB9D,OAAQ/E,KAAKgB,SAASM,OACtBwH,YAAa,KACbC,YAAa,EAEjB,IAAK,GAAIhG,KAAK6F,mBACd,CACIjB,UAAUsB,KAAKjJ,KAAK4H,aAAa7E,EAAGgC,KAAM+D,aAE9C,GAAIC,WAAa,EACjB,CACI,IAAK,GAAIhG,GAAI,EAAGA,EAAIgG,WAAYhG,IAChC,CACI4E,UAAUsB,MAAMxB,gBAAiBzH,KAAKgB,SAASM,QAAU,IAAM,OAGvE,GAAI6D,OAAQnF,KACR4E,MACIC,OAAQ,aACR1D,KAAMA,KACNwG,YAAcA,UAAaA,UAAY,GACvC5C,KAAM/E,KAAKgB,SAASM,QAAU,IAAM,IACpC2D,OAAQvE,GAAGwE,gBAEnBxE,IAAG4E,UACH5E,IAAG6E,KAAKC,KACJxF,KAAKD,QAAQ0F,QAASb,KACtB,SAAUc,KACNhF,GAAGiF,WACH,MAAKD,IACL,CACIA,IAAME,KAAK,IAAMF,IAAM,IACvB,IAAIvF,cAAgB0I,YAAaK,WACjC,MAAM/D,MAAMnE,SAASM,QACrB,CACI4H,YAAc/D,MAAM/E,kBAAkBe,MAAQgE,MAAM/E,kBAAkBe,QACtE0H,aAAc1D,MAAMhF,UAAUgB,UAGlC,CACI+H,YAAc/D,MAAMhF,UAAUgB,MAAQgE,MAAMhF,UAAUgB,QACtD0H,aAAc1D,MAAM/E,kBAAkBe,MAE1CgE,MAAM5E,OAAOY,MAAQ,CACrBgE,OAAM9E,aAAac,QACnB,IAAI2E,eAAeC,cACnB,KAAK,GAAIjE,KAAK4D,KACd,CAEI,GAAIZ,cACJgB,eAAgBX,MAAMa,uBAAuBlE,EAC7CgE,eAAclD,WAAWqD,aAAa,QAAS,6BAC/CF,gBAAiBZ,MAAMe,sBAAsBR,IAAI5D,GAAGqE,YAAarE,EACjEgE,eAAcjD,YAAYkD,eAE1B,KAAK,GAAIhD,KAAK2C,KAAI5D,GAAGuE,WACrB,CAEI,GAAIX,IAAI5D,GAAGuE,WAAWrD,eAAeD,GACrC,CACI,GAAI8F,YAAY/G,GAAGyB,QAAQR,IAAM2C,IAAI5D,GAAGuE,WAAWtD,GAAGS,MAAQqF,YAAY/G,GAAGyB,QAAQR,GAAGS,MAAQqF,YAAY/G,GAAGG,OAAOc,GACtH,CACI+B,WAAW/B,GAAK8F,YAAY/G,GAAGG,OAAOc,OAErC,IAAImG,YAAYpH,IAAMoH,YAAYpH,GAAGG,OAAOc,GACjD,CACI+B,WAAW/B,GAAKmG,YAAYpH,GAAGG,OAAOc,OAG1C,CACI,GAAIuD,eACA5F,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfX,OACIa,MAAO,QAGnB,IAAI+D,WAAYrB,MAAMsB,cAAcf,IAAI5D,GAAGuE,WAAWtD,GAAI,MAAOA,EAAG,KAAMoC,MAAM5E,OAAOY,MACvF,IAAIuF,OAAOzF,UAAU0F,SAASC,KAAKJ,aAAe,iBAClD,CACI,IAAK,GAAIK,IAAK,EAAGC,IAAMN,UAAUO,OAAQF,GAAKC,IAAKD,KAC/CP,cAAczD,YAAY2D,UAAUK,SAEvC,UAAWL,aAAc,SAC1BF,cAAcpD,UAAYsD,cAE1BF,eAAczD,YAAY2D,UAE9B1B,YAAW/B,GAAKrC,GAAG2B,OAAO,MACtBG,UACI9B,GAAG2B,OAAO,MACNC,OACIC,UAAW,6BAEfyE,KAAMtB,IAAI5D,GAAGuE,WAAWtD,GAAGkE,KAAO,IAClCrF,OACIa,MAAO,SAGf6D,eAEJhE,OACI4E,GAAI,kBAAoBpF,EAAI,IAAMiB,IAG1C+B,YAAW/B,GAAGkD,aAAa,SAAU,SAIjD9F,UAAU2B,IACNG,OAAQ6C,WACRvB,QAASmC,IAAI5D,GAAGuE,WAChBlE,WAAY2D,cAEhBX,OAAM5E,OAAOY,QAEjB,KAAMgE,MAAMnE,SAASM,QACrB,CACI6D,MAAM/E,kBAAkBe,MAAQhB,cAGpC,CACIgF,MAAMhF,UAAUgB,MAAQhB,UAE5BgF,MAAMjE,KAAKC,KAAM,WAGjBgE,OAAMiC,WAItB4B,YAAa,SAAUG,GAEnB,GAAIC,GAAQ,CACZ,KAAK,GAAIC,KAAKF,GAAK,CACf,GAAIA,EAAInG,eAAeqG,GACvB,GACMD,GAGV,MAAOA,IAEXlD,sBAAuB,SAAU/E,EAAMW,GAEnC,MAAOpB,IAAG2B,OAAO,SACbC,OACIgH,KAAM,SACN/G,UAAW,wBACXgH,MAAOpI,GAAQnB,KAAKC,KAAKgH,KACzB9F,KAAM,iBAAmBW,EAAI,qBAIzCoG,aAAc,SAAUsB,EAAQC,GAE5BD,EAASA,KACT,KAAK,GAAIE,KAAQD,GACjB,CACI,GAAIA,EAAOzG,eAAe0G,GACtBF,EAAOE,GAAQD,EAAOC,GAE9B,MAAOF,IAEXG,OAAQ,SAAUjF,GAEd,GAAIvD,GAAOnB,KAAKsH,QAAQ5C,GACpBC,GAASD,EAAQ,KAAO,MACxBrD,IAAarB,KAAKgB,SAASM,QAAUtB,KAAKI,kBAAkBe,GAAQnB,KAAKG,UAAUgB,EACvF,KAAKA,EACDnB,KAAKoH,YACJ,IAAG/F,EACJrB,KAAKkB,KAAKC,EAAM,WAEhBnB,MAAKqC,OAAOlB,EAAMuD,EAAMC,IAEhC2C,QAAS,SAAU5C,GAEf,GAAIkF,GAAUlJ,GAAGmJ,UAAU7J,KAAKS,OAAQqJ,QAAQ,UAAW,KAAM,MAC7D3I,EAAO,GACPuD,EAAOA,GAAQ,KAEnB,IAAIA,GAAQA,EAAKU,WAAW6B,KACxB9F,EAAOuD,EAAKU,WAAW6B,SAE3B,CACI,IAAK,GAAIlE,KAAK6G,GACd,CACI,GAAIA,EAAQ7G,GAAGgH,SACX5I,EAAOyI,EAAQ7G,GAAGiH,aAAa,cAG3C,MAAO7I,IAEXyG,aAAc,SAAUxG,EAAK6I,EAAKnB,GAE9B,GAAI3H,GAAOnB,KAAKsH,UACZ4C,IAAcD,EAAMjK,KAAKI,kBAAkBe,GAAQnB,KAAKG,UAAUgB,GAClEa,EAAUkI,EAAU9I,GAAKmC,QACzBoE,KACAwC,CACJ,KAAK,GAAIpH,KAAKf,GACd,CACI,GAAIA,EAAQgB,eAAeD,GAC3B,CACIoH,EAAOrB,EAAaoB,EAAU9I,GAAKa,OAAOc,GAAKrC,GAAG,iBAAmBU,EAAM,IAAM2B,EACjF,IAAIf,EAAQe,GAAGS,MAAQ,OACvB,CACI,GAAIoG,GAAUlJ,GAAG0J,aAAaD,GAAOE,IAAK,UAAW,KAAM,KAC3D,IAAIT,GAAWA,EAAQ7C,OAAS,EAChC,CACI,GAAI/E,EAAQe,GAAGtB,UAAY,IAC3B,CACIkG,EAAU5E,KACV,KAAK,GAAIuH,KAAOV,GAChB,CACI,GAAIA,EAAQU,GAAKP,SACbpC,EAAU5E,GAAGkG,KAAKW,EAAQU,GAAKf,YAI3C,CACI,IAAK,GAAIe,KAAOV,GAChB,CACI,GAAIA,EAAQU,GAAKP,SACbpC,EAAU5E,GAAK6G,EAAQU,GAAKf,SAKhD,GAAIvH,EAAQe,GAAGS,MAAQ,WACvB,CACI,GAAI+G,GAASzB,EACPqB,EAAKK,WAAW,GAAGA,WAAW,GAC9B9J,GAAG,iBAAmBU,EAAM,IAAO2B,EACzC,IAAIwH,EACJ,CACI,GAAIA,EAAOjB,MAAQ,YAAciB,EAAOjJ,QACpCqG,EAAU5E,GAAK,QAEf4E,GAAU5E,GAAK,OAKnC4E,EAAU,iBAAmBjH,GAAG,iBAAiBY,QAAU,IAAM,GACjE,OAAOqG,IAEXP,MAAO,WAEHpH,KAAKY,WAAWgB,MAAMC,QAAU,MAChC7B,MAAKW,eAAeiB,MAAMC,QAAU,MACpC7B,MAAKa,aAAae,MAAMC,QAAU,MAClCH,QAAOC,oBAAoB,UAE/B8E,cAAe,SAAUgE,EAASlB,EAAOpI,EAAMwD,EAAMkD,GAEjD,GAAIyB,GAAOmB,EAAQjH,KAAKkH,aACxB,QAAOpB,GAEH,IAAK,OAAQ,MAAOtJ,MAAK2K,gBAAgBF,EAASlB,EAAOpI,EAAMwD,EAAMkD,EACrE,KAAK,WAAY,MAAO7H,MAAK4K,oBAAoBH,EAASlB,EAAOpI,EAAM0G,EACvE,KAAK,SAAU,MAAO7H,MAAK6K,kBAAkBJ,EAAQlB,EAAOpI,EAAMwD,EAAMkD,EACxE,KAAK,cAAe,MAAO7H,MAAK8K,kBAAkBL,EAASlB,EAAOpI,EAAMwD,EAAMkD,EAC9E,KAAK,QAAS,MAAO7H,MAAK+K,iBAAiBN,EAC3C,KAAK,OAAQ,MAAOzK,MAAK+K,iBAAiBN,EAC1C,KAAK,OAAQ,MAAOzK,MAAK+K,iBAAiBN,EAC1C,KAAK,SAAU,MAAOzK,MAAKgL,sBAAsBP,EAASlB,EAAOpI,EAAMwD,EAAMkD,EAC7E,SAAS,MAAO7H,MAAK6K,kBAAkBJ,EAAQlB,EAAOpI,EAAMwD,EAAMkD,KAG1E8C,gBAAiB,SAAUF,EAASlB,EAAOpI,EAAMwD,EAAMkD,GAEnD,GAAIoD,GAAYR,EAAQhJ,UAAYgJ,EAAQhJ,UAAY,IAAO,KAAO,KACtE,IAAIL,GAAMqC,SAASoE,IAAU,EAAIA,EAAQ,EACrCX,EAAK,iBAAmB9F,EAAM,IAAMD,EACpCA,EAAO,iBAAmBC,EAAM,KAAOD,EAAO,KAAO8J,EAAW,KAAO,IACvEC,EAAOzH,SAASgH,EAAQU,KAAM,IAAKpB,EAAUqB,EAC7CjG,EAAQnF,IAEZ,KAAKkL,EACDA,EAAOD,EAAW,EAAI,CAE1B,IAAIR,EAAQY,SAAWZ,EAAQY,SAAW,IACtCD,GAAUE,OAAQ,WAAWnG,EAAMuC,QAAQtG,IAE/C,IAAImK,GAAQ7K,GAAG2B,OACX,UACIC,OACI4E,GAAIA,EACJ/F,KAAMA,EACN8J,SAAUA,EACVC,KAAMA,GAEVE,OAAQA,GAGhB,IAAIzG,EACJ,CACI,GAAIsG,EACJ,CACI1B,IACA,KAAK,GAAIzH,KAAK2I,GAAQe,QACtB,CACIf,EAAQe,QAAQ1J,KAAO2I,EAAQe,QAAQ1J,GAAK2I,EAAQe,QAAQ1J,GAAK,EACjEyH,GAAMN,KAAKwB,EAAQe,QAAQ1J,SAI/ByH,KAAWkB,EAAQe,QAAUf,EAAQe,QAAU,GAGvD,IAAK,GAAIzI,KAAK0H,GAAQgB,OACtB,CACI1B,EAAW,KACX,IAAIkB,EACJ,CACI,IAAK,GAAI5B,KAAKE,GACd,CACI,GAAIxG,GAAKwG,EAAMF,GACf,CACIU,EAAW,IACX,aAKRA,GAAYhH,GAAKwG,EAAS,KAAO,KAErC,IAAIkB,EAAQgB,OAAOzI,eAAeD,GAClC,CACI,GAAI2I,GAAShL,GAAG2B,OAAO,UACnBC,OACIiH,MAAOxG,EACPgH,SAAUA,GAEd/C,KAAMyD,EAAQgB,OAAO1I,IAEzBwI,GAAM1I,YAAY6I,IAG1B,MAAOH,IAEXX,oBAAqB,SAAUH,EAASlB,EAAOpI,EAAM0G,GAEjD,GAAIuD,GAAQjG,EAAQnF,KAAMoB,EAAMqC,SAASoE,IAAU,EAAIA,EAAQ,CAC/D,IAAI4C,EAAQY,SAAWZ,EAAQY,SAAW,IACtCD,GAAUE,OAAQ,WAAWnG,EAAMuC,QAAQtG,IAC/C,IAAIuK,GAAWjL,GAAG2B,OACd,SACIC,OACIgH,KAAM,SACNnI,KAAM,iBAAmBC,EAAM,KAAOD,EAAO,IAC7CoI,MAAO,MAGnB,IAAIqC,GAAalL,GAAG2B,OAChB,SACIC,OACI4E,GAAI,iBAAmB9F,EAAM,IAAMD,EACnCmI,KAAM,WACNnI,KAAM,iBAAmBC,EAAM,KAAOD,EAAO,IAC7CoI,MAAO,KAEX6B,OAAQA,GAEhB,KAAI7B,GAASkB,EAAQe,SAAW,KAAOjC,GAAS,IAC5CqC,EAAWtK,QAAU,IACzB,QAAQqK,EAAUC,IAEtBZ,sBAAuB,SAAUP,EAASlB,EAAOpI,EAAMwD,EAAMkD,GAEzD,IAAK4C,EAAQoB,OAAQ,QACrB,IAAIC,GAAYC,GAAcC,MAAO,WAAW7G,EAAM8G,aAAajM,QAC/DkM,KAAU/G,EAAQnF,KAClBoB,EAAMqC,SAASoE,IAAU,EAAIA,EAAQ,CACzC,IAAIsE,GAAMnM,KAAKD,QAAQqM,UAAY,kBAAoB,KACvD,IAAI3B,EAAQY,SAAWZ,EAAQY,SAAW,IACtCS,GAAcR,OAAQ,WAAWnG,EAAMuC,QAAQtG,IACnDmI,KAAUA,EAAQ9F,SAAS8F,KAAYkB,EAAQe,QAAU/H,SAASgH,EAAQe,SAAW,CACrF,KAAK,GAAIzI,KAAK0H,GAAQoB,OACtB,CACI,GAAIQ,GAAQ3L,GAAG2B,OACX,SACIC,OACIgH,KAAM,QACN/G,UAAW,eACXpB,KAAM,iBAAmBC,EAAM,KAAOD,EAAO,IAC7C+F,GAAI,SAAW9F,EAAM,IAAM2B,EAC3BwG,MAAOxG,GAEXqI,OAAQU,GAEhB,IAAIQ,GAAQ5L,GAAG2B,OACX,SACIG,UACI9B,GAAG2B,OACC,OACIC,OACIiK,IAAK9B,EAAQoB,OAAO9I,IAExBnB,OACI4K,QAASL,MAIzB7J,OACIC,UAAW,UAEf6I,OAAQW,GAEhBO,GAAMrG,aAAa,MAAO,SAAW7E,EAAM,IAAM2B,EACjD,IAAIwG,GAASxG,EACb,CACIsJ,EAAM/K,QAAU,IAChBZ,IAAG+L,SAASH,EAAO,kBAEvB,GAAII,GAAMhM,GAAG2B,OAAO,OAAQT,OAAOC,QAAS,eAAgB8K,OAAQ,UACpED,GAAI7J,YAAYwJ,EAChBK,GAAI7J,YAAYyJ,EAChBJ,GAAIjD,KAAKyD,GAEb,MAAOR,IAEXD,aAAc,SAAUK,GAEpB,GAAIM,GAAKN,EAAMO,WAAWA,UAC1B,IAAIC,GAASpM,GAAG0J,aAAawC,GAAK9C,QAAS,QAASvH,UAAW,UAAW,KAC1E,KAAK,GAAIQ,KAAK+J,GACd,CACIpM,GAAGqM,YAAYD,EAAO/J,GAAI,kBAE9BrC,GAAG+L,SAASH,EAAO,mBAEvBzB,kBAAmB,SAAUJ,EAASlB,EAAOpI,EAAMwD,EAAMkD,GAErD,GAAIb,GAAQrC,IAAU8F,EAAQe,QAAUf,EAAQe,QAAU,KAASjC,EAAQA,EAAQ,GAC/EyD,EAAOvJ,SAASgH,EAAQwC,OAAS,GACjCC,EAAOzJ,SAASgH,EAAQ0C,MACxB/L,EAAMqC,SAASoE,IAAU,EAAIA,EAAQ,CACzC,IAAIqF,GAAQA,EAAO,EACf,GAAI3B,GAAQ7K,GAAG2B,OACX,YACIC,OACInB,KAAM,iBAAmBC,EAAM,KAAOD,EAAO,IAC7C6L,KAAMA,EACNE,KAAMA,EACN3D,MAAOvC,SAInB,IAAIuE,GAAQ7K,GAAG2B,OACX,SACIC,OACIgH,KAAM,OACNnI,KAAM,iBAAmBC,EAAM,KAAOD,EAAO,IAC7C+J,KAAM,KACN3B,MAAOvC,IAGvB,OAAOuE,IAEXT,kBAAmB,SAAUL,EAASlB,EAAOpI,EAAMwD,EAAMkD,GAErD,GAAIuF,GAASzI,IAAU8F,EAAQe,QAAUf,EAAQe,QAAU,WAAejC,EAAQA,EAAQ,GACtFnI,EAAMqC,SAASoE,IAAU,EAAIA,EAAQ,CACzC,IAAI0D,GAAQ7K,GAAG2B,OACX,SACIC,OACI4I,KAAM,GACN/J,KAAM,iBAAmBC,EAAM,KAAOD,EAAO,IAC7CoI,MAAO6D,EACP9D,KAAM,OACNpC,GAAI,SAAW9F,EAAM,IAAMD,GAE/BS,OACIyL,SAAU,QACVhF,QAAO,OACPI,YAAa,QAIzB,IAAI6E,GAAS5M,GAAG2B,OACZ,UACI2E,KAAM,iJAAoJ5F,EAAI,IAAID,EAAK,OAASiM,EAAM,SAG9L,QAAQ7B,EAAO+B,IAEnBvC,iBAAkB,SAAUN,GAExB,MAAOA,GAAQ8C,MAEnBtK,SAAU,SAAUuK,GAEhB,GAAIC,GAAQ,GAAIC,QAAQ,IAAI,SAAS,cAAc,IAAK,SAAS,IAAK,MAAOC,CAC7E,IAAIC,GAAQJ,EAAKK,MAAMJ,EACvB,IAAIG,EACJ,CACI,IAAI,GAAI7K,GAAI,EAAGA,EAAI6K,EAAM7G,OAAQhE,IACjC,CACI,GAAI6K,EAAM7K,IAAM,GAChB,CACI4K,EAAIC,EAAM7K,GAAG+K,UAAU,EAAGF,EAAM7K,GAAGgE,OAAO,EAC1CrF,QAAOqM,WAAWJ,EAAG,KAIjC,GAAIK,GAAQ,GAAIN,QAAQ,IAAI,gCAAkC,cAAc,IAAK,SAAS,IAAK,MAAOC,CACtG,IAAIM,GAAQT,EAAKK,MAAMG,EACvB,IAAIC,EACJ,CACI,IAAI,GAAIlL,GAAI,EAAGA,EAAIkL,EAAMlH,OAAQhE,IACjC,CACI,GAAIkL,EAAMlL,IAAM,GAChB,CACI4K,EAAIM,EAAMlL,GAAG+K,UAAU,GAAIG,EAAMlL,GAAGgE,OAAO,EAC3CrF,QAAOqM,WAAWJ,EAAG,OAKrCO,eAAgB,SAAU/M,EAAMiM,GAE5Be,cAAclN,UAAUmN,OAAS,WAE7B,GAAIjJ,GAAQnF,IACZA,MAAKqO,QAAU3N,GAAG4N,cAAgB5N,GAAG4N,cAAcC,YAAc,CACjE7M,QAAO,sBAAwB1B,KAAKwO,KAAO,SAASC,GAAGtJ,EAAMuJ,WAAWD,GACxE/M,QAAO,mBAAqB1B,KAAKwO,KAAO,SAASC,GAAGtJ,EAAMwJ,gBAAgBF,GAE1EzO,MAAK4O,SAAWC,SAASC,KAAKjM,YAAYnC,GAAG2B,OAAO,OAAQC,OAAQC,UAAW,kBAAmBX,OAAQyM,OAAQrO,KAAKqO,UAEvH,IAAIU,IACA,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UACrK,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UACrK,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UACrK,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UACrK,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UACrK,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UACrK,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAAW,UAGzK,IACIC,GAAKC,EAAMC,EACXC,EAAMzO,GAAG2B,OAAO,SAAUC,OAAOC,UAAW,mBAC5CQ,EAAGqM,EAAIL,EAAShI,MAEpBiI,GAAMG,EAAIE,WAAW,EACrBJ,GAAOD,EAAIM,YAAY,EACvBL,GAAK7G,QAAU,CACf,IAAImH,GAASN,EAAKpM,YAAYnC,GAAG2B,OAAO,QAASC,OAAOC,UAAW,uBACnEgN,GAAOrM,UAAYxB,OAAO8N,kBAAkBC,YAC5CF,GAAOG,YAAc,WAEjB1P,KAAKuC,UAAY,0CACjB2M,GAAUtN,MAAM+N,gBAAkB,cAEtCJ,GAAOK,WAAa,WAAW5P,KAAKuC,UAAY,oBAChDgN,GAAOM,QAAU,SAASpB,GAAGtJ,EAAM2K,OAAO,OAE1CZ,GAAYF,EAAIM,YAAY,EAC5BJ,GAAU9G,QAAU,CACpB8G,GAAU3M,UAAY,mBACtB2M,GAAUtN,MAAM+N,gBAAkB,IAAIvC,CAEtC,KAAIrK,EAAI,EAAGA,EAAIqM,EAAGrM,IAClB,CACI,GAAIgN,KAAKC,MAAMjN,EAAI,KAAOA,EAAI,GAC1BiM,EAAMG,EAAIE,WAAW,EAEzBJ,GAAOD,EAAIM,YAAY,EACvBL,GAAK/L,UAAY,QACjB+L,GAAK1M,UAAY,oBACjB0M,GAAKrN,MAAM+N,gBAAkBZ,EAAShM,EACtCkM,GAAK/H,GAAK,gBAAkBnE,CAE5BkM,GAAKS,YAAc,SAAUjB,GAEzBzO,KAAKuC,UAAY,4CACjB2M,GAAUtN,MAAM+N,gBAAkBZ,EAAS/O,KAAKkH,GAAG4G,UAAU,gBAAgB/G,SAEjFkI,GAAKW,WAAa,SAAUnB,GAAGzO,KAAKuC,UAAY,qBAChD0M,GAAKY,QAAU,SAAUpB,GAErB,GAAIpF,GAAIrJ,KAAKkH,GAAG4G,UAAU,gBAAgB/G,OAC1C5B,GAAM2K,OAAOf,EAAS1F,KAI9B8F,EAAIS,WAAa,SAAUnB,GAAGS,EAAUtN,MAAM+N,gBAAkB,IAAMxK,EAAM8K,KAAK1E,MAAMhC,MACvFvJ,MAAK4O,SAAS/L,YAAYsM,EAC1BnP,MAAKkQ,SAAW,KAGpBxO,QAAO,eAAiBP,GAAQ,GAAIO,QAAOyM,eACvCjH,GAAI,SAAW/F,EACfoK,MAAO7K,GAAG,SAAWS,GACrBA,KAAMnB,KAAKC,KAAKkQ,aAChBC,SAAU1P,GAAG2P,SAAS,SAASjD,GAC3B,IAAKA,EACDA,EAAQ,aAERA,GAAQA,EAAMU,UAAU,EAE5BpN,IAAG,SAAWS,GAAMoI,MAAQ6D,GAC7BpN,OAEP0B,QAAO,eAAiBP,GAAMmP,KAAK1O,MAAM2O,mBAAqB,cAC9D,KAAK7P,GAAGmJ,UAAUnJ,GAAG,SAAWS,GAAM0L,YAAa/C,QAAS,MAAOvH,UAAW,yBAA0B,OACxG,CACI7B,GAAG,SAAWS,GAAM0L,WAAWhK,YAAYnB,OAAO,eAAiBP,GAAMqP,SAGjFC,mBAAoB,WAEhB,GAAItL,GAAQnF,IACZA,MAAK0Q,SAAWhQ,GAAGiQ,SAAStO,QACxBuO,kBAAmB,gCACnBC,yBAA0B,mBAC1BC,UACIC,SAAUrQ,GAAG,mCACbsQ,SAAU,UACVC,QAAS,IAEbC,QAAS,SAASC,EAAUC,EAAaC,GACrClM,EAAM/B,WACN+B,GAAMmM,aAAaF,OAI/BjO,iBAAkB,SAASoO,GAEvB,IAAKvR,KAAK0Q,SAAU,MACpB1Q,MAAK0Q,SAASc,gBAAgBD,EAC9BvR,MAAK0Q,SAASe,cAAcF,KAEhCG,oBAAqB,SAASH,GAE1B,IAAKvR,KAAK0Q,SAAU,MACpB1Q,MAAK0Q,SAASiB,mBAAmBJ,IAErCnO,UAAW,WAEP,GAAIwO,GAAW5R,KAAKe,iBAAiByB,SACjCqP,KAAsBC,EAAiBC,EAAiB7K,EAAI/F,EAAM6Q,EAAO,EAAGC,CAChF,KAAK,GAAIlP,KAAK6O,GACd,CACI,IAAKA,EAAS7O,KAAO6O,EAAS7O,GAAGmP,iBAC7B,QAEJJ,GAAkBF,EAAS7O,GAAGoP,cAAc,WAC5CJ,GAAkBH,EAAS7O,GAAGoP,cAAc,yBAC5C,IAAIL,GAAmBC,EACvB,CACI7K,EAAK4K,EAAgB5K,GAAG4G,UAAUgE,EAAgB5K,GAAGkL,QAAQ,sBAAsB,GACnFjR,GAAO4Q,EAAgBxI,OAASvJ,KAAKC,KAAKgH,IAC1C4K,GAAgB3K,GAAM8K,CACtBC,GAAUjS,KAAKQ,OAASR,KAAKC,KAAKoS,MAAQ,KAAOL,EAAO,iCAAmC7Q,EAAO,UAAY,+BAAiCA,EAAO,SACtJ2Q,GAAgBlP,WAAWA,WAAWM,UAAY+O,CAClDD,KAEJhS,KAAKM,UAAUN,KAAKsH,WAAauK,IAGzCP,aAAc,SAASC,GAEnB,GAAIe,GAAY5R,GAAGmJ,UAAU0H,GAAOhP,UAAa,YAAa,KAC9D,KAAK+P,EACD,MACJ,IAAIpL,GAAI/F,CACR,IAAIoR,EACJ,KAAI,GAAIxP,KAAKuP,GAAUE,WACvB,CACI,IAAKF,EAAUE,WAAWzP,GAAI,QAC9BwP,GAAOD,EAAUE,WAAWzP,EAE5B,IAAGwP,EAAKE,UAAY,KAChBvL,EAAKqL,EAAKG,cACT,IAAGH,EAAKE,UAAY,OACrBtR,EAAOoR,EAAKG,UAGpB,GAAIC,EACJ,IAAGjR,OAAOkR,aACND,EAAoBjR,OAAOkR,aAAaC,IAAI1R,EAEhD,KAAIwR,EACA,MAEJ5E,YACI,WACI4E,EAAkBG,kBACnB,MAGXzP,uBAAwB,SAASkO,GAE7B,IAAKA,IAASA,EAAKY,cACf,MAEJ,IAAIY,GAAmBxB,EAAKY,cAAc,0BAC1C,IAAIa,GAAiBzB,EAAKY,cAAc,oBACxC,IAAIY,GAAoBC,EACxB,CACItS,GAAGuS,KAAKF,EAAkB,QAASrS,GAAG2P,SAAS,WAC3CrQ,KAAKkT,WAAWF,EAAgBD,EAAkB,KAAM,OACzD/S,OAGP,GAAImT,GAAmB5B,EAAKY,cAAc,4BAC1C,IAAIgB,EACJ,CACIzS,GAAGuS,KAAKE,EAAkB,QAASzS,GAAG2P,SAAS,WAC3CrQ,KAAKoT,WAAW7B,IACjBvR,OAGP,GAAIqT,GAAyB9B,EAAKY,cAAc,kBAAkBtF,UAClE,IAAGwG,EACH,CACI3S,GAAGuS,KAAKI,EAAwB,QAAS3S,GAAG2P,SAAS,WACjDrQ,KAAKsT,iBAAiB/B,EAAM8B,IAC7BrT,MACHqT,GAAuBzR,MAAM0G,OAAS,YAG9C4K,WAAY,SAAUpE,EAAMyE,EAAQhC,EAAMiC,GAEtC,IAAK1E,GAAQyC,EACb,CACIzC,EAAOyC,EAAKY,cAAc,qBAE9B,IAAKoB,GAAUhC,EACf,CACIgC,EAAShC,EAAKY,cAAc,2BAGhC,GAAIrD,GAAQyE,EACZ,CACI,GAAIC,IAAW,KACf,CACI,GAAI1E,EAAKlN,MAAMC,SAAW,OACtB2R,EAAS,SAETA,GAAS,MAGjB9S,GAAGqM,YAAYwG,EAAQ,yBACvB,IAAGC,EACH,CACI,GAAI7P,GAAS,GAAIjD,IAAGiD,QAChBC,SAAW,IACXC,OAAUE,QAAU,GACpBC,QAAWD,QAAU,KACrBE,WAAavD,GAAGiD,OAAOO,YAAYC,MACnCC,KAAO,SAASC,GACZyK,EAAKlN,MAAMmC,QAAUM,EAAMN,QAAU,GACrC+K,GAAKlN,MAAMC,QAAU,KAG7B8B,GAAOY,SACPgP,GAAOrQ,UAAYlD,KAAKC,KAAKyI,IAC7BhI,IAAG+L,SAAS8G,EAAQ,8BAGxB,CACIzE,EAAKlN,MAAMC,QAAU,MACrB0R,GAAOrQ,UAAYlD,KAAKC,KAAKwT,QAKzCL,WAAY,SAAUM,GAElB,GAAI/P,GAAS,GAAIjD,IAAGiD,QAChBC,SAAW,IACXC,OAASE,QAAS,KAClBC,QAAUD,QAAU,GACpBE,WAAavD,GAAGiD,OAAOO,YAAYC,MACnCC,KAAO,SAASC,GACZqP,EAAc9R,MAAMmC,QAAUM,EAAMN,QAAQ,KAEhDO,SAAW5D,GAAG2P,SAAS,WACnBrQ,KAAK0R,oBAAoBgC,EACzBhT,IAAGiT,OAAOD,EACV1T,MAAKoD,aACNpD,OAEP2D,GAAOY,WAEX+O,iBAAkB,SAAS/B,EAAMgC,GAE7B,GAAIK,GAAclT,GAAGmT,mBAAmBxR,OACpC,qCACAkR,GAEIO,SAAY,MACZC,UAAa,KACbC,QAAWtT,GAAG,qBACd6B,UAAa,eACb0R,SAAY,KACZ5F,OAAU3N,GAAG4N,cAAgB5N,GAAG4N,cAAcC,YAAc,GAAK,GAGzEqF,GAAYxM,OACZwM,GAAYM,eAAeX,EAE3B,IAAIY,GAAkBzT,GAAG,+BACzB,IAAI0T,GAAgB1T,GAAG,6BAEvBkT,GAAYxM,OAEZ1G,IAAG2T,UAAUF,EACbzT,IAAGuS,KAAKkB,EAAiB,QAAS,WAAWP,EAAYxM,SAEzD1G,IAAG2T,UAAUD,EACb1T,IAAGuS,KAAKmB,EAAe,QAAS1T,GAAG2P,SAAS,WACxCrQ,KAAKsU,YAAY/C,EACjBvR,MAAKuU,YAAYhD,EACjBqC,GAAYxM,SACbpH,MAEHA,MAAKwU,gBAAgBjD,EACrBqC,GAAY1S,QAEhBsT,gBAAiB,SAASjD,GAEtB,IAAIA,IAASA,EAAKY,cACd,MAEJ,IAAIsC,GAAkB/T,GAAG,0BACzB,IAAIS,GAAOoQ,EAAKY,cAAc,yBAC9B,MAAMhR,EACFsT,EAAgBlL,MAAQ7I,GAAGgU,KAAKC,KAAKxT,EAAKoI,QAElD+K,YAAa,SAAS/C,GAElB,IAAIA,IAASA,EAAKY,cACd,MAEJ,IAAIsC,GAAkB/T,GAAG,0BACzB,IAAIS,GAAOoQ,EAAKY,cAAc,yBAE9BhR,GAAKoI,MAAQ7I,GAAGgU,KAAKC,KAAKF,EAAgBlL,QAE9CgL,YAAa,SAAShD,GAElB,IAAIA,IAASA,EAAKY,cACd,MAEJ,IAAIyC,GAAYrD,EAAKY,cAAc,yBACnC,IAAI0C,GAAWtD,EAAKY,cAAc,iBAElC0C,GAAS3R,UAAYxC,GAAGgU,KAAKC,KAAKC,EAAUrL"}