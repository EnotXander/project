<?
$MESS["AD_BACK_TO_BANNER_LIST"] = "Liste";
$MESS["AD_BACK_TO_BANNER_LIST_TITLE"] = "Banner";
$MESS["AD_EDIT_RECORD"] = "Banner Nr. #ID#";
$MESS["AD_NEW_RECORD"] = "Neues Banner";
$MESS["AD_CREATED"] = "Erstellt:";
$MESS["AD_MODIFIED"] = "Ge�ndert:";
$MESS["AD_ACTIVE"] = "Aktiv:";
$MESS["AD_NAME"] = "�berschrift:";
$MESS["AD_GROUP"] = "Bannergruppe:";
$MESS["AD_TYPE"] = "Bannertyp:";
$MESS["AD_WHEN"] = "Wann und wie oft soll der Banner angezeigt werden";
$MESS["AD_SHOW_INTERVAL"] = "Zeitraum der Anzeige";
$MESS["AD_WEIGHT"] = "Gewicht (Priorit�t):";
$MESS["AD_SHOW_COUNT"] = "Maximale Zahl der Banneranzeigen:";
$MESS["AD_SHOW_COUNT_2"] = "<b>Wie oft wurde der Banner angezeigt</b><br> / Maximale Anzeigeanzahl:";
$MESS["AD_SHOWN"] = "angezeigt:";
$MESS["AD_RESET_COUNTER"] = "Zur�cksetzen:";
$MESS["AD_CLICK_COUNT"] = "Maximale Zahl der Bannerklicks:";
$MESS["AD_CLICK_COUNT_2"] = "<b>Wie oft wurde auf den Banner geklickt</b><br> / Maximale Klickanzahl:";
$MESS["AD_CLICKED"] = "geklickt:";
$MESS["AD_WHERE"] = "Wo und f�r wen sollen Banner angezeigt werden";
$MESS["AD_ALL"] = "(f�r alle)";
$MESS["AD_SHOW_PAGES"] = "Verzeichnisse und Seiten, auf denen der Banner <b>auf jeden Fall angezeigt wird</b>:";
$MESS["AD_NOT_SHOW_PAGES"] = "Verzeichnisse und Seiten, auf denen der Banner <b>nicht angezeigt wird</b>:";
$MESS["AD_PAGES_ALT1"] = "Benutzen Sie eine neue Zeile, um mehrere Seiten voneinander zu trennen.<br>Wurde das Feld leer gelassen, wird das Objekt �berall angezeigt.<br>Benutzen Sie das Zeichen '%' als einen Platzhalter.";
$MESS["AD_COUNTRY"] = "Nur Besuchern aus folgenden L�ndern zeigen:";
$MESS["AD_SELECT_WHAT_YOU_NEED"] = "(gew�nschte w�hlen)";
$MESS["AD_STAT_ADV"] = "Nur Besuchern zeigen, die �ber die folgenden Werbekampangen gekommen sind:";
$MESS["AD_WHAT"] = "Media";
$MESS["AD_VISITORS_TYPE"] = "Besuchern zeigen:";
$MESS["AD_NEW_VISITORS_ONLY"] = "nur f�r neue Besucher";
$MESS["AD_RETURNED_VISITORS_ONLY"] = "nur f�r wiederkehrende Besucher";
$MESS["AD_ALL_VISITORS"] = "an alle";
$MESS["AD_IMAGE"] = "Bild:";
$MESS["AD_IMAGE_ALT"] = "Tooltiptext:";
$MESS["AD_URL"] = "URL zum Bild:";
$MESS["AD_URL_TARGET"] = "URL Ziel (target Attribut):";
$MESS["AD_OR"] = "Zus�tzlichen HTML-Code des Banners anzeigen/verbergen";
$MESS["AD_HTML_ALT"] = "Im \"Bannercode\" k�nnen eins bis f�nf zuf�llige Zahlen mit folgenden Namen verwendet werden:<br />";
$MESS["AD_STAT"] = "Wie sollen die Bannerklicks im Modul \"Statistik\" registriert werden";
$MESS["AD_EVENT12"] = "event1, event2 - Parameter f�r den Eventtyp";
$MESS["AD_EVENT3"] = "event3 - weiterer Parameter.<br><br>F�r event1, event2, event3 und den \"Bannercode\" k�nnen Sie folgende Felder verwenden:<br>";
$MESS["AD_BANNER_NAME"] = "Bannername";
$MESS["AD_BANNER_ID"] = "Banner ID";
$MESS["AD_CONTRACT_ID"] = "Vertrags ID";
$MESS["AD_TYPE_SID"] = "Bannertyp ID";
$MESS["AD_COMMENTS"] = "Kommentar";
$MESS["AD_WEEKDAY"] = "Zeit und Wochentage f�r die Anzeige:";
$MESS["AD_SUNDAY"] = "So";
$MESS["AD_MONDAY"] = "Mo";
$MESS["AD_TUESDAY"] = "Di";
$MESS["AD_WEDNESDAY"] = "Mi";
$MESS["AD_THURSDAY"] = "Do";
$MESS["AD_FRIDAY"] = "Fr";
$MESS["AD_SATURDAY"] = "Sa";
$MESS["AD_CONTRACT"] = "Vertrag:";
$MESS["AD_CONTRACT_SETTINGS"] = "Vertragsparameter anzeigen";
$MESS["AD_TEXT"] = "Text";
$MESS["AD_ALL_1"] = "(f�r alle)";
$MESS["AD_STATUS"] = "Status:";
$MESS["AD_STATUS_COMMENTS"] = "Kommentar zum Status";
$MESS["AD_CONFIRMED_FIELDS"] = "�nderungen in den aktuellen Feldern m�ssen vom Administrator best�tigt werden (abh�ngig von den Vertragseinstellungen).";
$MESS["AD_BANNER_VIEW_SETTINGS"] = "Banner anzeigen";
$MESS["AD_BANNER_EDIT"] = "Banner bearbeiten";
$MESS["AD_BANNER_STATUS"] = "Bannerstatus";
$MESS["AD_RED_ALT"] = "Der Banner wird auf der Seite nicht angezeigt";
$MESS["AD_GREEN_ALT"] = "Das Banner wurde best�tigt und wird auf der Seite angezeigt";
$MESS["AD_SEND_EMAIL"] = "�ber Status�nderung den Vertragsinhaber benachrichtigen:";
$MESS["AD_BANNER_STATISTICS"] = "CTR-Grafik f�r das Banner";
$MESS["AD_BANNER_STATISTICS_TITLE"] = "Banner CTR-Grafik anzeigen";
$MESS["AD_BANNER_VIEW_SETTINGS_TITLE"] = "Bannerparameter anzeigen";
$MESS["AD_BANNER_EDIT_TITLE"] = "Klicken Sie hier, um die Bannerparameter zu �ndern";
$MESS["AD_ADD_NEW_BANNER"] = "Neues Banner";
$MESS["AD_ADD_NEW_BANNER_TITLE"] = "Neues Banner";
$MESS["AD_DELETE_BANNER"] = "Banner l�schen";
$MESS["AD_DELETE_BANNER_TITLE"] = "Aktuellen Banner l�schen";
$MESS["AD_DELETE_BANNER_CONFIRM"] = "Wollen Sie den Banner wirklich l�schen?";
$MESS["AD_DELETE_BANNER_ERROR"] = "Beim L�schen des Banners ist ein Fehler aufgetreten";
$MESS["AD_CTR"] = "Click-Through-Ratio (CTR) (%):";
$MESS["AD_CAN_USE_EVENT_GID"] = "In den URL-Parametern k�nnen Sie die Vorlage #EVENT_GID# verwenden, die sp�ter durch die Besucher ID ersetzt wird (f�r das Modul \"Statistik\") ";
$MESS["AD_VISITOR_COUNT_2"] = "<b>Wieviele Besucher haben den Banner gesehen</b><br> / Maximale Besucheranzahl:";
$MESS["AD_VISITOR_COUNT"] = "Maximale Zahl der Besucher, denen der Banner angezeigt wird:";
$MESS["AD_VISITORS"] = "Besucher:";
$MESS["AD_SHOWS_FOR_VISITOR"] = "Maximale Zahl der Anzeigen des aktuellen Banners f�r einen Besucher:";
$MESS["AD_FIX_CLICK"] = "Klicks f�r den aktuellen Banner z�hlen:";
$MESS["AD_FIX_SHOW"] = "Anzahl der Banneranzeigen z�hlen:";
$MESS["AD_UNIFORM"] = "Banner gleichm��ig verteilen:";
$MESS["AD_KEYWORDS"] = "Schl�sselw�rter:";
$MESS["AD_KEYWORDS_ALT"] = "Schl�sselw�rter m�ssen durch eine neue Zeile getrennt werden.";
$MESS["AD_SITE"] = "Seiten:";
$MESS["AD_BANNER_COPY"] = "Banner kopieren";
$MESS["AD_BANNER_COPY_TITLE"] = "Bannerkopie erstellen";
$MESS["AD_USER_GROUPS"] = "Usergruppen:";
$MESS["AD_USER_GROUP_Y"] = "Banner nur f�r die gew�hlten Gruppen anzeigen";
$MESS["AD_USER_GROUP_N"] = "Banner f�r die gew�hlten Gruppen nicht anzeigen";
$MESS["AD_TAB_CONTRACT"] = "Banner";
$MESS["AD_TAB_TITLE_CONTRACT"] = "Banner-Parameter";
$MESS["AD_TAB_LIMIT"] = "Einschr�nkungen";
$MESS["AD_TAB_ACCESS"] = "Zugriff";
$MESS["AD_TAB_COMMENT"] = "Kommentar";
$MESS["AD_USER_ALT"] = "Userparameter anzeigen";
$MESS["AD_SITE_ALT"] = "Seitenparameter anzeigen";
$MESS["AD_FIX_STAT"] = "In der Statistik fixieren:";
$MESS["AD_TAB_BANNER"] = "Banner";
$MESS["AD_TAB_TITLE_BANNER"] = "Banner-Parameter";
$MESS["AD_TAB_TARGET"] = "Targeting";
$MESS["AD_TAB_STAT"] = "Statistik";
$MESS["AD_ERROR_INCORRECT_ID"] = "Der Banner wurde nicht gefunden";
$MESS["ADV_NO_LIMITS"] = "(ohne Einschr�nkung)";
$MESS["ADV_NOT_SET"] = "(nicht angegeben)";
$MESS["ADV_Y"] = "Ja";
$MESS["ADV_N"] = "nein";
$MESS["ADV_TYPE_VIEW"] = "Bannertyp-Parameter anzeigen";
$MESS["ADV_VIEW_UGROUP"] = "Usergruppen-Parameter anzeigen";
$MESS["ADV_VIEW_PAGE"] = "Seite anzeigen";
$MESS["AD_INS_TEMPL"] = "Template benutzen";
$MESS["ADV_BANNER_NO_LINK"] = "Flash-Banner Link festlegen (wenn Flash-Banner keinen Link enth�lt)";
$MESS["ADV_BANNER_IMAGE"] = "Bild";
$MESS["ADV_BANNER_FLASH"] = "Flash";
$MESS["ADV_BANNER_HTML"] = "HTML-Code";
$MESS["ADV_BANNER_FILE"] = "Datei:";
$MESS["AD_FLASH_TRANSPARENT"] = "Transparenz";
$MESS["AD_FLASH_JS"] = "Flash mit Hilfe von JavaScript einf�gen";
$MESS["AD_FLASH_JS_DESCRIPTION"] = "[Alternatives Bild, falls Flash nicht erlaubt oder nicht installiert ist]";
$MESS["ADV_FLASH_IMAGE"] = "Alternatives Bild:";
$MESS["ADV_FLASH_VERSION"] = "Ben�tigte Flashversion:";
$MESS["ADV_STAT_WHAT_QUESTION"] = "Geotargeting-Parameter";
$MESS["ADV_STAT_WHAT_COUNTRY"] = "L�nder";
$MESS["ADV_STAT_WHAT_REGION"] = "Regionen";
$MESS["ADV_STAT_WHAT_CITY"] = "Orte";
$MESS["ADV_STAT_WHAT_ADD"] = "Hinzuf�gen...";
$MESS["ADV_STAT_WHAT_DELETE"] = "Gew�hlte l�schen";
$MESS["AD_EDIT_NOT_FIX"] = "Sie haben die Option &quot;Anzahl der Banneranzeigen nicht z�hlen f�r alle Banner&quot; im Werbemodul gew�hlt. Banneranzeigen werden nicht gez�hlt (unabh�ngig der Bannereinstellungen).";
$MESS["AD_NOTE_2"] = "Der Wert &quot;Banner gleichm��ig verteilen&quot; entspricht nicht den Einstellungen des Wertes &quot;Zeit und Wochentage f�r die Anzeige&quot;";
$MESS["AD_ACTIONS"] = "Aktionen";
$MESS["AD_SELF_WINDOW"] = "_self (im aktuellen Fenster)";
$MESS["AD_BLANK_WINDOW"] = "_blank (im neuen Fenster)";
$MESS["AD_PARENT_WINDOW"] = "_parent (in seinem Frameset)";
$MESS["AD_TOP_WINDOW"] = "_top (im gesamten Browserfenster)";
$MESS["AD_RENAME_DIALOG"] = "Banner umbenennen";
$MESS["AD_APPLY"] = "Anwenden";
$MESS["AD_CANCEL"] = "Abbrechen";
$MESS["AD_EXTENDED_MODE"] = "Erweiterter Modus";
$MESS["AD_PREVIEW"] = "Vorschau";
$MESS["AD_ADD_SLIDE"] = "Dia hinzuf�gen";
$MESS["AD_JQUERY_WARNING"] = "Nivo Slider erfordert jQuery.";
$MESS["AD_HIDE"] = "Minimieren";
$MESS["AD_SHOW"] = "Maximieren";
$MESS["AD_SLIDE_NAME"] = "Name";
$MESS["AD_SLIDE"] = "Dia";
$MESS["AD_SELECT_COLOR"] = "Farbe ausw�hlen";
?>