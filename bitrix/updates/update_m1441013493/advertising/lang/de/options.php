<?
$MESS["AD_USE_HTML_EDIT"] = "HTML-Editor f�r die Bannereinstellungen benutzen:";
$MESS["AD_DONT_USE_CONTRACT"] = "Vertragsbegrenzung bei der Banneranzeige nicht benutzen:";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Standard wiederherstellen";
$MESS["AD_BANNER_DAYS"] = "Wieviele Tage soll Bannerdynamik aufbewahrt werden?";
$MESS["AD_CLEAR"] = "Bereinigen";
$MESS["AD_RECORDS"] = "Eintr�ge:";
$MESS["AD_BANNER_GRAPH_WEIGHT"] = "Grafikbreite:";
$MESS["AD_BANNER_GRAPH_HEIGHT"] = "Grafikh�he:";
$MESS["AD_BANNER_DIAGRAM_DIAMETER"] = "Durchmesser des Kreisdiagramms:";
$MESS["AD_COOKIE_DAYS"] = "Anzahl Tage, wie lange Informationen �ber Banneranzeige archiviert werden:";
$MESS["AD_REDIRECT_FILENAME"] = "Vollst�ndiger Dateiname f�r die Banner-Klickerfassung:";
$MESS["AD_UPLOAD_SUBDIR"] = "Unterverzeichnis f�r die Archivierung der hochzuladenden Banner:";
$MESS["AD_OPT_DONT_FIX_BANNER_SHOWS"] = "Anzahl der Banneranzeigen nicht z�hlen f�r alle Banner:";
$MESS["AD_SHOW_COMPONENT_PREVIEW"] = "Banner-Vorschau, wenn eine Vorlage gew�hlt wurde";
?>