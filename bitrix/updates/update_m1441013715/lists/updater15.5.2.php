<?
if(IsModuleInstalled("lists"))
{
	$updater->CopyFiles("install/components", "components");

	if($updater->CanUpdateDatabase() && CModule::IncludeModule('lists'))
	{

		if(CModule::IncludeModule('bizproc'))
		{
			$resQuery = $updater->query("
				SELECT bws.DOCUMENT_ID
				FROM b_bp_workflow_state bws
				LEFT JOIN b_iblock_element be ON be.ID = bws.DOCUMENT_ID
				WHERE bws.MODULE_ID = 'lists' AND bws.ENTITY = 'BizprocDocument' AND be.ID IS NULL
			");
			$idElementList = array();
			while($documentData = $resQuery->fetch())
				$idElementList[$documentData["DOCUMENT_ID"]] = $documentData["DOCUMENT_ID"];

			if(!empty($idElementList))
			{
				$errors = array();
				$socialnetwork = CModule::includeModule('socialnetwork') ? true : false;
				foreach($idElementList as $idElement)
				{
					if($socialnetwork)
					{
						$states = CBPStateService::getDocumentStates(array('lists', 'BizprocDocument', $idElement));
						foreach ($states as $workflowId => $state)
						{
							$sourceId = CBPStateService::getWorkflowIntegerId($workflowId);
							$resultQuery = CSocNetLog::getList(
								array(),
								array('EVENT_ID' => 'lists_new_element', 'SOURCE_ID' => $sourceId),
								false,
								false,
								array('ID')
							);
							while ($log = $resultQuery->fetch())
								CSocNetLog::delete($log['ID']);
						}
					}

					CBPDocument::onDocumentDelete(array('lists', 'BizprocDocument', $idElement), $errors);
				}
			}
		}

		if(IsModuleInstalled("bitrix24"))
		{
			$currentPermissions = CLists::GetPermission();
			unset($currentPermissions["bitrix_process"]);
			unset($currentPermissions["lists"]);

			$socnet_iblock_type_id = COption::GetOptionString("lists", "socnet_iblock_type_id");
			$isSocnetInstalled = IsModuleInstalled('socialnetwork');

			$arTypes = array();
			if (empty($currentPermissions))
			{
				$arTypes[] = array(
					"ID" => "lists",
					"SECTIONS" => "Y",
					"IN_RSS" => "N",
					"SORT" => 80,
					"LANG" => array(),
				);
				$arTypes[] = array(
					"ID" => "bitrix_processes",
					"SECTIONS" => "Y",
					"IN_RSS" => "N",
					"SORT" => 90,
					"LANG" => array(),
				);
			}

			if ($isSocnetInstalled && strlen($socnet_iblock_type_id) <= 0)
			{
				$arTypes[] = array(
					"ID" => "lists_socnet",
					"SECTIONS" => "Y",
					"IN_RSS" => "N",
					"SORT" => 83,
					"LANG" => array(),
				);
			}

			$arLanguages = array();
			if (!empty($arTypes))
			{
				$rsLanguage = CLanguage::GetList($by, $order, array());
				while ($arLanguage = $rsLanguage->Fetch())
				{
					$arLanguages[] = $arLanguage["LID"];
				}
			}

			foreach ($arTypes as $arType)
			{
				$dbType = CIBlockType::GetList(array(), array("=ID" => $arType["ID"]));
				if (!$dbType->Fetch())
				{
					foreach($arLanguages as $languageID)
					{
						IncludeModuleLangFile(__FILE__, $languageID);
						$code = strtoupper($arType["ID"]);
						$arType["LANG"][$languageID]["NAME"] = GetMessage($code."_TYPE_NAME");
						$arType["LANG"][$languageID]["ELEMENT_NAME"] = GetMessage($code."_ELEMENT_NAME");
						if ($arType["SECTIONS"] == "Y")
							$arType["LANG"][$languageID]["SECTION_NAME"] = GetMessage($code."_SECTION_NAME");
					}
					$iblockType = new CIBlockType;
					$iblockType->Add($arType);
				}
			}

			if (empty($currentPermissions))
			{
				CLists::SetPermission('lists', array(1));
				CLists::SetPermission('bitrix_processes', array(1));
			}

			$defaultSiteId = CSite::GetDefSite();
			$siteObject = CSite::GetByID($defaultSiteId);
			$site = $siteObject->fetch();
			$defaultLang = $site? $site['LANGUAGE_ID'] : 'en';
			if($defaultLang == 'ua')
				$defaultLang = 'ru';

			\Bitrix\Lists\Importer::installProcesses($defaultLang);
			\Bitrix\Main\Config\Option::set("lists", "livefeed_url", "/company/processes/");

			if ($isSocnetInstalled && strlen($socnet_iblock_type_id) <= 0)
			{
				COption::SetOptionString("lists", "socnet_iblock_type_id", "lists_socnet");
				CLists::EnableSocnet(true);
			}
		}

	}
}
?>
