<?
if(IsModuleInstalled('seo'))
{
	$updater->CopyFiles("install/admin", "admin");
}

if($updater->CanUpdateDatabase() && IsModuleInstalled("seo"))
{
	$eventManager = \Bitrix\Main\EventManager::getInstance();
	$eventManager->registerEventHandler("catalog", "OnProductUpdate", "seo", "\\Bitrix\\Seo\\Adv\\Auto", "checkQuantity");
	$eventManager->registerEventHandler("catalog", "OnProductSetAvailableUpdate", "seo", "\\Bitrix\\Seo\\Adv\\Auto", "checkQuantity");
	$eventManager->registerEventHandler("conversion", "OnGetAttributeTypes", "seo", "\\Bitrix\\Seo\\ConversionHandler", "onGetAttributeTypes");
	\CAgent::AddAgent("Bitrix\\Seo\\Adv\\Auto::checkQuantityAgent();","seo", "N", 3600);
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_seo_keywords'))
{
	if ($DB->type == "MYSQL")
	{
		if ($updater->TableExists("b_seo_adv_banner"))
		{
			if (!$DB->Query("SELECT AUTO_QUANTITY_OFF FROM b_seo_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_seo_adv_banner ADD AUTO_QUANTITY_OFF char(1) NULL DEFAULT 'N'");
			}
			if (!$DB->Query("SELECT AUTO_QUANTITY_ON FROM b_seo_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_seo_adv_banner ADD AUTO_QUANTITY_ON char(1) NULL DEFAULT 'N'");
			}
			if (!$DB->IndexExists("b_seo_adv_banner", array("AUTO_QUANTITY_OFF", "AUTO_QUANTITY_ON", )))
			{
				$DB->Query("CREATE INDEX ix_b_seo_adv_banner2 ON b_seo_adv_banner(AUTO_QUANTITY_OFF, AUTO_QUANTITY_ON)");
			}
		}
		if (!$updater->TableExists("b_seo_adv_autolog"))
		{
			$DB->Query("
				CREATE TABLE b_seo_adv_autolog(
					ID int(11) NOT NULL AUTO_INCREMENT,
					ENGINE_ID int(11) NOT NULL,
					TIMESTAMP_X timestamp NOT NULL,
					CAMPAIGN_ID int(11) NOT NULL,
					CAMPAIGN_XML_ID varchar(255) NOT NULL,
					BANNER_ID int(11) NOT NULL,
					BANNER_XML_ID varchar(255) NOT NULL,
					CAUSE_CODE int NULL DEFAULT 0,
					PRIMARY KEY pk_b_seo_adv_autolog (ID)
				)
			");
		}
		if ($updater->TableExists("b_seo_adv_autolog"))
		{
			if (!$DB->IndexExists("b_seo_adv_autolog", array("ENGINE_ID", )))
			{
				$DB->Query("CREATE INDEX ix_b_seo_adv_autolog1 ON b_seo_adv_autolog(ENGINE_ID)");
			}
			if (!$DB->IndexExists("b_seo_adv_autolog", array("TIMESTAMP_X", )))
			{
				$DB->Query("CREATE INDEX ix_b_seo_adv_autolog2 ON b_seo_adv_autolog(TIMESTAMP_X)");
			}
			if (!$DB->Query("SELECT SUCCESS FROM b_seo_adv_autolog WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_seo_adv_autolog ADD SUCCESS char(1) NULL DEFAULT 'Y'");
			}
		}
	}
	if ($DB->type == "MSSQL")
	{
		if ($updater->TableExists("B_SEO_ADV_BANNER"))
		{
			if (!$DB->Query("SELECT AUTO_QUANTITY_OFF FROM B_SEO_ADV_BANNER WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SEO_ADV_BANNER ADD AUTO_QUANTITY_OFF CHAR(1) NULL");
			}
			if (!$DB->Query("SELECT AUTO_QUANTITY_ON FROM B_SEO_ADV_BANNER WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SEO_ADV_BANNER ADD AUTO_QUANTITY_ON CHAR(1) NULL");
			}
			if (!$DB->IndexExists("B_SEO_ADV_BANNER", array("AUTO_QUANTITY_OFF", "AUTO_QUANTITY_ON", )))
			{
				$DB->Query("CREATE INDEX IX_B_SEO_ADV_BANNER2 ON B_SEO_ADV_BANNER(AUTO_QUANTITY_OFF, AUTO_QUANTITY_ON)");
			}
			$DB->Query("ALTER TABLE B_SEO_ADV_BANNER ADD CONSTRAINT DF_B_SEO_ADV_BANNER_AUTO_QUANTITY_OFF DEFAULT 'N' FOR AUTO_QUANTITY_OFF", true);
			$DB->Query("ALTER TABLE B_SEO_ADV_BANNER ADD CONSTRAINT DF_B_SEO_ADV_BANNER_AUTO_QUANTITY_ON DEFAULT 'N' FOR AUTO_QUANTITY_ON", true);
		}
		if (!$updater->TableExists("B_SEO_ADV_AUTOLOG"))
		{
			$DB->Query("
				CREATE TABLE B_SEO_ADV_AUTOLOG(
					ID INT NOT NULL IDENTITY (1, 1),
					ENGINE_ID INT NOT NULL,
					TIMESTAMP_X DATETIME NOT NULL,
					CAMPAIGN_ID INT NOT NULL,
					CAMPAIGN_XML_ID VARCHAR(255) NOT NULL,
					BANNER_ID INT NOT NULL,
					BANNER_XML_ID VARCHAR(255) NOT NULL,
					CAUSE_CODE INT NULL
				)
			");
			$DB->Query("
				ALTER TABLE B_SEO_ADV_AUTOLOG ADD CONSTRAINT PK_B_SEO_ADV_AUTOLOG PRIMARY KEY (ID)
			");
		}
		if ($updater->TableExists("B_SEO_ADV_AUTOLOG"))
		{
			if (!$DB->IndexExists("B_SEO_ADV_AUTOLOG", array("ENGINE_ID", )))
			{
				$DB->Query("CREATE INDEX IX_B_SEO_ADV_AUTOLOG1 ON B_SEO_ADV_AUTOLOG(ENGINE_ID)");
			}
			if (!$DB->IndexExists("B_SEO_ADV_AUTOLOG", array("TIMESTAMP_X", )))
			{
				$DB->Query("CREATE INDEX IX_B_SEO_ADV_AUTOLOG2 ON B_SEO_ADV_AUTOLOG(TIMESTAMP_X)");
			}
			if (!$DB->Query("SELECT SUCCESS FROM B_SEO_ADV_AUTOLOG WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SEO_ADV_AUTOLOG ADD SUCCESS CHAR(1) NULL");
				$DB->Query("ALTER TABLE B_SEO_ADV_AUTOLOG ADD CONSTRAINT DF_B_SEO_ADV_AUTOLOG_SUCCESS DEFAULT 'Y' FOR SUCCESS", true);
			}
		}
	}
	if ($DB->type == "ORACLE")
	{
		$DB->Query("CREATE SEQUENCE sq_b_seo_adv_autolog", true);
		if ($updater->TableExists("b_seo_adv_banner"))
		{
			if (!$DB->Query("SELECT AUTO_QUANTITY_OFF FROM b_seo_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_seo_adv_banner ADD (AUTO_QUANTITY_OFF char(1 CHAR) DEFAULT 'N' NULL)");
			}
			if (!$DB->Query("SELECT AUTO_QUANTITY_ON FROM b_seo_adv_banner WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_seo_adv_banner ADD (AUTO_QUANTITY_ON char(1 CHAR) DEFAULT 'N' NULL)");
			}
			if (!$DB->IndexExists("b_seo_adv_banner", array("AUTO_QUANTITY_OFF", "AUTO_QUANTITY_ON", )))
			{
				$DB->Query("CREATE INDEX ix_b_seo_adv_banner2 ON b_seo_adv_banner(AUTO_QUANTITY_OFF, AUTO_QUANTITY_ON)");
			}
		}
		if ($updater->TableExists("b_seo_adv_log"))
		{
			if (!$DB->IndexExists("b_seo_adv_log", array("ENGINE_ID", )))
			{
				$DB->Query("CREATE INDEX ix_b_seo_adv_autolog1 ON b_seo_adv_log(ENGINE_ID)");
			}
			if (!$DB->IndexExists("b_seo_adv_log", array("TIMESTAMP_X", )))
			{
				$DB->Query("CREATE INDEX ix_b_seo_adv_autolog2 ON b_seo_adv_log(TIMESTAMP_X)");
			}
		}
		if (!$updater->TableExists("b_seo_adv_autolog"))
		{
			$DB->Query("
				CREATE TABLE b_seo_adv_autolog(
					ID NUMBER(18) NOT NULL,
					ENGINE_ID NUMBER(18) NOT NULL,
					TIMESTAMP_X DATE NOT NULL,
					CAMPAIGN_ID NUMBER(18) NOT NULL,
					CAMPAIGN_XML_ID VARCHAR(255 CHAR) NOT NULL,
					BANNER_ID NUMBER(18) NOT NULL,
					BANNER_XML_ID VARCHAR(255 CHAR) NOT NULL,
					CAUSE_CODE NUMBER(5) NULL,
					CONSTRAINT PK_B_SEO_ADV_AUTOLOG PRIMARY KEY (ID)
				)
			");
		}
		if ($updater->TableExists("b_seo_adv_autolog"))
		{
			if (!$DB->Query("SELECT SUCCESS FROM b_seo_adv_autolog WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_seo_adv_autolog ADD (SUCCESS CHAR(1 CHAR) DEFAULT 'Y' NULL)");
			}
			if (!$DB->IndexExists("b_seo_adv_autolog", array("ENGINE_ID", )))
			{
				$DB->Query("CREATE INDEX ix_b_seo_adv_autolog1 ON b_seo_adv_autolog(ENGINE_ID)");
			}
			if (!$DB->IndexExists("b_seo_adv_autolog", array("TIMESTAMP_X", )))
			{
				$DB->Query("CREATE INDEX ix_b_seo_adv_autolog2 ON b_seo_adv_autolog(TIMESTAMP_X)");
			}
		}
	}
}
