<?
if(IsModuleInstalled('vote'))
{
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/public/tools", "tools");
}
if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/vote/install/components/bitrix/voting.vote.edit/templates/.default/images/live_feed_sprite_2.png",
		"components/bitrix/voting.vote.edit/templates/.default/images/live_feed_sprite_2.png",
		"modules/vote/install/components/bitrix/system.field.edit",
		"modules/vote/install/components/bitrix/system.field.view"
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
?>
