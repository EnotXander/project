<?
$MESS ['IBLOCK_TYPE'] = "Tipo de block de informaci�n";
$MESS ['IBLOCK_IBLOCK'] = "Block de informaci�n";
$MESS ['IBLOCK_SECTION_URL'] = "URL de la p�gina con secci�n de contenidos";
$MESS ['IBLOCK_SECTION_CODE'] = "C�digo de la secci�n";
$MESS ['IBLOCK_SECTION_ID'] = "ID de la secci�n";
$MESS ['T_IBLOCK_DESC_NEWS_PANEL'] = "Visualizar panel de botones para este componente";
$MESS ['P_ALBUM_PHOTO_WIDTH'] = "Ancho del �lbum de foto (px)";
$MESS ['P_ALBUM_PHOTO_THUMBS_WIDTH'] = "Ancho de la miniatura (px)";
$MESS ['IBLOCK_SECTION_EDIT_URL'] = "�lbum (edici�n)";
$MESS ['IBLOCK_SECTION_EDIT_ICON_URL'] = "�lbum (edici�n de la car�tula)";
$MESS ['IBLOCK_UPLOAD_URL'] = "Descargar foto";
$MESS ['T_DATE_TIME_FORMAT'] = "Formato de fecha";
$MESS ['IBLOCK_GALLERY_URL'] = "Contenidos de la galer�a";
$MESS ['P_USER_ALIAS'] = "C�digo de la galer�a";
$MESS ['P_GALLERY_SIZE'] = "Tama�o de la galer�a";
$MESS ['P_BEHAVIOUR'] = "Modo de galer�a";
$MESS ['IBLOCK_DETAIL_SLIDE_SHOW_URL'] = "Mostrar diapositiva";
$MESS ['P_RETURN_SECTION_INFO'] = "Retornar al orden de la base del �lbum";
$MESS ['IBLOCK_INDEX_URL'] = "Lista de secciones";
$MESS ['P_SET_STATUS_404'] = "Fijar estatus 404 si no se encuentra la secci�n o el elemento";
?>