<?
$MESS["LDAP_OPTIONS_SAVE"] = "Guargar";
$MESS["LDAP_OPTIONS_RESET"] = "Resetear";
$MESS["LDAP_OPTIONS_GROUP_LIMIT"] = "Registros m�ximo de concurrentes LDAP:";
$MESS["LDAP_OPTIONS_USE_NTLM"] = "Usar autorizaci�n NTLM ";
$MESS["LDAP_OPTIONS_USE_NTLM_MSG"] = "Nota: la autorizaci�n NTLM requiere que su propia configuraci�n la relacionada al m�dulo del servidor web y especificar la autorizaci�n de dominio NTLM en el portal de las configuraciones del servidor AD. ";
$MESS["LDAP_CURRENT_USER"] = "Conexi�n de usuario actual para la autorizaci�n NTLM (dominio\\login):";
$MESS["LDAP_CURRENT_USER_ABS"] = "Indefinido";
$MESS["LDAP_OPTIONS_NTLM_VARNAME"] = "variable para contener NTLM de usuario de acceso PHP:";
$MESS["LDAP_NOT_USE_DEFAULT_NTLM_SERVER"] = "No utilice";
$MESS["LDAP_DEFAULT_NTLM_SERVER"] = "Dominio predeterminado del servidor:";
$MESS["LDAP_OPTIONS_DEFAULT_EMAIL"] = "Direcci�n de correo predeterminada del usuario (si no est� especificada):";
$MESS["LDAP_OPTIONS_NEW_USERS"] = "Crear nuevos usuarios una vez que tuvo �xito la primera conexi�n:";
$MESS["LDAP_BITRIXVM_BLOCK"] = "Redirigir la autenticaci�n NTLM a Puertos 8890 y 8891";
$MESS["LDAP_BITRIXVM_SUPPORT"] = "Redirigir la autenticaci�n NTLM";
$MESS["LDAP_BITRIXVM_NET"] = "Restringir la redirecci�n de NTLM para esta subred:";
$MESS["LDAP_BITRIXVM_HINT"] = "Especifique aqu� la subred cuyos usuarios ser�n redirigidos al autenticar mediante NTLM.<br> Por ejemplo: <b>192.168.1.0/24</b> o <b>192.168.1.0/255.255.255.0</b>.<br>Separar varios rangos de IP con un punto y coma.<br> Deje el campo vac�o para redirigir todos los usuarios.";
$MESS["LDAP_WRONG_NET_MASK"] = "Marca y direcci�n de subred de autenticaci�n NTLM son incorrectas.<br> Utilice el siguiente formato:<br> subnet/mask <br> xxx.xxx.xxx.xxx/xxx.xxx.xxx.xxx <br> xxx.xxx.xxx.xxx/xx<br>Separar varios rangos de IP con un punto y coma.";
$MESS["LDAP_WITHOUT_PREFIX"] = "Compruebe la autenticaci�n en todos los servidores LDAP si la sesi�n no incluya prefijo";
?>