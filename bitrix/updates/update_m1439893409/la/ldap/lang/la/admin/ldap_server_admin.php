<?
$MESS ['LDAP_ADMIN_TITLE'] = "Directorio activo / configuraci�n del servidor LDAP";
$MESS ['LDAP_ADMIN_DEL_ERR'] = "Error al eliminar la configuraci�n del servidor.";
$MESS ['LDAP_ADMIN_F_ACT'] = "Activo";
$MESS ['LDAP_ADMIN_F_ACT_ANY'] = "(cualquiera)";
$MESS ['LDAP_ADMIN_F_NAME'] = "Nombre";
$MESS ['LDAP_ADMIN_NAVSTRING'] = "Servidores";
$MESS ['LDAP_ADMIN_TSTAMP'] = "Modificado";
$MESS ['LDAP_ADMIN_NAME'] = "Nombre";
$MESS ['LDAP_ADMIN_ACT'] = "Act.";
$MESS ['LDAP_ADMIN_CODE'] = "C�digo Mnemonic";
$MESS ['LDAP_ADMIN_SERV'] = "Servidor";
$MESS ['SAVE_ERROR'] = "Error al actualizar registro #";
$MESS ['LDAP_ADMIN_ACTIONS'] = "Acciones";
$MESS ['LDAP_ADMIN_CHANGE'] = "Modificar configuraci�n de servidor";
$MESS ['LDAP_ADMIN_CHANGE_LINK'] = "Modificar";
$MESS ['LDAP_ADMIN_DEL_ALT'] = "Eliminar servidor";
$MESS ['LDAP_ADMIN_DEL_LINK'] = "Borrar";
$MESS ['LDAP_ADMIN_EMPTY'] = "Lista vac�a";
$MESS ['LDAP_ADMIN_TOTAL'] = "Total:";
$MESS ['LDAP_ADMIN_DEL_CONF'] = "El servidor ser� eliminado y todos los usuarios registrados en e�l, no podr�n ingresar m�s al sistema, �Desea eliminar el servidor de todas formas?";
$MESS ['LDAP_ADMIN_SYNC'] = "Sincronice";
$MESS ['LDAP_ADMIN_SYNC_PERIOD'] = "Periodo de sincronizaci�n";
$MESS ['LDAP_ADMIN_SYNC_LAST'] = "Ultima sincronizaci�n";
?>