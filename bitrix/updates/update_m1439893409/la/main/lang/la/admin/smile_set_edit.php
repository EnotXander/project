<?
$MESS["SMILE_EDIT_RECORD"] = "Editar conjunto";
$MESS["SMILE_NEW_RECORD"] = "Agregar conjunto";
$MESS["ERROR_EDIT_SMILE"] = "Error al editar el conjunto";
$MESS["ERROR_ADD_SMILE"] = "Error al editar el conjunto";
$MESS["ERROR_BAD_SESSID"] = "Su sesi�n ha expirado. Por favor, int�ntelo de nuevo.";
$MESS["SMILE_SORT"] = "Clasificar";
$MESS["SMILE_STRING_ID"] = "Fijar ID";
$MESS["SMILE_SMILE_EXAMPLE"] = "Fijar emoticonos";
$MESS["SMILE_SMILE_EXAMPLE_LINK"] = "Ver emoticonos";
$MESS["SMILE_IMAGE_NAME"] = "Nombre de conjunto";
$MESS["SMILE_IMAGE_NAME_EN"] = "Ingl�s";
$MESS["SMILE_IMAGE_NAME_DE"] = "Deutsch";
$MESS["SMILE_IMAGE_NAME_RU"] = "Ruso";
$MESS["SMILE_BTN_BACK"] = "Fijar emoticonos";
$MESS["SMILE_BTN_NEW"] = "Agregar nuevos";
$MESS["SMILE_BTN_DELETE"] = "Eliminar";
$MESS["SMILE_BTN_DELETE_CONFIRM"] = "Est� seguro que desea eliminar este grupo? Esto tambi�n eliminar� todos los emoticonos que contiene el conjunto.";
$MESS["SMILE_TAB_SMILE"] = "Par�metros";
$MESS["SMILE_TAB_SMILE_DESCR"] = "Fijar par�metros";
$MESS["SMILE_IMPORT"] = "Importar los emoticonos a este conjunto una vez que se ha creado";
?>