<?
$MESS["MAIN_AI_NEW_NOTIF"] = "Nuevas notificaciones";
$MESS["MAIN_AI_ALL_NOTIF"] = "Mostrar todo";
$MESS["top_panel_ai_updates"] = "Actualizaciones";
$MESS["top_panel_ai_upd_stp"] = "Configurar actualizaciones";
$MESS["top_panel_ai_upd_chk"] = "Buscar actualizaciones";
$MESS["top_panel_ai_upd_instl"] = "Instalar actualizaciones";
$MESS["top_panel_ai_title_err"] = "error";
$MESS["top_panel_ai_upd_installed"] = "Se han instalado actualizaciones.";
$MESS["top_panel_ai_upd_aviable"] = "Actualizaciones<br>disponible:";
$MESS["top_panel_ai_used_space"] = "Espacio utilizado";
$MESS["top_panel_ai_in_all"] = "Total:";
$MESS["top_panel_ai_in_aviable"] = "Disponible:";
$MESS["top_panel_ai_in_recomend"] = "Recomendaci�n:";
$MESS["top_panel_ai_in_no"] = "Ninguno";
$MESS["top_panel_ai_sys_ver"] = "Versi�n actual";
$MESS["top_panel_ai_upd_last"] = "�ltima actualizaci�n:";
$MESS["top_panel_ai_upd_never"] = "No hay actualizaciones instaladas.";
$MESS["top_panel_ai_marketplace"] = "Evaluar soluci�n de mercado";
$MESS["top_panel_ai_marketplace_hide"] = "Ocultar";
$MESS["top_panel_ai_marketplace_add"] = "dejar comentarios";
$MESS["top_panel_ai_marketplace_descr"] = "Est� utilizando <b>#NAME#</b> (#ID#).<br />Por favor, comparte tu opini�n. <br />
�Cu�n grande es? <br /> Qu� le gustar�a ver mejorado? <br /> �Su opini�n nos interesa!";
?>