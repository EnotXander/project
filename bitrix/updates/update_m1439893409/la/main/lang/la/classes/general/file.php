<?
$MESS["FILE_TEXT"] = "Archivo";
$MESS["FILE_WIDTH"] = "Ancho";
$MESS["FILE_HEIGHT"] = "Alto";
$MESS["FILE_SIZE"] = "Medida";
$MESS["FILE_DELETE"] = "Eliminar archivo";
$MESS["FILE_NOT_FOUND"] = "Archivo no encontrado";
$MESS["FILE_BAD_FILE_TYPE"] = "El archivo no es una imagen v�lida";
$MESS["FILE_BAD_MAX_RESOLUTION"] = "Ha excedido el m�ximo permitido de dimensiones de la imagen ";
$MESS["FILE_BAD_SIZE"] = "Ha excedido el m�ximo permitido de dimensiones para cargar la imagen ";
$MESS["FILE_BAD_TYPE"] = "Tipo incorrecto de archivo o el tama�o m�ximo del archivo ha sido excedido";
$MESS["FILE_ENLARGE"] = "Extender";
$MESS["MAIN_FIELD_FILE_DESC"] = "Descripci�n";
$MESS["FILE_FILE_DOWNLOAD"] = "Descargar archivo";
$MESS["FILE_DOWNLOAD"] = "Descargar";
$MESS["main_include_dots"] = "pixeles";
$MESS["main_js_img_title"] = "Imagen";
$MESS["FILE_SIZE_b"] = "b";
$MESS["FILE_SIZE_Kb"] = "Kb";
$MESS["FILE_SIZE_Mb"] = "Mb";
$MESS["FILE_SIZE_Gb"] = "Gb";
$MESS["FILE_SIZE_Tb"] = "Tb";
$MESS["MAIN_BAD_FILENAME1"] = "El nombre del archivo contiene caracteres no v�lidos.";
$MESS["FILE_BAD_FILENAME"] = "No se especifica el nombre del archivo.";
$MESS["MAIN_BAD_FILENAME_LEN"] = "El nombre de archivo es demasiado largo.";
$MESS["FILE_BAD_QUOTA"] = "Ha excedido la cuota de disco.";
?>