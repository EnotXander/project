<?
$MESS["SUP_ENTER_KEY"] = "Ingrese su clave de licencia";
$MESS["SUP_ENTER_CORRECT_KEY"] = "Ingrese una clave de licencia v�lida";
$MESS["SUP_CANT_OPEN_FILE"] = "No se pudo abrir el archivo para escribir";
$MESS["SUP_ACT_NAME"] = "Por favor Ingrese el nombre de la compa��a para la cual se ha generado la clave";
$MESS["SUP_ACT_EMAIL"] = "Por favor ingrese la direcci�n de correo electr�nico del contacto";
$MESS["SUP_ACT_BAD_EMAIL"] = "Revise si la direcci�n de correo electr�nico del contacto es correcta";
$MESS["SUP_ACT_URL"] = "Por favor ingrese la direcci�n del sitio que usar� la clave ";
$MESS["SUP_ENTER_CHECK"] = "Por favor ingrese el cup�n para conseguir un sitio adicional ";
$MESS["SUP_NOT_REFRESH"] = "No actualice esta p�gina";
$MESS["SUP_EMPTY_UPD_MOD"] = "No se ha elegido ning�n m�dulo para actualizar";
$MESS["SUP_EMPTY_TEMP_DIR"] = "La carpeta temporal para la actualizaci�n no se ha especificado ";
$MESS["SUP_UPD_LOAD_BREAK"] = "La descarga de actualizaci�n se interrumpi� ";
$MESS["SUP_BAD_UPD_INSTALL"] = "Un error ocurrio mientras se instalaban las actualizaciones";
$MESS["SUP_NO_LOAD_MODULES"] = "No se ha elegido ning�n m�dulo para descargar ";
$MESS["SUP_NO_TEMP_FILE"] = "El archivo temporal \"#FILE#\"  no existe";
$MESS["SUP_NO_LANGS_LOAD"] = "No se ha elegido ning�n idioma para descargar ";
$MESS["SUP_LANG_LOAD_BREAK"] = "La descarga del idioma fue interrumpida";
$MESS["SUP_BAD_LANG_INSTALL"] = "Un error ocurrio mientras se instalaba el idioma";
$MESS["SUP_EMPTY_HELP_LANG"] = "Tiene que especificar el idioma de la secci�n de la ayuda para descargar";
$MESS["SUP_HELP_LOAD_BREAK"] = "La descarga de la secci�n de ayuda fue interrumpida";
$MESS["SUP_BAD_HELP_INSTALL"] = "Los errores ocurrieron mientras se instalaban la secci�n de ayuda ";
$MESS["SUP_TITLE_BASE"] = "Actualizar sistema";
$MESS["SUP_TITLE_UPD_1"] = "Paso 1. Elegir las actualizaciones para instalar ";
$MESS["SUP_TITLE_UPD_2"] = "Paso 2. Revisar y descargar las actualizaciones seleccionadas ";
$MESS["SUP_TITLE_UPD_3"] = "Paso 3. Revisar e instalar las actualizaciones seleccionadas ";
$MESS["SUP_TITLE_UPD_4"] = "Paso 4. Instalaci�n completa";
$MESS["SUP_TITLE_LANG_2"] = "Paso 2. Revisar e instalar los archivos de idioma seleccionado";
$MESS["SUP_TITLE_LANG_3"] = "Paso 3. Instalaci�n completa";
$MESS["SUP_TITLE_HELP_2"] = "Step 2. Revisar e instalar la secci�n de ayuda";
$MESS["SUP_TITLE_HELP_3"] = "Paso 3. Instalaci�n completa";
$MESS["SUP_TITLE_HISTORY"] = "Actualizar historial";
$MESS["SUP_TITLE_ADD_SITE"] = "Agregar sitio";
$MESS["SUP_ZLIB_RECOM"] = "Please install Zlib module for PHP to intensify the update process. You can get more information on Zlib module here: <a href=\"#ZLIB_URL#\" target=\"_blank\">#ZLIB_URL#</a>";
$MESS["SUP_LICENSE_KEY"] = "Clave de licencia";
$MESS["SUP_ACTIVE_PERIOD"] = "Activo desde <i>#DATE_FROM#</i> till <i>#DATE_TO#</i>";
$MESS["SUP_UPD_HOST"] = "Conectando a #HOST#";
$MESS["SUP_ACTIVATE_PROMT"] = "Necesita activar tu clave de licencia antes de usar el sistema de actualizaci�n. Complete el siguiente formulario para activar su clave de licencia: ";
$MESS["SUP_ACTIVATE_NAME"] = "Nombre de la compa��a la la cual se ha generado la clave ";
$MESS["SUP_ACTIVATE_NAME_DESCR"] = "Para uso personal -  su nombre";
$MESS["SUP_ACTIVATE_EMAIL"] = "Direcci�n de e-mail del cont�cto";
$MESS["SUP_ACTIVATE_CONTARC_INFO"] = "Informaci�n de contacto";
$MESS["SUP_ACTIVATE_URL"] = "Direcci�n del sitio web que usar� la calve";
$MESS["SUP_ACTIVATE_DO"] = "Activar su clave de licencia";
$MESS["SUP_NO_KEY_PROMT"] = "If you do not have a license key, please <a href=\"#URL#\" target=\"_blank\">register with us</a> and get a trial license key to activate the update system.";
$MESS["SUP_NO_KEY_ACT_ALT"] = "Conseguir una licencia de pruebas";
$MESS["SUP_NO_KEY_ACT"] = "Conseguir una licencia de pruebas";
$MESS["SUP_NO_KEY_ENTER_PROMT"] = "Please enter the obtained key on the server global settings page (menu <a href=\"#URL#\">Settings</a>), or here:";
$MESS["SUP_NO_KEY_ENTER_DO"] = "Guarde su clave de licencia";
$MESS["SUP_UPDATE_UPDATE_PROMT"] = "Una m�s nueva versi�n del sistema de actualizaci�n est� disponible. Instale esta edici�n del sistema de actualizaci�n antes de realizar actualizaciones. ";
$MESS["SUP_UPD_UPD_ACT_ALT"] = "Instalar una nueva versi�n del sistema de actualizaci�nes";
$MESS["SUP_UPD_UPD_ACT"] = "Nueva versi�n del sistema de actualizaciones";
$MESS["SUP_REG_PROMT"] = "Your license allows you to obtain the fully-functional version of the system. Time-based evaluation restrictions do not apply to the fully-functional version.";
$MESS["SUP_REG_ACT_ALT"] = "Registre su copia";
$MESS["SUP_REG_ACT"] = "Registre su copia";
$MESS["SUP_MUPD_PROMT"] = "El sistema de actualizaci�n ha detectado <b>#NUM#</b> m�dulo de actualizaci�n#END# para su sitio.";
$MESS["SUP_MUPD_ACT_ALT"] = "Revisar y descargar actualizaciones";
$MESS["SUP_MUPD_ACT"] = "Revisar y descargar actualizaciones";
$MESS["SUP_MUPD_NO_UPD"] = "No hay actualizaciones de kernel.";
$MESS["SUP_LUPD_PROMT"] = "The update system has detected <b>#NUM#</b> language file update#END#.";
$MESS["SUP_LUPD_PROMT_OTHER"] = "The update system has detected language files not in use with this site.";
$MESS["SUP_LUPD_ACT_ALT"] = "Revise y descargue archivos de idioma";
$MESS["SUP_LUPD_ACT"] = "Revise y descargue archivos de idioma";
$MESS["SUP_HUPD_PROMT"] = "The update system has detected help section update#END# in <b>#NUM#</b> language#END# installed with this site.";
$MESS["SUP_HUPD_PROMT_OTHER"] = "The update system has detected help section updates in languages not in use with this site.";
$MESS["SUP_HUPD_ACT_ALT"] = "Revise y descargue actualizaciones de la secci�n de ayuda";
$MESS["SUP_HUPD_ACT"] = "Revise y descargue actualizaciones de la secci�n de ayuda";
$MESS["SUP_SRC_PROMT"] = "Your license permits you to obtain the source code for the system modules. To do so, you need the latest versions of all modules installed on your system (in other words, no updates may be available). If any of the module updates are available, please install it first.";
$MESS["SUP_SRC_ACT_ALT"] = "Descargue c�digo fuente";
$MESS["SUP_SRC_ACT"] = "Descargue c�digo fuente";
$MESS["SUP_SITES_PROMT"] = "You cannot create more than #NUM# site#END# using this kernel according to your license. If you need more sites, you can buy them any time. After you have purchased the additional sites, you will have to add them to the system.";
$MESS["SUP_SITES_ACT_ALT"] = "Agregar sitios extras";
$MESS["SUP_SITES_ACT"] = "Agregar sitios extras";
$MESS["SUP_1STEP_NOTES1"] = "The update system allows to update the kernel, language files and help section of the system to the latest version. Furthermore, you can register your copy of the product (if still unregistered) and download the source code using the update system.";
$MESS["SUP_1STEP_NOTES2"] = "Note. The update system does not collect or send any private information from your site.";
$MESS["SUP_HISTORY_LINK"] = "Ver historia de actualizaciones";
$MESS["SUP_STEP2_UPD_PROMT"] = "Please review the list of updates you have choosen for download. You can dismiss any update you don't want to download. If any dependency exist between some of the modules, you have to select either all of the dependent modules or none.";
$MESS["SUP_STEP2_NO_MODULES"] = "No hay actualizaciones disponibles para m�dulos";
$MESS["SUP_STEP2_LOAD_BTN"] = "Descargar";
$MESS["SUP_STEP3_UPD_PROMT"] = "<b>Updates are downloaded and ready for install.</b><br>Please review the list of updates you have chosen to install. You can remove any update you do not want to install. If any dependency exist between some of the updates, you have to select either all of the dependent updates or none.";
$MESS["SUP_STEP3_NO_MODULES"] = "No hay actualizaciones disponibles para m�dulos";
$MESS["SUP_STEP3_LOAD_BTN"] = "Instalar";
$MESS["SUP_UPD_DESCR_VERC"] = "Este m�dulo requiere que los siguientes m�dulos funcionen correctamente ";
$MESS["SUP_UPD_DESCR_VERC_N"] = "versi�n #VERS# and higher";
$MESS["SUP_TOTAL_UPDS"] = "Total";
$MESS["SUP_MODULE"] = "M�dulo";
$MESS["SUP_NEW"] = "Nuevo";
$MESS["SUP_LAST_VERSION"] = "ultima versi�n";
$MESS["SUP_LOAD_SIZE"] = "Tama�o de la descarga";
$MESS["SUP_DELETE_FROM_LOAD"] = "Remover";
$MESS["SUP_ADD_TO_LOAD"] = "Agregar";
$MESS["SUP_ALERT_PART1"] = "M�dulo";
$MESS["SUP_ALERT_PART2"] = "no puede actualizarse sin el m�dulo";
$MESS["SUP_ALERT_PART3"] = "Actualizaci�n de m�dulos";
$MESS["SUP_ALERT_PART4"] = "ser� desactivado";
$MESS["SUP_ALERT1_PART1"] = "M�dulo";
$MESS["SUP_ALERT1_PART2"] = "tiene que ser actualizado si actualiza el m�dulo";
$MESS["SUP_ALERT1_PART3"] = "Actualizaci�n de m�dulos";
$MESS["SUP_ALERT1_PART4"] = "ser� activado";
$MESS["SUP_STEP4_UPD_PROMT"] = "Important! Do not try to refresh this page or click the \"Back\" button in your browser.";
$MESS["SUP_STEP4_INST"] = "Instalar";
$MESS["SUP_STEP4_INST_ALT"] = "�Tiene que instalar el m�dulo antes de usarlo! ";
$MESS["SUP_STEP4_INST_DO"] = "Instalar";
$MESS["SUP_STEP4_UPD_LOG"] = "Updating the module #MODULE# to version #VERS#.";
$MESS["SUP_STEP4_SUCCESS"] = "Los m�dulos siguientes se han actualizado con �xito ";
$MESS["SUP_STEP4_ERROR"] = "un error ocurri� mientras se instalaban los siguiente m�dulos";
$MESS["SUP_STEP4_NONE"] = "El siguiente m�dulo no fue actualizado";
$MESS["SUP_MAIN_PAGE"] = "P�gina principal del sistema de actualizaci�n ";
$MESS["SUP_LSTEP2_PROMT"] = "Revise la lista de actualizaciones de la idiomas disponibles para la instalaci�n de su sitio. Seleccione los idiomnas que desewa actualizar. ";
$MESS["SUP_LSTEP2_NO_LANGS"] = "No hay idiomas disponibles";
$MESS["SUP_LSTEP2_LOAD_BTN"] = "Descarga";
$MESS["SUP_LSTEP2_FROM"] = "desde";
$MESS["SUP_LSTEP2_INST"] = "Idiomas instalados";
$MESS["SUP_LSTEP2_OTHER"] = "Idiomas no instalados";
$MESS["SUP_LSTEP3_UPD_LOG"] = "Actualizaci�n de idioma #LANG# el #DATE#.";
$MESS["SUP_LSTEP3_SUCCESS"] = "Los siguiente idiomas fueron actualizados correctamente";
$MESS["SUP_LSTEP3_ERROR"] = "Un error ocurri� mientras se actualizaba el siguiente idioma";
$MESS["SUP_LSTEP3_NONE"] = "Los siguiente idiomas no pueden ser cargados";
$MESS["SUP_HSTEP2_PROMT"] = "Revise la lista de actualizaciones de la secci�n de la ayuda disponibles para la instalaci�n para su sitio. Selecciona secci�n de ayuda que desea actualizar. ";
$MESS["SUP_HSTEP2_NO_UPDS"] = "No hay actualizaciones disponibles";
$MESS["SUP_HSTEP2_LOAD_BTN"] = "Descargar";
$MESS["SUP_HSTEP2_INST"] = "Actualizaciones de secci�n de ayuda para los idiomas instalados";
$MESS["SUP_HSTEP2_OTHER"] = "Actualizaciones de secci�n de ayuda para los idiomas no instalados";
$MESS["SUP_HSTEP3_UPD_ERR"] = "Un error ocurri� mientras se instalaban la actualizaci�n de la secci�n de ayuda <b>#HELP#</b> al <b>#DATE#</b>.";
$MESS["SUP_HSTEP3_UPD_LOG"] = "Actualizaci�n de la secci�n de ayuda para el idioma #HELP# al #DATE#. ";
$MESS["SUP_HSTEP3_UPD_SUC"] = "La actualizaci�n de la secci�n de ayuda para el idioma<b>#HELP#</b> al <b>#DATE#</b> es exitosa. ";
$MESS["SUP_HSTEP3_UPD_NONE"] = "Las actualizaciones de la secci�n de ayuda para el idioma <b>#HELP#</b> no fue instaladoo";
$MESS["SUP_HIST_PROMT"] = "Esta tabla muestra las actualizaciones  instaladas";
$MESS["SUP_HIST_DATE"] = "Fecha";
$MESS["SUP_HIST_DESCR"] = "Descripci�n";
$MESS["SUP_HIST_STATUS"] = "Estatus";
$MESS["SUP_HIST_SUCCESS"] = "�xito ";
$MESS["SUP_HIST_ERROR"] = "Errores";
$MESS["SUP_HIST_NOTES"] = "Nota";
$MESS["SUP_HIST_EMPTY_LOG"] = "La historia de actualizaciones est� vac�a";
$MESS["SUP_HIST_PNOTES1"] = "Nota.";
$MESS["SUP_HIST_PNOTES2"] = "Esta p�gina muestra las �ltimas 20 actualizaciones";
$MESS["SUP_ADD_SITE_PROMT"] = "Ingrese el cup�n para conseguir un sitio extra";
$MESS["SUP_AD_SITE_DO"] = "Traer sitio";
$MESS["SUPP_AS_NO_CHECK"] = "El cup�n para conseguir un sitio extra no fue especificado";
$MESS["SUPP_AS_EMPTY_RESP"] = "Respuesta vac�a del servidor de actualizaci�n ";
$MESS["SUPP_RV_ER_TEMP_FILE"] = "No se puede crear el archivo temporal en el directorio \"#FILE#\"";
$MESS["SUPP_RV_WRT_TEMP_FILE"] = "no se puede escribir en el archivo temporal \"#FILE#\"";
$MESS["SUPP_RV_BREAK"] = "Registro interrumpido";
$MESS["SUPP_RV_ER_DESCR_FILE"] = "El archivo \"#FILE#\" de la descripci�n de la actualizaci�n no existe ";
$MESS["SUPP_RV_READ_DESCR_FILE"] = "No hay acceso de lectura en el archivo \"#FILE#\" ";
$MESS["SUPP_RV_NO_FILE"] = "El archivo no funciona";
$MESS["SUPP_RV_ER_SIZE"] = "La revisi�n de longitud fall� ";
$MESS["SUPP_RV_NO_WRITE"] = "No hay acceso de escritura en el archivo \"#FILE#\" ";
$MESS["SUPP_RV_ERR_COPY"] = "Un error ocurri� mientras se copiaban los archivos";
$MESS["SUPP_UU_BREAK"] = "El proceso de actualizaci�n fue interrumpido";
$MESS["SUPP_UU_NO_UFILE"] = "El archivo #FILE# no funciona";
$MESS["SUPP_UU_LOAD_BREAK"] = "La descarga de actualizaciones fue interrumpido";
$MESS["SUPP_UU_NO_NEW_VER"] = "El n�mero de la nueva versi�n del sistema de la actualizaci�n es inasequible";
$MESS["SUPP_LS_LOAD_BREAK"] = "El proceso de descarga del c�digo fuente fue interrumpido";
$MESS["SUPP_LS_NO_MOD_CAT"] = "No se puede crear el folder para el m�dulo \"#MODULE#\"";
$MESS["SUPP_LS_WR_MOD_CAT"] = "La carpeta para el m�dulo \"#MODULE#\" no es accesible para escritura";
$MESS["SUPP_LS_NO_SRC_MOD"] = "No se puede encontrar la carpeta con el c�digo fuente de el m�dulo \"#MODULE#\"";
$MESS["SUPP_LS_RD_SRC_MOD"] = "La carpeta con el c�digo fuente para el m�dulo \"#MODULE#\" no tiene acceso de lectura ";
$MESS["SUPP_GAUT_SYSERR"] = "Un error ocurri� en el sistema. POor favor av�sele a los desarrolladores.";
$MESS["SUPP_LMU_NO_MODS"] = "No hay m�dulos seleccionados para descargar";
$MESS["SUPP_LMU_NO_TMP_FILE"] = "No se puede crear el archivo temporal \"#FILE#\"";
$MESS["SUPP_LLU_NO_LANGS"] = "No hay idiomas para descargar";
$MESS["SUPP_LHU_NO_HELP"] = "Ho hay secciones de ayuda seleccionadas para descargar";
$MESS["SUPP_UGA_NO_TMP_FILE"] = "El archivo temporal \"#FILE#\" no existe";
$MESS["SUPP_UGA_NO_READ_FILE"] = "No se tiene acceso de lectura en el archivo \"#FILE#\" ";
$MESS["SUPP_UGA_NO_TMP_CAT"] = "No se puede crear la carpeta temporal \"#FILE#\"";
$MESS["SUPP_UGA_WRT_TMP_CAT"] = "No se puede tener acceso de escritura a la carpeta temporal \"#FILE#\" ";
$MESS["SUPP_UGA_CANT_OPEN"] = "No se puede abrir el archivo \"#FILE#\"";
$MESS["SUPP_UGA_BAD_FORMAT"] = "El formato del archivo \"#FILE#\"  es incorrecto";
$MESS["SUPP_UGA_FILE_CRUSH"] = "La falta ocurri� mientras que intentaba tener acceso al archivo \"#FILE#\"";
$MESS["SUPP_UGA_CANT_OPEN_WR"] = "No se puede abrir para escribir en el archivo temporal \"#FILE#\" ";
$MESS["SUPP_UGA_CANT_WRITE_F"] = "No se puede escribir en el archivo temporal \"#FILE#\"";
$MESS["SUPP_CV_ERR_ARR"] = "Error en la descripci�n del archivo";
$MESS["SUPP_CV_NO_SELECTED"] = "La lista de m�dulos seleccionados est� vac�a";
$MESS["SUPP_CV_EMPTY_MODS"] = "The list of modules is empty";
$MESS["SUPP_CV_RES_ERR"] = "To update the module #MODULE1#, you have to update the module #MODULE2# to version #VERS# or higher. ";
$MESS["SUPP_CU_NO_TMP_CAT"] = "The temporary folder \"#FILE#\" does not exist";
$MESS["SUPP_CU_RD_TMP_CAT"] = "The temporary folder \"#FILE#\" cannot be accessed for reading";
$MESS["SUPP_CU_MAIN_ERR_FILE"] = "The target file \"#FILE#\" cannot be accessed for writing";
$MESS["SUPP_CU_MAIN_ERR_CAT"] = "The target folder \"#FILE#\" cannot be accessed for writing";
$MESS["SUPP_UK_NO_MODS"] = "No modules has beed selected to update";
$MESS["SUPP_UK_NO_MODIR"] = "The module folder \"#MODULE_DIR#\" does not exist";
$MESS["SUPP_UK_WR_MODIR"] = "The module folder \"#MODULE_DIR#\" cannot be accessed for writing";
$MESS["SUPP_UK_NO_FDIR"] = "The temporary update folder \"#DIR#\" does not exist";
$MESS["SUPP_UK_READ_FDIR"] = "The module folder \"#DIR#\" cannot be accessed for reading";
$MESS["SUPP_UK_UPDN_ERR"] = "The module #MODULE# updater #VER# error";
$MESS["SUPP_UK_UPDN_ERR_BREAK"] = "The module #MODULE# update installation interrupted.";
$MESS["SUPP_UK_UPDY_ERR"] = "The module #MODULE# post-updater #VER# error";
$MESS["SUPP_UK_UPDN_ERR_BREAK1"] = "The module #MODULE# update installation interrupted.";
$MESS["SUPP_UL_NO_LANGS"] = "No se ha especificado ning�n idioma para actualizar";
$MESS["SUPP_UL_CAT"] = "The folder \"#FILE#\" does not exist";
$MESS["SUPP_UL_NO_WRT_CAT"] = "The folder \"#FILE#\" cannot be accessed for writing";
$MESS["SUPP_UL_NO_TMP_LANG"] = "The temporary language folder \"#FILE#\" does not exist";
$MESS["SUPP_UL_NO_READ_LANG"] = "The language folder \"#FILE#\" cannot be accessed for reading";
$MESS["SUPP_UL_BREAK_LANG"] = "The language #LANG# update installation interrupted.";
$MESS["SUPP_UH_NO_LANG"] = "La secci�n de ayuda para el idioma no est� especificado";
$MESS["SUPP_UH_NO_HELP_CAT"] = "Cannot create the help section folder \"#FILE#\"";
$MESS["SUPP_UH_NO_WRT_HELP"] = "The help section folder \"#FILE#\" cannot be accessed for writing";
$MESS["SUPP_UH_CANT_DEL"] = "No se puede borrar la carpeta \"#FILE#\"";
$MESS["SUPP_UH_CANT_RENAME"] = "No se puede renombrar la carpeta \"#FILE#\"";
$MESS["SUPP_UH_CANT_CREATE"] = "Cannot create the help section folder \"#FILE#\"";
$MESS["SUPP_UH_CANT_WRITE"] = "The help section folder \"#FILE#\" cannot be accessed for writing";
$MESS["SUPP_UH_INST_BREAK"] = "The help section update for language #HELP# interrupted.";
$MESS["SUPP_PSD_BAD_RESPONSE"] = "Respuesta desconocida del servidor de actualizaci�nes";
$MESS["SUPP_PSD_BAD_TRANS"] = "Error al transferir datos";
$MESS["SUPP_GM_ERR_DMAIN"] = "Error en la descripci�n del m�dulo principal";
$MESS["SUPP_GM_ERR_DMOD"] = "Error en la descripci�n del m�dulo \"#MODULE#\" ";
$MESS["SUPP_GM_NO_KERNEL"] = "El n�cleo no funciona";
$MESS["SUPP_GL_WHERE_LANGS"] = "No se puede detectar el idioma del sitio";
$MESS["SUPP_GL_ERR_DLANG"] = "Error enla descripci�n del idioma \"#LANG#\" ";
$MESS["SUPP_GL_NO_SITE_LANGS"] = "No puede encontrar el idioma del sitio";
$MESS["SUPP_GH_ERR_DHELP"] = "Error en la secci�n de ayuda para el idioma \"#HELP#\"";
$MESS["SUPP_GHTTP_ER"] = "Error al conectar al servidor de actualizaciones";
$MESS["SUPP_GHTTP_ER_DEF"] = "No se puede conectar al servidor de actualizaciones";
$MESS["SUPP_CDF_SELF_COPY"] = "No puede copiar a s� mismo";
$MESS["SUPP_CDF_NO_PATH"] = "La ruta \"#FILE#\" no existe";
$MESS["SUPP_CDF_CANT_CREATE"] = "No se puede crear la carpeta \"#FILE#\"";
$MESS["SUPP_CDF_CANT_WRITE"] = "No se puede escribir en la carpeta \"#FILE#\" ";
$MESS["SUPP_CDF_CANT_FILE"] = "No se puede escribir en la carpeta \"#FILE#\" ";
$MESS["SUPP_CDF_CANT_FOLDER"] = "No se puede crear la carpeta \"#FILE#\"";
$MESS["SUPP_CDF_CANT_FOLDER_WR"] = "No se puede escribir en la carpeta \"#FILE#\" ";
$MESS["HINT_WIND_TITLE"] = "Descripci�n de actualizaciones";
$MESS["HINT_WIND_EXEC"] = "Descripci�n de actualizaciones";
$MESS["HINT_WIND_EXEC_ALT"] = "Descripci�n de actualizaciones";
$MESS["SUP_ERROR_NO_MAIL_U"] = "Ingrese su direcci�n de e-mail para la suscripci�n";
$MESS["SUP_ERROR_BAD_MAIL_U"] = "La direcci�n de e-mail \"#EMAIL#\"  tiene un formato errado";
$MESS["SUP_SUBSCR_ALREADY_U"] = "Usted ya est� susvcrito a los boletines de noticias del sistema de la actualizaci�n. Puede modificar su suscripci�n ";
$MESS["SUP_SUBSCR_NEW_U"] = "Aqu� puede suscribirse a los boletines de noticias del sistema de la actualizaci�n ";
$MESS["SUP_SUBSCR_ALREADY_CHANGE_U"] = "Cambiar";
$MESS["SUP_SUBSCR_ALREADY_DEL_U"] = "Borrar";
$MESS["SUP_SUBSCR_ALREADY_ADD_U"] = "Suscribir";
$MESS["SUP_ACT_NO_USER_NAME"] = "Name of the account owner at <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a> was not specified";
$MESS["SUP_ACT_NO_USER_LAST_NAME"] = "Last Name of the account owner at <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a> was not specified";
$MESS["SUP_ACT_NO_USER_LOGIN"] = "Login for the site account at <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a> was not entered";
$MESS["SUP_ACT_SHORT_USER_LOGIN"] = "Login of the account at <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a> can not be less than three characters";
$MESS["SUP_ACT_NO_USER_PASSWORD"] = "Password for the account on the site <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a> was not specified";
$MESS["SUP_ACT_NO_USER_PASSWORD_CONFIRM"] = "Password for the account on <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a> does not match the password confirmation";
$MESS["SUP_ACT_CREATE_USER_HINT"] = "If you are not registered on the <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a>, please be sure that checkbox \"Create a user\" is ticked and then fill in your registration info (name, last name, login and password) in the appropriate form fields. Registration on the siet <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a> will provide you the possibility to use <a href=\"http://www.bitrixsoft.com/support/\" target=\"_blank\">Helpdesk service</a> and <a href=\"http://www.bitrixsoft.com/support/forum/\" target=\"_blank\">Private forum</a> for solving all the product related questions.";
$MESS["SUP_ACT_F_CREATE_USER"] = "Create a user on the site <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a>";
$MESS["SUP_ACT_F_USER_NAME"] = "Su nombre";
$MESS["SUP_ACT_F_USER_LAST_NAME"] = "Su Apellido";
$MESS["SUP_ACT_F_USER_LOGIN"] = "Login (no menos de 3 caracteres)";
$MESS["SUP_ACT_F_USER_PASSWORD"] = "Contrase�a";
$MESS["SUP_ACT_F_USER_PASSWORD_CONFIRM"] = "Confirmaci�n de contrase�a";
$MESS["SUP_NO_KEY_PROMT_SRC"] = "ingrese la clave de licencia en el campo apropiado en esta p�gina. La clave de licencia se puede encontrar en tu caja del CD o en tu email de la confirmaci�n de la compra. La clave de la licencia debe tener el formato siguiente:  SM4-XL-XXXXXXXXXXXXXXX, donde X - es cualquier car�cter. ";
$MESS["SUPN_NO_FOLDER_EX"] = "La carpeta \"#FILE#\" no existe";
$MESS["SUPN_CU_RD_TMP_CAT"] = "La carpeta \"#FILE#\" no puede ser leida";
$MESS["SUP_MYSQL_L4"] = "From the version 5.0, Bitrix Site Manager requires MySql 4.0 or higher.<br>Should you have any questions regarding minimum system requirements, please contact the <a href=\"http://www.bitrixsoft.com/support/\" target=\"_blank\">Helpdesk service</a>.";
$MESS["SUP_STABLE_OFF"] = "�Advertencia! se permiten  actualizaciones de versiones beta para  ser instaladas en su sitio. ";
$MESS["SUP_STABLE_OFF_PROMT"] = "Las versiones beta de actualizaciones se permiten ser instaladas en tu sitio. Si prefiere instalar solamente actualizaciones estables, inhabilite plas versiones beta desde esta p�gina mediante la configuraci�n del m�dulo del n�cleo. ";
$MESS["SUP_STABLE_ON_PROMT"] = "Solamente estan permitidas las actualizaciones estables en su sitio. Si deseas descargar e instalar las versiones beta, permita por favor las versiones beta en esta p�gina o en los ajustes del m�dulo del n�cleo.";
$MESS["SUP_STABLE_PROMT"] = "Una &quot; Version&quot beta; es una trozo actualizaci�n que ha sido probada internamente por los desarrolladores antes de su lanzamiento. Las versiones beta incluyen los �ltimos cambios y arreglos, y son relativamente estables pero pueden causar problemas en algunos casos. ";
$MESS["SUP_STABLE_TURN_ON"] = "Permitir s�lo versiones estables";
$MESS["SUP_STABLE_TURN_OFF"] = "Permitir las versiones beta";
$MESS["LICENSE_AGREE_PROMT"] = "Acepto los t�rminos de la licencia";
$MESS["SUP_LICENSE_ENTER_DO"] = "Aplicar";
$MESS["SUP_EDITION"] = "Edici�n:";
$MESS["SUP_CHECK_PROMT"] = "Segun su licencia usted no puede crear m�s de #NUM# sitio(s) basado en este kernel.";
$MESS["SUP_CHECK_PROMT_2"] = "Segun su licencia usted puede crear infinitos sitios basado en este kernel.";
$MESS["SUP_CHECK_PROMT_1"] = "Usted puede extendee el periodo de soporte t�cnicoo comprando un sitio adicional cambiando su licencia.";
$MESS["SUP_CHECK_ACT"] = "Activar cupon";
$MESS["SUP_ADD_CHECK_PROMT"] = "Ingresze el c�digo del cup�n";
$MESS["SUP_AD_CHECK_DO"] = "Activar";
$MESS["SUP_SITES"] = "N�mero de sitios:";
$MESS["SUP_WORD_YAZIK_END1"] = "s";
$MESS["SUP_WORD_YAZIK_END2"] = "s";
$MESS["SUP_WORD_YAZIK_END3"] = " ";
$MESS["SUP_WORD_YAZIK_END4"] = "s";
$MESS["SUP_WORD_SAIT_END1"] = "s";
$MESS["SUP_WORD_SAIT_END2"] = "s";
$MESS["SUP_WORD_SAIT_END3"] = " ";
$MESS["SUP_WORD_SAIT_END4"] = "s";
$MESS["SUP_WORD_OBNOVL_END1"] = "s";
$MESS["SUP_WORD_OBNOVL_END2"] = "s";
$MESS["SUP_WORD_OBNOVL_END3"] = " ";
$MESS["SUP_WORD_OBNOVL_END4"] = "s";
$MESS["SUP_CANT_CONTRUPDATE"] = "Por favor usar el controlador de sistema de actualizaci�n para actualizar este sitio";
?>