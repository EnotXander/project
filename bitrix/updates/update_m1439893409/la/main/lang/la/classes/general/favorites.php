<?
$MESS["fav_general_err_name"] = "&quot;Nombre&quot; el campo no puede estar vacio.";
$MESS["fav_general_err_url"] = "&quot;Link&quot; el campo no puede estar vacio.";
$MESS["fav_general_err_user"] = "El usuario especificado no existe.";
$MESS["fav_general_err_user1"] = "Especifique el usuario al que le pertenece esta marca.";
$MESS["fav_general_err_lang"] = "La interface de lenguaje especificada no existe.";
$MESS["fav_general_err_lang1"] = "Especifique una interface de lenguaje por favor.";
$MESS["fav_main_menu_alt"] = "V�nculos favoritos";
$MESS["fav_main_menu_header"] = "Favoritos";
$MESS["fav_main_menu_nothing"] = "Su lista de Favoritos<br>est� actualmente vac�a.";
$MESS["fav_main_menu_add_icon"] = "Agregar p�ginas a Favoritos<br>haciendo clic en el asterisco<br>al lado del t�tulo de la secci�n.";
$MESS["fav_main_menu_add_dd"] = "Otra forma es<br>arrastrar un elemento de men� y col�quelo<br>n la zona de la derecha.";
$MESS["fav_main_menu_close_hint"] = "Cerrar sugerencia";
$MESS["fav_main_menu_open_hint"] = "Abrir sugerencia";
?>