<?
$MESS["FOLDER_EDIT_ACCESS_DENIED"] = "Acceso a la edici�n de la secci�n denegado.";
$MESS["ACCESS_EDIT_FILE_NOT_FOUND"] = "El archivo no existe.";
$MESS["EDIT_ACCESS_TO_DENIED"] = "Permisos insuficientes para modificar los par�metros de acceso del";
$MESS["EDIT_ACCESS_TO_FOLDER"] = "Secci�n de permisos";
$MESS["EDIT_ACCESS_TO_FILE"] = "Los permisos de archivo";
$MESS["EDIT_ACCESS_USER_GROUP"] = "Grupo";
$MESS["EDIT_ACCESS_PERMISSION"] = "Permiso";
$MESS["EDIT_ACCESS_INHERIT_PERMISSION"] = "Permisos inherentes ";
$MESS["EDIT_ACCESS_CURRENT_PERMISSION"] = "Permiso Actual ";
$MESS["EDIT_ACCESS_NOT_SET"] = "- Sin definir --";
$MESS["EDIT_ACCESS_SET_INHERIT"] = "Inherente";
$MESS["EDIT_ACCESS_SET_INHERITED"] = "Inherentemente";
$MESS["EDIT_ACCESS_SELECT_GROUP"] = "- Seleccione grupo --";
$MESS["EDIT_ACCESS_ALL_GROUPS"] = "Configuraci�n de acceso por defecto para todos los grupos";
$MESS["EDIT_ACCESS_SET_PERMISSION"] = "Permiso est� inherente a la secci�n parental. Para cambiar el nivel de permisos de entrada haga clic en este campo.";
$MESS["EDIT_ACCESS_ADD_PERMISSION"] = "Nuevo permiso";
$MESS["EDIT_ACCESS_PERMISSION_INFO"] = "Un <b>permiso de acceso</b> est� fijado por las reglas que definen los niveles del acceso para grupos de usuarios (e.g. &quot;read&quot; ? ver el contenido de una p�gina o una secci�n). Permisos de acceso se pueden heredar o heredado: si una secci�n o una p�gina no se especifica expl�citamente el permiso de acceso, las reglas de la secci�n parental tome su lugar. ";
$MESS["EDIT_ACCESS_REMOVE_PERM"] = "No restringir accesos a esta secci�n";
$MESS["EDIT_ACCESS_REMOVE_PERM_FILE"] = "No restringir el acceso a este archivo";
$MESS["EDIT_ACCESS_REMOVE_PERM_CONF"] = "Cualquier permisos de acceso asignado a este archivo podr� ser quitado. En vez de que los permisos de la secci�n parental sean aplicados. Continuar?";
?>