<?
$MESS["PAGE_DELETE_ACCESS_DENIED"] = "Usted no tiene permisos para borrar este archivo.";
$MESS["PAGE_DELETE_FILE_NOT_FOUND"] = "Archivo especificado no existe.";
$MESS["PAGE_DELETE_WINDOW_TITLE"] = "Suprimir P�gina de confirmaci�n";
$MESS["PAGE_DELETE_CONFIRM_TEXT"] = "�Est�s seguro de que desea eliminar la p�gina seleccionada<b>#FILENAME#</b>?";
$MESS["PAGE_DELETE_BUTTON_YES"] = "S�, elim�nelo";
$MESS["PAGE_DELETE_BUTTON_NO"] = "No, no suprimir";
$MESS["PAGE_DELETE_ERROR_OCCURED"] = "Error al eliminar el archivo. Posible raz�n: Usted tiene los permisos no basta para eliminar el archivo.";
$MESS["PAGE_DELETE_INDEX_WARNING"] = "�Atenci�n! Eliminar de la p�gina de �ndice puede causar la secci�n del sitio no est�n disponibles.";
$MESS["PAGE_DELETE_FROM_MENU"] = "borrar la p�gina de men�";
?>