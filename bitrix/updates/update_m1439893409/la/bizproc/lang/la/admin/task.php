<?
$MESS["BPAT_BACK"] = "Regresar";
$MESS["BPAT_GOTO_DOC"] = "Ir al documento";
$MESS["BPAT_TAB"] = "Tarea";
$MESS["BPAT_TAB_TITLE"] = "Tarea";
$MESS["BPAT_TITLE"] = "Tarea #ID#";
$MESS["BPAT_DESCR"] = "Descripción tarea";
$MESS["BPAT_NAME"] = "Nombre de la tarea";
$MESS["BPAT_NO_TASK"] = "La tarea no fue encontrada.";
$MESS["BPAT_USER"] = "Usuario";
$MESS["BPAT_USER_NOT_FOUND"] = "(No encontrado)";
?>