<?
$MESS["AD_PAGE_TITLE"] = "Contratos de Publicidad";
$MESS["AD_RED"] = "rojo";
$MESS["AD_RED_ALT"] = "el contrato no est� activo o expir� debido a sus limitaciones";
$MESS["AD_GREEN"] = "verde";
$MESS["AD_GREEN_ALT"] = "el contrato no est� activo y no ha expirado";
$MESS["AD_F_DATE_MODIFY"] = "Fecha de modifcaci�n:";
$MESS["AD_F_NAME"] = "T�tulo:";
$MESS["AD_F_ID"] = "ID";
$MESS["AD_F_DESCRIPTION"] = "Descripci�n:";
$MESS["AD_F_LAMP"] = "Indicador:";
$MESS["AD_F_OWNER"] = "ID del propietario, login o nombre:";
$MESS["AD_F_BANNER_COUNT"] = "N�mero de banners";
$MESS["AD_F_SHOWN"] = "Mostrar:";
$MESS["AD_F_CLICKED"] = "Clicks:";
$MESS["AD_F_CTR"] = "CTR (%):";
$MESS["AD_PAGES"] = "Contratos";
$MESS["AD_LAMP"] = "Ind.";
$MESS["AD_DATE_MODIFY"] = "Modificado";
$MESS["AD_MODIFIED_BY"] = "Modificado por";
$MESS["AD_ACTIVE"] = "Act.";
$MESS["AD_WEIGHT"] = "Peso";
$MESS["AD_OWNER"] = "Usuarios quienes acceden al contrato (propietarios)";
$MESS["AD_BANNER_COUNT"] = "Banners";
$MESS["AD_SHOW_COUNT"] = "(SH) Mostrado";
$MESS["AD_MAX_SHOW_COUNT"] = "Mostrar";
$MESS["AD_NAME"] = "T�tulo";
$MESS["AD_DELETE_CONTRACT"] = "Eliminar el contrato y toda su publicidad";
$MESS["AD_DELETE_CONTRACT_CONFIRM"] = "�Est� seguro que desea eliminar el contrato y todos sus banners?";
$MESS["AD_RIGHT_EDIT"] = "Modificar contrato:";
$MESS["AD_RIGHT_ADD"] = "Administrar banners:";
$MESS["AD_RIGHT_VIEW"] = "Ver estad�sticas:";
$MESS["AD_F_ADMIN_COMMENTS"] = "Comentarios administrativos:";
$MESS["AD_DESCRIPTION"] = "Descripci�n";
$MESS["AD_CLICK_COUNT"] = "(CL) Clicks";
$MESS["AD_MAX_CLICK_COUNT"] = "Clicks";
$MESS["AD_CONTRACT_EDIT"] = "Configuraci�n de contratos";
$MESS["AD_CONTRACT_VIEW_SETTINGS"] = "Ver configuraci�n de contratos";
$MESS["AD_CONTRACT_STATISTICS_VIEW"] = "Gr�fico CTR de contratos";
$MESS["AD_SORT"] = "Clasificaci�n";
$MESS["AD_VISITOR_COUNT"] = "(VS) Visitantes";
$MESS["AD_VISITOR_COUNT_SHORT"] = "VS:";
$MESS["AD_CLICK_COUNT_SHORT"] = "CL:";
$MESS["AD_SHOW_COUNT_SHORT"] = "SH:";
$MESS["AD_F_VISITORS"] = "Visitantes:";
$MESS["AD_F_SITE"] = "Sitio:";
$MESS["AD_SITE"] = "Sitios";
$MESS["ADV_EDIT_TITLE"] = "Editar contrato";
$MESS["ADV_SITE_VIEW"] = "Ver detalles del sitio";
$MESS["ADV_BANNER_LIST"] = "Ver lista de Banners";
$MESS["ADV_FLT_SEARCH"] = "Buscar";
$MESS["AD_ADD"] = "Agregar contrato";
$MESS["AD_ADD_TITLE"] = "Agregar un nuevo contrato";
$MESS["AD_VIEW"] = "Ver";
$MESS["AD_VIEW_TITILE"] = "Ver par�metros de contratos";
$MESS["ADV_FLT_SEARCH_TITLE"] = "ingrese texto a buscar";
$MESS["AD_CLICK_COUNT_MAX"] = "Clicks max.";
$MESS["AD_SHOW_COUNT_MAX"] = "Impresiones max.";
$MESS["AD_VISITOR_COUNT_MAX"] = "Visitantes max.";
$MESS["DELETE_ERROR"] = "Error al borrar el contrato";
$MESS["MAIN_EDIT_ERROR"] = "Error al modificar el contrato";
$MESS["AD_EDIT"] = "Modificar";
$MESS["AD_STATISTICS"] = "Diagrama";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Seleccionado:";
$MESS["AD_EXACT_MATCH"] = "Utilizar coincidencia exacta";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Comprobado:";
$MESS["AD_ALL"] = "(todo)";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "eliminar";
$MESS["AD_TILL"] = "hasta";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "activar";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "desactivar";
?>