<?
$MESS["FID_TIP"] = "ID del foro";
$MESS["TID_TIP"] = "ID del tema";
$MESS["MID_TIP"] = "ID del env�o";
$MESS["URL_TEMPLATES_INDEX_TIP"] = "P�gina de los foros";
$MESS["URL_TEMPLATES_LIST_TIP"] = "P�gina de los temas";
$MESS["URL_TEMPLATES_READ_TIP"] = "P�gina del tema visto";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "Plantilla URL para perfil del usuario";
$MESS["URL_TEMPLATES_TOPIC_SEARCH_TIP"] = "P�gina de b�squeda del tema del foro";
$MESS["PATH_TO_SMILE_TIP"] = "Ruta a la carpeta de los smileys, realtiva a la ra�z del sitio";
$MESS["PATH_TO_ICON_TIP"] = "Ruta a la carpeta del �cono tem�tico, relativo a la r��z del sitio.";
$MESS["WORD_LENGTH_TIP"] = "Extensi�n de la palabra";
$MESS["IMAGE_SIZE_TIP"] = "Dimensi�n de la imagen adjunta (px)";
$MESS["DATE_FORMAT_TIP"] = "Formato de fecha";
$MESS["DATE_TIME_FORMAT_TIP"] = "Formato de fecha y hora";
$MESS["SET_NAVIGATION_TIP"] = "Mostrar controles de navegaci�n";
$MESS["CACHE_TYPE_TIP"] = "Tipo de Cach�";
$MESS["CACHE_TIME_TIP"] = "Tiempo de duraci�n del cach� (seg.)";
$MESS["SET_TITLE_TIP"] = "Establecer el t�tulo de la p�gina";
?>