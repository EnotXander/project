<?
$MESS["F_TITLE"] = "Editar perfil";
$MESS["FP_SEX_MALE"] = "Masculino";
$MESS["FP_SEX_FEMALE"] = "Femenino";
$MESS["FP_NO_AUTHORIZE"] = "Usted no est� autorizado";
$MESS["FP_CHG_REG_INFO"] = "Cambiar informaci�n de registro";
$MESS["FP_ERR_INTERN"] = "Error interno #HZ1257";
$MESS["FP_ERR_PROF"] = "Error modificando perfil";
$MESS["F_NO_MODULE"] = "M�dulo del Foro no est� instalado";
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "Su sesi�n ha expirado. Por favor reenv�e su mensaje.";
$MESS["F_USER_NOT_FOUND"] = "El usuario no fue encontrado.";
$MESS["F_TITLE_TITLE"] = "Editar perfil";
$MESS["F_ACCESS_DENIED"] = "Acceso denegado.";
?>