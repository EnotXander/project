<?
$MESS["FORUM_ID_TIP"] = "ID de la discusi�n del Foro";
$MESS["IBLOCK_TYPE_TIP"] = "Tipo de Block de informaci�n (s�lo para verificaci�n)";
$MESS["IBLOCK_ID_TIP"] = "ID del Block de Informaci�n";
$MESS["ELEMENT_ID_TIP"] = "ID del elemento";
$MESS["URL_TEMPLATES_READ_TIP"] = "P�gina de vista del tema del foro";
$MESS["URL_TEMPLATES_DETAIL_TIP"] = "P�gina del elemento del Bock de Informaci�n";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "P�gina del perfil del usuario";
$MESS["MESSAGES_PER_PAGE_TIP"] = "N�mero de Mensajes Por P�gina";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "Plantilla del breadcrumb de navegaci�n";
$MESS["DATE_TIME_FORMAT_TIP"] = "Formato de Fecha y Hora";
$MESS["PATH_TO_SMILE_TIP"] = "Ruta a la carpeta de los Emoticons (relativo a la ra�z del sitio)";
$MESS["USE_CAPTCHA_TIP"] = "Utiliza CAPTCHA";
$MESS["PREORDER_TIP"] = "Mostrar primeros los primeros mensajes";
$MESS["CACHE_TYPE_TIP"] = "Tipo de cache";
$MESS["CACHE_TIME_TIP"] = "Duraci�n del Cach� (seg.)";
$MESS["EDITOR_CODE_DEFAULT_TIP"] = "Por defecto editor en modo de texto plano";
$MESS["SHOW_AVATAR_TIP"] = "Mostrar avatars";
$MESS["SHOW_RATING_TIP"] = "Mostrar calificaci�n";
$MESS["SHOW_MINIMIZED_TIP"] = "Colapsar formulario de respuesta";
$MESS["ENTITY_TYPE_TIP"] = "Una entidad de tipo ID, debe contener s�lo dos caracteres Latinos. Ejemplo: tipo de entidad tarea es TK.";
$MESS["ENTITY_ID_TIP"] = "Un ID de entidad num�rica. Ejemplo: para un ID de tarea es la 348 y el ID de la entidad es 348.";
$MESS["ENTITY_XML_ID_TIP"] = "Un ID entidad alfanum�rico (ID XML). Puede incluir caracteres alfanum�ricos y un gui�n bajo. Ejemplo: para un ID de tarea es la 348, la entidad ID alfanum�rico es TASKS_348.";
$MESS["PERMISSION_TIP"] = "Los permios externos sobreescriben los permisos de usuarios del foro. A < E < I < P < U < Y. A - acceso denegado; E - leer; I - responder; Q - moderar; U - editar; Y - acceso completo.";
?>