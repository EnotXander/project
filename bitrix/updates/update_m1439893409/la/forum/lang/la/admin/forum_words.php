<?
$MESS["FLT_WORDS"] = "Palabra";
$MESS["FLT_PATTERN"] = "Plantilla";
$MESS["FLT_REPLACEMENT"] = "Reemplaze";
$MESS["FLT_DESCRIPTION"] = "Descripci�n";
$MESS["FLT_PATTERN_CREATE"] = "Modo creaci�n patr�n";
$MESS["FLT_USE_IT"] = "Activar";
$MESS["FLT_ACT_ADD"] = "Agregar";
$MESS["FLT_ACT_EDIT"] = "Editar";
$MESS["FLT_ACT_DEL"] = "Borrar";
$MESS["FLT_ACT_GEN"] = "Generar plantilla";
$MESS["FLT_ACT_GEN_CONFIRM"] = "&nbsp;using&nbsp;";
$MESS["FLT_ACT_USE_IT_Y"] = "Si";
$MESS["FLT_ACT_USE_IT_N"] = "No";
$MESS["FLT_ACT_DEL_CONFIRM"] = "�Est�s seguro de que desea eliminar el registro?";
$MESS["FLT_NOT_UPDATE"] = "Acrtualizaci�n fall�.";
$MESS["FLT_NOT_DICTIONARY"] = "No se especific� el diccionario.";
$MESS["FLT_TITLE"] = "Diccionario de palabras";
$MESS["FLT_TITLE_NAV"] = "Diccionario";
$MESS["FLT_FLT_WORDS"] = "Palabra";
$MESS["FLT_FLT_TRNSL"] = "Conversi�n";
$MESS["FLT_FLT_PTTRN"] = "Patr�n";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Seleccionado:";
$MESS["MAIN_FIND"] = "B�scar";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Marcado:";
$MESS["MAIN_FIND_TITLE"] = "Introduzca el texto a buscar";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "eliminar";
?>