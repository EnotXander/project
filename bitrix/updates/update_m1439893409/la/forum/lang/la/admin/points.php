<?
$MESS["FORUM_P_POINTS"] = "Puntos";
$MESS["FORUM_P_ERROR_DEL"] = "Error al borrar el puntaje";
$MESS["FORUM_P_RANKS"] = "Puntajes";
$MESS["FORUM_P_MIN_POINTS"] = "N�mero m�nimo de puntos";
$MESS["FORUM_P_NAME"] = "Nombre";
$MESS["FORUM_P_VOTES"] = "Votos";
$MESS["FORUM_P_EDIT_DESC"] = "Modificar los par�metros de puntuaci�n";
$MESS["FORUM_P_DELETE_DESC"] = "Borrar";
$MESS["FORUM_P_DELETE_CONF"] = "�Est� seguro que desea eliminar este punto?";
$MESS["FPAN_UPDATE_ERROR"] = "Error al actualizar el registro";
$MESS["FPAN_ADD_NEW"] = "Nuevo rango";
$MESS["FPAN_ADD_NEW_ALT"] = "Haga clic para a�adir una nueva categor�a";
$MESS["FORUM_P_RATING_VOTES"] = "Votos requeridos normalizados";
$MESS["FORUM_P_RATING_VALUE"] = "Autoridad necesaria";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Seleccionado:";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Marcado:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "eliminar";
?>