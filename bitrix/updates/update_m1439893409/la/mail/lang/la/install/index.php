<?
$MESS ['MAIL_MODULE_NAME'] = "Mail";
$MESS ['MAIL_MODULE_DESC'] = "El m�dulo mail, est� dirigido a recibir mensajes, filtrar y a realizar acciones espec�ficas.";
$MESS ['MAIL_INSTALL_TITLE'] = "Desintalaci�n del M�dulo MAIL";
$MESS ['MAIL_UNINSTALL_WARNING'] = "Peligro! El m�dulo ser� desinstalado de su sistema.";
$MESS ['MAIL_UNINSTALL_SAVEDATA'] = "Para guardar los datos guardados en las tablas de la base de datos, haga check en &quot;Guardar Tablas&quot;.";
$MESS ['MAIL_UNINSTALL_SAVETABLE'] = "Tablas guardadas";
$MESS ['MAIL_UNINSTALL_DEL'] = "Desinstalar";
$MESS ['MAIL_UNINSTALL_ERROR'] = "Errores de desinstalaci�n:";
$MESS ['MAIL_UNINSTALL_COMPLETE'] = "Desinstalaci�n completa.";
$MESS ['MAIL_INSTALL_BACK'] = "Regresar a la secci�n administrativa del m�dulo ";
?>