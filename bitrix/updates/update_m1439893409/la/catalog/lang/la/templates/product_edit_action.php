<?
$MESS["ERROR_EMPTY_BASE_PRICE"] = "El campo &quot;Precio Base&quot; es requerido";
$MESS["ERROR_EMPTY_BASE_CURRENCY"] = "El campo &quot;Moneda del precio base&quot; es requerido";
$MESS["ERROR_DELETE_PRICE"] = "Error al eliminar el precio";
$MESS["ERROR_UPDATING_PRICE"] = "Error al actualizar el precio";
$MESS["ERROR_ADDING_PRICE"] = "Error al agregar el precio";
$MESS["C2IT_INTERNAL_ERROR"] = "Error interno";
$MESS["C2IT_NO_BASE_TYPE"] = "Tipo de precio base no encontrado";
$MESS["C2IT_ERROR_BOUND_LEFT"] = "Quantity bounds error: lower bound \"#BORDER#\" must be above zero (for the first range)";
$MESS["C2IT_ERROR_BOUND_RIGHT"] = "Quantity bounds error: higher bound \"#BORDER#\" must be above zero (for the last range)";
$MESS["C2IT_ERROR_BOUND"] = "Quantity bounds error: range #DIAP# is incorrect";
$MESS["C2IT_ERROR_BOUND_PRICE"] = "Entrada de precio inv�lido: Ning�n precio est� especificado para el rango ";
$MESS["C2IT_ERROR_PRICE"] = "La base del precio no est� especificada";
$MESS["C2IT_ERROR_BOUND_CROSS"] = "Quantity bounds error: ranges #DIAP1# and #DIAP2# overlap";
$MESS["C2IT_ERROR_BOUND_MISS"] = "Ingreso del rango de la cantidad inv�lida: ning�n precio est� especificado para el rango #DIAP1#";
$MESS["C2IT_ERROR_BOUND_MISS_TOP"] = "Ingreso del rango de la cantidad inv�lida: ning�n precio est� especificado para la cantidad arriba de #BORDER#";
$MESS["C2IT_ERROR_PRPARAMS"] = "Error al actualizar el precio con ID #ID#";
$MESS["C2IT_ERROR_SAVEPRICE"] = "Error al guardar precio #PRICE#";
$MESS["C2IT_ERROR_NO_PRICE"] = "No existen precios";
$MESS["C2IT_ERROR_SAVE_BARCODE"] = "Un producto con este c�digo de barras ya existe.";
$MESS["C2IT_ERROR_SAVE_MULTIBARCODE"] = "Este producto tiene varias instancias, cada uno con un c�digo de barras �nico.";
$MESS["C2IT_ERROR_USE_MULTIBARCODE"] = "Este producto est� presente en el almac�n sin c�digos de barras �nicos. Elimine este producto de todos los almacenes antes de activar los c�digos de barras �nicos.";
?>