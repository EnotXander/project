<?
$MESS["CAT_EDIT_RECORD"] = "Modificar tipo de precio # #ID#";
$MESS["CAT_NEW_RECORD"] = "Nuevo tipo de precio";
$MESS["NAME"] = "Nombre";
$MESS["CODE"] = "C�digo:";
$MESS["CAT_GROUPS"] = "Grupos dde usuarios permitidos para ver este tipo de precios:";
$MESS["CAT_GROUPS_BUY"] = "Grupo de usuarios permitidos a comprar con este tipo de precio:";
$MESS["BASE"] = "Base:";
$MESS["BASE_COMMENT"] = "S�lo un tipo de precios se pueden fijar como BASE. Si establece este par�metro para el precio tipo ser� autom�ticamente desactivada de la actual BASE tipo de precio.";
$MESS["BASE_COMMENT_Y"] = "Debe haber por lo menos un tipo de precio BASE. Si desea cambiar el valor por defecto BASE del precio fijado por el tipo de bandera con el tipo de precio que quiere sea utilizado como BASE.";
$MESS["BASE_YES"] = "Si";
$MESS["ERROR_UPDATING_TYPE"] = "Error mientras se actualizaba el tipo de precio.";
$MESS["ERROR_ADDING_TYPE"] = "Error mientras se adicionaba el tipo de precio.";
$MESS["CGEN_2FLIST"] = "Tipos de precio";
$MESS["CGEN_NEW_GROUP"] = "Agregar un nuevo tipo de precio";
$MESS["CGEN_DELETE_GROUP"] = "Eliminar tipo de precio";
$MESS["CGEN_DELETE_GROUP_CONFIRM"] = "�Est� seguro que desea eliminar este tipo de precio?";
$MESS["CGEN_TAB_GROUP"] = "Tipo de precio";
$MESS["CGEN_TAB_GROUP_DESCR"] = "Par�metros del Tipo de precio";
$MESS["SORT2"] = "Modificar orden:";
$MESS["BT_CAT_GROUP_EDIT_FIELDS_XML_ID"] = "ID externo";
$MESS["CAT_FEATURE_NOT_ALLOW"] = "La edici�n no puede utilizar esta funci�n.";
?>