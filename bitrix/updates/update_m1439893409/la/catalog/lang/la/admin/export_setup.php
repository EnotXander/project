<?
$MESS["CES_ERROR_NO_FILE"] = "Archivo de exportaci�n no fijado.";
$MESS["CES_ERROR_NO_ACTION"] = "Acci�n no fijada.";
$MESS["CES_ERROR_FILE_NOT_EXIST"] = "Archivo de exportaci�n no encontrado.";
$MESS["CES_ERROR_ADD_PROFILE"] = "Error al adicionar perfil.";
$MESS["CES_ERROR_ADD2CRON"] = "Error al instalar el archivo de configuraci�n con cron:";
$MESS["CES_ERROR_UNKNOWN"] = "error desconocido.";
$MESS["CES_ERROR_NO_PROFILE1"] = "Perfil #";
$MESS["CES_ERROR_NO_PROFILE2"] = "no encontrado.";
$MESS["CES_ERROR_SAVE_PROFILE"] = "Error guardando perfil de ecportaci�n.";
$MESS["CES_ERROR_NO_SETUP_FILE"] = "Archivo de configuraci�n de exportaci�n no encontrado.";
$MESS["TITLE_EXPORT_PAGE"] = "Configurar exportaci�n.";
$MESS["CES_ERRORS"] = "Error mientras se realizaba la operaci�n:";
$MESS["CES_SUCCESS"] = "Operaci�n completada satisfactoriamente.";
$MESS["CES_EXPORT_FILE"] = "Archivo de exportaci�n de datos:";
$MESS["CES_EXPORTER"] = "Exportados";
$MESS["CES_ACTIONS"] = "Acciones";
$MESS["CES_PROFILE"] = "Perfil";
$MESS["CES_IN_MENU"] = "En el men�";
$MESS["CES_IN_AGENT"] = "En agentes";
$MESS["CES_IN_CRON"] = "sobre CRON";
$MESS["CES_USED"] = "�ltima ejecuci�n";
$MESS["CES_ADD_PROFILE_DESCR"] = "Agregar un nuevo perfil de exportaci�n";
$MESS["CES_ADD_PROFILE"] = "Agregar perfil";
$MESS["CES_DEFAULT"] = "Por defecto";
$MESS["CES_NOTES6"] = "Para ver la lista de tareas instaladas actualemte, ejecute el comanado";
$MESS["CES_NOTES7"] = "Para remover todas las tareas asignadas al CRON, ejecute el comando";
$MESS["CES_NOTES8"] = "Lista actual de tareas del CRON";
$MESS["CES_NOTES10"] = "Atenci�n! Esto tambi�n remover� cualquier tarea en el archivo de configuraci�n.";
$MESS["CES_NOTES11"] = "El archivo que lanzar� el script de exportaci�n para ejecutar las tareas con el cron es";
$MESS["CES_NOTES12"] = "ASeg�rese de que el archivo contiene la ruta correcta al PHP y a la raiz del sitio";
$MESS["export_setup_cat"] = "El script de exportaci�n est� en la carpeta:";
$MESS["export_setup_script"] = "El scriot de exportaci�n";
$MESS["export_setup_name"] = "Nombre";
$MESS["export_setup_file"] = "Archivo";
$MESS["export_setup_begin"] = "Iniciar exportaci�n de datos";
$MESS["CES_NO"] = "No";
$MESS["CES_YES"] = "Si";
$MESS["CES_RUN_INTERVAL"] = "Periodo antes del lanzamiento (horas):";
$MESS["CES_SET"] = "Instalar";
$MESS["CES_DELETE"] = "Eliminar";
$MESS["CES_CLOSE"] = "Cerrar";
$MESS["CES_OR"] = "o";
$MESS["CES_RUN_TIME"] = "Lanzar hora:";
$MESS["CES_PHP_PATH"] = "Ruta a php:";
$MESS["CES_AUTO_CRON"] = "Fijar autom�ticamente:";
$MESS["CES_AUTO_CRON_DEL"] = "Eliminar autom�ticamente:";
$MESS["CES_RUN_EXPORT_DESCR"] = "Iniciar exportaci�n de datos";
$MESS["CES_RUN_EXPORT"] = "Exportar";
$MESS["CES_TO_LEFT_MENU_DESCR"] = "Agregar link al menu izquierdo";
$MESS["CES_TO_LEFT_MENU_DESCR_DEL"] = "Eliminar link del men� izquierdo";
$MESS["CES_TO_LEFT_MENU"] = "Agregar al men�";
$MESS["CES_TO_LEFT_MENU_DEL"] = "Eliminar del men�";
$MESS["CES_TO_AGENT_DESCR"] = "Crear agente para lanzamiento autom�tico";
$MESS["CES_TO_AGENT_DESCR_DEL"] = "Eliminar agente para lanzamiento autom�tico";
$MESS["CES_TO_AGENT"] = "Crear agente";
$MESS["CES_TO_AGENT_DEL"] = "Eliminar agente";
$MESS["CES_TO_CRON_DESCR"] = "USar cron para lanzamiento autom�tico";
$MESS["CES_TO_CRON_DESCR_DEL"] = "Remover del cron";
$MESS["CES_TO_CRON"] = "Usar cron";
$MESS["CES_TO_CRON_DEL"] = "Detener cron";
$MESS["CES_SHOW_VARS_LIST_DESCR"] = "Mostrar lista de variables para este perfil de exportaci�n";
$MESS["CES_SHOW_VARS_LIST"] = "Lista de variables";
$MESS["CES_DELETE_PROFILE_DESCR"] = "Eliminar este perfil";
$MESS["CES_DELETE_PROFILE_CONF"] = "�Est� seguro que quiere eliminar este perfil?";
$MESS["CES_DELETE_PROFILE"] = "Eliminar perfil";
$MESS["CES_NOTES1"] = "Agents are PHP functions which are run periodically at a given interval. Every time a page is requested, the system automatically checks for agents that need to be executed and runs them. It is not recommended to assign lengthy or large export jobs to agents. You should use the cron daemon for this purpose.";
$MESS["CES_NOTES2"] = "The cron daemon is only available on UNIX-based servers.";
$MESS["CES_NOTES3"] = "The cron daemon works in the background mode and runs the assigned tasks at the specified time. You need to specify the configuration file to add an export operation to the task list";
$MESS["CES_NOTES4"] = "in cron. This file contains instructions for the export operations . After you have changed the cron tasks set, you have to install the configuration file again.";
$MESS["CES_NOTES5"] = "PAra fijar el archivo de confiduraci�n usted se debe conectar a su sitio v�a SSH, SSH2 o cualquier otro protocolo similar que su proveedor soporte para operaciones en el shelll remoto. In command line, run the command";
$MESS["CES_ERROR_NOT_AGENT"] = "Este perfil no puede ser usado por agentes, porque es usado por defecto y un archivo de configuraci�n est� definido para la actual exportaci�n.";
$MESS["CES_ERROR_NOT_CRON"] = "Este perfil no puede ser usado con cron, porque es usado por defecto y un archivo de configuraci�n est� definido para la actual exportaci�n.";
$MESS["CES_EDIT_PROFILE"] = "Editar";
$MESS["CES_EDIT_PROFILE_ERR_DEFAULT"] = "El perfil predeterminado no puede ser modificado. ";
$MESS["CES_EDIT_PROFILE_ERR_ID_BAD"] = "El ID del perfil est� errado.";
$MESS["CES_ERROR_PROFILE_UPDATE"] = "Error al actualizar el perfil.";
$MESS["CES_COPY_PROFILE"] = "Copiar";
$MESS["CES_COPY_PROPFILE_DESCR"] = "Copiar Perfil";
$MESS["CES_COPY_PROFILE_ERR_DEFAULT"] = "El perfil predeterminado no puede ser copiado. ";
$MESS["CES_ERROR_COPY_PROFILE"] = "Hubo un error al copiar el perfil.";
$MESS["CES_EDIT_PROPFILE_DESCR"] = "Editar el Perfil";
$MESS["CES_EDIT_PROFILE_ERR_ID_ABSENT"] = "El ID del perfil no est� especificado. ";
$MESS["CES_NEED_EDIT"] = "el perfil debe configurarse";
$MESS["CES_ERROR_BAD_FILENAME"] = "El nombre del archivo de importaci�n contiene caracteres no v�lidos.";
$MESS["CES_ERROR_BAD_FILENAME2"] = "El nombre de archivo de secuencia de comandos de exportaci�n contiene caracteres no v�lidos.";
$MESS["CES_ERROR_BAD_EXPORT_FILENAME"] = "El nombre de archivo de exportaci�n contiene caracteres no v�lidos.";
$MESS["CES_CREATED_BY"] = "Creado por";
$MESS["CES_DATE_CREATE"] = "Creado en ";
$MESS["CES_MODIFIED_BY"] = "Modificado por";
$MESS["CES_TIMESTAMP_X"] = "Modificado en";
$MESS["CES_DEFAULT_PROFILE"] = "sistema";
$MESS["CES_CRON_AGENT_ERRORS"] = "Los perfiles est�n configurados para funcionar con el agente y cron. No se recomienda utilizar ambos simult�neamente.";
?>