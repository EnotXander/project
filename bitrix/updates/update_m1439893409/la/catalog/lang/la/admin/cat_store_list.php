<?
$MESS["CAT_STORE_INDEX_TITLE"] = "Lista de ubicaciones de almac�n";
$MESS["TITLE"] = "Nombre";
$MESS["ADDRESS"] = "Direcci�n";
$MESS["DESCRIPTION"] = "Descripci�n";
$MESS["GPS_S"] = "Longitud GPS";
$MESS["GPS_N"] = "Latitud GPS";
$MESS["STORE_ADD_NEW"] = "Agregar";
$MESS["STORE_ADD_NEW_ALT"] = "Agregar nuevo almac�n";
$MESS["EDIT_STORE_ALT"] = "Editar";
$MESS["DELETE_STORE_ALT"] = "Eliminar";
$MESS["DELETE_STORE_CONFIRM"] = "�Est� seguro que desea eliminar el almac�n?";
$MESS["STORE_TITLE"] = "Almacenes";
$MESS["STORE_IMAGE"] = "Imagen";
$MESS["PHONE"] = "Tel�fono";
$MESS["SCHEDULE"] = "horas de oficina";
$MESS["STORE_ACTIVE"] = "Activo";
$MESS["group_admin_nav"] = "Almacenes";
$MESS["USER_ID"] = "Creado por";
$MESS["DATE_CREATE"] = "Fecha de creaci�n";
$MESS["MODIFIED_BY"] = "Modificado por";
$MESS["DATE_MODIFY"] = "Fecha de modificaci�n";
$MESS["CSTORE_SORT"] = "Ordenar";
$MESS["STORE_SITE_ID"] = "Sitio Web";
$MESS["SHIPPING_CENTER"] = "Centro de env�os";
$MESS["ISSUING_CENTER"] = "Lugar de entrega";
?>