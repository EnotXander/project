<?
$MESS["CONTRACTOR_PAGE_TITLE"] = "Proveedores";
$MESS["CONTRACTOR_ADD_NEW"] = "Agregar proveedor";
$MESS["CONTRACTOR_ADD_NEW_ALT"] = "Agregar nuevos proveedores";
$MESS["EDIT_CONTRACTOR_ALT"] = "Editar";
$MESS["DELETE_CONTRACTOR_ALT"] = "Eliminar";
$MESS["DELETE_CONTRACTOR_CONFIRM"] = "�Est� seguro de que desea eliminar el proveedor?";
$MESS["group_admin_nav"] = "Proveedores";
$MESS["CONTRACTOR_TITLE"] = "Nombre";
$MESS["CONTRACTOR_POST_INDEX"] = "C�digo postal";
$MESS["CONTRACTOR_INN"] = "ID del Contribuyente";
$MESS["CONTRACTOR_PHONE"] = "Tel�fono";
$MESS["CONTRACTOR_EMAIL"] = "Correo electr�nico";
$MESS["CONTRACTOR_TYPE"] = "Tipo de proveedor";
$MESS["CONTRACTOR_INDIVIDUAL"] = "Persona natural";
$MESS["CONTRACTOR_JURIDICAL"] = "Persona jur�dica";
$MESS["CONTRACTOR_PERSON_NAME"] = "Primer nombre";
$MESS["CONTRACTOR_PERSON_LASTNAME"] = "Apellido";
$MESS["CONTRACTOR_ADDRESS"] = "Direcci�n";
$MESS["ERROR_UPDATING_REC"] = "Error al actualizar el registro.";
$MESS["ERROR_DELETING_TYPE"] = "Error al eliminar el registro.";
?>