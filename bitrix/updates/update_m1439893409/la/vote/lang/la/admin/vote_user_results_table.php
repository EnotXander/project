<?
$MESS["VOTE_CHANNEL"] = "Canal:";
$MESS["VOTE_VOTE"] = "Encuesta:";
$MESS["VOTE_PAGE_TITLE"] = "Voto # #ID#";
$MESS["VOTE_USER_LIST"] = "Lista del visitante";
$MESS["VOTE_RESULTS_LIST"] = "Lista de voto";
$MESS["VOTE_GUEST"] = "Visitante:";
$MESS["VOTE_RESULT_NOT_FOUND"] = "El Voto no fue encontrado";
$MESS["VOTE_NOT_FOUND"] = "La encuesta no fue encontrada";
$MESS["VOTE_EDIT_USER"] = "Perfil del usuario";
$MESS["VOTE_NOT_AUTHORIZED"] = "No autorizado";
$MESS["VOTE_DATE"] = "Fecha:";
$MESS["VOTE_SESSION"] = "Sesi�n:";
$MESS["VOTE_VALID"] = "V�lido:";
$MESS["VOTE_PARAMS"] = "Par�metros";
$MESS["VOTE_PARAMS_TITE"] = "Configuraciones del par�metro";
$MESS["VOTE_F_IP"] = "Direcci�n del IP:";
$MESS["VOTE_VOTE_IS_ACTIVE"] = "La encuesta est� <b>active</b>";
$MESS["VOTE_VOTE_IS_NOT_ACTIVE"] = "La encuesta no est� activa";
$MESS["VOTE_VOTES"] = "Total de votos:";
$MESS["VOTE_ENLARGE"] = "Extender";
$MESS["VOTE_START_DATE"] = "Fecha de inicio:";
$MESS["VOTE_END_DATE"] = "Fecha de t�rmino:";
?>