<?
$MESS["LM_POPUP_TITLE"] = "T�tulo";
$MESS["LM_POPUP_TAB_LAST"] = "�ltimo";
$MESS["LM_POPUP_TAB_SG"] = "Mis grupos";
$MESS["LM_POPUP_TAB_STRUCTURE"] = "Los empleados y departamentos";
$MESS["LM_POPUP_TAB_LAST_USERS"] = "Personas";
$MESS["LM_POPUP_TAB_LAST_SG"] = "Grupos";
$MESS["LM_POPUP_TAB_LAST_STRUCTURE"] = "Departamentos";
$MESS["LM_POPUP_CHECK_STRUCTURE"] = "Todos los empleados del Departamento y Subdepartamento";
$MESS["LM_SEARCH_PLEASE_WAIT"] = "Buscando... Por favor espera";
$MESS["LM_PLEASE_WAIT"] = "Cargando... Por favor espera";
$MESS["LM_POPUP_TAB_LAST_CONTACTS"] = "Contactos";
$MESS["LM_POPUP_TAB_LAST_COMPANIES"] = "Compa��as";
$MESS["LM_POPUP_TAB_LAST_LEADS"] = "Prospectos";
$MESS["LM_POPUP_TAB_LAST_DEALS"] = "Negociaciones";
$MESS["LM_POPUP_TAB_STRUCTURE_EXTRANET"] = "Usuarios externos";
$MESS["LM_CREATE_SONETGROUP_BUTTON_CREATE"] = "Crear";
$MESS["LM_CREATE_SONETGROUP_BUTTON_CANCEL"] = "Cancelar";
$MESS["LM_CREATE_SONETGROUP_TITLE"] = "No existe un grupo con ese nombre. �Quiere crear el grupo \"#TITLE#\"?";
$MESS["LM_EMPTY_LIST"] = "No hay elementos";
$MESS["LM_POPUP_WAITER_TEXT"] = "Buscar otros resultados...";
?>