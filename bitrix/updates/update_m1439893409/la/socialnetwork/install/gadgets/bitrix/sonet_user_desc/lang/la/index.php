<?
$MESS["GD_SONET_USER_DESC_CONTACT_TITLE"] = "Informaci�n de contacto";
$MESS["GD_SONET_USER_DESC_CONTACT_UNSET"] = "La informaci�n del contacto no ha sido proporcionada.";
$MESS["GD_SONET_USER_DESC_CONTACT_UNAVAIL"] = "La informaci�n del contacto no est� disponible.";
$MESS["GD_SONET_USER_DESC_PERSONAL_TITLE"] = "Detalles personales";
$MESS["GD_SONET_USER_DESC_PERSONAL_UNAVAIL"] = "La informaci�n personal no est� disponible.";
$MESS["GD_SONET_USER_DESC_VOTE"] = "Votaci�n";
$MESS["GD_SONET_USER_DESC_MANAGER"] = "Subordinado A:";
$MESS["GD_SONET_USER_DESC_EMPLOYEES"] = "A cargo de:";
$MESS["GD_SONET_USER_DESC_EMPLOYEES_NUM"] = "Empleados";
$MESS["GD_SONET_USER_DESC_SECURITY_TITLE"] = "Seguridad";
$MESS["GD_SONET_USER_DESC_OTP_AUTH"] = "Autenticaci�n de dos pasos";
$MESS["GD_SONET_USER_DESC_OTP_NO_DAYS"] = "siempre";
$MESS["GD_SONET_USER_DESC_OTP_ACTIVE"] = "En";
$MESS["GD_SONET_USER_DESC_OTP_NOT_ACTIVE"] = "Apagado";
$MESS["GD_SONET_USER_DESC_OTP_CHANGE_PHONE"] = "Configuraci�n de nuevo tel�fono";
$MESS["GD_SONET_USER_DESC_OTP_ACTIVATE"] = "Permitir";
$MESS["GD_SONET_USER_DESC_OTP_DEACTIVATE"] = "Desactivar";
$MESS["GD_SONET_USER_DESC_OTP_PROROGUE"] = "Retraso";
$MESS["GD_SONET_USER_DESC_OTP_LEFT_DAYS"] = "(se habilitar� en #NUM#)";
$MESS["GD_SONET_USER_DESC_OTP_CODES"] = "C�digos de copia de seguridad";
$MESS["GD_SONET_USER_DESC_OTP_CODES_SHOW"] = "Ver";
$MESS["GD_SONET_USER_DESC_OTP_PASSWORDS"] = "Contrase�as de aplicaci�n";
$MESS["GD_SONET_USER_DESC_OTP_PASSWORDS_SETUP"] = "Configuraci�n";
?>