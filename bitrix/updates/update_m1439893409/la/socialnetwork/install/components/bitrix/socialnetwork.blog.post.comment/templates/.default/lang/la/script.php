<?
$MESS["BPC_TEXT_ENTER_URL"] = "Introduzca la direcci�n completa (URL)";
$MESS["BPC_TEXT_ENTER_URL_NAME"] = "Introduzca el texto que se muestra como un hiperv�nculo";
$MESS["BPC_TEXT_ENTER_IMAGE"] = "Introduzca la direcci�n de la imagen completa (URL)";
$MESS["BPC_LIST_PROMPT"] = "Introducir el elemento de la lista. Haga clic en 'Cancelar' o espacio para completar la lista";
$MESS["BPC_ERROR_NO_URL"] = "Por favor introduzca la direcci�n (URL)";
$MESS["BPC_ERROR_NO_TITLE"] = "Introduce un t�tulo";
?>