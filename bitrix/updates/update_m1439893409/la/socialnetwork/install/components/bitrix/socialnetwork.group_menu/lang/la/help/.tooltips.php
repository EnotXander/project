<?
$MESS["PATH_TO_USER_TIP"] = "La ruta a la p�gina del perfil. Ejemplo: sonet_user.php?page=user&user_id=#user_id#.";
$MESS["PATH_TO_USER_EDIT_TIP"] = "La ruta a la p�gina de edici�n del perfil. Ejemplo: sonet_user_edit.php?page=user&user_id=#user_id#&mode=edit.";
$MESS["PAGE_VAR_TIP"] = "Especificar aqu� el nombre de la variable de la p�gina de social network.";
$MESS["USER_VAR_TIP"] = "Especificar aqu� el nombre de la variable del ID del usuario de social network. ";
$MESS["ID_TIP"] = "Especificar el c�digo que eval�a el ID del usuario.";
$MESS["SET_TITLE_TIP"] = "Al comprobar esta opci�n usted podr� fijar el t�tulo de la p�gina a <i>\"user name\"</i> <b>User Profile</b>.";
$MESS["USER_PROPERTY_TIP"] = "Seleccione aqu� las propiedades adicionales que podr�an ser mostradas en el perfil del usuario.";
?>