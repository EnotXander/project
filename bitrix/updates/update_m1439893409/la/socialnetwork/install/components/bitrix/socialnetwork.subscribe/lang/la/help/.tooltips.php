<?
$MESS["PATH_TO_USER_TIP"] = "La ruta hacia una p�gina de perfil de usuario. Ejemplo: sonet_user.php?page=user&user_id=#user_id#. ";
$MESS["PATH_TO_USER_EDIT_TIP"] = "La ruta hacia una p�gina de perfil del editor. Ejemplo: sonet_user_edit.php?page=user&user_id=#user_id#&mode=edit.";
$MESS["PAGE_VAR_TIP"] = "Especifique aqu� el nombre de una variable a la p�gina de usuario de Social Network.";
$MESS["USER_VAR_TIP"] = "Especifique aqu� el nombre de una variable a la que el ID de la red social de la p�gina se pasar�.";
$MESS["ID_TIP"] = "Especifica el c�digo que se eval�a como un identificador de usuario.";
$MESS["SET_TITLE_TIP"] = "Si la opci�n est� activa, el t�tulo de la p�gina se va a establecer a <nobr><b>Perfil para</b> <i>\"nombre del usuario\"</i>.</nobr>";
$MESS["USER_PROPERTY_TIP"] = "Seleccione aqu� propiedades adicionales que se mostrar� en el perfil de usuario.";
?>