<?
$MESS["PATH_TO_USER_TIP"] = "La ruta hacia una p�gina de perfil de usuario. Ejemplo: sonet_user.php?page=user&user_id=#user_id#.";
$MESS["PATH_TO_USER_EDIT_TIP"] = "La ruta a un perfil de usuario editor de la p�gina. Ejemplo:";
$MESS["PAGE_VAR_TIP"] = "Especificar aqu� el nombre de la variable de la p�gina de social network";
$MESS["USER_VAR_TIP"] = "Especifique aqu� el nombre de la variable del ID del usuario de social network";
$MESS["ID_TIP"] = "Especifica el c�digo que se eval�a como un identificador de usuario.";
$MESS["SET_TITLE_TIP"] = "Al seleccionar esta opci�n aparecer� el t�tulo de la p�gina a <i>\"user name\"</i> <b>Perfil del usuario</b>.";
$MESS["USER_PROPERTY_TIP"] = "Seleccione aqu� propiedades adicionales que se mostrar� en el perfil de usuario.";
?>