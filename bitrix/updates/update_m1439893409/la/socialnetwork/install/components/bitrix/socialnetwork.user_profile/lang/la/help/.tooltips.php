<?
$MESS["PATH_TO_USER_TIP"] = "La ruta a la p�gina del perfil del usuario. Ejemplo: sonet_user.php?page=user&user_id=#user_id#.";
$MESS["PATH_TO_USER_EDIT_TIP"] = "La ruta a la p�gina del editor de perfil del usuario. Ejemplo: sonet_user_edit.php?page=user&user_id=#user_id#&mode=edit.";
$MESS["PAGE_VAR_TIP"] = "Especificar aqu� el nombre de la variable de la p�gina de social network.";
$MESS["USER_VAR_TIP"] = "Especificar aqu� el nombre de la variable del ID del usuario de social network.";
$MESS["ID_TIP"] = "Especifica el c�digo que eval�a el ID  del usuario.";
$MESS["SET_TITLE_TIP"] = "Eligiendo esta opci�n podr� establecer el t�tulo a <i>\"nombre del usuario\"</i> <b>perfil del usuario</b>.";
$MESS["USER_PROPERTY_TIP"] = "Seleccione aqu� las propiedades adicionales que podr�n ser mostrados en el perfil del usuario.";
?>