<?
$MESS["FORUM_LIST_PROMPT"] = "Introduzca el elemento de la lista. Haga clic en \"Cancelar\" o deje el espacio en el s�mbolo para completar la lista";
$MESS["FORUM_TEXT_ENTER_URL"] = "Introduzca la URL completa del sitio";
$MESS["FORUM_TEXT_ENTER_URL_NAME"] = "Introduzca el nombre del sitio";
$MESS["FORUM_TEXT_ENTER_IMAGE"] = "Introduzca la ruta de la imagen completa (URL)";
$MESS["FORUM_ERROR_NO_URL"] = "Usted debe especificar la direcci�n (URL)";
$MESS["FORUM_ERROR_NO_TITLE"] = "Tiene que especificar el t�tulo";
$MESS["FORUM_HELP_BOLD"] = "Negrita (alt + b)";
$MESS["FORUM_HELP_ITALIC"] = "Cursiva (alt + i)";
$MESS["FORUM_HELP_UNDER"] = "Subrayado (alt + u)";
$MESS["FORUM_HELP_FONT"] = "Cara de la fuente";
$MESS["FORUM_HELP_COLOR"] = "Color de la fuente";
$MESS["FORUM_HELP_CLOSE"] = "Cerrar todas las etiquetas abiertas";
$MESS["FORUM_HELP_URL"] = "Hiperv�nculo (alt + h)";
$MESS["FORUM_HELP_IMG"] = "Adjuntar imagen (alt + g)";
$MESS["FORUM_HELP_QUOTE"] = "Cotizaci�n (alt + q)";
$MESS["FORUM_HELP_LIST"] = "Lista (alt + l)";
$MESS["FORUM_HELP_CODE"] = "C�digo (alt + p)";
$MESS["FORUM_HELP_CLICK_CLOSE"] = "Haga clic en el bot�n para cerrar la etiqueta";
$MESS["JERROR_NO_TOPIC_NAME"] = "S�rvase proporcionar mensaje t�tulo.";
$MESS["JERROR_NO_MESSAGE"] = "Por favor, proporcione su mensaje.";
$MESS["JERROR_MAX_LEN2"] = "s�mbolos. Total de s�mbolos:";
$MESS["JERROR_NO_RECIPIENT"] = "Por favor, especificar el destinatario.";
$MESS["FORUM_TRANSLIT_EN"] = "Convertir";
$MESS["JQOUTE_AUTHOR_WRITES"] = "escrib�o";
$MESS["FORUM_HELP_TRANSLIT"] = "Convertir a romanas trans (alt+t)";
$MESS["JERROR_MAX_LEN1"] = "La extensi�n m�xima del mensaje es";
?>