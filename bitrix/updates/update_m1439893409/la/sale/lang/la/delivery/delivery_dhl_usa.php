<?
$MESS ['SALE_DH_DHL_USA_NAME'] = "DHL (USA)";
$MESS ['SALE_DH_DHL_USA_DESCRIPTION'] = "Servicio de Delivery DHL (USA) ";
$MESS ['SALE_DH_DHL_USA_PROFILE_TITLE'] = "delivery";
$MESS ['SALE_DH_DHL_USA_PROFILE_DESCRIPTION'] = "entrega dentro de 1 - 4 d�as �tiles";
$MESS ['SALE_DH_DHL_USA_CONFIG_DELIVERY_TITLE'] = "Informaci�n del env�o";
$MESS ['SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE'] = "Informaci�n del equipaje";
$MESS ['SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_CP'] = "Su equipaje";
$MESS ['SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_EE'] = "Carta DHL Express";
$MESS ['SALE_DH_DHL_USA_ERROR_RESPONSE'] = "Respuesta del servidor no reconocida";
$MESS ['SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_OD'] = "Otros embalajes DHL ";
$MESS ['SALE_DH_DHL_USA_DESCRIPTION_INNER'] = "Manejador del servicio Delivery para <a href=\"http://www.dhl-usa.com\" target=\"_blank\">DHL (USA)</a>. Basado en una calculadora en l&iacute;nea. DHL (USA) entrega desde USA a cualquier pa&iacute;s alrederor del mundo. Requiere specificar el c&oacute;digoZIP en el m&oacute;dulo de configuraciones del e-Store.";
?>