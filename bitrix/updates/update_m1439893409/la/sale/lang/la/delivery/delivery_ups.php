<?
$MESS ['SALE_DH_UPS_NAME'] = "UPS";
$MESS ['SALE_DH_UPS_DESCRIPTION'] = "correo internacional";
$MESS ['SALE_DH_UPS_EXPRESS_TITLE'] = "UPS Express";
$MESS ['SALE_DH_UPS_EXPRESS_DESCRIPTION'] = "Entrega al d�a siguiente en oficina hasta 10:30 � 12:00 a la mayor�a de las direcciones Europeas.";
$MESS ['SALE_DH_UPS_EXPRESS_SAVER_DESCRIPTION'] = "Entrega al d�a siguiente en oficina a la mayor�a de las direcciones Europeas.";
$MESS ['SALE_DH_UPS_TARIFF_TITLE'] = "Tarifas";
$MESS ['SALE_DH_UPS_CONFIG_zones_csv'] = "Archivo CSV con zonas de entrega";
$MESS ['SALE_DH_UPS_CONFIG_export_csv'] = "Archivo CSV con tarifas de entrega";
$MESS ['SALE_DH_UPS_EXPRESS_SAVER_TITLE'] = "UPS Express Saver";
$MESS ['SALE_DH_UPS_DESCRIPTION_INNER'] = "C�lculo de la entrega de UPS. Usar cotizaciones de los archivos CSV suministrados por UPS, Inc de precios de entrega . Puede descargar archivos CSV para cualquier pa�s de <a href=\"http://www.ups.com\" target=\"_blank\"> Sitio web de UPS</ a> Y especificar su ubicaci�n en la prestaci�n de servicios manejador de propiedades. La direcci�n del e-store debe ser especificado en el <a href=\"/bitrix/admin/settings.php?mid=sale&lang=en\"> m�dulo de configuraci�n </a>.";
?>