<?
$MESS["BIGDATA_PERSONALIZATION"] = "Personalizaci�n";
$MESS["BIGDATA_CONVERT"] = "Venda m�s y vence a tus rivales!";
$MESS["BIGDATA_NUM_ONE"] = "Bitrix BigData<br>La primera tienda web existente.";
$MESS["BIGDATA_CONNECT"] = "Trae las ventas online y offline juntas en una interfaz de usuario simplificada.";
$MESS["BIGDATA_PLATFORM"] = "Integrado en Bitrix CMS.";
$MESS["BIGDATA_HOWTO_ENABLE"] = "�C�mo habilitamos Bitrix BigData? ";
$MESS["BIGDATA_ENABLED"] = "Bitrix BigData est� disponible";
$MESS["BIGDATA_DISABLED"] = "Bitrix bigdata est� desconectado. Permitirle utilizar sus servicios.";
$MESS["BIGDATA_INSTALLED"] = "Instale los catalog.bigdata.products widgets a la p�gina principal, a una lista de productos o p�gina de vista de producto, carrito de la compra o en la p�gina de pedido.";
$MESS["BIGDATA_UNINSTALLED"] = "Widget no est� instalado";
$MESS["BIGDATA_OBSERVE"] = "Resaltar las ventas de los productos recomendados en la p�gina de pedidos";
$MESS["BIGDATA_GO"] = "Ir";
$MESS["BIGDATA_DESC_1"] = "<strong>Bitrix BigData</strong> Servicio de personalizaci�n de los procesos los datos obtenidos de todas las tiendas web de Bitrix.";
$MESS["BIGDATA_DESC_2"] = "El servicio analiza las preferencias de los usuarios (intereses, art�culos pedidos, compara a los usuarios encontrando rasgos similares) mientras mantiene a los usuarios seguros y an�nimos.";
$MESS["BIGDATA_DESC_3"] = "El componente de personalizaci�n se puede agregar casi en cualquier lugar: en la p�gina principal, a la p�gina de la lista de productos o producto Vistos, a la cesta de compra o en la p�gina de pedido.";
?>