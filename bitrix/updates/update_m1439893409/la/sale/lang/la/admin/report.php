<?
$MESS["REPORT_MODULE_NOT_INSTALLED"] = "No est� instalado el m�dulo de informe.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "No est� instalado el m�dulo de cat�logo comercial.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "No est� instalado el m�dulo de bloques de informaci�n.";
$MESS["SALE_REPORT_TITLE"] = "Informes";
$MESS["SALE_REPORT_CREATE_DEFAULT"] = "Crear informes t�picos";
$MESS["SALE_REPORT_EMPTY_LIST"] = "No hay reportes.";
$MESS["SALE_REPORT_LIST_TITLE"] = "Nombre del informe";
$MESS["SALE_REPORT_LIST_CREATED_DATE"] = "Creado en";
$MESS["SALE_REPORT_LIST_ADD_REPORT"] = "Nuevo informe";
$MESS["SALE_REPORT_LIST_ADD_REPORT_TITLE"] = "Crea un nuevo informe en el Dise�ador de informes";
$MESS["SALE_REPORT_ERROR_GETREPORTLIST"] = "Error al obtener la lista de informe.";
$MESS["SALE_REPORT_ERROR_DELREPFROMLIST"] = "Error al eliminar el informe.";
$MESS["SALE_REPORT_LIST_ROW_ACTIONS_DELETE_TEXT"] = "Eliminar informe";
$MESS["SALE_REPORT_LIST_ROW_ACTIONS_EDIT_TEXT"] = "Editar el informe";
$MESS["SALE_REPORT_LIST_ROW_ACTIONS_COPY_TEXT"] = "Copia de informe";
$MESS["SALE_REPORT_LIST_ROW_ACTIONS_VIEW_TEXT"] = "Crear informe";
$MESS["REPORT_DELETE_CONFIRM"] = "�Confirmar eliminaci�n?";
$MESS["REPORT_UPDATE_14_5_2_MESSAGE"] = "Tenga en cuenta que las plantillas est�ndar son s�lo de lectura. Para personalizar un informe, utilice el bot�n Copiar para crear una nueva basada en una de las plantillas est�ndar o crear una nueva plantilla de informe desde cero.";
?>