<?
$MESS["SALE_SECTION_TITLE"] = "Ubicaciones";
$MESS["SALE_F_FILTER"] = "Filtro";
$MESS["SALE_F_COUNTRY"] = "Pa�s";
$MESS["SALE_ALL"] = "(todo)";
$MESS["SALE_F_CITY"] = "Ciudad";
$MESS["SALE_F_SUBMIT"] = "Fijar filtro";
$MESS["SALE_F_DEL"] = "Remover filtro";
$MESS["SALE_ADD"] = "Agregar";
$MESS["SALE_COUNTRY"] = "Pa�s";
$MESS["SALE_CITY"] = "Ciudad";
$MESS["SALE_SORT"] = "Clasi.";
$MESS["SALE_ACTION"] = "Acciones";
$MESS["SALE_EDIT"] = "Modificar";
$MESS["SALE_CONFIRM_DEL_MESSAGE"] = "�Est� seguro que desea eliminar esta ubicaci�n?";
$MESS["SALE_DELETE"] = "Eliminar";
$MESS["SALE_PRLIST"] = "Ubicaciones";
$MESS["ERROR_DELETING"] = "Error mientras se eliminaba.";
$MESS["SALE_EDIT_DESCR"] = "Modifcar preferencias de ubicaciones.";
$MESS["SALE_DELETE_DESCR"] = "Eliminar la ubicaci�n";
$MESS["SLAN_ADD_NEW"] = "Agregar una nueva ubicaci�n";
$MESS["SLAN_ADD_NEW_ALT"] = "Click para agregar una nueva ubicaci�n";
$MESS["SLAN_IMPORT"] = "Importar ubicaci�n";
$MESS["SLAN_IMPORT_ALT"] = "Importaci�n de ubicaci�n de base de datos de fuentes externas";
$MESS["SALE_F_REGION"] = "Regi�n";
$MESS["SALE_REGION"] = "Regi�n";
?>