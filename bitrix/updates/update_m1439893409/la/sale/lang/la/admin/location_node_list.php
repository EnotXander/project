<?
$MESS["SALE_MODULE_ACCES_DENIED"] = "Se le neg� acceso al m�dulo";
$MESS["SALE_LOCATION_L_ITEM_NOT_FOUND"] = "No se encontr� ubicaci�n con este ID.";
$MESS["SALE_LOCATION_L_ITEM_SAVE_ERROR"] = "No se puede actualizar la ubicaci�n #ITEM#";
$MESS["SALE_LOCATION_L_ITEM_DELETE_ERROR"] = "Error al eliminar la ubicaci�n";
$MESS["SALE_LOCATION_L_PAGES"] = "Ubicaciones";
$MESS["SALE_LOCATION_L_ADD_ITEM"] = "Agregar ubicaci�n";
$MESS["SALE_LOCATION_L_EDIT_ITEM"] = "Editar ubicaci�n";
$MESS["SALE_LOCATION_L_COPY_ITEM"] = "Copiar ubicaci�n";
$MESS["SALE_LOCATION_L_DELETE_ITEM"] = "Eliminar ubicaci�n";
$MESS["SALE_LOCATION_L_EDIT_PAGE_TITLE"] = "Ubicaciones";
$MESS["SALE_LOCATION_L_ITEM"] = "Ubicaci�n";
$MESS["SALE_LOCATION_L_CONFIRM_DELETE_ITEM"] = "�Seguro que quieres eliminar la ubicaci�n? Todas las ubicaciones secundarias se eliminar�n tambi�n.";
$MESS["SALE_LOCATION_L_ANY"] = "cualquier";
$MESS["SALE_LOCATION_L_VIEW_CHILDREN"] = "Ver ubicaciones secundarias";
$MESS["SALE_LOCATION_L_FROM_AND_TO"] = "rango";
?>