<?
$MESS["SALE_MODULE_ACCES_DENIED"] = "Se le neg� acceso al m�dulo";
$MESS["SALE_LOCATION_L_ITEM_NOT_FOUND"] = "No se encontr� ubicaci�n con este ID.";
$MESS["SALE_LOCATION_L_ITEM_SAVE_ERROR"] = "No se puede actualizar el tipo de ubicaci�n #ITEM#";
$MESS["SALE_LOCATION_L_ITEM_DELETE_ERROR"] = "Error al eliminar el tipo de ubicaci�n";
$MESS["SALE_LOCATION_L_PAGES"] = "Tipos";
$MESS["SALE_LOCATION_L_ADD_ITEM"] = "Agregar tipos";
$MESS["SALE_LOCATION_L_EDIT_ITEM"] = "Editar tipos";
$MESS["SALE_LOCATION_L_COPY_ITEM"] = "Copiar tipos";
$MESS["SALE_LOCATION_L_DELETE_ITEM"] = "Eliminar tipos";
$MESS["SALE_LOCATION_L_EDIT_PAGE_TITLE"] = "Tipos de ubicaci�n";
$MESS["SALE_LOCATION_L_ITEM"] = "Tipo";
$MESS["SALE_LOCATION_L_CONFIRM_DELETE_ITEM"] = "�Seguro que quieres eliminar el tipo de ubicaci�n?";
$MESS["SALE_LOCATION_L_FROM_AND_TO"] = "rango";
?>