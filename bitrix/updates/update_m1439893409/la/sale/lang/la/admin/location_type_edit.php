<?
$MESS["SALE_MODULE_ACCES_DENIED"] = "Se le neg� acceso al m�dulo";
$MESS["SALE_LOCATION_E_ITEM_EDIT"] = "Editar tipo de ubicaci�n: #ITEM_NAME#";
$MESS["SALE_LOCATION_E_ITEM_NEW"] = "Nuevo tipo de ubicaci�n";
$MESS["SALE_LOCATION_E_CANNOT_SAVE_ITEM"] = "Error al guardar tipo de ubicaci�n";
$MESS["SALE_LOCATION_E_CANNOT_UPDATE_ITEM"] = "Error al actualizar tipo de ubicaci�n";
$MESS["SALE_LOCATION_E_MAIN_TAB"] = "Tipo de ubicaci�n";
$MESS["SALE_LOCATION_E_MAIN_TAB_TITLE"] = "Par�metros de tipo";
$MESS["SALE_LOCATION_E_ITEM_NOT_FOUND"] = "No se ha encontrado tipo de ubicaci�n ";
$MESS["SALE_LOCATION_E_GO_BACK"] = "Volver";
$MESS["SALE_LOCATION_E_HEADING_NAME_ALL"] = "T�tulos dependientes del idioma";
$MESS["SALE_LOCATION_E_HEADING_NAME"] = "T�tulos dependientes del idioma (#LANGUAGE_ID#)";
?>