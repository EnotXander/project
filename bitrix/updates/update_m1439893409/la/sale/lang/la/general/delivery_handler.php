<?
$MESS["SALE_DH_ERROR_WRONG_HANDLER_FILE"] = "Controlador de secuencia de comandos incorrecta";
$MESS["SALE_DH_ERROR_HANDLER_NOT_INSTALLED"] = "El servicio de entrega no est� instalado";
$MESS["SALE_DH_CF_ERROR_P_RESTRICTIONS_WEIGHT"] = "Restricci�n de peso";
$MESS["SALE_DH_CF_ERROR_P_RESTRICTIONS_SUM"] = "Restricci�n en el importe de la orden";
$MESS["SALE_DH_CF_ERROR_P_TAX_RATE"] = "Precio de entrega marcado";
$MESS["SALE_DH_CF_ERROR_P_RESTRICTIONS_MAX_SIZE"] = "Longitud Max.";
$MESS["SALE_DH_CF_ERROR_P_RESTRICTIONS_DIMENSIONS_SUM"] = "Suma max. de dimensiones";
$MESS["SALE_DH_CF_ERROR_P_RESTRICTIONS_DIMENSION"] = "Dimensiones max. del paquete";
$MESS["SALE_DH_CF_ERROR_SORT"] = "Ordenar";
?>