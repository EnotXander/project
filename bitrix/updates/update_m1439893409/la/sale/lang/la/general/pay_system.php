<?
$MESS["SKGPS_EMPTY_NAME"] = "El nombre del sistema de pago no se especifica";
$MESS["SKGPS_EMPTY_CURRENCY"] = "La moneda del sistema de pago no se especifica";
$MESS["SKGPS_NO_SITE"] = "No se encuentra el sitio ##ID#";
$MESS["SKGPS_ORDERS_TO_PAYSYSTEM"] = "Error al borrar un registro. Aqu� hay ordenes que usan para este sistema de pago";
$MESS["SKGPS_PS_NOT_FOUND"] = "No se encontr� el sistema de pago";
?>