<?
$MESS["SPOD_DESC_YES"] = "Si";
$MESS["SPOD_DESC_NO"] = "No";
$MESS["SPOD_PATH_TO_LIST"] = "P�gina de la lista de las �rdenes";
$MESS["SPOD_PATH_TO_CANCEL"] = "P�gina de la cancelaci�n de la orden";
$MESS["SPOD_PATH_TO_PAYMENT"] = "P�gina de la integraci�n de sistema del pago ";
$MESS["SPOD_ID"] = "Identificaci�n de la orden ";
$MESS["SPOD_PROPS_NOT_SHOW"] = "Ocultar propiedades de tipo de pagador";
$MESS["SPOD_SHOW_ALL"] = "(mostrar todo)";
$MESS["SPOD_ACTIVE_DATE_FORMAT"] = "Formato de fecha";
$MESS["SPOD_CACHE_GROUPS"] = "Respetar permisos de acceso";
$MESS["SPOD_PARAM_PREVIEW_PICTURE_WIDTH"] = "Ancho m�ximo para el thumbnail, px";
$MESS["SPOD_PARAM_PREVIEW_PICTURE_HEIGHT"] = "Alto m�ximo para el thumbnail, px";
$MESS["SPOD_PARAM_DETAIL_PICTURE_WIDTH"] = "Ancho m�ximo para la imagen de detalle, px";
$MESS["SPOD_PARAM_DETAIL_PICTURE_HEIGHT"] = "Alto m�ximo para la imagen de detalle, px";
$MESS["SPOD_PARAM_CUSTOM_SELECT_PROPS"] = "Propiedades extra para el Block de Informaci�n";
$MESS["SPOD_PARAM_RESAMPLE_TYPE"] = "Escalar imagen";
$MESS["SPOD_PARAM_RESAMPLE_TYPE_BX_RESIZE_IMAGE_EXACT"] = "Cortar";
$MESS["SPOD_PARAM_RESAMPLE_TYPE_BX_RESIZE_IMAGE_PROPORTIONAL"] = "Restringir proporciones";
$MESS["SPOD_PARAM_RESAMPLE_TYPE_BX_RESIZE_IMAGE_PROPORTIONAL_ALT"] = "Restringir proporciones, mantener calidad";
?>