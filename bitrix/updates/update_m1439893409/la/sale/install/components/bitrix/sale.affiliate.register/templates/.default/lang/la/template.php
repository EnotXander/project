<?
$MESS ['SPCR1_UNACTIVE_AFF'] = "Eres el afiliado registrado pero tu cuenta est� inactiva. Entrar en contacto por favor con la administraci�n para m�s informaci�n. ";
$MESS ['SPCR1_IF_REG'] = "ya registrado";
$MESS ['SPCR1_IF_NOT_REG'] = "No registrado";
$MESS ['SPCR1_IF_REMEMBER'] = "Incorporar por favor tu conexi�n y contrase�a:";
$MESS ['SPCR1_LOGIN'] = "Conexi�n ";
$MESS ['SPCR1_PASSWORD'] = "Contrase�a ";
$MESS ['SPCR1_FORG_PASSWORD'] = "�Se olvid� de contrase�a? ";
$MESS ['SPCR1_NEXT'] = "Continuar ";
$MESS ['SPCR1_NAME'] = "Nombre";
$MESS ['SPCR1_LASTNAME'] = "Apellido";
$MESS ['SPCR1_PASS_CONF'] = "Confirmar la contrase�a ";
$MESS ['SPCR1_CAPTCHA'] = "Prevenci�n automatizada del registro ";
$MESS ['SPCR1_CAPTCHA_WRD'] = "Incorporar la palabra en cuadro:";
$MESS ['SPCR1_SITE_URL'] = "URL de su  sitio: ";
$MESS ['SPCR1_SITE_DESCR'] = "Descripci�n de tu sitio: ";
$MESS ['SPCR1_I_AGREE'] = "Estoy de acuerdo con los t�rminos del acuerdo del afiliado";
$MESS ['SPCR1_REGISTER'] = "Registro";
?>