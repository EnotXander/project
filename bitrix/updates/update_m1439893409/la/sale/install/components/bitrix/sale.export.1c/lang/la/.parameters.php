<?
$MESS["CP_BCI1_GROUP_PERMISSIONS"] = "Grupos de usuarios permitidos para exportar";
$MESS["CP_BCI1_SITE_LIST"] = "Exportar pedidos de este sitio para 1C";
$MESS["CP_BCI1_USE_ZIP"] = "Usar co9mpresi�n ZIP si est� disponible";
$MESS["CP_BCI1_ALL_SITES"] = "Todos los sitios";
$MESS["CP_BCI1_EXPORT_PAYED_ORDERS"] = "Exportar s�lo pedidos pagados";
$MESS["CP_BCI1_EXPORT_ALLOW_DELIVERY_ORDERS"] = "Exportar s�lo los pedidos entregables";
$MESS["CP_BCI1_EXPORT_FINAL_ORDERS"] = "Exportar el estado de los pedidos";
$MESS["CP_BCI1_FINAL_STATUS_ON_DELIVERY"] = "Estado de los pedidos una vez adquiridos los entregables desde 1C";
$MESS["CP_BCI1_NO"] = "<Ninguno seleccionado>";
$MESS["CP_BCI1_REPLACE_CURRENCY"] = "Cuando exporta a 1C, cambia la moneda a ";
$MESS["CP_BCI1_REPLACE_CURRENCY_VALUE"] = "rublos";
$MESS["SALE_1C_INTERVAL"] = "Duraci�n del paso de importaci�n (0 - importan todo de una vez)";
$MESS["SALE_1C_FILE_SIZE_LIMIT"] = "Tama�o m�ximo al importar un pedazo del archivo (bytes)";
$MESS["SALE_1C_SITE_NEW_ORDERS"] = "Sitop a importar nuevas �rdenes y contratistas a";
$MESS["SALE_1C_IMPORT_NEW_ORDERS"] = "Crear nuevas �rdenes y contratistas desde 1C";
$MESS["CP_BCI1_CHANGE_STATUS_FROM_1C"] = "Utilice los datos 1C para establecer el estado del pedido";
?>