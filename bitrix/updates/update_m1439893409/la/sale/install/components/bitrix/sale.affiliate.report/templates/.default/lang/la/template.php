<?
$MESS ['SPCAS1_FILTER'] = "Muestra per�odo";
$MESS ['SPCAS1_PERIOD'] = "Per�odo:";
$MESS ['SPCAS1_SET'] = "Fijar";
$MESS ['SPCAS1_UNSET'] = "Resetear";
$MESS ['SPCAS1_NAME'] = "Nombre";
$MESS ['SPCAS1_QUANTITY'] = "Cantidad ";
$MESS ['SPCAS1_SUM'] = "Cantidad ";
$MESS ['SPCAS1_ITOG'] = "Total";
$MESS ['SPCAS1_NO_ACT'] = "Ninguna de las transacciones durante el per�odo especificado ";
$MESS ['SPCAS1_UNACTIVE_AFF'] = "Eres el afiliado registrado pero tu cuenta est� inactiva. Entrar en contacto por favor con la administraci�n para m�s informaci�n. ";
?>