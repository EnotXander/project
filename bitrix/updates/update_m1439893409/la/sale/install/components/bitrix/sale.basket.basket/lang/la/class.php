<?
$MESS["SALE_EMPTY_BASKET"] = "Su carrito de compras esta vacio";
$MESS["SBB_PRODUCT_NOT_AVAILABLE"] = "#PRODUCT# est� fuera de stock";
$MESS["SBB_PRODUCT_NOT_ENOUGH_QUANTITY"] = "El stock actual de \"#PRODUCT#\" es insuficiente (se requiere #NUMBER#)";
$MESS["SOA_TEMPL_ORDER_PS_ERROR"] = "El m�todo de pago seleccionado ha fallado. P�ngase en contacto con el administrador del sitio o seleccione otro m�todo.";
?>