<?
$MESS["WIKI_MODULE_NOT_INSTALLED"] = "El m�dulo de Wiki no est� instalado.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "El m�dulo Information Blocks no est� instalado.";
$MESS["IBLOCK_NOT_ASSIGNED"] = "No se seleccion� ning�n block de informaci�n.";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "El m�dulo Forum no est� instalado.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "El m�dulo Social Network no est� instalado.";
$MESS["WIKI_SOCNET_INITIALIZING_FAILED"] = "Social Network fall� al inicializar.";
$MESS["WIKI_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["WIKI_DISCUSSION_DISABLED"] = "Las discusiones est�n deshabilitadas.";
$MESS["SONET_FORUM_LOG_TEMPLATE_AUTHOR"] = "Autor: [URL=#URL#]#TITLE#[/URL].";
$MESS["SONET_FORUM_LOG_TEMPLATE_GUEST"] = "Author: #TITLE#.";
$MESS["WIKI_SONET_LOG_TITLE_TEMPLATE"] = "El Art�culo \"#TITLE#\" se ha agregado o actualizado";
$MESS["WIKI_SONET_IM_COMMENT"] = "Se ha agregado un comentario en el art�culo de Wiki \"#title#\"";
?>