<?
$MESS["SEARCH_REINDEX_REINDEX_CHANGED"] = "Volver a indexar solamente el contenido modificado  ";
$MESS["SEARCH_REINDEX_REINDEX_BUTTON"] = "Reindexar sitio";
$MESS["SEARCH_REINDEX_TOTAL"] = "Documentos indexados: ";
$MESS["SEARCH_REINDEX_COMPLETE"] = "reindexaci�n completa";
$MESS["SEARCH_REINDEX_STEP"] = "paso:";
$MESS["SEARCH_REINDEX_STEP_sec"] = "segundos";
$MESS["SEARCH_REINDEX_TITLE"] = "Sitio reindexado";
$MESS["SEARCH_REINDEX_TAB"] = "Reindexar";
$MESS["SEARCH_REINDEX_TAB_TITLE"] = "Par�metros de reindexaci�n";
$MESS["SEARCH_REINDEX_SITE"] = "Sitio";
$MESS["SEARCH_REINDEX_ALL"] = "(todo)";
$MESS["SEARCH_REINDEX_MODULE"] = "M�dulo";
$MESS["SEARCH_REINDEX_MAIN"] = "Archivos de estad�sticas";
$MESS["SEARCH_REINDEX_CONTINUE"] = "Continuar";
$MESS["SEARCH_REINDEX_STOP"] = "Detener";
$MESS["SEARCH_REINDEX_IN_PROGRESS"] = "Reindexaci�n...";
$MESS["SEARCH_REINDEX_NEXT_STEP"] = "Siguiente Paso ";
$MESS["SEARCH_REINDEX_SOCNET_WARNING"] = "El m�dulo Social network est� instaladado";
$MESS["SEARCH_REINDEX_SOCNET_WARN_DETAILS"] = "Un vez indexado por el m�dulo de b�squeda,el Social network ser� reindexada usando el componente Social Network en la secci�n p�blica.";
$MESS["SEARCH_REINDEX_SOCNET_MESSAGE"] = "!Atenci�n� Cuando el reindexado est� completo, el m�dulo <b> Social Network</b> necesita ser reindexado individualmente desde la secci�n p�blica. Cuando, desde el modo \"dise�o\" abra cualquier secci�n de la red social (que contengan cualquiera de los componentes <i>socialnetwork, socialnetwork_group, socialnetwork_user</i>) deber� hacer click en reindex en la barra de herramientas.<br><br><img src=\"/bitrix/images/search/socnet_reindex_en.png\">";
$MESS["SEARCH_REINDEX_CLEAR_SUGGEST"] = "Eliminar sugerencia de frase de b�squeda:";
?>