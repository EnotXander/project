<?
$MESS["SEO_ICON_ALT"] = "Optimizaci�n del SEO";
$MESS["SEO_ICON_TEXT"] = "SEO";
$MESS["SEO_ICON_HINT"] = "Ejecutar las herramientas del SEO para subir su sitio Web a los resultados del motor de b�squeda";
$MESS["SEO_DIR_LOGICAL_NO_NAME"] = "<Sin t�tulo>";
$MESS["SEO_UNIQUE_TEXT_YANDEX"] = "Presentar el texto �nico en Yandex";
$MESS["SEO_UNIQUE_TEXT_YANDEX_SUBMIT"] = "Presentar en Yandex";
$MESS["SEO_YANDEX_ERROR"] = "No se especifica la relaci�n con el Website. <a href=\"/bitrix/admin/seo_search_yandex.php?lang=ru\">Set now</a>";
$MESS["SEO_YANDEX_DOMAIN"] = "Dominio";
$MESS["SEO_YANDEX_ORIGINAL_TEXT_TOO_SHORT"] = "El texto es demasiado corto. Su texto debe ser de #NUM# caracteres o m�s.";
$MESS["SEO_YANDEX_ORIGINAL_TEXT_TOO_LONG"] = "El texto es demasiado largo. Su texto debe ser #NUM# caracteres como m�ximo o m�s corto.";
$MESS["SEO_YANDEX_ORIGINAL_TEXT_OK"] = "Operaci�n correcta";
$MESS["SEO_YANDEX_ORIGINAL_TEXT_OK_DETAILS"] = "Se ha a�adido texto original.<a href=\"/bitrix/admin/seo_search_yandex.php?lang=#LANGUAGE_ID#\">Manage original texts</a>";
?>