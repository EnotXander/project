<?
$MESS["SEO_MENU_MAIN"] = "Optimizaci�n de Motor de B�squeda";
$MESS["SEO_MENU_MAIN_TITLE"] = "Herramientas de SEO del Sitio web";
$MESS["SEO_MENU_SEARCH_ENGINES"] = "Motores de b�squeda";
$MESS["SEO_MENU_SEARCH_ENGINES_ALT"] = "Herramientas espec�ficas para motores de b�squeda";
$MESS["SEO_MENU_ROBOTS"] = "robots.txt";
$MESS["SEO_MENU_ROBOTS_ALT"] = "Par�metros para robots.txt";
$MESS["SEO_MENU_SITEMAP"] = "sitemap.xml";
$MESS["SEO_MENU_SITEMAP_ALT"] = "Par�metros para sitemap.xml";
$MESS["SEO_MENU_YANDEX"] = "Yandex";
$MESS["SEO_MENU_GOOGLE"] = "Google";
?>