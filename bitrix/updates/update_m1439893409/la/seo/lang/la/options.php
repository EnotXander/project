<?
$MESS["SEO_OPT_PROP_WINDOW_TITLE"] = "T�tulo de la ventana del navegador";
$MESS["SEO_OPT_PROP_DESCRIPTION"] = "Descripci�n de p�gina";
$MESS["SEO_OPT_PROP_KEYWORDS"] = "Palabras clave de la p�gina";
$MESS["SEO_OPT_PROP_INTERNAL_KEYWORDS"] = "Promoci�n de palabras clave";
$MESS["SEO_OPT_TAB_PROP"] = "Propiedades de la p�gina";
$MESS["SEO_OPT_TAB_PROP_TITLE"] = "C�digos de las propiedades de la p�gina";
$MESS["SEO_OPT_TAB_SEARCHERS"] = "Herramientas";
$MESS["SEO_OPT_TAB_SEARCHERS_TITLE"] = "Configuraciones de las herramientas SEO";
$MESS["SEO_OPT_COUNTERS"] = "Contadores visualizados";
$MESS["SEO_OPT_SEARCHERS"] = "Motores de b�squeda usados en an�lisis";
$MESS["SEO_OPT_SEARCHERS_SELECTED"] = "Lista actual";
$MESS["SEO_OPT_ERR_NO_STATS"] = "Esta caracter�stica no est� disponible sin que el M�dulo de An�lisis de web est� instalado";
?>