<?
$MESS["CATALOG_COMPARE"] = "Comparar";
$MESS["CATALOG_BUY"] = "Comprar";
$MESS["CATALOG_ADD"] = "Agregar al carrito";
$MESS["CATALOG_NOT_AVAILABLE"] = "(no est� disponibles en stock)";
$MESS["CATALOG_QUANTITY"] = "Cantidad";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "desde #FROM# hasta #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "#FROM# y m�s";
$MESS["CATALOG_QUANTITY_TO"] = "hasta #TO#";
$MESS["CT_BCT_QUANTITY"] = "Cantidad";
$MESS["CATALOG_MORE"] = "M�s";
$MESS["CATALOG_FROM"] = "desde";
$MESS["CT_BCT_ELEMENT_DELETE_CONFIRM"] = "Toda la informaci�n vinculada a este registro se borrar�. Desea continuar?";
?>