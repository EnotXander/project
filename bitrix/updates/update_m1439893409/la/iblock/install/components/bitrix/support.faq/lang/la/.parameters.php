<?
$MESS["SUPPORT_FAQ_GROUP_SETTINGS"] = "Configuraciones del componente";
$MESS["SUPPORT_FAQ_SETTING_IBTYPES"] = "Tipos de block de informaci�n";
$MESS["SUPPORT_FAQ_SETTING_IBLIST"] = "Blocks de informaci�n";
$MESS["SUPPORT_FAQ_SETTING_SECTIONS_LIST"] = "Lista de la secci�n";
$MESS["SUPPORT_FAQ_SETTING_EXPAND_LIST"] = "Mostrar sub secciones";
$MESS["SEF_PAGE_FAQ"] = "�ndice de las p�ginas";
$MESS["SEF_PAGE_FAQ_SECTION"] = "P�gina de la secci�n";
$MESS["SEF_PAGE_FAQ_DETAIL"] = "P�gina de detalle";
$MESS["CP_BSF_CACHE_GROUPS"] = "Respetar permisos de acceso";
$MESS["SUPPORT_RATING_SETTINGS"] = "Par�metros de la calificaci�n";
$MESS["SHOW_RATING"] = "Permitir calificaci�n";
$MESS["SHOW_RATING_CONFIG"] = "predeterminado";
$MESS["RATING_TYPE"] = "Calificar el dise�o de los botones";
$MESS["RATING_TYPE_CONFIG"] = "predetrminado";
$MESS["RATING_TYPE_STANDART_TEXT"] = "Me gusta/No me gusta (texto)";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "Me gusta/No me gusta (imagen)";
$MESS["RATING_TYPE_LIKE_TEXT"] = "Me gusta (texto)";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "Me gusta(imagen)";
$MESS["PATH_TO_USER"] = "plantilla a la ruta del perfil del usuario";
?>