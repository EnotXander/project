<?
$MESS["IBLOCK_BAD_BLOCK_SECTION_ID_PARENT"] = "El c�digo del block no coincide con la secci�n parental!";
$MESS["IBLOCK_BAD_SECTION"] = "Nombre de la secci�n no est� especificada.";
$MESS["IBLOCK_BAD_SECTION_ID"] = "No existe ninguna secci�n con este ID (#ID#)";
$MESS["IBLOCK_BAD_BLOCK_SECTION_RECURSE"] = "No puede mover la secci�n dentro de �sta.";
$MESS["IBLOCK_BAD_BLOCK_SECTION_PARENT"] = "Secci�n parental incorrecta!";
$MESS["IBLOCK_DUP_SECTION_CODE"] = "Una secci�n con este c�digo simb�lico ya existe";
$MESS["IBLOCK_BAD_SECTION_FIELD"] = "El campo \"#FIELD_NAME#\" es requerido";
?>