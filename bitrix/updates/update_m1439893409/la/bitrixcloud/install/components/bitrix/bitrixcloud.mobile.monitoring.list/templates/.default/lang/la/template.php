<?
$MESS["BCLMMSL_MONITORING_MONITORING_PERIOD"] = "Rango de fechas";
$MESS["BCLMMSL_MONITORING_DOMAIN_REGISTRATION"] = "El dominio expirar� en";
$MESS["BCLMMSL_MONITORING_LICENSE"] = "La licencia expirar� en";
$MESS["BCLMMSL_MONITORING_MONITORING_SSL"] = "El certificado SSL expirar� en";
$MESS["BCLMMSL_MONITORING_TITLE"] = "Inspector Cloud";
$MESS["BCLMMSL_MONITORING_NO_PROBS"] = "Actualmente no hay problemas con sus sitios.";
$MESS["BCLMMSL_MONITORING_BUT_DETAIL"] = "Detalles";
$MESS["BCLMMSL_MONITORING_NO_DATA2"] = "Sin datos.";
$MESS["BCLMMSL_TITLE"] = "Inspector Cloud";
$MESS["BCLMMSL_MONITORING_HTTP_RESPONSE_TIME"] = "Respuesta del Sitio Web";
$MESS["BCLMMSL_MONITORING_FAILED_PERIOD"] = "Inactividad";
$MESS["BCLMMSL_MONITORING_MONTH_LOST"] = "P�rdida mensual";
?>