<?
$MESS["CLU_MEMCACHE_LIST_ADD"] = "Agregar";
$MESS["CLU_MEMCACHE_LIST_ADD_TITLE"] = "Agregar a la Conexi�n de la Mem de cach�";
$MESS["CLU_MEMCACHE_LIST_REFRESH"] = "Actualizar";
$MESS["CLU_MEMCACHE_LIST_FLAG"] = "Bandera";
$MESS["CLU_MEMCACHE_LIST_STATUS"] = "Estado";
$MESS["CLU_MEMCACHE_LIST_WEIGHT"] = "Peso (%)";
$MESS["CLU_MEMCACHE_LIST_HOST"] = "Servidor";
$MESS["CLU_MEMCACHE_LIST_EDIT"] = "Editar";
$MESS["CLU_MEMCACHE_LIST_DELETE"] = "Eliminar";
$MESS["CLU_MEMCACHE_LIST_DELETE_CONF"] = "Eliminar conexi�n?";
$MESS["CLU_MEMCACHE_LIST_START_USING"] = "Usar la Base de datos";
$MESS["CLU_MEMCACHE_LIST_STOP_USING"] = "Detener Uso de la Basede datos";
$MESS["CLU_MEMCACHE_UPTIME"] = "subir hora";
$MESS["CLU_MEMCACHE_LIST_TITLE"] = "Conexiones de la Mem del Cach�";
$MESS["CLU_MEMCACHE_NOCONNECTION"] = "esconectado";
$MESS["CLU_MEMCACHE_LIST_ID"] = "ID";
$MESS["CLU_MEMCACHE_NO_EXTENTION"] = "Una extensi�n PHP \"Memcached\" no est� instalada.";
$MESS["CLU_MEMCACHE_LIST_NOTE"] = "<p> Los sistemas de archivos de varios servidores deben estar sincronizados para funcionar correctamente. </p>
<p> Memcached es muy recomendable para reducir el estr�s de sincronizaci�n de archivos y la carga del servidor, ya que almacena los archivos en la memoria cach� en lugar de en archivos f�sicos. </p>";
$MESS["CLU_MEMCACHE_LIST_WARNING_NO_CACHE"] = "Atenci�n! Cach� est� desactivado. Para habilitar,  utilizar una memoria de cach� en conexi�n o modificar par�metros en .settings.php.";
?>