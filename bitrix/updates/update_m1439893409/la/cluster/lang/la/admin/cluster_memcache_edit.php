<?
$MESS["CLU_MEMCACHE_EDIT_TAB"] = "Par�metros";
$MESS["CLU_MEMCACHE_EDIT_MENU_LIST"] = "Lista";
$MESS["CLU_MEMCACHE_EDIT_HOST"] = "Servidor";
$MESS["CLU_MEMCACHE_EDIT_PORT"] = "Puerta";
$MESS["CLU_MEMCACHE_EDIT_SAVE_ERROR"] = "Error al guardar los par�metros de la conexi�n.";
$MESS["CLU_MEMCACHE_EDIT_ID"] = "ID";
$MESS["CLU_MEMCACHE_EDIT_TAB_TITLE"] = "Par�metros de conexi�n de servidor Memcached  ";
$MESS["CLU_MEMCACHE_NO_EXTENTION"] = "Una extensi�n PHP \"Memcached\" no est� instalada.";
$MESS["CLU_MEMCACHE_EDIT_EDIT_TITLE"] = "Editar la Conexi�n Memcached ";
$MESS["CLU_MEMCACHE_EDIT_NEW_TITLE"] = "Nueva Conexi�n Memcached";
$MESS["CLU_MEMCACHE_EDIT_MENU_LIST_TITLE"] = "Conexiones Memcached";
$MESS["CLU_MEMCACHE_EDIT_WEIGHT"] = "Carga, % (0..100)";
$MESS["CLU_MEMCACHE_EDIT_WARNING"] = "�Atenci�n! Ya existe una conexi�n con el servidor memcached. Algunas versiones de la extensi�n de memcached cuentan con un error que resulta en una disminuci�n significativa del rendimiento si existen varias conexiones. Utilice <a href=\"#link#\"> Monitor de rendimiento </a> para evaluar el rendimiento de su sistema antes y despu�s de agregar otra conexi�n. En caso de producirse una disparidad significativa, trate de instalar una versi�n diferente de memcached.";
?>