<?
$MESS["CLUWIZ_NO_MODULE_ERROR"] = "El m�dulo Web Cluster no est� instalado. El asistente abortar� ahora.";
$MESS["CLUWIZ_NO_NODE_ERROR"] = "El m�dulo de la base de datos no est� especificado. El asistente no abortar�.";
$MESS["CLUWIZ_NO_CONN_ERROR"] = "Error al conectarse con la base de datos. El asistente no podr� abortar.";
$MESS["CLUWIZ_STEP1_TITLE1"] = "Asistente del Uso de la Base de Datos";
$MESS["CLUWIZ_STEP1_TITLE2"] = "Asistente del M�dulo de Migraci�n";
$MESS["CLUWIZ_STEP2_NO_TABLES"] = "La tablas tienen ue ser encontradas; la transferencia de la base de datos en segura para el inicio.";
$MESS["CLUWIZ_STEP2_TABLES_EXIST"] = "La base de datos especificada contiene tablas. Esas tablas deben ser borradas para continuar.";
$MESS["CLUWIZ_STEP2_TABLES_LIST"] = "ver tablas";
$MESS["CLUWIZ_STEP2_DELETE_TABLES"] = "Permitirtablas en la base de datos #database# para ser borradas";
$MESS["CLUWIZ_STEP3_TITLE"] = "Borrando Tablas";
$MESS["CLUWIZ_STEP4_TITLE"] = "Moviendo Tablas";
$MESS["CLUWIZ_FINALSTEP_BUTTONTITLE"] = "Listo";
$MESS["CLUWIZ_CANCELSTEP_TITLE"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_CANCELSTEP_BUTTONTITLE"] = "Cerrar";
$MESS["CLUWIZ_CANCELSTEP_CONTENT"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_DATABASE_NOT_SUPPORTED"] = "El asistente no soporta el tipo de base de datos especificada.";
$MESS["CLUWIZ_STEP2_TITLE"] = "Seleccionando la Disponibilidad de las Tablas del M�dulo";
$MESS["CLUWIZ_STEP1_CONTENT1"] = "<p>Para iniciar el uso de la base de datos#database#, uno de los m�dulos que se van a migrar a
 .</p><p>Seleccionar el m�dulo a tranferir :</p>#module_select_list#<p>Este m�dulo podr� deshabilitarse mientras se le procesa para la migraci�n. El sitio web podr� mantenerte arriba y funcionando.</p>";
$MESS["CLUWIZ_STEP1_CONTENT2"] = "<p>Para detener el uso de la base de datos, el m�dulo debe ser migrado a otra base de datos.</p><p>Seleccione el destino de la base de datos:</p>#module_select_list#<p>Este m�dulo podr� deshabilitarse mientras se procesa para migraci�n. ";
?>