<?
$MESS["CLUWIZ_STEP1_TITLE"] = "Asistente de la base de datos de la nueva Master slave";
$MESS["CLUWIZ_NO_MODULE_ERROR"] = "El m�dulo Web Cluster no est� instalado. El asistente se Cancelar�.";
$MESS["CLUWIZ_CHEKED"] = "Comprobado.";
$MESS["CLUWIZ_STEP1_CONTENT"] = "El asistente se conectar� un nueva base de datos del maestro-slave y verificar� los par�metros principales de la base de datos maestra esencial para la replicaci�n correcta. <br />";
$MESS["CLUWIZ_STEP2_TITLE"] = "Los par�metros de Conexi�n de la base de datos del master-slave";
$MESS["CLUWIZ_STEP2_DB_HOST"] = "La cadena de conexi�n de la base de datos";
$MESS["CLUWIZ_STEP2_DB_NAME"] = "Base de datos";
$MESS["CLUWIZ_STEP2_DB_NAME_HINT"] = "(el mismo nombre de la base de datos maestra)";
$MESS["CLUWIZ_STEP2_DB_LOGIN"] = "Usuario";
$MESS["CLUWIZ_STEP2_DB_PASSWORD"] = "Contrase�a";
$MESS["CLUWIZ_STEP2_MASTER_CONN"] = "Par�metros de la conexi�n de la base de datos maestra:";
$MESS["CLUWIZ_STEP2_MASTER_HOST"] = "IP del servidor o nombre";
$MESS["CLUWIZ_STEP2_MASTER_PORT"] = "Puerto";
$MESS["CLUWIZ_STEP4_TITLE"] = "Verificaci�n de par�metros de la base de datos";
$MESS["CLUWIZ_STEP4_CONN_ERROR"] = "La conexi�n de la base de datos no puede ser establecida. Por favor regrese al paso anterior y modifique los par�metros de conexi�n.";
$MESS["CLUWIZ_FINALSTEP_TITLE"] = "Finalizar el asistente";
$MESS["CLUWIZ_FINALSTEP_NAME"] = "Nombre de la conexi�n ";
$MESS["CLUWIZ_FINALSTEP_BUTTONTITLE"] = "Terminar";
$MESS["CLUWIZ_CANCELSTEP_TITLE"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_CANCELSTEP_BUTTONTITLE"] = "Cerrar";
$MESS["CLUWIZ_CANCELSTEP_CONTENT"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_DATABASE_NOT_SUPPORTED"] = "El asistente no soporte el tipo de base de datos especificada.";
$MESS["CLUWIZ_STEP4_SLAVE_IS_RUNNING"] = "La base de datos maestra est� siendo replicada a la base de datos espec�fica.";
$MESS["CLUWIZ_NO_GROUP_ERROR"] = "El grupo del servidor no est� definido.";
?>