<?
$MESS["CLUWIZ_ERROR_ACCESS_DENIED"] = "�Error! Acceso denegado.";
$MESS["CLUWIZ_CONNECTION_ERROR"] = "Error al conectar con la base de datos.";
$MESS["CLUWIZ_QUERY_ERROR"] = "Error al consultar la base de datos.";
$MESS["CLUWIZ_TABLE_PROGRESS"] = "Tabla: #table_name#<br />Filas movidas: #records#<br />Tables left: #tables#";
$MESS["CLUWIZ_ALL_DONE"] = "La base de datos ha sido transferido exitosamente. La replicac�n est� ahora en progreso (ONLINE).";
$MESS["CLUWIZ_INIT"] = "Inicializando...";
$MESS["CLUWIZ_SITE_OPEN"] = "Oprimir \"Finalizar\" para hacer la secci�n p�blica disponible a los visitantes.";
?>