<?
$MESS["CLUWIZ_NO_MODULE_ERROR"] = "El m�dulo del web cluster no est� instalado. El asistente finalizar� ahora.";
$MESS["CLUWIZ_NO_NODE_ERROR"] = "La base de datos sumisa no est� especificada. El asistente finalizar� ahora.";
$MESS["CLUWIZ_NO_CONN_ERROR"] = "Error al conectar la base de datos sumisa. El asistente finalizar� ahora.";
$MESS["CLUWIZ_STEP2_TITLE"] = "Comprobaci�n para las tablas en la base de datos sumisa";
$MESS["CLUWIZ_STEP2_NO_TABLES"] = "No se encontraron tablas; la transferencia de la base de datos est� segura para su inicio.";
$MESS["CLUWIZ_STEP2_TABLES_EXIST"] = "La base de datos especificada contiene tablas. Aquellas tablas deben ser eliminadas para continuar.";
$MESS["CLUWIZ_STEP2_TABLES_LIST"] = "ver tablas";
$MESS["CLUWIZ_STEP2_DELETE_TABLES"] = "Permitir tablas en la base de datos  #database# para ser eliminados";
$MESS["CLUWIZ_STEP2_WARNING"] = "La secci�n p�blica del sitio web no est� disponible durante la transferencia de la data.";
$MESS["CLUWIZ_STEP3_TITLE"] = "Eliminando tablas";
$MESS["CLUWIZ_STEP4_TITLE"] = "Moviendo tablas";
$MESS["CLUWIZ_FINALSTEP_BUTTONTITLE"] = "Finalizar";
$MESS["CLUWIZ_CANCELSTEP_TITLE"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_CANCELSTEP_BUTTONTITLE"] = "Cerrar";
$MESS["CLUWIZ_CANCELSTEP_CONTENT"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_INIT"] = "Inicializaci�n";
$MESS["CLUWIZ_DATABASE_NOT_SUPPORTED"] = "El asistente no soporta el tipo de base de datos especificada.";
?>