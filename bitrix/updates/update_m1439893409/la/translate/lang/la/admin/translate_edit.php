<?
$MESS["TRANS_TITLE"] = "Traducir mensajes";
$MESS["TRANS_FOLDERS_LIST"] = "Lista de carpetas";
$MESS["TRANS_FILENAME"] = "Nombre del archivo:";
$MESS["TRANS_NOT_TRANS"] = "Sin traducir:";
$MESS["TRANS_SAVE"] = "Guardar";
$MESS["TRANS_RESET"] = "Resetear";
$MESS["TRANS_TOTAL"] = "Total de mensajes:";
$MESS["TRANS_DELETE"] = "Borrar:";
$MESS["TRANS_SHOW_ONLY_ERROR"] = "Mostrar solamente los mensajes sin traducir ";
$MESS["TRANS_SHOW_ALL"] = "Mostar todos los mensajes";
$MESS["TRANS_TITLE_TITLE"] = "Traducci�n de mensajes por idiomas";
$MESS["TRANS_SHOW_ALL_TITLE"] = "Click para mostrar todos los mensajes de lenguaje";
$MESS["TRANS_SHOW_ONLY_ERROR_TITLE"] = "Click para mostrar s�lo los mensajes no traducidos";
$MESS["TRANS_LIST"] = "Lista";
$MESS["TRANS_LIST_TITLE"] = "Click para regresar a la lista";
$MESS["TRANS_CHAIN_FOLDER_ROOT"] = "Click para ir a la carpeta ra�z";
$MESS["TRANS_CHAIN_FOLDER"] = "Click para ir al folder";
$MESS["TRANS_DELETE_ALL"] = "Borrar todo";
$MESS["trans_edit_err"] = "Nombre de archivo incorrecto.";
$MESS["TR_FILE_EDIT"] = "Editar C�digo PHP";
$MESS["TR_FILE_EDIT_TITLE"] = "Editar C�digo PHP";
$MESS["TR_FILE_SHOW"] = "Ver C�digo PHP";
$MESS["TR_FILE_SHOW_TITLE"] = "Ver C�digo PHP";
$MESS["TR_CREATE_BACKUP_ERROR"] = "Usted fall� al crear el backup para el archivo \"%FILE%\"";
$MESS["TRANS_DELETE_CURRENT"] = "Eliminar los mensajes no existentes en el idioma actual";
$MESS["TR_FILE_PHP"] = "C�digo PHP";
$MESS["TRANS_GET_UNTRANSLATE"] = "Exportar los mensajes sin traducir";
$MESS["TRANS_GET_UNTRANSLATE_TITLE"] = "Exportar mensajes sin traducir para este archivo";
?>