<?
$MESS["TRANS_INIT_FOLDERS"] = "Carpeta inicial (separadas por coma): ";
$MESS["TRANS_SAVE"] = "Guardar";
$MESS["TRANS_RESET"] = "Resetear";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Restaurar por defecto";
$MESS["TRANS_SHOW_ONLY_ERRORS"] = "Mostrar s�lo mensajes no traducidos";
$MESS["TRANS_AUTO_CALCULATE"] = "Calcular diferencias en los archivo de idioma autom�ticamente:";
$MESS["TRANS_BUTTON_LANG_FILES"] = "Incluir el bot�n de \"Mostrar archivos de idiomas\" en la Barra de Herramientas";
$MESS["TRANS_BACKUP_FILES"] = "Hacer backup de los archivos que ser�n afectados antes de guardar";
?>