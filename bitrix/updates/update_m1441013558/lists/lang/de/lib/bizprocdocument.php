<?
$MESS["LISTS_BIZPROC_INVALID_INT"] = "Der Wert des Feldes ist nicht eine ganze Zahl. ";
$MESS["LISTS_BIZPROC_INVALID_DATE"] = "Der Wert im Feld ist nicht ein korrektes Datum. ";
$MESS["LISTS_BIZPROC_INVALID_SELECT"] = "Ungültiger Wert des Listenelements.";
?>