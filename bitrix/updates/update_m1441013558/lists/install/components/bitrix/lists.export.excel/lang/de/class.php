<?
$MESS["CC_BLL_MODULE_NOT_INSTALLED"] = "Das Modul Allgemeine Listen ist nicht installiert.";
$MESS["CC_BLL_COLUMN_BIZPROC"] = "Gesch�ftsprozesse";
$MESS["CC_BLL_COLUMN_SECTION"] = "Bereiche";
$MESS["CC_BLL_ENLARGE"] = "Vergr��ern";
$MESS["CC_BLL_DOWNLOAD"] = "Zum Herunterladen verf�gbar";
$MESS["CC_BLL_WRONG_IBLOCK_TYPE"] = "Nicht korrekter Typ des Informationsblocks.";
$MESS["CC_BLL_WRONG_IBLOCK"] = "Die definierte Liste ist falsch.";
$MESS["CC_BLL_LISTS_FOR_SONET_GROUP_DISABLED"] = "Die Listen sind f�r diese Gruppe deaktiviert.";
$MESS["CC_BLL_UNKNOWN_ERROR"] = "Unbekannter Fehler.";
$MESS["CC_BLL_ACCESS_DENIED"] = "Keine Berechtigung zur Vorschau und Bearbeitung der Liste.";
?>