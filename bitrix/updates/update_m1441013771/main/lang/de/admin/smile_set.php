<?
$MESS["SMILE_TITLE"] = "Smiley-S�tze";
$MESS["SMILE_DEL_CONF"] = "M�chten Sie diesen Satz wirklich l�schen? Dadurch werden auch alle Smileys gel�scht, die in diesem Satz enthalten sind.";
$MESS["SMILE_NAV"] = "S�tze";
$MESS["SMILE_ID"] = "ID";
$MESS["SMILE_SORT"] = "Sortierung";
$MESS["SMILE_NAME"] = "Name";
$MESS["SMILE_SET_NAME"] = "Satz: #ID#";
$MESS["SMILE_STRING_ID"] = "Satz-ID";
$MESS["SMILE_SMILE_COUNT"] = "Smileys";
$MESS["SMILE_DELETE_DESCR"] = "L�schen";
$MESS["SMILE_EDIT"] = "Bearbeiten";
$MESS["SMILE_EDIT_DESCR"] = "Bearbeiten";
$MESS["ERROR_DEL_SMILE"] = "Fehler beim L�schen vom Satz.";
$MESS["SMILE_BTN_ADD_NEW"] = "Neuer Satz";
$MESS["SMILE_BTN_ADD_NEW_ALT"] = "Klicken Sie, um einen neuen Satz hinzuzuf�gen.";
$MESS["SMILE_BTN_BACK"] = "Galerien mit Emoticons";
?>