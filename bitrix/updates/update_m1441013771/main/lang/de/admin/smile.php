<?
$MESS["SMILE_TITLE"] = "Smileys";
$MESS["SMILE_DEL_CONF"] = "M�chten Sie dieses Smiley wirklich l�schen?";
$MESS["SMILE_NAV"] = "Smileys";
$MESS["SMILE_ID"] = "ID";
$MESS["SMILE_SORT"] = "Sortierung";
$MESS["SMILE_SET_NAME"] = "Satz";
$MESS["SMILE_TYPE"] = "Typ";
$MESS["SMILE_TYPING"] = "Schreibweise";
$MESS["SMILE_ICON"] = "Bild";
$MESS["SMILE_IMAGE_FILE"] = "Datei";
$MESS["SMILE_TYPE_ICON"] = "Icon";
$MESS["SMILE_TYPE_SMILE"] = "Smiley";
$MESS["SMILE_NAME"] = "Name";
$MESS["SMILE_SET_NO_NAME"] = "Satz: #ID#";
$MESS["SMILE_NO_NAME"] = "Unbenannt";
$MESS["SMILE_DELETE_DESCR"] = "L�schen";
$MESS["SMILE_EDIT"] = "Bearbeiten";
$MESS["SMILE_EDIT_DESCR"] = "Bearbeiten";
$MESS["ERROR_DEL_SMILE"] = "Fehler beim L�schen von Smiley.";
$MESS["SMILE_BTN_IMPORT"] = "Import";
$MESS["SMILE_BTN_CLEAR_FILTER"] = "Filter zur�cksetzen";
$MESS["SMILE_BTN_ADD_NEW"] = "Neues Smiley";
$MESS["SMILE_BTN_ADD_NEW_ALT"] = "Klicken Sie, um ein neues Smiley hinzuzuf�gen";
$MESS["SMILE_BTN_BACK"] = "Zur�ck zu Paketen";
$MESS["SMILE_BITRIX_SET_WARNING_TITLE"] = "Standardpaket";
$MESS["SMILE_BITRIX_SET_WARNING_DESC"] = "Achtung: Das ist ein Standardpaket. Eine neue Aktualisierung kann alle �nderung, welche Sie in diesem Paket vornehmen k�nnen, zur�cksetzen.<br>Es wird empfohlen, ein benutzerdefiniertes Paket zu #LINK_START#erstellen#LINK_END# und Emoticons dorthin hinzuzuf�gen.";
?>