<?
if($updater->CanUpdateKernel())
{
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/js", "js");
	$updater->CopyFiles("install/panel", "panel");
	$updater->CopyFiles("install/admin", "admin");

	$arToDelete = array(
		"modules/main/install/panel/main/images/popup_menu_sprite.png",
		"modules/main/install/panel/main/images/popup_menu_sprite_1.png",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}

if ($updater->CanUpdateDatabase())
{
	$updater->Query(array(
		"Oracle" => "ALTER TABLE b_group_subordinate MODIFY (AR_SUBGROUP_ID VARCHAR2(2000 CHAR))",
		"MySql" => "ALTER TABLE b_group_subordinate CHANGE AR_SUBGROUP_ID AR_SUBGROUP_ID text not null",
		"MSSQL" => "ALTER TABLE B_GROUP_SUBORDINATE ALTER COLUMN AR_SUBGROUP_ID varchar(2000) not null",
	));

	if(!$DB->Query("select HIDDEN from b_smile WHERE 1=0", true))
	{
		$updater->Query(array(
			"MySQL"  => "alter table b_smile add column HIDDEN char(1) not null default 'N'",
			"Oracle" => "alter table B_SMILE add HIDDEN char(1 CHAR) default 'N' not null",
			"MSSQL"  => "alter table B_SMILE add HIDDEN char(1) CONSTRAINT DF_B_SMILE_HIDDEN DEFAULT 'N'",
		));
	}

	if(!$DB->Query("select IMAGE_DEFINITION from b_smile WHERE 1=0", true))
	{
		$updater->Query(array(
			"MySQL"  => "alter table b_smile add column IMAGE_DEFINITION varchar(10) not null default 'SD'",
			"Oracle" => "alter table B_SMILE add IMAGE_DEFINITION varchar2(10 CHAR) default 'SD' not null",
			"MSSQL"  => "alter table B_SMILE add IMAGE_DEFINITION varchar(10) CONSTRAINT DF_B_SMILE_ID DEFAULT 'SD'",
		));
	}

	if($DB->Query('select IMAGE_HR from b_smile where 1=0', true))
	{
		$updater->Query(array(
			"MSSQL"  => "ALTER TABLE b_smile DROP CONSTRAINT DF_B_SMILE_IMAGE_HR",
		));
		$updater->Query("alter table b_smile drop COLUMN IMAGE_HR");
	}

	if(!$DB->Query("select TYPE from b_smile_set WHERE 1=0", true))
	{
		$updater->Query(array(
			"MySQL"  => "alter table b_smile_set add column TYPE char(1) not null default 'G'",
			"Oracle" => "alter table B_SMILE_SET add TYPE char(1 CHAR) default 'G' not null",
			"MSSQL"  => "alter table B_SMILE_SET add TYPE char(1) CONSTRAINT DF_B_SMILE_SET_TYPE DEFAULT 'G'",
		));
	}
	if(!$DB->Query("select PARENT_ID from b_smile_set WHERE 1=0", true))
	{
		$updater->Query(array(
			"MySQL"  => "alter table b_smile_set add column PARENT_ID int(18) not null default 0",
			"Oracle" => "alter table B_SMILE_SET add PARENT_ID number(18) default '0' not null",
			"MSSQL"  => "alter table B_SMILE_SET add PARENT_ID int CONSTRAINT DF_B_SMILE_SET_PID DEFAULT '0'",
		));
	}
}

if ($updater->CanUpdateDatabase() && $updater->CanUpdateKernel())
{
	if(!IsModuleInstalled('bitrix24') && $updater->TableExists("b_smile"))
	{
		include_once($_SERVER["DOCUMENT_ROOT"].$updater->curPath."/smile.php");

		CSmileGalleryUpdater::convertGallery($updater);
	}
}
?>
