<?
$MESS["AD_USE_HTML_EDIT"] = "Use HTML editor (for IE 5.0 and higher only):";
$MESS["AD_DONT_USE_CONTRACT"] = "Do not apply contract restrictions to banners:";
$MESS["MAIN_RESTORE_DEFAULTS"] = "پیش فرض";
$MESS["AD_BANNER_DAYS"] = "Days to keep the banner dynamics?";
$MESS["AD_CLEAR"] = "پاکسازی";
$MESS["AD_RECORDS"] = "رکوردها:";
$MESS["AD_BANNER_GRAPH_WEIGHT"] = "پهنای گراف:";
$MESS["AD_BANNER_GRAPH_HEIGHT"] = "بلندای گراف:";
$MESS["AD_BANNER_DIAGRAM_DIAMETER"] = "Pie chart diameter:";
$MESS["AD_COOKIE_DAYS"] = "چه مدت اطلاعات بینندگان بنر حفظ شود:";
$MESS["AD_REDIRECT_FILENAME"] = "Full name of file handler for banner clicks registration:";
$MESS["AD_UPLOAD_SUBDIR"] = "فولدر برای بنرهای دانلود شده";
$MESS["AD_MOVE_LOADED_BANNERS"] = "انتقال بنرها<br>به فولدر دیگر";
?>