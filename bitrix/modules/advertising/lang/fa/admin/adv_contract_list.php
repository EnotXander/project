<?
$MESS["AD_RED"] = "قرمز";
$MESS["AD_GREEN"] = "سبز";
$MESS["AD_F_NAME"] = "عنوان";
$MESS["AD_F_ID"] = "شناسه";
$MESS["AD_F_DESCRIPTION"] = "توضیحات";
$MESS["AD_F_LAMP"] = "نماینده";
$MESS["AD_F_SHOWN"] = "نمایش";
$MESS["AD_F_CLICKED"] = "کلیکها";
$MESS["AD_LAMP"] = "نماینده";
$MESS["AD_DATE_MODIFY"] = "ویرایش شده";
$MESS["AD_MODIFIED_BY"] = "ویرایش شده توسط";
$MESS["AD_WEIGHT"] = "وزن";
$MESS["AD_BANNER_COUNT"] = "بنرها";
$MESS["AD_MAX_SHOW_COUNT"] = "نمایش";
$MESS["AD_NAME"] = "عنوان";
$MESS["AD_DESCRIPTION"] = "توضیحات";
$MESS["AD_MAX_CLICK_COUNT"] = "کلیکها";
$MESS["AD_SORT"] = "ترتیب";
$MESS["AD_F_VISITORS"] = "بینندگان";
$MESS["AD_F_SITE"] = "سایت";
$MESS["AD_SITE"] = "سایتها";
$MESS["ADV_FLT_SEARCH"] = "جستجو";
$MESS["AD_ADD"] = "افزودن قرارداد";
$MESS["AD_VIEW"] = "مشاهده";
?>