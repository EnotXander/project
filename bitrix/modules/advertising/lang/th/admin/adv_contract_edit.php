<?
$MESS ['AD_BACK_TO_CONTRACT_LIST'] = "��¡���ѭ�Һ�ԡ��";
$MESS ['AD_BACK_TO_CONTRACT_LIST_TITLE'] = "����¡���ѭ�Һ�ԡ��";
$MESS ['AD_EDIT_RECORD'] = "�ѭ�Һ�ԡ�� # #ID#";
$MESS ['AD_NEW_RECORD'] = "�ѭ�Һ�ԡ������";
$MESS ['AD_MODIFIED'] = "��Ѻ��ا�����:";
$MESS ['AD_CREATED'] = "���ҧ�����:";
$MESS ['AD_ACTIVE'] = "Activity flag:";
$MESS ['AD_TITLE'] = "Title:";
$MESS ['AD_DESCRIPTION'] = "��������´:";
$MESS ['AD_WEIGHT'] = "�ӴѺ��� (priority):";
$MESS ['AD_CONTRACT_LIMITS'] = "��èӡѴ�ӹǹ��������ẹ����";
$MESS ['AD_ADV_TYPE'] = "������ẹ����:";
$MESS ['AD_MAX_SHOW_COUNT'] = "�ӹǹ�٧�ش���͹حҵ��������<br>�ͧ�ѭ�Һ�ԡ�ù��:";
$MESS ['AD_SHOW_COUNT'] = "�ӹǹ����ʴ�:";
$MESS ['AD_SHOW_INTERVAL'] = "��ǧ�ѹ��������";
$MESS ['AD_SHOW_PAGES'] = "˹�������ǹ��ҧ� ���͹حҵ�����������ѭ��:";
$MESS ['AD_SHOW_PAGES_ALT'] = "�����˵�: ��èТ�鹺�÷Ѵ���������˹�ҷ���ͧ���<br>�ҡ��ͧ�������ʴ��ء˹�� (��������ҧ���)";
$MESS ['AD_NOT_SHOW_PAGES'] = "˹�������ǹ��ҧ� ������͹حҵ�����������ѭ��:";
$MESS ['AD_NOT_SHOW_PAGES_ALT'] = "�����˵�: ��èТ�鹺�÷Ѵ���������˹�ҷ���ͧ���<br>�ҡ��ͧ�������ʴ��ء˹�� (��������ҧ���)";
$MESS ['AD_WEEKDAY'] = "�ѹ������ҷ���ͧ��������:";
$MESS ['AD_SUNDAY'] = " ��.";
$MESS ['AD_MONDAY'] = " �.";
$MESS ['AD_TUESDAY'] = " �.";
$MESS ['AD_WEDNESDAY'] = " �.";
$MESS ['AD_THURSDAY'] = " ��.";
$MESS ['AD_FRIDAY'] = " �.";
$MESS ['AD_SATURDAY'] = " �.";
$MESS ['AD_OWNER_PERMISSIONS'] = "��á�˹��Է����������ҹ (����Ѻ��Ңͧ�ѭ��)";
$MESS ['AD_VIEW_STATISTICS'] = "���¡��:";
$MESS ['AD_MANAGE_BANNERS'] = "�Ѵ���ẹ����:";
$MESS ['AD_EDIT_CONTRACT'] = "�������¹�Է���ͧ�����ҹ����Ңͧ�ѭ��:";
$MESS ['AD_ADMIN_COMMENTS'] = "��͸Ժ������ͧ��ô���";
$MESS ['AD_SET_GROUPS_PERMISSIONS'] = "��駤���Է���ͧ�����";
$MESS ['AD_SHOWN'] = "<b>�ʴ�����</b><br> / �ʴ��٧�ش����˹�:";
$MESS ['AD_BANNER_COUNT'] = "�ӹǹẹ����:";
$MESS ['AD_DEFAULT_STATUS'] = "��á�˹�ʶҹ��������㹡�����ҧ������ẹ����:";
$MESS ['AD_ALL_TYPIES'] = "(��������������������)";
$MESS ['AD_MAX_CLICK_COUNT'] = "�ӹǹ��ԡ�٧�ش<br>�ͧ�ѭ�Һ�ԡ�ù��:";
$MESS ['AD_CLICKS'] = "<b>��ԡ����</b><br> / �ʴ��٧�ش����˹�:";
$MESS ['AD_CLICKED'] = "��ԡ:";
$MESS ['AD_CONTRACT_EDIT'] = "��Ѻ��ا��õ�駤���ѭ��";
$MESS ['AD_RED_ALT'] = "�ѭ�Һ�ԡ�ù����ҹ��������ͤú��˹�������������:";
$MESS ['AD_GREEN_ALT'] = "�ѭ�Һ�ԡ�ù����ҹ��:";
$MESS ['AD_SORT'] = "�ش���:";
$MESS ['AD_ADD_NEW_CONTRACT'] = "�����ѭ�Һ�ԡ������ (contract)";
$MESS ['AD_ADD_NEW_CONTRACT_TITLE'] = "����˹���ѭ�Һ�ԡ������ (contract)";
$MESS ['AD_DELETE_CONTRACT'] = "ź�ѭ�Һ�ԡ�� (contract)";
$MESS ['AD_DELETE_CONTRACT_TITLE'] = "ź��ë���-����ѭ�Һ�ԡ�� (contract)";
$MESS ['AD_DELETE_CONTRACT_CONFIRM'] = "�س��ͧ��÷���ź�ѭ�ҹ�����ẹ������������������§�Ѻ�ѭ�ҹ���������?";
$MESS ['AD_CONTRACT_STATISTICS'] = "�١�ҿ CTR �ͧ�ѭ�ҹ��";
$MESS ['AD_CONTRACT_VIEW_SETTINGS'] = "�١�õ�駤���ѭ��";
$MESS ['AD_CONTRACT_STATISTICS_TITLE'] = "�١�ҿ�ѭ�Һ�ԡ��  CTR ";
$MESS ['AD_CONTRACT_VIEW_SETTINGS_TITLE'] = "���ѭ�Һ�ԡ�� parameters";
$MESS ['AD_CONTRACT_EDIT_TITLE'] = "����ѭ�Һ�ԡ�� parameters";
$MESS ['AD_CTR'] = "�ѵ�� CTR (%):";
$MESS ['AD_VISITORS'] = "<b>�Դ������</b><br> / �Դ���٧�ش����˹�:";
$MESS ['AD_VISITORS_2'] = "�����������:";
$MESS ['AD_MAX_VISITOR_COUNT'] = "�ӹǹ������������٧�ش���͹حҵ<br>����Դ��ẹ����ͧ�ѭ�ҹ��:";
$MESS ['AD_SET'] = "��駤��";
$MESS ['AD_NOT_SET'] = "�ѧ������駤��";
$MESS ['AD_KEYWORDS'] = "Keywords:";
$MESS ['AD_KEYWORDS_ALT'] = "�����˵�: ��èТ�鹺�÷Ѵ��������Ф�������촷���ͧ���";
$MESS ['AD_SITE'] = "���䫵�:";
$MESS ['AD_USER_ALT'] = "�ټ������ǹ����";
$MESS ['AD_TYPE_ALT'] = "��ẹ������������ǻ�Ъ�����ѹ��";
$MESS ['AD_SITE_ALT'] = "����������´���䫵�";
$MESS ['AD_BANNER_ALT'] = "����¡�âͧẹ����";
$MESS ['AD_PAGE_ALT'] = "��˹�����䫵�";
$MESS ['AD_CONTRACT_ALT'] = "����������´�ѭ�Һ�ԡ��";
$MESS ['AD_TAB_CONTRACT'] = "�ѭ�Һ�ԡ��";
$MESS ['AD_TAB_TITLE_CONTRACT'] = "�ѭ�Һ�ԡ�� parameters";
$MESS ['AD_TAB_LIMIT'] = "�١�ӡѴ";
$MESS ['AD_TAB_ACCESS'] = "��Ҷ֧";
$MESS ['AD_TAB_COMMENT'] = "��͸Ժ��";
$MESS ['AD_TAB_TITLE_TARG'] = "��á�˹�������¢ͧẹ����";
$MESS ['AD_TAB_TARG'] = "��á�˹��������";
$MESS ['AD_STATUS'] = "ʶҹ�:";
$MESS ['ADV_NO_LIMITS'] = "(���ӡѴ)";
$MESS ['ADV_NOT_SET'] = "(���੾����Ш�)";
?>