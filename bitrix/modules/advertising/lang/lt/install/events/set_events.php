<?
$MESS['ADV_BANNER_STATUS_CHANGE_NAME'] = 'Keisti banerio status�';
$MESS['ADV_BANNER_STATUS_CHANGE_DESC'] = '#EMAIL_TO# - El.pa�tas prane�imo gav�jo
(#OWNER_EMAIL#) #ADMIN_EMAIL# - El.pa�tas naudotoj� turin�i� \"administratorius\" bei \"baneri� valdytojas\" statusus
#ADD_EMAIL# - El.pa�tas naudotoj� turin�i� teis� valdyti kontrakto banerius
#STAT_EMAIL# - El.pa�tas naudotoj� turin�i� teis� per�i�r�ti kontrakto banerius
#EDIT_EMAIL# - El.pa�tas naudotoj� turin�i� teis� modifikuoti kaikuriuos kontrakto laukus
#OWNER_EMAIL# - El.pa�tas naudotoj� turin�i� kokias nors teises � kontrakt�
#BCC# - Pasl�pta kopija
(#ADMIN_EMAIL#) #ID# - Banerio ID
#CONTRACT_ID# - Kontrakto ID
#CONTRACT_NAME# - Kontrakto pavadinimas
#TYPE_SID# - Tipo ID
#TYPE_NAME# - Tipo pavadinimas
#STATUS# - statusas
#STATUS_COMMENTS# - Statuso komentaras
#NAME# - Banerio pavadinimas
#GROUP_SID# - Banerio grup�
#INDICATOR# - Atvaizduoti baner� svetain�je?
#ACTIVE# - �ym� ar baneris aktyvus [Y | N]
#MAX_SHOW_COUNT# - Maksimalus banerio parodym� skai�ius
#SHOW_COUNT# - Kiek kart� baneris buvo rodomas svetain�je
#MAX_CLICK_COUNT# - Maksimalus banerio paspaudim� skai�ius
#CLICK_COUNT# - Kiek kart� paspaud� baner�
#DATE_LAST_SHOW# - Paskutin� banerio parodymo data
#DATE_LAST_CLICK# - Paskutin� banerio paspaudimo data
#DATE_SHOW_FROM# - Banerio rodymo prad�ios data
#DATE_SHOW_TO# - Banerio rodymo pabaigos data
#IMAGE_LINK# - Banerio vaizdo nuoroda
#IMAGE_ALT# - Alternatyvus banerio tekstas
#URL# - URL ant vaizdo
#URL_TARGET# - Kur atidaryti vaizdo URL
#CODE# - Banerio kodas
#CODE_TYPE# - Banerio kodo tipas (text | html)
#COMMENTS# - Banerio komentarai
#DATE_CREATE# - Banerio suk�rimo data
#CREATED_BY# - Kas suk�r� baneris
#DATE_MODIFY# - Banerio modifikavimo data
#MODIFIED_BY# - Kas modifikavo';
$MESS['ADV_BANNER_STATUS_CHANGE_SUBJECT'] = '[BID##ID#] #SITE_NAME#: Pasikeit� banerio statusas - [#STATUS#]';
$MESS['ADV_BANNER_STATUS_CHANGE_MESSAGE'] = '';
$MESS['ADV_CONTRACT_INFO_NAME'] = '';
$MESS['ADV_CONTRACT_INFO_DESC'] = '';
$MESS['ADV_CONTRACT_INFO_SUBJECT'] = '';
$MESS['ADV_CONTRACT_INFO_MESSAGE'] = '';
?>