<?
$MESS["AD_INSTALL_MODULE_NAME"] = "Reklamos ir baneriai";
$MESS["AD_INSTALL_MODULE_DESCRIPTION"] = "Reklamos valdymo modulis.";
$MESS["AD_INSTALL_ADV"] = "Reklama svetain�je";
$MESS["AD_INSTALL_ADV_ALT"] = "Reklamos valdymas";
$MESS["AD_INSTALL_TYPE"] = "Reklamos tipai";
$MESS["AD_INSTALL_TYPE_ALT"] = "Reklamos tip� nustatymai";
$MESS["AD_INSTALL"] = "Reklamos valdymo modulio diegimas";
$MESS["AD_DELETE_TITLE"] = "Reklamos valdymo modulio �alinimas";
$MESS["AD_ATTENTION"] = "D�mesio!Modulis bus pa�alintas.";
$MESS["AD_SAVE_TABLES"] = "Nor�dami i�saugoti duomenis, saugomus duomen� baz�s lentel�se, patikrinkite &quot;Save Tables&quot; langel�";
$MESS["AD_SAVE_DATA"] = "I�saugoti lenteles";
$MESS["AD_DELETE"] = "Pa�allinti";
$MESS["AD_REMOVING_COMPLETED"] = "Pa�alinimas atliktas.";
$MESS["AD_BACK"] = "Gr��ti";
$MESS["AD_ERRORS"] = "Klaidos:";
$MESS["AD_INSTALLATION_COMPLETED"] = "Diegimas atliktas.";
$MESS["AD_DENIED"] = "atsisakyti
";
$MESS["AD_ADVERTISER"] = "reklamuotojas";
$MESS["AD_DEMO"] = "demo prieiga";
$MESS["AD_BANNERS_MANAGER"] = "banerio vadybininkas";
$MESS["AD_ADMIN"] = "reklamos administratorius";
?>