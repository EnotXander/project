<?
$MESS["AD_ERROR_FORGOT_SID"] = "Klaida! Nepavyko �vesti simbolin� identifikatori� (ID).";
$MESS["AD_ERROR_INCORRECT_SID"] = "Klaida! Neteisingas simbolinis identifikatorius (leid�iamos tik lotyn� raid�s ir  pabraukimo simbolis \"_\")";
$MESS["AD_ERROR_SID_EXISTS"] = "Klaida! Identifikatorius \"#SID#\" jau yra naudojamas.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_CONTRACT"] = "Klaida! Prieiga prie sutarties negalima.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_BANNER"] = "Klaida! Prieiga prie banerio negalima.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_FOR_NEW_CONTRACT"] = "Klaida! Nepakankamai leidim� sudaryti sutart�.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_TYPE"] = "Klaida! Prieiga prie tip� negalima.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_FOR_CREATE_TYPE"] = "Klaida! Nepakankamai leidim� sudaryti tip�.";
$MESS["AD_ERROR_WRONG_DATE_MODIFY_FROM"] = "Klaida! �veskite teising� pakeitimo dat� \"nuo\".";
$MESS["AD_ERROR_WRONG_DATE_MODIFY_TILL"] = "Klaida! �veskite teising� pakeitimo dat� \"iki\".";
$MESS["AD_ERROR_FROM_TILL_DATE_MODIFY"] = "Klaida! Pakeitimo dat� \"iki\" turi b�ti v�lesn� nei \"nuo\".";
$MESS["AD_ERROR_WRONG_DATE_SHOW_FROM_CONTRACT"] = "Klaida! Neteisinga prad�ios data lauke \"Baneri� parodym� laikotarpis\".";
$MESS["AD_ERROR_WRONG_DATE_SHOW_TO_CONTRACT"] = "Klaida! Neteisinga pabaigos data lauke \"Baneri� parodym� laikotarpis\".";
$MESS["AD_ERROR_WRONG_DATE_SHOW_FROM_BANNER"] = "Klaida! Neteisinga prad�ios data lauke \"Parodyti laikotarp�\".";
$MESS["AD_ERROR_WRONG_DATE_SHOW_TO_BANNER"] = "Klaida! Neteisinga pabaigos data lauke \"Parodyti laikotarp�\".";
$MESS["AD_ERROR_INCORRECT_CONTRACT_ID"] = "Klaida! Neteisingas sutarties ID";
$MESS["AD_ERROR_INCORRECT_BANNER_ID"] = "Klaida! Neteisingas banerio ID";
$MESS["AD_ERROR_WRONG_PERIOD_FROM"] = "Klaida! �veskite teising� dat� \"nuo\" lauke \"Laikotarpis\"";
$MESS["AD_ERROR_WRONG_PERIOD_TILL"] = "Klaida! �veskite teising� dat� \"iki\" lauke \"Laikotarpis\"";
$MESS["AD_ERROR_FROM_TILL_PERIOD"] = "Klaida! Data \"iki\" �iame filtre turi b�ti v�lesn� nei \"nuo\"";
$MESS["AD_CONTRACT_DISABLE"] = "D�mesio! Sutarties apribojimai i�jungti modulio nustatymuose.";
$MESS["AD_ERROR_FROMTO_DATE_HAVETOBE_SET"] = "Klaida! Parodym� laiko intervalas turi b�ti apibr��tas.";
$MESS["AD_ERROR_FIXSHOW_HAVETOBE_SET"] = "Klaida! �i parinktis turi b�ti �jungta: \"Sekti banerio parodymus\"";
$MESS["AD_ERROR_MAX_SHOW_COUNT_HAVETOBE_SET"] = "Klaida! �is parametras turi b�ti nustatytas: \"Daugiausia parodym�\"";
?>