<?
$MESS["AD_EDIT_TYPE"] = "Tipas \\\"#SID#\\\"";
$MESS["AD_NEW_TYPE"] = "Naujas tipas";
$MESS["AD_BACK_TO_TYPE_LIST"] = "Tip� s�ra�as";
$MESS["AD_CREATED"] = "Sukurtas:";
$MESS["AD_MODIFIED"] = "Modifikuotas:";
$MESS["AD_ACTIVE"] = "Aktyvus:";
$MESS["AD_SORT"] = "R��iavimas:";
$MESS["AD_NAME"] = "Vardas:";
$MESS["AD_SID"] = "Simbolinis identifikatorius (ID):";
$MESS["AD_SID_ALT"] = "(galima naudoti tik lotini�kas raid�s, skai�iai bei simbolis \\\"_\\\")";
$MESS["AD_DESCRIPTION"] = "Apra�ymas:";
$MESS["AD_ADD_NEW_TYPE"] = "Prid�ti tip�";
$MESS["AD_DELETE_TYPE"] = "�alinti tip�";
$MESS["AD_DELETE_TYPE_CONFIRM"] = "Ar tikrai norite pa�alinti tip� ir visus banerius susietus su juo?";
$MESS["AD_STATISTICS"] = "CTR grafikas banerio tipo";
$MESS["AD_TYPE"] = "Banerio tipas";
$MESS["AD_USER_ALT"] = "Per�i�r�ti informacij� apie naudotoj�";
$MESS["AD_TYPE_VIEW_SETTINGS"] = "Per�i�r�ti tip�";
$MESS["AD_TYPE_VIEW_SETTINGS_TITLE"] = "Per�i�r�ti banerio tipo parametrus";
$MESS["AD_TYPE_EDIT"] = "Keisti tip�";
$MESS["AD_TYPE_EDIT_TITLE"] = "Keisti banerio tip�";
$MESS["AD_STATISTICS_TITILE"] = "Per�i�r�ti �io tipo baneri� statistik�";
$MESS["AD_ADD_NEW_TYPE_TITLE"] = "Prid�ti nauj� banerio tip�";
?>