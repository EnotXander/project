<?
$MESS["IM_NS_MAIN_RATING_VOTE"] = "Notificación de voto (\"Me gusta\")";
$MESS["IM_NS_BIZPROC_ACTIVITY"] = "Notificación sobre la actividad de proceso de negocio";
$MESS["IM_NS_DEFAULT"] = "Notificaciones no definidas";
$MESS["IM_NS_MESSAGE"] = "Mensajes privados";
$MESS["IM_NS_MAIN"] = "Valoraciones y Me Gusta";
$MESS["IM_NS_MAIN_RATING_VOTE_MENTIONED"] = "Notificación del derecho de voto en los posts que lo mencionan";
?>