<?
$MESS["IM_ERROR_EMPTY_FROM_USER_ID"] = "No se especifica el remitente del mensaje.";
$MESS["IM_ERROR_EMPTY_TO_CHAT_ID"] = "No se especifica el destinatario del mensaje.";
$MESS["IM_ERROR_EMPTY_MESSAGE"] = "El texto del mensaje est� vac�o.";
$MESS["IM_MESSAGE_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["IM_MESSAGE_FORMAT_TIME"] = "g:i a";
$MESS["IM_ERROR_EMPTY_CHAT_ID"] = "No se encuentra el ID del chat.";
$MESS["IM_ERROR_EMPTY_USER_ID"] = "El ID de usuario no se ha especificado.";
$MESS["IM_ERROR_EMPTY_USER_OR_CHAT"] = "No se especifica el chat o ID de usuario.";
$MESS["IM_ERROR_USER_NOT_FOUND"] = "El usuario especificado no est� en el chat.";
$MESS["IM_ERROR_KICK"] = "S�lo el propietario del chat puede prohibir a los usuarios.";
$MESS["IM_ERROR_AUTHORIZE_ERROR"] = "S�lo los miembros de chat pueden invitar a otros usuarios.";
$MESS["IM_ERROR_NOTHING_TO_ADD"] = "Todos los usuarios seleccionados ya est�n en el chat.";
$MESS["IM_ERROR_MIN_USER"] = "Por favor, seleccione los usuarios antes de crear un nuevo chat.";
$MESS["IM_ERROR_MAX_USER"] = "No puede incluir al chat m�s de #COUNT# usuarios.";
$MESS["IM_CHAT_JOIN_M"] = "#USER_1_NAME# invit� a #USER_2_NAME# al chat.";
$MESS["IM_CHAT_JOIN_F"] = "#USER_1_NAME# invit� a #USER_2_NAME# al chat.";
$MESS["IM_CHAT_KICK_M"] = "#USER_1_NAME# expuls� a #USER_2_NAME# del chat.";
$MESS["IM_CHAT_KICK_F"] = "#USER_1_NAME# expuls� a #USER_2_NAME# from chat.";
$MESS["IM_CHAT_CHANGE_TITLE_M"] = "#USER_NAME# cambi� el t�tulo del chat a \"#CHAT_TITLE#\".";
$MESS["IM_CHAT_CHANGE_TITLE_F"] = "#USER_NAME# cambi� el t�tulo del chat a \"#CHAT_TITLE#\".";
$MESS["IM_CHAT_LEAVE_M"] = "#USER_NAME# dej� el chat.";
$MESS["IM_CHAT_LEAVE_F"] = "#USER_NAME# dej� el chat.";
$MESS["IM_ERROR_EMPTY_USER_ID_BY_PRIVACY"] = "Estos usuarios negaron siu invitaci�n al chat";
$MESS["IM_ERROR_MIN_USER_BY_PRIVACY"] = "No se puede iniciar el chat porque los usuarios niegan la invitaci�n";
?>