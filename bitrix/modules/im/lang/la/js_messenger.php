<?
$MESS["IM_STATUS_ONLINE"] = "En l�nea";
$MESS["IM_STATUS_OFFLINE"] = "Sin conexi�n";
$MESS["IM_STATUS_DND"] = "No molestar";
$MESS["IM_MESSENGER_NEW_MESSAGE"] = "Nuevo mensaje";
$MESS["IM_MESSENGER_NO_MESSAGE"] = "No hay mensajes";
$MESS["IM_MESSENGER_HISTORY"] = "Registro de mensajes";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL"] = "Borrar todos los mensajes";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL_CONFIRM"] = "�Est� seguro de que desea borrar todos los registros?";
$MESS["IM_MESSENGER_HISTORY_DELETE"] = "Eliminar mensaje";
$MESS["IM_MESSENGER_HISTORY_DELETE_CONFIRM"] = "�Est� seguro que desea eliminar este mensaje?";
$MESS["IM_MESSENGER_LOAD_MESSAGE"] = "Cargar mensajes";
$MESS["IM_MESSENGER_SEND_FILE"] = "Enviar archivo";
$MESS["IM_MESSENGER_SEND_MESSAGE"] = "Enviar";
$MESS["IM_MESSENGER_NOT_DELIVERED"] = "no se entreg� el mensaje";
$MESS["IM_MESSENGER_DELIVERED"] = "mensaje enviado";
$MESS["IM_MESSENGER_CONTACT_LIST"] = "Contactos";
$MESS["IM_MESSENGER_VIEW_OFFLINE"] = "Mostrar u ocultar contactos sin conexi�n";
$MESS["IM_MESSENGER_VIEW_GROUP"] = "Mostrar u ocultar grupos de usuarios";
$MESS["IM_MESSENGER_WRITE_MESSAGE"] = "Enviar mensaje";
$MESS["IM_MESSENGER_OPEN_HISTORY"] = "Ver registro de mensaje";
$MESS["IM_MESSENGER_OPEN_VIDEO"] = "Video llamada";
$MESS["IM_MESSENGER_OPEN_PROFILE"] = "Perfil de usuario";
$MESS["IM_MESSENGER_MESSAGES"] = "Mensaje";
$MESS["IM_MESSENGER_CL_EMPTY"] = "-No hay contactos-";
?>