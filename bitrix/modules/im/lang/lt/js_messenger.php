<?
$MESS["IM_STATUS_ONLINE"] = "Prisijung�";
$MESS["IM_STATUS_OFFLINE"] = "Neprisijung�";
$MESS["IM_STATUS_DND"] = "Netrukdykite";
$MESS["IM_MESSENGER_NEW_MESSAGE"] = "Naujas prane�imas";
$MESS["IM_MESSENGER_NO_MESSAGE"] = "N�ra prane�im�";
$MESS["IM_MESSENGER_HISTORY"] = "Prane�im� �urnalas";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL"] = "Pa�alinti visus prane�imus";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL_CONFIRM"] = "Ar tikrai norite pa�alinti visus �urnalo �ra�us?";
$MESS["IM_MESSENGER_HISTORY_DELETE"] = "Pa�alinti prane�im�";
$MESS["IM_MESSENGER_HISTORY_DELETE_CONFIRM"] = "Ar tikrai norite pa�alinti �� prane�im�?";
$MESS["IM_MESSENGER_LOAD_MESSAGE"] = "�kelti prane�imus";
$MESS["IM_MESSENGER_SEND_FILE"] = "Si�sti fail�";
$MESS["IM_MESSENGER_SEND_MESSAGE"] = "Si�sti ";
$MESS["IM_MESSENGER_NOT_DELIVERED"] = "prane�imas nebuvo pristatytas";
$MESS["IM_MESSENGER_DELIVERED"] = "prane�imas siun�iamas";
$MESS["IM_MESSENGER_CONTACT_LIST"] = "Kontaktai";
$MESS["IM_MESSENGER_VIEW_OFFLINE"] = "Rodyti arba sl�pti ne�sijungusiuosius";
$MESS["IM_MESSENGER_VIEW_GROUP"] = "Rodyti arba sl�pti naudotoj� grupes";
$MESS["IM_MESSENGER_WRITE_MESSAGE"] = "Si�sti prane�im�";
$MESS["IM_MESSENGER_OPEN_HISTORY"] = "Per�i�r�ti prane�im� �urnal�";
$MESS["IM_MESSENGER_OPEN_VIDEO"] = "Vaizdo skambutis";
$MESS["IM_MESSENGER_OPEN_PROFILE"] = "Naudotojo profilis";
$MESS["IM_MESSENGER_MESSAGES"] = "Prane�imas";
$MESS["IM_MESSENGER_CL_EMPTY"] = "- N�ra kontakt� -";
?>