<?
$MESS['TITLE_SECTION_counter'] = 'Statistics System';
$MESS['TITLE_SECTION_meta'] = 'Control system of indexation';

$MESS['LABEL_GA'] = 'Code Google Analytics';
$MESS['DESC_GA'] = 'Address to add the site and obtain the code: <a href="https://www.google.com/analytics/" target="_blank">https://www.google.com/analytics/</a>';

$MESS['LABEL_YA'] = 'Code Yandex.Metrica';
$MESS['DESC_YA'] = 'Address to add the site and obtain the code: <a href="http://metrika.yandex.com/" target="_blank">http://metrika.yandex.com/</a>';

$MESS['LABEL_LI'] = 'Code LiveInternet';
$MESS['DESC_LI'] = 'Address to add the site and obtain the code: <a href="http://www.liveinternet.ru/add/?lang=en" target="_blank">http://www.liveinternet.ru/add/?lang=en</a>';

$MESS['LABEL_GW'] = 'Мета-тег Google Webmasters';
$MESS['DESC_GW'] = 'Address to add the site and get meta tag: <a href="https://www.google.com/webmasters/" target="_blank">https://www.google.com/webmasters/</a>';

$MESS['LABEL_YW'] = 'Meta tag Yandex.Webmaster';
$MESS['DESC_YW'] = 'Address to add the site and get meta tag: <a href="http://webmaster.yandex.com/" target="_blank">http://webmaster.yandex.com/</a>';


$MESS['BUTTON_SAVE']  = 'Save';
$MESS['BUTTON_RESET'] = 'Reset';

