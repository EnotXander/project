<?
$MESS['FILE_NOT_FOUND'] = 'File #FILENAME#.php is not found in the specified template (#TEMPLATE_ID#)';
$MESS['FILE_NOT_WR'] = 'File #FILENAME#.php in the specified template (#TEMPLATE_ID#) not available for read/write';
$MESS['FILE_NOT_TAG'] = 'Could not find the tag &lt;/#TAG#&gt; in the file #FILENAME#.php in the specified template (#TEMPLATE_ID#)';

$MESS['TPL_NOT_SELECT'] = 'The template is not selected!';
$MESS['TPL_NOT_FOUND'] = 'The specified template is not found(#TPL_PATH#)!';

$MESS['DATA_NOT_SAVE'] = 'Could not save data';
$MESS['CODE_NOT_SAVE'] = 'Could not connect to the code #CODE_ID#';