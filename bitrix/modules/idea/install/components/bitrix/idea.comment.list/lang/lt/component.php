<?
$MESS["BLOG_MODULE_NOT_INSTALL"] = "Blog� modulis n�ra �diegtas.";
$MESS["IDEA_MODULE_NOT_INSTALL"] = "Id�j� modulis n�ra �diegtas.";
$MESS["B_B_PC_CAPTCHA_ERROR"] = "CAPTCHA kodas, kur� j�s �ved�te, yra neteisingas.";
$MESS["B_B_PC_COM_ERROR"] = "Klaida pridedant komentar�:";
$MESS["B_B_PC_NO_RIGHTS"] = "Nepakankamas leidimas prid�ti komentarus.";
$MESS["B_B_PC_NO_COMMENT"] = "Reikia komentaro teksto.";
$MESS["B_B_PC_NO_ANAME"] = "Reikia pavadinimo.";
$MESS["B_B_PC_COM_ERROR_EDIT"] = "Klaida atnaujinant komentar�:";
$MESS["B_B_PC_COM_ERROR_LOST"] = "Komentaras nerastas.";
$MESS["B_B_PC_NO_RIGHTS_EDIT"] = "Nepakankamas leidimas redaguoti komentarus.";
$MESS["B_B_PC_EDIT_ALREADY_COMMENTED"] = "J�s� komentaro komentaras jau sukurtas.";
$MESS["B_B_PC_MES_DELED"] = "Komentaras buvo s�kmingai pa�alintas.";
$MESS["B_B_PC_MES_ERROR_DELETE"] = "Klaida �alinant komentar�.";
$MESS["B_B_PC_MES_ERROR_SHOW"] = "Klaida rodant komentar�.";
$MESS["B_B_PC_MES_ERROR_HIDE"] = "Klaida slepiant komentar�.";
$MESS["B_B_PC_MES_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["B_B_PC_HIDDEN_POSTED"] = "You komentaras buvo s�kmingai prid�tas. Komentarai �iame bloge yra moderuojami; j�s� komentaras bus matomas, kai blogo savininkas patvirtins j�.";
$MESS["B_B_PC_MES_ERROR_SESSION"] = "J�s� sesija pasibaig�. Pra�ome ra�yti savo komentar� dar kart�.";
$MESS["B_B_PC_MES_HIDDEN_ADDED"] = "You komentaras buvo s�kmingai prid�tas. Komentarai �iame bloge yra moderuojami; j�s� komentaras bus matomas, kai blogo savininkas patvirtins j�.";
$MESS["B_B_PC_MES_NOCOMMENTREASON_L"] = "Sistemos parametrai draud�ia naudoti nuorodas.";
$MESS["B_B_PC_MES_NOCOMMENTREASON_A"] = "Neu�siregistrav� lankytojai negali �d�ti nuorodas. Pra�ome u�siregistruoti arba autorizuotis.";
$MESS["B_B_PC_MES_NOCOMMENTREASON_R"] = "J�s� reitingas yra per�emas. J�s negalite naudoti nuorodas. ";
$MESS["BPC_SONET_COMMENT_TITLE"] = "prid�tas komentaras prie \"#TITLE#\" ";
?>