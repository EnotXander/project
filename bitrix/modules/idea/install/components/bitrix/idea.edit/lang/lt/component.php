<?
$MESS["B_B_MES_NO_BLOG"] = "Blogas nerastas.";
$MESS["BLOG_POST_EDIT"] = "Redaguoti id�j� \"#IDEA_TITLE#\"";
$MESS["BLOG_ERR_NO_RIGHTS"] = "Pra�ome <a href='#' onclick='authFormWindow.ShowLoginForm();'>autorizuotis</a>, nor�dami prid�ti id�j�. ";
$MESS["BLOG_P_INSERT"] = "Paspauskite, nor�dami �terpti viazd�";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "Blogai neprieinami �iam naudotojui.";
$MESS["BLG_NAME"] = "Naudotojo blogai";
$MESS["BLG_GRP_NAME"] = "Grup�s blogas";
$MESS["BPE_HIDDEN_POSTED"] = "J�s� pateikimas buvo prid�tas. Komentarai �iame bloge yra moderuojami; j�s� komentaras bus matomas, kai blogo savininkas patvirtins j�.";
$MESS["BPE_SESS"] = "J�s� sesija pasibaig�. Pra�ome i�saugoti prane�im� dar kart�.";
$MESS["BPE_COPY_DELETE_ERROR"] = "Klaida bandant pa�alinti original� prane�im�.<br />";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "Blog� modulis n�ra �diegtas.";
$MESS["IDEA_NEW_MESSAGE"] = "Pasi�lyti nauj� id�j� ";
$MESS["IDEA_MODULE_NOT_INSTALL"] = "Id�j� modulis n�ra �diegtas.";
$MESS["IDEA_NEW_MESSAGE_SUCCESS"] = "Naujas prane�imas s�kmingai prid�tas.";
?>