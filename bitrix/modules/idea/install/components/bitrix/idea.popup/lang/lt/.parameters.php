<?
$MESS["ONE_BLOG_BLOG_URL"] = "Blogas";
$MESS["BC_POST_IBLOCK_CATEGORIES"] = "Kategorijos strukt�ros informaijos blokas";
$MESS["IDEA_PARAM_POST_BIND_STATUS_DEFAULT"] = "Numatytasis statusas naujoms id�joms ";
$MESS["BC_MESSAGE_COUNT"] = "Id�j� skai�ius kategorijoje";
$MESS["CATEGORIES_CNT"] = "Kategorij� kiekis";
$MESS["B_SHOW_RATING"] = "Naudoti reitingus";
$MESS["IDEA_PARAM_RATING_TEMPLATE_TITLE"] = "Reiting� �ablonas";
$MESS["IDEA_PARAM_RATING_TEMPLATE_STANDART"] = "Standartas";
$MESS["IDEA_PARAM_RATING_TEMPLATE_LIKE"] = "Man patinka (�viesus)";
$MESS["IDEA_PARAM_PATH_IDEA_INDEX"] = "Id�j� s�ra�o kelias";
$MESS["IDEA_PARAM_PATH_IDEA_POST"] = "Id�jo informacijos kelias";
$MESS["IDEA_PARAM_BUTTON_COLOR"] = "Mygtuko fono spalva";
$MESS["IDEA_PARAM_REGISTER_URL"] = "Pamir�t� slapta�od�i� puslapis ";
$MESS["IDEA_PARAM_FORGOT_PASSWORD_URL"] = "Registracijos puslapis";
$MESS["IDEA_DISABLE_SONET_LOG"] = "I�jungti veiklos srauto prane�imus";
$MESS["IDEA_DISABLE_EMAIL"] = "I�jungti el.pa�to prane�imus";
$MESS["IDEA_PARAM_AUTH_TEMPLATE"] = "Autentifikavimo formos �ablonas";
$MESS["BC_NAME_TEMPLATE"] = "Pavadinimo parodymo �ablonas";
$MESS["BC_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["BC_SHOW_LOGIN"] = "Rodyti prisijungimo vard�, jei truksta vardo";
$MESS["IDEA_DISABLE_RSS"] = "I�jungti RSS";
$MESS["BC_SEF_PATH_POST_RSS"] = "RSS prane�imo komentar� adresas";
$MESS["BC_SEF_PATH_RSS"] = "Blogo RSS adresas";
$MESS["BC_SEF_PATH_RSS_STATUS"] = "Blogo RSS su statusu URL";
$MESS["BC_SEF_PATH_RSS_CATEGORY"] = "Blogo RSS su kategorija URL";
$MESS["BC_SEF_PATH_RSS_CATEGORY_STATUS"] = "Blogo RSS su statusu ir kategorija URL";
$MESS["BC_SEF_PATH_RSS_USER_IDEAS"] = "Id�j� blogo RSS URL";
$MESS["BC_SEF_PATH_RSS_USER_IDEAS_STATUS"] = "Id�j� blogo RSS su statusu URL";
$MESS["BC_POST_IBLOCK_CATOGORIES"] = "Kategorijos strukt�ros informacijos blokas";
$MESS["CATOGORIES_CNT"] = "Kategorij� skai�ius";
?>