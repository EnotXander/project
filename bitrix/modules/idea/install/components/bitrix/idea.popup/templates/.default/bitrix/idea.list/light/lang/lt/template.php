<?
$MESS["BLOG_BLOG_BLOG_MORE"] = "Skaityti daugiau...";
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "�ym�s:";
$MESS["BLOG_BLOG_BLOG_COMMENTS"] = "Komentarai:";
$MESS["BLOG_BLOG_BLOG_VIEWS"] = "Per�i�ros:";
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "N�ra id�j� �ioje kategorijoje.";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "Ar tikrai norite pa�alinti s� prane�im�?";
$MESS["BLOG_MES_EDIT"] = "Redaguoti";
$MESS["BLOG_MES_DELETE"] = "Pa�alinti";
$MESS["BLOG_MES_HIDE"] = "Sl�pti";
$MESS["BLOG_MES_HIDE_POST_CONFIRM"] = "Ar tikrai norite pasl�pti s� prane�im�?";
$MESS["IDEA_STATUS_NEW"] = "Naujas";
$MESS["IDEA_STATUS_PROCESSING"] = "Vykdoma";
$MESS["IDEA_STATUS_COMPLETED"] = "�gyvendinama";
$MESS["IDEA_RATING_TITLE"] = "Reitingas";
$MESS["IDEA_INTRODUCED_TITLE"] = "Pasi�l�";
$MESS["IDEA_POST_DUBLICATE"] = "Id�jos, <a href='#LINK#'>pasi�lytos anks�iau</a>, kopija";
$MESS["IDEA_POST_DO_COMMENT"] = "Komentaras";
$MESS["IDEA_POST_COMMENT_CNT"] = "komentarai";
$MESS["IDEA_MES_SHOW"] = "Rodyti";
$MESS["IDEA_MES_SHOW_POST_CONFIRM"] = "Ar tikrai norite rodyti �� prane�im�?";
?>