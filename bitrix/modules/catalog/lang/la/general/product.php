<?
$MESS["KGP_EMPTY_ID"] = "El ID del producto no se especific�";
$MESS["BT_MOD_CATALOG_PROD_ERR_PRODUCT_ID_ABSENT"] = "El ID del producto est� faltando o es incorrecto.";
$MESS["BT_MOD_CATALOG_PROD_ERR_QUANTITY_ABSENT"] = "La cantidad del producto est� faltando o es incorrecta.";
$MESS["BT_MOD_CATALOG_PROD_ERR_NO_BASE_CURRENCY"] = "No puede resolver la base actual.";
$MESS["BT_MOD_CATALOG_PROD_ERR_ELEMENT_ID_NOT_FOUND"] = "El block de informaci�n del elemento ##ID# no fue encontrado.";
$MESS["BT_MOD_CATALOG_PROD_ERR_COST_CURRENCY"] = "El tipo de cambio de compra est� vac�a.";
$MESS["BT_MOD_CATALOG_PROD_PERIOD_HOUR"] = "Hora";
$MESS["BT_MOD_CATALOG_PROD_PERIOD_DAY"] = "D�a";
$MESS["BT_MOD_CATALOG_PROD_PERIOD_WEEK"] = "Semana";
$MESS["BT_MOD_CATALOG_PROD_PERIOD_MONTH"] = "Mes";
$MESS["BT_MOD_CATALOG_PROD_PERIOD_QUART"] = "Trimestre";
$MESS["BT_MOD_CATALOG_PROD_PERIOD_SEMIYEAR"] = "Medio a�o";
$MESS["BT_MOD_CATALOG_PROD_PERIOD_YEAR"] = "A�o";
?>