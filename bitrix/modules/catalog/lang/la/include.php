<?
$MESS["I_NO_PRODUCT"] = "N� de producto #ID# no fue encontrado";
$MESS["I_NO_TRIAL_PRODUCT"] = "N� de producto #ID# no fue encontrado (for trial no. #TRIAL_ID#)";
$MESS["I_PRODUCT_NOT_SUBSCR"] = "N� de producto #ID# no soporta la suscripci�n";
$MESS["I_NO_IBLOCK_ELEM"] = "N� de Block de Informaci�n #ID#  no fue encontrado";
$MESS["I_CATALOG_NOT_SUBSCR"] = "N� del C�talogo #ID# no soporta la suscripci�n";
$MESS["I_PRODUCT_SOLD"] = "N� de producto #ID#  est� fuera de stock";
$MESS["CAT_ERROR_IBLOCK_NOT_INSTALLED"] = "Error! M�dulo de Information Blocks no est� instalado.";
$MESS["CAT_ERROR_CURRENCY_NOT_INSTALLED"] = "Error! M�dulo Currency no est� instalado.";
$MESS["CAT_VAT_REF_NOT_SELECTED"] = "---no se seleccion�---";
$MESS["CAT_INCLUDE_CURRENCY"] = "M�dulo Commercial Catalog requiere el m�dulo Currency para ser instalado.";
$MESS["CATALOG_PRODUCT_PRICE_NOT_FOUND"] = "El precio del producto no se encontr�";
$MESS["CATALOG_ERR_EMPTY_PRODUCT_ID"] = "Falta el ID del producto.";
$MESS["CATALOG_ERR_NO_SALE_MODULE"] = "El m�dulo e-Store no est� instalado.";
$MESS["CATALOG_ERR_SESS_SEARCHER"] = "Un sistema de b�squeda no puede ser un comprador.";
$MESS["CATALOG_ERR_NO_PRODUCT"] = "El producto no fue encontrado.";
$MESS["CATALOG_ERR_PRODUCT_RUN_OUT"] = "El producto no est� en stock.";
$MESS["CATALOG_ERR_NO_IBLOCK_ELEMENT"] = "El elemento del block de informaci�n no fue encontrado.";
$MESS["CATALOG_ERR_NO_SUBSCRIBE"] = "Este producto no puede ser suscrito.";
?>