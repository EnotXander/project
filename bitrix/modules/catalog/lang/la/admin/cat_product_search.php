<?
$MESS["SPS_SEARCH_TITLE"] = "Productos";
$MESS["SPS_CATALOG"] = "Cat�logo";
$MESS["SPS_ID_FROM_TO"] = "inicio y final";
$MESS["SPS_TIMESTAMP"] = "Modificado";
$MESS["SPS_CHANGER"] = "Modificado por";
$MESS["SPS_ANY"] = "cualquier";
$MESS["SPS_STATUS"] = "Estado";
$MESS["SPS_SECTION"] = "Secci�n";
$MESS["SPS_TOP_LEVEL"] = "Nivel superior";
$MESS["SPS_INCLUDING_SUBS"] = "Subsecciones incluidas";
$MESS["SPS_ACTIVE"] = "Activo";
$MESS["SPS_YES"] = "Si";
$MESS["SPS_NO"] = "No";
$MESS["SPS_NAME"] = "Nombre";
$MESS["SPS_DESCR"] = "Descripci�n";
$MESS["SPS_SET"] = "Fijar";
$MESS["SPS_UNSET"] = "Resetear";
$MESS["SPS_ACT"] = "Activo";
$MESS["SPS_SELECT"] = "Selecionar";
$MESS["SPS_NO_PERMS"] = "Usted no tiene suficientes permisos para ver este cat�logo";
$MESS["SPS_CLOSE"] = "Cerrar";
$MESS["sale_prod_search_nav"] = "Mercader�as";
$MESS["prod_search_find"] = "Fijar filtro";
$MESS["prod_search_find_title"] = "Seleccionar registros coincidentes";
$MESS["prod_search_cancel"] = "Cancelar";
$MESS["prod_search_cancel_title"] = "Mostrar todos los registros";
?>