<?
$MESS["I_NO_PRODUCT"] = "Prek� nr. #ID# nerasta";
$MESS["I_NO_TRIAL_PRODUCT"] = "Prek� nr. #ID# nerasta (bandom�jam nr. #TRIAL_ID#)";
$MESS["I_PRODUCT_NOT_SUBSCR"] = "Prek� nr. #ID# nepalaiko prenumeratos";
$MESS["I_NO_IBLOCK_ELEM"] = "Nerastas bloko elementas nr. #ID# nerasta ";
$MESS["I_CATALOG_NOT_SUBSCR"] = "Katalogas nr. #ID# nepalaiko prenumeratos";
$MESS["I_PRODUCT_SOLD"] = "Prek�s nr. #ID# n�ra sand�lyje";
$MESS["CAT_ERROR_IBLOCK_NOT_INSTALLED"] = "Klaida! Informacijos blok� modulis n�ra �diegtas.";
$MESS["CAT_ERROR_CURRENCY_NOT_INSTALLED"] = "Klaida! Valiutos modulis n�ra �diegtas.";
$MESS["CAT_VAT_REF_NOT_SELECTED"] = "-- nepasirinkta --";
$MESS["CAT_INCLUDE_CURRENCY"] = "Komercinio katalogo modulis reikalauja, kad b�t� �diegtas valiutos modulis.";
$MESS["CATALOG_PRODUCT_PRICE_NOT_FOUND"] = "Prek�s kaina nerasta";
$MESS["CATALOG_ERR_EMPTY_PRODUCT_ID"] = "Tr�ksta prek�s ID.";
$MESS["CATALOG_ERR_NO_SALE_MODULE"] = "El.parduotuv�s modulis n�ra �diegtas. ";
$MESS["CATALOG_ERR_SESS_SEARCHER"] = "Paie�kos sistema negali b�ti pirk�ju.";
$MESS["CATALOG_ERR_NO_PRODUCT"] = "Prek� nerasta.";
$MESS["CATALOG_ERR_PRODUCT_RUN_OUT"] = "Prek�s n�ra sand�lyje.";
$MESS["CATALOG_ERR_NO_IBLOCK_ELEMENT"] = "Informacijos bloko elementas nerastas.";
$MESS["CATALOG_ERR_NO_SUBSCRIBE"] = "�ios prek�s negalima prenumeruoti. ";
?>