<?
$MESS["CLUWIZ_NO_MODULE_ERROR"] = "El m�dulo de la Web Cluster no est� instalado. El Asistente podr� ser anulado.";
$MESS["CLUWIZ_CHEKED"] = "Comprobado.";
$MESS["CLUWIZ_STEP2_DB_HOST"] = "Cadena de Conexi�n de la Base de Datos";
$MESS["CLUWIZ_STEP2_DB_NAME"] = "Base de Datos";
$MESS["CLUWIZ_STEP2_DB_NAME_HINT"] = "(el mismo nombre como la base de datos primaria)";
$MESS["CLUWIZ_STEP2_DB_LOGIN"] = "Usuario";
$MESS["CLUWIZ_STEP2_DB_PASSWORD"] = "Contrase�a";
$MESS["CLUWIZ_STEP2_MASTER_CONN"] = "Par�metros de la conexi�n de la base de datos primaria";
$MESS["CLUWIZ_STEP2_MASTER_HOST"] = "IP del Servidor o Nombre";
$MESS["CLUWIZ_STEP2_MASTER_PORT"] = "Puerto";
$MESS["CLUWIZ_STEP4_TITLE"] = "Verificaci�n de los Par�metros de la Base de Datos";
$MESS["CLUWIZ_FINALSTEP_TITLE"] = "Finalizando el Asistente";
$MESS["CLUWIZ_FINALSTEP_NAME"] = "Nombre de la Conexi�n";
$MESS["CLUWIZ_FINALSTEP_BUTTONTITLE"] = "Finalizar";
$MESS["CLUWIZ_CANCELSTEP_TITLE"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_CANCELSTEP_BUTTONTITLE"] = "Cerrar";
$MESS["CLUWIZ_CANCELSTEP_CONTENT"] = "El asistente ha sido cancelado.";
$MESS["CLUWIZ_DATABASE_NOT_SUPPORTED"] = "El asistente no soporta el tipo de base de datos especificada.";
$MESS["CLUWIZ_STEP1_TITLE"] = "Asistente de la nueva base de datos esclava";
$MESS["CLUWIZ_STEP1_CONTENT"] = "El asistente se conectar� una nueva base de datos esclava y verificar� los par�metros principales de la base de datos primaria esencial para la replicaci�n adecuada.<br />";
$MESS["CLUWIZ_STEP2_TITLE"] = "Par�metros de la Conexi�n de la Base de Datos Esclava";
$MESS["CLUWIZ_STEP4_CONN_ERROR"] = "La conexi�n de base de datos no se pudo establecer. Por favor, regrese al paso anterior y modifique los par�metros de conexi�n.";
$MESS["CLUWIZ_STEP4_SLAVE_IS_RUNNING"] = "La base de datos maestra est� seindo replicada a la base de datos espeficada.";
$MESS["CLUWIZ_NO_GROUP_ERROR"] = "El grupo del servidor  no est� definido.";
$MESS["CLUWIZ_NO_MASTER_ERROR"] = "Este servidor del grupo no tiene una base de datos maestra funcionando.";
?>