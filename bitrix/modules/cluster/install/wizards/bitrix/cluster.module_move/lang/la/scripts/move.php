<?
$MESS["CLUWIZ_ERROR_ACCESS_DENIED"] = "Error! Acceso Denegado.";
$MESS["CLUWIZ_NOMODULE_ERROR"] = "Error al obtener las tablas para su eliminaci�n.";
$MESS["CLUWIZ_CONNECTION_ERROR"] = "Error al conectar a la base de datos.";
$MESS["CLUWIZ_QUERY_ERROR"] = "Error al consultar la base de datos.";
$MESS["CLUWIZ_ALL_DONE1"] = "El m�dulo ha ido transferido exitosamente. La base de datos est� ahora en l�nea.";
$MESS["CLUWIZ_ALL_DONE2"] = "El m�dulo ha sido rnasferida exitosamente. La base de datos est� lista.";
$MESS["CLUWIZ_INIT"] = "Inicializando...";
$MESS["CLUWIZ_TABLE_PROGRESS"] = "La tabla #table_name#: filas movidas: #records#.";
?>