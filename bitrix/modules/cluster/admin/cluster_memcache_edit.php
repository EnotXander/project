<?
define("ADMIN_MODULE_NAME", "cluster");

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/cluster/include.php");

IncludeModuleLangFile(__FILE__);

if(!$USER->IsAdmin())
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$arServer = CClusterMemcache::GetByID($_REQUEST["ID"]);

$aTabs = array(
	array(
		"DIV" => "edit1",
		"TAB" => GetMessage("CLU_MEMCACHE_EDIT_TAB"),
		"ICON"=>"main_user_edit",
		"TITLE"=>GetMessage("CLU_MEMCACHE_EDIT_TAB_TITLE"),
	),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$strError = "";
$bVarsFromForm = false;

if(!extension_loaded('memcache'))
{
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	ShowError(GetMessage("CLU_MEMCACHE_NO_EXTENTION"));
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
}

if($REQUEST_METHOD == "POST" && check_bitrix_sessid())
{
	if($save!="" || $apply!="")
	{
		$ob = new CClusterMemcache;
		$arFields = array(
			"HOST" => $_POST["HOST"],
			"PORT" => $_POST["PORT"],
			"WEIGHT" => $_POST["WEIGHT"],
		);

		if(is_array($arServer))
			$res = $ob->Update($arServer["ID"], $arFields);
		else
			$res = $ob->Add($arFields);

		if($res)
		{
			if($apply!="")
				LocalRedirect("/bitrix/admin/cluster_memcache_edit.php?ID=".$res."&lang=".LANG."&".$tabControl->ActiveTabParam());
			else
				LocalRedirect("/bitrix/admin/cluster_memcache_list.php?lang=".LANG);
		}
		else
		{
			if($e = $APPLICATION->GetException())
				$message = new CAdminMessage(GetMessage("CLU_MEMCACHE_EDIT_SAVE_ERROR"), $e);
			$bVarsFromForm = true;
		}
	}
}

ClearVars("str_");

if($bVarsFromForm)
{
	$str_HOST = htmlspecialchars($_REQUEST["HOST"]);
	$str_PORT = htmlspecialchars($_REQUEST["PORT"]);
	$str_WEIGHT = htmlspecialchars($_REQUEST["WEIGHT"]);
}
elseif(is_array($arServer))
{
	$str_HOST = htmlspecialchars($arServer["HOST"]);
	$str_PORT = htmlspecialchars($arServer["PORT"]);
	$str_WEIGHT = htmlspecialchars($arServer["WEIGHT"]);
}
else
{
	$str_HOST = "";
	$str_PORT = "11211";
	$str_WEIGHT = "100";
}

$APPLICATION->SetTitle(is_array($arServer)? GetMessage("CLU_MEMCACHE_EDIT_EDIT_TITLE"): GetMessage("CLU_MEMCACHE_EDIT_NEW_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$arMemcacheServers = CClusterMemcache::LoadConfig();
if($ID == 0 && count($arMemcacheServers) > 0)
	echo BeginNote(), GetMessage("CLU_MEMCACHE_EDIT_WARNING", array("#link#"=>"perfmon_panel.php?lang=".LANGUAGE_ID)), EndNote();

$aMenu = array(
	array(
		"TEXT" => GetMessage("CLU_MEMCACHE_EDIT_MENU_LIST"),
		"TITLE" => GetMessage("CLU_MEMCACHE_EDIT_MENU_LIST_TITLE"),
		"LINK" => "cluster_memcache_list.php?lang=".LANG,
		"ICON" => "btn_list",
	)
);
$context = new CAdminContextMenu($aMenu);
$context->Show();

if($message)
	echo $message->Show();

?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>"  enctype="multipart/form-data" name="editform" id="editform">
<?
$tabControl->Begin();
?>
<?
$tabControl->BeginNextTab();
?>
	<?if(is_array($arServer)):?>
		<tr>
			<td><?echo GetMessage("CLU_MEMCACHE_EDIT_ID")?>:</td>
			<td><?echo $arServer["ID"];?></td>
		</tr>
	<?endif?>
	<tr>
		<td><?echo GetMessage("CLU_MEMCACHE_EDIT_HOST")?>:</td>
		<td><input type="text" size="20" name="HOST" value="<?echo $str_HOST?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("CLU_MEMCACHE_EDIT_PORT")?>:</td>
		<td><input type="text" size="6" name="PORT" value="<?echo $str_PORT?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("CLU_MEMCACHE_EDIT_WEIGHT")?>:</td>
		<td><input type="text" size="6" name="WEIGHT" value="<?echo $str_WEIGHT?>"></td>
	</tr>
<?
$tabControl->Buttons(
	array(
		"back_url"=>"cluster_memcache_list.php?lang=".LANG,
	)
);
?>
<?echo bitrix_sessid_post();?>
<input type="hidden" name="lang" value="<?echo LANG?>">
<?if(is_array($arServer)):?>
	<input type="hidden" name="ID" value="<?echo $arServer["ID"]?>">
<?endif;?>
<?
$tabControl->End();
?>
</form>
<?
$tabControl->ShowWarnings("editform", $message);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>