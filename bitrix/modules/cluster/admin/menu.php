<?
IncludeModuleLangFile(__FILE__);

if(!$USER->IsAdmin())
	return false;

$arMenu = array(
	"parent_menu" => "global_menu_settings",
	"section" => "cluster",
	"sort" => 1600,
	"text" => GetMessage("CLU_MENU_ITEM"),
	"title" => GetMessage("CLU_MENU_TITLE"),
	"url" => "cluster_index.php?lang=".LANGUAGE_ID,
	"icon" => "cluster_menu_icon",
	"page_icon" => "cluster_page_icon",
	"items_id" => "menu_cluster",
	"items" => array(
		array(
			"text" => GetMessage("CLU_MENU_DBNODE_ITEM"),
			"url" => "cluster_dbnode_list.php?lang=".LANGUAGE_ID,
			"more_url" => Array("cluster_dbnode_list.php", "cluster_dbnode_edit.php"),
			"title" => GetMessage("CLU_MENU_DBNODE_TITLE"),
		),
	),
);

if($DB->type == "MYSQL")
{
	$arMenu["items"][] = array(
		"text" => GetMessage("CLU_MENU_SLAVE_ITEM"),
		"url" => "cluster_slave_list.php?lang=".LANGUAGE_ID,
		"more_url" => Array("cluster_slave_list.php", "cluster_slave_edit.php"),
		"title" => GetMessage("CLU_MENU_SLAVE_ITEM_TITLE"),
	);
}

$arMenu["items"][] = array(
	"text" => GetMessage("CLU_MENU_MEMCACHE_ITEM"),
	"url" => "cluster_memcache_list.php?lang=".LANGUAGE_ID,
	"more_url" => Array("cluster_memcache_list.php", "cluster_memcache_edit.php"),
	"title" => GetMessage("CLU_MENU_MEMCACHE_ITEM_TITLE"),
);

$arMenu["items"][] = array(
	"text" => GetMessage("CLU_MENU_SESSION_ITEM"),
	"url" => "cluster_session.php?lang=".LANGUAGE_ID,
	"more_url" => Array("cluster_session.php"),
	"title" => GetMessage("CLU_MENU_SESSION_ITEM_TITLE"),
);

$arMenu["items"][] = array(
	"text" => GetMessage("CLU_MENU_WEBNODE_ITEM"),
	"url" => "cluster_webnode_list.php?lang=".LANGUAGE_ID,
	"more_url" => Array("cluster_webnode_list.php", "cluster_webnode_edit.php"),
	"title" => GetMessage("CLU_MENU_WEBNODE_ITEM_TITLE"),
);

return $arMenu;
?>
