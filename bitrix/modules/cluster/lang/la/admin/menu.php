<?
$MESS["CLU_MENU_ITEM"] = "Web Cluster";
$MESS["CLU_MENU_TITLE"] = "Administrar Web Cluster";
$MESS["CLU_MENU_MEMCACHE_ITEM_TITLE"] = "Administrar las Configuraciones del Cach�";
$MESS["CLU_MENU_SESSION_ITEM"] = "Sesiones";
$MESS["CLU_MENU_SESSION_ITEM_TITLE"] = "Administrar el Almacenamiento de las Sesiones en la Base de Datos";
$MESS["CLU_MENU_WEBNODE_ITEM"] = "Servidores Web";
$MESS["CLU_MENU_WEBNODE_ITEM_TITLE"] = "Estado de los Servidores Web Cluster";
$MESS["CLU_MENU_SLAVE_ITEM"] = "Replicaci�n";
$MESS["CLU_MENU_DBNODE_ITEM"] = "Base de datos sharding";
$MESS["CLU_MENU_DBNODE_TITLE"] = "Sharding vertical: administrar las conexiones de la base de datos";
$MESS["CLU_MENU_SLAVE_ITEM_TITLE"] = "Administrar la replicaci�n de los Nodos";
$MESS["CLU_MENU_MEMCACHE_ITEM"] = "Memcached";
$MESS["CLU_MENU_GROUP_ITEM"] = "Grupos de servidores";
$MESS["CLU_MENU_GROUP_ITEM_TITLE"] = "Administrar grupos de servidores";
$MESS["CLU_MENU_SERVER_ITEM"] = "Licencias";
$MESS["CLU_MENU_SERVER_ITEM_TITLE"] = "Los servidores requieren una licencia";
?>