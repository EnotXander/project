<?
$MESS["CLU_SESSION_SAVEDB_TAB"] = "Sesiones en la Base de datos";
$MESS["CLU_SESSION_SAVEDB_TAB_TITLE"] = "Configurar almacenamiento de la sesi�n de los datos en la base de datos";
$MESS["CLU_SESSION_DB_ON"] = "La sesi�n de datos est� almacenada en el M�dulo de Seguridad de la Base de Datos.";
$MESS["CLU_SESSION_DB_OFF"] = "No Almacenar la Data de la Sesi�n En el M�dulo de Seguridad de la Base de Datos";
$MESS["CLU_SESSION_DB_BUTTON_OFF"] = "No Almacenar la sesi�n de los datos en el M�dulo de Seguidad de la base de datos";
$MESS["CLU_SESSION_DB_BUTTON_ON"] = "Almacenar la Data de la Sesi�n En el M�dulo de Seguridad de la Base de Datos";
$MESS["CLU_SESSION_NO_SECURITY"] = "Se requiere el M�dulo de La \"Protecci�n Proactive\".";
$MESS["CLU_SESSION_TITLE"] = "Almacenar las Sesiones en la Base de Datos";
$MESS["CLU_SESSION_DB_WARNING"] = "�Atenci�n! Cambiar el modo de sesi�n o desactivar podr� hacer que los usuarios autorizados actualmente pierdan la autorizaci�n (los datos de sesi�n ser�n destruidos).";
$MESS["CLU_SESSION_SESSID_WARNING"] = "Identificador de sesi�n no es compatible con el m�dulo de protecci�n proactiva. Identificador regres� con sesi�n_id () no debe tener m�s de 32 caracteres y debe contener s�lo letras latinas y n�meros.";
$MESS["CLU_SESSION_NOTE"] = "<p>El servidor cluster Web requiere que usted configure correctamente el soporte de las sesiones.</p>
<p>Las estrategias de carga de uso m�s frecuente de distribuci�n son: </ p>
 <p>1) la asignaci�n de una sesi�n de visitante a un servidor web para el procesamiento de todas las nuevas solicitudes. </ p>
<p>2) que permite realiza diferentes de una sesi�n web para ser procesado por los servidores Web diferentes. <br>
El requisito previo obligatorio para la estrategia (2) es que el m�dulo de seguridad debe estar configurado para almacenar los datos de la sesi�n. </p>";
?>