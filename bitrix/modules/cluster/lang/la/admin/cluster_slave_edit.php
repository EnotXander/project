<?
$MESS["CLU_SLAVE_EDIT_ERROR"] = "Conexi�n espeificada es inv�lida.";
$MESS["CLU_SLAVE_EDIT_TAB"] = "Par�metros";
$MESS["CLU_SLAVE_EDIT_TAB_TITLE2"] = "Par�metros de la base de datos primaria";
$MESS["CLU_SLAVE_EDIT_SAVE_ERROR"] = "Error al guardar la conexi�n de la base de datos.";
$MESS["CLU_SLAVE_EDIT_EDIT_TITLE2"] = "Par�metros de la base de datos primaria";
$MESS["CLU_SLAVE_EDIT_ID"] = "ID";
$MESS["CLU_SLAVE_EDIT_NAME"] = "Nombre";
$MESS["CLU_SLAVE_EDIT_DB_HOST"] = "Cadena de Conexi�n de la base de datos";
$MESS["CLU_SLAVE_EDIT_DB_NAME"] = "Base de datos";
$MESS["CLU_SLAVE_EDIT_DB_LOGIN"] = "Usuario";
$MESS["CLU_SLAVE_EDIT_DESCRIPTION"] = "Descripci�n";
$MESS["CLU_SLAVE_EDIT_SELECTABLE1"] = "Para el Backup";
$MESS["CLU_SLAVE_EDIT_SELECTABLE2"] = "Carga M�nima";
$MESS["CLU_SLAVE_EDIT_TAB_TITLE1"] = "Par�metros de la Base de Datos Esclava";
$MESS["CLU_SLAVE_EDIT_EDIT_TITLE1"] = "Par�metros de la Base de Datos Esclava";
$MESS["CLU_SLAVE_EDIT_MENU_LIST"] = "Bases de Datos Esclava";
$MESS["CLU_SLAVE_EDIT_MENU_LIST_TITLE"] = "Base de Datos Esclava";
$MESS["CLU_SLAVE_EDIT_WEIGHT"] = "Carga, % (0..100)";
?>