<?
IncludeModuleLangFile(__FILE__);

class CClusterSlave
{
	function SetOnLine($node_id)
	{
		global $DB;

		$arNode = CClusterDBNode::GetByID($node_id);
		if(is_array($arNode) && $arNode["ROLE_ID"] == "SLAVE")
		{
			$rs = $DB->Query("show master status", false, '', array('fixed_connection'=>true));
			if($arMasterStatus = $rs->Fetch())
			{
				ob_start();
				$nodeDB = CDatabase::GetDBNodeConnection($arNode["ID"], true);
				$error = ob_get_contents();
				ob_end_clean();
				if(is_object($nodeDB))
				{
					$rs = $nodeDB->Query("
						CHANGE MASTER TO
							MASTER_HOST = '".$DB->ForSQL($arNode["MASTER_HOST"])."'
							,MASTER_USER = '".$DB->ForSQL($DB->DBLogin)."'
							,MASTER_PASSWORD = '".$DB->ForSQL($DB->DBPassword)."'
							,MASTER_PORT = ".$DB->ForSQL($arNode["MASTER_PORT"])."
							,MASTER_LOG_FILE = '".$arMasterStatus["File"]."'
							,MASTER_LOG_POS = ".$arMasterStatus["Position"]."
					", false, '', array('fixed_connection'=>true));
					if($rs)
					{
						$rs = $nodeDB->Query("START SLAVE");
					}
					if($rs)
					{
						CClusterDBNode::SetOnline($node_id);
						CClusterSlave::AdjustServerID($arNode, $nodeDB);
					}
				}
			}
		}
	}

	function Pause($node_id)
	{
		$arNode = CClusterDBNode::GetByID($node_id);
		if(
			is_array($arNode)
			&& $arNode["ROLE_ID"] == "SLAVE"
			&& $arNode["STATUS"] == "ONLINE"
		)
		{
			ob_start();
			$nodeDB = CDatabase::GetDBNodeConnection($arNode["ID"], true);
			$error = ob_get_contents();
			ob_end_clean();
			if(is_object($nodeDB))
			{
				$rs = $nodeDB->Query("STOP SLAVE SQL_THREAD");
				if($rs)
				{
					$ob = new CClusterDBNode;
					$ob->Update($arNode["ID"], array("STATUS"=>"PAUSED"));
				}
			}
		}
	}

	function Resume($node_id)
	{
		$arNode = CClusterDBNode::GetByID($node_id);
		if(
			is_array($arNode)
			&& $arNode["ROLE_ID"] == "SLAVE"
			&& $arNode["STATUS"] == "PAUSED"
		)
		{
			ob_start();
			$nodeDB = CDatabase::GetDBNodeConnection($arNode["ID"], true, false);
			$error = ob_get_contents();
			ob_end_clean();
			if(is_object($nodeDB))
			{
				//TODO check if started just make active
				$rs = $nodeDB->Query("START SLAVE");
				//if($rs)
				{
					$ob = new CClusterDBNode;
					$ob->Update($arNode["ID"], array("STATUS"=>"ONLINE", "ROLE_ID"=>"SLAVE"));
				}
				CClusterSlave::AdjustServerID($arNode, $nodeDB);
			}
		}
	}

	function Stop($node_id)
	{
		$arNode = CClusterDBNode::GetByID($node_id);
		if(
			is_array($arNode)
			&& $arNode["ROLE_ID"] == "SLAVE"
			&& $arNode["STATUS"] == "PAUSED"
		)
		{
			ob_start();
			$nodeDB = CDatabase::GetDBNodeConnection($arNode["ID"], true, false);
			$error = ob_get_contents();
			ob_end_clean();
			if(is_object($nodeDB))
			{
				//TODO check if started just make active
				$rs = $nodeDB->Query("STOP SLAVE");
				//if($rs)
				{
					$ob = new CClusterDBNode;
					$ob->Update($arNode["ID"], array("STATUS"=>"READY"));
				}
			}
		}
	}

	function SkipSQLError($node_id)
	{
		$arNode = CClusterDBNode::GetByID($node_id);
		if(
			is_array($arNode)
			&& $arNode["ROLE_ID"] == "SLAVE"
		)
		{
			ob_start();
			$nodeDB = CDatabase::GetDBNodeConnection($arNode["ID"], true, false);
			$error = ob_get_contents();
			ob_end_clean();
			if(is_object($nodeDB))
			{
				//TODO check if started just make active
				$rs = $nodeDB->Query("STOP SLAVE");
				if($rs)
					$rs = $nodeDB->Query("SET GLOBAL SQL_SLAVE_SKIP_COUNTER = 1");
				if($rs)
					$rs = $nodeDB->Query("START SLAVE");
			}
		}
	}

	function GetStatus($node_id, $bSlaveStatus = true, $bGlobalStatus = true, $bVariables = true)
	{
		if($node_id > 1)
		{
			$arSlaveStatus = array(
				'server_id' => null,
				'Slave_IO_State' => null,
				'Slave_IO_Running' => null,
				'Read_Master_Log_Pos' => null,
				'Slave_SQL_Running' => null,
				'Exec_Master_Log_Pos' => null,
				'Seconds_Behind_Master' => null,
				'Last_IO_Error' => null,
				'Last_SQL_Error' => null,
	//			'Replicate_Ignore_Table' => null,
				'Com_select' => null,
			);

			ob_start();
			$nodeDB = CDatabase::GetDBNodeConnection($node_id, true, false);
			$error = ob_get_contents();
			ob_end_clean();
			$sqlStatus = "SHOW SLAVE STATUS";
		}
		else
		{
			$arSlaveStatus = array(
				'server_id' => null,
				'File' => null,
				'Position' => null,
				'Com_select' => null,
			);

			$nodeDB = $GLOBALS["DB"];
			$sqlStatus = "SHOW MASTER STATUS";
		}

		if(is_object($nodeDB))
		{
			if($bSlaveStatus)
			{
				$rs = $nodeDB->Query($sqlStatus, true, "", array("fixed_connection" => true));
				if(!$rs)
					return GetMessage("CLU_NO_PRIVILEGES", array("#sql#" => "GRANT REPLICATION CLIENT on *.* to '".$nodeDB->DBLogin."'@'%';"));
				$ar = $rs->Fetch();
				if(is_array($ar))
				{
					foreach($ar as $key=>$value)
					{
						if(array_key_exists($key, $arSlaveStatus))
							$arSlaveStatus[$key] = $value;
					}
				}

//				if(strpos($arSlaveStatus['Replicate_Ignore_Table'], "b_sec_session") !== false)
//					unset($arSlaveStatus['Replicate_Ignore_Table']);
			}

			if($bGlobalStatus)
			{
				$rs = $nodeDB->Query("show global status where Variable_name in ('Com_select', 'Com_do')", true, "", array("fixed_connection" => true));
				if(is_object($rs))
				{
					while($ar = $rs->Fetch())
					{
						if($ar['Variable_name'] == 'Com_do')
							$arSlaveStatus['Com_select'] -= $ar['Value']*2;
						else
							$arSlaveStatus['Com_select'] += $ar['Value'];
					}
				}
				else
				{
					$rs = $nodeDB->Query("show status like 'Com_select'", false, "", array("fixed_connection" => true));
					$ar = $rs->Fetch();
					if($ar)
						$arSlaveStatus['Com_select'] += $ar['Value'];
					$rs = $nodeDB->Query("show status like 'Com_do'", false, "", array("fixed_connection" => true));
					$ar = $rs->Fetch();
					if($ar)
						$arSlaveStatus['Com_select'] -= $ar['Value']*2;
				}
			}

			if($bVariables)
			{
				$rs = $nodeDB->Query("show variables like 'server_id'", false, "", array("fixed_connection" => true));
				if($ar = $rs->Fetch())
					$arSlaveStatus['server_id'] = $ar["Value"];
			}

			return $arSlaveStatus;
		}
		else
		{
			return false;
		}

	}

	function GetList()
	{
		global $DB, $CACHE_MANAGER;
		static $arSlaves = false;
		if($arSlaves === false)
		{
			$cache_id = "db_slaves";
			if(
				CACHED_b_cluster_dbnode !== false
				&& $CACHE_MANAGER->Read(CACHED_b_cluster_dbnode, $cache_id, "b_cluster_dbnode")
			)
			{
				$arSlaves = $CACHE_MANAGER->Get($cache_id);
			}
			else
			{
				$arSlaves = array();

				$rs = $DB->Query("
					SELECT ID, WEIGHT
					FROM b_cluster_dbnode
					WHERE ROLE_ID in ('MAIN', 'SLAVE')
					AND STATUS = 'ONLINE'
					AND (SELECTABLE is null or SELECTABLE = 'Y')
					ORDER BY ID
				", false, '', array('fixed_connection'=>true));
				while($ar = $rs->Fetch())
					$arSlaves[intval($ar['ID'])] = $ar;

				if(CACHED_b_cluster_dbnode !== false)
					$CACHE_MANAGER->Set($cache_id, $arSlaves);
			}
		}
		return $arSlaves;
	}

	function AdjustServerID($arNode, $nodeDB)
	{
		$rs = $nodeDB->Query("show variables like 'server_id'", false, '', array("fixed_connection"=>true));
		if($ar = $rs->Fetch())
		{
			if($ar["Value"] != $arNode["SERVER_ID"])
			{
				$ob = new CClusterDBNode;
				$ob->Update($arNode["ID"], array("SERVER_ID"=>$ar["Value"]));
			}
		}
	}
}
?>