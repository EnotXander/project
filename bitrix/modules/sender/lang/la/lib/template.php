<?
$MESS["SENDER_TEMPLATE_EDITOR_MAILBLOCK"] = "Blocks de correo";
$MESS["SENDER_TEMPLATE_EDITOR_MAILBLOCK_SEARCH"] = "B�squeda de blocks de correo...";
$MESS["SENDER_TEMPLATE_EDITOR_SAVE"] = "Guardar en Mis plantillas";
$MESS["SENDER_TEMPLATE_EDITOR_SAVE_NAME"] = "utilizando el nombre";
$MESS["SENDER_ENTITY_TEMPLATE_FIELD_TITLE_NAME"] = "Nombre";
$MESS["SENDER_ENTITY_TEMPLATE_FIELD_TITLE_CONTENT"] = "Texto de la plantilla";
$MESS["SENDER_ENTITY_TEMPLATE_NOTE_OLD_EDITOR"] = "Para utilizar el editor visual,%LINK_START% habilitar la opci�n \"Usar nuevo editor visual\" %LINK_END%.
<br> El cuerpo del mensaje puede incluir macros de personalizaci�n; usted los encontrar� junto al campo \"Asunto\".";
?>