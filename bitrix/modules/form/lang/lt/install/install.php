<?
$MESS["FORM_MODULE_NAME"] = "Web formos";
$MESS["FORM_MODULE_DESCRIPTION"] = "�is modulis valdo �vairias web formas, leid�ia saugoti ir filtruoti duomenis u�pildytose formose.";
$MESS["FORM_INSTALL_TITLE"] = "Web form� modulio diegimas";
$MESS["FORM_UNINSTALL_TITLE"] = "Web form� modulio �alinimas";
$MESS["FORM_SELECT_INITITAL"] = "Pra�ome pasirinkti aplank� demo failams:";
$MESS["FORM_NO"] = "ne";
$MESS["FORM_ATTENTION"] = "D�mesio! Modulis bus pa�alintas.";
$MESS["FORM_YOU_CAN_SAVE_TABLES"] = "Nor�dami i�saugoti duomenis, saugomus duomen� baz�s lentel�se, pa�ym�kyte &quot;Save Tables&quot; checkbox";
$MESS["FORM_SAVE_TABLES"] = "I�saugoti lenteles";
$MESS["FORM_DELETE"] = "Pa�alinti";
$MESS["FORM_DELETE_COMLETE"] = "�alinimas atliktas.";
$MESS["FORM_BACK"] = "Gr��ti";
$MESS["FORM_ERRORS"] = "Klaidos:";
$MESS["FORM_INSTALL"] = "�diegti";
$MESS["FORM_COMPLETE"] = "Diegimas atliktas. ";
$MESS["FORM_WRONG_MAIN_VERSION"] = "Nor�dami �diegti �� modul�, j�s turite atnaujinti sistemos branduol� iki versijos #VER#";
$MESS["FORM_DENIED"] = "U�drausti prieig�";
$MESS["FORM_OPENED"] = "prieiga suteikiama";
$MESS["FORM_FULL"] = "pilna prieiga ";
$MESS["FORM_URL_PUBLIC"] = "Aplankas demo failams:";
$MESS["FORM_DEMO_DIR"] = "Sekite �i� nuorod�, nor�dami per�i�r�ti demo fail� veikym�:";
$MESS["FORM_RESET"] = "Atstatyti";
$MESS["FORM_SITE"] = "Svetain�";
$MESS["FORM_LINK"] = "Nuoroda";
$MESS["COPY_PUBLIC_FILES"] = "Kopijuoti vie�uosius failus";
$MESS["COPY_FOLDER"] = "Aplankas, kur failai bus kopijuojami (susij�s su svetain�s �akniniu aplanku):";
$MESS["INSTALL_PUBLIC_REW"] = "Perra�yti esamus failus";
?>