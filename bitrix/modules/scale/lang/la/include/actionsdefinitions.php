<?
$MESS["SCALE_ADEF_CHPASS_NAME"] = "Cambiar la contrase�a del usuario root";
$MESS["SCALE_ADEF_CHPASS_UP_IP"] = "Direcci�n IP del servidor";
$MESS["SCALE_ADEF_CHPASS_UP_OLD_PASS"] = "contrase�a actual";
$MESS["SCALE_ADEF_CHPASS_UP_NEW_PASS"] = "Nueva contrase�a";
$MESS["SCALE_ADEF_NEW_SERVER_CHAIN"] = "Agregar nuevo servidor ";
$MESS["SCALE_ADEF_DEL_SERVER"] = "Eliminar el servidor desde el pool";
$MESS["SCALE_ADEF_REBOOT"] = "Reanudar";
$MESS["SCALE_ADEF_ROOT_PASS"] = "Contrase�a del administrador del sistema";
$MESS["SCALE_ADEF_CREATE_PULL"] = "Crear pool";
$MESS["SCALE_ADEF_GET_CURRENT_KEY"] = "Obtener clave";
$MESS["SCALE_ADEF_COPY_KEY_TO_SERVER"] = "Copiar clave al servidor";
$MESS["SCALE_ADEF_ADD_SERVER"] = "Agregar servidor pool";
$MESS["SCALE_ADEF_MONITORING_ENABLE"] = "Permitir la supervisi�n";
$MESS["SCALE_ADEF_MONITORING_DISABLE"] = "Deshabilitar la supervisi�n";
$MESS["SCALE_ADEF_MONITORING_UPDATE"] = "Par�metros de actualizaci�n de monitoreo";
$MESS["SCALE_ADEF_MYSQL_ADD_SLAVE"] = "Agregar escala";
$MESS["SCALE_ADEF_MYSQL_CHANGE_MASTER"] = "Marca principal";
$MESS["SCALE_ADEF_MYSQL_DEL_SLAVE"] = "Eliminar secundario";
$MESS["SCALE_ADEF_MYSQL_PASS"] = "MySQL contrase�a de root";
$MESS["SCALE_ADEF_MEMCACHED_ADD_ROLE"] = "Agregar funci�n en memoria en cach� ";
$MESS["SCALE_ADEF_MEMCACHED_DEL_ROLE"] = "Eliminar funci�n en memoria en cach� ";
$MESS["SCALE_ADEF_SET_EMAIL"] = "Configurar e-mail";
$MESS["SCALE_ADEF_SET_EMAIL_SMTP_HOST"] = "Servidor SMTP";
$MESS["SCALE_ADEF_SET_EMAIL_SMTP_PORT"] = "Puerto SMTP";
$MESS["SCALE_ADEF_SET_EMAIL_SMTP_USER"] = "Login de usuario SMTP";
$MESS["SCALE_ADEF_SET_EMAIL_USER_PASSWORD"] = "Contrase�a de usuario";
$MESS["SCALE_ADEF_SET_EMAIL_EMAIL"] = "Direcci�n de e-mail";
$MESS["SCALE_ADEF_SET_EMAIL_SMTPTLS"] = "Utilice TLS";
$MESS["SCALE_ADEF_SET_EMAIL_SITE"] = "Sitio";
$MESS["SCALE_ADEF_CRON_SET"] = "Acoplar en cron";
$MESS["SCALE_ADEF_CRON_UNSET"] = "Desacoplar cron";
$MESS["SCALE_ADEF_HTTP_OFF"] = "inhabilitar HTTP";
$MESS["SCALE_ADEF_HTTP_ON"] = "Habilitar HTTP";
$MESS["SCALE_ADEF_BVM_UPDATE"] = "Actualizar Bitrix Environment";
$MESS["SCALE_ADEF_CHANGE_PASSWD_BITRIX"] = "Cambiar contrase�a de usuario bitrix";
$MESS["SCALE_ADEF_SITE_ADD"] = "Agregar sitio";
$MESS["SCALE_ADEF_SITE_ADD_NAME"] = "Nombre del sitio";
$MESS["SCALE_ADEF_SITE_ADD_DB_NAME"] = "Nombre de la base de datos";
$MESS["SCALE_ADEF_SITE_ADD_DB_USERNAME"] = "Nombre de usuario de la base de datos";
$MESS["SCALE_ADEF_SITE_ADD_DB_USERPASS"] = "Contrase�a de la base de datos";
$MESS["SCALE_ADEF_SITE_ADD_SITE_PATH"] = "Ruta del sitio";
$MESS["SCALE_ADEF_SITE_DEL"] = "Eliminar sitio";
$MESS["SCALE_ADEF_APACHE_ADD_ROLE"] = "Agregar funci�n Apache";
$MESS["SCALE_ADEF_APACHE_DEL_ROLE"] = "Eliminar funci�n Apache";
$MESS["SCALE_ADEF_SPHINX_ADD_ROLE"] = "Agregar funci�n Sphinx";
$MESS["SCALE_ADEF_SPHINX_DEL_ROLE"] = "Eliminar funci�n Sphinx";
$MESS["SCALE_ADEF_CHPASS_UP_NET_ADDRESS"] = "Direcci�n de red de trabajo";
$MESS["SCALE_ADEF_CHPASS_UP_HOSTNAME"] = "Nombre de host";
$MESS["SCALE_ADEF_SPHINX_ADD_ROLE_INDEX"] = "Volver a indexar el sitio web despu�s de agregar la nueva funci�n";
?>