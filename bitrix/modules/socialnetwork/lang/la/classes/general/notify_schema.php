<?
$MESS["SONET_NS_INVITE_USER"] = "Notificación de la amistad";
$MESS["SONET_NS_INVITE_GROUP"] = "Notificación de invitación al grupo";
$MESS["SONET_NS_INOUT_GROUP"] = "Cambie la pertenencia al grupo";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Asignado o no asignado como moderador del grupo de trabajo";
$MESS["SONET_NS_OWNER_GROUP"] = "Cambio de propietario de grupo de trabajo";
$MESS["SONET_NS_FRIEND"] = "Que esten o no esten en la lista de amigo";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Actualizar grupo de trabajo";
?>