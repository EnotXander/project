<?
$MESS["SONET_WRONG_PARAMETER_ID"] = "شناسه دریافت شده نادرست است.";
$MESS["SONET_UR_ERROR_CREATE_U_GROUP"] = "خطا در افزودن کاربر به گروه.";
$MESS["SONET_UR_ERROR_CREATE_GROUP"] = "خطا در ایجاد گروه.";
$MESS["SONET_UR_EMPTY_FIELDS"] = "پارامترهای گروه تکمیل نشده اند.";
$MESS["SONET_GB_EMPTY_OWNER_ID"] = "مدیر مشخص نشده است.";
$MESS["SONET_UG_ERROR_NO_SPAM_PERMS"] = "مجوز برای ارسال پیغام به گروه نادرست است.";
$MESS["SONET_UG_EMPTY_SPAM_PERMS"] = "مجوز برای ارسال پیغام به گروه تعیین نشده است.";
$MESS["SONET_NO_GROUP"] = "داده پیدا نشد.";
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "تاریخ ایجاد نادرست است.";
$MESS["SONET_GB_EMPTY_DATE_UPDATE"] = "تاریخ بروز رسانی نادرست است.";
$MESS["SONET_UR_EMPTY_OWNERID"] = "شناسه مدیر گروه مشخص نشده است.";
$MESS["SONET_GP_ERROR_IMAGE_ID"] = "تصویر نادرست است.";
$MESS["SONET_UG_ERROR_NO_INITIATE_PERMS"] = "مجوز دعوتنامه نادرست است.";
$MESS["SONET_UG_EMPTY_INITIATE_PERMS"] = "مجوز دعوتنامه تعیین نشده است.";
$MESS["SONET_GB_EMPTY_DATE_ACTIVITY"] = "تاریخ آخرین بازدید نادرست است.";
$MESS["SONET_GB_ERROR_NO_OWNER_ID"] = "شناسه مدیر نادرست است.";
$MESS["SONET_GG_ERROR_NO_SITE"] = "سایت نادرست است.";
$MESS["SONET_GG_EMPTY_SITE_ID"] = "سایت مشخص نشده است.";
$MESS["SONET_GB_EMPTY_NAME"] = "نام مشخص نشده است.";
$MESS["SONET_GB_ERROR_NO_SUBJECT_ID"] = "موضوع نادرست است.";
$MESS["SONET_GB_EMPTY_SUBJECT_ID"] = "موضوع مشخص نشده است.";
$MESS["SONET_GG_ERROR_CANNOT_DELETE_USER_1"] = "کاربر مدیر کلوبهای زیر است:<br>";
$MESS["SONET_GG_ERROR_CANNOT_DELETE_USER_2"] = "لطفا به خدمات > کلوب > گروه ها بروید و مدیر گروه را تغییر دهید یا گروه را حذف کنید.";
?>