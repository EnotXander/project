<?
$MESS["SMILE_NAV"] = "خندانکها";
$MESS["SMILE_ID"] = "شناسه";
$MESS["SMILE_SORT"] = "ترتیب";
$MESS["SMILE_TYPE"] = "نوع";
$MESS["SONET_TYPING"] = "نوشتن";
$MESS["SONET_SMILE_ICON"] = "تصویر";
$MESS["SMILE_TYPE_ICON"] = "آیکون";
$MESS["SMILE_TYPE_SMILE"] = "خندانک";
$MESS["SMILE_UPD"] = "ذخیره";
$MESS["SMILE_RESET"] = "لغو";
$MESS["SMILE_F_DESCR"] = "توضیحات:";
$MESS["SMILE_F_NAME"] = "عنوان:";
$MESS["SMILE_DEL"] = "حذف";
$MESS["SONET_NAME"] = "عنوان";
$MESS["SONET_ACTIONS"] = "اقدامات";
$MESS["SONET_EDIT"] = "ویرایش";
$MESS["SONET_DELETE_DESCR"] = "حذف خندانک";
$MESS["ERROR_EMPTY_NAME2"] = "برای: ";
$MESS["FSAN_ADD_NEW"] = "خندانک جدید";
$MESS["PAGES"] = "خندانکها";
?>