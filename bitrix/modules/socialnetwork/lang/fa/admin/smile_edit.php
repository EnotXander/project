<?
$MESS["SONET_NEW_RECORD"] = "خندانک جدید";
$MESS["SONET_RECORDS_LIST"] = "خندانکها";
$MESS["SONET_SAVE"] = "ذخیره";
$MESS["SONET_ADD"] = "افزودن";
$MESS["SONET_APPLY"] = "اعمال";
$MESS["SONET_RESET"] = "لغو";
$MESS["SONET_SORT"] = "ترتیب";
$MESS["SONET_CODE"] = "کد";
$MESS["SONET_NAME"] = "عنوان";
$MESS["SONET_DESCR"] = "توضیحات";
$MESS["SONET_PT_PROPS"] = "پارامترها";
$MESS["SONET_TYPE"] = "نوع خندانک";
$MESS["SONET_TYPING"] = "نوشتن خندانک";
$MESS["SONET_IMAGE"] = "تصویر خندانک";
$MESS["FSE_SMILE"] = "خندانک";
$MESS["FSE_ICON"] = "آیکون";
$MESS["SONET_CODE_LEN"] = "حرف";
$MESS["FSN_2FLIST"] = "خندانکها";
$MESS["FSN_NEW_SMILE"] = "خندانک جدید";
$MESS["FSN_DELETE_SMILE"] = "حذف خندانک";
?>