<?
$MESS["SONET_DELETE_ALT"] = "حذف گروه";
$MESS["SONET_DELETE_CONF"] = "آیا گروه را حذف می کنید؟";
$MESS["SONET_GROUP_NAV"] = "گروه ها";
$MESS["SONET_GROUP_SUBJECT_ID"] = "موضوع";
$MESS["SONET_GROUP_OWNER_ID"] = "مالک";
$MESS["SONET_SUBJECT_SORT"] = "ترتیب";
$MESS["SONET_TITLE"] = "گروه ها";
$MESS["SONET_GROUP_NAME"] = "نام";
$MESS["SONET_FILTER_SITE_ID"] = "سایت";
$MESS["SONET_FILTER_SUBJECT_ID"] = "موضوع";
$MESS["SONET_SPT_ALL"] = "[همه]";
$MESS["SONET_ADMIN_LIST_CHANGE_OWNER"] = "تغییر مالک";
$MESS["SONET_OWNER_USER"] = "مالک";
$MESS["SONET_OWNER_ID"] = "شناسه مالک";
?>