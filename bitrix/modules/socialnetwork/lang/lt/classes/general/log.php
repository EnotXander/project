<?
$MESS["SONET_GL_TITLE_JOIN1"] = "Naujas narys #TITLE# prisijung� prie grup�s";
$MESS["SONET_GL_TITLE_JOIN2"] = "Nauji nariai #TITLE# prisijung� prie grup�s";
$MESS["SONET_GL_TITLE_UNJOIN1"] = "Narys #TITLE# i��jo i� grup�s";
$MESS["SONET_GL_TITLE_UNJOIN2"] = "Nariai #TITLE# i��jo i� grup�s";
$MESS["SONET_GL_TITLE_MODERATE1"] = "Grupei priskirtas moderatorius #TITLE#.";
$MESS["SONET_GL_TITLE_MODERATE2"] = "Grupei priskirti moderatoriai #TITLE#.";
$MESS["SONET_GL_TITLE_UNMODERATE1"] = "#TITLE# pa�alintas i� grup�s moderatori�.";
$MESS["SONET_GL_TITLE_UNMODERATE2"] = "#TITLE# pa�alinti i� grup�s moderatori�.";
$MESS["SONET_GL_TITLE_GROUP1"] = "Naudotojas tapo grup�s #TITLE# nariu.";
$MESS["SONET_GL_TITLE_UNGROUP1"] = "Naudotojas i��jo i� grup�s #TITLE#.";
?>