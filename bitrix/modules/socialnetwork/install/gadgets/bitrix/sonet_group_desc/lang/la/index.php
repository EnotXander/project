<?
$MESS["GD_SONET_GROUP_DESC_NAME"] = "Grupo";
$MESS["GD_SONET_GROUP_DESC_ARCHIVE"] = "Grupo archivado";
$MESS["GD_SONET_GROUP_DESC_SUBJECT_NAME"] = "Tema";
$MESS["GD_SONET_GROUP_DESC_DESCRIPTION"] = "Descripci�n";
$MESS["GD_SONET_GROUP_DESC_CREATED"] = "Creado";
$MESS["GD_SONET_GROUP_DESC_NMEM"] = "Miembros";
$MESS["GD_SONET_GROUP_DESC_TYPE"] = "Tipo de grupo";
$MESS["GD_SONET_GROUP_DESC_TYPE_O1"] = "Este es un grupo p�blico. Cualquiera puede unirse.";
$MESS["GD_SONET_GROUP_DESC_TYPE_O2"] = "Este es un grupo privado. La pertenencia del usuario est� sujeta a la aprobaci�n del administrador.";
$MESS["GD_SONET_GROUP_DESC_TYPE_V1"] = "Este grupo es visible para cualquiera.";
$MESS["GD_SONET_GROUP_DESC_TYPE_V2"] = "Este grupo es invisible. S�lo los miembros del grupo pueden verlo.";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE"] = "Usted ya ha enviado una solicitud para unirse a este grupo de trabajo.";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE_BY_GROUP"] = "La invitaci�n a formar parte de este grupo de trabajo ya le ha sido enviad. Puede confirmar su membres�a en <a href='#LINK#'>la p�gina de invitaci�n</a>.";
?>