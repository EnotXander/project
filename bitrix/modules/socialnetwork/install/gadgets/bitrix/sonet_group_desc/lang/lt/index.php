<?
$MESS["GD_SONET_GROUP_DESC_NAME"] = "Grup�";
$MESS["GD_SONET_GROUP_DESC_ARCHIVE"] = "Archyvin� grup�";
$MESS["GD_SONET_GROUP_DESC_SUBJECT_NAME"] = "Tema";
$MESS["GD_SONET_GROUP_DESC_DESCRIPTION"] = "Apra�ymas";
$MESS["GD_SONET_GROUP_DESC_CREATED"] = "Sukurta";
$MESS["GD_SONET_GROUP_DESC_NMEM"] = "Nariai";
$MESS["GD_SONET_GROUP_DESC_TYPE"] = "Grup�s tipas";
$MESS["GD_SONET_GROUP_DESC_TYPE_O1"] = "Tai - vie�a grup�. Prisijungti gali visi.";
$MESS["GD_SONET_GROUP_DESC_TYPE_O2"] = "Tai - privati grup�. Vartotojo naryst� turi patvirtinti administratorius.";
$MESS["GD_SONET_GROUP_DESC_TYPE_V1"] = "�i grup� yra matoma visiems.";
$MESS["GD_SONET_GROUP_DESC_TYPE_V2"] = "�i grup� yra nematoma. J� gali matyti tik grup�s nariai.";
?>