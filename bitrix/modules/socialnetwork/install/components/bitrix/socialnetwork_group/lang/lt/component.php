<?
$MESS["FL_FORUM_CHAIN"] = "Forumas";
$MESS["SONET_LOG_TEMPLATE_AUTHOR_MAIL"] = "
Autorius: #AUTHOR_NAME# (#AUTHOR_URL#)";
$MESS["SONET_LOG_TEMPLATE_AUTHOR"] = "
Autorius: <a href=\"#AUTHOR_URL#\">#AUTHOR_NAME#</a>.";
$MESS["SONET_LOG_TEMPLATE_GUEST"] = "Autorius: Sve�ias.";
$MESS["SONET_ACCESS_DENIED"] = "Prieiga u�drausta";
$MESS["SONET_FILES"] = "Failai";
$MESS["SONET_GROUP"] = "Grup�s";
$MESS["SONET_GROUP_PREFIX"] = "Grup�: ";
$MESS["SONET_PHOTO"] = "Foto";
$MESS["SONET_FILES_LOG"] = "#AUTHOR_NAME# prid�jo fail� #TITLE#.";
$MESS["SONET_FILES_LOG_TEXT"] = "Naujas failas #TITLE# adresu #URL#.";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# prid�jo fail� #TITLE#";
$MESS["SONET_PHOTO_LOG_2"] = "Nuotraukos (#COUNT#)";
?>