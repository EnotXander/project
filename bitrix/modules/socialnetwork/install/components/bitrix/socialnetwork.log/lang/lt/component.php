<?
$MESS["SONET_MODULE_NOT_INSTALL"] = "Ne�diegtas socialinio tinklo modulis.";
$MESS["SONET_P_USER_NO_USER"] = "Vartotojas nerastas.";
$MESS["SONET_C73_PAGE_TITLE"] = "Naujinti �urnal�";
$MESS["SONET_C73_TITLE_JOIN1"] = "Naujas narys #TITLE# prisijung� prie grup�s.";
$MESS["SONET_C73_TITLE_JOIN2"] = "Nauji nariai #TITLE# prisijung� prie grup�s.";
$MESS["SONET_C73_TITLE_UNJOIN1"] = "Narys #TITLE# paliko grup�.";
$MESS["SONET_C73_TITLE_UNJOIN2"] = "Nariai #TITLE# paliko grup�.";
$MESS["SONET_C73_TITLE_MODERATE1"] = "Moderatorius #TITLE# paskirtas grupei.";
$MESS["SONET_C73_TITLE_MODERATE2"] = "Moderatoriai #TITLE# paskirti grup�je.";
$MESS["SONET_C73_TITLE_UNMODERATE1"] = "#TITLE# neb�ra �ios grup�s moderatorius.";
$MESS["SONET_C73_TITLE_UNMODERATE2"] = "#TITLE# neb�ra �ios grup�s moderatoriai.";
$MESS["SONET_C73_TITLE_FRIEND1"] = "#TITLE# prid�tas prie draug� s�ra�o.";
$MESS["SONET_C73_TITLE_UNFRIEND1"] = "#TITLE# pa�alintas i� draug� s�ra�o.";
$MESS["SONET_C73_TITLE_GROUP1"] = "Vartotojas prisijung� prie grup�s #TITLE#.";
$MESS["SONET_C73_TITLE_UNGROUP1"] = "Vartotojas paliko grup� #TITLE#.";
$MESS["SONET_C73_TITLE_OWNER1"] = "Grupei #TITLE# priskirtas naujas savininkas";
?>