<?
$MESS["SONET_MODULE_NOT_INSTALL"] = "Socialinio tinklo modulis ne�diegtas.";
$MESS["SONET_P_USER_TITLE_VIEW"] = "Vartotojo profilis";
$MESS["SONET_P_USER_NO_USER"] = "Vartotojas nerastas.";
$MESS["SONET_P_USER_NO_GROUP"] = "Grup� nerasta.";
$MESS["SONET_C39_PAGE_TITLE"] = "Grup�s naryst�s u�klausa";
$MESS["SONET_C39_CANT_VIEW"] = "Prie �ios grup�s prisijungti negalite.";
$MESS["SONET_C39_ALREADY_MEMBER"] = "Jau esate �ios grup�s narys.";
$MESS["SONET_C39_ALREADY_JOINED"] = "Jau prisijung�te prie �ios grup�s.";
$MESS["SONET_C39_NO_TEXT"] = "Prane�imo tekstas tu��ias.";
?>