<?
$MESS["PATH_TO_USER_TIP"] = "La ruta hacia una p�gina de perfil de usuario. Ejemplo: sonet_user.php?page=user&user_id=#user_id#.";
$MESS["PATH_TO_USER_EDIT_TIP"] = "La ruta hacia la p�gina del perfil de usuario editor. Ejemplo: sonet_user_edit.php?page=user&user_id=#user_id#&mode=edit.";
$MESS["PAGE_VAR_TIP"] = "Especifique aqu� el nombre de una variable a la p�gina de usuario de Social Network.";
$MESS["USER_VAR_TIP"] = "Especifique aqu� el nombre de una variable a la que la el ID de la red social del usuario se pasa.";
$MESS["ID_TIP"] = "Especifica el c�digo que eval�a el ID del usuario.";
$MESS["SET_TITLE_TIP"] = "Al seleccionar esta opci�n aparecer� el t�tulo de la p�gina a <i>\"user name\"</i> <b>User Profile</b>.";
$MESS["USER_PROPERTY_TIP"] = "Seleccione aqu� propiedades adicionales que se mostrar�n en el perfil de usuario.";
?>