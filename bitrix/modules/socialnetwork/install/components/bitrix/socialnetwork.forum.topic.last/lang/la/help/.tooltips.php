<?
$MESS["FID_TIP"] = "ID del foro";
$MESS["SORT_BY_TIP"] = "Campo de clasificaci�n";
$MESS["SORT_ORDER_TIP"] = "Orden de clasificaci�n";
$MESS["URL_TEMPLATES_INDEX_TIP"] = "P�gina de los foros";
$MESS["URL_TEMPLATES_LIST_TIP"] = "P�ginas de temas";
$MESS["URL_TEMPLATES_READ_TIP"] = "P�gina de temas vistos";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "P�gina de perfil del usuario";
$MESS["TOPICS_PER_PAGE_TIP"] = "N�mero de temas por p�gina";
$MESS["DATE_TIME_FORMAT_TIP"] = "Formato de Fecha y hora";
$MESS["SHOW_FORUM_ANOTHER_SITE_TIP"] = "Mostrar foros de otros sitios";
$MESS["SET_NAVIGATION_TIP"] = "Mostrar controles de navegaci�n";
$MESS["DISPLAY_PANEL_TIP"] = "Muestra los botones del panel de control para este componente";
$MESS["CACHE_TYPE_TIP"] = "Tipo del cach�";
$MESS["CACHE_TIME_TIP"] = "Tiempo de duraci�n del cach� (seg.)";
$MESS["SET_TITLE_TIP"] = "Fijar t�tulo de la p�gina";
$MESS["PAGER_DESC_NUMBERING_TIP"] = "Usar Breadcrumbs inversos";
$MESS["PAGER_SHOW_ALWAYS_TIP"] = "Mostrar siempre";
$MESS["PAGER_TITLE_TIP"] = "Nombres de las categor�as";
$MESS["PAGER_TEMPLATE_TIP"] = "T�tulo de la plantilla";
?>