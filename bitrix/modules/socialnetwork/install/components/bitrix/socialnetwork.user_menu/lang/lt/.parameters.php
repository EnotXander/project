<?
$MESS["SONET_PATH_TO_USER"] = "Vartotojo profilio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_EDIT"] = "Vartotojo profilio puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_FRIENDS"] = "Vartotojo draug� puslapio mar�ruto �ablonas";
$MESS["SONET_PAGE_VAR"] = "Puslapio kintamasis";
$MESS["SONET_USER_VAR"] = "Vartotojo kintamasis";
$MESS["SONET_ID"] = "Vartotojo ID";
$MESS["SONET_PATH_TO_USER_FRIENDS_ADD"] = "Prid�jimo prie draug� puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_GROUPS_ADD"] = "Vartotojo grupi� tvarkymo puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_GROUPS"] = "Vartotojo grupi� puslapio mar�ruto �ablonas";
$MESS["SONET_USER_PROPERTY_MAIN"] = "Vartotojo pagrindin�s papildomos ypatyb�s";
$MESS["SONET_USER_PROPERTY_CONTACT"] = "Vartotojo papildomos kontakt� ypatyb�s";
$MESS["SONET_USER_PROPERTY_PERSONAL"] = "Vartotojo pasirinktin�s ypatyb�s";
$MESS["SONET_PATH_TO_USER_FRIENDS_DELETE"] = "�alinimo i� draug� puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_SEARCH"] = "Vartotojo paie�kos puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_MESSAGE_FORM"] = "Prane�imo skelbimas i� mar�ruto �ablono";
$MESS["SONET_PATH_TO_MESSAGES_INPUT"] = "Gaunam� prane�im� puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_BLOG"] = "Vartotojo internetinio dienora��io puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_PHOTO"] = "Vartotojo nuotrauk� puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_FORUM"] = "Vartotojo forumo puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_CALENDAR"] = "Vartotojo kalendoriaus puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_FILES"] = "Vartotojo fail� puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_TASKS"] = "Vartotojo u�duo�i� puslapio URL �ablonas";
?>