<?
$MESS["F_TOPIC_LIST"] = "Lista de temas";
$MESS["F_ERR_SESS_FINISH"] = "Su sesi�n ha expirado. Por favor repita la operaci�n";
$MESS["F_NO_MODULE"] = "El m�dulo Forum no est� instalado";
$MESS["FORUM_SONET_MODULE_NOT_AVAIBLE"] = "El foro no est� disponible para este usuario.";
$MESS["SONET_MODULE_NOT_INSTALL"] = "El m�dulo Social Network no est� instalada.";
$MESS["FORUM_SONET_NO_ACCESS"] = "Usted no cuenta con permiso para ver el foro.";
$MESS["FL_FORUM_CHAIN"] = "Foro";
$MESS["F_ERR_EMPTY_ACTION"] = "No ha seleccionado una acci�n.";
$MESS["F_ERR_EMPTY_TOPICS"] = "Ning�n tema fue seleccionado.";
$MESS["F_ERR_TOPICS_NOT_MODERATION"] = "Los siguientes temas no pueden ser moderados:#TOPICS#.";
$MESS["F_FID_IS_EMPTY"] = "El foro de Social Network no se ha especificado.";
$MESS["F_FID_IS_LOST"] = "El foro de Social Network no se ha encontrado.";
$MESS["SOCNET_FORUM_TL_EMAIL_RULE"] = "Agregar mensajes para los foros de Social Network";
?>