<?
$MESS["SONET_SET_NAVCHAIN"] = "Nar�ymo kelio nustatymas";
$MESS["SONET_PATH_TO_USER"] = "Vartotojo profilio mar�ruto �ablonas";
$MESS["SONET_PAGE_VAR"] = "Puslapio kintamasis";
$MESS["SONET_USER_VAR"] = "Vartotojo kintamasis";
$MESS["SONET_GROUP_VAR"] = "Grup�s kintamasis";
$MESS["SONET_GROUP_ID"] = "Grup�s ID";
$MESS["SONET_VARIABLE_ALIASES"] = "Kintam�j� pseudonimai";
$MESS["SONET_DATE_TIME_FORMAT"] = "Datos ir laiko formatas";
$MESS["SONET_SHORT_FORM"] = "Trumpas formatas";
$MESS["SONET_PATH_TO_GROUP"] = "Grup�s puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_SEARCH"] = "Vartotojo paie�kos puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_GROUP_EDIT"] = "Grup�s parametr� puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_GROUP_CREATE"] = "Grup�s k�rimo puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_GROUP_REQUEST_SEARCH"] = "Vartotojo kvietimo puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_REQUEST_GROUP"] = "Mar�ruto �ablonas grup�s naryst�s pritaikymo puslapiui";
$MESS["SONET_PATH_TO_GROUP_REQUESTS"] = "Mar�ruto �ablonas grup�s naryst�s u�klausos puslapiui";
$MESS["SONET_PATH_TO_GROUP_MODS"] = "Grup�s moderatori� puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_GROUP_USERS"] = "Grup�s nari� puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_USER_LEAVE_GROUP"] = "Grup�s i��jimo puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_GROUP_DELETE"] = "Grup�s trynimo puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_GROUP_FEATURES"] = "Konfig�racijos puslapio mar�ruto �ablonas";
$MESS["SONET_PATH_TO_GROUP_BAN"] = "Grup�s draudimo puslapio mar�ruto �ablonas";
$MESS["SONET_ITEMS_COUNT"] = "Elementai s�ra�uose";
$MESS["SONET_PATH_TO_GROUP_SUBSCRIBE"] = "Prenumeratos tvarkymo puslapio mar�ruto �ablonas";
?>