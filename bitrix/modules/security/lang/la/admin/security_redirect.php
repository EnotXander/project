<?
$MESS["SEC_REDIRECT_TITLE"] = "Redirige la protecci�n contra ataques de phishing";
$MESS["SEC_REDIRECT_MAIN_TAB"] = "Redirige protecci�n";
$MESS["SEC_REDIRECT_MAIN_TAB_TITLE"] = "Que permita reorientar la protecci�n contra ataques de phishing.";
$MESS["SEC_REDIRECT_ON"] = "Redirige la protecci�n contra ataques de phishing est� habilitado";
$MESS["SEC_REDIRECT_OFF"] = "Redirige la protecci�n contra ataques de phishing est� desactivado";
$MESS["SEC_REDIRECT_BUTTON_OFF"] = "Reorientar desactivaci�n la protecci�n contra ataques de phishing";
$MESS["SEC_REDIRECT_BUTTON_ON"] = "Permitir reorientar la protecci�n contra ataques de phishing";
$MESS["SEC_REDIRECT_PARAMETERS_TAB"] = "Par�metros";
$MESS["SEC_REDIRECT_PARAMETERS_TAB_TITLE"] = "Protecci�n de redirecci�n contra ataques de phishing est� habilitado";
$MESS["SEC_REDIRECT_METHODS_HEADER"] = "M�todos";
$MESS["SEC_REDIRECT_METHODS"] = "M�todos de protecci�n de \"phishing\":";
$MESS["SEC_REDIRECT_REFERER_CHECK"] = "Comprobaci�n de la existencia de la cabecera HTTP que describe la p�gina referida.";
$MESS["SEC_REDIRECT_HREF_SIGN"] = "A�adir la URL a la firma digital a continuaci�n:";
$MESS["SEC_REDIRECT_URLS"] = "Firmado URL";
$MESS["SEC_REDIRECT_SYSTEM"] = "Sistema";
$MESS["SEC_REDIRECT_USER"] = "Usuarios";
$MESS["SEC_REDIRECT_URL"] = "URL:";
$MESS["SEC_REDIRECT_PARAMETER_NAME"] = "Nombre del par�metro:";
$MESS["SEC_REDIRECT_ADD"] = "Agregar";
$MESS["SEC_REDIRECT_ACTIONS_HEADER"] = "Acciones";
$MESS["SEC_REDIRECT_ACTIONS"] = "Acciones de protecci�n de \"phishing\":";
$MESS["SEC_REDIRECT_ACTION_SHOW_MESSAGE"] = "Mostrar mensaje de notificaci�n y redirigir a otro sitio despu�s de demora.";
$MESS["SEC_REDIRECT_MESSAGE"] = "Mensaje:";
$MESS["SEC_REDIRECT_TIMEOUT"] = "Plazo:";
$MESS["SEC_REDIRECT_TIMEOUT_SEC"] = "seg.";
$MESS["SEC_REDIRECT_LOG"] = "A�adir intento de phishing al <a href=\"#HREF#\">log</a>";
$MESS["SEC_REDIRECT_ACTION_REDIRECT"] = "Redirigir a la URL especificada.";
$MESS["SEC_REDIRECT_ACTION_REDIRECT_URL"] = "URL:";
$MESS["SEC_REDIRECT_NOTE"] = "<p><a href=\"http://es.wikipedia.org/wiki/Phishing\" target=\"_blank\">Phishing</a> - es el proceso criminal y fraudulento, en el cual, mediante el mediante el uso de medios electr&oacute;nicos, el atacante intenta conseguir datos sencibles como nombres de usuarios, contrase&ntilde;as, n&uacute;mero de tarjetas de cr&eacute;dito, etc., al hacerse pasar por una identidad de confianza. </p>

<p>Existen dos m�todos para prevenir directamente ataques de este tipo:</p>
<ul style=\"font-size:100%\">
<li>Detectar redirecciones maliciosas por la falta de referencias de la p�gina en la cabecera HTTP.</li>
<li>Marcando links con una firma digital, y verificando intenciones de redirecci�n.</li>
</ul>
<p>Los siguiente puede ser usado como protecci&oacute;n :
<ul style=\"font-size:100%\">
<li>Mostrar avisos de redirecci�n al visitante .</li>
<li>Incondicionalmente redireccionar a un visitante a un sitio seguro.</li>
</ul>
<p><i>Recomendado para un nivel de seguridad alto. </i></p>";
$MESS["SEC_REDIRECT_REFERER_SITE_CHECK"] = "La \"referencia\" encabezado HTTP debe contener el nombre del dominio del sitio actual.";
?>