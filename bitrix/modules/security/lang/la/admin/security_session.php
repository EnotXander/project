<?
$MESS["SEC_SESSION_ADMIN_SAVEDB_TAB"] = "Sesiones en la base de datos";
$MESS["SEC_SESSION_ADMIN_TITLE"] = "Sesi�n de protecci�n";
$MESS["SEC_SESSION_ADMIN_DB_ON"] = "La base de datos de la sesi�n est� almacenada en el m�dulo de seguridad de la base de datos.";
$MESS["SEC_SESSION_ADMIN_DB_OFF"] = "Sesi�n de la base de datos no est�n almacenados el m�dulo de la base de datos.";
$MESS["SEC_SESSION_ADMIN_DB_BUTTON_OFF"] = "No almacene la sesi�n de la base de datos en el m�dulo de seguridad de la base de datos";
$MESS["SEC_SESSION_ADMIN_DB_BUTTON_ON"] = "Almacenar la sesi�n de la base de datos en el m�dulo de seguridad de la base de datos";
$MESS["SEC_SESSION_ADMIN_SESSID_TAB"] = "Cambio del ID";
$MESS["SEC_SESSION_ADMIN_SESSID_TAB_TITLE"] = "Configurar la rotaci�n de ID�S de la sesi�n";
$MESS["SEC_SESSION_ADMIN_SESSID_ON"] = "El cambio del ID de la sesi�n est� habilitado.";
$MESS["SEC_SESSION_ADMIN_SESSID_OFF"] = "El cambio del ID de la sesi�n est� desactivado.";
$MESS["SEC_SESSION_ADMIN_SESSID_BUTTON_OFF"] = "Cambio del ID desactivado";
$MESS["SEC_SESSION_ADMIN_SESSID_BUTTON_ON"] = "Habilitar cambio del ID ";
$MESS["SEC_SESSION_ADMIN_SESSID_TTL"] = "Tiempo de duraci�n del ID de la sesi�n, seg.";
$MESS["SEC_SESSION_ADMIN_SAVEDB_TAB_TITLE"] = "Configurar el almacenamiento de la sesi�n de la data en la base de datos";
$MESS["SEC_SESSION_ADMIN_DB_NOTE"] = "<p>La mayor&iacute;a de los ataques web se producen al intentar robar datos de una sesi&oacute;n. Al habilitar la  <b>Prototecci&oacute;n de sesi&oacute;n </b> hacemos a esta sencible a los ataques.</p>
<p>Adicionalmente al la protecci&oacute;n de sesiones estandard, usted puede configurar opciones en las preferencias de cada grupo de usuarios en la  <b>protecci&oacute;n proactiva de la sesi&oacute;n </b>:
<ul style='font-size:100%'>
<li>Cambiando el ID de la sesi&oacute;n despu&eacute;s de cierto periodo de tiempo;</li>
<li>Almacenando los datos de la sesi&ntilde;n en una  base de  datos del m&oacute;dulo.</li>
</ul>
<p>Almacenando los datos de la sesi&oacute;n en la base de datos del m&oacute;dulo evitamos que los datos sean robados por scripts ejecutados en el servidor virtual, scripts que se aprovechen de una mala confguraci&oacute;n, mala asignaci&oacute;n de permisos en las carpetas personales y otros problemas relacinados con el sistema operativo. Esto tambi&eacute;n reduce la carga del sistema de archivos y el proceso de descarga del servidor de la base de datos.</p>
<p><i>Recomendado para un nivel alto .</i></p>";
$MESS["SEC_SESSION_ADMIN_SESSID_NOTE"] = "<p>Si esta caracter&iacute;stica es habilitada, la ID de la sesi&oacute;n ser&aacute; cambiada despu&eacute;s del periodo de tiempo especificado. Esto le dar&aacute; mayor trabajo al servidor, pero obviamente har&aacute; imposible el secuestro de los datos de la sesi&oacute;n.</p>
<p><i>Recomendado para un nivel alto .</i></p>";
$MESS["SEC_SESSION_ADMIN_DB_WARNING"] = "�Atenci�n! Alternar el per�odo de sesiones o desactivar este modo har� que los usuarios autorizados a pierdan su actual autorizaci�n (lso datos de la sesi�n ser�n destruidos).";
$MESS["SEC_SESSION_ADMIN_SESSID_WARNING"] = "La ID de sesi�n no es compatible con el m�dulo de Protecci�n Proactia. El identificador retornado con la funic�n session_id() debe no tener m�s de 32 caracteres y deber�a contener s�lo d�gitos y caracteres latinos.";
?>