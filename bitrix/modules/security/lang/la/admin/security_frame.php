<?
$MESS["SEC_FRAME_TITLE"] = "Protecci�n anti-formas";
$MESS["SEC_FRAME_MAIN_TAB"] = "Protecci�n anti-formas";
$MESS["SEC_FRAME_MAIN_TAB_TITLE"] = "Protecci�n anti-formas";
$MESS["SEC_FRAME_ON"] = "Protecci�n anti-formas esta activada";
$MESS["SEC_FRAME_EXCL_FOUND"] = "Existen exclusiones.";
$MESS["SEC_FRAME_OFF"] = "Protecci�n anti-formas esta desactivada";
$MESS["SEC_FRAME_BUTTON_OFF"] = "Desactivar la protecci�n de anti-formas";
$MESS["SEC_FRAME_BUTTON_ON"] = "Habilitar la protecci�n de anti-formas";
$MESS["SEC_FRAME_NOTE"] = "<p>Proteja su sitio web y de los usuarios contra UI corregir, clickjacking y framesniffing mediante la desactivaci�n o restringir en la pantalla de tu sitio web las p�ginas en formas.Esto tambi�n reducir� el riesgo de ataques de secuencias de comandos entre sitios sustancialmente.</p>";
$MESS["SEC_FRAME_EXCEPTIONS_TAB"] = "Excepciones";
$MESS["SEC_FRAME_EXCEPTIONS_TAB_TITLE"] = "No se aplicar� a las p�ginas que coincidan con el filtro de protecci�n.";
$MESS["SEC_FRAME_SITE"] = "sitio Web:";
$MESS["SEC_FRAME_MASKS"] = "M�scaras de exclusi�n: <br>(e.g.: /bitrix/* or */news/*)";
$MESS["SEC_FRAME_ADD"] = "Agregar";
?>