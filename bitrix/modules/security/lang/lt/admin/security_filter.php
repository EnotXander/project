<?
$MESS["SEC_FILTER_TITLE"] = "Proaktyvus filtras";
$MESS["SEC_FILTER_MAIN_TAB"] = "Proaktyvus filtras";
$MESS["SEC_FILTER_MAIN_TAB_TITLE"] = "�jungti arba i�jungti proaktyv� filtr�";
$MESS["SEC_FILTER_ON"] = "Proaktyvi apsauga �jungta.";
$MESS["SEC_FILTER_EXCL_FOUND"] = "Yra filtro i�imtys.";
$MESS["SEC_FILTER_OFF"] = "Proaktyvi apsauga i�jungta.";
$MESS["SEC_FILTER_BUTTON_OFF"] = "I�jungti proaktyvi� apsaug�.";
$MESS["SEC_FILTER_BUTTON_ON"] = "�jungti proaktyvi� apsaug�.";
$MESS["SEC_FILTER_PARAMETERS_TAB"] = "Aktyvi reakcija";
$MESS["SEC_FILTER_PARAMETERS_TAB_TITLE"] = "Konfig�ruoti sistemos reakcij� � �sibrovimus";
$MESS["SEC_FILTER_ACTION"] = "Reakcija � �sibrovimus";
$MESS["SEC_FILTER_ACTION_FILTER"] = "Apsaugoti duomenis";
$MESS["SEC_FILTER_ACTION_CLEAR"] = "Pa�alinti pavojingus duomenis";
$MESS["SEC_FILTER_ACTION_NONE"] = "Praleisti pavojingus duomenis";
$MESS["SEC_FILTER_STOP"] = "Prid�ti u�puoliko IP adres� � Stop-s�ra��";
$MESS["SEC_FILTER_DURATION"] = "Prid�ti � stop-s�ra�� (min.)";
$MESS["SEC_FILTER_LOG"] = "Prid�ti �sibrovimo bandymus � �urnal�";
$MESS["SEC_FILTER_EXCEPTIONS_TAB"] = "I�imtys";
$MESS["SEC_FILTER_EXCEPTIONS_TAB_TITLE"] = "Nurodykite kauk� tiems URL, kurie nebus atmesti";
$MESS["SEC_FILTER_SITE"] = "svetainei:";
$MESS["SEC_FILTER_MASKS"] = "I�imtys: <br>(e.g.: /bitrix/* arba */news/*)";
$MESS["SEC_FILTER_ADD"] = "Prid�ti";
?>