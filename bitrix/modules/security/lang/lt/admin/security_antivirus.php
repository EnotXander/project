<?
$MESS["SEC_ANTIVIRUS_TITLE"] = "Web antivirusas";
$MESS["SEC_ANTIVIRUS_MAIN_TAB"] = "Web antivirusas";
$MESS["SEC_ANTIVIRUS_MAIN_TAB_TITLE"] = "Aktyvuoti Web antivirus�.";
$MESS["SEC_ANTIVIRUS_PARAMETERS_TAB"] = "Parametrai";
$MESS["SEC_ANTIVIRUS_PARAMETERS_TAB_TITLE"] = "Virusin�s infekcijos prane�imo parametrai.";
$MESS["SEC_ANTIVIRUS_WHITE_LIST_TAB"] = "I�imtys";
$MESS["SEC_ANTIVIRUS_WHITE_LIST_SET_TAB"] = "I�imtys (s�ra�as)";
$MESS["SEC_ANTIVIRUS_ON"] = "Web antivirusas aktyvuotas";
$MESS["SEC_ANTIVIRUS_BUTTON_OFF"] = "I�jungti Web antivirus�";
$MESS["SEC_ANTIVIRUS_OFF"] = "Web antivirusas i�jungtas";
$MESS["SEC_ANTIVIRUS_BUTTON_ON"] = "Aktyvuoti Web antivirus�";
$MESS["SEC_ANTIVIRUS_TIMEOUT"] = "Prane�imo intervalas (min)";
$MESS["SEC_ANTIVIRUS_WHITE_LIST"] = "I�imtys:";
$MESS["SEC_ANTIVIRUS_ADD"] = "Prid�ti";
$MESS["SEC_ANTIVIRUS_ACTION"] = "Veiksmas, kai aptiktas virusas";
$MESS["SEC_ANTIVIRUS_ACTION_REPLACE"] = "I�kirpti objekt� i� svetain�s kodo";
$MESS["SEC_ANTIVIRUS_ACTION_NOTIFY_ONLY"] = "�ra�yti � �urnal�  ir prane�ti administratoriui";
?>