<?
$MESS["SECURITY_OTP_DESCR"] = "<p>�iandien j�s� Bitrix24 yra apsaugotas duomen� �ifravimo technologija ir prisijungimo vardu bei slapta�od�iu kiekvienam naudotojui. Ta�iau yra�
yra b�dai, kaip piktybinis naudotojas gali patekti � j�s� kompiuter� ir pavogti �iuos duomenis.�
<br/><br/>
Mes primygtinai rekomenduojame jums pereiti prie dviej� pakop� autentifikavimo strategijos. <br /> <br /> Dviej� �ingsni� autenti�kumas rei�kia, kad naudotojas tur�s praeiti dviej� lygi� patikrinim�. Pirma, naudotojai �veda savo slapta�od�ius. Po to jie turi �vesti vienkartin�
apsaugos kod�, i�si�st� � j� mobil�j� prietais�. J�s� ir verslo duomenys bus saugesni.";
$MESS["SECURITY_OTP_CONNECT"] = "�jungti dviej� etap� autentifikavim�";
$MESS["SECURITY_OTP_MOBILE"] = "Parsisi�sti Bitrix24 OTP mobiliaj� aplikacij�";
$MESS["SECURITY_OTP_MOBILE_TMP"] = "Parsisi�sti FreeOTP or Google autentifikatoriaus mobiliaj� aplikacij�";
$MESS["SECURITY_OTP_MOBILE2"] = "j�s� telefonui nuo AppStore arba GooglePlay";
$MESS["SECURITY_OTP_MOBILE2_TMP"] = "j�s� telefonui nuo AppStore arba GooglePlay";
$MESS["SECURITY_OTP_APP_EXECUTE"] = "Paleisti aplikacij�";
$MESS["SECURITY_OTP_APP_EXECUTE2"] = "paspauskite mygtuk� <strong>Konfig�ruoti</strong>";
$MESS["SECURITY_OTP_APP_EXECUTE_TMP"] = "ir paspauskite miniati�r�, nor�dami prid�ti nauj� paskyr�";
$MESS["SECURITY_OTP_CHOOSE_TYPE"] = "Pasirinkite pageidaujam� metod�, nor�dami gauti patvirtinimo kod�";
$MESS["SECURITY_OTP_SCAN_CODE"] = "Nuskaityti QR kod�";
$MESS["SECURITY_OTP_SCAN_DESCR"] = "Nor�dami nuskaityti kod�, prid�kite savo mobiliojo telefono kamer� prie ekrano ir lauite, kol aplikacija nuskaitys kod�.";
$MESS["SECURITY_OTP_HAND_TYPE"] = "�veskite kod� rankiniu b�du";
$MESS["SECURITY_OTP_HAND_DESCR"] = "Jei negalite nuskaityti kod�, �veskite j� rankiniu b�du.
<br /> J�s turite nurodyti svetain�s (ar Bitrix24) adres�, savo el.pa�t�, patikrinimo �od� ir pasirinkti pagrindin� tip�.";
$MESS["SECURITY_OTP_ENTER_CODE"] = "�veskite patvirtinimo kod�";
$MESS["SECURITY_OTP_ENTER_CODE_PL"] = "�veskite kod�";
$MESS["SECURITY_OTP_ENTER_CODE_PL1"] = "Pirmas kodas";
$MESS["SECURITY_OTP_ENTER_CODE_PL2"] = "Antras kodas";
$MESS["SECURITY_OTP_CODE_DESCR"] = "Kai kodas bus s�kmingai nuskaitytas ar �ra�ytas rankiniu b�du, j�s� mobilusis telefonas parodys kod�, kur� j�s tur�site �vesti �emiau.";
$MESS["SECURITY_OTP_DONE"] = "Atlikta";
$MESS["SECURITY_OTP_CODE_DESCR2"] = "Nor�dami u�baigti inicializacij�, spauskite mygtuk� \"Gauti nauj� kod�\" mobilioje aplikacijoje ir �veskite kit� kod�, kur� pamatysite mobiliojo telefono ekrane.";
$MESS["SECURITY_OTP_ERROR_TITLE"] = "Nepavyko i�saugoti, nes �vyko klaida.";
$MESS["SECURITY_OTP_UNKNOWN_ERROR"] = "Netik�ta klaida. Pra�ome pabandyti v�liau.";
$MESS["SECURITY_OTP_CODE_INFO_TOTP"] = "Pagal laik�";
$MESS["SECURITY_OTP_CODE_INFO_HOTP"] = "Pagal skaitliuk�";
$MESS["SECURITY_OTP_DESCR1"] = "<p>�iandien j�s� Bitrix24 yra apsaugotas duomen� �ifravimo technologija ir prisijungimo vardu bei slapta�od�iu kiekvienam naudotojui. Ta�iau yra�
yra b�dai, kaip piktybinis naudotojas gali patekti � j�s� kompiuter� ir pavogti �iuos duomenis.�
<br/><br/>�
Bitrix24 dabar turi dviej� pakop� autentifikavim�, specialius metodus apsisaugoti nuo �silau��li� programin�s �rangos. Jums primygtinai rekomenduojama �jungti �i� nauj� funkcij� geresniam saugumui u�tikrinti.�
<br/><br/>�
Dviej� �ingsni� autenti�kumas rei�kia, kad naudotojas tur�s praeiti dviej� lygi� patikrinim�. Pirma, naudotojai �veda savo slapta�od�ius. Po to jie turi �vesti vienkartin�
apsaugos kod�, i�si�st� � j� mobil�j� prietais� kiekvien� kart�, kai jie prisijungia.�
<br/><br/>�
Padarykite savo verslo duomenis dar saugesnius!</p>� ";
?>