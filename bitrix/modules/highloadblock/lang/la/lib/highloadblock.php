<?
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_NAME_FIELD_INVALID"] = "El nombre de la entidad debe comenzar con una letra y s�lo puede incluir caracteres alfanum�ricos.";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_TABLE_NAME_FIELD"] = "Nombre de la tabla de base de datos";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_NAME_FIELD"] = "Nombre de la entidad";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_NAME_FIELD_REGEXP_INVALID"] = "Nombre de la entidad debe comenzar con una may�scula y puede incluir s�lo los caracteres latinos y n�meros";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_TABLE_NAME_FIELD_REGEXP_INVALID"] = "Nombre de la tabla s�lo puede incluir los caracteres latinos, n�meros y guiones bajos";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_NAME_FIELD_LENGTH_INVALID"] = "Nombre de la entidad no debe tener m�s de 100 caracteres.";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_TABLE_NAME_FIELD_LENGTH_INVALID"] = "Nombre de la tabla no debe tener m�s de 64 caracteres.";
?>