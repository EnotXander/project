<?
$MESS["BPATT_BACK"] = "بازگشت";
$MESS["BPATT_AE_DELETE"] = "حذف";
$MESS["BPATT_DO_DELETE1"] = "حذف";
$MESS["BPATT_DO_EDIT1"] = "ویرایش";
$MESS["BPATT_F_EDIT"] = "ویرایش";
$MESS["BPATT_ERROR"] = "خطا";
$MESS["BPATT_MODIFIED"] = "ویرایش شده";
$MESS["BPATT_USER"] = "ویرایش شده توسط";
$MESS["BPATT_NAME"] = "نام";
$MESS["BPATT_F_NAME"] = "نام";
$MESS["BPATT_AE_NONE"] = "خیر";
$MESS["BPATT_USER_PROFILE"] = "پروفایل کاربر";
?>