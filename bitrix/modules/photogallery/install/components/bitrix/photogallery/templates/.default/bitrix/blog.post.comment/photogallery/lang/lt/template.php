<?
$MESS["B_B_MS_FROM"] = "i�";
$MESS["B_B_MS_REPLY"] = "Atsakyti";
$MESS["B_B_MS_PARENT"] = "Pagrindinis";
$MESS["B_B_MS_LINK"] = "Nuoroda";
$MESS["B_B_MS_NAME"] = "Pavadinimas:";
$MESS["B_B_MS_CAPTCHA_SYM"] = "Simboliai ant paveiksliuko:";
$MESS["B_B_MS_SEND"] = "Si�sti";
$MESS["B_B_MS_SAVE"] = "I�saugoti";
$MESS["B_B_MS_ADD_COMMENT"] = "Prid�ti komentar�";
$MESS["BLOG_P_INSERT_IMAGE_LINK"] = "�terpti nuorod� � atvaizd�";
$MESS["BPC_BOLD"] = "Ry�kus";
$MESS["BPC_ITALIC"] = "Kursyvinis";
$MESS["BPC_UNDER"] = "Pabr��tas";
$MESS["BPC_FONT"] = "�riftas";
$MESS["BPC_CLOSE_OPENED_TAGS"] = "U�daryti visas atidarytas �ymes";
$MESS["BPC_CLOSE_ALL_TAGS"] = "U�daryti visas �ymes";
$MESS["BPC_HYPERLINK"] = "Hipernuoroda";
$MESS["BPC_IMAGE"] = "Spalva";
$MESS["BPC_LINK"] = "�terpti nuorod�";
$MESS["BPC_CODE"] = "Kodas";
$MESS["BPC_LIST"] = "Sukurti s�ra��";
$MESS["BPC_QUOTE"] = "Citata";
$MESS["BPC_TEXT_ENTER_URL"] = "�vesti piln� adres� (URL)";
$MESS["BPC_TEXT_ENTER_URL_NAME"] = "�veskite tekst�, kuris bus rodomas kaip hipernuoroda";
$MESS["BPC_TEXT_ENTER_IMAGE"] = "�veskite piln� vaizdo adres� (URL)";
$MESS["BPC_LIST_PROMPT"] = "�veskite s�ra�o element�. Paspauskite \"At�aukti\" arba �veskite tarp� u�baigti s�ra��";
$MESS["BPC_ERROR_NO_URL"] = "�veskite adres� (URL)";
$MESS["BPC_ERROR_NO_TITLE"] = "�veskite antra�t�";
$MESS["BLOG_CATEGORY_NAME"] = "�ym�s pavadinimas";
$MESS["BPC_PAGE"] = "Puslapiai:";
$MESS["BPC_MES_DELETE_POST_CONFIRM"] = "Ar tikrai norite pa�alinti �� komentar�? ";
$MESS["BPC_MES_DELETE"] = "Pa�alinti";
$MESS["BPC_MES_EDIT"] = "Redaguoti";
$MESS["B_B_MS_PREVIEW"] = "Per�i�ra";
$MESS["B_B_MS_PREVIEW_TITLE"] = "Per�i�ra";
$MESS["BPC_STRIKE"] = "Sukirsti";
$MESS["BPC_SUBJECT"] = "Tema";
$MESS["BPC_MES_SHOW"] = "Rodyti";
$MESS["BPC_MES_HIDE"] = "Sl�pti";
$MESS["BPC_HIDDEN_COMMENT"] = "(Sl�pti komentar�)";
$MESS["BPC_SMILE"] = "Visos �ypsen�l�s";
$MESS["BLOG_P_IMAGE_LINK"] = "�terpti vaizdo nuorod�";
$MESS["FPF_VIDEO"] = "�terpti video";
$MESS["BPC_VIDEO_P"] = "Video kelias";
$MESS["BPC_VIDEO_PATH_EXAMPLE"] = "Pavyzdys: <i>http://www.youtube.com/watch?v=j8YcLyzJOEg</i> <br/> arba <i>www.mysite.com/video/my_video.mp4</i>";
$MESS["BPC_IMAGE_SIZE_NOTICE"] = "Minimalus vaizdo dydis yra <b>#WIDTH#</b> x <b>#HEIGHT#</b> pikseli�. Didesni vaizdai bus suma�inti automati�kai.";
$MESS["BPC_MES_SPAM"] = "�lam�tas?";
$MESS["BPC_MES_SPAM_TITLE"] = "Valdyti visus naudotojo komentarus";
?>