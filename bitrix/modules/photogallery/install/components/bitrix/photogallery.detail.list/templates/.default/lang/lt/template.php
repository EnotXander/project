<?
$MESS["P_SELECT_ALL"] = "Pasirinkti visas";
$MESS["P_DELETE_SELECTED"] = "�alinti nuotrauk�";
$MESS["P_MOVE_SELECTED"] = "Perkelti nuotrauk�";
$MESS["P_MOVE_SELECTED_IN"] = "Perkelti �";
$MESS["P_DELETE_CONFIRM"] = "Ar tikrai norite negr��tamai pa�alinti nuotraukas?";
$MESS["P_PHOTO_NOT_APPROVED"] = "[pasl�pta]";
$MESS["P_DEFAULT_TEMPLATE"] = "numatytasis";
$MESS["P_DEFAULT_TEMPLATE_TITLE"] = "Nepakeist� nuotrauk� miniati�ros ";
$MESS["P_SQUARE_TEMPLATE"] = "kvadratas";
$MESS["P_SQUARE_TEMPLATE_TITLE"] = "Kvadratini� nuotrauk� miniati�ros";
$MESS["P_RECTANGLE_TEMPLATE"] = "sta�iakampis";
$MESS["P_RECTANGLE_TEMPLATE_TITLE"] = "Dyd�io pakoreguot� nuotrauk� miniati�ros ";
$MESS["P_VIEW"] = "Per�i�ra";
$MESS["P_VIEW_TITLE"] = "Atvaizduoti nuotrauk� album�";
$MESS["P_EDIT"] = "Redaguoti";
$MESS["P_EDIT_TITLE"] = "Redaguoti nuotrauk� album�";
$MESS["P_STANDARD"] = "Standartinis";
$MESS["P_PICTURES_SIGHT"] = "Miniati�ros";
$MESS["P_PICTURES_SIGHT_TITLE"] = "Nuotrauk� miniati�ros";
$MESS["P_SLIDE_SHOW"] = "Skaidri� prezentacija (slide show)";
$MESS["T_IBLOCK_VOTE_RESULTS"] = "(Bals�: #VOTES#, Reitingas: #RATING#)";
$MESS["T_IBLOCK_VOTE_NO_RESULTS"] = "(n�ra bals�)";
$MESS["P_SHOWS"] = "Per�i�r� skai�ius";
$MESS["P_COMMENTS"] = "Komentar� skai�ius";
$MESS["P_FULL_SCREEN"] = "Visas ekranas";
$MESS["P_FULL_SCREEN_TITLE"] = "Per�i�r�ti �i� nuotrauk� viso ekrano re�ime";
$MESS["P_ELEMENTS_MOVE"] = "Perkelti nuotrauk� � kit� album�";
$MESS["P_SELECT_ALBUM"] = "Pasirinkite album� � kur� nor�site perkelti nuotraukas";
$MESS["P_MOVE"] = "Perkelti";
$MESS["P_CANCEL"] = "At�aukti";
?>