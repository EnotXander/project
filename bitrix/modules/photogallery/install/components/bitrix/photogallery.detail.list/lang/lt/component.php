<?
$MESS['IBLOCK_MODULE_NOT_INSTALLED'] = 'Informacini� blok� modulis n�ra �diegtas';
$MESS['P_PHOTOS'] = 'Nuotraukos';
$MESS['P_LIST_PHOTO'] = 'Nuotrauk� s�ra�as';
$MESS['P_BAD_IBLOCK_ID'] = 'Nuotrauka nepriklauso albumui.';
$MESS['P_DELETE_ERROR'] = 'Klaida �alinant nuotrauk�.';
$MESS['P_MODULE_IS_NOT_INSTALLED'] = 'Nuotrauk� galerijos 2.0 modulis n�ra �diegtas.';
$MESS['P_SECTION_IS_NOT_IN_GALLERY'] = 'Albumas nepriklauso nurodytai galerijai';
$MESS['P_SECTION_EMPTY_TO_MOVE'] = 'Nenurodytas paskirties albumas.';
$MESS['P_SECTION_THIS_TO_MOVE'] = 'Blogai nurodytas paskirties albumas';
$MESS['P_SECTION_NOT_FOUND'] = 'Albumas nerastas';
$MESS['P_GALLERY_NOT_FOUND'] = 'Galerija nerasta';
$MESS['P_DENIED_ACCESS'] = 'Prieigos n�ra.';
?>