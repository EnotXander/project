<?
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Informacini� blok� modulis n�ra �diegtas";
$MESS["P_MODULE_IS_NOT_INSTALLED"] = "Nuotrauk� galerijos 2.0 modulis n�ra �diegtas.";
$MESS["P_SECTION_EMPTY"] = "Nenurodytas albumo kodas";
$MESS["P_GALLERY_EMPTY"] = "Nenurodytas galerijos kodas";
$MESS["P_SECTION_ACCESS_DENIED"] = "Prieiga prie albumo u�drausta.";
$MESS["P_SECTION_NOT_FOUND"] = "Albumas nerastas";
$MESS["P_SECTION_NOT_RIGTH"] = "Albumo per�i�rai n�ra prieigos teisi�.";
$MESS["P_GALLERY_NOT_FOUND"] = "Galerija nerasta";
$MESS["P_DENIED_ACCESS"] = "Prieigos n�ra.";
?>