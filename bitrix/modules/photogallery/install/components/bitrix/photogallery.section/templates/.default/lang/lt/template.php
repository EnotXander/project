<?
$MESS["P_ADD_ALBUM"] = "Prid�ti album�";
$MESS["P_UPLOAD"] = "�kelti foto album�";
$MESS["P_SECTION_EDIT"] = "Redaguoti foto albumo savybes";
$MESS["P_SECTION_EDIT_ICON"] = "Redaguoti albumo vir�el�";
$MESS["P_SECTION_DELETE"] = "�alinti album�";
$MESS["P_SECTION_DELETE_ASK"] = "Ar tikrai norite negr��tamai pa�alinti album�?";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "Albumas yra pasl�ptas.";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "Albumas yra pasl�ptas ir apsaugotas slapta�od�iu.";
$MESS["P_ALBUM_IS_PASSWORDED"] = "Albumas yra apsaugotas slapta�od�iu.";
$MESS["P_PHOTOS_CNT"] = "Nuotraukos";
$MESS["P_ALBUMS_CNT"] = "Albumai";
$MESS["P_ADD_ALBUM_TITLE"] = "Prid�ti nauj� foto album�";
$MESS["P_EDIT_ICON"] = "Pasirinkti nuotrauk� albumo aplankal�";
$MESS["P_UP"] = "Album� s�ra�as";
$MESS["P_UP_TITLE"] = "Pereiti prie album� s�ra�o";
$MESS["P_PASSWORD_TITLE"] = "�is albumas yra apsaugotas slapta�od�iu";
$MESS["P_PASSWORD"] = "�vesti slapta�od�";
$MESS["P_SUPPLY_PASSWORD"] = "�vesti slapta�od�";
?>