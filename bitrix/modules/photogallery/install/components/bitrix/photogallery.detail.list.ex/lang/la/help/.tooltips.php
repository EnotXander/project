<?
$MESS["IBLOCK_TYPE_TIP"] = "Seleccionar aqu� uno de los tipos de block de informaci�n existentes. Oprimir <b><i>OK</i></b> para cargar los blocks de informaci�n del tipo seleccionado.";
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: el cache es v�lido durante el tiempo predefinido en las configuraciones del cache;<br /><i>Cache</i>: el cache siempre se especificar� en el siguiente campo;<br /><i>Do not cahce</i>: ning�n cache est� funcionando.";
$MESS["CACHE_TIME_TIP"] = "Especificar aqu� el per�odo de tiempo durante el cual el cache es v�lido.";
$MESS["SEARCH_URL_TIP"] = "Ruta de la p�gina de b�squeda.";
$MESS["IBLOCK_ID_TIP"] = "Especificar aqu� el block de informaci�n que almacenar� las fotos. Alternativamente, usted podr� seleccionar <b>(other)-></b> y especificar el ID del block de informaci�n en el campo de al lado.";
$MESS["SECTION_ID_TIP"] = "Este campo contiene una expresi�n que eval�a para la selecci�n del ID del (album) ";
$MESS["ELEMENT_LAST_TYPE_TIP"] = "Especificar el manera de fotos ser� seleccionado para la muestra.";
$MESS["USE_DESC_PAGE_TIP"] = "Esecificar el uso del breadcrumb inverso de navegaci�n en �a p�gina de fotos.";
$MESS["ELEMENT_SORT_FIELD_TIP"] = "Especificar aqu� el campo por el cual las fotos ser�n clasificados.";
$MESS["ELEMENT_SORT_ORDER_TIP"] = "Especificar el orden de clasificaci�n.";
$MESS["ADDITIONAL_SIGHTS_TIP"] = "Seleccionar aqu� los tama�os de las miniaturas que estar�n disponibles.";
$MESS["PICTURES_SIGHT_TIP"] = "Seleccionar aqu� el tama�o inicia�l de las miniaturas mostradas en la p�gina del �lbum.";
$MESS["DETAIL_URL_TIP"] = "Especifica la direcci�n de la p�gina de detalle del �lbum.";
$MESS["PAGE_ELEMENTS_TIP"] = "Especificar el n�mero de fotos por p�gina. Las fotos que queden fuera de la p�gina estar�n disponibles a trav�s del breadcrumb de navegaci�n. ";
$MESS["USE_PERMISSIONS_TIP"] = "Especificaciones para restringir el acceso del �lbum.";
$MESS["GROUP_PERMISSIONS_TIP"] = "Especificar aqu� aquellos miembros de los grupos de usuarios que pueden ver el �lbum.";
$MESS["COMMENTS_TYPE_TIP"] = "Seleccione aqu� el m�dulo cuyas funciones ser�n usado para comentar.";
$MESS["SET_TITLE_TIP"] = "Seleccionar esta opci�n para establecer el t�tulo de la p�gina a \"Fotos\".";
$MESS["DATE_TIME_FORMAT_TIP"] = "Especificar aqu� la fecha de formato a visualizar.";
$MESS["ELEMENTS_LAST_COUNT_TIP"] = "Especificar el n�mero de fotos recientes para ser mostradas.";
$MESS["ELEMENTS_LAST_TIME_TIP"] = "Especificar el n�mero de d�as recientes para lo cuales se mostrar�n las fotos.";
$MESS["ELEMENTS_LAST_TIME_FROM_TIP"] = "Especificar aqu� el primer d�a del per�odo de selecci�n.";
$MESS["ELEMENTS_LAST_TIME_TO_TIP"] = "Especificar aqu� el �ltimo d�a del per�odo de selecci�n.";
?>