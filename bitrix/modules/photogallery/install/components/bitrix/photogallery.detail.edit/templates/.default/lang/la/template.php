<?
$MESS["P_TITLE"] = "Nombre";
$MESS["P_DATE"] = "Fecha";
$MESS["P_TAGS"] = "Etiquetas";
$MESS["P_DESCRIPTION"] = "Descripci�n";
$MESS["P_SUBMIT"] = "Guardar";
$MESS["P_CANCEL"] = "Cancelar";
$MESS["P_ALBUMS"] = "�lbums";
$MESS["P_PUBLIC_ELEMENT"] = "Foto p�blica";
$MESS["P_APPROVE_ELEMENT"] = "Foto aprovada";
$MESS["P_EDIT_ELEMENT"] = "Editar propiedades de la foto";
$MESS["P_ACTIVE_ELEMENT"] = "Aprobar foto (si no se seleccion�, s�lo el propietario podr� ver la foto)";
?>