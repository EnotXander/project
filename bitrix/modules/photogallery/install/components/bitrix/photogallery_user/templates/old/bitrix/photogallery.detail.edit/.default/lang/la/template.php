<?
$MESS["P_TITLE"] = "Nombre";
$MESS["P_DATE"] = "Fecha";
$MESS["P_TAGS"] = "Etiquetas";
$MESS["P_DESCRIPTION"] = "Descripci�n";
$MESS["P_UP"] = "Ver foto";
$MESS["P_GO_TO_SECTION"] = "Volver a ver la foto";
$MESS["P_SUBMIT"] = "Guardar";
$MESS["P_CANCEL"] = "Cancelar";
$MESS["P_ALBUMS"] = "Albums";
$MESS["P_PUBLIC_ELEMENT"] = "Publicar fotos";
$MESS["P_APPROVE_ELEMENT"] = "Aprobar foto";
$MESS["P_EDIT_ELEMENT"] = "Editar propiedades de foto ";
$MESS["P_ACTIVE_ELEMENT"] = "Aprobar de fotos (si no se controla, s�lo un due�o puede ver la foto)";
?>