<?
$MESS["SEARCH_GO"] = "Pirmyn";
$MESS["SEARCH_ERROR"] = "Klaida paie�kos fraz�je: ";
$MESS["SEARCH_CORRECT_AND_CONTINUE"] = "Pra�ome i�taisyti paie�kos fraz� ir pabandyti dar kart�.";
$MESS["SEARCH_LOGIC"] = "Logi�ki operatoriai:";
$MESS["SEARCH_OPERATOR"] = "Operatorius";
$MESS["SEARCH_SYNONIM"] = "Sinonimai";
$MESS["SEARCH_DESCRIPTION"] = "Apra�ymas";
$MESS["SEARCH_AND"] = "ir";
$MESS["SEARCH_AND_ALT"] = "Operatorius <i>logi�ka ir </i> yra numanomas ir gali b�ti praleid�iamas: u�klausa &quot;contact information&quot; visi�kai atitinka  &quot;contact and information&quot;.";
$MESS["SEARCH_OR"] = "arba";
$MESS["SEARCH_OR_ALT"] = "Operatorius <i>logi�ka arba</i> leid�ia ie�koti subjektus, kuri� sud�tyje yra bent vienas i� operand�.";
$MESS["SEARCH_NOT"] = "ne";
$MESS["SEARCH_NOT_ALT"] = "Operatorius <i>logi�ka ne</i> apriboja paie�k� puslapiuose, kuri� sud�tyje n�ra operando.";
$MESS["SEARCH_BRACKETS_ALT"] = "<i>Apval�s skliausteliai</i> apibr��ia loginio operatoriaus eili�kum�.";
$MESS["SEARCH_NOTHING_TO_FOUND"] = "Deja, J�s� paie�kai neatitiko jokie dokumentai.";
$MESS["SEARCH_SORT_BY_RANK"] = "R��iuoti pagal svarbum�";
$MESS["SEARCH_SORT_BY_DATE"] = "R��iuoti pagal dat�";
$MESS["P_TAGS_CLOUD"] = "�ymi� debesis";
$MESS["P_TAGS"] = "�ym�s";
$MESS["CT_BSP_KEYBOARD_WARNING"] = "U�klausos �vesties kalba pasikeit� � \"#query#\".";
?>