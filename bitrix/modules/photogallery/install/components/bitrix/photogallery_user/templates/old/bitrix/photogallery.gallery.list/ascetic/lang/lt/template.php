<?
$MESS['P_GALLEY_BY_USER'] = 'Autorius:';
$MESS['P_UPLOAD'] = '�kelti nuotraukas';
$MESS['P_UPLOAD_TITLE'] = '�kelti nuotraukas';
$MESS['P_GALLERY_CREATE'] = 'Kurti galerij�';
$MESS['P_GALLERY_CREATE_TITLE'] = 'Kurti asmenin� galerij�';
$MESS['P_GALLERY_EDIT'] = 'Redaguoti galerijos nustatymus';
$MESS['P_GALLERY_EDIT_TITLE'] = 'Redaguoti galerijos nustatymus';
$MESS['P_GALLERY_DELETE'] = 'Trinti galerij�';
$MESS['P_GALLERY_DELETE_ASK'] = 'Ar tikrai norite trinti galerij�? �ios operacijos atkurti ne�manoma!';
$MESS['P_GALLERY_VIEW_TITLE'] = 'Per�i�r�ti albumus, esan�ius �#GALLERY#�';
$MESS['P_ERROR_CODE'] = 'Galerijos kodas neteisingas. Galerija gali veikti netinkamai.';
$MESS['P_GALLERY_ACTIVE'] = 'Numatytoji';
?>