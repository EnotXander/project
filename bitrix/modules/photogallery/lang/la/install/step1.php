<?
$MESS ['P_CREATE_NEW_IBLOCK'] = "Nuevo block de informaci�n";
$MESS ['P_CREATE_NEW_IBLOCK_NAME'] = "Nombre del block de informaci�n";
$MESS ['P_CREATE_NEW_IBLOCK_TYPE'] = "Tipo de block de informaci�n";
$MESS ['P_SELECT'] = "Seleccionar";
$MESS ['P_CREATE'] = "Crear";
$MESS ['P_ID'] = "ID";
$MESS ['P_CREATE_NEW_BLOG'] = "Crear blog";
$MESS ['P_NAME'] = "Nombre";
$MESS ['P_DESCRIPTION'] = "Descripci�n";
$MESS ['P_NAME_LATIN'] = "Nombre (s�lo letras latinas)";
$MESS ['P_GROUP_BLOG'] = "Grupo del blog";
$MESS ['P_INSTALL'] = "Instalar";
?>