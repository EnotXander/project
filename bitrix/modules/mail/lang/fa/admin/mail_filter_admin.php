<?
$MESS["MAIL_FILT_ADM_ACTIONS"] = "اقدامات";
$MESS["MAIL_FILT_ADM_CHANGE"] = "ویرایش";
$MESS["MAIL_FILT_ADM_SUBJECT"] = "موضوع";
$MESS["MAIL_FILT_ADM_LISTEMPTY"] = "لیست خالی است";
$MESS["MAIL_FILT_ADM_RECIPIENT"] = "دریافت کننده";
$MESS["MAIL_FILT_ADM_LISTTOTAL"] = "مجموع:";
$MESS["MAIL_FILT_ADM_DEL"] = "حذف";
$MESS["MAIL_FILT_ADM_FILT_MBOX"] = "صندوق نامه";
$MESS["MAIL_FILT_ADM_MBOX"] = "صندوق نامه";
$MESS["MAIL_FILT_ADM_NAVIGATION"] = "قوانین";
$MESS["MAIL_FILT_ADM_SORT"] = "ترتیب";
$MESS["MAIL_FILT_ADM_EQUAL_STRING"] = "رشته";
$MESS["MAIL_FILT_ADM_STRING"] = "رشته";
$MESS["MAIL_FILT_ADM_FILT_NAME"] = "نام";
$MESS["MAIL_FILT_ADM_NAME"] = "نام";
?>