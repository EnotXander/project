<?
$MESS["MAIL_MBOX_EDT_TITLE_2"] = "صندوق نامه جدید";
$MESS["MAIL_MBOX_EDT_TAB"] = "صندوق نامه";
$MESS["MAIL_MBOX_EDT_ID"] = "شناسه:";
$MESS["MAIL_MBOX_EDT_LANG"] = "سایت:";
$MESS["MAIL_MBOX_EDT_ACT"] = "فعال:";
$MESS["MAIL_MBOX_EDT_NAME"] = "نام:";
$MESS["MAIL_MBOX_EDT_DESC"] = "توضیحات:";
$MESS["MAIL_MBOX_EDT_LOGIN"] = "نام کاربری:";
$MESS["MAIL_MBOX_EDT_PASSWORD"] = "رمز عبور:";
$MESS["MAIL_MBOX_EDT_ADD"] = "افزودن...";
$MESS["MAIL_MBOX_SERVER_TYPE"] = "نوع:";
$MESS["MAIL_MBOX_EDT_KEEP_DAYS_D"] = "روز";
$MESS["MAIL_MBOX_EDT_MAX_SIZE_KB"] = "کیلوبایت";
?>