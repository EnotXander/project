<?
$MESS['MAIL_MODULE_NAME'] = 'El. pa�to nustatymai';
$MESS['MAIL_MODULE_DESC'] = 'Pa�to modulis skirtas el. lai�k� gavimui, j� filtravimui ir nurodyt� veiksm� atlikimui.';
$MESS['MAIL_INSTALL_TITLE'] = 'Darbo su el. pa�tu modulio �alinimas';
$MESS['MAIL_UNINSTALL_WARNING'] = 'D�mesio! Modulis bus pa�alintas i� sistemos.';
$MESS['MAIL_UNINSTALL_SAVEDATA'] = 'J�s galite i�saugoti duomenis duomen� baz�s lentel�se, pa�ym�kite \"I�saugoti lenteles\"';
$MESS['MAIL_UNINSTALL_SAVETABLE'] = 'I�saugoti lenteles';
$MESS['MAIL_UNINSTALL_DEL'] = '�alinti';
$MESS['MAIL_UNINSTALL_ERROR'] = 'Nepavyko pa�alinti. �vyko klaida. :';
$MESS['MAIL_UNINSTALL_COMPLETE'] = '�alinimas baigtas.';
$MESS['MAIL_INSTALL_BACK'] = 'Atgal � moduli� administravim�';
?>