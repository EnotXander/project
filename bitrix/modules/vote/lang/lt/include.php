<?
$MESS["VOTE_VOTE_NOT_FOUND"] = "Apklausa #ID# nerasta. ";
$MESS["VOTE_CHANNEL_ID_ERR"] = "Neteisinga apklausos grup�.";
$MESS["VOTE_QUESTION_EMPTY"] = "Nenurodytas klausimo tekstas.";
$MESS["VOTE_ANSWERS_EMPTY"] = "Klausimas \"#QUESTION#\" neturi atsakym�.";
$MESS["VOTE_DENIED"] = "u�daryta";
$MESS["VOTE_READ"] = "per�i�r�ti rezultatus";
$MESS["VOTE_WRITE"] = "balsuoti";
$MESS["VOTE_EDIT_MY_OWN"] = "redaguoti savo apklaus�";
$MESS["VOTE_EDIT"] = "Prid�ti apklaus�";
$MESS["VOTE_OK"] = "A�i�, kad balsavote!";
$MESS["VOTE_ALREADY_VOTE"] = "Jus negalite dalyvauti �ioje apklausoje kelis kartus.";
$MESS["VOTE_ADD"] = "Prid�ti";
$MESS["VOTE_F_ID"] = "ID:";
$MESS["VOTE_F_TITLE"] = "Pavadinimas:";
$MESS["VOTE_TITLE"] = "Pavadinimas";
$MESS["VOTE_YES"] = "taip";
$MESS["VOTE_NO"] = "ne";
$MESS["VOTE_ALL"] = "(visk�)";
$MESS["VOTE_ACTION"] = "Veiksmas";
$MESS["VOTE_F_SET_FILTER"] = "�diegti";
$MESS["VOTE_F_DEL_FILTER"] = "Atstatyti";
$MESS["VOTE_TOTAL"] = "Viso:";
$MESS["VOTE_SAVE"] = "I�saugoti";
$MESS["VOTE_RESET"] = "Atstatyti";
$MESS["VOTE_ACTIVE"] = "Aktyvus";
$MESS["VOTE_CHANGE"] = "Redaguoti";
$MESS["VOTE_DELETE"] = "�alinti";
$MESS["VOTE_ERROR"] = "Klaida !";
$MESS["VOTE_APPLY"] = "Vykdyti";
$MESS["VOTE_NOT_FOUND"] = "Neteisingas apklausos ID";
$MESS["VOTE_ENLARGE"] = "Padidinti";
$MESS["VOTE_START_DATE"] = "Prad�ios data";
$MESS["VOTE_END_DATE"] = "Pabaigos data:";
$MESS["VOTE_VOTE_IS_ACTIVE"] = "Apklausa <b>aktyvi</b>";
$MESS["VOTE_PAGES"] = "Apklausos";
$MESS["VOTE_VOTING"] = "Balsuoti";
$MESS["VOTE_RESULTS"] = "Rezultatai";
$MESS["VOTE_GROUP"] = "Grup�:";
$MESS["VOTE_VOTING_TITLE"] = "Apklausa";
$MESS["VOTE_RESULTS_TITLE"] = "Apklausos rezultatai";
$MESS["VOTE_NEW"] = "Prid�ti nauj� �ra��";
$MESS["VOTE_TILL"] = "iki";
$MESS["VOTE_F_IP"] = "IP adresas:";
$MESS["VOTE_EXACT_MATCH"] = "Ie�koti pagal visus �ra�ytus duomenis";
$MESS["VOTE_PUBLIC_ICON_TEMPLATE"] = "redaguoti �ablon�";
$MESS["VOTE_CREATE"] = "Kurti";
$MESS["VOTE_LIST"] = "S�ra�as";
$MESS["VOTE_DIAGRAM_TYPE_CIRCLE"] = "Skritulin� diagrama";
?>