<?
$MESS["VOTE_FOR_NAME"] = "Naujas balsavimas";
$MESS["VOTE_FOR_DESC"] = "#ID# - Balsavimo rezultato ID
#TIME# - Balsavimo laikas
#VOTE_TITLE# - Apklausos pavadinimas
#VOTE_DESCRIPTION# - Apklausos apra�ymas
#VOTE_ID# - Apklausos ID
#CHANNEL# - Apklausos grup�s pavadinimas
#CHANNEL_ID# - Apklausos grup�s ID
#VOTER_ID# - Rink�jo naudotojo ID
#USER_NAME# - Naudotojo pilnas vardas
#LOGIN# - prisijungimo vardas
#USER_ID# - Naudotojo ID
#STAT_GUEST_ID# - Lankytojo ID web analyz�s modulyje
#SESSION_ID# - Sesijos ID web analyz�s modulyje
#IP# - IP adresas
#VOTE_STATISTIC# - Bendra �i� apklaus� tipo statistika ( - Klausimas - Atsakymas)
#URL# - Apklausos URL";
$MESS["VOTE_FOR_SUBJECT"] = "#SITE_NAME#: [V] #VOTE_TITLE#";
$MESS["VOTE_FOR_MESSAGE"] = "#USER_NAME# balsavo apklausoje \"#VOTE_TITLE#\":
#VOTE_STATISTIC#

http://#SERVER_NAME##URL#
Automati�kai sugeneruotas prane�imas.";
$MESS["VOTE_NEW_NAME"] = "Naujas balsavimas";
$MESS["VOTE_NEW_DESC"] = "#ID# - balsavimo rezultato ID
#TIME# - balsavimo laikas
#VOTE_TITLE# - apklausos pavadinimas
#VOTE_DESCRIPTION# - apklausos apra�imas
#VOTE_ID# - apklausos ID
#CHANNEL# - apklausos grup�s pavadinimas
#CHANNEL_ID# - apklausos grup�s ID
#VOTER_ID# - balsavusio lankytojo ID
#USER_NAME# - naudotojo vardas
#LOGIN# - prisijungimo vardas
#USER_ID# - naudotojo ID
#STAT_GUEST_ID# - statistikos modulio naudotojo ID
#SESSION_ID# - statistikos modulio sesijos ID
#IP# - IP adresas";
$MESS["VOTE_NEW_SUBJECT"] = "#SITE_NAME#: Naujas balsas apklausoje \\\"[#VOTE_ID#] #VOTE_TITLE#\\\"";
?>