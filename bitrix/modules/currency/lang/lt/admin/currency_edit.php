<?
$MESS["currency_curr"] = "Valiuta";
$MESS["currency_curr_settings"] = "Valiutos nustatymai";
$MESS["currency_format_string"] = "Laukas \"Formatas\" kalbai #LANG# yra tu��ias.";
$MESS["currency_error"] = "Klaida";
$MESS["CURRENCY_EDIT_TITLE"] = "Redaguoti valiut�";
$MESS["CURRENCY_NEW_TITLE"] = "Nauja valiuta";
$MESS["currency_rate_cnt"] = "Suma";
$MESS["currency_rate"] = "Numatytasis kursas";
$MESS["currency_sort_ex"] = "R��iavimo indeksas";
$MESS["CURRENCY_FULL_NAME"] = "Pavadinimas";
$MESS["CURRENCY_FULL_NAME_DESC"] = "Pilnas valiutos pavadinimas";
$MESS["CURRENCY_FORMAT_TEMPLATE"] = "Formato �ablonas";
$MESS["CURRENCY_FORMAT_DESC"] = "Valiutos formato eilut�";
$MESS["CURRENCY_DEC_POINT_DESC"] = "De�imtainio ta�kas";
$MESS["THOU_SEP_DESC"] = "T�kstan�io atskirtuvas";
$MESS["DECIMALS_DESC"] = "Skaitmen� po kablelio kiekis";
$MESS["HIDE_ZERO_DECIMALS"] = "Apkarpyti galinius nulius nuo frakcini� kain� (vie�ojoje srityje)";
$MESS["CURRENCY_BASE_CURRENCY"] = "Tur�t� b�ti nurodyta bent viena valiuta su valiutos keitimo kursu 1";
$MESS["CURRENCY_CODES_ISO_STANDART"] = "�i�r�ti <a target=\"_blank\" href=\"#ISO_LINK#\">ISO 4217</a> standart� d�l galim� valiut� kod�. ";
?>