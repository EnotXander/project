<?
$MESS["CURRENCY_SAVE_ERR"] = "Klaida atnaujinant valiut� #ID#: #ERROR_TEXT#";
$MESS["CURRENCY_SAVE_ERR2"] = "Klaida atnaujinant valiut� #ID#";
$MESS["currency_err1"] = "Klaida �alinant valiut�.";
$MESS["CURRENCY_TITLE"] = "Valiutos";
$MESS["currency_curr"] = "Valiuta";
$MESS["CURRENCY_FULL_NAME"] = "Pavadinimas";
$MESS["currency_sort"] = "R��is";
$MESS["currency_rate_cnt"] = "Kiekis";
$MESS["currency_rate"] = "Numatytasis kursas";
$MESS["CURRENCY_A_EDIT"] = "Redaguoti valiutos savybes";
$MESS["CURRENCY_A_EDIT_TITLE"] = "Redaguoti valiutos savybes";
$MESS["currency_add"] = "Prid�ti nauj� valiut�";
$MESS["currency_list"] = "Valiutos kursas";
$MESS["CURRENCY_BASE_CURRENCY"] = "Tur�t� b�ti nurodyta bent viena valiuta su valiutos keitimo kursu 1";
$MESS["CURRENCY_CODES_ISO_STANDART"] = "�i�r�ti <a target=\"_blank\" href=\"#ISO_LINK#\">ISO 4217</a> standart� d�l galim� valiut� kod�.";
?>