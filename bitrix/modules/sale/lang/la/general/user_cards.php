<?
$MESS ['SKGUC_NO_ID'] = "El ID del registro no se ha especificado";
$MESS ['SKGUC_NO_CURRENCY'] = "La moneda para los l�mites del pago no se especifica ";
$MESS ['SKGUC_NO_USER'] = "El usuario ##ID# no puede ser encontrado";
$MESS ['SKGUC_NO_PS'] = "No puede encontrar el procesador para el sistema de pago ##ID#";
$MESS ['SKGUC_EMPTY_SUM'] = "Monto pagado no est� especificado";
$MESS ['SKGUC_EMPTY_CURRENCY'] = "Moneda de pago no especificado";
$MESS ['SKGUC_EMPTY_ID'] = "El ID del registro no est� especificada";
$MESS ['SKGUC_NO_RECID'] = "No se puede encontrar el registro ##ID#";
$MESS ['SKGUC_NO_PARAMS'] = "El conjunto de par�metros de la tarjeta de cr�dito no fue especificada";
$MESS ['SKGUC_CROSS_BOUND'] = "El l�mite del pago para esta tarjeta es #SUM1#, y usted intenta pagar #SUM2#";
$MESS ['SKGUC_NO_ACTION'] = "No se puede enocontrar elmanejadeor para el sistema de pagos ##ID#";
$MESS ['SKGUC_NO_PATH'] = "La ruta \"#FILE#\" para el procesador del sistema de pagos es inv�lido";
$MESS ['SKGUC_NO_SCRIPT'] = "No sep puede encontrar el script de integraci�n del sistema de pagos \"#FILE#\"";
?>