<?
$MESS["SKGD_NO_CURRENCY"] = "No se puede encontrar la modena ##ID";
$MESS["SKGD_NO_SITE"] = "No se puede encontrar el sitio ##ID";
$MESS["SKGD_EMPTY_DVAL"] = "El valor del descuento no se ha especificado";
$MESS["SKGD_WRONG_DBOUND"] = "Los l�mites de descuentos en efectivo son inv�lidos";
$MESS["BT_MOD_SALE_DISC_ERR_USER_GROUPS_ABSENT_SHORT"] = "No se especifican los grupos de usuarios.";
$MESS["BT_MOD_SALE_DISC_ERR_EMPTY_CONDITIONS"] = "No se especifican las condiciones de aplicaci�n.";
$MESS["BT_MOD_SALE_DISC_ERR_BAD_CONDITIONS"] = "Las aplicaciones de acciones son incorrectas.";
$MESS["BT_MOD_SALE_ERR_DSC_SITE_ID_ABSENT"] = "No se especifica el Sitio Web.";
$MESS["BT_MOD_SALE_ERR_DSC_CURRENCY_ABSENT"] = "No se especifica el tipo de moneda.";
$MESS["BT_MOD_SALE_ERR_DSC_TYPE_ABSENT"] = "No se especifica tipo de descuento.";
$MESS["BT_MOD_SALE_ERR_DSC_TYPE_BAD"] = "Tipo de descuento no es v�lido.";
$MESS["BT_MOD_SALE_ERR_DSC_VALUE_ABSENT"] = "No se especifica la cantidad de descuento.";
$MESS["BT_MOD_SALE_ERR_DSC_VALUE_BAD"] = "Importe de descuento es incorrecto.";
$MESS["BT_MOD_SALE_ERR_DSC_ABSENT"] = "El ID de descuento es incorrecto.";
$MESS["BT_MOD_SALE_DISC_ERR_EMPTY_ACTIONS_EXT"] = "No se especificaron las acciones para las normas. La norma no puede ser salvada.";
$MESS["BT_MOD_SALE_DISC_ERR_BAD_ACTIONS_EXT"] = "Acciones para la regla no son v�lidas. La norma no puede ser salvada.";
$MESS["BX_SALE_DISCOUNT_APPLY_MODE_ADD"] = "Actualizaci�n de descuento aplicado";
$MESS["BX_SALE_DISCOUNT_APPLY_MODE_LAST"] = "No se aplican si \"el �ltimo descuento\" est� activado";
$MESS["BX_SALE_DISCOUNT_APPLY_MODE_DISABLE"] = "No se aplican si otros descuentos fueron aplicados";
?>