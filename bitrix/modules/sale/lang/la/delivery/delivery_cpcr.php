<?
$MESS["SALE_DH_CPCR_NAME"] = "Express SPSR ";
$MESS["SALE_DH_CPCR_DESCRIPTION"] = "servicio de env�o express";
$MESS["SALE_DH_CPCR_DESCRIPTION_INNER"] = "Manejador SPSR . Basado en el <a href=\"http://www.cpcr.ru/novoe-otpravl.htm\" target=\"_blank\">calculador de tarifa SPSR </a>. La direcci&oacute;n de e-Store debe ser especificada en el  <a href=\"/bitrix/admin/settings.php?mid=sale&lang=en\">m&oacute;dulo de configuraciones </a>. <br />
 S&oacute;lo destinos Rusos.";
$MESS["SALE_DH_CPCR_SIMPLE_TITLE"] = "delivery express";
$MESS["SALE_DH_CPCR_SIMPLE_DESCRIPTION"] = "Cargo de entrega con el horario acordado m�ximo  (hasta 35 kg)";
$MESS["SALE_DH_CPCR_ECONOM_TITLE"] = "CPCR-Econom";
$MESS["SALE_DH_CPCR_ECONOM_DESCRIPTION"] = "La soluci�n m�s econ�mica para la entrega de los paquetes pesados y los env�os es dentro de Rusia ";
$MESS["SALE_DH_CPCR_CONFIG_TITLE"] = "Par�metros";
$MESS["SALE_DH_CPCR_CONFIG_CATEGORY"] = "Categor�a del cargo";
$MESS["SALE_DH_CPCR_CONFIG_CATEGORY_1"] = "documentos";
$MESS["SALE_DH_CPCR_CONFIG_CATEGORY_2"] = "tel�fonos m�viles";
$MESS["SALE_DH_CPCR_CONFIG_CATEGORY_3"] = "equipo de casa y oficina";
$MESS["SALE_DH_CPCR_CONFIG_CATEGORY_4"] = "seguridades";
$MESS["SALE_DH_CPCR_CONFIG_CATEGORY_5"] = "joyer�a";
$MESS["SALE_DH_CPCR_CONFIG_CATEGORY_6"] = "cosm�ticos";
$MESS["SALE_DH_CPCR_CONFIG_CATEGORY_7"] = "ropa";
$MESS["SALE_DH_CPCR_CONFIG_CATEGORY_8"] = "otro";
$MESS["SALE_DH_CPCR_ERROR_CONNECT"] = "No puede calcular el costo del delivery: error de conexi�n";
$MESS["SALE_DH_CPCR_ERROR_RESPONSE"] = "No puede calcular el costo de delivery: Mala respuesta del servidor";
$MESS["SALE_DH_CPCR_NEXT_STEP"] = "Editar ubicaciones";
$MESS["SALE_DH_CPCR_SIMPLE13_TITLE"] = "Servicio Express de la 1:00pm";
$MESS["SALE_DH_CPCR_SIMPLE13_DESCRIPTION"] = "Servicio express de entrega Puerta a puerta para la 1:00pm del d�a siguiente laborable";
$MESS["SALE_DH_CPCR_SIMPLE18_TITLE"] = "Servicio express de las 6pm";
$MESS["SALE_DH_CPCR_SIMPLE18_DESCRIPTION"] = "Servicio express de entrega Puerta a puerta para la 6:00pm del d�a siguiente laborable";
$MESS["SALE_DH_CPCR_BIZON_TITLE"] = "Squirrel Cargo";
$MESS["SALE_DH_CPCR_BIZON_DESCRIPTION"] = "Entrega de paquetes de hasta 150 libras.";
$MESS["SALE_DH_CPCR_COLIBRI_TITLE"] = "Sparrow Courier";
$MESS["SALE_DH_CPCR_COLIBRI_DESCRIPTION"] = "Entrega de env�os ligeros puerta a puerta.";
$MESS["SALE_DH_CPCR_PELICAN_TITLE"] = "Pelican Express";
$MESS["SALE_DH_CPCR_PELICAN_DESCRIPTION"] = "Env�o para paquetes de hasta 70 libras por tierra o aire.";
$MESS["SALE_DH_CPCR_FRAXT_TITLE"] = "Icebreaker Freight";
$MESS["SALE_DH_CPCR_FRAXT_DESCRIPTION"] = "Servicios personalizados de transporte para art�culos pesados ??o de gran tama�o.";
?>