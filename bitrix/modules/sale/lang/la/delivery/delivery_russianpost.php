<?
$MESS["SALE_DH_RUSSIANPOST_NAME"] = "Env�o Ruso";
$MESS["SALE_DH_RUSSIANPOST_DESCRIPTION"] = "Correo Delivery";
$MESS["SALE_DH_RUSSIANPOST_GROUND_TITLE"] = "correo terrestre";
$MESS["SALE_DH_RUSSIANPOST_AVIA_TITLE"] = "correo a�reo";
$MESS["SALE_DH_RUSSIANPOST_DESCRIPTION_INNER"] = "Manejador de Correo a Rusia. Basado en l&iacute;nea  <a href=\"http://fcr.russianpost.ru/autotarif/SelautotarifRus.aspx\" target=\"_blank\">lista de precio de correo dom&eacute;stico</a> y <a href=\"http://fcr.russianpost.ru/autotarif/Selautotarif.aspx\" target=\"_blank\">lista internacional de precios de mails</a>. La direcci&oacute;n del e-Store deber&aacute; ser especificada en el <a href=\"/bitrix/admin/settings.php?mid=sale&lang=en\">m&oacute;dule de configuraciones </a>.<br />
Entregas s&oacute;lo desde Mosc&uacute;";
$MESS["SALE_DH_RUSSIANPOST_GROUND"] = "Terrestre";
$MESS["SALE_DH_RUSSIANPOST_AVIA"] = "A�reo";
$MESS["SALE_DH_RUSSIANPOST_CONFIG_CATEGORY_23"] = "Env�o registrado en libro ";
$MESS["SALE_DH_RUSSIANPOST_CONFIG_CATEGORY_12"] = "Tarjeta registrada";
$MESS["SALE_DH_RUSSIANPOST_CONFIG_CATEGORY_13"] = "Carta registrada";
$MESS["SALE_DH_RUSSIANPOST_CONFIG_CATEGORY_26"] = "Registro de sobre postal";
$MESS["SALE_DH_RUSSIANPOST_CONFIG_CATEGORY_36"] = "Secci�n registrada";
$MESS["SALE_DH_RUSSIANPOST_CONFIG_CATEGORY_16"] = "Carta registrada";
$MESS["SALE_DH_RUSSIANPOST_CONFIG_TITLE"] = "Par�metros";
$MESS["SALE_DH_RUSSIANPOST_CONFIG_CATEGORY"] = "Tipo de correo";
$MESS["SALE_DH_RUSSIANPOST_ERROR_CONNECT"] = "No puede calcular el costo de delivery: error de conexi�n";
$MESS["SALE_DH_RUSSIANPOST_ERROR_RESPONSE"] = "No puede calcular el costo de delivery: Servidor responde mal";
$MESS["SALE_DH_RUSSIANPOST_ERROR_NOZIP"] = "No puede calcular el costo de delivery: No puede identificar el c�digo Zip";
$MESS["SALE_DH_RUSSIANPOST_DESCRIPTION_INNER2"] = "<br /><b>Note</b> el controlador requiere la propiedad de la orden<b> Utilice el c�digo postal</b>existente.";
?>