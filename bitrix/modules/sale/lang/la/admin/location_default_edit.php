<?
$MESS["SALE_MODULE_ACCES_DENIED"] = "Se le neg� acceso al m�dulo";
$MESS["SALE_LOCATION_E_ITEM_EDIT"] = "Lugares favoritos: #ITEM_NAME#";
$MESS["SALE_LOCATION_E_CANNOT_UPDATE_ITEM"] = "Error al actualizar el grupo";
$MESS["SALE_LOCATION_E_MAIN_TAB"] = "Ubicaciones";
$MESS["SALE_LOCATION_E_MAIN_TAB_TITLE"] = "Seleccione las ubicaciones";
$MESS["SALE_LOCATION_E_ITEM_NOT_FOUND"] = "El sitio no fue encontrado.";
$MESS["SALE_LOCATION_E_GO_BACK"] = "Volver";
$MESS["SALE_LOCATION_E_ITEM_NEW"] = "Ubicaciones favoritas";
$MESS["SALE_LOCATION_E_HEADER_LOC_LOCATION"] = "Ubicaci�n";
$MESS["SALE_LOCATION_E_HEADER_LOC_MORE"] = "M�s...";
$MESS["SALE_LOCATION_E_HEADER_LOC_SORT"] = "Clasificar";
$MESS["SALE_LOCATION_E_HEADER_LOC_REMOVE"] = "Del.";
?>