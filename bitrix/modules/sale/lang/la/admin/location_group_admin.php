<?
$MESS["SALE_SECTION_TITLE"] = "Grupos de ubicaci�n";
$MESS["SALE_F_FILTER"] = "Filtro";
$MESS["SALE_F_LOCATION"] = "Ubicaci�n";
$MESS["SALE_ALL"] = "(todo)";
$MESS["SALE_F_SUBMIT"] = "Fijar filtro";
$MESS["SALE_F_DEL"] = "Remover filtro";
$MESS["SALE_ADD"] = "Agregar";
$MESS["SALE_NAME"] = "Nombre";
$MESS["SALE_SORT"] = "Clasi.";
$MESS["SALE_ACTION"] = "Acciones";
$MESS["SALE_EDIT"] = "Modifcar";
$MESS["SALE_CONFIRM_DEL_MESSAGE"] = "�Est� seguro que desea eliminar este grupo de ubicaci�n?";
$MESS["SALE_DELETE"] = "Eliminar";
$MESS["SALE_PRLIST"] = "Grupos de ubicaci�n";
$MESS["ERROR_DELETE"] = "Un error ocurri�mientras se eliminaba";
$MESS["SALE_EDIT_DESCR"] = "Modificar preferencias el Grupo de ubicaci�n ";
$MESS["SLGAN_ADD_NEW"] = "Nuevo grupo";
$MESS["SLGAN_ADD_NEW_ALT"] = "Click para agregar un nuevo grupo";
$MESS["SALE_DELETE_TEXT"] = "Eliminar ubicaci�n de grupo ";
?>