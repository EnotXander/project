<?
$MESS["SALE_LOCATION_MIGRATE_TITLE"] = "Migrar a Ubicaciones 2,0";
$MESS["SALE_LOCATION_MIGRATE_TAB"] = "Migraci�n";
$MESS["SALE_LOCATION_MIGRATE_DO_OPERATION"] = "Migrar a Ubicaciones 2,0";
$MESS["SALE_LOCATION_MIGRATE_HELP"] = "Una vez que haga clic en \"Migrar a Ubicaciones 2,0\", su tienda web, empezar� a usar nuevas ubicaciones.";
$MESS["SALE_LOCATION_MIGRATE_COMPLETE"] = "La migraci�n se ha completado. Su tienda web utiliza ahora Ubicaciones 2.0.";
$MESS["SALE_LOCATION_MIGRATE_FAIL"] = "Eso es un error.";
$MESS["SALE_LOCATION_AJAX_FAIL"] = "Se produjo un error AJAX";
$MESS["SALE_LOCATION_MIGRATE_STOP"] = "Stop";
$MESS["SALE_LOCATION_MIGRATE_STEP"] = "Ejecutar ahora...";
$MESS["SALE_LOCATION_MIGRATE_STEP_CLEANUP_TABLES"] = "Limpieza de basura dejada por migraciones anteriores intentos";
$MESS["SALE_LOCATION_MIGRATE_STEP_CREATE_KINDOF"] = "Creaci�n de tipos de ubicaci�n";
$MESS["SALE_LOCATION_MIGRATE_STEP_MOVE_COUNTRY"] = "Mover pa�ses a la nueva tabla";
$MESS["SALE_LOCATION_MIGRATE_STEP_MOVE_REGION"] = "Mover zonas a la nueva tabla";
$MESS["SALE_LOCATION_MIGRATE_STEP_MOVE_CITY"] = "Mover ciudades a la nueva tablao. Esto puede tardar un rato";
$MESS["SALE_LOCATION_MIGRATE_STEP_MAKE_LEGACY"] = "Crear diccionarios para que coincida con la APIs heredadas del ID de las ubicaciones";
$MESS["SALE_LOCATION_MIGRATE_STEP_FIN"] = "Todo listo. Ahora descansa en paz";
$MESS["SALE_LOCATION_MIGRATE_STEP_GRAB_TREE"] = "Creaci�n de �rbol del �rea de la ciudad de dependencias";
?>