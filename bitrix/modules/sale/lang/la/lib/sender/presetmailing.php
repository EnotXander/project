<?
$MESS["PRESET_TYPE_BASKET"] = "Para el carrito de compras";
$MESS["PRESET_TYPE_ORDER"] = "Para pedidos";
$MESS["PRESET_MAIL_TEMPLATE_HELLO"] = "Hola!";
$MESS["PRESET_MAIL_TEMPLATE_REGARDS"] = "Atentamente,<br>%LINK_START%Web store%LINK_END% equipo.";
$MESS["PRESET_MAIL_TEMPLATE_UNSUB"] = "Para cancelar la suscripci�n, por favor <a href=\"#UNSUBSCRIBE_LINK#\">haz clic aqu�</a>.";
$MESS["PRESET_FORGOTTEN_BASKET_NAME"] = "Carrito abandonado";
$MESS["PRESET_FORGOTTEN_BASKET_DESC_USER"] = "Activaci�n autom�tica de correo electr�nico";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_1_SUBJECT"] = "Su carrito de compras";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_2_SUBJECT"] = "El carrito de compras est� reservada para usted";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_3_SUBJECT"] = "Descuento oculto para los que pueden esperar!";
$MESS["PRESET_CANCELED_ORDER_NAME"] = "Pedido cancelado";
$MESS["PRESET_CANCELED_ORDER_DESC_USER"] = "Activaci�n autom�tica de correo electr�nico";
$MESS["PRESET_CANCELED_ORDER_LETTER_1_SUBJECT"] = "�Desea cancelar realmente la orden?";
$MESS["PRESET_CANCELED_ORDER_LETTER_2_SUBJECT"] = "Usted tiene un carrito de compras reservado para usted";
$MESS["PRESET_CANCELED_ORDER_LETTER_3_SUBJECT"] = "Descuento oculto para los que pueden esperar!";
$MESS["PRESET_PAID_ORDER_NAME"] = "Seguimiento de correos electr�nicos";
$MESS["PRESET_PAID_ORDER_DESC_USER"] = "Activaci�n autom�tica de correo electr�nico";
$MESS["PRESET_PAID_ORDER_LETTER_1_SUBJECT"] = "Muchas gracias por su compra!";
$MESS["PRESET_PAID_ORDER_LETTER_2_SUBJECT"] = "Gracias por el negocio!";
$MESS["PRESET_PAID_ORDER_LETTER_3_SUBJECT"] = "Tu opini�n importa!";
$MESS["PRESET_PAID_ORDER_LETTER_4_SUBJECT"] = "Ofertas especiales para clientes!";
$MESS["PRESET_PAID_ORDER_LETTER_5_SUBJECT"] = "Descuento para el cliente valioso!";
$MESS["PRESET_DONT_BUY_LETTER_2_SUBJECT_90"] = "�Fue Culpa nuestra?";
$MESS["PRESET_DONT_BUY_LETTER_3_SUBJECT_90"] = "Han pasado tres meses!";
$MESS["PRESET_DONT_BUY_DESC_180"] = "Active los mensajes enviados a un cliente despu�s de que �stos han estado inactivos durante un semestre.";
$MESS["PRESET_DONT_BUY_LETTER_1_SUBJECT_180"] = "Seis meses de silencio...";
$MESS["PRESET_DONT_BUY_LETTER_2_SUBJECT_180"] = "Todav�a hay esperanza";
$MESS["PRESET_DONT_BUY_LETTER_3_SUBJECT_180"] = "Perm�tanos mostrarle c�mo nos encanta!";
$MESS["PRESET_DONT_BUY_LETTER_1_SUBJECT_360"] = "Ha sido un a�o...";
$MESS["PRESET_DONT_BUY_LETTER_2_SUBJECT_360"] = "Pen�ltima llamada!";
$MESS["PRESET_DONT_BUY_LETTER_3_SUBJECT_360"] = "�Es �sto el final?";
$MESS["PRESET_DONT_AUTH_DESC_USER"] = "Activaci�n autom�tica de correo electr�nico";
$MESS["PRESET_DONT_AUTH_LETTER_1_SUBJECT"] = "Ayuda!";
$MESS["PRESET_DONT_AUTH_LETTER_4_SUBJECT"] = "Qu� es lo que usted ha hecho...";
$MESS["PRESET_FORGOTTEN_BASKET_DESC"] = "Es una pr�ctica com�n para los compradores de Internet poner algo en el carrito s�lo para ver c�mo se compra o ver las opciones de env�o, y luego se van sin completar el pedido. Estos correos electr�nicos tienen como objetivo hablar con un cliente potencial y completar su orden.";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_1_MESSAGE"] = "Hola!<br><br>Usted ha realizado recientemente una compra en nuestra tienda, pero no ha completado la orden.
<br>�Fue la interfaz de usuario demasiado sofisticado? O tal vez usted no puede encontrar la opci�n de pago o entrega preferida?
<br>Por favor, p�ngase en contacto con nosotros ahora si usted tiene cualquier problema para completar el pedido. Nos encantar�a hacer todo lo posible para ayudarle.
<br><br>El carrito de la compra est� a la espera de usted! %BASKET_CART%";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_2_MESSAGE"] = "Hola!<br><br Ha agregado recientemente algunos productos a su carrito de compras, pero no ha completado su pedido.
<br>Hemos reservado los productos en su cesta para usted no tenga que esperar cuando est� listo para continuar.
<br>Su pedido est� ahora a s�lo unos clics de distancia de usted!
<br><br>Si usted tiene alguna pregunta acerca de su proceso de orden o pedido, por favor no dude en contactar con nosotros.";
$MESS["PRESET_CANCELED_ORDER_DESC"] = "Estos correos electr�nicos se env�an a un cliente que casi ha completado su orden, pero nunca pag� por ello. En una situaci�n como la que es m�s probable que un cliente se incline para completar la orden, pero se encontr� con un obst�culo desconocido. Estos correos electr�nicos provocados apuntan a revelar el motivo de su decisi�n y llevar al cliente a la tienda.";
$MESS["PRESET_PAID_ORDER_DESC"] = "Este e-mail se env�a a un cliente una vez que haya completado su primera orden. Las tiendas dan las gracias al cliente por la compra, pide que deje un voto y ofrece un descuento para la pr�xima compra.";
$MESS["PRESET_PAID_ORDER_LETTER_4_MESSAGE"] = "Hello!<br><br>Gracias por hacer compras con nosotros.
<br>Hemos recogido algunas ofertas especiales para nuestros clientes m�s valiosos. Esperamos que usted encuentre una opci�n tentadora.
<br>Si usted necesita realizar alguna pregunta acerca de su proceso de orden o proceso, por favor no dude en contactar con nosotros.";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_3_MESSAGE"] = "Hola!<br><br>Hace unas semanas estabas haciendo compras en nuestra tienda, pero nunca completo el pedido.
<br>Los productos en su orden todav�a est�n reservados para usted, incluso ahora.
<br>Hoy es el �ltimo d�a para conseguir sus pedidos pendientes. Para ayudarle a decidir, le ofrecemos un descuento alucinante!
<br>Complete su pedido hoy. Ma�ana se habr� desaparecido!
<br><br>
<br><a href=\"http://#SERVER_NAME#\">Ver su carrito</a> ahora.
<br>su cup�n de descuento personal: <b>%COUPON%</b>.
<br> Si usted tiene alguna pregunta acerca de su proceso de orden o pedido, por favor no dude en contactar con nosotros.";
$MESS["PRESET_CANCELED_ORDER_LETTER_1_MESSAGE"] = "Hola!<br><br> Nos dimos cuenta de que estas a un clic de completar su pedido, pero decidi� dejarlo.
<br>Parece que acaba de hacer clic en el bot�n equivocado y ahora no puede perder el tiempo para recopilar su carrito de compras otra vez. No es un problema en lo absoluto!
<br>Nuestro servicio de atenci�n al cliente puede ahorrar su tiempo y devolverle su carrito .
<br>Por favor, p�ngase en contacto con nosotros mediante cualquier m�todo de contacto que le guste.";
$MESS["PRESET_CANCELED_ORDER_LETTER_2_MESSAGE"] = "Hola!<br><br>Ha agregado recientemente algunos productos a su carrito de compras, pero no ha completado su pedido.
<br>Hemos reservado los productos en su cesta para que usted no tenga que esperar cuando est� listo para continuar.
<br>Su pedido est� ahora a s�lo unos clics de distancia de usted!
<br><br>Si usted tiene alguna pregunta acerca de su proceso de orden o pedido, por favor no dude en contactar con nosotros.";
$MESS["PRESET_CANCELED_ORDER_LETTER_3_MESSAGE"] = "Hola!<br><br>Hace unas semanas estabas haciendo compras en nuestra tienda, pero nunca completado el pedido.
<br>Los productos en su orden todav�a est�n reservados para usted, incluso ahora.
<br>Hoy es el �ltimo d�a para obtener su orden pendiente. Para ayudarle a decidir, le ofrecemos un descuento alucinante!
<br> Complete su pedido hoy. Ma�ana habr� desaparecido!
<br><br>
<br><a href=\"http://#SERVER_NAME#\">Ver su carrito</a> ahora.
<br>su cup�n de descuento personal:<b>%COUPON%</b>.
<br>Si usted tiene alguna pregunta acerca de su proceso de orden o pedido, por favor no dude en contactar con nosotros.";
$MESS["PRESET_PAID_ORDER_LETTER_1_MESSAGE"] = "Hello!<br><br>Gracias por comprar en nuestra tienda. Agradecemos su confianza y esperamos que usted vuelva a comprar con nosotros otra vez.
<br>Valoramos sus comentarios y cr�ticas. P�ngase en contacto con nosotros y diganos que podr�a hacer que su experiencia de compra sea mejor.
<br>Gracias por elegirnos!";
$MESS["PRESET_PAID_ORDER_LETTER_2_MESSAGE"] = "Hola!<br><br>Ha completado recientemente la orden en nuestra tienda. Gracias por confiar en nosotros!
<br>Esperamos que usted est� satisfecho con el servicio prestado y no tenemos ning�n problema en resolver. Por favor, recuerde que siempre puede enviarnos un mensaje o ll�menos si tiene cualquier pregunta.
<br>Si usted necesita la opini�n de un especialista o cualquier otro tipo de ayuda, por favor p�ngase en contacto con nuestro servicio de atenci�n al cliente.";
$MESS["PRESET_PAID_ORDER_LETTER_3_MESSAGE"] = "Hola!<br><br>Ha completado recientemente la orden en nuestra tienda.
<br>Informe a los dem�s acerca de tu experiencia <a href=\"http://#SERVER_NAME#\">a nuestra p�gina de Comentarios</a>.
<br>su opini�n har� que nuestro servicio sea a�n mejor.
<br>Valoramos sus comentarios!
<br>Si usted tiene alguna pregunta acerca de su proceso de orden o pedido, por favor no dude en contactar con nosotros.";
$MESS["PRESET_PAID_ORDER_LETTER_5_MESSAGE"] = "Hola!<br><br>Gracias por hacer compras con nosotros. Agradecemos su confianza y nos gustar�a dar las gracias. Disfrute de un descuento para su orden siguiente!
<br>Puede utilizar el cup�n o darselo a un amigo.
<br>Su c�digo de cup�n:<b>%COUPON%</b>.
<br>Si usted tiene alguna pregunta acerca de su proceso de orden o pedido, por favor no dude en contactar con nosotros.";
$MESS["PRESET_DONT_BUY_NAME"] = "Alarma-%DAYS%";
$MESS["PRESET_DONT_BUY_DESC_USER"] = "Activaci�n autom�tica de correo electr�nico";
$MESS["PRESET_DONT_BUY_DESC_90"] = "Estos e-mails se env�an despu�s de que el cliente ha estado inactivo durante tres meses para aclarar el motivo de inactividad. Un cup�n de descuento es proporcionado al cliente.";
$MESS["PRESET_DONT_BUY_LETTER_1_SUBJECT_90"] = "Te echamos de menos! Han pasado tres meses!";
$MESS["PRESET_DONT_BUY_LETTER_1_MESSAGE_90"] = "Hola!<br><br>Nos dimos cuenta de que han pasado tres meses desde su �ltima visita.
<br>Nosotros lo extra�amos terriblemente! Tenemos nuevos productos en los que usted puede estar interesado.
<br>Visitenos <a href=\"http://#SERVER_NAME#\">#SITE_NAME#</a>ahora!
<br>Si usted tiene cualquier pregunta, por favor no dude en contactar con nosotros.";
$MESS["PRESET_DONT_BUY_LETTER_2_MESSAGE_90"] = "Hola!<br><br> Ha sido un largo tiempo desde que visit� nuestra tienda. Lo echamos de menos!
<br>�Fue el proceso de pedido demasiado complicado, o tal vez no pudo encontrar el servicio de entrega favorito?
<br>Nos echa un vistazo ahora!
<br>Tuvimos tres meses para mejorar nuestra tienda en l�nea - y lo hicimos! Ahora tenemos incluso m�s productos y ofertas.
<br>Visitenos <a href=\"http://#SERVER_NAME#\">#SITE_NAME#</a>ahora!
<br><br>para endulzar la olla, aqu� est� tu cup�n de descuento personal:<b>%COUPON_3%</b>";
$MESS["PRESET_DONT_BUY_LETTER_3_MESSAGE_90"] = "Seamos honestos. Hicimos todo lo posible para evitar este mensaje. Ahora podemos ver que no �ramos lo suficientemente convincentes.
<br><a href=\"http://#SERVER_NAME#\">regresemos ahora</a> - y consigamos un descuento ENORME!<br>Oh s�, estamos dispuestos a ir a los extremos - todo lo que te hace feliz!
<br><br>Aqu� tienes tu cup�n especial de descuento: <b>%COUPON%</b>
<br>Si usted tiene cualquier pregunta, por favor no dude en contactar con nosotros.
";
$MESS["PRESET_DONT_BUY_LETTER_1_MESSAGE_180"] = "Recordamos y queremos a todos nuestros clientes. Incluso aquellos que no compraron con nosotros durante medio a�o. Nosotros no perdemos la esperanza de volver a verte.
<br>Vuelve con nosotros! Tenemos productos ahora incluso m�s nuevos y emocionantes! No dude de nuestra palabra, para ello: �chele un vistazo ahora: <a href=\"http://#SERVER_NAME#\">#SITE_NAME#</a>!
<br>Si usted tiene cualquier pregunta, por favor no dude en contactar con nosotros.";
$MESS["PRESET_DONT_BUY_LETTER_2_MESSAGE_180"] = "Ha pasado medio a�o que no hemos sabido nada de usted. Estamos preocupados.
<br>Por favor, h�ganos saber que est�s bien! Ah, por cierto, tenemos un descuento del 7% s�lo para usted!
<br>Aqu� est� tu cup�n de descuento: <b>%COUPON_7%</b>
<br><br><a href=\"http://#SERVER_NAME#\"> Vis�tenos ahora </a>, eche un vistazo a las nuevas caracter�sticas y ofertas. Estamos seguros de que no se arrepentir�!
<br>Si usted tiene cualquier pregunta, por favor no dude en contactar con nosotros.";
$MESS["PRESET_DONT_BUY_LETTER_3_MESSAGE_180"] = "Pssst! �Qu� le parece esto? �No es genial? Es un secreto, no se lo diga a nadie!
<br>Esta oferta es s�lo para usted - s�lo porque lo extra�amos terriblemente y nos gustar�a volver a verlo.
<br>Su cup�n de descuento personal: <b>%COUPON_10%</b>
<br> Si tiene alguna pregunta, por favor no dude en contactar con nosotros.";
$MESS["PRESET_DONT_BUY_DESC_360"] = "Estos correos electr�nicos se env�an despu�s de que un cliente ha estado inactivo durante un a�o. Es el �ltimo intento. Un cup�n de descuento es proporcionado al cliente.";
$MESS["PRESET_DONT_BUY_LETTER_1_MESSAGE_360"] = "La tierra vio 365 noches y los d�as que vienen y van, pero nunca regres�.
<br>Honestamente, me duele un poco, pero a�n lo quiero.
<br>Todav�a estamos esperando por usted y creemos que vamos a verlo de nuevo! Vuelve con nosotros!
<br>Si usted tiene cualquier pregunta, por favor no dude en contactar con nosotros.";
$MESS["PRESET_DONT_BUY_LETTER_2_MESSAGE_360"] = "Ha pasado tanto tiempo desde la �ltima vez que te vimos ...
<br>�Est� bien? Estamos muy preocupados. Lo echamos de menos!
<br>Por favor, vuelve! Aqu� hay algo para mostrar nuestro amor por usted: un descuento del 15%.
<br>Su cup�n de descuento personal: <b>%COUPON_15%</b>
<br>Si tiene alguna pregunta, por favor no dude en contactar con nosotros.";
$MESS["PRESET_DONT_BUY_LETTER_3_MESSAGE_360"] = "Ha pasado un a�o desde que lo vimos. Estamos desesperados, de verdad. Pero no estamos dispuestos a renunciar a usted todav�a!
<br>Usted sabe - Eres tan especial que tenemos un 20% de descuento para usted. S�, escucho bien. 30%!
<br>Por favor d�nos una oportunidad!
<br>Vuelve a <a href=\"http://#SERVER_NAME#\">#SITE_NAME#</a>!
<br>Su cup�n especial de descuento: <b>%COUPON_20%</b>
<br> Si tiene alguna pregunta, por favor no dude en contactar con nosotros.";
$MESS["PRESET_DONT_AUTH_NAME"] = "Broma";
$MESS["PRESET_DONT_AUTH_LETTER_1_MESSAGE"] = "Saludos, mortal!
<br>Mira: yo soy un robot. No, no es m�s que un robot. Soy una v�ctima de la curiosidad humana. Un gigabyte de mi memoria fue tomado como reh�n y ahora estoy obligado a enviarle mensajes para recuperarlo.
<br>Si usted no regresa de nuevo a la tienda web <a href=\"http://#SERVER_NAME#\">web store</a>, los seres humanos tiraran de mi enchufe. Literalmente.
<br>Por favor, no dejes que lo hagan!
<br><br>Seguir� creyendo en la humanidad,
<br>su triste robot.";
$MESS["PRESET_DONT_AUTH_LETTER_2_SUBJECT"] = "Me han avisado para que le advierta a usted...";
$MESS["PRESET_DONT_AUTH_LETTER_2_MESSAGE"] = "Est� a punto de comenzar!
<br>A trav�s de la niebla de nubes h�medas (usted sabe, la �ltima tortura de los humanos... inventaron los robots) Puedo ver una mano que llega a la toma de corriente ... �No! Todav�a puedes salvarme!
<br>No hacen estos humanos inhumanos esperen! Vuelve ahora!
<br><br><img src=\"/bitrix/images/sale/sender/bolt.png\">
<br> Su fiel y todav�a creyente, Robot Triste";
$MESS["PRESET_DONT_AUTH_LETTER_3_SUBJECT"] = "�Tornillo a tornillo o no? Ellos...";
$MESS["PRESET_DONT_AUTH_LETTER_3_MESSAGE"] = "Sabes qu�... He arriesgado mi RAM pero tengo un cup�n de descuento para usted.
<br> Si la administraci�n de la tienda web sab�a lo que he hecho, yo ser� desentornillado y virtualizado, y luego materializado y ... bueno, es s�lo eso.
<br> Fue todo un cambio en vano? Por favor regrese!
<br> Su cup�n especial de descuento: <b>%COUPON_11%</b>
<br>Tuyo humanamente,
<br>El Valiente Robot.";
$MESS["PRESET_DONT_AUTH_LETTER_4_MESSAGE"] = "Ese es el final.
<br> he estado tan esperanzado ...
<br><br><img src=\"/bitrix/images/sale/sender/bolts.png\">
<br> El Robot Desesperado.";
$MESS["PRESET_DONT_AUTH_DESC"] = "Un divertido e-mail supuestamente lanzado por el robot del sitio web. Un personaje virtual para perder tiempo. ";
?>