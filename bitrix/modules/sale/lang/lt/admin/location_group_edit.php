<?
$MESS["SALE_EDIT_RECORD"] = "Redaguoti lokacij� grup� #ID#";
$MESS["SALE_NEW_RECORD"] = "Nauja lokacij� grup�";
$MESS["SALE_RECORDS_LIST"] = "Lokacij� grup�s";
$MESS["SALE_SAVE"] = "I�saugoti";
$MESS["SALE_ADD"] = "Prid�ti";
$MESS["SALE_APPLY"] = "Pritaikyti";
$MESS["SALE_RESET"] = "Atstatyti";
$MESS["ERROR_EMPTY_LOCATION"] = "Nurodykite bent vien� lokacij�.";
$MESS["ERROR_EMPTY_NAME"] = "Grup�s pavadinimas nenurodytas kalboje";
$MESS["ERROR_EDIT_GROUP"] = "Klaida kei�iant lokacij� grup�.";
$MESS["ERROR_ADD_GROUP"] = "Klaida pridedant lokacij� grup�.";
$MESS["SALE_NEW"] = "nauja";
$MESS["SALE_SORT"] = "R��iuoti indeks�";
$MESS["SALE_LOCATIONS"] = "Lokacijos grup�je";
$MESS["SALE_NAME"] = "Pavadinimas";
$MESS["SALE_PT_PROPS"] = "Parametrai";
$MESS["SLGEN_2FLIST"] = "Lokacij� grup�s s�ra�as";
$MESS["SLGEN_NEW_LGROUP"] = "Prid�ti nauj� lokacij� grup�";
$MESS["SLGEN_DELETE_LGROUP"] = "Pa�alinti lokacij� grup�";
$MESS["SLGEN_DELETE_LGROUP_CONFIRM"] = "Ar tkrai norite pa�alinti �i� lokacij� grup�?";
$MESS["SLGEN_TAB_LGROUP"] = "Lokacij� grup�";
$MESS["SLGEN_TAB_LGROUP_DESCR"] = "Lokacij� grup�";
?>