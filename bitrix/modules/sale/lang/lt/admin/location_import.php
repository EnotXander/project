<?
$MESS["NO_LOC_FILE"] = "Lokacijos duomen� failas n�ra �keltas";
$MESS["ERROR_LOC_FORMAT"] = "Neteisingas lokacijos failo formatas: pagrindin� kalba n�ra nustatyta";
$MESS["NO_MAIN_LANG"] = "Nenustatyta pagrindin� kalba i� lokacijos failo svetain�je";
$MESS["OMLOADED1"] = "�kelta";
$MESS["OMLOADED2"] = "�alys";
$MESS["OMLOADED3"] = "miestai";
$MESS["OMLOADED4"] = "Sukurtos lokacijos";
$MESS["LOCA_LOADING"] = "Lokacij� importo vedlys";
$MESS["LOCA_LOADING_OLD"] = "�kelti lokacijas i� failo";
$MESS["LOCA_FILE"] = "Failas";
$MESS["LOCA_DEL_OLD"] = "I�valyti svetain�s lokacijas prie� �keliant";
$MESS["LOCA_DO_LOAD"] = "�kelti";
$MESS["location_admin_import"] = "Importuoti lokacijas";
$MESS["location_admin_import_tab"] = "Importuoti ";
$MESS["location_admin_import_tab_old"] = "Importuoti i� failo";
$MESS["LOCA_LOADING_WIZARD"] = "Paleisti lokacij� importo vedl�";
$MESS["LOCA_SAVE"] = "�kelti";
$MESS["LOCA_HINT"] = "J�s galite parsisi�sti lokacij� fail� i� Bitrix svetain�s:<ul style=\"font-size: 100%\">
	<li><a href=\"http://www.1c-bitrix.ru/download/files/locations/loc_cntr.csv\">Pasaulio �alys</a></li>
	<li><a href=\"http://www.1c-bitrix.ru/download/files/locations/loc_usa.csv\">USA</a></li>
</ul>";
$MESS["LOCA_LOCATIONS_STATS"] = "Lokacij� statistika";
$MESS["LOCA_LOCATIONS_COUNTRY_STATS"] = "�alys";
$MESS["LOCA_LOCATIONS_CITY_STATS"] = "miestai";
$MESS["LOCA_LOCATIONS_LOC_STATS"] = "i� viso lokacij�";
$MESS["LOCA_LOCATIONS_GROUP_STATS"] = "lokacij� grup�s";
$MESS["LOCA_LOCATIONS_REGION_STATS"] = "regionai";
$MESS["LOCATION_CLEAR"] = "I�valyti";
$MESS["LOCATION_CLEAR_DESC"] = "Pa�alina visas lokacijas";
$MESS["LOCATION_CLEAR_BTN"] = "I�valyti";
$MESS["LOCATION_CLEAR_OK"] = "Lokacijos pa�alintos.";
$MESS["LOCATION_CLEAR_CONFIRM"] = "Ar tikrai norite pa�alinti visas lokacijas?";
?>