<?
$MESS["SPOL_CUR_ORDERS"] = "Ver �rdenes actuales";
$MESS["SPOL_ORDERS_HISTORY"] = "Ver historial de �rdenes";
$MESS["SPOL_FROM"] = "desde";
$MESS["SPOL_NO_ORDERS"] = "No se encontraron �rdenes.";
$MESS["SPOL_ORDER"] = "Orden";
$MESS["SPOL_ORDER_DETAIL"] = "Detalles de la orden";
$MESS["SPOL_PAY_SUM"] = "Total de la orden";
$MESS["SPOL_CANCELLED"] = "Anulado";
$MESS["SPOL_PAYSYSTEM"] = ";�todo de pago";
$MESS["SPOL_DELIVERY"] = "Entrega";
$MESS["SPOL_BASKET"] = "Contenido de la orden";
$MESS["SPOL_CANCEL_ORDER"] = "Anular orden";
$MESS["SPOL_REPEAT_ORDER"] = "Volver a ordenar";
$MESS["SPOL_PAYED"] = "Pagado";
$MESS["SPOL_SHT"] = "pcs.";
$MESS["SPOL_STATUS"] = "�rdenes en el estatus";
$MESS["SPOL_ORDERS_ALL"] = "Ver todas las �rdenes";
$MESS["SPOL_YES"] = "Si";
$MESS["SPOL_NO"] = "No";
$MESS["SPOL_NUM_SIGN"] = "N�";
?>