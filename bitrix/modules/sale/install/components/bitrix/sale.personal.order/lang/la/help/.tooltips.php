<?
$MESS["SEF_MODE_TIP"] = "Compruebe esta opci�n para habilitar el modo SEF y los campos de configuraci�n del URL.";
$MESS["ORDERS_PER_PAGE_TIP"] = "Especificar el n�mero de �rdenes por p�gina. Otras �rdenes est�n disponibles a trav�s del breadcrumb de navegaci�n.";
$MESS["PATH_TO_PAYMENT_TIP"] = "Usted puede crear esta p�gina usando el componente del Sistema de Integraci�n de Pago<b>Payment System Integration</b>. Los clientes abren esta opci�n haciendo click en <b>Repetir pago</b> el la p�gina de detalles del pedido.";
$MESS["PATH_TO_BASKET_TIP"] = "Usted puede crear esta p�gina usando el componente <b>Carrito de compras</b>. Los clientes pueden abrir esta opci�nhaciendo click en  <b>Repeat</b> en la p�gina de lista de pedidos.";
$MESS["SET_TITLE_TIP"] = "Comprobando esta opci�n se fijar� el t�tulo de la p�gina a <b>Mis Pedidos</b>.";
$MESS["SEF_FOLDER_TIP"] = "Especifica la carpeta de trabajo del componente. La carpeta puede ser representada por un archivo real de la ruta del sistema, o puede ser virtual.";
$MESS["SEF_URL_TEMPLATES_list_TIP"] = "Especificar el segmento de la URL de la p�gina de pedidos del cliente. Por ejemplo: <b>lista/</b>";
$MESS["SEF_URL_TEMPLATES_detail_TIP"] = "Especifica el segmento del URL de la p�gina del detalle del pedido. Por ejemplo: <b>detalle/#ID#/</b>";
$MESS["SEF_URL_TEMPLATES_cancel_TIP"] = "Especifica el segmento del URL de la p�gina de cancelaci�n del pedido. Por ejemplo: <b>Cancelar/#ID#/</b>";
$MESS["HISTORIC_STATUSES_TIP"] = "Estados que se pueden utilizar para filtrar el historial. Si el par�metro \"filter_history\" no es Y sino una orden est� en uno de estos estados, ser� rechazada y no se muestra en los resultados de b�squeda.";
?>