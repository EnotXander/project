<?
$MESS["SPCD1_AFFILIATES"] = "Nariai";
$MESS["SPCD1_REPORT"] = "Nari� ataskaita";
$MESS["SPCD1_MONEYS"] = "Nario sandoriai";
$MESS["SPCD1_REGISTER"] = "Registracijos puslapis";
$MESS["SPCD1_PROGR"] = "Programos ataskaita";
$MESS["SPCD1_PLANS"] = "Nari� planai";
$MESS["SPCD1_PLANS_ALT"] = "Turimi nari� planai";
$MESS["SPCD1_REGISTER_AFF"] = "Nario registracija";
$MESS["SPCD1_REGISTER_PAGE"] = "Puslapis po registracijos";
$MESS["SPCD1_TECH"] = "Instrukcijos";
$MESS["SPCD1_TECH_ALT"] = "Technin�s instrukcijos";
$MESS["SPCD1_SHOP_NAME"] = "Svetain�s pavadinimas (palikite tu��i�, jei norite naudoti pavadinim� i� svetain�s nustatym�)";
$MESS["SPCD1_SHOP_URL"] = "Svetain�s URL (be http://; palikite tu��i�, jei norite naudoti URL i� svetain�s nustatym�)";
$MESS["SPCD1_AFF_REG_PAGE"] = "Mar�rutas � nario registracijos puslap�";
?>