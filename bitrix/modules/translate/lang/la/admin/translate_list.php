<?
$MESS["TRANS_TITLE"] = "Interfaz de Traducciones";
$MESS["TRANS_FILE_NAME"] = "Nombre del Archivo/Carpeta";
$MESS["TRANS_BACK"] = "Atr�s";
$MESS["TRANS_TOTAL_MESSAGES"] = "Total de mensajes";
$MESS["TRANS_NOT_TRANSLATED"] = "Sin traducir";
$MESS["TRANS_TOTAL"] = "Total";
$MESS["TRANS_PATH"] = "Ruta:";
$MESS["TRANS_GO"] = "Ir";
$MESS["TR_UP_TITLE"] = "Click para ir al nivel superior";
$MESS["TR_FOLDER_TITLE"] = "Click para ir a la carpeta";
$MESS["TR_FILE_TITLE"] = "Click para editar el archivo";
$MESS["TR_DOWNLOAD_CSV_TEXT"] = "Exportar a CVS";
$MESS["TR_DOWNLOAD_CSV_TITLE"] = "Exportar mensajes a un archivo CSV";
$MESS["TR_UPLOAD_CSV_FILE"] = "Archivo CSV:";
$MESS["TR_UPLOAD_SUBMIT_BUTTON"] = "Importar";
$MESS["TR_REWRITE_LANG_FILES"] = "Reemplazar completamente los archivos de lenguajes";
$MESS["TR_CSV_UPLOAD_OK"] = "El archivo fue importado satisfactoriamente.";
$MESS["TR_SHOW_DIFF_TEXT"] = "Mostrar diferencias";
$MESS["TR_SHOW_DIFF_TITLE"] = "Mostrar diferencias e archivos de lenguaje";
$MESS["TR_NO_SHOW_DIFF_TEXT"] = "Ocultar diferencias";
$MESS["TR_NO_SHOW_DIFF_TITLE"] = "Ocultar diferencias en archivos de lnguaje";
$MESS["TR_FILEUPLFORM_TAB1"] = "Importar";
$MESS["TR_UPLOAD_FILE"] = "Importar archivo CSV";
$MESS["TR_NO_REWRITE_LANG_FILES"] = "importar s�lo nuevos mensajes";
$MESS["TR_FILE_ACTIONS"] = "Acci�n de archivo:";
$MESS["TR_DOWNLOAD_SUBMIT_BUTTON"] = "Exportar";
$MESS["TR_CHECK_FILES_TEXT"] = "Marcar Archivos";
$MESS["TR_CHECK_FILES_TITLE"] = "Marcar para cadenas redundantes al final de los archivos";
$MESS["TR_FILEDOWNFORM_TAB2"] = "Exportar archivos";
$MESS["TR_DOWNLOAD_LANG"] = "Exportar todas las cadenas";
$MESS["TR_DOWNLOAD_NO_TRANSLATE"] = "Exportar las Cadenas sin Traducir";
$MESS["TR_MESSAGE_EDIT"] = "Editar Cadenas";
$MESS["TR_FILE_EDIT"] = "Editar C�digo PHP";
$MESS["TR_FILE_SHOW"] = "Ver C�digo PHP";
$MESS["TR_PATH_GO"] = "Abrir Carpeta";
$MESS["TR_NEW_SEARCH"] = "Nueva b�squeda";
$MESS["TR_SEARCH"] = "B�squeda";
$MESS["TR_SEARCH_TITLE"] = "B�squeda";
$MESS["CONFRIM_REWRITE_LANG_FILES"] = "Atenci�n! La secuencia de caracteres que no se encuentran en el archivo importado se eliminar�n. �Est� seguro de que desea reemplazar el contenido de los archivos de localizaci�n completamente?";
?>