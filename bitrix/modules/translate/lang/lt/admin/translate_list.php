<?
$MESS['TRANS_TITLE'] = 'Kalbini� fail� vertimas';
$MESS['TRANS_FILE_NAME'] = 'Failo/katalogo pavadinimas';
$MESS['TRANS_BACK'] = 'Gr��ti';
$MESS['TRANS_TOTAL_MESSAGES'] = 'Viso prane�im�';
$MESS['TRANS_NOT_TRANSLATED'] = 'Nei�versta';
$MESS['TRANS_TOTAL'] = 'Viso';
$MESS['TRANS_PATH'] = 'Kelias:';
$MESS['TRANS_GO'] = 'Pereiti';
$MESS['TR_UP_TITLE'] = 'Pereiti � auk�tesn� lyg�';
$MESS['TR_FOLDER_TITLE'] = 'Pereiti � pasirinkt� katalog�';
$MESS['TR_FILE_TITLE'] = 'Redaguoti fail�';
$MESS['TR_DOWNLOAD_CSV_TEXT'] = 'eksportuoti � CSV';
$MESS['TR_DOWNLOAD_CSV_TITLE'] = 'eksportuoti � CSV fail�';
$MESS['TR_UPLOAD_CSV_FILE'] = 'CSV failas:';
$MESS['TR_UPLOAD_SUBMIT_BUTTON'] = 'Importuoti';
$MESS['TR_REWRITE_LANG_FILES'] = 'visi�kai pakeisti kalbinius failus';
$MESS['TR_CSV_UPLOAD_OK'] = 'Failas s�kmingai �keltas';
$MESS['TR_SHOW_DIFF_TEXT'] = 'Atvaizduoti skirtumus';
$MESS['TR_SHOW_DIFF_TITLE'] = 'Atvaizduoti kalbini� fail� skirtumus';
$MESS['TR_NO_SHOW_DIFF_TEXT'] = 'Neatvaizduoti skirtum�';
$MESS['TR_NO_SHOW_DIFF_TITLE'] = 'Neatvaizduoti kalbini� fail� skirtumus';
$MESS['TR_FILEUPLFORM_TAB1'] = 'Failo parsisiuntimas';
$MESS['TR_UPLOAD_FILE'] = 'Importuoti CSV fail�';
$MESS['TR_NO_REWRITE_LANG_FILES'] = '�kelti tik naujus vertimus';
$MESS['TR_FILE_ACTIONS'] = 'Fail� apdorojimas:';
?>