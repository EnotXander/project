<?
$MESS['BPE_BLOG_URL'] = 'Tinklara��io URL adresas';
$MESS['BPE_ID'] = 'Prane�imo ID';
$MESS['BPE_PATH_TO_BLOG'] = 'Kelio �ablonas � tinklara��io puslap�';
$MESS['BPE_PATH_TO_POST'] = 'Kelio �ablonas � tinklara��io prane�imo puslap�';
$MESS['BPE_PATH_TO_USER'] = 'Kelio �ablonas � tinklara��io naudotojo puslap�';
$MESS['BPE_PATH_TO_POST_EDIT'] = 'Kelio � tinklara��io prane�imo redagavimo puslap� �ablonas.';
$MESS['BH_PATH_TO_DRAFT'] = 'Kelio � nebaigt� ra�yti prane�im� puslap� �ablonas.';
$MESS['BPE_BLOG_VAR'] = 'Tinklara��io identifikavimo kintamasis';
$MESS['BPE_POST_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io prane�imo identifikatorius';
$MESS['BPE_USER_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io naudotojo identifikatorius';
$MESS['BPE_PAGE_VAR'] = 'Puslapio kintamojo pavadinimas';
$MESS['POST_PROPERTY'] = 'Atvaizduoti prane�imo papildomas savybes';
$MESS['BB_PATH_TO_SMILE'] = 'Kelias � aplank� su �ypsen�l�mis, svetain�s �akninio aplanko at�vilgiu';
$MESS['B_VARIABLE_ALIASES'] = 'Kintam�j� vardai';
$MESS['BC_DATE_TIME_FORMAT'] = 'Datos ir laiko atvaizdavimo formatas';
?>