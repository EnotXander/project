<?
$MESS['BP_BLOG_URL'] = 'Tinklara��io URL adresas';
$MESS['BP_PATH_TO_BLOG'] = 'Kelio �ablonas � tinklara��io puslap�';
$MESS['BP_PATH_TO_BLOG_CATEGORY'] = 'Kelio �ablonas � tinklara��io puslap� su filtru pagal �ym�';
$MESS['BP_PATH_TO_POST_EDIT'] = 'Kelio � tinklara��io prane�imo redagavimo puslap� �ablonas.';
$MESS['BP_PATH_TO_USER'] = 'Kelio �ablonas � tinklara��io naudotojo puslap�';
$MESS['BP_BLOG_VAR'] = 'Tinklara��io identifikavimo kintamasis';
$MESS['BP_POST_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io prane�imo identifikatorius';
$MESS['BP_USER_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io naudotojo identifikatorius';
$MESS['BP_PAGE_VAR'] = 'Puslapio kintamojo pavadinimas';
$MESS['BP_SET_NAV_CHAIN'] = 'Prid�ti punkt� � navigacijos grandin�';
$MESS['BP_ID'] = 'Prane�imo ID';
$MESS['POST_PROPERTY'] = 'Atvaizduoti prane�imo papildomas savybes';
$MESS['BB_PATH_TO_SMILE'] = 'Kelias � aplank� su �ypsen�l�mis, svetain�s �akninio aplanko at�vilgiu';
$MESS['B_VARIABLE_ALIASES'] = 'Kintam�j� vardai';
$MESS['BC_DATE_TIME_FORMAT'] = 'Datos ir laiko atvaizdavimo formatas';
?>