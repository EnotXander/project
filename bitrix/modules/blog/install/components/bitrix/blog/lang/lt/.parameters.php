<?
$MESS['BC_BLOG_VAR'] = 'Tinklara��io identifikavimo kintamasis';
$MESS['BC_POST_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io prane�imo identifikatorius';
$MESS['BC_USER_VAR'] = 'Kintamojo pavadinimas kuriame saugomas tinklara��io naudotojo identifikatorius';
$MESS['BC_PAGE_VAR'] = 'Puslapio kintamojo pavadinimas';
$MESS['BC_GROUP_VAR'] = 'Tinklara��io grup�s identifikatoriaus kintamojo pavadinimas';
$MESS['BC_CACHE_TIME_LONG'] = 'Likusi� puslapi� ke�avimo laikas';
$MESS['BC_SET_NAV_CHAIN'] = 'Prid�ti tinklara�t� � navigacijos grandin�';
$MESS['BC_MESSAGE_COUNT'] = 'Prane�im� skai�ius viename puslapyje';
$MESS['BC_BLOG_COUNT'] = 'Tinklara��i� skai�ius viename puslapyje';
$MESS['BC_PATH_TO_SMILE'] = 'Kelias � aplank� su �ypsen�l�mis, svetain�s �akninio aplanko at�vilgiu';
$MESS['BC_SEF_PATH_INDEX'] = 'Pagrindinis tinklara��i� puslapis';
$MESS['BC_SEF_PATH_GROUP'] = 'Grupi� tinklara��iai';
$MESS['BC_SEF_PATH_BLOG'] = 'Tinklara�tis';
$MESS['BC_SEF_PATH_USER'] = 'Naudotojo anketa';
$MESS['BC_SEF_PATH_USER_FRIENDS'] = 'Naudotojo draug� puslapis';
$MESS['BC_SEF_PATH_SEARCH'] = 'Paie�kos tinklara��iuose puslapis';
$MESS['BC_SEF_PATH_USER_SETTINGS'] = 'Naudotoj� nustatymai';
$MESS['BC_SEF_PATH_USER_SETTINGS_EDIT'] = 'Naudotoj� prieigos teisi� nustatymas';
$MESS['BC_SEF_PATH_GROUP_EDIT'] = 'Tinklara��io naudotoj� grupi� nustatymas';
$MESS['BC_SEF_PATH_BLOG_EDIT'] = 'Tinklara��io redagavimas';
$MESS['BC_SEF_PATH_CATEGORY_EDIT'] = 'Tinklara��io �ymi� nustatymas';
$MESS['BC_SEF_PATH_POST_EDIT'] = 'Prane�im� redagavimas';
$MESS['BC_SEF_PATH_DRAFT'] = 'Nebaigti prane�imai';
$MESS['BC_SEF_PATH_TRACKBACK'] = 'Prane�imo Trackback adresas';
$MESS['BC_SEF_PATH_POST'] = 'Prane�imas i�samiai';
$MESS['BC_SEF_PATH_RSS'] = 'Tinklara��io adresas RSS formate';
$MESS['BC_SEF_PATH_RSS_ALL'] = 'Vis� tinklara��i� prane�imai RSS formate';
$MESS['BC_MESSAGE_COUNT_MAIN'] = 'Pagrindiniame tinklara��io puslapyje i�vedam� prane�im� skai�ius';
$MESS['BC_BLOG_COUNT_MAIN'] = 'Pagrindiniame tinklara��io puslapyje i�vedam� tinklara��i� skai�ius';
$MESS['BC_COMMENTS_COUNT'] = 'Komentar� skai�ius puslapyje';
$MESS['BC_MESSAGE_LENTH'] = 'Pagrindiniame tinklara��io puslapyje i�vedam� prane�im� ilgis (simboliais)';
$MESS['USER_PROPERTY'] = 'Atvaizduoti papildomas savybes anketoje';
$MESS['BLOG_PROPERTY'] = 'Atvaizduoti savybes tinklara�tyje';
$MESS['BLOG_PROPERTY_LIST'] = 'Atvaizduoti papildomas tinklara��io savybes informacijoje apie tinklara�t�';
$MESS['POST_PROPERTY'] = 'Atvaizduoti prane�imo papildomas savybes';
$MESS['POST_PROPERTY_LIST'] = 'Atvaizduoti prane�imo papildomas savybes tinklara�tyje';
$MESS['B_VARIABLE_ALIASES'] = 'Kintam�j� vardai';
$MESS['B_SEARCH_TAG_CLOUD'] = '�ymi� debesio nustatymai';
$MESS['BC_PERIOD_DAYS'] = 'Rodyti populiariausius prane�imus ir tinklara��ius (dien�) ';
$MESS['BC_DATE_TIME_FORMAT'] = 'Datos ir laiko atvaizdavimo formatas';
$MESS['BC_USE_ASC_PAGING'] = 'Komentarams naudoti tiesi� navigacijos grandin�';
$MESS['BB_NAV_TEMPLATE'] = 'Navigacijos grandin�s �ablono pavadinimas';
$MESS['BC_NOT_USE_COMMENT_TITLE'] = 'Komentaruose nenaudoti skelbimo pavadinimo';
?>