<?
$MESS["BLG_GP_EMPTY_TITLE"] = "Nenurodyta prane�imo antra�t�";
$MESS["BLG_GP_EMPTY_DETAIL_TEXT"] = "Prane�imo tekstas tu��ias.";
$MESS["BLG_GP_EMPTY_BLOG_ID"] = "Nenurodytas prane�imo tinklara�tis";
$MESS["BLG_GP_ERROR_NO_BLOG"] = "Tinklara�tis su \\\"#ID#\\\" kodu nerastas";
$MESS["BLG_GP_EMPTY_AUTHOR_ID"] = "Nenurodytas prane�imo autorius";
$MESS["BLG_GP_ERROR_NO_AUTHOR"] = "Prane�imo autoriaus ID nurodytas neteisingai";
$MESS["BLG_GP_ERROR_DATE_CREATE"] = "Prane�imo suk�rimo data nurodyta neteisingai";
$MESS["BLG_GP_ERROR_DATE_PUBLISH"] = "Prane�imo publikavimo data nurodyta neteisingai ";
$MESS["BLG_GP_ERROR_ATTACH_IMG"] = "�keltas neteisingas paveiksliukas";
$MESS["BLG_SONET_TITLE"] = "prid�jo prane�im� \"#TITLE#\" � blog�";
$MESS["BLG_GP_CODE_EXIST"] = "Blogo prane�imas su kodu <b>#CODE#</b> jau egzistuoja. Pakeiskite prane�imo adres�.";
$MESS["BLG_GP_RESERVED_CODE"] = "Prane�imas negali tur�ti kod� <b>#CODE#</b>. Pakeiskite prane�imo adres�.";
$MESS["BLG_GP_IM_1"] = "i�si�nt� jums prane�im� \"#title#\"";
$MESS["BLG_GP_IM_4"] = "pakomentavo \"#title#\"";
$MESS["BLG_GP_IM_5"] = "pakomentavo j�s� susira�in�jim� \"#title#\"";
$MESS["BLG_GP_IM_6"] = "pamin�jo jus susira�in�jime \"#title#\"";
$MESS["BLG_GP_IM_7"] = "pamin�jo jus komentaruose � susira�in�jim� \"#title#\"";
$MESS["SONET_IM_NEW_POST"] = "Naujas prane�imas #title# prid�tas grup�je #group_name#.";
$MESS["BLG_GP_IM_1_FEMALE"] = "adresavo prane�im� jums \"#title#\"";
$MESS["BLG_GP_IM_4_FEMALE"] = "pakomentavo \"#title#\"";
$MESS["BLG_GP_IM_5_FEMALE"] = "pakomentavo j�s� prane�im� \"#title#\"";
$MESS["BLG_GP_IM_6_FEMALE"] = "pamin�jo jus \"#title#\"";
$MESS["BLG_GP_IM_7_FEMALE"] = "pamin�jo jus \"#title#\" komentaruose";
$MESS["BLG_GP_IM_8"] = "pasidalino j�s� prane�imu \"#title#\" su kitais naudotojais";
$MESS["BLG_GP_IM_8_FEMALE"] = "pasidalino j�s� prane�imu \"#title#\" su kitais naudotojais";
?>