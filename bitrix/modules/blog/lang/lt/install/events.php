<?
$MESS['NEW_BLOG_MESSAGE_NAME'] = 'Naujas prane�imas tinklara�tyje';
$MESS['NEW_BLOG_MESSAGE_DESC'] = '#BLOG_ID# - tinklara��io ID 
#BLOG_NAME# - tinklara��io pavadinimas
#BLOG_URL# - tinklara��io URL
#MESSAGE_TITLE# - prane�imo antra�t�
#MESSAGE_TEXT# - prane�imo tekstas
#MESSAGE_DATE# - prane�imo data
#MESSAGE_PATH# - prane�imo URL adresas
#AUTHOR# - prane�imo autorius
#EMAIL_FROM# - siunt�jo el. pa�tas
#EMAIL_TO# - gav�jo el. pa�tas';
$MESS['NEW_BLOG_MESSAGE_SUBJECT'] = '#SITE_NAME#: [B] #BLOG_NAME# : #MESSAGE_TITLE#';
$MESS['NEW_BLOG_MESSAGE_MESSAGE'] = 'Svetain�s #SITE_NAME# informacinis prane�imas
------------------------------------------

Naujas prane�imas tinklara�tyje \"#BLOG_NAME#\"

Antra�t�:
#MESSAGE_TITLE#

Autorius: #AUTHOR#
Data: #MESSAGE_DATE#
Prane�imo tekstas:

#MESSAGE_TEXT#

Prane�imo adresas:
#MESSAGE_PATH#

Prane�imas sugeneruotas automati�kai.';
$MESS['NEW_BLOG_COMMENT_NAME'] = 'Naujas komentaras tinklara�tyje';
$MESS['NEW_BLOG_COMMENT_DESC'] = '#BLOG_ID# - tinklara��io ID 
#BLOG_NAME# - tinklara��io pavadinimas
#BLOG_URL# - tinklara��io URL
#MESSAGE_TITLE# - prane�imo antra�t�
#COMMENT_TITLE# - komentaro antra�t�
#COMMENT_TEXT# - komentaro tekstas
#COMMENT_DATE# - komentaro data
#COMMENT_PATH# - komentaro URL adresas
#AUTHOR# - autorius
#EMAIL_FROM# - siunt�jo el. pa�tas
#EMAIL_TO# - gav�jo el. pa�tas';
$MESS['NEW_BLOG_COMMENT_SUBJECT'] = '#SITE_NAME#: [B] #MESSAGE_TITLE# : #COMMENT_TITLE#';
$MESS['NEW_BLOG_COMMENT_MESSAGE'] = 'Svetain�s #SITE_NAME# informacinis prane�imas
------------------------------------------

Naujas komentaras tinklara�tyje \"#BLOG_NAME#\" prane�imui \"#MESSAGE_TITLE#\"

Antra�t�:
#COMMENT_TITLE#

Autorius: #AUTHOR#
Data: #COMMENT_DATE#
Komentaro tekstas:

#COMMENT_TEXT#

Adresas:
#COMMENT_PATH#

Prane�imas sugeneruotas automati�kai.';
$MESS['NEW_BLOG_COMMENT2COMMENT_NAME'] = 'Naujas komentaras J�s� komentarui tinklara�tyje';
$MESS['NEW_BLOG_COMMENT2COMMENT_DESC'] = '#BLOG_ID# - tinklara��io ID 
#BLOG_NAME# - tinklara��io pavadinimas
#BLOG_URL# - tinklara��io URL
#MESSAGE_TITLE# - Prane�imo antra�t�
#COMMENT_TITLE# - Komentaro antra�t�
#COMMENT_TEXT# - Komentaro tekstas
#COMMENT_DATE# - Komentaro data
#COMMENT_PATH# - URL adresas
#AUTHOR# - Komentaro autorius
#EMAIL_FROM# - Siunt�jo el. pa�tas
#EMAIL_TO# - Gav�jo el. pa�tas';
$MESS['NEW_BLOG_COMMENT2COMMENT_SUBJECT'] = '#SITE_NAME#: [B] #MESSAGE_TITLE# : #COMMENT_TITLE#';
$MESS['NEW_BLOG_COMMENT2COMMENT_MESSAGE'] = 'Svetain�s #SITE_NAME# informacinis prane�imas
------------------------------------------

Naujas komentaras J�s� komentarui tinklara�tyje \"#BLOG_NAME#\" prane�imui \"#MESSAGE_TITLE#\".

Antra�t�:
#COMMENT_TITLE#

Autorius: #AUTHOR#
Data: #COMMENT_DATE#
Komentaro tekstas:

#COMMENT_TEXT#

Adresas:
#COMMENT_PATH#

Prane�imas sugeneruotas automati�kai.';
?>