<?
$MESS['IBLOCK_PRICES'] = 'Kainos';
$MESS['IBLOCK_TYPE'] = 'Informacinio bloko tipas';
$MESS['IBLOCK_IBLOCK'] = 'Informacinis blokas';
$MESS['IBLOCK_PROPERTY'] = 'Savyb�s';
$MESS['IBLOCK_SORT_ASC'] = 'did�jan�ia';
$MESS['IBLOCK_SORT_DESC'] = 'ma��jan�ia';
$MESS['IBLOCK_PRICE_CODE'] = 'Kainos tipas';
$MESS['IBLOCK_FIELD'] = 'Laukai';
$MESS['IBLOCK_FILTER_NAME_OUT'] = 'Filtracijos rezultato masyvo pavadinimas';
$MESS['IBLOCK_LIST_HEIGHT'] = 'Daugelio pasirinkim� s�ra�o lauko auk�tis';
$MESS['IBLOCK_TEXT_WIDTH'] = 'Vienos teksto eilut�s �vedimo lauko plotis';
$MESS['IBLOCK_NUMBER_WIDTH'] = 'Skaitini� interval� �vedimo lauk� plotis';
$MESS['IBLOCK_SAVE_IN_SESSION'] = 'Saugoti filtro nustatymus naudotojo sesijoje';
?>