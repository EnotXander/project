<?
$MESS["BITRIXTV_GROUP_SETTINGS"] = "Configuraciones generales";
$MESS["BITRIXTV_GROUP_PREVIEW_TV"] = "Configuraciones del reproductor";
$MESS["BITRIXTV_SETTING_IBLOCK_TYPE"] = "Tipo de Block de Informaci�n";
$MESS["BITRIXTV_SETTING_IBLOCK_ID"] = "C�digo del Block de Informaci�n";
$MESS["BITRIXTV_SETTING_PATH_TO_FILE"] = "Propiedad con la ruta a los archivos de v�deo";
$MESS["BITRIXTV_SETTING_DURATION"] = "Propiedad con la duraci�n del archivo de video ";
$MESS["BITRIXTV_SETTING_PREVIEW_TV_ELEMENT"] = "Elemento";
$MESS["BITRIXTV_SETTING_PREVIEW_TV_SECTION"] = "Secci�n";
$MESS["BITRIXTV_SETTING_PREVIEW_WIDTH"] = "Ancho del reproductor";
$MESS["BITRIXTV_SETTING_PREVIEW_HEIGHT"] = "Alto del reproductor";
$MESS["BITRIXTV_GROUP_PREVIEW_TV_PLAYER"] = "Configuraci�n de la Lista de Reproducci�n";
$MESS["BITRIXTV_SETTING_DEFAULT_SMALL_IMAGE"] = "Imagen peque�a (por defecto)";
$MESS["BITRIXTV_SETTING_DEFAULT_BIG_IMAGE"] = "Imagen grande (por defecto)";
$MESS["BITRIXTV_SETTING_LOGO"] = "Logo";
$MESS["T_IBLOCK_DESC_ASC"] = "Ascendente";
$MESS["T_IBLOCK_DESC_DESC"] = "Descendente";
$MESS["T_IBLOCK_DESC_FID"] = "ID";
$MESS["T_IBLOCK_DESC_FNAME"] = "Nombre";
$MESS["T_IBLOCK_DESC_FACT"] = "Fecha de inicio";
$MESS["T_IBLOCK_DESC_FSORT"] = "Orden";
$MESS["T_IBLOCK_DESC_FTSAMP"] = "�ltima Modificaci�n";
$MESS["T_IBLOCK_DESC_IBORD1"] = "Primera modificaci�n";
$MESS["T_IBLOCK_DESC_IBBY1"] = "Primer orden";
$MESS["BITRIXTV_SETTING_ALLOW_SWF"] = "Permitir l areproducci�n de archivos SWF (no es recomendable)";
$MESS["BITRIXTV_SETTING_STAT"] = "Ver estad�sticas";
$MESS["BITRIXTV_SETTING_STAT_EVENT"] = "Seguimiento de eventos desde an�lisis web";
$MESS["BITRIXTV_SETTING_STAT_EVENT1"] = "event1";
$MESS["BITRIXTV_SETTING_STAT_EVENT2"] = "event2";
$MESS["BITRIXTV_SETTING_SHOW_COUNTER_EVENT"] = "Incrementar contador de vistas del elemento";
$MESS["CP_BIT_CACHE_GROUPS"] = "Respetar permisos de acceso";
?>