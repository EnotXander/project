<?
$MESS['IBLOCK_ADD_ERROR_REQUIRED'] = 'Laukas \"#PROPERTY_NAME#\" turi b�ti u�pildytas!';
$MESS['IBLOCK_ADD_ELEMENT_NOT_FOUND'] = 'Elementas nerastas';
$MESS['IBLOCK_ADD_ACCESS_DENIED'] = 'prieigos n�ra';
$MESS['IBLOCK_FORM_WRONG_CAPTCHA'] = '�vestas neteisingas tekstas i� paveiksl�lio';
$MESS['IBLOCK_ADD_MAX_ENTRIES_EXCEEDED'] = 'Vir�ytas �ra�� limitas';
$MESS['IBLOCK_ADD_MAX_LEVELS_EXCEEDED'] = 'Vir�itas skyri� limitas - #MAX_LEVELS# ';
$MESS['IBLOCK_ADD_LEVEL_LAST_ERROR'] = 'Leid�iama �terpti tik � paskutinio lygio skyrius';
$MESS['IBLOCK_USER_MESSAGE_ADD_DEFAULT'] = 'Elementas sekmingai �terptas';
$MESS['IBLOCK_USER_MESSAGE_EDIT_DEFAULT'] = 'Pakeitimai sekmingai i�saugoti';
$MESS['IBLOCK_ERROR_FILE_TOO_LARGE'] = '�keltos bylos dydis vir�ija limit�';
?>