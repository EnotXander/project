<?
$MESS['IBLOCK_TYPE_TIP'] = 'I�skleid�iamajame s�ra�e galite pasirinkti vien� i� sukurt� sistemoje informacini� blok� tip�. Paspaudus mygtuk� <b><i>OK</i></b> bus �kelti pasirinkto tipo informaciniai blokai. Pasirinkus <i>(kitas)</i> gretimame lange tur�site nurodyti informacinio bloko ID.';
$MESS['IBLOCK_ID_TIP'] = 'Pasirinkite vien� i� esan�i� informacini� blok�. Pasirinkus <i>(kitas)</i> gretimame lange tur�site nurodyti informacinio bloko ID.';
$MESS['SECTION_ID_TIP'] = 'Nurodomas ID ar simbolinis kodas skyriaus, kurio naujienos bus eksportuojami i RSS.';
$MESS['NUM_NEWS_TIP'] = 'Eksportuojam� � RSS naujien� skai�ius.';
$MESS['NUM_DAYS_TIP'] = 'Parametras nustato u� kiek dien� naujienas eksportuoti. Pvz. u� paskutines 30 dien�.';
$MESS['YANDEX_TIP'] = 'Naujien� siuntimo formato <i>';
?>