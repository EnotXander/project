<?
$MESS['CP_BCSL_DISPLAY_PANEL'] = 'Atvaizduoti �io komponento valdymo mygtukus';
$MESS['CP_BCSL_IBLOCK_TYPE'] = 'Informacinio bloko tipas';
$MESS['CP_BCSL_IBLOCK_ID'] = 'Informacinis blokas';
$MESS['CP_BCSL_SECTION_URL'] = 'URL, vedantis � puslap� su skyriaus turiniu';
$MESS['CP_BCSL_SECTION_ID'] = 'Skyriaus ID';
$MESS['CP_BCSL_SECTION_CODE'] = 'Skyriaus kodas';
$MESS['CP_BCSL_COUNT_ELEMENTS'] = 'Atvaizduoti element� skyriuje skai�i�';
$MESS['CP_BCSL_TOP_DEPTH'] = 'Atvaizduojamas skyri� gylis';
$MESS['CP_BCSL_ADD_SECTIONS_CHAIN'] = '�traukti skirsn� � navigacijos grandin�';
?>