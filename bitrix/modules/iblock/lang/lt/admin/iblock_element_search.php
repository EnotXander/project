<?
$MESS['IBLOCK_ELSEARCH_TITLE'] = 'Elemento paie�ka';
$MESS['IBLOCK_ELSEARCH_LOCK_BY'] = 'U�blokuotas';
$MESS['IBLOCK_ELSEARCH_CHOOSE_IBLOCK'] = 'Pasirinkite informacin� blok�';
$MESS['IBLOCK_ELSEARCH_USERINFO'] = 'Per�i�r�ti naudotojo parametrus';
$MESS['IBLOCK_ELSEARCH_SECTION_EDIT'] = 'Atidaryti sekcijos redagavimo puslap�';
$MESS['IBLOCK_ELSEARCH_ELEMENT_EDIT'] = 'Atidaryti elemento redagavimo puslap�';
$MESS['IBLOCK_ELSEARCH_SELECT'] = 'Pasirinkti';
$MESS['IBLOCK_ELSEARCH_NOT_SET'] = '(nenustatyta)';
$MESS['IBLOCK_ELSEARCH_F_TYPE'] = 'Formatas';
$MESS['IBLOCK_ELSEARCH_F_DATE'] = 'Data';
$MESS['IBLOCK_ELSEARCH_F_CHANGED'] = 'Atnaujino';
$MESS['IBLOCK_ELSEARCH_F_STATUS'] = 'Statusas';
$MESS['IBLOCK_ELSEARCH_F_SECTION'] = 'Skyrius';
$MESS['IBLOCK_ELSEARCH_F_ACTIVE'] = 'Aktyvus:';
$MESS['IBLOCK_ELSEARCH_F_TITLE'] = 'Pavadinimas';
$MESS['IBLOCK_ELSEARCH_F_DSC'] = 'Apra�ymas';
$MESS['IBLOCK_ELSEARCH_IBLOCK'] = 'Informacinis blokas:';
$MESS['IBLOCK_ELSEARCH_TYPE'] = 'Informacinio bloko tipas:';
$MESS['IBLOCK_ELSEARCH_DESC'] = 'Apra�ymas:';
?>