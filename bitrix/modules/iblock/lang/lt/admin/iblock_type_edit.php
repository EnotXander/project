<?
$MESS['IBTYPE_E_TAB1'] = 'Pagrindinis';
$MESS['IBTYPE_E_TAB1_T'] = 'Tipo nustatymas';
$MESS['IBTYPE_E_TAB2'] = 'Papildomi duomenys';
$MESS['IBTYPE_E_TAB2_T'] = 'Papildomi tipo nustatymai';
$MESS['IBTYPE_E_TITLE'] = 'Informacinio bloko tipo \"#ITYPE#\" redagavimas';
$MESS['IBTYPE_E_TITLE_2'] = 'Naujas informacinio bloko tipas';
$MESS['IBTYPE_E_LIST'] = 'S�ra�as';
$MESS['IBTYPE_E_CREATE'] = 'Sukurti';
$MESS['IBTYPE_E_DEL'] = '�alinti';
$MESS['IBTYPE_E_ID'] = 'Identifikatorius (ID):';
$MESS['IBTYPE_E_SECTIONS'] = 'Naudoti med�io klasifikatori� elementams sekcijuose:';
$MESS['IBTYPE_E_LANGS'] = 'Nuo kalbos priklausomi pavadinimai ir objekt� antra�t�s:';
$MESS['IBLOCK_TYPE_ADMIN_DEL_CONF'] = 'D�mesio! Tipas ir visi �io tipo informaciniai blokai bus pa�alinti! T�sti?';
$MESS['IBTYPE_E_FILE_AFTER'] = 'Byla su elemento redagavimo forma:';
?>