<?
$MESS['IBLOCK_ADM_UPD_ERROR'] = '�vyko klaida. Nepavyko atnaujinti �ra�o:';
$MESS['IBLOCK_ADM_HEADER_EL'] = 'Element�';
$MESS['IBLOCK_ADM_HEADER_SECT'] = 'Skirsniai';
$MESS['IBLOCK_ADM_HEADER_CODE'] = 'Simbolinis kodas';
$MESS['IBLOCK_ADM_HEADER_LIST_URL'] = 'Informacinio bloko URL adresas';
$MESS['IBLOCK_ADM_HEADER_DETAIL_URL'] = 'Puslapio elemento URL';
$MESS['IBLOCK_ADM_HEADER_TOINDEX'] = 'Indeksuoti elementus';
$MESS['IBLOCK_ADM_TO_EL_LIST'] = 'Atidaryti element� s�ra��';
$MESS['IBLOCK_ADM_TO_EDIT'] = 'Redaguoti informacin� blok�';
$MESS['IBLOCK_ADM_TO_ELLIST'] = 'Atidaryti element� s�ra��';
$MESS['IBLOCK_ADM_TO_SECTLIST'] = 'Atidaryti skirsni� s�ra��';
$MESS['IBLOCK_ADM_TO_ADDIBLOCK'] = 'Prid�ti informacin� blok�';
$MESS['IBLOCK_ADM_TO_ADDIBLOCK_TITLE'] = 'Prid�ti nauj� informacin� blok�';
$MESS['IBLOCK_ADM_FILT_SITE'] = 'Svetain�';
$MESS['IBLOCK_ADM_FILT_ACT'] = 'Aktyvus:';
$MESS['IBLOCK_ADM_FILT_CODE'] = 'Mnemoninis kodas';
$MESS['IBLOCK_ADM_FILT_NAME'] = 'Pavadinimas:';
$MESS['IBLOCK_ADM_FILTER_CODE'] = 'Mnemoninis kodas:';
$MESS['IBLOCK_ADM_HEADER_WORKFLOW'] = 'Dokument� apyvarta';
$MESS['IBLOCK_SECTIONS'] = 'Skirsniai';
$MESS['IBLOCK_ELEMENTS'] = 'Elementai';
$MESS['IBLOCK_ADM_MANAGE_HINT'] = '�ia galite tvarkyti informacini� blok� nustatymus';
$MESS['IBLOCK_ADM_MANAGE_HINT_HREF'] = 'Informaciniai blokai - Informacini� blok� tipai';
?>