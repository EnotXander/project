<?
$MESS['IBLOCK_TYPE_ADMIN_ERR_SAVE'] = 'Klaida i�saugojant tip�';
$MESS['IBLOCK_TYPE_ADMIN_ERR_DEL'] = 'Klaida pa�alinant tip�';
$MESS['IBLOCK_TYPE_ADMIN_NAV'] = 'Tipai';
$MESS['IBLOCK_TYPE_ADMIN_COL_NAME'] = 'Pavadinimas';
$MESS['IBLOCK_TYPE_ADMIN_COL_SORT'] = 'R��iavimas';
$MESS['IBLOCK_TYPE_ADMIN_COL_SECT'] = 'Sud�tyje yra skyriai';
$MESS['IBLOCK_TYPE_ADMIN_COL_RSS'] = 'RSS eksportuotas';
$MESS['IBLOCK_TYPE_ADMIN_COL_EDIT_BEF'] = 'failo redagavimas';
$MESS['IBLOCK_TYPE_ADMIN_COL_EDIT_AFT'] = 'redagavimo formos failas';
$MESS['IBLOCK_TYPE_ADMIN_DEL_CONF'] = 'D�mesio!Tipas ir visi �io tipo informaciniai blokai bus pa�alinti! T�sti?';
$MESS['IBLOCK_TYPE_ADMIN_TITLE'] = 'Informacini� blok� tipai';
$MESS['IBLOCK_TYPE_ADMIN_FILTER_ID'] = 'Tipo ID';
$MESS['IBLOCK_TYPE_ADMIN_ADD'] = 'Prid�ti nauj� tip�';
$MESS['IBLOCK_TYPE_ADMIN_ADD_HINT'] = 'Prid�ti nauj� informacinio bloko tip�';
$MESS['IBTYPE_A_GOLIST'] = 'Per�i�r�ti informacini� blok� s�ra��';
$MESS['IBTYPE_A_IB'] = 'Informaciniai blokai';
?>