<?
$MESS['IBLOCK_BAD_SITE_ID_NA'] = 'Nurodykit� tinklap�.';
$MESS['IBLOCK_BAD_SITE_ID'] = 'Blogai nurodytas daresas!';
$MESS['IBLOCK_BAD_NAME'] = 'Nenurodytas informacinio bloko pavadinimas.';
$MESS['IBLOCK_BAD_BLOCK_TYPE'] = 'Nenurodytas informacinio bloko tipas.';
$MESS['IBLOCK_MESS_ELEMENT_NAME'] = 'Elementas';
$MESS['IBLOCK_MESS_ELEMENTS_NAME'] = 'Elementai';
$MESS['IBLOCK_MESS_ELEMENT_ADD'] = 'Prid�ti element�';
$MESS['IBLOCK_MESS_ELEMENT_EDIT'] = 'Redaguoti element�';
$MESS['IBLOCK_MESS_ELEMENT_DELETE'] = '�alinti element�';
$MESS['IBLOCK_MESS_SECTION_NAME'] = 'Skyrius';
$MESS['IBLOCK_MESS_SECTIONS_NAME'] = 'Skirsniai';
$MESS['IBLOCK_MESS_SECTION_ADD'] = '�terpti skyri�';
$MESS['IBLOCK_MESS_SECTION_EDIT'] = 'Redaguoti skyri�';
$MESS['IBLOCK_MESS_SECTION_DELETE'] = '�alinti skyri�';
$MESS['IBLOCK_PANEL_HISTORY_BUTTON'] = 'Darbo istorija';
$MESS['IBLOCK_PANEL_EDIT_IBLOCK_BUTTON'] = 'Informacinio bloko \"#IBLOCK_NAME#\" savyb�s';
$MESS['IBLOCK_PANEL_UNKNOWN_COMPONENT'] = 'Ne�inomas komponentas';
$MESS['IBLOCK_PANEL_CONTROL_PANEL'] = 'Administravimo �rankiai';
$MESS['IBLOCK_PANEL_CONTROL_PANEL_ALT'] = 'Pereiti � administravimo �ranki� panel�';
$MESS['IBLOCK_TABLE_CREATION_ERROR'] = 'Klaida kuriant informacinio bloko savybi� reik�mi� lentel�.';
$MESS['IBLOCK_BAD_FILE_ERROR'] = 'Klaida �keliant byl� � server�.';
$MESS['IBLOCK_BAD_FILE_NOT_FOUND'] = 'Failas nerastas.';
$MESS['IBLOCK_BAD_FILE_NOT_PICTURE'] = 'Byla ne paveiksliukas.';
$MESS['IBLOCK_BAD_FILE_UNSUPPORTED'] = 'Nuotraukos dyd�io keitimas galimas tik JPEG, GIF arba PNG formato byloms.';
?>