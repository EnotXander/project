<?
$MESS["CT_BLL_TOOLBAR_ADD_ELEMENT_TITLE"] = "agregar un nuevo elemento a la secci�n actual";
$MESS["CT_BLL_TOOLBAR_EDIT_SECTION"] = "Gesti�n de secci�n";
$MESS["CT_BLL_TOOLBAR_EDIT_SECTION_TITLE"] = "Editar y eliminar secciones";
$MESS["CT_BLL_TOOLBAR_LIST"] = "Ajustes de la Lista";
$MESS["CT_BLL_TOOLBAR_LIST_TITLE"] = "Configurara la lista de configuraciones";
$MESS["CT_BLL_MOVE_TO_SECTION"] = "Mover la secci�n";
$MESS["CT_BLL_SELECTED"] = "Seleccionado";
$MESS["CT_BLL_TOOLBAR_BIZPROC_TITLE"] = "Procesos de Negocio";
$MESS["CT_BLL_TOOLBAR_BIZPROC"] = "Procesos de Negocio";
$MESS["CT_BLL_EXPORT_EXCEL"] = "Exportar a Microsoft Excel";
$MESS["CT_BLL_EXPORT_EXCEL_TITLE"] = "Exportar a Microsoft Excel";
$MESS["CT_BLL_TOOLBAR_PROCESS"] = "Configuraci�n de flujo de trabajo";
$MESS["CT_BLL_TOOLBAR_PROCESS_TITLE"] = "Cambiar las preferencias del flujo de trabajo";
?>