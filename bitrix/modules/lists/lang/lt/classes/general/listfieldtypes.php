<?
$MESS["LISTS_LIST_FIELD_NAME"] = "Pavadinimas";
$MESS["LISTS_LIST_FIELD_SORT"] = "R��is";
$MESS["LISTS_LIST_FIELD_ACTIVE_FROM"] = "Aktyvi nuo";
$MESS["LISTS_LIST_FIELD_ACTIVE_TO"] = "Aktyvi iki";
$MESS["LISTS_LIST_FIELD_PREVIEW_PICTURE"] = "Per�i�r�ti vaizd�";
$MESS["LISTS_LIST_FIELD_PREVIEW_TEXT"] = "Per�i�r�ti tekst�";
$MESS["LISTS_LIST_FIELD_DETAIL_PICTURE"] = "Detalus vaizdas";
$MESS["LISTS_LIST_FIELD_DETAIL_TEXT"] = "Detalus tekstas";
$MESS["LISTS_LIST_FIELD_DATE_CREATE"] = "Sukurti dat�";
$MESS["LISTS_LIST_FIELD_CREATED_BY"] = "Suk�r�";
$MESS["LISTS_LIST_FIELD_TIMESTAMP_X"] = "Pakeitimo data";
$MESS["LISTS_LIST_FIELD_MODIFIED_BY"] = "Pakeit�";
$MESS["LISTS_LIST_FIELD_S"] = "Eilut�";
$MESS["LISTS_LIST_FIELD_N"] = "Numeris";
$MESS["LISTS_LIST_FIELD_L"] = "S�ra�as";
$MESS["LISTS_LIST_FIELD_F"] = "Failas";
$MESS["LISTS_LIST_FIELD_G"] = "Susieti su skyriais";
$MESS["LISTS_LIST_FIELD_E"] = "Susieti su elementais";
?>