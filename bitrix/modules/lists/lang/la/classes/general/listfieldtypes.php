<?
$MESS["LISTS_LIST_FIELD_NAME"] = "Nombre";
$MESS["LISTS_LIST_FIELD_SORT"] = "Clasificar";
$MESS["LISTS_LIST_FIELD_ACTIVE_FROM"] = "Activar Desde";
$MESS["LISTS_LIST_FIELD_ACTIVE_TO"] = "Activar Hasta";
$MESS["LISTS_LIST_FIELD_PREVIEW_PICTURE"] = "Imagen Anterior";
$MESS["LISTS_LIST_FIELD_PREVIEW_TEXT"] = "Texto Anterior";
$MESS["LISTS_LIST_FIELD_DETAIL_PICTURE"] = "Cuadro Detallado";
$MESS["LISTS_LIST_FIELD_DETAIL_TEXT"] = "Texto Detallado";
$MESS["LISTS_LIST_FIELD_DATE_CREATE"] = "Fecha de Creaci�n";
$MESS["LISTS_LIST_FIELD_CREATED_BY"] = "Creado por";
$MESS["LISTS_LIST_FIELD_TIMESTAMP_X"] = "Modificar fecha";
$MESS["LISTS_LIST_FIELD_MODIFIED_BY"] = "Modificado por";
$MESS["LISTS_LIST_FIELD_S"] = "Cadena";
$MESS["LISTS_LIST_FIELD_N"] = "N�mero";
$MESS["LISTS_LIST_FIELD_L"] = "Lista";
$MESS["LISTS_LIST_FIELD_F"] = "Archivo";
$MESS["LISTS_LIST_FIELD_G"] = "Derivar a las Secciones";
$MESS["LISTS_LIST_FIELD_E"] = "Derivar a los elementos";
?>