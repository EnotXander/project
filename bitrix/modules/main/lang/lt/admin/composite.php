<?
$MESS["MAIN_COMPOSITE_TITLE"] = "Sud�tin� svetain�";
$MESS["MAIN_COMPOSITE_WARNING_TRANSID"] = "D�mesio! \"Session.use_trans_sid\" parametras yra �jungtas. HTML talpykla bus neaktyvi.";
$MESS["MAIN_COMPOSITE_WARNING"] = "Statinis talpyklos re�imas yra �jungtas. I�junkite j�.";
$MESS["MAIN_COMPOSITE_TAB"] = "Sud�tinis re�imas";
$MESS["MAIN_COMPOSITE_TAB_TITLE"] = "Parametrai ir nustatymai";
$MESS["MAIN_COMPOSITE_ON"] = "Sud�tinis re�imas yra �jungtas";
$MESS["MAIN_COMPOSITE_OFF"] = "Sud�tinis re�imas yra i�jungtas";
$MESS["MAIN_COMPOSITE_BUTTON_OFF"] = "I�jungti sud�tin� re�im�";
$MESS["MAIN_COMPOSITE_BUTTON_ON"] = "�jungti sud�tin� re�im�";
$MESS["MAIN_COMPOSITE_OPT"] = "Nustatymai";
$MESS["MAIN_COMPOSITE_INC_MASK"] = "�traukimo kauk�";
$MESS["MAIN_COMPOSITE_EXC_MASK"] = "Atskirties kauk�";
$MESS["MAIN_COMPOSITE_NO_PARAMETERS"] = "I�saugoti � disk� tik puslapius be parametr�";
$MESS["MAIN_COMPOSITE_ONLY_PARAMETERS"] = "taip pat i�saugoti puslapius tik su �iais parametrais";
$MESS["MAIN_COMPOSITE_QUOTA"] = "Disko kvota (MB)";
$MESS["MAIN_COMPOSITE_STAT_FILE_SIZE"] = "Dabartinis talpyklos dydis:";
$MESS["MAIN_COMPOSITE_SAVE"] = "I�saugoti nustatymus";
$MESS["MAIN_COMPOSITE_RESET"] = "Gr��inti numatytuosius";
$MESS["MAIN_COMPOSITE_TAB_SITES"] = "Svetain�s";
$MESS["MAIN_COMPOSITE_TAB_SITES_TITLE"] = "�jungti sud�tin� re�im� individualiose svetain�se";
$MESS["MAIN_COMPOSITE_TAB_GROUPS"] = "Grup�s";
$MESS["MAIN_COMPOSITE_TAB_GROUPS_TITLE"] = "Pasirinkite naudotoj� grupes, kurias laikysite kaip anonimi�kas";
$MESS["MAIN_COMPOSITE_BANNER_SEP"] = "Mygtukas";
$MESS["MAIN_COMPOSITE_BANNER_DISCLAIMER"] = "Technologijos <b>Sukurti svetain� </b> aktyvinimas automati�kai prid�s mygtuk� apatiniame de�iniajame puslapio kampe, kur �i funkcija yra naudojama. Mygtuko parodymo parametrus galima konfig�ruoti. Jei jums reikia �d�ti mygtuk� � kit� viet�, galite �vesti viet� � svetain�s �ablon�, naudodami <b>id=&quot;bx-composite-banner&quot;</b>.";
$MESS["MAIN_COMPOSITE_BANNER_SELECT_STYLE"] = "Mygtuko vaizdas";
$MESS["MAIN_COMPOSITE_BANNER_BGCOLOR"] = "Pakeisti fono spalv�";
$MESS["MAIN_COMPOSITE_BANNER_STYLE"] = "Pakeisti logotip� ir u�ra��";
$MESS["MAIN_COMPOSITE_BANNER_STYLE_WHITE"] = "Baltas fonas";
$MESS["MAIN_COMPOSITE_CLEAR_CACHE"] = "Atstatyti talpykl�";
$MESS["MAIN_COMPOSITE_HITS_WITHOUT_CACHE"] = "Paspaudim� be puslapio sud�tingumo d�l vietos diske tr�kumo kiekis:";
$MESS["MAIN_COMPOSITE_ACCELERATION"] = "Pagreitis";
$MESS["MAIN_COMPOSITE_IMAGES"] = "Vaizdai";
$MESS["MAIN_COMPOSITE_DYNAMIC_DATA"] = "Dinaminiai duomenys";
$MESS["MAIN_COMPOSITE_SLOGAN"] = "Stulbinantis greitis. Ne�tik�tinos galimyb�s ";
$MESS["MAIN_COMPOSITE_DESCRIPTION"] = "Nauja ir unikali svetain�s kartos technologija �ymiai paspartina j�s� statinio turinio atsisiuntimo laik� ir pateikia dinami�k� sluoksn� su tobulesn�mis galimyb�mis. Gal� gale, j�s� lankytojai atsidaro tinklalapius akimoju.";
$MESS["MAIN_COMPOSITE_X100_FEATURE"] = "Puslapiai �kelti <br/> 100 kart� grei�iau";
$MESS["MAIN_COMPOSITE_X100_RANKING"] = "Geresnis reitingas <br/> paie�kos sistemose";
$MESS["MAIN_COMPOSITE_X100_CONVERSION"] = "Keitimo kursas <br/> labai pager�jo";
$MESS["MAIN_COMPOSITE_LETS_GET_STARTED"] = "Prad�ia";
$MESS["MAIN_COMPOSITE_WARNING_EDUCATION"] = "-";
$MESS["MAIN_COMPOSITE_DOMAINS"] = "Domen� s�ra�as (kiekvienas � nauj� eilut�)";
$MESS["MAIN_COMPOSITE_STORAGE"] = "Laikyti pod�lyje";
$MESS["MAIN_COMPOSITE_STORAGE_FILES"] = "failai";
$MESS["MAIN_COMPOSITE_MEMCACHED_HOST"] = "Pagrindinis kompiuteris";
$MESS["MAIN_COMPOSITE_MEMCACHED_PORT"] = "Prievvadas";
$MESS["MAIN_COMPOSITE_EXT_ERROR"] = "#EXTENSION# i�pl�timas n�ra �diegtas";
$MESS["MAIN_COMPOSITE_MODULE_ERROR"] = "#MODULE# modulis n�ra �diegtas";
$MESS["MAIN_COMPOSITE_STORAGE_TITLE"] = "Pod�lio saugojimo re�imas";
$MESS["MAIN_COMPOSITE_HOST_HINT"] = "Laukas <b>Pagrindinis kompiuteris</b> turi nurodyti Unix socket, pavyzd�iui: <i>unix:///tmp/memcached.socket</i>,<br>�iuo atvej� laukas <b>Pagrindinis kompiuteris</b> turi b�ti nustatytas � nul� (<b>0</b>).";
$MESS["MAIN_COMPOSITE_CHECK_CONNECTION"] = "Testuoti jungt�";
$MESS["MAIN_COMPOSITE_CHECK_CONNECTION_ERR1"] = "PHP memcached n�ra �diegtas";
$MESS["MAIN_COMPOSITE_CHECK_CONNECTION_ERR2"] = "Nepavyko prisijungti prie memcached";
$MESS["MAIN_COMPOSITE_CHECK_CONNECTION_OK"] = "Sujungimas buvo s�kmingas";
$MESS["MAIN_COMPOSITE_CLUSTER_HINT"] = "Naudoti #A_START#Web klasterio#A_END# modulio nustatym� puslap� memcached sujungimo parametrams nustatyti. ";
$MESS["COMPOSITE_BANNER_TEXT"] = "Grei�iiau su Bitrix";
$MESS["MAIN_ADD"] = "Prid�ti";
$MESS["COMPOSITE_BANNER_URL"] = "http://www.bitrixsoft.com/composite/";
?>