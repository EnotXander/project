<?
$MESS["MAIN_SITE_SPEED_TITLE"] = "Velocidad del sitio";
$MESS["MAIN_SITE_SPEED_SECONDS"] = "sec";
$MESS["MAIN_SITE_SPEED_DOMAINS_LABEL"] = "Estad�sticas de dominio";
$MESS["MAIN_SITE_SPEED_HITS_LABEL"] = "Solicitudes procesadas";
$MESS["MAIN_SITE_SPEED_COMPOSITE_HITS"] = "Hits compuesto";
$MESS["MAIN_SITE_SPEED_TITLE_DESC"] = "Tiempo promedio de p�ginas en el sitio web se muestra en un explorador de cliente en los �ltimos 1.000 visitantes.";
$MESS["MAIN_SITE_SPEED_HISTO_TITLE"] = "Velocidad del sitio a trav�s del tiempo";
$MESS["MAIN_SITE_SPEED_GRAPH_TITLE"] = "Hits recientes";
$MESS["MAIN_SITE_SPEED_DOMAIN_NOT_FOUND"] = "No hay datos para este dominio.";
$MESS["MAIN_SITE_SPEED_CHOOSE_DOMAIN"] = "Elija otro dominio en la lista.";
$MESS["MAIN_SITE_SPEED_CONNECTION_ERROR"] = "No se puede conectar al servidor de estad�sticas.";
$MESS["MAIN_SITE_SPEED_PERIOD_LABEL"] = "Per�odo de presentaci�n de informes";
$MESS["MAIN_SITE_SPEED_PERF"] = "Monitor de rendimiento";
$MESS["MAIN_SITE_SPEED_COMPOSITE_SITE"] = "Sitio Web compuesto";
$MESS["MAIN_SITE_SPEED_CDN"] = "CDN Web Accelerator";
$MESS["MAIN_SITE_SPEED_ENABLED"] = "Encendido";
$MESS["MAIN_SITE_SPEED_DISABLED"] = "Apagado";
$MESS["MAIN_SITE_SPEED_PERF_NO_RES"] = "No se realiz� prueba de rendimiento";
$MESS["MAIN_SITE_SPEED_NOTES"] = "<p><strong>Prestaci�n de P�gina</strong> - tiempo transcurrido desde el momento en que se inicio la navegaci�n  hasta que la p�gina es visible en pantalla. Este valor se utiliza para obtener la velocidad del sitio. <br />
  <strong>DNS -</strong> el tiempo que una petici�n DNS tarda en completarse. <br />
  <strong>Conexi�n al servidor -</strong> tiempo requerido para que una m�quina cliente pueda conectarse a un servidor. <br />
  <strong>De respuesta del servidor -</strong> tiempo que un servidor consume al procesar una solicitud del cliente (incluyendo las negociaciones de red). <br />
  <strong>Descarga HTML -</strong> tiempo necesario para descargar el c�digo HTML de la p�gina (sin recursos como im�genes, CSS, Javascript). <br />
  <strong>Procesamiento de HTML -</strong> el tiempo consumido por el navegador para procesar una p�gina (incluyendo an�lisis de HTML, CSS, procesamiento de c�digo JavaScript y la renderizaci�n de p�ginas). Este valor representa el tiempo medido despu&eacute;s de que una p�gina fue descargada hasta que estuvo lista para verse en la pantalla.<br>
</p>";
?>