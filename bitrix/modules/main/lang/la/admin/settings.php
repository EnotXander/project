<?
$MESS["MAIN_TITLE"] = "Configuraci�n de M�dulos";
$MESS["MAIN_KERNEL"] = "N�cleo";
$MESS["MAIN_SELECT"] = "Seleccionar";
$MESS["MAIN_GROUP_RIGHTS"] = "Permisos del m�dulo";
$MESS["MAIN_DEFAULT"] = "< default >";
$MESS["MAIN_BY_DEFAULT"] = "Default:";
$MESS["MAIN_HINT_RESTORE_DEFAULTS"] = "Restauran configuraci�n por defecto";
$MESS["MAIN_HINT_RESTORE_DEFAULTS_WARNING"] = "Peligro! Toda la configuraci�n ser� revertida a los valores por defecto. �desea continuar?";
$MESS["MAIN_SUPER_ADMIN_RIGHTS_COMMENT"] = "(no puede restringir permisos de este grupo de usuarios <br>para tener acceso a las p�ginas del m�dulo y <br>modificar los datos) ";
$MESS["MAIN_TAB_SET"] = "Configuraciones";
$MESS["MAIN_TAB_TITLE_SET"] = "Configuraci�n del m�dulo";
$MESS["MAIN_TAB_RIGHTS"] = "Acceso";
$MESS["MAIN_TAB_TITLE_RIGHTS"] = "Permisos de acceso al m�dulo";
$MESS["MAIN_USER_GROUP_TITLE"] = "Ver par�metros de usuarios del grupo";
$MESS["MAIN_ADMIN_SET_CONTROLLER_ALT"] = "Anulado por el Contralor";
?>