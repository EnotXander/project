<?
$MESS["MAIN_PROLOG_ADMIN_LOGOUT"] = "Cerrar Sesi�n";
$MESS["MAIN_PROLOG_ADMIN_TITLE"] = "Secci�n Administrativa";
$MESS["TRIAL_ATTENTION"] = "ATENCI�N! Use <a href=\"/bitrix/admin/sysupdate.php\">SiteUpdate</a> para conseguir las actualizaciones m�s recientes de este software.<br>";
$MESS["TRIAL_ATTENTION_TEXT2"] = "El periodo de evaluaci�n expira en ";
$MESS["TRIAL_ATTENTION_TEXT3"] = "d�as";
$MESS["main_prolog_help"] = "Ayuda";
$MESS["prolog_main_show_menu"] = "Mostrar men�";
$MESS["prolog_main_m_e_n_u"] = "M<br>e<br>n<br>�<br>";
$MESS["prolog_main_less_buttons"] = "Usar botones peque�os en el men�";
$MESS["prolog_main_hide_menu"] = "Ocultar men�";
$MESS["MAIN_PR_ADMIN_CUR_LINK"] = "Link a la p�gina actual";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix"] = "�sta es una versi�n de pruebas de Bitrix Site Manager. ";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix"] = "El per�odo de prueba de Bitrix Site Manager ha expirado. Este sitio dejar� de trabajar totalmente en dos semanas. ";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix"] = "Puede comprar la versi�n completa de Bitrix Site Manager en <a href=\"http://www.bitrix.es/tienda/cms/\">http://www.bitrix.es/tienda/cms/</a>.";
$MESS["TRIAL_ATTENTION_TEXT1_bitrix"] = "Esta es una versi�n de pruebas de Bitrix Site Manager. ";
$MESS["TRIAL_ATTENTION_TEXT4_bitrix"] = "El per�odo de prueba de Bitrix Site Manager ha expirado. Este sitio dejar� de trabajar totalmente en dos semanas. ";
$MESS["TRIAL_ATTENTION_TEXT5_bitrix"] = "Puede comprar la versi�n completa de Bitrix Site Manager en <a href=\"http://www.bitrix.es/tienda/cms/\">http://www.bitrix.es/tienda/cms/</a>.";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_portal"] = "Esta es versi�n de pruebas del Portal de Intranet de Bitrix. ";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_portal"] = "El per�odo de pruebas del Portal de Intranet de Bitrix  ha expirado. Este sitio dejar� de funcionar por completo en dos semanas.";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_portal"] = "Usted puede comprar la versi�n completa del Portal de Intranet de Bitrix en <a href=\"http://bitrix.es/tienda/intranet/\">http://bitrix.es/tienda/intranet/</a>.";
$MESS["TRIAL_ATTENTION_TEXT1_bitrix_portal"] = "Esta es la versi�n de pruebas de Bitrix24";
$MESS["TRIAL_ATTENTION_TEXT4_bitrix_portal"] = "El per�odo de prueba de Bitrix24 ha expirado. Este sitio dejar� de funcionar por completo en dos semanas.";
$MESS["TRIAL_ATTENTION_TEXT5_bitrix_portal"] = "Usted puede comprar la versi�n completa de Bitrix24  en <a href=\"http://bitrix.es/tienda/intranet/\">http://bitrix.es/tienda/intranet/</a>.";
$MESS["prolog_main_more_buttons"] = "Agrandar botones";
$MESS["prolog_main_support1"] = "<span class=\"required\">Atenci�n!</span> su suscripci�n a actualizaciones y soporte t�cnico <b>expirar�</b> el #FINISH_DATE#, #DAYS_AGO#. Usted puede realizar la <a href=\"http://www.bitrix.es/tienda/cms/#REN/\">renovaci�n temprana </a> de su suscripci�n hasta 30 d�as despu�s del vencimiento de su licencia. ";
$MESS["prolog_main_support_days"] = "en <b>#N_DAYS_AGO#&nbsp;d�as(s)</b>";
$MESS["prolog_main_support2"] = "<span class=\"required\">Atenci�n!</span> su suscripci�n a Actualizaciones y Soporte T�cnico a <b>expirado</b> el #FINISH_DATE#, hace <b>#DAYS_AGO#&nbsp;d�as</b>. Usted puede comprar <a href=\"http://bitrix.es/tienda/\">la renovaci�n a \"precio temprano\" de su suscripci�n</a> hasta 30 d�as despu�s de vencida su licencia, luego deber� comprarla a precio tard�o.";
$MESS["prolog_main_support3"] = "<span class=\"required\">Atenci�n!</span>Su suscripci�n a Actualizaciones y Soporte T�cnico <b>expirar� </b> en #FINISH_DATE#. Usted puede comprar <a href=\"http://www.bitrix.es/tienda/\">la renovaci�n tard�a de su suscripci�n</a>.";
$MESS["prolog_main_today"] = "<b>hoy</b>";
$MESS["prolog_admin_headers_sent"] = "�Atenci�n! Caracteres no v�lidos se han detectado en el sistema de archivos: #FILE#, l�nea #LINE#.";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_eduportal"] = "Esta es una versi�n de prueba del producto del Portal Educativo de Bitrix.";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_eduportal"] = "El per�odo de prueba del producto del Portal educativo de Bitrix ha expirado. Este sitio dejar� de funcionar por completo en dos semanas.";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_eduportal"] = "Usted puede comprar la versi�n completa del Portal Educativo de Bitrix desde la p�gina <a href=\"http://www.bitrix.es/tienda/intranet/\">http://www.bitrix.es/tienda/intranet/</a>.";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_gosportal"] = "Esta es la versi�n de prueba de Portal Gubernamental de Bitrix.";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_gosportal"] = "El periodo de prueba del Portal Gubernamental de Bitrix ha caducado. Este sitio dejar� de funcionar por completo en dos semanas.";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_gosportal"] = "Usted puede comprar la versi�n completa del Portal Gubernamental de Bitrix desde <a href=\"http://www.bitrix.es/tienda/intranet/?r1=bsmtrial&amp;r2=expirado\">http://www.bitrix.es/tienda/intranet/</a>.";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_gossite"] = "Esta es la versi�n de pruebas del  Portal Gubernamental de Bitrix.";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_gossite"] = "El periodo de prueba del Portal Gubernamental de Bitrix ha caducado. Este sitio dejar� de funcionar por completo en dos semanas.";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_gossite"] = "Usted puede comprar la versi�n completa del Portal Gubernamental de Bitrix desde <a href=\"http://www.bitrix.es/tienda/intranet/\">http://www.bitrix.es/tienda/intranet/</a>.";
$MESS["MAIN_PR_ADMIN_FAV_ADD"] = "Agregar a Favoritos";
$MESS["MAIN_PR_ADMIN_FAV_DEL"] = "Eliminar de favoritos";
$MESS["MAIN_PR_ADMIN_FAV"] = "Favoritos";
$MESS["admin_panel_browser"] = "Panel de control no es compatible con Internet Explorer 7 o inferior. Por favor, instale un navegador moderno:<a href=\"http://www.firefox.com\">Firefox</a>, <a href=\"http://www.google.com/chrome/\">Chrome</a>, <a href=\"http://www.opera.com\">Opera</a> o <a href=\"http://www.microsoft.com/windows/internet-explorer/\">una nueva versi�n de Internet Explorer</a>.";
$MESS["prolog_main_support_wit"] = "�Qu� es esto?";
$MESS["prolog_main_support_wit_descr1"] = "�Qu� es vencimiento de la suscripci�n?";
$MESS["prolog_main_support_button_prolong"] = "Renovar suscripci�n";
$MESS["prolog_main_support_button_no_prolong"] = "No, gracias";
$MESS["prolog_main_support_button_no_prolong2"] = "Recordar m�s tarde";
$MESS["prolog_main_support_menu1"] = "en:";
$MESS["prolog_main_support_menu2"] = "semana";
$MESS["prolog_main_support_menu3"] = "dos semanas";
$MESS["prolog_main_support_wit_descr2"] = "Tan pronto como caduque su soporte y actualizaciones de suscripci�n t�cnica, usted ya no tendr� acceso a Marketplace. Efectivamente esto significa que usted no ser� capaz de instalar actualizaciones de la plataforma, comprar, instalar o
actualizar las soluciones del Marketplace. Ser� relegada su prioridad a techsupport
como los usuarios comunes con un tiempo de respuesta de hasta 24 horas. Usted todav�a puede
seguir utilizando el producto el tiempo que quiera. Para tener acceso a las actualizaciones del sistema y del mercado, lo que tiene que renovar es su suscripci�n.<br/><br/>
Se le concede un per�odo de gracia de un mes (30 d�as) despu�s de la expiraci�n en el que el
costo de la renovaci�n es s�lo el 22% del precio de la edici�n del producto (renovaci�n anticipada).<br/><br/>
Despu�s de ese per�odo, el costo de renovaci�n es del 60% del precio de su producto
edici�n (la renovaci�n tard�a).";
$MESS["prolog_main_support11"] = "<span class=\"required\">Attention!</span> Su techsupport y actualizaci�n de suscripci�n <b>will expire</b> on #FINISH_DATE#, #DAYS_AGO#.#WHAT_IS_IT#<br /> Su per�odo de gracia de renovaci�n temprana finalizar� el #SUP_FINISH_DATE#.";
$MESS["prolog_main_support21"] = "<span class=\"required\">Attention!</span> Su techsupport y suscripci�n de actualizaci�n ha caducado el #FINISH_DATE#, <b>#DAYS_AGO#&nbsp;</b> days ago.#WHAT_IS_IT#<br /> Su renovaci�n temprana finalizar� el #SUP_FINISH_DATE#.";
$MESS["prolog_main_support31"] = "<span class = \"necesaria\"> <span class = \"necesaria\"> �Atenci�n! </ span> Su techsupport y actualizaci�n de suscripci�n ha expirado el #FINISH_DATE#.#WHAT_IS_IT#<br />Ahora puede renovar su suscripci�n.";
$MESS["prolog_main_support_menu4"] = "<span style=\"color:red;\">month</span>";
$MESS["prolog_main_support_wit_descr2_cp"] = "Despu�s de caducar su suscripci�n de soporte y actualizaciones de productos t�cnicos, las actualizaciones de la plataforma no se pueden instalar para su copia del producto; usted no ser� capaz de obtener nuevas versiones del producto, instalaci�n o actualizaci�n de soluciones del mercado, utilizar los servicios de telefon�a web, o la caracter�stica libre de \"Cloud Backup\". Tambi�n, sus tickets de soporte presentados en nuestro servicio de asistencia ser�n tratados con una prioridad m�s baja (es posible que tenga que esperar hasta 48 horas).<br/><br/>
Aunque la renovaci�n de la suscripci�n de soporte t�cnico y actualizaciones de productos, no es obligatorio, te animamos a que renueve su suscripci�n por un a�o m�s. Las actualizaciones de productos incluyen correcciones de errores cr�ticos, parches y nuevas funcionalidades. Con una suscripci�n activa, puede instalar nuevos m�dulos, caracter�sticas y plantillas de sitio que est�n disponibles con cada nueva versi�n del producto, y actualizar su copia del producto a la �ltima versi�n.
<br/><br/>
Usted puede renovar su soporte y actualizaciones de productos t�cnicos de suscripci�n por un a�o adicional a tan solo el 22% del precio actual de productos. Tenga en cuenta que la opci�n de renovaci�n anticipada s�lo se encuentra disponible dentro de los pr�ximos 30 d�as despu�s del vencimiento de su suscripci�n actual. Despu�s del per�odo de gracia de 30 d�as, la opci�n de renovaci�n tard�a estar� disponible en cualquier momento posterior (60% del precio actual de productos).
<br/><br/>
Para m�s informaci�n, no dude en consultar la p�gina <a href=\"https://store.bitrix24.com/help/licensing-policy.php\">Pol�tica de Licencias</a>";
?>