<?
$MESS["MAIN_SMILE_IMPORT_FILE_ERROR"] = "El archivo especificado no existe.";
$MESS["MAIN_SMILE_IMPORT_FILE_EXT_ERROR"] = "El archivo especificado tienes un formato no v�lido.";
$MESS["MAIN_SMILE_IMPORT_SET_ID_ERROR"] = "El ID del smiley no se ha especificado.";
$MESS["MAIN_SMILE_TYPE_ERROR"] = "El tipo del smiley no se es correcto.";
$MESS["MAIN_SMILE_SET_ID_ERROR"] = "El ID del smiley no se ha especificado.";
$MESS["MAIN_SMILE_TYPING_ERROR"] = "No se ha provedido el tipo del smiley.";
$MESS["MAIN_SMILE_IMAGE_ERROR"] = "No se ha proveido el nombre del smiley.";
$MESS["MAIN_SMILE_IMAGE_XY_ERROR"] = "El tama�o del smiley no se ha especificado.";
$MESS["MAIN_SMILE_ALL_SET"] = "Todos los conjuntos";
$MESS["MAIN_SMILE_SET_NAME"] = "Conjunto #ID#";
$MESS["MAIN_SMILE_IMPORT_UNPACK_ERROR"] = "Error extrayendo archivos. El archivo puede estar da�ado.";
?>