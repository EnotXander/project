<?
$MESS["instructionsCommon"] = "<div class='iu-help'><b>Para cargar m�ltiples fotos, usted necesita instalar el software especial.</b> ";
$MESS["instructionsNotWinXPSP2"] = " Recargar la p�gina y hacer click en \"Yes\" en el di�logo de la instalaci�n.";
$MESS["instructionsWinXPSP2"] = " ";
$MESS["instructionsVista"] = "por favor oprima en la barra de informaci�n y seleccione El control ActiveX del men� desplegable. Luego de recargar la p�gina oprima Continuar e instale cuando usted vea el di�logo de instalaci�n control.";
$MESS["instructionsCommon2"] = "</div>";
$MESS["AuthenticationRequestNtlmText"] = "[Nombre] requiere autentificaci�n.";
$MESS["AuthenticationRequestPasswordText"] = "Contrase�a:";
$MESS["AuthenticationRequestLoginText"] = "Registro:";
$MESS["AuthenticationRequestDomainText"] = "Dominio:";
$MESS["AuthenticationRequestButtonCancelText"] = "Cancelar";
$MESS["ButtonAddAllToUploadListText"] = "Agregar todo";
$MESS["ButtonAddFilesText"] = "Agregar archivos...";
$MESS["ButtonAddFoldersText"] = "Agregar carpetas...";
$MESS["ButtonAddToUploadListText"] = "Agregar";
$MESS["ButtonAdvancedDetailsCancelText"] = "Cancelar";
$MESS["ButtonCheckAllText"] = "Marcar todo";
$MESS["ButtonDeselectAllText"] = "Deseleccionar";
$MESS["ButtonRemoveAllFromUploadListText"] = "Borrar todo";
$MESS["ButtonRemoveFromUploadListText"] = "Borrar";
$MESS["ButtonSelectAllText"] = "Seleccionar todo";
$MESS["ButtonSendText"] = "Enviar";
$MESS["ButtonUncheckAllText"] = "No marcar ninguno";
$MESS["DescriptionEditorButtonCancelText"] = "Cancelar";
$MESS["DropFilesHereText"] = "Agregar fotos a la lista usando los botones &laquo;Add folder&raquo; and &laquo;Add files&raquo;";
$MESS["HoursText"] = "h";
$MESS["KilobytesText"] = "K";
$MESS["MegabytesText"] = "Mb";
$MESS["LoadingFilesText"] = "Leyendo archivos...";
$MESS["MenuThumbnailsText"] = "Miniaturas";
$MESS["MenuDetailsText"] = "Detalles";
$MESS["MenuListText"] = "Lista";
$MESS["MenuIconsText"] = "�conos";
$MESS["MenuArrangeByText"] = "Ordenar los �conos por";
$MESS["MenuArrangeByUnsortedText"] = "Desordenado";
$MESS["MenuArrangeByNameText"] = "Nombre";
$MESS["MenuArrangeBySizeText"] = "Tama�o";
$MESS["MenuArrangeByTypeText"] = "Tipo";
$MESS["MenuArrangeByModifiedText"] = "Modificado";
$MESS["MenuArrangeByPathText"] = "Ruta";
$MESS["MenuSelectAllText"] = "Seleccionar todo";
$MESS["MenuRefreshText"] = "Actualizar";
$MESS["MenuDeselectAllText"] = "Deseleccionar todo";
$MESS["MenuInvertSelectionText"] = "Invertir selecci�n";
$MESS["MenuRemoveFromUploadListText"] = "Quitar desde la lista a cargar";
$MESS["MenuRemoveAllFromUploadListText"] = "Quitar todo desde la lista a cargar";
$MESS["MenuAddAllToUploadListText"] = "Agregar todo a la lista a cargar";
$MESS["MenuAddToUploadListText"] = "Agregar a la lista a cargar";
$MESS["MessageBoxTitleText"] = "Imagen a cargar";
$MESS["MessageCannotConnectToInternetText"] = "Falla al conectarse a internet.";
$MESS["AuthenticationRequestBasicText"] = "[Nombre] requiere autentificaci�n.";
$MESS["MessageMaxFileCountExceededText"] = "El archivo[Nombre] no puede ser cargado. Usted no puede cargar mas de [Limit] archivos.";
$MESS["MessageMaxTotalFileSizeExceededText"] = "El achivo [Nombre] no pude ser cargado. Tama�o total de los archivos excede el [L�mite] kB.";
$MESS["MessageNoInternetSessionWasEstablishedText"] = "La conexi�n de internet no est� establecida.";
$MESS["MessageNoResponseFromServerText"] = "No hay respuesta del servidor.";
$MESS["MessageRetryOpenFolderText"] = "La carpeta reciente no est� disponible. Es como si estuviera ubicada en un medio removible. Insertar disco y oprimir Retro alimentaci�n. O, tambi�n oprimir Cancelar para saltar esta acci�n y procedimiento.";
$MESS["MessageServerNotFoundText"] = "Servidor o proxy [Name] no se ha encontrado.";
$MESS["MessageSwitchAnotherFolderWarningText"] = "Usted desea una carpeta diferente. Esto podr�a deseleccionar todos los archivos seleccionados.\\n\\n Para continuar y Cancelar la selecci�n, oprimir OK.\\n Para preservar la selecci�n y mantener en la carpeta actual, oprimir Cancelar.";
$MESS["MessageUnexpectedErrorText"] = "Error al cargar. Por favor intente nuevamente.";
$MESS["MessageUploadCancelledText"] = "Carga cancelada.";
$MESS["MessageUploadCompleteText"] = "Carga finalizada.";
$MESS["MessageUploadFailedText"] = "La carga fall� (La conexi�n se rompi�).";
$MESS["MessageUserSpecifiedTimeoutHasExpiredText"] = "El usuario define la espera del tiempo expirado";
$MESS["MessageMaxFileSizeExceededText"] = "El archivo [nombre] no puede ser seleccionado. El tama�o de este archivo excede el l�mite ([L�mite] KB).";
$MESS["MessageFileSizeIsTooSmallText"] = "El archivo [Nombre] no puede ser seleccionado. El tama�o de este archivo es m�s peque�o que el l�mite ([Limite] KB).";
$MESS["MessageDimensionsAreTooSmallText"] = "La imagen [Nombre] no puede ser seleccionado. Las dimensiones de esta imagen ([AnchoOriginaldeImagen]x [AltoOriginaldeImagen])son muy peque�as. La imagen debe ser m�s larga que [AnchoM�ximode Imagen]x[AltoM�ximodeImagen].";
$MESS["MessageCmykImagesAreNotAllowedText"] = "Im�genes CMYK no est�n permitidas";
$MESS["ListKilobytesText"] = "KB";
$MESS["ListColumnFileNameText"] = "Nombre";
$MESS["ListColumnFileSizeText"] = "Tama�o";
$MESS["ListColumnFileTypeText"] = "Tipo";
$MESS["ListColumnLastModifiedText"] = "Modificado";
$MESS["LargePreviewGeneratingPreviewText"] = "Generando previo...";
$MESS["LargePreviewIconTooltipText"] = "Miniaturas previas";
$MESS["LargePreviewNoPreviewAvailableText"] = "No hay previo disponible.";
$MESS["IncludeSubfoldersText"] = "Incluir sub carpetas";
$MESS["EditDescriptionText"] = "Editar descripci�n...";
$MESS["FileIsTooLargeText"] = "El archivo es demasiado extenso";
$MESS["FileIsTooSmallText"] = "Archivo es muy chico";
$MESS["ErrorDeletingFilesDialogMessageText"] = "No puede ser eliminado [Nombre]";
$MESS["DimensionsAreTooSmallText"] = "Imagen es muy chica";
$MESS["DimensionsAreTooLargeText"] = "Imagen es demasiado extensa";
$MESS["CmykImagesAreNotAllowedText"] = "Archivo es CMYK";
$MESS["AddFolderDialogButtonCancelText"] = "Cancelar";
$MESS["AddFolderDialogButtonSkipAllText"] = "Saltar todo";
$MESS["AddFolderDialogButtonSkipText"] = "saltar";
$MESS["AddFolderDialogTitleText"] = "Agregando carpeta...";
$MESS["MinutesText"] = "minutos";
$MESS["ProgressDialogCancelButtonText"] = "Cancelar";
$MESS["ProgressDialogCloseButtonText"] = "Cerrar";
$MESS["ProgressDialogCloseWhenUploadCompletesText"] = "Cerrar cuadro de di�logo cuando la carga finalize.";
$MESS["ProgressDialogEstimatedTimeText"] = "Tiempo estimado: [Actual] del [Total]";
$MESS["ProgressDialogPreparingDataText"] = "Preparando base de datos...";
$MESS["ProgressDialogSentText"] = "Cargado: Uploaded: [Actual] del [Total]";
$MESS["ProgressDialogTitleText"] = "Archivo a cargar";
$MESS["ProgressDialogWaitingForResponseFromServerText"] = "Esperando por respuesta del servidor...";
$MESS["ProgressDialogWaitingForRetryText"] = "Esperando por recarga";
$MESS["RemoveIconTooltipText"] = "Eliminar";
$MESS["SecondsText"] = "segundos";
$MESS["UnixFileSystemRootText"] = "Sistema de archivo";
$MESS["UnixHomeDirectoryText"] = "Directorio principal";
$MESS["RotateIconClockwiseTooltipText"] = "Rotar en sentido dek reloj";
$MESS["RotateIconCounterclockwiseTooltipText"] = "Rotar en sentido contrario al reloj";
$MESS["MessageDimensionsAreTooLargeText"] = "La imagen [Nombre] no puede ser cargada. Las dimensiones de la imagen ([AnchoOriginaldeImagen]x[AltoOriginaldeImagen]) debe ser menor que [AnchoM�ximodeImagen]x[AltoM�ximodeImagen] p�xeles.";
?>