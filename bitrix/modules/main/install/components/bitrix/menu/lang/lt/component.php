<?
$MESS["MAIN_MENU_EDIT"] = "Redaguoti meniu elementus";
$MESS["MAIN_MENU_ADD"] = "Prid�ti meniu elementus";
$MESS["MAIN_MENU_ADD_NEW"] = "Sukurti meniu �iame skyriuje";
$MESS["MAIN_MENU_TOP_PANEL_BUTTON_TEXT"] = "Meniu";
$MESS["MAIN_MENU_TOP_PANEL_BUTTON_ALT"] = "Redaguoti meniu elementus";
$MESS["MAIN_MENU_TOP_PANEL_BUTTON_HINT"] = "Atidaro meniu redagavimo form�. Spustel�kite rodykl� nor�dami redaguoti visas dabartinio puslapio meniu arba sukurti nauj� meniu.";
$MESS["MAIN_MENU_TOP_PANEL_ITEM_TEXT"] = "Redaguoti #MENU_TITLE#";
$MESS["MAIN_MENU_TOP_PANEL_ITEM_ALT"] = "Redaguoti meniu elementus #MENU_TITLE#";
$MESS["MAIN_MENU_ADD_TOP_PANEL_ITEM_TEXT"] = "Sukurti #MENU_TITLE#";
$MESS["MAIN_MENU_ADD_TOP_PANEL_ITEM_ALT"] = "Sukurti meniu #MENU_TITLE# �iame skyriuje";
$MESS["MAIN_MENU_DEL_TOP_PANEL_ITEM_TEXT"] = "Pa�alinti  \"#MENU_TITLE#\"";
$MESS["MAIN_MENU_DEL_TOP_PANEL_ITEM_ALT"] = "Pa�alinti meniu \"#MENU_TITLE#\" dabartiniame skyriuje";
$MESS["menu_comp_del_menu"] = "Pa�alinti meniu dabartiniame skyriuje";
$MESS["menu_comp_del_conf"] = "Ar tikrai norite pa�alinti meniu \"#MENU_TITLE#\" dabartiniame skyriuje?";
?>