<?
$MESS["MPF_INSERT_FILE"] = "Spauskite nor�dami �terpti fail�";
$MESS["MPF_IMAGE_LINK"] = "�terpti vaizdo nuorod�";
$MESS["MPF_TAGS"] = "�ym�s:";
$MESS["MPF_SPOILER"] = "�terpti spoiler�";
$MESS["FPF_VIDEO"] = "�terpti video";
$MESS["BPC_VIDEO_P"] = "Video kelias";
$MESS["MPF_VIDEO_SIZE"] = "Dydis (P/A)";
$MESS["BPC_VIDEO_PATH_EXAMPLE"] = "Pavyzdys: <i>http://www.youtube.com/watch?v=lWbjWfTttWU</i> <br/> arba <i>www.mysite.com/video/my_video.mp4</i>";
$MESS["MPF_ADD_TAG"] = "Prid�ti daugiau";
$MESS["MPF_DESTINATION"] = "�:";
$MESS["MPF_DESTINATION_1"] = "Prid�ti asmen�, grup� arba skyri�";
$MESS["MPF_DESTINATION_2"] = "Prid�ti daugiau";
$MESS["MPF_DESTINATION_3"] = "Visi darbuotojai";
$MESS["MPF_DESTINATION_4"] = "Visi naudotojai";
$MESS["MPF_EDITOR"] = "Vizualus redaktorius";
$MESS["MPF_FILES"] = "Failai:";
$MESS["MPF_FILE_TITLE"] = "�kelti failus";
$MESS["MPF_MORE"] = "Daugiau";
$MESS["MPF_TAG_TITLE"] = "Prid�ti �ym�";
$MESS["MPF_ADD_TAG1"] = "Prid�ti ";
$MESS["MPF_MENTION_TITLE"] = "Prid�ti pamin�jim�
";
$MESS["MPF_IMAGE_TITLE"] = "�terpti vaizd�";
$MESS["MPF_FILE_INSERT_IN_TEXT"] = "�terpti <br /> tekste";
$MESS["MPF_FILE_IN_TEXT"] = "Tekste";
$MESS["MPF_BUTTON_SEND"] = "Si�sti";
$MESS["MPF_BUTTON_CANCEL"] = "At�aukti";
$MESS["MPF_SMILE_SET"] = "rinkiniai";
$MESS["BLOG_LINK_SHOW_NEW"] = "Si�sti �inut�";
?>