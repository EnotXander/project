<?
$MESS["ECL_T_ACCESS_DENIED"] = "Prieiga u�drausta";
$MESS["ECL_T_INACTIVE_FEATURE"] = "Naudotojo �vyki� kalendorius yra nepasiekiamas.";
$MESS["ECL_T_NO_ITEMS"] = "Joki� artimiausi� �vyki�.";
$MESS["ECLF_LHE_SHOW_PANEL"] = "Vizualus redaktorius";
$MESS["ECLF_EVENT_NAME"] = "�vykio pavadinimas";
$MESS["ECLF_EVENT_FROM"] = "Prad�ios datta";
$MESS["ECLF_EVENT_TO"] = "Pabaigos data";
$MESS["ECLF_EVENT_FROM_DATE_TIME"] = "�vykis prasid�s";
$MESS["ECLF_EVENT_TO_DATE_TIME"] = "�vykis pasibaigs";
$MESS["ECLF_TIME_FROM"] = "Pasirinkti prad�ios dat� ir laik�";
$MESS["ECLF_TIME_TO"] = "Pasirinkti pabaigos dat� ir laik�";
$MESS["ECLF_EVENT_ALL_DAY"] = "Vis� dien�";
$MESS["ECLF_EVENT_REMIND"] = "Nustatyti priminim�";
$MESS["ECLF_EVENT_REMIND_FOR"] = "Nustatyti priminim�";
$MESS["ECLF_EVENT_LOCATION"] = "�vykio vieta";
$MESS["ECLF_EVENT_DESCRIPTION"] = "Apra�ymas";
$MESS["ECLF_REM_MIN"] = "minut�s";
$MESS["ECLF_REM_HOUR"] = "valandos";
$MESS["ECLF_REM_DAY"] = "dienos";
$MESS["ECLF_LHE_CREATE_LINK"] = "Nuoroda";
$MESS["ECLF_LHE_UPLOAD_FILE"] = "�kelti fail�";
$MESS["ECLF_LHE_VIDEO"] = "Prid�ti video";
$MESS["ECLF_DESTINATION"] = "Dalyviai";
$MESS["ECLF_SHOW_ADD_SECT"] = "Daugiau";
$MESS["ECLF_HIDE_ADD_SECT"] = "Sl�pti papaildomus parametrus";
?>