<?
$MESS["CAL_REST_ACCESS_DENIED"] = "Prieiga u�drausta";
$MESS["CAL_REST_SECTION_NEW_ERROR"] = "Klaida kuriant skyri�";
$MESS["CAL_REST_SECTION_SAVE_ERROR"] = "Klaida redaguojant skyri�";
$MESS["CAL_REST_SECTION_DELETE_ERROR"] = "Klaida �alinant skyri�";
$MESS["CAL_REST_SECT_ID_EXCEPTION"] = "Nenurodytas skyriaus ID";
$MESS["CAL_REST_EVENT_ID_EXCEPTION"] = "Nenurodytas �vykio ID";
$MESS["CAL_REST_EVENT_DELETE_ERROR"] = "Klaida �alinant �vyk�";
$MESS["CAL_REST_EVENT_NEW_ERROR"] = "Klaida kuriant �vyk�";
$MESS["CAL_REST_EVENT_UPDATE_ERROR"] = "Klaida redaguojant �vyk�";
$MESS["CAL_REST_PARAM_EXCEPTION"] = "Nenurodytas privalomas metodo \"#REST_METHOD#\" parametras \"#PARAM_NAME#\"";
$MESS["CAL_REST_SECTION_ERROR"] = "J�s nurod�te neteising� kalendoriaus skyriaus ID arba dabartinis vartotojas neturi prieigos prie jos";
$MESS["CAL_REST_PARAM_ERROR"] = "Neteisinga parametro \"#PARAM_NAME#\" reik�m�";
$MESS["CAL_REST_GET_STATUS_ERROR"] = "Klaida i�gaunant status�";
$MESS["CAL_REST_GET_DATA_ERROR"] = "Klaida i�gaunant duomenis";
?>