<?
$MESS["EC_ACCESS_DENIED"] = "Prieiga u�drausta";
$MESS["EC_CALENDAR_CREATE_ERROR"] = "Klaida kuriant kalendori�.";
$MESS["EC_CAL_INCORRECT_ERROR"] = "Neteisingas kalendorius.";
$MESS["EC_WRONG_TYPE"] = "Neteisingas kalendoriaus tipas.";
$MESS["EC_EVENT_DEL_ERROR"] = "Klaida �alinant �vyk�.";
$MESS["EC_CALENDAR_SAVE_ERROR"] = "Klaida saugant kalendori�.";
$MESS["EC_CALENDAR_DEL_ERROR"] = "Klaida �alinant kalendori�.";
$MESS["EC_MO_F"] = "Pirmadienis";
$MESS["EC_TU_F"] = "Antradienis";
$MESS["EC_WE_F"] = "Tre�iadienis";
$MESS["EC_TH_F"] = "Ketvirtadienis";
$MESS["EC_FR_F"] = "Penktadienis";
$MESS["EC_SA_F"] = "�e�tadienis";
$MESS["EC_SU_F"] = "Sekmadienis";
$MESS["EC_MO"] = "Pr";
$MESS["EC_TU"] = "An";
$MESS["EC_WE"] = "Tr";
$MESS["EC_TH"] = "Kt";
$MESS["EC_FR"] = "Pn";
$MESS["EC_SA"] = "�t";
$MESS["EC_SU"] = "Sk";
$MESS["EC_JAN"] = "Sausis";
$MESS["EC_FEB"] = "Vasaris";
$MESS["EC_MAR"] = "Kovas";
$MESS["EC_APR"] = "Balandis";
$MESS["EC_MAY"] = "Gegu��";
$MESS["EC_JUN"] = "Bir�elis";
$MESS["EC_JUL"] = "Liepa";
$MESS["EC_AUG"] = "Rugpj�tis";
$MESS["EC_SEP"] = "Rugs�jis";
$MESS["EC_OCT"] = "Spalis";
$MESS["EC_NOV"] = "Lapkritis";
$MESS["EC_DEC"] = "Gruodis";
$MESS["EC_JAN_R"] = "Sausis";
$MESS["EC_FEB_R"] = "Vasaris";
$MESS["EC_MAR_R"] = "Kovas";
$MESS["EC_APR_R"] = "Balandis";
$MESS["EC_MAY_R"] = "Gegu��";
$MESS["EC_JUN_R"] = "Bir�elis";
$MESS["EC_JUL_R"] = "Liepa";
$MESS["EC_AUG_R"] = "Rugpj�tis";
$MESS["EC_SEP_R"] = "Rugs�jis";
$MESS["EC_OCT_R"] = "Spalis";
$MESS["EC_NOV_R"] = "Lapkritis";
$MESS["EC_DEC_R"] = "Gruodis";
$MESS["EC_DEF_SECT_USER"] = "Naudotojas:";
$MESS["EC_DEF_SECT_GROUP"] = "Grup�";
$MESS["EC_DEF_SECT_USER_CAL"] = "Mano kalendorius";
$MESS["EC_DEF_SECT_GROUP_CAL"] = "Pagrindiniai �vykiai";
$MESS["EC_DEF_SECT_DESC"] = "Kalendorius buvo sukurtas sistemos.";
$MESS["EC_EXP_SP_TITLE"] = "M�gstamiausi �vykiai";
$MESS["EC_PARENT_EVENT"] = "�altinis";
$MESS["EC_PROP_CONFIRMED_NAME"] = "Patvirtintas";
$MESS["EC_PROP_CONFIRMED_TEXT_Y"] = "Pakvietimas priimtas";
$MESS["EC_PROP_CONFIRMED_TEXT_N"] = "Pakvietimas atmestas";
$MESS["EC_PROP_CONFIRMED_UNK"] = "?";
$MESS["EC_PROP_CONFIRMED_YES"] = "Taip";
$MESS["EC_PROP_CONFIRMED_NO"] = "Ne";
$MESS["EC_PERIOD_TYPE"] = "Laikotarpio tipas";
$MESS["EC_PERIOD_COUNT"] = "Periodi�kumas";
$MESS["EC_EVENT_LENGTH"] = "Trukm�";
$MESS["EC_PERIOD_ADDITIONAL"] = "Daugiau";
$MESS["EC_REMIND_SETTINGS"] = "Priminimo parametrai";
$MESS["EC_EVENT_REMINDER"] = "Priminimas d�l �vykio";
$MESS["EC_EVENT_REMINDER_IN_GROUP"] = "Nustatyti kalendoriuje \"#CALENDAR_NAME#\" i� \"#GROUP_NAME#\"";
$MESS["EC_EVENT_REMINDER_IN_USER"] = "Nustatyti kalendoriuje \"#CALENDAR_NAME#\" i� [B]#USER_NAME#[/B]";
$MESS["EC_EVENT_REMINDER_IN_PERSONAL"] = "Nustatyti J�s� kalendoriuje \"#CALENDAR_NAME#\"";
$MESS["EC_EVENT_REMINDER_IN_COMMON"] = "Nustatyti kalendoriuje \"#CALENDAR_NAME#\" (#IBLOCK_NAME#)";
$MESS["EC_EVENT_REMINDER_DETAIL"] = "[url=#URL_VIEW#]I�samiau[/url]";
$MESS["EC_ACCESSIBILITY_S"] = "Prieinamumas";
$MESS["EC_ACCESSIBILITY"] = "Naudotojo prieinamumas";
$MESS["EC_IMPORTANCE"] = "Pirmumas";
$MESS["EC_PRIVATE"] = "Asmeninis";
$MESS["EC_PRIVATE_ABSENT"] = "joks";
$MESS["EC_VERSION"] = "Versija";
$MESS["EC_ACCESSIBILITY_ABSENT"] = "I�vyk�s";
$MESS["EC_ACCESSIBILITY_BUSY"] = "U�imtas";
$MESS["EC_ACCESSIBILITY_QUEST"] = "nei�spr�stas";
$MESS["EC_ACCESSIBILITY_FREE"] = "Laisvas";
$MESS["EC_IMPORTANCE_HIGH"] = "Didel�";
$MESS["EC_IMPORTANCE_NORMAL"] = "Normali";
$MESS["EC_IMPORTANCE_LOW"] = "�ema";
$MESS["EC_T_CALENDARS"] = "Kalendoriai";
$MESS["EC_T_EVENT_CALENDAR"] = "�vyki� kalendorius";
$MESS["EC_T_CALENDAR"] = "Kalendorius";
$MESS["EC_T_EDIT_EVENT"] = "Redaguoti �vyk�";
$MESS["EC_BASIC"] = "Bendras";
$MESS["EC_BASIC_TITLE"] = "Bendri �vykio parametrai";
$MESS["EC_T_NAME"] = "Vardas";
$MESS["EC_T_EVENT_NAME"] = "�vykio pavadinimas";
$MESS["EC_T_DESC"] = "Apra�ymas";
$MESS["EC_T_DESC_TITLE"] = "�vykio informacija";
$MESS["EC_T_REPEATING"] = "Pasikartojantis �vykis";
$MESS["EC_T_REPEAT"] = "Pakartoti";
$MESS["EC_T_REPEAT_CHECK_LABEL"] = "Pakartoti �vyk�";
$MESS["EC_T_REPEAT_NONE"] = "Niekada";
$MESS["EC_T_REPEAT_DAILY"] = "Kasdien";
$MESS["EC_T_REPEAT_WEEKLY"] = "Kas savait�";
$MESS["EC_T_REPEAT_MONTHLY"] = "Kas menes�";
$MESS["EC_T_REPEAT_YEARLY"] = "Kasmet";
$MESS["EC_T_DIALOG_STOP_REPEAT"] = "kartoti tol, kol";
$MESS["EC_T_CREATE_DEF"] = "[Sukurti Numatyt�j�]";
$MESS["EC_T_DELETE_EVENT"] = "I�trinti �vyk�";
$MESS["EC_T_SAVE"] = "I�saugoti";
$MESS["EC_T_CLOSE"] = "Nutraukti";
$MESS["EC_T_NEW_EVENT"] = "Naujas �vykis";
$MESS["EC_T_ADD"] = "Prid�ti";
$MESS["EC_T_VIEW_EVENT"] = "Per�i�r�ti �vyk�";
$MESS["EC_T_EDIT"] = "Redaguoti";
$MESS["EC_T_DELETE"] = "Pa�alinti";
$MESS["EC_T_COLOR"] = "Spalva";
$MESS["EC_T_DELETE_CALENDAR"] = "Pa�alinti kalendori�";
$MESS["EC_T_CALEN_DIS_WARNING"] = "Pasirinktas kalendorius yra i�jungtas ir nerodo �vyki�.";
$MESS["EC_T_CALEN_EXP_TITLE"] = "Eksportuoti kalendori� � iCal format�";
$MESS["EC_T_ALLOW_CALEN_EXP"] = "�jungti kalendoriaus eksport� (iCal)";
$MESS["EC_T_CALEN_EXP_CREATE_LINK"] = "Sukurti";
$MESS["EC_T_CALEN_EXP_RECREATE_LINK"] = "Atkurti";
$MESS["EC_T_CALEN_EXP_SET"] = "Eksportuoti �vykius";
$MESS["EC_T_CALEN_EXP_SET_ALL"] = "Visi";
$MESS["EC_T_CALEN_EXP_SET_3_9"] = "3 m�n. atgal ir 9 m�n. � priek�";
$MESS["EC_T_CALEN_EXP_SET_6_12"] = "6 m�n. atgal ir 12 m�n. � priek�";
$MESS["EC_T_EXPORT_NOTICE_OUTLOOK_LINK"] = "Kaip rankini� b�du eksportuoti kalendori� � MS Outlook";
$MESS["EC_T_EXPORT_NOTICE_OUTLOOK_TITLE"] = "Kaip rankini� b�du konfig�ruoti kalendoriaus prenumerat� MS Outlooke";
$MESS["EC_T_EXPORT_NOTICE_OUTLOOK"] = "<ol>
<li>Nukopijuokite nuorod� � i�karpin�.</li>
<li>MS Outlooke pasirinkite meniu komand�: <b>�rankiai</b> > <b>Paskyros nustatymai...</b>.</li>
<li> <b>Interneto kalendoriai</b> �sel�je spragtel�kite<b>Naujas...</b>.</li>
<li>�d�kite kalendoriaus nuorod� i� i�karpin�s</li>
<li>�veskite aplanko pavadinim� ir kalendoriaus apra�ym�.</li>
</ol>";
$MESS["EC_T_SP_CALENDARS"] = "M�gstamiausias kalendorius";
$MESS["EC_T_SUPERPOSE_TITLE"] = "Galimi kalendoriai";
$MESS["EC_ADD_EX_CAL"] = "Konfig�ruot";
$MESS["EC_ADD_EX_CAL_TITLE"] = "Surasti ir prid�ti kalendori�";
$MESS["EC_EXPORT_SP_CALS"] = "Eksportuoti pasirinktus kalendorius � iCal format�";
$MESS["EC_SP_DIALOG_USERS_NOT_FOUND"] = "Naudotuoj� nerasta";
$MESS["EC_T_ADD_TO_SP"] = "Prid�ti prie Megstamiausi�";
$MESS["EC_ADD_CAL"] = "Prid�ti kalendori�";
$MESS["EC_ADD_CAL_TITLE"] = "Sukurti nauj� kalendori�";
$MESS["EC_USER_ADD_SP_TRACKING"] = "Prid�ti naudotojo kalendorius";
$MESS["EC_TAB_MONTH"] = "M�nuo";
$MESS["EC_TAB_WEEK"] = "Savait�";
$MESS["EC_TAB_DAY"] = "Diena";
$MESS["EC_TAB_MONTH_TITLE"] = "Parodyti m�nesio �vykius";
$MESS["EC_TAB_WEEK_TITLE"] = "Parodyti dsavait�s �vykius";
$MESS["EC_TAB_DAY_TITLE"] = "Parodyti dienos �vykius";
$MESS["EC_EXT_DIAL"] = "Daugiau...";
$MESS["EC_GO_TO_EXT_DIALOG"] = "Redaguoti �vykio Informacija";
$MESS["EC_ADD_GUEST"] = "Prid�ti sve�ius";
$MESS["EC_MARK_MEETING"] = "Pa�ym�ti �vyk� kaip susitikim�";
$MESS["EC_MARK_MEETING_2"] = "Dukart spustel�kite, jei norite pa�ym�ti �vyk� kaip susitikim�";
$MESS["EC_ADD_METTING_TEXT"] = "Prid�ti kvietimo tekst�";
$MESS["EC_METTING_TEXT"] = "Kvietimo tekstas";
$MESS["EC_HIDE"] = "Pasl�pti";
$MESS["EC_HIDE_METTING_TEXT_TITLE"] = "Sl�pti kvietimo teksto lauk�";
$MESS["EC_EDEV_EVENT"] = "�vykis";
$MESS["EC_EDEV_GUESTS"] = "Sve�iai";
$MESS["EC_NO_ATTENDEES"] = "n�ra dalyvi�";
$MESS["EC_ATT_SUM"] = "viso";
$MESS["EC_ATT_Y"] = "Patvirtinta";
$MESS["EC_ATT_N"] = "Atsisakyti";
$MESS["EC_ATT_Q"] = "Dar svarstoma";
$MESS["EC_OPEN_MEETING"] = "�is susitikimas yra vie�as";
$MESS["EC_OPEN_MEETING_TITLE"] = "Dalyvavimas nereikalauja patvirtinimo";
$MESS["EC_NOTIFY_STATUS"] = "Informuoti, kai dalyviai patvirtina arba atmeta kvietim�";
$MESS["EC_REINVITE"] = "Papra�yti vartotoj� dar kart� patvirtinti dalyvavim�";
$MESS["EC_DECLINE_INFO"] = "J�s atsisak�te dalyvauti �iame susitikime,";
$MESS["EC_ACCEPTED_STATUS"] = "J�s dalyvausite �iame susitikime.";
$MESS["EC_STATUS_COMMENT"] = "j�s� komentaras";
$MESS["EC_STATUS_COMMENT_TITLE"] = "Prid�ti trump� komentar�, paai�kinti savo sprendimo prie�ast�.";
$MESS["EC_EDEV_EVENT_TITLE"] = "�vykio parametrai";
$MESS["EC_EDEV_GUESTS_TITLE"] = "Sve�iai";
$MESS["EC_GUEST_LIST"] = "Sve�i� s�ra�as";
$MESS["EC_DEL_ALL_GUESTS"] = "Pa�alinti visus";
$MESS["EC_DEL_ALL_GUESTS_TITLE"] = "Pa�alinti visus sve�ius";
$MESS["EC_EDEV_HOST"] = "Valdytojas";
$MESS["EC_EDEV_DATE_FROM"] = "Prad�ios data";
$MESS["EC_EDEV_DATE_TO"] = "Pabaigos data";
$MESS["EC_EDEV_FROM_DATE_TIME"] = "�vykio prad�ios data ir laikas";
$MESS["EC_EDEV_TO_DATE_TIME"] = "�vykio pabaigos data ir laikas";
$MESS["EC_EDEV_TIME_FROM"] = "Prad�ios laikas";
$MESS["EC_EDEV_TIME_TO"] = "Pabaigos laikas";
$MESS["EC_FULL_DAY"] = "Visos dienos";
$MESS["EC_EDEV_ADD_TAB"] = "Daugiau";
$MESS["EC_EDEV_ADD_TAB_TITLE"] = "Papildomi �vykio parametrai";
$MESS["EC_EDEV_REMINDER"] = "Priminimas";
$MESS["EC_EDEV_REMIND_EVENT"] = "Nustatykite priminim�";
$MESS["EC_EDEV_REMIND_FOR"] = "Priminti man prie�";
$MESS["EC_EDEV_REM_MIN"] = "minu�i�";
$MESS["EC_EDEV_REM_HOUR"] = "valand�";
$MESS["EC_EDEV_REM_DAY"] = "dien�";
$MESS["EC_EDEV_REM_SAVE"] = "�ra�yti nustatyt�j�";
$MESS["EC_EDEV_REM_SAVE_TITLE"] = "I�saugoti �iuos parametrus kaip numatytuosius ";
$MESS["EC_EDEV_EXP_WARN"] = "Pastaba! Eksporto duomen� patikrinimas nepavyko. Pra�ome u�tikrinti, kad j�s� prieigos teises galioja.";
$MESS["EC_MORE_BUT_TITLE"] = "Daugiau";
$MESS["EC_EDDIV_SPECIAL_NOTES"] = "Ypatingos pastabos";
$MESS["EC_ACCESSIBILITY_TITLE"] = "Mano dalyvavimas �vykyje";
$MESS["EC_IMPORTANCE_TITLE"] = "Svarba";
$MESS["EC_IMPORTANCE_H"] = "Didel�";
$MESS["EC_IMPORTANCE_N"] = "Vidutin�";
$MESS["EC_IMPORTANCE_L"] = "ma�a";
$MESS["EC_PRIVATE_EVENT"] = "Privatus �vykis";
$MESS["EC_CAL_STATUS"] = "Vie�asis re�imas";
$MESS["EC_CAL_STATUS_PRIVATE"] = "Privatus kalendorius";
$MESS["EC_CAL_STATUS_TIME"] = "Rodyti prieinamum�";
$MESS["EC_CAL_STATUS_TITLE"] = "Rodyti prieinamum� ir pavadinim�";
$MESS["EC_CAL_STATUS_FULL"] = "Rodyti vis� informacij�";
$MESS["EC_EDEV_ENCOUNTER_PARAMS"] = "�vykio parametrai";
$MESS["EC_JS_DEL_EVENT_CONFIRM"] = "Ar tikrai norite i�trinti �� �vyk�?";
$MESS["EC_JS_DEL_EVENT_ERROR"] = "Klaida trinant �vyk�.";
$MESS["EC_JS_EV_NAME_ERR"] = "�vykio pavadinimas nenurodomas.";
$MESS["EC_JS_EV_DIAP_END_ERR"] = "Nenurodyta paskutin� intervalo data";
$MESS["EC_JS_EV_FROM_ERR"] = "Tr�ksta �vykio prad�ios datos arba ji yra neteisinga.";
$MESS["EC_JS_EV_DATES_ERR"] = "�vykio pabaigos data yra ankstesn� u� prad�ios dat�.";
$MESS["EC_JS_EV_SAVE_ERR"] = "Klaida �ra�ant �vyk�.";
$MESS["EC_JS_NEW_EVENT"] = "Naujas �vykis";
$MESS["EC_JS_EDIT_EVENT"] = "Redaguoti �vyk�";
$MESS["EC_JS_VIEW_EVENT"] = "Per�i�r�ti �vyk�";
$MESS["EC_JS_DEL_EVENT"] = "I�trinti �vyk�";
$MESS["EC_JS_FROM"] = "Nuo:";
$MESS["EC_JS_TO"] = "Iki:";
$MESS["EC_JS_TO_"] = "iki";
$MESS["EC_JS_FROM_"] = "nuo";
$MESS["EC_RRULE_EVERY_DAY"] = "kasdien";
$MESS["EC_RRULE_EVERY_DAY_1"] = "kas  #DAY#  dien�";
$MESS["EC_RRULE_EVERY_WEEK"] = "kas savait�  #DAYS_LIST# ";
$MESS["EC_RRULE_EVERY_WEEK_1"] = "kiekvien� #WEEK#  savait�: #DAYS_LIST# ";
$MESS["EC_RRULE_EVERY_MONTH"] = "kas m�nes�";
$MESS["EC_RRULE_EVERY_MONTH_1"] = "kas #MONTH# m�nes�";
$MESS["EC_RRULE_EVERY_YEAR"] = "kasmet #MONTH# m�nesio  #DAY# dien� ";
$MESS["EC_RRULE_EVERY_YEAR_1"] = "kas #YEAR# metus  #MONTH# m�nesio #DAY# dien�";
$MESS["EC_RRULE_UNTIL"] = "iki #UNTIL_DATE#";
$MESS["EC_RRULE_FROM"] = "nuo  #FROM_DATE#";
$MESS["EC_JS_EVERY_M"] = "kiekvien�";
$MESS["EC_JS_EVERY_F"] = "kiekvien�";
$MESS["EC_JS_EVERY_M_"] = "Kiekvien�";
$MESS["EC_JS_EVERY_F_"] = "Kiekvien�";
$MESS["EC_JS_EVERY_N"] = "kiekvien�";
$MESS["EC_JS_EVERY_N_"] = "Kiekvien�";
$MESS["EC_JS_WEEK_P"] = "savait� (-es)";
$MESS["EC_JS_DAY_P"] = "diena (-os)";
$MESS["EC_JS_MONTH_P"] = "m�nuo� (-iai)";
$MESS["EC_JS_YEAR_P"] = "metai ";
$MESS["EC_JS_DATE_P_"] = "dienos";
$MESS["EC_JS_MONTH_P_"] = "m�nesai";
$MESS["EC_JS_SHOW_PREV_YEAR"] = "Praeiti metai";
$MESS["EC_JS_SHOW_NEXT_YEAR"] = "Kiti metai";
$MESS["EC_JS_ADD_CALEN"] = "Prid�ti kalendori�";
$MESS["EC_JS_ADD_CALEN_TITLE"] = "Prid�ti nauj� kalendori�";
$MESS["EC_JS_EDIT"] = "Redaguoti";
$MESS["EC_JS_DELETE"] = "I�trinti";
$MESS["EC_JS_EDIT_CALENDAR"] = "Pakeisti kalendoriaus nustatymus";
$MESS["EC_JS_DEL_CALENDAR"] = "I�trinti kalendori� ir visus �vykius";
$MESS["EC_JS_NEW_CALEN_TITLE"] = "Naujas kalendorius";
$MESS["EC_JS_EDIT_CALEN_TITLE"] = "Redaguoti kalendori�";
$MESS["EC_JS_CALEN_NAME_ERR"] = "Kalendoriaus pavadinimas nenurodomas.";
$MESS["EC_JS_CALEN_SAVE_ERR"] = "Klaida �ra�ant kalendori�.";
$MESS["EC_JS_DEL_CALENDAR_CONFIRM"] = "Ar tikrai norite i�trinti kalendori� ir visus �vykius?";
$MESS["EC_JS_DEL_CALEN_ERR"] = "Klaida trinant kalendori�.";
$MESS["EC_JS_ADD_NEW_EVENT"] = "Prid�ti nauj� �vyk�";
$MESS["EC_JS_SELECT_MONTH"] = "Pasirinkite m�nes�";
$MESS["EC_JS_SHOW_PREV_MONTH"] = "Praeitas m�nuo";
$MESS["EC_JS_SHOW_NEXT_MONTH"] = "Kitas m�nuo";
$MESS["EC_JS_LOAD_EVENTS_ERR"] = "Klaida �keliant �vykius";
$MESS["EC_JS_MORE"] = "Daugiau";
$MESS["EC_JS_ITEM"] = "�ra�ai";
$MESS["EC_JS_EXPORT"] = "Eksportuoti";
$MESS["EC_JS_EXPORT_TILE"] = "Eksportuoti � iCal format�";
$MESS["EC_SONET_GROUP"] = "Grup�";
$MESS["EC_SONET_CALENDAR"] = "Kalendorius";
$MESS["EC_SUPERPOSE_GR_COMMON"] = "Bendrieji kalendoriai";
$MESS["EC_SUPERPOSE_GR_GROUP"] = "Grup�s kalendoriai";
$MESS["EC_SUPERPOSE_GR_USER"] = "Naudotojo kalendoriai";
$MESS["EC_SUPERPOSE_GR_CUR_USER"] = "Mano kalendoriai";
$MESS["EC_CAL_HIDE"] = "Pa�alinti i� m�gstamiausi�";
$MESS["EC_CAL_HIDE_TITLE"] = "I�trinti kalendori� i� m�gstamiausi� s�ra�o";
$MESS["EC_ADD_TO_SP"] = "Prid�ti prie m�gstamiausi�";
$MESS["EC_CAL_ADD_TO_SP_TITLE"] = "Prid�ti kalendori� � m�gstamiausi� s�ra��";
$MESS["EC_HIDE_SP_CALENDAR_ERR"] = "Klaida slepiant kalendori�.";
$MESS["EC_APPEND_SP_CALENDAR_ERR"] = "Klaida pridedant kalendori�.";
$MESS["EC_FLIPPER_HIDE"] = "Sl�pti";
$MESS["EC_FLIPPER_SHOW"] = "Atkurti";
$MESS["EC_SHOW_All_CALS"] = "Rodyti vis� kalendori� �vykius";
$MESS["EC_HIDE_All_CALS"] = "Sl�pti vis� kalendori� �vykius ";
$MESS["EC_EXP_DIAL_TITLE"] = "Eksportuoti nuorod�";
$MESS["EC_EXP_DIAL_TITLE_SP"] = "Nuoroda � Eksportuoti pasirinktus kalendorius";
$MESS["EC_EXP_TEXT"] = "Eksportuoti kalendorius � tre�i�j� �ali� programas (iCal formatu):";
$MESS["EC_EXP_TEXT_SP"] = "Eksportuoti pasirinktus kalendorius � tre�i�j� �ali� programas (iCal formatu):";
$MESS["EC_USER_CALENDARS"] = "Naudotojo kalendoriai";
$MESS["EC_DELETE_DYN_SP_GROUP_TITLE"] = "Pa�alinti grup� i� s�ra�o";
$MESS["EC_DELETE_DYN_SP_GROUP"] = "I�trinti";
$MESS["EC_CALS_ARE_ABSENT"] = "Kalendori� nerasta";
$MESS["EC_DELETE_ALL_USER_CALENDARS"] = "Pa�alinti visus nsudotojo kalendorius i� M�gstamiausi� (i�skyrus sav� kalendori�)";
$MESS["EC_DEL_ALL_TRACK_USERS_CONF"] = "Ar tikrai norite pa�alinti vis� naudotoj� kalendorius i� M�gstamiausi�?";
$MESS["EC_SHOW_PREV_WEEK"] = "Rodyti praeit� savait�";
$MESS["EC_SHOW_NEXT_WEEK"] = "Rodyti kit� savait�";
$MESS["EC_CUR_TIME"] = "Dabartinis laikas";
$MESS["EC_GO_TO_DAY"] = "Per�i�r�ti �ios dienos �vykius";
$MESS["EC_DEL_GUEST_TITLE"] = "Pa�alinti asmen� i� sve�i� s�ra�o";
$MESS["EC_DEL_GUEST_CONFIRM"] = "Ar tikrai norite pa�alinti �� asmen� i� sve�i� s�ra�o?";
$MESS["EC_DEL_ALL_GUESTS_CONFIRM"] = "Ar j�s tikrai norite i�trinti visus pakviestus sve�ius?";
$MESS["EC_DEL_OWNER_CONFIRM"] = "J�s negalite savo asmeniniame kalendoriuje sukurti �vyk� be j�s� dalyvavimo. �alinant save i� �vykio j�s pa�alinsite ir visus kitus dalyvius. Ar tikrai norite pa�alinti save?";
$MESS["EC_CANT_DEL_GUEST_TITLE"] = "�alinant �vykio k�r�j� i� susitikimo bus pa�alinti visi kiti dalyviai.";
$MESS["EC_GUEST_STATUS_Q"] = "Nuomon� ne�inoma";
$MESS["EC_GUEST_STATUS_Y"] = "Dalyvavimas patvirtintas";
$MESS["EC_GUEST_STATUS_N"] = "Dalyvauti atsisakyta";
$MESS["EC_USER_PROFILE"] = "naudotojo profilis";
$MESS["EC_ALL_GUESTS"] = "Visi nariai";
$MESS["EC_ALL_GUESTS_TITLE"] = "Rodyti visus sve�ius";
$MESS["EC_DEL_ENCOUNTER"] = "Atsisakyti dalyvauti";
$MESS["EC_EDEV_CONFIRMED"] = "Patvirtinta";
$MESS["EC_ACCEPT_MEETING"] = "Dalyvauti susirinkime";
$MESS["EC_ACCEPT_MEETING_2"] = "Pergalvoti ir dalyvauti susirinkime";
$MESS["EC_EDEV_CONF_Y_TITLE"] = "Patvirtinti dalyvavim�";
$MESS["EC_EDEV_CONF_N"] = "Atsisakyti";
$MESS["EC_EDEV_CONF_N_TITLE"] = "Atsisakyti dalyvauti (ir i�trinti �vyk� i� j�s� kalendoriaus)";
$MESS["EC_NOT_CONFIRMED"] = "Dalyvavims nepatvirtintas";
$MESS["EC_T_DIALOG_NEVER"] = "neribotas";
$MESS["EC_ACCESSIBILITY_B"] = "U�imtas";
$MESS["EC_ACCESSIBILITY_Q"] = "Neapsisprend�s";
$MESS["EC_ACCESSIBILITY_F"] = "Nemokamai";
$MESS["EC_ACCESSIBILITY_A"] = "N�ra vietoj";
$MESS["EC_LOST_SESSION_ERROR"] = "J�s� sesijos laikas baig�si! Pra�ome autorizuotis.";
$MESS["EC_CONNECT_TO_OUTLOOK"] = "Prisijungti prie \"Outlook\"";
$MESS["EC_CONNECT_TO_OUTLOOK_TITLE"] = "Prisijungti prie \"Outlook\"";
$MESS["EC_USERS_NOT_FOUND"] = "Naudotojas nerastas";
$MESS["EC_USER_BUSY"] = "Naudotojas #USER# u�si�m�s arba jo n�ra per nustatyt� laikotarp�";
$MESS["EC_USERS_NOT_AVAILABLE"] = "Ne visi pakviesti naudotojai yra per nustatyt� laikotarp�";
$MESS["EC_MESS_INVITE"] = "Naudotojas #OWNER_NAME#  kvie�ia jus dalyvauti \"[B]#TITLE#[/B]\", kuris vyks #ACTIVE_FROM#";
$MESS["EC_MESS_INVITE_SITE"] = "Nor��iau pakviesti jus � susitikim� \"[B]#TITLE#[/B]\" [B]#ACTIVE_FROM#[/B]";
$MESS["EC_MESS_INVITE_CHANGED"] = "Naudotojas #OWNER_NAME#  pakeit� susitikim� \"[B]#TITLE#[/B]\", kuris vyks  #ACTIVE_FROM# ir kuriame jus dalyvaujate";
$MESS["EC_MESS_INVITE_CHANGED_SITE"] = "Susitikimas \"[B]#TITLE#[/B]\", kuriame jus dalyvaujate [B]#ACTIVE_FROM#[/B], buvo pakeistas.";
$MESS["EC_MESS_INVITE_CANCEL"] = "Naudotojas #OWNER_NAME# at�auk� susitikim� \"[B]#TITLE#[/B]\", kuris tur�jo vykti#ACTIVE_FROM# ir kuriame  Jus nor�jote dalyvauti";
$MESS["EC_MESS_INVITE_CANCEL_SITE"] = "Susitikimas  \"[B]#TITLE#[/B]\", kuri tur�jo vykti [B]#ACTIVE_FROM#[/B], buvo at�auktas.";
$MESS["EC_MESS_MEETING_TEXT"] = "Kvietimo tekstas: \"#MEETING_TEXT#\"";
$MESS["EC_MESS_INVITE_DETAILS"] = "J�s galite per�i�r�ti informacij� j�s� kalendoriuje: [url=#LINK#]I�sami informacija apie �vyk�[/url]";
$MESS["EC_MESS_INVITE_DETAILS_SITE"] = "J�s galite per�i�r�ti susitikimo informacij� [url=#LINK#]j�s� kalendorius[/url].";
$MESS["EC_MESS_INVITE_CONF_Y"] = "[url=#LINK#]Patvirtinti dalyvavim�[/url]";
$MESS["EC_MESS_INVITE_CONF_N"] = "[url=#LINK#]Atsisakyti dalyvauti[/url]";
$MESS["EC_MESS_INVITE_CONF_Y_SITE"] = "Patvirtinti";
$MESS["EC_MESS_INVITE_CONF_N_SITE"] = "Atsisakyti";
$MESS["EC_MESS_VIEW_OWN_CALENDAR"] = "J�s galite per�i�r�ti kitus renginius j�s� [url=#LINK#]asmeniniame kalendoriuje[/url]";
$MESS["EC_MESS_VIEW_OWN_CALENDAR_OUT"] = "J�s  galite per�i�r�ti kitus renginius ir susitikimus savo asmeniame kalendoriuje: #LINK#";
$MESS["EC_MESS_INVITE_TITLE"] = "Kvietimas \"#TITLE#\" nuo #OWNER_NAME#";
$MESS["EC_MESS_INVITE_CHANGED_TITLE"] = "�vykio asikeitimai: \"#TITLE#\" ";
$MESS["EC_MESS_INVITE_CANCEL_TITLE"] = "�vykis \"#TITLE#\" buvo at�auktas";
$MESS["EC_MESS_INVITE_ACCEPTED"] = "#GUEST_NAME# ketina atvykti � �vyk� \"[B]#TITLE#[/B]\", kuris vyks #ACTIVE_FROM#";
$MESS["EC_MESS_INVITE_ACCEPTED_SITE"] = "A� ketinu dalyvauti susitikime \"[B]#TITLE#[/B]\", kuris vyks [B]#ACTIVE_FROM#[/B]";
$MESS["EC_MESS_INVITE_DECLINED"] = "#GUEST_NAME# neketina dalyvauti j�s� �vykyje  \"[B]#TITLE#[/B]\", kuris vyks #ACTIVE_FROM#";
$MESS["EC_MESS_INVITE_DECLINED_SITE"] = "Deja, a� negal�siu dalyvauti susirinkime  \"[B]#TITLE#[/B]\", kuris vyks [B]#ACTIVE_FROM#[/B]";
$MESS["EC_MESS_INVITE_ACC_COMMENT"] = "Komentaras: \"#COMMENT#\"";
$MESS["EC_IS_MEETING"] = "Susitikimas ";
$MESS["EC_HOST_IS_ABSENT"] = "K�r�jo n�ra";
$MESS["EC_MEETING_TEXT"] = "Kvietimo tekstas";
$MESS["EC_MEETING_TEXT2"] = "Kvietimas";
$MESS["EC_LOCATION"] = "Vieta";
$MESS["EC_LOCATION_TITLE"] = "�vykio viet�";
$MESS["EC_ADD_GROUP_MEMBER"] = "Visi grup�s nariai";
$MESS["EC_ADD_GROUP_MEMBER_TITLE"] = "Prid�ti visus grup�s narius prie �vykio";
$MESS["EC_ADD_MEMBERS_FROM_STR"] = "I� �mon�s strukt�ros";
$MESS["EC_ADD_MEMBERS_FROM_STR_TITLE"] = "Prid�ti �vykio dalyvius i� �mon�s strukt�ros";
$MESS["EC_COMPANY_STRUCTURE"] = "�mon�s strukt�ra";
$MESS["EC_SELECT"] = "Pasirinkti";
$MESS["EC_SELECT_USERS_NOTICE"] = "Pasirinkite reikiam� skyri� arba naudotoj� (-us) ir paspauskite \"Pasirinkti\".";
$MESS["EC_OPEN_CLOSE_SECT"] = "Rodyti/Sl�pti";
$MESS["EC_SELECT_SECTION"] = "Prid�ti visus naudotojus prie \"#SEC_TITLE#\" skyriaus";
$MESS["EC_SELECT_USER"] = "Pasirinkti naudotojus";
$MESS["EC_NO_COMPANY_STRUCTURE"] = "�mon�s strukt�ros n�ra.";
$MESS["EC_MEET_TEXT_CHANGE_ALERT"] = "Pastaba: kei�iant kvietimo tekst� dalyviams turi b�ti prane�ta dar kart�.";
$MESS["EC_MEETING_CALENDAR"] = "Prid�ti visus kvietimus i� kit� naudotoj� prie �io kalendoriaus";
$MESS["EC_PLANNER"] = "Tvarkara�tis";
$MESS["EC_PLANNER2"] = "�vykio tvarkara�tis";
$MESS["EC_PLANNER_TITLE"] = "Atidaryti tvarkaras�io lang�";
$MESS["EC_APPLY"] = "Pritaikyti";
$MESS["EC_NEXT"] = "Kitas";
$MESS["EC_EVENT_DURATION"] = "Trukm�";
$MESS["EC_EVENT_DURATION_TITLE"] = "�vykio trukm�";
$MESS["EC_EVENT_DUR_LOCK"] = "Fiksuoti trukm�";
$MESS["EC_FROM"] = "Prad�ia";
$MESS["EC_TO"] = "Pabaiga";
$MESS["EC_SCALE"] = "Skal�";
$MESS["EC_AUTO_SEL"] = "Parinkti automati�kai";
$MESS["EC_AUTO_SEL_MORE"] = "Parinkti automati�kai daugiau";
$MESS["EC_IMP_GUEST"] = "Privalomas sve�ias";
$MESS["EC_NOT_IMP_GUEST"] = "Pageidautinas sve�ias";
$MESS["EC_PL_DUR_HOUR1"] = "valanda";
$MESS["EC_PL_DUR_HOUR2"] = "valanda";
$MESS["EC_PL_EVENT"] = "Planuojamas �vykis";
$MESS["EC_PL_EVENT_MOVE"] = "Perkelti �vyk�";
$MESS["EC_PL_EVENT_MOVE_LEFT"] = "Keisti prad�ios dat� ir/arba laik�";
$MESS["EC_PL_EVENT_MOVE_RIGHT"] = "Keisti pabaigos dat� ir/arba laik�";
$MESS["EC_PL_CLEAN_LIST"] = "I�valyti";
$MESS["EC_PL_CLEAN_MEETING_LIST_TITLE"] = "I�valyti pos�d�i� sal�s s�ra��";
$MESS["EC_PL_SHOW_LEGEND"] = "Rodyti sutartinius �ym�jimus";
$MESS["EC_PL_LEGEND"] = "Sutartiniai �ym�jimai";
$MESS["EC_PL_SEL_MEET_ROOM"] = "pasirinkite pos�d�i� sal�";
$MESS["EC_PL_OPEN_MR_PAGE"] = "Pos�d�i� sal�s informacija";
$MESS["EC_PL_MR_PLACE_COUNT"] = "Viet� skai�ius";
$MESS["EC_PL_MR_PHONE"] = "Telefonas";
$MESS["EC_MR_FREE"] = "At�aukti susitikim� sal�s u�sakym�";
$MESS["EC_NO_GUEST_MESS"] = "Prid�ti naudotojus per�i�r�ti grafikus vienu metu";
$MESS["EC_RESERVE_FOR_EVENT"] = "Pos�d�i� sal�s rezervavimas";
$MESS["EC_MR_RESERVE_ERR_BUSY"] = "Pasirinkta susitikim� sal� negali b�ti rezervuota nurodytam laikui.";
$MESS["EC_MR_RESERVE_ERR"] = "�vyko klaida rezervuojant susitikim� sal�.";
$MESS["EC_BUT_SET"] = "Nustatymai";
$MESS["EC_ADV_MEETING_CAL"] = "Kvietim� kalendorius";
$MESS["EC_FIRST_IN_LIST"] = "Pirmasis s�ra�e";
$MESS["EC_BLINK_SET"] = "Pa�ym�kite nepatvirtintus �vykius";
$MESS["EC_ACC_EX"] = "Prid�ti nedalyvavimo diagram�";
$MESS["EC_OUTL_BAN_ACT"] = "Sujungti";
$MESS["EC_OUTL_BAN_SEL_CAL"] = "Pasirinkti kalendori�";
$MESS["EC_OUTL_BAN_CLOSE"] = "U�daryti ir neberodyti";
$MESS["EC_DEF_MEETING_NAME"] = "Susitikimas";
$MESS["EC_NO_GUESTS_ERR"] = "Nepasirinkta ne vieno sve�io.";
$MESS["EC_NO_FROM_TO_ERR"] = "Tr�ksta ��vykio prad�ios ir pabaigos laiko.";
$MESS["EC_JS_DEL_MEETING_CONFIRM"] = "J�s ruo�iat�s pa�alinti �vyk�, kuriame yra kit� dalyvi�. Ar tikrai norite pa�alinti �� �vyk�?";
$MESS["EC_JS_DEL_MEETING_GUEST_CONFIRM"] = "Pa�alinus �� �vyk� bus automati�kai at�auktas j�s� dalyvavimas. Ar tikrai norite pa�alinti �� �vyk�?";
$MESS["EC_MR_EXPIRE_ERR_BUSY"] = "Pasirinkta vaizdo susitikim� sal� negali b�ti rezervuota praeityje.";
$MESS["EC_MR_MAX_USERS_ERR_BUSY"] = "Vaizdo pokalbi� sal�je gali b�ti ne daugiau kaip #max_users# vartotoj�.";
$MESS["EC_BAN_CONNECT_OUTL"] = "Prisijungti prie \"Outlook\"";
$MESS["EC_BAN_CONNECT_MOBI"] = "Mobilus ry�ys";
$MESS["EC_BAN_CONNECT_EXCH"] = "Prijungta prie Exchange";
$MESS["EC_BAN_CONNECT_EXCH_TITLE"] = "J�s� kalendoriai yra prijungti prie \"Microsoft Exchange Server\" ir sinchronizuoti automati�kai.";
$MESS["EC_BAN_NOT_CONNECT_EXCH"] = "N�ra ry�io su Exchange";
$MESS["EC_BAN_NOT_CONNECT_EXCH_TITLE"] = "N�ra ry�io su \"Microsoft Exchange Server\". Pra�ome susisiekti su sistemos administratoriumi.";
$MESS["EC_MANAGE_CALDAV"] = "Konfig�ruoti i�orinius kalendorius (CalDAV)";
$MESS["EC_MANAGE_CALDAV_TITLE"] = "Valdyti i�orinius kalendorius (Google tt)";
$MESS["EC_CALDAV_TITLE"] = "I�orinio kalendoriaus ry�iai";
$MESS["EC_CALDAV_CONNECTION_ERROR"] = "CalDav serveris gr��ino klaid� \"#ERROR_STR#\" tikrinant ry�� \"#CONNECTION_NAME#\". Pra�ome patikrinti ry�io parametrus ir bandyti dar kart�.";
$MESS["EC_ADD_CALDAV_LINK"] = "Adresas";
$MESS["EC_ADD_CALDAV_USER_NAME"] = "Naudotojo vardas";
$MESS["EC_ADD_CALDAV_PASS"] = "Slapta�odis";
$MESS["EC_ADD_CALDAV_NAME"] = "Vardas";
$MESS["EC_CALDAV_EDIT"] = "keisti";
$MESS["EC_CALDAV_DEL"] = "�alinti";
$MESS["EC_CALDAV_COLLAPSE"] = "sl�pti";
$MESS["EC_CALDAV_RESTORE"] = "rodyti";
$MESS["EC_NEW_EX_CAL"] = "Mano I�orinis Kalendorius";
$MESS["EC_ADD_CALDAV"] = "Prid�ti I�orin� kalendoriu";
$MESS["EC_CALDAV_NO_CHANGE"] = "- palikti kaip yra -";
$MESS["EC_CALDAV_SYNC_OK"] = "Kalendoriai buvo s�kmingai sinchronizuoti.";
$MESS["EC_CALDAV_SYNC_DATE"] = "Sinchronizuotia";
$MESS["EC_CALDAV_SYNC_ERROR"] = "Paskutinis sinchronizavimas nepavyko.";
$MESS["EC_CALDAV_NOTICE"] = "I�oriniai kalendoriai sinchronizuoti automati�kai. <br/> �ra�� pakeitimus, kalendoriaus puslapis bus perkrautas.";
$MESS["EC_CALDAV_NOTICE_GOOGLE"] = "Nor�dami prisijungti <b>Google</b> kalendori�, naudokite nuorod� formatu <span class=\"bxec-link\">https://www.google.com/calendar/dav/YOUREMAIL@DOMAIN.COM/user</span> adreso lauke, ir j�s� prisijungimo vard� ir slapta�od�. <br/> Daugiau informacijos �i�r�kite <a href=\"http://www.google.com/support/calendar/bin/answer.py?answer=99358&&hl=en#ical\"> \"Google\" pagalbos skyriuje </a>.";
$MESS["EC_ALL_CALENDARS"] = "Visi kalendoriai";
$MESS["EC_MOBILE_HELP_TITLE"] = "Mobilieji ry�iai";
$MESS["EC_MOBILE_HELP_HEADER"] = "Prisijungti naudojant CalDav";
$MESS["EC_MOBILE_MAC_OS"] = "iCal for MacOS";
$MESS["EC_MOBILE_APPLE"] = "Apple prietaisai: iPhone, iPad";
$MESS["EC_MOBILE_SUNBIRD"] = "Mozilla Thunderbird / Sunbird, Android programos ";
$MESS["EC_CALENDAR_TO_EXCH"] = "Sinchronizuoti kalendori� su \"Microsoft Exchange\"";
$MESS["EC_MOBILE_HELP_IPHONE_ALL_HELP"] = "<p> Nor�dami nustatyti savo Apple �rengin� kad palaikyt� CalDAV: </p>
<ol>
<li> Spustel�kite <b>Nustatymai</b> ir pasirinkite <b>Pa�tas, Kontaktai, Kalendoriai> Paskyros </b>. </li>
<li> Spustel�kite <b> Prid�ti paskyr� </b>.</li>
<li> Pasirinkite <b> Kita </b> &gt; <b>Prid�ti CalDAV paskyr�</b>.</li>
<li> Nurodyti svetain�s adres� kaip server� (<span class=\"bxec-link\">#CALENDAR_LINK#</span>).Naudokite savo prisijungimo vard� ir slapta�od�.</li>
#POINT_SET_PORT#
</ ol> <p> J�s� kalendoriai pasirodys \"Kalendoriaus\" programoje. </ p>";
$MESS["EC_MOBILE_HELP_MAC_1"] = "<p>Konfig�ruoti iCal j�s� kalendoriui naudoti:</p>
<ol>
		<li>Pasirinkite \"iCal\" > \"Nustatymai\" meniu. Pasirinkite Paskyros.</li>
		<li>Pasirinkite \"+\" , nor�dami sukurti nauj� paskyr�.</li>
		<li>Pasirinkite CalDAV kaip paskyros tipas.</li>
		<li>Paskyros parametruose naudokite ��ios svetain�s URL (<span class=\"bxec-link\">#CALENDAR_LINK#</span>) kaip serverio adres�. �veskite savo prisijungimo vard� ir slapta�od�.</li>
		#POINT_SET_PORT#
</ol>
<p>J�s� kalendorius pasirodys j�s� iCal.</p>";
$MESS["EC_MOBILE_HELP_IPHONE_ONE_HELP"] = "<p>Nor�dami �d�ti pasirinkt� kalendori� � savo Apple �rengin�:</p>
<ol>
<li>Spustel�kite <b>Nustatymai</b> ir pasirinkite <b>Pa�tas, Kontaktai, Kalendoriai> Paskyros</b>.</li>
<li>Spustel�kite <b>Prid�ti paskyr�</b>.</li>
<li>Pasirinkite <b>Kita</b> &gt; <b>Prid�ti CalDAV paskyr�</b>.</li>
<li>Nurodykite tinklalapio adres� kaip server� (<span class=\"bxec-link\">#CALENDAR_LINK#</span>). Naudokite savo prisijungimo vard� ir slapta�od�.</li>
<li>Naudokite pagrindin� autorizacij�.</li>
#POINT_SET_PORT#
</ol>";
$MESS["EC_MOBILE_HELP_SUNBIRD_ALL_HELP"] = "<p>Deja, \"Mozilla Thunderbird\" ir dauguma \"Android\" program� negali sujungti visus kalendorius i�kart.</p>";
$MESS["EC_MOBILE_HELP_SUNBIRD_ONE_HELP"] = "<p>Norint sukonfig�ruoti Mozilla Thunderbird naudoti CalDAV:</p>
<ol>
<li>Paleiskite Thunderbird. �sitikinkite, kad �dieg�te Lightning pried� (Mozilla Calendar Project).</li>
<li>Pasirinkite <b>File &gt; Create &gt; Calendar</b> (arba <b>File &gt; Calendar</b>).</li>
<li>Pasirinkite <b>On the Network</b> ir paspauskite <b>Next</b>.</li>
<li>Pasirinkite <b>CalDAV</b> format�.</li>
<li>Laukelyje <b>Location</b>, �veskite <span class=\"bxec-link\">#CALENDAR_LINK#</span> ir paspauskite <b>Next</b>.</li>
<li>U�vadinkite savo kalendori� ir pasirinkite jam spalv�.</li>
<li>I��okan�iame lange �veskite savo naudotojo vard� ir slapta�od�.</li>
</ol>";
$MESS["EC_SET_PORT"] = "<li>Nor�dami nurodyti prievado numer�, i�saugokite paskyr� ir atidarykte j� dar kart� redaguoti.</li>";
$MESS["EC_SET_PORT_BX24"] = "<li>Kai i�saugojote �ra��, atidarykite j� redaguoti dar kart� ir nustatykite prievado numer�: <b>1443</b>.</li>";
$MESS["EC_EVENT_NOT_FOUND"] = "Klaida! Elementas nerastas.";
$MESS["EC_EVENT_ERROR_DEL"] = "Klaida �alinant element�.";
$MESS["DEL_CON_CALENDARS"] = "�alinti kalendorius";
$MESS["EC_CLOSE_BANNER_NOTIFY"] = "J�s galite atkurti juost� atstatydami naudotoj� nuostatas asmeniniame kalendoriuje.";
$MESS["EC_CALDAV_URL_ERROR"] = "CalDAV ry�io parametrai yra neteisingi";
$MESS["EC_BAN_EXCH_SYNC"] = "Sinchronizuoti";
$MESS["EC_BAN_EXCH_SYNC_TITLE"] = "Nors Exchange sinchronizacija atsiranda automati�kai, j�s galite tai padaryti rankiniu b�du.";
$MESS["EC_BAN_EXCH_NO_SYNC"] = "Sinchronizacijos nereikia. J�s� kalendoriai ir �vykiai sutampa su �vykiais \"Microsoft Exchange\".";
$MESS["EC_NONAME_EVENT"] = "�vykis";
$MESS["EC_NEW_EVENT"] = "naujas �vykis";
$MESS["EC_NEW_TASK"] = "nauja u�duotis";
$MESS["EC_NEW_TASK_TITLE"] = "Sukurti nauj� u�duot�";
$MESS["EC_NEW_SECT"] = "naujas kalendorius";
$MESS["EC_NEW_SECT_TITLE"] = "Sukurti nauj� kalendori�";
$MESS["EC_NEW_EV_PL"] = "�vykis naudojant �vyki� planuokl�";
$MESS["EC_JS_ADD_NEW_EVENT_PL"] = "Prid�ti nauj� �vyk� naudojant planuokl�";
$MESS["EC_NEW_EX_SECT"] = "i�orinis kalendorius (CalDav)";
$MESS["EC_NEW_EX_SECT_TITLE"] = "Prisijungti prie i�orinio kalendoriaus per CalDav arba konfig�ruoti esamus kalendorius";
$MESS["EC_SECT_BASE_TAB"] = "Pagrindiniai parametrai";
$MESS["EC_SECT_ACCESS_TAB"] = "Prieigos teises";
$MESS["EC_SET_TAB_BASE"] = "Bendri";
$MESS["EC_SET_TAB_BASE_TITLE"] = "Bendri kalendoriaus parametrai";
$MESS["EC_SET_TAB_PERSONAL"] = "Asmeninis";
$MESS["EC_SET_TAB_PERSONAL_TITLE"] = "Asmeninio kalendoriaus parametrai";
$MESS["EC_CLEAR"] = "I�valyti";
$MESS["EC_CLEAR_SET_CONFIRM"] = "Ar tikrai norite pa�alinti j�s� asmenines nuostatas?";
$MESS["EC_MANAGE_SP_CALENDARS"] = "Konfig�ruoti m�gstamiausius kalendorius";
$MESS["EC_MANAGE_SP_CALENDARS_TITLE"] = "Turimi kalendoriai";
$MESS["EC_TASKS_VIEW"] = "Per�i�r�ti u�duot�";
$MESS["EC_TASKS_EDIT"] = "Per�i�r�ti u�duot�";
$MESS["EC_MY_TASKS"] = "Mano u�duotys";
$MESS["EC_FIRST_WEEKDAY"] = "Pirma savait�s diena";
$MESS["EC_WEEK_HOLIDAYS"] = "Savaitgaliai";
$MESS["EC_YEAR_HOLIDAYS"] = "Savaitgaliais ir �ven�i� dienos";
$MESS["EC_YEAR_WORKDAYS"] = "Darbo dienos";
$MESS["EC_NO_ACCESS_RIGHTS"] = "nenurodyta";
$MESS["EC_MANAGE_SETTING"] = "Visi nustatymai";
$MESS["EC_MANAGE_SETTING_TITLE"] = "Redaguoti modulio nustatymus Valdymo skydelyje";
$MESS["EC_MANAGE_CALENDAR_TYPES"] = "Redaguoti kalendori� tipus";
$MESS["EC_MANAGE_CALENDAR_TYPES_TITLE"] = "Redaguoti kalendori� tipus modulio nustatymuose";
$MESS["EC_WORK_TIME"] = "Darbo laiko nustatymai";
$MESS["EC_WEEK_START"] = "Pirma savait�s diena";
$MESS["EC_CLEAR_PERS_SETTINGS"] = "Atstatyti asmeninius nustatymus";
$MESS["EC_SHOW_BANNER"] = "Rodyti I�orinio sujungimo juost�";
$MESS["EC_ADD_GUESTS_DEF"] = "Pasirinkite i� strukt�ros";
$MESS["EC_ADD_GUESTS_EMAIL"] = "Pakviesti naudojant elektronin� pa�t�";
$MESS["EC_ADD_ATTENDEES"] = "Prid�ti dalyvius";
$MESS["EC_USER_EMAIL"] = "Naudotojo elektroninis pa�tas";
$MESS["EC_OPTION_SHOW_DECLINED"] = "Rodyti j�s� atmestus �vykius";
$MESS["EC_OPTION_SHOW_MUTED"] = "Rodyti ankstesnius �vykius pilkais";
$MESS["EC_EDIT_MEETING_NOTE"] = "�ie poky�iai yra susij� tik su j�s� dalyvavimu, jie netur�s �takos pokalbio parametrams ar kit� asmen� nuostatoms.";
$MESS["EC_IT_IS_YOU"] = "tai jus";
$MESS["EC_PAST_MEETING_NOTICE"] = "Buv�s �vykis negali b�ti sukurtas praeityje.";
$MESS["EC_NO_EXCHANGE_SERVER"] = "Klaida jungiantis prie Exchange serverio.";
$MESS["EC_NO_CAL_DAV"] = "Klaida jungiantis prie CalDav  serverio.";
$MESS["EC_FROM_HR"] = "i� nedalyvavimo diagramos";
$MESS["EC_PRIVATE_NOTICE"] = "I�samesn� informacija apie �vyk� prieinama tik jums";
$MESS["EC_TEXT_COLOR"] = "Teksto spalva";
$MESS["EC_DEFAULT_COLOR"] = "Numatytasis";
$MESS["EC_NO_CALENDARS_ALERT"] = "Nepavyko prid�ti nauj� �vyk�. Pra�ome sukurti kalendori� arba atnaujinti puslap� sukuriant numatyt�j� nauj� kalendori�.";
$MESS["EC_MR_CHECK_PERIOD_WARN"] = "Nepavyko prid�ti nauj� �vyk�.Nuolatinis �vykis negali b�ti naudojamas rezervuoti pokalbi� kambar�.";
$MESS["EC_CAL_DAV_CON_WAIT"] = "Pra�ome palaukti, kol apdorojamas ankstesnis pra�ymas.";
$MESS["EC_CAL_DAV_REFRESH"] = "Atnaujinti";
$MESS["EC_DEFAULT_EVENT_NAME"] = "Mano �vykis";
$MESS["EC_ATTENDEE_MORE_1"] = "ir dar #NUM# dalyvis";
$MESS["EC_ATTENDEE_MORE_2"] = "ir dar #NUM# dalyviai";
$MESS["EC_ATTENDEE_MORE_3"] = "ir dar #NUM# dalyviai";
$MESS["EC_ATTENDEE_MORE_4"] = "ir dar #NUM# dalyviai";
$MESS["EC_ATTENDEE_MORE_5"] = "ir dar #NUM# dalyviai";
$MESS["EC_ATTENDEE_MORE_6"] = "ir dar #NUM# dalyviai";
$MESS["EC_ATTENDEE_MORE_7"] = "ir dar #NUM# dalyviai";
$MESS["EC_ATTENDEE_MORE_8"] = "ir dar #NUM# dalyviai";
$MESS["EC_ATTENDEE_MORE_9"] = "ir dar #NUM# dalyviai";
$MESS["EC_ATTENDEE_MORE_0"] = "ir dar #NUM# dalyvi�";
$MESS["EC_COMMENTS"] = "Komentarai";
$MESS["MPF_DESTINATION_3"] = "Visiems darbuotojams";
$MESS["MPF_DESTINATION_4"] = "Visiems naudotojams";
$MESS["EC_DATE"] = "Data";
$MESS["EC_VIEW_FULL_DAY"] = "visa dien�";
$MESS["EC_VIEW_DATE_FROM_TO"] = "Nuo #DATE_FROM# iki #DATE_TO#";
$MESS["EC_VIEW_TIME_FROM_TO_TIME"] = "nuo #TIME_FROM# iki #TIME_TO#";
$MESS["EC_DESTINATION_1"] = "Prid�ti asmenis, grupes ar skyrius";
$MESS["EC_DESTINATION_2"] = "Prid�ti daugiau";
$MESS["EC_MPF_FILE_INSERT_IN_TEXT"] = "�d�ti <br /> � tekst�";
$MESS["EC_MPF_INSERT_FILE"] = "Spauskite nor�dami �terpti fail�";
$MESS["EC_MPF_IMAGE_TITLE"] = "�terpti nuotrauk�";
$MESS["EC_MPF_FILE_IN_TEXT"] = "Tekste";
$MESS["EC_MPF_IMAGE_LINK"] = "�terpti nuorod� � nuotrauk�";
$MESS["EC_FPF_VIDEO"] = "�terpti vaizdo fail�";
$MESS["EC_BPC_VIDEO_P"] = "Vaizdo failo adresas";
$MESS["EC_BPC_VIDEO_PATH_EXAMPLE"] = "Pavyzdys: <i>http://www.youtube.com/watch?v=vPpPEWPVN5Y</i> <br/> or <i>www.mysite.com/video/my_video.mp4</i>";
$MESS["EC_MPF_VIDEO_SIZE"] = "Dydis (W / H)";
$MESS["EC_LF_ADD_COMMENT_SOURCE_ERROR"] = "Nepavyko prid�ti komentar� prie �vykio �altinio.";
$MESS["EC_ATTENDEE_1"] = "#NUM# dakyvis";
$MESS["EC_ATTENDEE_2"] = "#NUM# dalyviai";
$MESS["EC_ATTENDEE_3"] = "#NUM# dalyviai";
$MESS["EC_ATTENDEE_4"] = "#NUM# dalyviai";
$MESS["EC_ATTENDEE_5"] = "#NUM# dalyviai";
$MESS["EC_ATTENDEE_6"] = "#NUM# dalyviai";
$MESS["EC_ATTENDEE_7"] = "#NUM# dalyviai";
$MESS["EC_ATTENDEE_8"] = "#NUM# dalyviai";
$MESS["EC_ATTENDEE_9"] = "#NUM# dalyviai";
$MESS["EC_ATTENDEE_0"] = "#NUM# dalyviai";
$MESS["EC_DD_DENY_REPEATED"] = "Negalima vilkti ir �terpti pasikartojan�ius �vykius.";
$MESS["EC_DD_DENY_TASK"] = "Negalima vilkti ir �terpti u�duot�.";
$MESS["EC_DD_DENY_EVENT"] = "Negalima vilkti ir �terpti u�duot�.";
$MESS["EC_JS__J"] = " ";
$MESS["EC_JS__U"] = " ";
$MESS["EC_MOBILE_HELP_MAC"] = "<p>Nor�dami prid�ti kalendori� prie j�s� iCal:</p>
<ol>
<li>Atidarykite iCal -> Nustatymai ir pasirinkite Paskyros.</li>
<li>Prid�kite nauj� paskyr� su pliuso mygtuku apa�ioje.</li>
<li>Pasirinkite CalDAV paskyros tip�.</li>
<li>Paskyros parametruose naudokite �ios svetain�s URL (<span class=\"bxec-link\">#CALENDAR_LINK#</span>) kaip serverio adres�. �veskite savo prisijungimo vard� ir slapta�od�.</li>
#POINT_SET_PORT#
</ol>
<p>J�s� kalendorius pasirodys j�s� iCal.</p>";
$MESS["EC_ATT_AGR"] = "patvirtinta";
$MESS["EC_ATT_DEC"] = "atsisakyta";
$MESS["EC_DECLINE_NOTICE"] = "Kai langas yra u�darytas, �vykis nebus matomas j�s� asmeniniame kalendoriuje.";
$MESS["EC_EDEV_FOR"] = "";
?>