<?
$MESS["MOD_BACK"] = "Atgal � modulius";
$MESS["TYPE_ID"] = "Simbolinis kodas";
$MESS["TYPE_TITLE"] = "Pavadinimas";
$MESS["TYPE_CONVERT_IT"] = "Konvertuoti �� tip�";
$MESS["CAL_SETTINGS"] = "Nustatymai";
$MESS["CAL_PRODUCT_SETTINGS"] = "Sistemos nustatymai";
$MESS["CAL_MODULES"] = "Moduliai";
$MESS["CAL_CONVERT"] = "Kovertuoti kalendorius";
$MESS["CAL_CONVERT_IBLOCK_LIST"] = "Informacijos blokai bus sukonvertuoti � kalendoriaus tipo blokus";
$MESS["CAL_CONVERT_START"] = "Konvertuoti";
$MESS["CAL_CONVERT_PROCESSING"] = "�iuo metu konvertuojama...";
$MESS["CAL_CONVERT_PROCESSING_NOTICE"] = "D�mesio! Negalima nutraukti konvertavimo proces� arba u�daryti nar�ykl�s lang�, kitaip gausite netinkamus konvertavimo rezultatus.";
$MESS["CAL_CONVERT_FROM"] = "nuo";
$MESS["CAL_CONVERT_USERS"] = "Naudotojo kalendoriai";
$MESS["CAL_CONVERT_GROUPS"] = "Grup�s kalendoriai";
$MESS["CAL_CONVERT_STAGE_PREPARE"] = "konvertacija ruo�iama";
$MESS["CAL_CONVERT_STAGE_SAVE"] = "saugomi parametrai";
$MESS["CAL_CONVERT_STAGE_CREATE_TYPES"] = "sukuriami kalendoriaus tipai";
$MESS["CAL_CONVERT_STAGE_USER_CALS"] = "konvertuoti  #USER_NAME# kalendorius: (kalendoriai: #SECT_COUNT#, �vykiai: #EVENTS_COUNT#)";
$MESS["CAL_CONVERT_STAGE_GROUP_CALS"] = "konvertuoti  \"#GROUP_NAME# \" kalendorius: (kalendoriai: #SECT_COUNT#, kalendoriai: #EVENTS_COUNT#)";
$MESS["CAL_CONVERT_STAGE_TYPE"] = "konvertuoti kalendorius i� \"#TYPE_NAME# \": (kalendoriai: #SECT_COUNT#, kalendoriai: #EVENTS_COUNT#)";
$MESS["CAL_CONVERT_SET"] = "Kalendoriaus modulio parametrai";
$MESS["CAL_CONVERT_PROCES"] = "Konvertavimo procesas";
$MESS["CAL_GO_TO_SETTINGS"] = "Atversti modulio nustatymus";
$MESS["CAL_CONVERT_SUCCESS"] = "Konvertavimas baigtas.";
$MESS["CAL_NOT_1"] = "Patikrinkite informacijos blokus, kuriuos norite konvertuoti � kalendori� tipus. Jei reikia, galite pakeisti pasirinkt� tip� pavadinimus ir/ar simbolinius kodus.";
$MESS["CAL_NOT_2"] = "
Patikrinkite kalendoriaus modulio si�lomus nustatymus.";
$MESS["CAL_NOT_3"] = "Paspauskite mygtuk� \"Konvertuoti\".";
$MESS["CAL_NOT_4"] = "Kai konversijos procesas yra baigtas, atidarykite �vyki� kalendoriaus modulio nustatym� puslap� ir �sitikinkite, kad kalendoriaus tipai yra teisingi ir turi atitinkamas prieigos teises.";
$MESS["CAL_PARSE_CONTENT"] = "Analizuoti vie�uosius failus";
$MESS["CAL_NOT_PARSE"] = "Svetain�s vie�ieji failai turi b�ti nagrin�jami siekiant gauti teisingus modulio parametrus.";
$MESS["CAL_PROC_WAIT"] = "Analizuojami vie�ieji failai";
?>