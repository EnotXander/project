<?
$MESS["TASK_NAME_CALENDAR_DENIED"] = "Prieiga u�drausta";
$MESS["TASK_NAME_CALENDAR_VIEW_TIME"] = "Per�i�r�ti prieinamum�";
$MESS["TASK_NAME_CALENDAR_VIEW_TITLE"] = "Per�i�r�ti prieinamum� ir pavadinimus";
$MESS["TASK_NAME_CALENDAR_VIEW"] = "Per�i�ra";
$MESS["TASK_NAME_CALENDAR_EDIT"] = "Redaguoti kalendorius ir �vykius";
$MESS["TASK_NAME_CALENDAR_ACCESS"] = "Pilna prieiga";
$MESS["TASK_NAME_CALENDAR_TYPE_DENIED"] = "Prieiga u�drausta";
$MESS["TASK_NAME_CALENDAR_TYPE_VIEW"] = "Per�i�ra";
$MESS["TASK_NAME_CALENDAR_TYPE_EDIT"] = "Redaguoti kalendorius ir �vykius";
$MESS["TASK_NAME_CALENDAR_TYPE_ACCESS"] = "Pilna prieiga";
$MESS["TASK_BINDING_CALENDAR_SECTION"] = "Kalendoriaus skyrius";
$MESS["TASK_BINDING_CALENDAR_TYPE"] = "Kalendoriaus tipas";
?>