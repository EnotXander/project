<?
$MESS["CAL_REST_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CAL_REST_SECTION_NEW_ERROR"] = "Error durante la creaci�n de la secci�n";
$MESS["CAL_REST_SECTION_SAVE_ERROR"] = "Error durante la edici�n de la secci�n";
$MESS["CAL_REST_SECTION_DELETE_ERROR"] = "Error al eliminar la secci�n";
$MESS["CAL_REST_SECT_ID_EXCEPTION"] = "ID de la secci�n no especificada";
$MESS["CAL_REST_EVENT_ID_EXCEPTION"] = "ID del evento es especificado";
$MESS["CAL_REST_EVENT_DELETE_ERROR"] = "Error al eliminar evento";
$MESS["CAL_REST_EVENT_NEW_ERROR"] = "Error durante la creaci�n del evento";
$MESS["CAL_REST_EVENT_UPDATE_ERROR"] = "Error durante la edici�n del evento";
$MESS["CAL_REST_PARAM_EXCEPTION"] = "El par�metro obligatorio \"#PARAM_NAME#\" no se ha especificado para el m�todo \"#REST_METHOD#\"";
$MESS["CAL_REST_SECTION_ERROR"] = "Se ha especificado un ID de secci�n de calendario no v�lido o el usuario actual no tiene acceso a �l.";
$MESS["CAL_REST_PARAM_ERROR"] = "Valor de par�metro no v�lido para \"#PARAM_NAME#\"";
$MESS["CAL_REST_GET_STATUS_ERROR"] = "Error al recuperar el estado";
$MESS["CAL_REST_GET_DATA_ERROR"] = "Error al recuperar datos";
?>