<?
$MESS["SEO_TOOLS_ERROR_FILE_NOT_FOUND"] = "Archivo no fue encontrado";
$MESS["SEO_TOOLS_TAB_PAGE"] = "P�gina";
$MESS["SEO_TOOLS_TAB_PAGE_TITLE"] = "Estad�stica de p�ginas";
$MESS["SEO_TOOLS_TAB_EDIT"] = "Propiedades";
$MESS["SEO_TOOLS_TAB_EDIT_TITLE"] = "Administrar propiedades de la p�gina";
$MESS["SEO_TOOLS_TAB_INDEX"] = "Indexaci�n";
$MESS["SEO_TOOLS_TAB_INDEX_TITLE"] = "P�gina y sitio de la estad�stica de indexaci�n";
$MESS["SEO_TOOLS_TAB_WORDS"] = "Buscadores";
$MESS["SEO_TOOLS_TAB_WORDS_TITLE"] = "Buscar motores de b�squeda utilizados para encontrar la p�gina/sitio ";
$MESS["SEO_TOOLS_TAB_REFERERS"] = "Sitios de referencia";
$MESS["SEO_TOOLS_TAB_REFERERS_TITLE"] = "Sitios de referencia";
$MESS["SEO_TOOLS_TITLE"] = "Herramientas de la p�gina SEO";
$MESS["SEO_TOOLS_COUNTERS"] = "Contadores";
$MESS["SEO_TOOLS_LOADING"] = "Cargando informaci�n de la p�gina...";
$MESS["SEO_TOOLS_STATS"] = "Estad�sticas de la p�gina";
$MESS["SEO_TOOLS_ANALYSIS"] = "An�lisis de la p�gina";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS"] = "Promoci�n de palabras";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_TITLE"] = "Palabras clave para esta p�gina";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_WORD"] = "palabra";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_ALL_CONTRAST"] = "todo/densidad";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_HEADERS"] = "ventana/t�tulo de la p�gina";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_BOLD_ITALIC"] = "&lt;b&gt;/&lt;i&gt;";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_DESCR_KEYWORDS"] = "descripci�n/palabras clave";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_LINKS"] = "v�nculos/ext.";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_HINT"] = "Promoci�n de palabras claves - palabras clave para la colocaci�n del motor de b�squeda y posicionamiento. En la pesta�a, ver� Promoci�n de palabras clave que debe llevar a los usuarios a esta p�gina de los motores de b�squeda. Esta pesta�a muestra de an�lisis de Promoci�n Palabras clave utilizadas en el contenido de la p�gina.";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_RELOAD"] = "Actualizar";
$MESS["SEO_TOOLS_INTERNAL_KEYWORDS_RELOAD_TITLE"] = "Actualizar estad�sticas de Promoci�n Palabras clave ";
$MESS["SEO_TOOL_TAB_local"] = "Palabras de la p�gina";
$MESS["SEO_TOOL_TAB_section"] = "Secci�n de palabras";
$MESS["SEO_PAGE_PROPERTY_WINDOW_TITLE"] = "T�tulo de la ventana del navegador";
$MESS["SEO_PAGE_STAT_URL"] = "URL de la p�gina";
$MESS["SEO_PAGE_STAT_TOTAL_LENGTH"] = "Longitud (bytes)";
$MESS["SEO_PAGE_STAT_TOTAL_WORDS_COUNT"] = "Contador de palabras";
$MESS["SEO_PAGE_STAT_UNIQUE_WORDS_COUNT"] = "Palabras solas";
$MESS["SEO_PAGE_STAT_META_DESCRIPTION"] = "Descripci�n de etiquetas META";
$MESS["SEO_PAGE_STAT_META_KEYWORDS"] = "Etiqueta de Palabras clave META ";
$MESS["SEO_PAGE_RECOMMEMDATIONS"] = "Recomendaci�n";
$MESS["SEO_PAGE_BASE_TITLE"] = "T�tulo de la p�gina base";
$MESS["SEO_PAGE_CURRENT_TITLE"] = "T�tulo actual";
$MESS["SEO_PAGE_WINDOW_TITLE_CURRENT"] = "actual";
$MESS["SEO_PAGE_CURRENT_TITLE_EDIT"] = "Editar";
$MESS["SEO_PAGE_TITLE_EDIT"] = "Editar";
$MESS["SEO_PAGE_ERROR_NO_STATS"] = "Esta caracter�stica requiere el m�dulo de An�lisis de Web.";
$MESS["SEO_PAGE_ERROR_NO_STATS_RIGHTS"] = "No tiene suficientes permisos para acceder al m�dulo de An�lisis web.";
$MESS["SEO_PAGE_ERROR_NO_SEARCHERS"] = "Para usar esta caracter�tica, especificar los motores de b�squeda que podr� usar en el m�dulo <a href=\"/bitrix/admin/settings.php?mid=seo&tabControl_active_tab=edit3\">SEO module settings</a>.";
$MESS["SEO_PAGE_STATS_INDEX"] = "Hits de motores de b�squeda para #COUNT# de d�as";
$MESS["SEO_PAGE_STATS_ERROR_NO_DATA"] = " ";
$MESS["SEO_PAGE_STATS_SITE_INDEX"] = "Resumen de la indexaci�n del sitio";
$MESS["SEO_PAGE_PHRASES_SEARCHER"] = "motores de b�squeda";
$MESS["SEO_PAGE_PHRASES_HITS"] = "hits";
$MESS["SEO_PAGE_PHRASES_LAST_HIT"] = "�ltimo hit";
$MESS["SEO_PAGE_GOTO_CP"] = "Ver en el panel de control";
$MESS["SEO_PAGE_PHRASES_ERROR_NO_DATA"] = "Ning�n hit de p�gina est� disponible.";
$MESS["SEO_PAGE_REFERERS_ERROR_NO_DATA"] = "No hay datos de referencia disponibles para esta p�gina.";
$MESS["SEO_PAGE_STATS_SITE_INDEX_ERROR_NO_DATA"] = "No hay datos para crear el motor de b�squeda rastree gr�fico.


Sugiere una traducci�n mejor
Gracias por proponer una traducci�n al Traductor de Google.
Sugiere una traducci�n mejor:
No hay datos para crear el motor de b�squeda rastree gr�fico.

Idiomas disponibles para traducci�n:

afrikaans
alban�s
alem�n
�rabe
bielorruso
b�lgaro
catal�n
checo
chino coreano
criollo haitiano
croata
dan�s
eslovaco
esloveno
espa�ol
estonio
finland�s franc�s
gal�s
gallego
griego
hebreo
hindi
holand�s
h�ngaro
indonesio ingl�s
irland�s
island�s
italiano
japon�s
let�n
lituano
macedonio
malayo malt�s
noruego
persa
polaco
portugu�s
rumano
ruso
serbio
suajili sueco
tagalo
tailand�s
turco
ucraniano
vietnamita
No hay datos para crear el motor de b�squeda que rastree el gr�fico.";
$MESS["SEO_PAGE_REFERERS_LINK"] = "link";
$MESS["SEO_PAGE_REFERERS_COUNT"] = "hits";
$MESS["SEO_PAGE_REFERERS_SEO_PAGE_PHRASES_ALT"] = "mostrar informaci�n detallada en solicitudes";
$MESS["SEO_PAGE_REFERERS_SEO_PAGE_REFERERS_ALT"] = "mostrar informaci�n detallada en referencias";
$MESS["SEO_HELP_counters"] = "Contadores de calificaci�n de p�gina. Usted puede agregar contadores de SEO en la configuraci�n del m�dulo.";
$MESS["SEO_HELP_base_title"] = "El t�tulo de p�ginas base. Esto puede ser modificado por los componentes existentes en la p�gina.";
$MESS["SEO_HELP_current_title"] = "T�tulo de la p�gina actual. El texto del t�tulo podr�a ser modificada por alg�n componente(s).";
$MESS["SEO_HELP_property_window_title"] = "T�tulo de la ventana de navegaci�n. Esto puede ser modificado por componentes existentes en la p�gina.";
$MESS["SEO_HELP_property_keywords"] = "Palabras clave de la p�gina. Puede ser heredada de la secci�n parent en cuyo caso la propiedad se guardar� en los par�metros de la p�gina.";
$MESS["SEO_HELP_property_description"] = "La descripci�n de la p�gina. Puede ser heredada de la secci�n parent en cuyo caso la propiedad se guardar� en los par�metros de la p�gina";
$MESS["SEO_PAGE_CONNECTION_ERROR_HINT"] = "Posible raz�n: puerto no se especific� en <a href=\"/bitrix/admin/settings.php?mid=main\">Kernel module settings \"Website URL\"</a>.";
?>