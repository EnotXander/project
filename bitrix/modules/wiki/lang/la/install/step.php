<?
$MESS["WIKI_CREATE_NEW_IBLOCK"] = "Crear block de información";
$MESS["WIKI_CREATE_NEW_SOCNET_IBLOCK"] = "Crear block de información para social network";
$MESS["WIKI_CREATE_NEW_IBLOCK_NAME"] = "Nombre del block de información";
$MESS["WIKI_CREATE_NEW_SOCNET_IBLOCK_NAME"] = "Nombre del block de información de social network";
$MESS["WIKI_CREATE_NEW_IBLOCK_TYPE"] = "Tipo de block de información";
$MESS["WIKI_CREATE_NEW_SOCNET_IBLOCK_TYPE"] = "Tipo de block de información de social network";
$MESS["WIKI_SELECT"] = "Seleccionar";
$MESS["WIKI_CREATE"] = "Crear";
$MESS["WIKI_ID"] = "ID";
$MESS["WIKI_CREATE_NEW_FORUM"] = "Crear formulario";
$MESS["WIKI_CREATE_NEW_SOCNET_FORUM"] = "Crear formulario para social network";
$MESS["WIKI_CREATE_NEW_FORUM_NAME"] = "Nombre del foro";
$MESS["WIKI_CREATE_NEW_SOCNET_FORUM_NAME"] = "Nombre del foro de social network";
?>