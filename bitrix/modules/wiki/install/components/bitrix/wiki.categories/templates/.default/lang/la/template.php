<?
$MESS["SEARCH_GO"] = "Buscar";
$MESS["SEARCH_ERROR"] = "Hay un error en la solicitud de b�squeda:";
$MESS["SEARCH_CORRECT_AND_CONTINUE"] = "Su b�squeda no produjo resultados. Corrija su solicitud e intente nuevamente.";
$MESS["NAV_TITLE"] = "Categor�as";
$MESS["WIKI_NO_CATEGORIES"] = "No hay categor�as de wiki en la actualidad. Las categor�as se crean seg�n sea necesario, primero se especifica la categor�a de una p�gina wiki.";
?>