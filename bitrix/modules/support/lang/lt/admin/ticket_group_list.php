<?
$MESS["SUP_GL_TITLE"] = "Technin�s pagalbos klient� grup�s";
$MESS["SUP_GL_FLT_NAME"] = "Pavadinimas";
$MESS["SUP_GL_PAGES"] = "Grup�s:";
$MESS["SUP_GL_NAME"] = "Pavadinimas";
$MESS["SUP_GL_SORT"] = "R��iavimas";
$MESS["SUP_GL_XML_ID"] = "XML_ID";
$MESS["SUP_GL_EDIT"] = "Redaguoti";
$MESS["SUP_GL_DELETE_CONFIRMATION"] = "�alinti grup�?";
$MESS["SUP_GL_DELETE"] = "Trinti";
$MESS["SUP_GL_ADD"] = "�ra�yti nauj� grup�";
$MESS["SUP_GL_EXACT_MATCH"] = "Ie�koti atitikmenys tiksliau";
$MESS["SUP_GL_USERLIST"] = "Grup�s naudotoj� s�ra�as";
$MESS["SUP_GL_USERADD"] = "Prid�ti naudotoj� prie grup�s";
$MESS["SUP_GL_IS_TEAM_GROUP"] = "Technin�s pagalbos darbuotoj� grup�";
$MESS["SUP_GL_FLT_IS_TEAM_GROUP"] = "Technin�s pagalbos darbuotoj� grup�";
$MESS["SUP_GL_FLT_IS_TEAM_GROUP_CN"] = "Kategorija:";
$MESS["SUP_GL_FLT_SUPPORT"] = "Palaikymo komandos grup�";
$MESS["SUP_GL_FLT_CLIENT"] = "Klient� grup�";
?>