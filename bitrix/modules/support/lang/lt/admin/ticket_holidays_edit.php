<?
$MESS["SUP_ADMIN_TAB1"] = "��imtis";
$MESS["SUP_ADMIN_TITLE"] = "��imtis";
$MESS["SUP_ERROR"] = "Klaidos";
$MESS["SUP_BACK_TO_ADMIN"] = "Gr��ti";
$MESS["SUP_TIMETABLE_NOT_FOUND"] = "I�imtis nerasta.";
$MESS["SUP_CONFIRM_DEL_MESSAGE"] = "Ar tikrai norite pa�alinti �i� i�imt�?";
$MESS["SUP_FORM_SETTINGS"] = "Nustatymai";
$MESS["SUP_FORM_SETTINGS_EX"] = "Nustatymai";
$MESS["SUP_NAME"] = "Pavadinimas";
$MESS["SUP_DESCRIPTION"] = "Apra�ymas";
$MESS["SUP_OPEN_TIME"] = "Veiksmas";
$MESS["SUP_SLA"] = "SLA";
$MESS["SUP_SLA_ID"] = "Taikoma SLA";
$MESS["SUP_OPEN_TIME_HOLIDAY_G"] = "R&R";
$MESS["SUP_OPEN_TIME_WORKDAY_G"] = "Darbas";
$MESS["SUP_OPEN_TIME_HOLIDAY_H"] = "Pertrauka";
$MESS["SUP_OPEN_TIME_HOLIDAY"] = "Bendroji i�eigin�";
$MESS["SUP_OPEN_TIME_WORKDAY_H"] = "Ypatingos darbo valandos";
$MESS["SUP_OPEN_TIME_WORKDAY_0"] = "Darbo diena vietoj pirmadienio";
$MESS["SUP_OPEN_TIME_WORKDAY_1"] = "Darbo diena vietoj antradienio";
$MESS["SUP_OPEN_TIME_WORKDAY_2"] = "Darbo diena vietoj tre�iadienio";
$MESS["SUP_OPEN_TIME_WORKDAY_3"] = "Darbo diena vietoj ketvirtadienio";
$MESS["SUP_OPEN_TIME_WORKDAY_4"] = "Darbo diena vietoj penktadienio";
$MESS["SUP_OPEN_TIME_WORKDAY_5"] = "Darbo diena vietoj �e�tadienio";
$MESS["SUP_OPEN_TIME_WORKDAY_6"] = "Darbo diena vietoj sekmadienio";
$MESS["SUP_ADMIN_USER_FIELDS"] = "Papildomos savyb�s";
?>