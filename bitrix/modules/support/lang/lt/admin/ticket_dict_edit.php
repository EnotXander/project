<?
$MESS['SUP_FORGOT_NAME'] = 'Laukelyje \"Pavadinimas\" �ra�ykite pavadinim�';
$MESS['SUP_EDIT_RECORD'] = '�ra�o redagavimas # #ID#';
$MESS['SUP_NEW_RECORD'] = 'Naujas katalogo �ra�as';
$MESS['SUP_RECORD'] = '�ra�ymas';
$MESS['SUP_RECORD_TITLE'] = '�ra�ymo � katalog� parametrai';
$MESS['SUP_STAT'] = 'Statistika';
$MESS['SUP_STAT_TITLE'] = 'Registracija statistikoje';
$MESS['SUP_RECORDS_LIST'] = 'Katalogas';
$MESS['SUP_SITE'] = 'Svetain�s:';
$MESS['SUP_ERROR'] = 'Klaida i�saugojant �ra��';
$MESS['SUP_TYPE'] = 'Tipas:';
$MESS['SUP_SORT'] = 'R��iavimas:';
$MESS['SUP_NAME'] = 'Pavadinimas:';
$MESS['SUP_DESCRIPTION'] = 'Apra�ymas:';
$MESS['SUP_RESPONSIBLE'] = 'Atsakingas:';
$MESS['SUP_EVENT_PARAMS'] = 'Kaip registruoti nauj� technin�s pagalbos u�klausim� statistikos modulyje';
$MESS['SUP_EVENT12'] = 'event1, event2 - parametrai identifikuojantys �vykio tip�';
$MESS['SUP_EVENT3'] = 'event3 - papildomas parametras';
$MESS['SUP_INCORRECT_SID'] = 'Neteisingas simbolinis kodas (galima naudoti tik lotini�kas raid�s, skai�iai bei simbol� \"_\")';
$MESS['SUP_SID'] = 'Mnemoninis kodas:';
$MESS['SUP_SID_ALREADY_IN_USE'] = '�odyno tipui \"#TYPE#\" bei kalbai \"#LANG#\" nurodytas simbolinis kodas jau naudojamas �ra�e # #RECORD_ID#.';
$MESS['SUP_BY_DEFAULT'] = 'K�riant nauj� u�klausim� svetain�je, i�rinkin�ti pagal nutyl�jim� i� i�skleid�iamo s�ra�o:';
$MESS['SUP_CREATE_NEW_RECORD'] = 'Suk�rti nauj� �ra��';
$MESS['SUP_DELETE_RECORD'] = '�alinti �ra��';
$MESS['SUP_DELETE_RECORD_CONFIRM'] = 'Ar tikrai norite pa�alinti �� katalogo �ra��?';
?>