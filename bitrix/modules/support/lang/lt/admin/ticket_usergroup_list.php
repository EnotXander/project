<?
$MESS['SUP_UGL_PAGES'] = 'Naudotojai';
$MESS['SUP_UGL_GROUP_ID'] = 'Grup�s ID';
$MESS['SUP_UGL_USER_ID'] = 'Naudotojo ID';
$MESS['SUP_UGL_CAN_VIEW_GROUP_MESSAGES'] = 'Naudotojas gali matyti grup�s u�klausimus';
$MESS['SUP_UGL_GROUP_NAME'] = 'Grup�s pavadinimas';
$MESS['SUP_UGL_LOGIN'] = 'Prisijungimo vardas ';
$MESS['SUP_UGL_FIRST_NAME'] = 'Vardas';
$MESS['SUP_UGL_LAST_NAME'] = 'Pavard�';
$MESS['SUP_UGL_EDIT'] = 'Redaguoti';
$MESS['SUP_UGL_ADD'] = 'Prid�ti';
$MESS['SUP_UGL_ADD_TITLE'] = 'Prid�ti';
$MESS['SUP_UGL_TITLE'] = 'Naudotojai ir grup�s';
$MESS['SUP_UGL_DELETE'] = 'Trinti';
$MESS['SUP_UGL_DELETE_CONFIRMATION'] = 'Tikrai �alinti?';
$MESS['SUP_UGL_FLT_GROUP'] = 'Grup�';
$MESS['SUP_UGL_FLT_USER'] = 'Naudotojas';
$MESS['SUP_UGL_FLT_LOGIN'] = 'Prisijungimo vardas ';
$MESS['SUP_UGL_FLT_CAN_VIEW_GROUP_MESSAGES'] = 'Naudotojas gali skaityti grup�s u�klausas';
$MESS['SUP_UGL_EXACT_MATCH'] = 'Ie�koti atitikmenys tiksliau';
$MESS['SUP_UGL_FLT_ALL_GROUPS'] = 'visos grup�s';
$MESS['SUP_UGL_IS_TEAM_GROUP'] = 'Technin�s pagalbos darbuotoj� grup�';
$MESS['SUP_UGL_FLT_IS_TEAM_GROUP'] = 'Technin�s pagalbos darbuotoj� grup�';
?>