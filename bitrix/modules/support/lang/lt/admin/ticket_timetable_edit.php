<?
$MESS["SUP_ADMIN_TAB1"] = "Tvarkara�tis";
$MESS["SUP_ADMIN_TAB2"] = "Darbo valandos";
$MESS["SUP_ADMIN_TITLE"] = "Tvarkara��iai";
$MESS["SUP_ADMIN_TITLE_ADD"] = "Naujas tvarkara�tis";
$MESS["SUP_ADMIN_TITLE_EDIT"] = "Redaguoti tvarkara�t� \"#NAME#\"";
$MESS["SUP_ERROR"] = "Klaidos";
$MESS["SUP_BACK_TO_ADMIN"] = "Gr��ti";
$MESS["SUP_TIMETABLE_NOT_FOUND"] = "Tvarkara�tis nerastas.";
$MESS["SUP_CONFIRM_DEL_MESSAGE"] = "Ar tikrai norite pa�alinti �� tvarkara�t�?";
$MESS["SUP_FORM_SETTINGS"] = "Nustatymai";
$MESS["SUP_FORM_SETTINGS_EX"] = "Nustatymai";
$MESS["SUP_NAME"] = "Pavadinimas";
$MESS["SUP_DESCRIPTION"] = "Apra�ymas";
$MESS["SUP_HOURS"] = "Darbo valandos";
$MESS["SUP_24H"] = "24V";
$MESS["SUP_CLOSED"] = "Darbas u�darytas";
$MESS["SUP_CUSTOM"] = "darbo valandos:<br>(VV24:MI)";
$MESS["SUP_ADMIN_USER_FIELDS"] = "Papildomos savyb�s";
$MESS["SUP_ADMIN_ROW_COPY"] = "Kopijuoti";
$MESS["SUP_ADMIN_ROW_DELETE"] = "Pa�alinti eilut�";
?>