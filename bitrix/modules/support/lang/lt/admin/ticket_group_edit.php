<?
$MESS["SUP_GE_TITLE_EDIT"] = "Grup�s \\\"%GROUP_NAME%\\\" redagavimas";
$MESS["SUP_GE_TITLE_NEW"] = "Naujos grup�s prid�jimas";
$MESS["SUP_GE_GROUP"] = "Grup�";
$MESS["SUP_GE_GROUP_TITLE"] = "Grup�s parametrai";
$MESS["SUP_GE_NAME"] = "Grup�s pavadinimas";
$MESS["SUP_GE_SORT"] = "R��iavimas";
$MESS["SUP_GE_XML_ID"] = "XML_ID";
$MESS["SUP_GE_ERROR"] = "Klaida";
$MESS["SUP_GE_GROUPS_LIST"] = "Grupi� s�ra�as";
$MESS["SUP_GE_USERGROUPS_LIST"] = "Grup�s naudotoj� s�ra�as";
$MESS["SUP_GE_USERGROUPS_ADD"] = "Prid�ti naudotoj� prie grup�s";
$MESS["SUP_GE_IS_TEAM_GROUP"] = "Technin�s pagalbos darbuotoj� grup�";
$MESS["SUP_GE_GROUP_USERS"] = "Naudotojai";
$MESS["SUP_GE_GROUP_USERS_TITLE"] = "Naudotojai grup�je";
$MESS["SUP_GE_ADD_MORE_USERS"] = "Prid�ti";
$MESS["SUP_GE_USER"] = "Naudotojas";
$MESS["SUP_GE_CAN_VIEW"] = "Gali matyti grup�s prane�imus";
$MESS["SUP_GE_CAN_MAIL"] = "Gali gauti grup�s prane�imus";
$MESS["MAIN_USER_PROFILE"] = "Redaguoti profil�";
$MESS["SUP_GE_CAN_MAIL_UPDATE"] = "Gauna bilieto atnaujinimo prane�imus";
?>