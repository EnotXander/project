<?
$MESS['SUP_F_PERIOD'] = 'Laikotarpis';
$MESS['SUP_MESSAGES'] = 'Prane�imai';
$MESS['SUP_PAGE_TITLE'] = 'Darbo aplinka';
$MESS['SUP_WRONG_DATE_FROM'] = 'Filtravimo langelyje �ra�ykite teising� \"Period�\" \"nuo\"';
$MESS['SUP_WRONG_DATE_TILL'] = 'Filtravimo langelyje �ra�ykite teising� \"Period�\" \"iki\"';
$MESS['SUP_FROM_TILL_DATE'] = 'Filtre \"Periodas\" \"iki\" turi b�ti v�lesnis, nei \"nuo\"';
$MESS['SUP_ALL_TICKET'] = 'visi u�klausimai';
$MESS['SUP_OPEN_TICKET'] = 'atidaryti u�klausimai';
$MESS['SUP_CLOSE_TICKET'] = 'u�daryti u�klausimai';
$MESS['SUP_OPEN'] = '�rankis prieinamas';
$MESS['SUP_OPEN_TOTAL'] = 'Viso atidaryta';
$MESS['SUP_CLOSE'] = 'U�daryta';
$MESS['SUP_TOTAL'] = 'Viso';
$MESS['SUP_TOTAL_TICKETS'] = 'Viso u�klausim�';
$MESS['SUP_STATUS'] = 'Statusas';
$MESS['SUP_DIFFICULTY'] = 'Sudetingumo lygis';
$MESS['SUP_SERVER_TIME'] = 'Serverio laikas:';
$MESS['SUP_MARK'] = 'Atsakym� vertinimas';
$MESS['SUP_CRITICALITY'] = 'Kriti�kumas';
$MESS['SUP_CATEGORY'] = 'Kategorija';
$MESS['SUP_SOURCE'] = '�altinis';
$MESS['SUP_RESPONSIBLE'] = 'Atsakingas';
$MESS['SUP_SLA'] = 'Palaikymo lygiai';
$MESS['SUP_F_RESPONSIBLE'] = 'Atsakingas';
$MESS['SUP_TICKETS1'] = 'U�klausim�';
$MESS['SUP_MESSAGES1'] = 'Prane�im�';
$MESS['SUP_FROM_OPEN_MESSAGES'] = 'nuo kiekio vis� prane�im� atidarytuose u�klausimuose';
$MESS['SUP_FROM_CLOSE_MESSAGES'] = 'nuo kiekio vis� prane�im� u�darytuose u�klausimuose';
$MESS['SUP_FROM_ALL_MESSAGES'] = 'nuo kiekio vis� prane�im�';
$MESS['SUP_FROM_OPEN_TICKETS'] = 'nuo kiekio atidaryt� u�klausim�';
$MESS['SUP_FROM_CLOSE_TICKETS'] = 'nuo kiekio u�daryt� u�klausim�';
$MESS['SUP_FROM_ALL_TICKETS'] = 'nuo kiekio vis� u�klausim�';
$MESS['SUP_F_SHOW_MESSAGES'] = 'Rodyti prane�imus (tame tarpe ir pasibaigusius)';
$MESS['SUP_F_SHOW_MESSAGES_S'] = 'Rodyti prane�imus';
$MESS['SUP_GREEN_ALT'] = 'paskutin� kart� � u�klausim� para�� technin�s pagalbos darbuotojas';
$MESS['SUP_RED_ALT'] = 'paskutin� kart� u�klausime ra�� technin�s pagalbos klientas';
$MESS['SUP_GREY_ALT'] = 'u�klausimas u�darytas';
$MESS['SUP_F_SITE'] = 'Tinklapiai';
$MESS['SUP_OVERDUE_MESSAGES'] = 'Pasibaig� prane�imai';
?>