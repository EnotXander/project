<?
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_TITLE"] = "Naujas u�klausimas (autoriui)";
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_TEXT"] = "#ID# - bilieto ID
#LANGUAGE_ID# - svetain�s kalbosa identifikatorius, su kuriuo yra susij�s problem� bilietas 
#DATE_CREATE# - suk�rimo data
#TIMESTAMP# - pakeitimo data
#DATE_CLOSE# - u�darymo data
#TITLE# - bilieto pavadinimas
#CATEGORY# - bilieto kategorija
#STATUS# - bilieto statusas
#CRITICALITY# - biliepalaikymo lygis
#SOURCE# - bilieto �altinis (web, el.pa6tas, telefonas ir tt)
#SPAM_MARK# - �lam�to �enklas
#MESSAGE_BODY# - prane�imo tekstas
#FILES_LINKS# - nuoroda � prisegtus failus
#ADMIN_EDIT_URL# - nuoroda � bilieto pakeitim� (administracijos skyrius)
#PUBLIC_EDIT_URL# - nuoroda � bilieto pakeitim� (vie�as skyrius)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# ir/arba #OWNER_SID#
#OWNER_USER_ID# - bilieto autoriaus ID
#OWNER_USER_NAME# - bilieto autoriaus vardas
#OWNER_USER_LOGIN# - bilieto autoriaus prisijungimo vardas
#OWNER_USER_EMAIL# - bilieto autoriaus el.pa�tas
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - bilieto autoriaus identifikatorius (el.pa�tas, telefonas ir pan.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# arba #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_NAME# - Atsakingo asmens pilnas vardas
#RESPONSIBLE_USER_ID# - atsakingo asmens naudootojo ID
#RESPONSIBLE_USER_EMAIL# - atsakingo asmens el. pa�tas
#RESPONSIBLE_USER_LOGIN# - atsakingo asmens prisijungimo vardas
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - palaikymo administratoriaus el. pa�tas

#CREATED_USER_ID# - bilieto k�r�jo ID
#CREATED_USER_LOGIN# - bilieto k�r�jo prisijungimo vardas
#CREATED_USER_EMAIL# - bilieto k�r�jo el.pa�tas
#CREATED_USER_NAME# - bilieto k�r�jo vardas
#CREATED_MODULE_NAME# - modulio, kuris buvo naudojamas k�riant biliet�, pavadinimas
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#SUPPORT_COMMENTS# - administratoriaus komentarai
";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_TITLE"] = "Naujas bilietas (techniniam palaikymui)";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_TEXT"] = "#ID# - bilieto ID
#LANGUAGE_ID# - svetain�s kalbos identifikatorius, su kuriuo yra susij�s problem� bilietas 
#DATE_CREATE# - suk�rimo data
#TIMESTAMP# - pakeitimo data
#DATE_CLOSE# - u�darymo data
#TITLE# - bilieto pavadinimas
#CATEGORY# - bilieto kategorija
#STATUS# - bilieto statusas
#CRITICALITY# - bilieto prioritetas
#SOURCE# - bilieto �altinis (web, el.pa6tas, telefonas ir tt)
#SLA# - palaikymo lygis
#SPAM_MARK# - �lam�to �enklas
#MESSAGE_BODY# - prane�imo tekstas
#FILES_LINKS# - nuoroda � prisegtus failus
#ADMIN_EDIT_URL# - nuoroda � bilieto pakeitim� (administracijos skyrius)
#PUBLIC_EDIT_URL# - nuoroda � bilieto pakeitim� (vie�as skyrius)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# ir/arba #OWNER_SID#
#OWNER_USER_ID# - bilieto autoriaus ID
#OWNER_USER_NAME# - bilieto autoriaus vardas
#OWNER_USER_LOGIN# - bilieto autoriaus prisijungimo vardas
#OWNER_USER_EMAIL# - bilieto autoriaus el.pa�tas
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - bilieto autoriaus identifikatorius (el.pa�tas, telefonas ir pan.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# arba #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_NAME# - Atsakingo asmens pilnas vardas
#RESPONSIBLE_USER_ID# - atsakingo asmens naudotojo ID
#RESPONSIBLE_USER_EMAIL# - atsakingo asmens el. pa�tas
#RESPONSIBLE_USER_LOGIN# - atsakingo asmens prisijungimo vardas
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - palaikymo administratoriaus el. pa�tas

#CREATED_USER_ID# - bilieto k�r�jo ID
#CREATED_USER_LOGIN# - bilieto k�r�jo prisijungimo vardas
#CREATED_USER_EMAIL# - bilieto k�r�jo el.pa�tas
#CREATED_USER_NAME# - bilieto k�r�jo vardas
#CREATED_MODULE_NAME# - modulio, kuris buvo naudojamas k�riant biliet�, pavadinimas
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#

#SUPPORT_COMMENTS# - administratoriaus komentarai

#COUPON# - kuponas";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_TITLE"] = "Bilietas buvo pakeistas techninio palaikymo atstovo (autoriui)";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_TEXT"] = "#ID# - bilieto ID
#LANGUAGE_ID# - svetain�s kalbos identifikatorius, su kuriuo yra susij�s problem� bilietas
#WHAT_CHANGE# - pakeitim� s�ra�as
#DATE_CREATE# - suk�rimo data
#TIMESTAMP# - pakeitimo data
#DATE_CLOSE# - u�darymo data
#TITLE# - bilieto pavadinimas
#STATUS# - bilieto statusas
#CATEGORY# - bilieto kategorija
#CRITICALITY# - bilieto prioritetas
#RATE# - atsakym� da�nis
#SLA# - palaikymo lygis
#SOURCE# - pirminis prane�imo �altinis (web, el.pa�tas, telefonas ir tt)
#SPAM_MARK# - �lam�to �enklas
#MESSAGES_AMOUNT# - prane�im� biliete kiekis
#ADMIN_EDIT_URL# - nuoroda � bilieto pakeitim� (administracijos skyrius)
#PUBLIC_EDIT_URL# - nuoroda � bilieto pakeitim� (vie�as skyrius)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# and/or #OWNER_SID#
#OWNER_USER_ID# - bilieto autoriaus ID
#OWNER_USER_NAME# - bilieto autoriaus vardas
#OWNER_USER_LOGIN# - bilieto autoriaus prisijungimo vardas
#OWNER_USER_EMAIL# - bilieto autoriaus el.pa�tas
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - bilieto autoriaus identifikatorius (el.pa�tas, telefonas ir pan.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# or #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_NAME# - Atsakingo asmens pilnas vardas
#RESPONSIBLE_USER_ID# - atsakingo asmens naudotojo ID
#RESPONSIBLE_USER_EMAIL# - atsakingo asmens el. pa�tas
#RESPONSIBLE_USER_LOGIN# - atsakingo asmens prisijungimo vardas
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - palaikymo administratoriaus el. pa�tas

#CREATED_USER_ID# - bilieto k�r�jo ID
#CREATED_USER_LOGIN# - bilieto k�r�jo prisijungimo vardas
#CREATED_USER_EMAIL# - bilieto k�r�jo el.pa�tas
#CREATED_USER_NAME# - bilieto k�r�jo vardas
#CREATED_MODULE_NAME# - modulio, kuris buvo naudojamas k�riant biliet�, pavadinimas
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME# 

#MODIFIED_USER_ID# - asmens, kuris pakeit� biliet�, naudotojo ID
#MODIFIED_USER_LOGIN# - asmens, kuris pakeit� biliet�, prisijungimo vardas
#MODIFIED_USER_EMAIL# - asmens, kuris pakeit� biliet�, el.pa�tas
#MODIFIED_USER_NAME# - asmens, kuris pakeit� biliet�, pilnas vardas
#MODIFIED_MODULE_NAME# - modulio, kuris buvo naudojamas k�riant biliet�, pavadinimas
#MODIFIED_TEXT# - [#MODIFIED_USER_ID#] (#MODIFIED_USER_LOGIN#) #MODIFIED_USER_NAME#

#MESSAGE_AUTHOR_USER_ID# - prane�imo autoriaus ID
#MESSAGE_AUTHOR_USER_NAME# - prane�imo autoriaus vardas
#MESSAGE_AUTHOR_USER_LOGIN# - prane�imo autoriaus prisijungimo vardas
#MESSAGE_AUTHOR_USER_EMAIL# - prane�imo autoriaus el. pa�tas
#MESSAGE_AUTHOR_TEXT# - [#MESSAGE_AUTHOR_USER_ID#] (#MESSAGE_AUTHOR_USER_LOGIN#) #MESSAGE_AUTHOR_USER_NAME#
#MESSAGE_AUTHOR_SID# - prane�imo autoriaus identifikatorius (el.pa�tas, telefonas ir pan.)
#MESSAGE_SOURCE# - prane�imo �altinis
#MESSAGE_BODY# - prane�imo tekstas
#FILES_LINKS# - nuorodoss � prisegtus failus

#SUPPORT_COMMENTS# - administratoriaus komentarai";
$MESS["SUP_SE_TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR_TITLE"] = "Bilietas buvo pakeistas autoriaus (autoriui)";
$MESS["SUP_SE_TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR_TEXT"] = "#ID# - bilieto ID
#LANGUAGE_ID# - svetain�s kalbos identifikatorius, su kuriuo yra susij�s problem� bilietas
#WHAT_CHANGE# - pakeitim� s�ra�as
#DATE_CREATE# - suk�rimo data
#TIMESTAMP# - pakeitimo data
#DATE_CLOSE# - u�darymo data
#TITLE# - bilieto pavadinimas
#STATUS# - bilieto statusas
#CATEGORY# - bilieto kategorija
#CRITICALITY# - bilieto prioritetas
#RATE# - atsakym� da�nis
#SLA# - palaikymo lygis
#SOURCE# - pirminis prane�imo �altinis (web, el.pa�tas, telefonas ir tt)
#SPAM_MARK# - �lam�to �enklas
#MESSAGES_AMOUNT# - prane�im� biliete kiekis
#ADMIN_EDIT_URL# - nuoroda � bilieto pakeitim� (administracijos skyrius)
#PUBLIC_EDIT_URL# - nuoroda � bilieto pakeitim� (vie�as skyrius)

#OWNER_USER_ID# - bilieto autoriaus ID
#OWNER_USER_NAME# - bilieto autoriaus vardas
#OWNER_USER_LOGIN# - bilieto autoriaus prisijungimo vardas
#OWNER_USER_EMAIL# - bilieto autoriaus el.pa�tas
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - bilieto autoriaus identifikatorius (el.pa�tas, telefonas ir pan.)


#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# or #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_NAME# - atsakingo asmens pilnas vardas
#RESPONSIBLE_USER_ID# - atsakingo asmens naudotojo ID
#RESPONSIBLE_USER_EMAIL# - atsakingo asmens el. pa�tas
#RESPONSIBLE_USER_LOGIN# - atsakingo asmens prisijungimo vardas
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - palaikymo administratoriaus el. pa�tas

#CREATED_USER_ID# - bilieto k�r�jo ID
#CREATED_USER_LOGIN# - bilieto k�r�jo prisijungimo vardas
#CREATED_USER_EMAIL# - bilieto k�r�jo el.pa�tas
#CREATED_USER_NAME# - bilieto k�r�jo vardas
#CREATED_MODULE_NAME# - modulio, kuris buvo naudojamas k�riant biliet�, pavadinimas
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME# 

#MODIFIED_USER_ID# - asmens, kuris pakeit� biliet�, naudotojo ID
#MODIFIED_USER_LOGIN# - asmens, kuris pakeit� biliet�, prisijungimo vardas
#MODIFIED_USER_EMAIL# - asmens, kuris pakeit� biliet�, el.pa�tas
#MODIFIED_USER_NAME# - asmens, kuris pakeit� biliet�, pilnas vardas
#MODIFIED_MODULE_NAME# - modulio, kuris buvo naudojamas k�riant biliet�, pavadinimas
#MODIFIED_TEXT# - [#MODIFIED_USER_ID#] (#MODIFIED_USER_LOGIN#) #MODIFIED_USER_NAME#

#MESSAGE_AUTHOR_USER_ID# - prane�imo autoriaus ID
#MESSAGE_AUTHOR_USER_NAME# - prane�imo autoriaus vardas
#MESSAGE_AUTHOR_USER_LOGIN# - prane�imo autoriaus prisijungimo vardas
#MESSAGE_AUTHOR_USER_EMAIL# - prane�imo autoriaus el. pa�tas
#MESSAGE_AUTHOR_TEXT# - [#MESSAGE_AUTHOR_USER_ID#] (#MESSAGE_AUTHOR_USER_LOGIN#) #MESSAGE_AUTHOR_USER_NAME#
#MESSAGE_AUTHOR_SID# - prane�imo autoriaus identifikatorius (el.pa�tas, telefonas ir pan.)
#MESSAGE_SOURCE# - prane�imo �altinis
#MESSAGE_BODY# - prane�imo tekstas
#FILES_LINKS# - nuorodoss � prisegtus failus

#SUPPORT_COMMENTS# - administratoriaus komentarai";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_TITLE"] = "Pakeitimai bilete (techniniam palaikymui)";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_TEXT"] = "#ID# - bilieto ID
#LANGUAGE_ID# - svetain�s kalbos identifikatorius, su kuriuo yra susij�s problem� bilietas
#WHAT_CHANGE# - pakeitim� s�ra�as
#DATE_CREATE# - suk�rimo data
#TIMESTAMP# - pakeitimo data
#DATE_CLOSE# - u�darymo data
#TITLE# - bilieto pavadinimas
#STATUS# - bilieto statusas
#CATEGORY# - bilieto kategorija
#CRITICALITY# - bilieto prioritetas
#RATE# - atsakym� da�nis
#SLA# - palaikymo lygis
#SOURCE# - pirminis prane�imo �altinis (web, el.pa�tas, telefonas ir tt)
#SPAM_MARK# - �lam�to �enklas
#MESSAGES_AMOUNT# - prane�im� biliete kiekis
#ADMIN_EDIT_URL# - nuoroda � bilieto pakeitim� (administracijos skyrius)
#PUBLIC_EDIT_URL# - nuoroda � bilieto pakeitim� (vie�as skyrius)

#OWNER_EMAIL# - #OWNER_USER_EMAIL# and/or #OWNER_SID#
#OWNER_USER_ID# - bilieto autoriaus ID
#OWNER_USER_NAME# - bilieto autoriaus vardas
#OWNER_USER_LOGIN# - bilieto autoriaus prisijungimo vardas
#OWNER_USER_EMAIL# - bilieto autoriaus el.pa�tas
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - bilieto autoriaus identifikatorius (el.pa�tas, telefonas ir pan.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# or #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_NAME# - atsakingo asmens pilnas vardas
#RESPONSIBLE_USER_ID# - atsakingo asmens naudotojo ID
#RESPONSIBLE_USER_EMAIL# - atsakingo asmens el. pa�tas
#RESPONSIBLE_USER_LOGIN# - atsakingo asmens prisijungimo vardas
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - palaikymo administratoriaus el. pa�tas 

#CREATED_USER_ID# - bilieto k�r�jo ID
#CREATED_USER_LOGIN# - bilieto k�r�jo prisijungimo vardas
#CREATED_USER_EMAIL# - bilieto k�r�jo el.pa�tas
#CREATED_USER_NAME# - bilieto k�r�jo vardas
#CREATED_MODULE_NAME# - modulio, kuris buvo naudojamas k�riant biliet�, pavadinimas
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME#  

#MODIFIED_USER_ID# - asmens, kuris pakeit� biliet�, naudotojo ID
#MODIFIED_USER_LOGIN# - asmens, kuris pakeit� biliet�, prisijungimo vardas
#MODIFIED_USER_EMAIL# - asmens, kuris pakeit� biliet�, el.pa�tas
#MODIFIED_USER_NAME# - asmens, kuris pakeit� biliet�, pilnas vardas
#MODIFIED_MODULE_NAME# - modulio, kuris buvo naudojamas k�riant biliet�, pavadinimas
#MODIFIED_TEXT# - [#MODIFIED_USER_ID#] (#MODIFIED_USER_LOGIN#) #MODIFIED_USER_NAME#

#MESSAGE_AUTHOR_USER_ID# - prane�imo autoriaus ID
#MESSAGE_AUTHOR_USER_NAME# - prane�imo autoriaus vardas
#MESSAGE_AUTHOR_USER_LOGIN# - prane�imo autoriaus prisijungimo vardas
#MESSAGE_AUTHOR_USER_EMAIL# - prane�imo autoriaus el. pa�tas
#MESSAGE_AUTHOR_TEXT# - [#MESSAGE_AUTHOR_USER_ID#] (#MESSAGE_AUTHOR_USER_LOGIN#) #MESSAGE_AUTHOR_USER_NAME#
#MESSAGE_AUTHOR_SID# - prane�imo autoriaus identifikatorius (el.pa�tas, telefonas ir pan.)
#MESSAGE_SOURCE# - prane�imo �altinis
#MESSAGE_HEADER# - \"******* MESSAGE *******\", arba \"******* HIDDEN MESSAGE *******\"
#MESSAGE_BODY# - prane�imo tekstas
#MESSAGE_FOOTER# - \"*********************** \"
#FILES_LINKS# - nuorodoss � prisegtus failus


#SUPPORT_COMMENTS# - administratoriaus komentarai";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_TITLE"] = "Atsakymo b�tinyb�s priminimas (techniniam palaikymui)";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_TEXT"] = "#ID# - bilieto ID
#LANGUAGE_ID# - svetain�s kalbos identifikatorius, su kuriuo yra susij�s problem� bilietas
#DATE_CREATE# - suk�rimo data 
#TITLE# - bilieto pavadinimas
#STATUS# - bilieto statusas
#CATEGORY# - bilieto kategorija
#CRITICALITY# - bilieto prioritetas
#RATE# - atsakym� da�nis
#SLA# - palaikymo lygis
#SOURCE# - pirminis prane�imo �altinis (web, el.pa�tas, telefonas ir tt)
#ADMIN_EDIT_URL# - nuoroda � bilieto pakeitim� (administracijos skyrius)

#EXPIRATION_DATE# - atsakymo galiojimo data
#REMAINED_TIME# - kiek liko iki atsakymo galiojimo pabaigos

#OWNER_EMAIL# - #OWNER_USER_EMAIL# and/or #OWNER_SID#
#OWNER_USER_ID# - bilieto autoriaus ID
#OWNER_USER_NAME# - bilieto autoriaus vardas
#OWNER_USER_LOGIN# - bilieto autoriaus prisijungimo vardas
#OWNER_USER_EMAIL# - bilieto autoriaus el.pa�tas
#OWNER_TEXT# - [#OWNER_USER_ID#] (#OWNER_USER_LOGIN#) #OWNER_USER_NAME#
#OWNER_SID# - bilieto autoriaus identifikatorius (el.pa�tas, telefonas ir pan.)

#SUPPORT_EMAIL# - #RESPONSIBLE_USER_EMAIL# or #SUPPORT_ADMIN_EMAIL#
#RESPONSIBLE_USER_NAME# - atsakingo asmens pilnas vardas
#RESPONSIBLE_USER_ID# - atsakingo asmens naudotojo ID
#RESPONSIBLE_USER_EMAIL# - atsakingo asmens el. pa�tas
#RESPONSIBLE_USER_LOGIN# - atsakingo asmens prisijungimo vardas
#RESPONSIBLE_TEXT# - [#RESPONSIBLE_USER_ID#] (#RESPONSIBLE_USER_LOGIN#) #RESPONSIBLE_USER_NAME#
#SUPPORT_ADMIN_EMAIL# - palaikymo administratoriaus el. pa�tas

#CREATED_USER_ID# - bilieto k�r�jo ID
#CREATED_USER_LOGIN# - bilieto k�r�jo prisijungimo vardas
#CREATED_USER_EMAIL# - bilieto k�r�jo el.pa�tas
#CREATED_USER_NAME# - bilieto k�r�jo vardas
#CREATED_MODULE_NAME# - modulio, kuris buvo naudojamas k�riant biliet�, pavadinimas
#CREATED_TEXT# - [#CREATED_USER_ID#] (#CREATED_USER_LOGIN#) #CREATED_USER_NAME# 

#MESSAGE_BODY# - prane�imo, kuris reikalauja atsakymo, tekstas";
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_SUBJECT"] = "[TID##ID#] #SERVER_NAME#: J�s� pra�ymas buvo s�kmingai priimtas";
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_MESSAGE"] = "
J�s� pra�ymas buvo priimtas su unikaliu numeriu #ID#. 

Pra�ome neatsakin�ti � �� prane�im�. Tai tik sukurtas 
patvirtinimas, rodantis, kad tech.palaikymas gavo j�s� pra�ym� 
ir su juo dirba. 

Informacija apie j�s� pra�ym�: 

Tema        - #TITLE# 
Nuo         - #SOURCE##OWNER_SID##OWNER_TEXT#
Kategorija   - #CATEGORY#
Prioritetas  - #CRITICALITY#

Sukurtas       - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]
Atsakingas   - #RESPONSIBLE_TEXT#
Palaikymo lygis - #SLA#

>======================== MESSAGE ====================================

#FILES_LINKS##MESSAGE_BODY#

>=====================================================================

Nor�dami per�i�r�ti ir redaguoti pra�ym�, spauskite nuorod�: 
http://#SERVER_NAME##PUBLIC_EDIT_URL#?ID=#ID#

Automati�kai sugeneruotas prane�imas. ";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_SUBJECT"] = "[TID##ID#] #SERVER_NAME#: Naujas pra�ymas";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_MESSAGE"] = "Naujas pra6ymas # #ID# at #SERVER_NAME#.
#SPAM_MARK#
Nuo: #SOURCE##OWNER_SID##OWNER_TEXT#

Tema: #TITLE#

>======================== MESSAGE ====================================

#FILES_LINKS##MESSAGE_BODY#

>=====================================================================

Atsakingas   - #RESPONSIBLE_TEXT#
Kategorija      - #CATEGORY#
Prioritetas      - #CRITICALITY#
Palaikymo lygis - #SLA#
Sukurtas       - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]

Nor�dami per�i�r�ti ir redaguoti pra�ym�, spauskite nuorod�:
http://#SERVER_NAME##ADMIN_EDIT_URL#?ID=#ID#&lang=#LANGUAGE_ID#


Automati�kai sugeneruotas prane�imas. ";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_SUBJECT"] = "[TID##ID#] #SERVER_NAME#: Poky�iai j�s� pra�yme";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_MESSAGE"] = "Poky�iai j�s� pra�yme # #ID# at #SERVER_NAME#.

#WHAT_CHANGE#
Tema: #TITLE# 

Nuo: #MESSAGE_SOURCE##MESSAGE_AUTHOR_SID##MESSAGE_AUTHOR_TEXT#

>======================== PRANE�IMAS ====================================#FILES_LINKS##MESSAGE_BODY#
>=====================================================================

Autorius  - #SOURCE##OWNER_SID##OWNER_TEXT#
Sukurtas - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]
Pakeistas - #MODIFIED_TEXT##MODIFIED_MODULE_NAME# [#TIMESTAMP#]

Atsakingas   - #RESPONSIBLE_TEXT#
Kategorija      - #CATEGORY#
Prioritetas      - #CRITICALITY#
Statusas        - #STATUS#
Ratingas          - #RATE#
Palaikymo lygis - #SLA#

Nor�dami per�i�r�ti ir redaguoti pra�ym�, paspauskite nuorod�:
http://#SERVER_NAME##PUBLIC_EDIT_URL#?ID=#ID#

Pra�ome nepamir�ti �vertinti techninio palaikymo atsakymus, u�darius pra�ym�:
http://#SERVER_NAME##PUBLIC_EDIT_URL#?ID=#ID#

Automati�kai sugeneruotas prane�imas.";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_SUBJECT"] = "[TID##ID#] #SERVER_NAME#: Poky�iai pra�yme";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_MESSAGE"] = "Poky�iai pra�yme # #ID#  #SERVER_NAME#.
#SPAM_MARK#
#WHAT_CHANGE#
Tema: #TITLE# 

Nuo: #MESSAGE_SOURCE##MESSAGE_AUTHOR_SID##MESSAGE_AUTHOR_TEXT#

>#MESSAGE_HEADER##FILES_LINKS##MESSAGE_BODY#
>#MESSAGE_FOOTER#

Autorius  - #SOURCE##OWNER_SID##OWNER_TEXT#
Sukurtas - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]
Pakeistas - #MODIFIED_TEXT##MODIFIED_MODULE_NAME# [#TIMESTAMP#]

Atsakingas   - #RESPONSIBLE_TEXT#
Kategorija      - #CATEGORY#
Prioritetas      - #CRITICALITY#
Statusas        - #STATUS#
Ratingas          - #RATE#
Palaikymo lygis - #SLA#

>======================= KOMENTARAI ===================================#SUPPORT_COMMENTS#
>====================================================================

Nor�dami per�i�r�ti ir redaguoti pra�ym�, paspauskite nuorod�:
http://#SERVER_NAME##ADMIN_EDIT_URL#?ID=#ID#&lang=#LANGUAGE_ID#

Automati�kai sugeneruotas prane�imas.";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_SUBJECT"] = "[TID##ID#] #SERVER_NAME#: Priminimas b�tinai atsakyti";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_MESSAGE"] = "Priminimas b�tinai atsakyti bilietui # #ID# at #SERVER_NAME#.

Galiojimo data - #EXPIRATION_DATE# (priminti: #REMAINED_TIME#)

>======================= BILIETO DATA =================================

Tema - #TITLE# 

Autorius  - #SOURCE##OWNER_SID##OWNER_TEXT#
Sukurtas - #CREATED_TEXT##CREATED_MODULE_NAME# [#DATE_CREATE#]

Palaikymo lygis - #SLA#

Atsakingas   - #RESPONSIBLE_TEXT#
Kategorija      - #CATEGORY#
Prioritetas      - #CRITICALITY#
Statusas        - #STATUS#
Atsakymo reitingas  - #RATE#

>================= PRANE�IMAS REIKALAUJA ATSAKYMO ===========================
#MESSAGE_BODY#
>=====================================================================

Nor�dami per�i�r�ti ir redaguoti pra�ym�, paspauskite nuorod�:
http://#SERVER_NAME##ADMIN_EDIT_URL#?ID=#ID#&lang=#LANGUAGE_ID#

Automati�kai sugeneruotas prane�imas.";
$MESS["SUP_SE_TICKET_GENERATE_SUPERCOUPON_TITLE"] = "Kuponas aktyvuotas";
$MESS["SUP_SE_TICKET_GENERATE_SUPERCOUPON_TEXT"] = "#COUPON# - Kuponas
#COUPON_ID# - Kupono ID
#DATE# - Naudojimo data
#USER_ID# - Naudotojo ID
#SESSION_ID# - Sesijos ID 
#GUEST_ID# - Sve�io ID";
?>