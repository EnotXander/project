<?
$MESS["SUP_MODULE_NAME"] = "Pagalbos tarnyba";
$MESS["SUP_MODULE_DESCRIPTION"] = "�is modulis suteikia svetainei pagalbos tarnybos sistem�.";
$MESS["SUP_INSTALL_TITLE"] = "Pagalbos tarnybos modulio diegimas";
$MESS["SUP_UNINSTALL_TITLE"] = "Pagalbos tarnybos modulio �alinimas";
$MESS["SUP_SELECT_INITITAL"] = "Pasirinkite aplank� pagalbos tarnybos modulio failams:";
$MESS["SUP_NO"] = "ne";
$MESS["SUP_ATTENTION"] = "D�mesio!Modulis bus pa�alintas.";
$MESS["SUP_YOU_CAN_SAVE_TABLES"] = "Nor�dami i�saugoti duomenis, saugomus duomen� baz�s lentel�se, patikrinkite &quot;Save Tables&quot; langel�";
$MESS["SUP_SAVE_TABLES"] = "I�saugoti lenteles";
$MESS["SUP_DELETE_COMLETE"] = "�alinimas atliktas.";
$MESS["SUP_BACK"] = "Gr��ti";
$MESS["SUP_ERRORS"] = "Klaidos:";
$MESS["SUP_INSTALL"] = "�diegti";
$MESS["SUP_COMPLETE"] = "�diegimas atliktas.";
$MESS["SUP_WRONG_MAIN_VERSION"] = "Nor�dami �diegti �� modul�, j�s turite atnaujinti sistemos branduol� iki versijos #VER#";
$MESS["SUP_DENIED"] = "negalima �eiti";
$MESS["SUP_CREATE_TICKET"] = "techninio palaikymo klientas";
$MESS["SUP_SUPPORT_STAFF_MEMBER"] = "techninio palaikymo darbuotojas";
$MESS["SUP_DEMO_ACCESS"] = "demo prieiga";
$MESS["SUP_SUPPORT_ADMIN"] = "techninio palaikymo administratorius";
$MESS["SUP_URL_PUBLIC"] = "Pagalbos tarnybos modulio aplankas (#SITE_DIR# - svetain� �akninis aplankas):";
$MESS["SUP_RESET"] = "Atstatyti";
$MESS["SUP_DEMO_DIR"] = "Sekite �i� nuorod�, nor�dami pamatyti palaikym� vykdym�:";
$MESS["SUP_SITE"] = "Svetain�";
$MESS["SUP_LINK"] = "Nuoroda";
$MESS["SUPPORT_ERROR_EDITABLE"] = "J�s� laida nesuteikia �io modulio.";
?>