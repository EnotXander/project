<?
$MESS['SUPPORT_MAIL_DEF_REGISTERED'] = 'Nustatyti pagal el. pa�t� ank��iau u�siregistravus� naudotoj�:';
$MESS['SUPPORT_MAIL_DEF_REGISTERED_Y'] = 'taip, bandyti susieti u�klausim� su naudotoju';
$MESS['SUPPORT_MAIL_DEF_REGISTERED_N'] = 'ne, visada kurti anoniminius u�klausimus';
$MESS['SUPPORT_MAIL_ADD_TO_OPENED_TICKET'] = 'Prid�ti nauj� prane�im� � jau atidaryt� u�klausim�:';
$MESS['SUPPORT_MAIL_ADD_TO_OPENED_T_EMAIL'] = 'tik i� u�klausimo k�r�jo el. pa�to user@domain.com ';
$MESS['SUPPORT_MAIL_ADD_TO_OPENED_T_DOMAIN'] = 'i� betkokio adreso su u�klausimo k�r�jo domenu (*@domain.com)';
$MESS['SUPPORT_MAIL_ADD_TO_OPENED_T_ANY'] = 'i� betkokio adreso (tikrinama tik prane�imo tema)';
$MESS['SUPPORT_MAIL_SUBJECT_TEMPLATE'] = 'Temos �ablonai nustatyti atsakym� � u�klausim�:';
$MESS['SUPPORT_MAIL_SUBJECT_TEMPLATE_NOTES'] = '(reguliari i�rai�ka, pirmuose kabut�se turi b�ti u�klausimo numeris)';
$MESS['SUPPORT_MAIL_ADD_TO_CATEGORY'] = 'Susieti nauj� u�klausim� su kategorija:';
$MESS['SUPPORT_MAIL_HIDDEN'] = 'kaip pasl�ptus (tik technin�s pagalbos darbuotojams)';
$MESS['SUPPORT_MAIL_ADD_WITH_CRITICALITY'] = 'nustatyti kriti�kum� naujam u�klausimui:';
$MESS['SUPPORT_MAIL_CONNECT_TICKET_WITH_SITE'] = 'Susieti nauj� u�klausim� su svetaine:';
$MESS['SUPPORT_MAIL_MAILBOX'] = '< el. pa�to >';
?>