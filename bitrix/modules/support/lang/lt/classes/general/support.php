<?
$MESS["SUP_ERROR_INVALID_COUPON"] = "Kuponas ne�inomas arba pasibaig�s.";
$MESS["SUP_ERROR_EMPTY_TITLE"] = "U�pildykite lauk� \\\"Antra�t�\\\"";
$MESS["SUP_ERROR_EMPTY_MESSAGE"] = "U�pildykite lauk� \\\"Prane�imas\\\"";
$MESS["SUP_UNKNOWN_USER"] = "Ne�inomas naudotojas";
$MESS["SUP_UNKNOWN_GUEST"] = "Neu�siregistrav�s naudotojas";
$MESS["SUP_ADD_MESSAGE_TO_TECHSUPPORT"] = "prane�imo suk�rimas techniniam palaikymui";
$MESS["SUP_FORGOT_NAME"] = "Laukelyje \\\"Pavadinimas\\\" �ra�ykite pavadinim�";
$MESS["SUP_INCORRECT_SID"] = "Neteisingas simbolinis kodas (galima naudoti tik lotini�kas raid�s, skai�iai bei simbol� \\\"_\\\")";
$MESS["SUP_SID_ALREADY_IN_USE"] = "�odyno tipui \\\"#TYPE#\\\" bei kalbai \\\"#LANG#\\\" nurodytas simbolinis kodas jau naudojamas �ra�e # #RECORD_ID#.";
$MESS["SUP_UNKNOWN_ID"] = "N�ra �ra�o su kodu #ID#";
$MESS["SUP_ERROR_ADD_DICTONARY"] = "Klaida i�saugojant �ra��.";
$MESS["SUP_ERROR_UPDATE_DICTONARY"] = "Klaida atnaujinant �ra��.";
$MESS["SUP_ERROR_GROUP_NOT_FOUND"] = "Grup� su tokiu ID nerasta.";
$MESS["SUP_ERROR_GROUP_NAME_EMPTY"] = "Grup�s pavadinimas tu��ias.";
$MESS["SUP_ERROR_USER_ID_EMPTY"] = "Naudotojo ID tu��ias.";
$MESS["SUP_ERROR_GROUP_ID_EMPTY"] = "Grup�s ID tu��ias.";
$MESS["SUP_ERROR_USERGROUP_EXISTS"] = "�is naudotojas jau priklauso �iai grupei.";
$MESS["SUP_ERROR_NO_SUPPORT_USER"] = "�is naudotojas n�ra technin�s pagalbos grup�s narys.";
$MESS["SUP_ERROR_USER_NO_TEAM"] = "�iai grupei gali priklausyti tik technin�s pagalbos nariai. ";
$MESS["SUP_ERROR_USER_NO_CLIENT"] = "�iai grupei gali priklausyti tik technin�s pagalbos klientai.";
$MESS["SUP_ERROR_NO_GROUP"] = "N�ra tokios grup�s.";
$MESS["SUP_ERROR_NO_USER"] = "N�ra tokio naudotojo.";
?>