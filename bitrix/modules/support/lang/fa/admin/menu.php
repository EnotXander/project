<?
$MESS["SUP_M_CATEGORY"] = "گروه";
$MESS["SUP_M_CRITICALITY"] = "میزان اهمیت";
$MESS["SUP_M_REPORT_TABLE"] = "پیشخوان";
$MESS["SUP_M_REPORT_GRAPH"] = "دیاگرامها";
$MESS["SUP_M_DIFFICULTY"] = "سختی";
$MESS["SUP_M_DIFFICULTY_TITLE"] = "سختی";
$MESS["SUP_M_GROUPS"] = "گروه ها";
$MESS["SUP_M_SUPPORT"] = "پشتیبانی";
$MESS["SUP_M_DICT"] = "کتاب مرجع";
$MESS["SUP_M_SOURCE"] = "منبع";
$MESS["SUP_M_STATUS"] = "وضعیت";
$MESS["SUP_M_SLA"] = "سطوح پشتیبانی";
$MESS["SUP_M_TICKETS"] = "تیکت های دریافت شده";
?>