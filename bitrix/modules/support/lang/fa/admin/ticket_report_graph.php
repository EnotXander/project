<?
$MESS["SUP_F_MARK"] = "دقت پاسخ";
$MESS["SUP_F_DEL_FILTER"] = "لغو";
$MESS["SUP_F_CATEGORY"] = "گروه";
$MESS["SUP_F_CRITICALITY"] = "میزان اهمیت";
$MESS["SUP_PAGE_TITLE"] = "دیاگرامها";
$MESS["SUP_F_SET_FILTER"] = "بیاب";
$MESS["SUP_MESSAGES"] = "پیغامها";
$MESS["SUP_F_RESPONSIBLE"] = "پاسخگو";
$MESS["SUP_F_SITE"] = "سایتها";
$MESS["SUP_F_SOURCE"] = "منبع";
$MESS["SUP_F_STATUS"] = "وضعیت";
$MESS["SUP_F_SLA"] = "سطح پشتیبانی";
?>