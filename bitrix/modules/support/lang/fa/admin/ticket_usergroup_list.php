<?
$MESS["SUP_UGL_ADD"] = "افزودن";
$MESS["SUP_UGL_ADD_TITLE"] = "افزودن";
$MESS["SUP_UGL_FLT_ALL_GROUPS"] = "همه گروه ها";
$MESS["SUP_UGL_DELETE"] = "حذف";
$MESS["SUP_UGL_EDIT"] = "ویرایش";
$MESS["SUP_UGL_FLT_GROUP"] = "گروه";
$MESS["SUP_UGL_GROUP_NAME"] = "نام گروه";
$MESS["SUP_UGL_FLT_LOGIN"] = "نام کاربری";
$MESS["SUP_UGL_LOGIN"] = "نام کاربری";
$MESS["SUP_UGL_FIRST_NAME"] = "نام";
$MESS["SUP_UGL_PAGES"] = "صفحات";
$MESS["SUP_UGL_FLT_USER"] = "کاربر";
$MESS["SUP_UGL_USER_ID"] = "شناسه کاربر";
?>