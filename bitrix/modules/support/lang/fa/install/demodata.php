<?
$MESS["SUP_DEF_ANSWER_DOES_NOT_SUIT"] = "پاسخ راهگشا نبود";
$MESS["SUP_DEF_ANSWER_IS_NOT_COMPLETE"] = "پاسخ کافی نبود";
$MESS["SUP_DEF_ANSWER_SUITS_THE_NEEDS"] = "پاسخ مناسب بود";
$MESS["SUP_DEF_COULD_NOT_BE_SOLVED"] = "قابل حل نیست";
$MESS["SUP_DEF_EASY"] = "ساده";
$MESS["SUP_DEF_E_MAIL"] = "ایمیل";
$MESS["SUP_DEF_FORUM"] = "انجمن";
$MESS["SUP_DEF_HARD"] = "سخت";
$MESS["SUP_DEF_HIGH"] = "بالا";
$MESS["SUP_DEF_LOW"] = "پایین";
$MESS["SUP_DEF_MEDIUM"] = "متوسط";
$MESS["SUP_DEF_MIDDLE"] = "متوسط";
$MESS["SUP_DEF_PHONE"] = "تلفن";
$MESS["SUP_DEF_PROBLEM_SOLVING_IN_PROGRESS"] = "مشکل در دست اقدام است";
$MESS["SUP_DEF_REQUEST_ACCEPTED"] = "درخواست پذیرفته شد";
$MESS["SUP_DEF_SUCCESSFULLY_SOLVED"] = "به خوبی حل شد";
?>