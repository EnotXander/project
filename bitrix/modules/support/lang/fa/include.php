<?
$MESS["SUP_ALL"] = "(همه)";
$MESS["SUP_ACTION"] = "اقدام";
$MESS["SUP_ADD"] = "افزودن";
$MESS["SUP_MARK"] = "دقت پاسخ";
$MESS["SUP_APPLY"] = "اعمال";
$MESS["SUP_F_DEL_FILTER"] = "لغو";
$MESS["SUP_CATEGORY"] = "گروه";
$MESS["SUP_CREATED"] = "ساخته شده";
$MESS["SUP_CRITICALITY"] = "میزان اهمیت";
$MESS["SUP_DELETE"] = "حذف";
$MESS["SUP_DIFFICULTY"] = "سختی";
$MESS["SUP_F_SET_FILTER"] = "بیاب";
$MESS["SUP_MIN"] = "دقیقه";
$MESS["SUP_MODIFIED"] = "ویرایش شده";
$MESS["SUP_EDIT"] = "ویرایش";
$MESS["SUP_NO"] = "خیر";
$MESS["SUP_RESET"] = "نوسازی";
$MESS["SUP_SAVE"] = "ذخیره";
$MESS["SUP_SEC"] = "ثانیه";
$MESS["SUP_SOURCE"] = "منبع";
$MESS["SUP_STATUS"] = "وضعیت";
$MESS["SUP_GREY_ALT"] = "تیکت بسته است";
$MESS["SUP_TOTAL"] = "مجموع";
$MESS["SUP_EXACT_MATCH"] = "دقیقا همین";
$MESS["SUP_USER_PROFILE"] = "پروفایل کاربر";
?>