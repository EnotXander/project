<?
$MESS["BPSFA_PD_ADD"] = "Prid�ti s�lyg�";
$MESS["BPSFA_PD_CALENDAR"] = "Kalendorius";
$MESS["BPSFA_PD_DELETE"] = "Pa�alinti";
$MESS["BPSFA_PD_NO"] = "Ne";
$MESS["BPSFA_PD_YES"] = "Taip";
$MESS["BPSFA_PD_CREATE"] = "Prid�ti lauk�";
$MESS["BPSFA_PD_EMPTY_NAME"] = "Truksta lauko pavadinimo";
$MESS["BPSFA_PD_EMPTY_CODE"] = "Truksta lauko kodo";
$MESS["BPSFA_PD_WRONG_CODE"] = "Lauko kodas gali tur�ti tik lotyni�kaos raides ir skaitmenis.";
$MESS["BPSFA_PD_FIELD"] = "Laukas";
$MESS["BPSFA_PD_F_NAME"] = "Pavadinimas";
$MESS["BPSFA_PD_F_CODE"] = "ID";
$MESS["BPSFA_PD_F_TYPE"] = "Tipas";
$MESS["BPSFA_PD_F_MULT"] = "Sud�tinis";
$MESS["BPSFA_PD_F_REQ"] = "Privalomas";
$MESS["BPSFA_PD_F_LIST"] = "Reik�m�s";
$MESS["BPSFA_PD_SAVE"] = "I�saugoti";
$MESS["BPSFA_PD_SAVE_HINT"] = "Sukurti lauk�";
$MESS["BPSFA_PD_CANCEL"] = "At�aukti";
$MESS["BPSFA_PD_CANCEL_HINT"] = "At�aukti";
?>