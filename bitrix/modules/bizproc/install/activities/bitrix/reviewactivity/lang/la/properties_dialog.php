<?
$MESS["BPAR_PD_REVIEWERS"] = "Revisiones";
$MESS["BPAR_PD_DESCR"] = "Descripci�n de la tarea";
$MESS["BPAR_PD_NAME"] = "Nombre de la tarea";
$MESS["BPAR_PD_APPROVE_TYPE"] = "Requiere lectura por";
$MESS["BPAR_PD_APPROVE_TYPE_ALL"] = "Todos los empleados";
$MESS["BPAR_PD_APPROVE_TYPE_ANY"] = "Cualquier Empleado";
$MESS["BPAR_PD_SET_STATUS_MESSAGE"] = "Establecer Estado del Mensaje";
$MESS["BPAR_PD_YES"] = "Si";
$MESS["BPAR_PD_NO"] = "No";
$MESS["BPAR_PD_STATUS_MESSAGE"] = "Estado del Texto";
$MESS["BPAR_PD_STATUS_MESSAGE_HINT"] = "Los siguiente macros est�n habilitables: #PERC# - porcentaje, #REV# - personas actualmente conocidas, #TOT# - total de personas por ser conocidas.";
$MESS["BPAR_PD_TASK_BUTTON_MESSAGE"] = "Titulo del bot�n tarea";
$MESS["BPAR_PD_TIMEOUT_DURATION"] = "Periodo de examinaci�n";
$MESS["BPAR_PD_TIMEOUT_DURATION_HINT"] = "El proceso de examen de documentos se dar� por terminado autom�ticamente cuando el per�odo de tiempo de espera. Un vac�o o especifica el valor cero o per�odo de aprobaci�n se va a aplicar.";
$MESS["BPAR_PD_TIME_D"] = "d�as";
$MESS["BPAR_PD_TIME_H"] = "horas";
$MESS["BPAR_PD_TIME_M"] = "minutos";
$MESS["BPAR_PD_TIME_S"] = "segundos";
$MESS["BPAR_PD_STATUS_MESSAGE_HINT1"] = "Son posibles las siguientes macros: #PERCENT# - conocimientos de la relaci�n,el n�mero de personas con las que cuentan actualmente, #TOTAL# - total de personas requeridas, #REVIEWERS# - los usuarios que vieron el tema";
$MESS["BPAR_PD_SHOW_COMMENT"] = "Mostrar el campo de entrada de comentarios";
$MESS["BPAR_PD_COMMENT_LABEL_MESSAGE"] = "Etiqueta de campo de entrada de comentarios";
$MESS["BPAR_PD_ACCESS_CONTROL"] = "Mostrar descripci�n de la tarea s�lo a las personas responsables";
?>