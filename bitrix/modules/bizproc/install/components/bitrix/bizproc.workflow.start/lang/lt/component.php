<?
$MESS["BPABS_EMPTY_DOC_ID"] = "Nenurodytas dokumento, kuriam turi b�ti sukurtas verslo procesas, ID.";
$MESS["BPABS_EMPTY_ENTITY"] = "Nenurodytas subjektas, kuriam turi b�ti sukurtas verslo procesas.";
$MESS["BPABS_TITLE"] = "Prad�ti verslo proces�";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "Dokumento tipas yra privalomas.";
$MESS["BPATT_NO_MODULE_ID"] = "Modulio ID yra privalomas.";
$MESS["BPABS_NO_PERMS"] = "J�s neturite teis�s prad�ti verslo proces� �iam dokumentui.";
?>