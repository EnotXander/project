<?
$MESS["BPATT_AUTO_EXECUTE"] = "Automatinis paleidimas";
$MESS["BPATT_HELP1_TEXT"] = "Valstyb�s skatinamas verslo procesas yra nuoseklus verslo procesas su prieigos leidimo paskirstymu tvarkyti skirting� status� dokumentus.";
$MESS["BPATT_HELP2_TEXT"] = "Nuoseklus verslo procesas yra paprastas verslo procesas, kuris atlieka nuosekli� veiksm� su dokumentais serij�.";
$MESS["BPATT_NAME"] = "Pavadinimas";
$MESS["BPATT_MODIFIED"] = "Pakeista";
$MESS["BPATT_USER"] = "Pakeit�";
$MESS["WD_EMPTY"] = "N�ra verslo proceso �ablono. Sukurti standartin� <a href=#HREF#>verslo proces�</a>.";
$MESS["BPATT_ALL"] = "I� viso";
?>