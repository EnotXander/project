<?
$MESS["BPT_SYNC_NAME"] = "Publicando en los sitios controlados";
$MESS["BPT_SYNC_DESC"] = "Permitir la publicación de elementos de block de información en los sitios controlados.";
$MESS["BPT_SYNC_SEQ"] = "Proceso de negocio secuencial";
$MESS["BPT_SYNC_SYNC"] = "Publicando en los sitios controlados";
$MESS["BPT_SYNC_PUBLISH"] = "Documento Publicado";
?>