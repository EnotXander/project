<?
$MESS["BP_V1ST_NAME"] = "Pirmasis patvirtinimas";
$MESS["BP_V1ST_TASK_NAME"] = "Patvirtinti dokument�: \"{=Document:NAME}\"";
$MESS["BP_V1ST_APPR"] = "Patvirtinta";
$MESS["BP_V1ST_MAIL_NAME"] = "si�sti prane�im�";
$MESS["BP_V1ST_TASK_T"] = "Pra�ome patvirtinti arba atmesti dokument�.";
$MESS["BP_V1ST_T3"] = "Paskelbti dokument�";
$MESS["BP_V1ST_DESC"] = "Rekomenduojama, kai patvirtinimo i� vieno respondento  pakanka. Sukurti asmen�, kuriems bus si�loma dalyvauti balsavime, s�ra��. Balsavimas yra baigtas, kai buvo gautas pirmasis balsas.";
$MESS["BP_V1ST_STAT_NA"] = "Atmesta";
$MESS["BP_V1ST_S2"] = "Veiksm� seka";
$MESS["BP_V1ST_SEQ"] = "Nuseklus verslo procesas";
$MESS["BP_V1ST_APPR_S"] = "Statusas: patvirtinta";
$MESS["BP_V1ST_STAT_NA_T"] = "Statusas: atmesta";
$MESS["BP_V1ST_TNA"] = "Dokumentas buvo atmestas.";
$MESS["BP_V1ST_PARAM1_DESC"] = "Naudotojai, kurie dalyvauja patvirtinimo procese. ";
$MESS["BP_V1ST_VNAME"] = "Atsakymas d�l dokumento";
$MESS["BP_V1ST_MAIL_TEXT"] = "Balsavimas d�l \"{=Document:NAME}\" baigtas.

Dokumentas patvirtintas. 
{=ApproveActivity1:Comments}";
$MESS["BP_V1ST_MAIL2_NA_TEXT"] = "Balsavimas d�l \"{=Document:NAME}\" baigtas.

Dokumentas atmestas. 
{=ApproveActivity1:Comments}";
$MESS["BP_V1ST_MAIL_SUBJ"] = "Balsavimas d�l  {=Document:NAME}: Dokumentas buvo patvirtintas.";
$MESS["BP_V1ST_MAIL2_NA"] = "Balsavimas d�l  {=Document:NAME}: Dokumentas buvo atmestas.";
$MESS["BP_V1ST_PARAM1"] = "Balsuojantys asmenys";
$MESS["BP_V1ST_TASK_DESC"] = "J�s turite patvirtinti arba atmesti dokument� \"{=Document:NAME}\".

Autorius: {=Document:CREATED_BY_PRINTABLE}";
$MESS["BP_V1ST_TASK_TEXT"] = "J�s turite patvirtinti arba atmesti dokument� \"{=Document:NAME}\".

Atidarykite nuorod�: http://#HTTP_HOST##TASK_URL#

Autorius: {=Document:CREATED_BY_PRINTABLE}";
?>