<?
$MESS["BPT_SM_TASK1_TITLE"] = "Patvirtinti dokument�: \"{=Document:NAME}\"";
$MESS["BPT_SM_STATUS"] = "Patvirtintas";
$MESS["BPT_SM_ACT_TITLE"] = "Si�sti el. prane�im�";
$MESS["BPT_SM_APPROVE_NAME"] = "Pra�ome patvirtinti arba atmesti dokument�.";
$MESS["BPT_SM_PUB"] = "Paskelbti dokument�";
$MESS["BPT_SM_DESC"] = "Rekomenduojama, kai sprendimas turi b�ti priimamas paprasta bals� dauguma. J�s galite paskirti balsuojan�ius asmenis ir leisti jiems komentuoti. Kai balsavimas yra atliktas, visiems dalyvaujantiems asmenims prane�ta apie rezultat�.";
$MESS["BPT_SM_MAIL2_STATUS"] = "Atmestas";
$MESS["BPT_SM_ACT_NAME"] = "Veiksm� seka";
$MESS["BPT_SM_TITLE1"] = "Nuoseklus verslo procesas";
$MESS["BPT_SM_NAME"] = "Paprastas patvirtinimas/Balsavimas";
$MESS["BPT_SM_STATUS2"] = "Statusas: patvirtintas";
$MESS["BPT_SM_MAIL2_STATUS2"] = "Statusas: atmestas";
$MESS["BPT_SM_MAIL1_TITLE"] = "Dokumentas buvo patvirtintas";
$MESS["BPT_SM_MAIL2_TITLE"] = "Dokumentas buvo atmestas.";
$MESS["BPT_SM_PARAM_DESC"] = "Naudotojai, kurie dalyvauja balsavime.";
$MESS["BPT_SM_APPROVE_TITLE"] = "Atsakyti d�l dokumento";
$MESS["BPT_SM_MAIL1_TEXT"] = "Balsavimas d�l \"{=Document:NAME}\" atliktas. 

Dokumentas buvo priimtas  {=ApproveActivity1:ApprovedPercent}% bals�.";
$MESS["BPT_SM_MAIL2_TEXT"] = "Balsavimas d�l \"{=Document:NAME}\" atliktas. 

Dokumentas buvo atmestas. 
";
$MESS["BPT_SM_MAIL2_SUBJ"] = "Balsavimas d�l \"{=Document:NAME}: dokumentas buvo atmestas.";
$MESS["BPT_SM_MAIL1_SUBJ"] = "Balsavimas d�l \"{=Document:NAME}: dokumentas buvo patvirtintas.";
$MESS["BPT_SM_PARAM_NAME"] = "Balsuojantys asmenys";
$MESS["BPT_SM_APPROVE_DESC"] = "J�s turite patvirtinti arba atmesti dokument� \"{=Document:NAME}\".

Autorius: {=Document:CREATED_BY_PRINTABLE}";
$MESS["BPT_SM_TASK1_TEXT"] = "J�s turite patvirtinti arba atmesti dokument� \"{=Document:NAME}\".

T�skite atidarant nuorod�: http://#HTTP_HOST##TASK_URL#";
?>