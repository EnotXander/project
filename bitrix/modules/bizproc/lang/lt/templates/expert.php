<?
$MESS["BP_EXPR_NAME"] = "Ekspert� nuomon�";
$MESS["BP_EXPR_DESC"] = "Rekomenduojama situacijoms, kai asmeniui, kuris turi patvirtinti arba atmesti dokument�, reikia ekspert� pastab�. �is procesas sukuria ekspert� grup�, kuri� kiekvienas i�rei�kia savo nuomon� d�l dokumento. Tada nuomon�s pereina asmeniui, kuris priima galutin� sprendim�.";
$MESS["BP_EXPR_S"] = "Nuoseklus verslo procesas";
$MESS["BP_EXPR_TASK1"] = "Dokumentas \"{=Document:NAME}\" reikalauja j�s� komentaro.";
$MESS["BP_EXPR_TASK1_MAIL"] = "J�s� nuomon� yra b�tina, kad galima b�t� priimti sprendim� d�l �io dokumento  \"{=Document:NAME}\".

Atlikite atidarius nuorod�: http://#HTTP_HOST##TASK_URL#";
$MESS["BP_EXPR_M"] = "si�sti el. prane�im�";
$MESS["BP_EXPR_APPR1"] = "Dokumentas \"{=Document:NAME}\" reikalauja j�s� komentaro.";
$MESS["BP_EXPR_APPR1_DESC"] = "J�s� nuomon� yra reikalinga priimti sprendim� d�l �io dokumento  \"{=Document:NAME}\".";
$MESS["BP_EXPR_ST"] = "Veiksm� seka";
$MESS["BP_EXPR_MAIL2_SUBJ"] = "Patvirtinti dokument�: \"{=Document:NAME}\"";
$MESS["BP_EXPR_MAIL2_TEXT"] = "Visi paskirtieji asmenys i�nagrin�jo dokument� ir parei�k� savo nuomon�.
Dabar reikia patvirtinti arba atmesti �� dokument�.";
$MESS["BP_EXPR_APP2_TEXT"] = "Patvirtinti dokument�: \"{=Document:NAME}\"";
$MESS["BP_EXPR_APP2_DESC"] = "Visi paskirtieji asmenys i�nagrin�jo dokument� ir parei�k� savo nuomon�.

{=ApproveActivity1:Comments}

Dabar reikia patvirtinti arba atmesti �� dokument�.";
$MESS["BP_EXPR_TAPP"] = "Patvirtinti dokument�";
$MESS["BP_EXPR_MAIL3_SUBJ"] = "Patvirtinimas \"{=Document:NAME}\": dokumentas yra priimtas.";
$MESS["BP_EXPR_MAIL3_TEXT"] = "D�rybos d�l \"{=Document:NAME}\" u�baigtos, dokumentas  patvirtintas. 

{=ApproveActivity2:Comments}";
$MESS["BP_EXPR_ST3_T"] = "Patvirtintas";
$MESS["BP_EXPR_ST3_TIT"] = "Statusas: patvirtintas";
$MESS["BP_EXPR_PUB"] = "Paskelbti dokument�";
$MESS["BP_EXPR_MAIL4_SUBJ"] = "Svarstymas d�l \"{=Document:NAME}\": dokumentas yra atmestas.";
$MESS["BP_EXPR_MAIL4_TEXT"] = "Svarstymas d�l \"{=Document:NAME}\" atliktas; dokumentas yra atmestas.																									
																										
{=ApproveActivity2:Comments}";
$MESS["BP_EXPR_NA"] = "Atmesta";
$MESS["BP_EXPR_NA_ST"] = "Statusas: Atmestas";
$MESS["BP_EXPR_PARAM2"] = "Ekspertai";
$MESS["BP_EXPR_PARAM2_DESC"] = "Ekspert� grup�, kurios nariai gali pareik�ti savo nuomon� d�l dokumento.";
$MESS["BP_EXPR_PARAM1"] = "Patvirtinantis asmuo";
?>