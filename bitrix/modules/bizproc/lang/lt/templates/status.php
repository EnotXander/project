<?
$MESS["BPT_ST_SEQ"] = "Aktyvumo seka";
$MESS["BPT_ST_CMD_APP"] = "Patvirtinimas";
$MESS["BPT_ST_APPROVE_NAME"] = "Patvirtinti dokument�  \"{=Document:NAME}\"";
$MESS["BPT_ST_NAME"] = "Patvirtinti dokument� su valstybe";
$MESS["BPT_ST_SUBJECT"] = "Patvirtinti dokument�: \"{=Document:NAME}\"";
$MESS["BPT_ST_DESC"] = "Patvirtina valstyb�s skatinam� dokument�  keleto asmen�.";
$MESS["BPT_ST_AUTHOR"] = "Autorius;";
$MESS["BPT_ST_APPROVE_TITLE"] = "Dokumento patvirtinimas";
$MESS["BPT_ST_PUBDC"] = "Dokumento paskelbimas";
$MESS["BPT_ST_ST_DRAFT"] = "Projektas";
$MESS["BPT_ST_MAIL_TITLE"] = "Si�sti el. prane�im�";
$MESS["BPT_ST_F"] = "�vykio apvalkalas";
$MESS["BPT_ST_SAVEH"] = "I�saugojama istorija";
$MESS["BPT_ST_INIT"] = "Inicializuoti b�kl�";
$MESS["BPT_ST_APPROVERS"] = "Asmenys, kurie tvirtins dokument�";
$MESS["BPT_ST_CREATORS"] = "Asmenys, kurie redaguos dokument�";
$MESS["BPT_ST_CMD_PUBLISH"] = "Paskelbti";
$MESS["BPT_ST_TP"] = "Skelbiama";
$MESS["BPT_ST_CMD_APPR"] = "Si�sti patvirtinimui";
$MESS["BPT_ST_SET_DRAFT"] = "Nustatyti projekto status�";
$MESS["BPT_ST_SET_PUB"] = "Nustatyti paskelbimo status�";
$MESS["BPT_ST_SETSTATE"] = "Nustatyti b�kl�";
$MESS["BPT_ST_BP_NAME"] = "Valstyb�s skatinamas verslo procesas";
$MESS["BPT_ST_INS"] = "Statuso inicializavimas";
$MESS["BPT_ST_TEXT"] = "J�s turite patvirtinti arba atmesti �� dokument� \"{=Document:NAME}\".

T�sti atidarydami �i� nuorod�: http://#HTTP_HOST##TASK_URL#";
$MESS["BPT_ST_APPROVE_DESC"] = "J�s turite patvirtinti  �� dokument� \"{=Document:NAME}\":

Dokumento turinys:
{=Document:PREVIEW_TEXT}

Autorius:
{=Document:CREATED_BY_PRINTABLE}";
?>