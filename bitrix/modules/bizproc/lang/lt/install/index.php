<?
$MESS["BIZPROC_INSTALL_DESCRIPTION"] = "Modulis sukurti, valdyti ir vykdyti verslo procesus.";
$MESS["BIZPROC_PERM_D"] = "negalima �eiti";
$MESS["BIZPROC_INSTALL_NAME"] = "Verslo procesai";
$MESS["BIZPROC_INSTALL_TITLE"] = "Modulio �diegimas";
$MESS["BIZPROC_PERM_R"] = "skaityti";
$MESS["BIZPROC_PERM_W"] = "ra�yti";
$MESS["BIZPROC_PHP_L439"] = "J�s naudojate PHP versij�  #VERS#	, bet modulis reikalauja versijos 5.0.0 arba naujesn�s. Atnaujinkite savo �diegt� PHP arba susisiekite su technine pagalba.";
$MESS["BIZPROC_BIZPROCDESIGNER_INSTALLED"] = "Verslo proces� dizainerio modulis �diegtas. Pra�ome j� pa�alinti prie� t�siant.";
?>