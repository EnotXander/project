<?
$MESS["BIZPROC_TAB_RIGHTS"] = "Prieiga";
$MESS["BIZPROC_TAB_RIGHTS_ALT"] = "Modulio prieiga";
$MESS["BIZPROC_TAB_SET_ALT"] = "Modulio nustatymai";
$MESS["BIZPROC_TAB_SET"] = "Nustatymai";
$MESS["BIZPROC_LOG_CLEANUP_DAYS"] = "Dien� skai�ius saugoti verslo proces� �urnal�";
$MESS["BIZPROC_NAME_TEMPLATE"] = "Pavadinimo rodymo formatas";
$MESS["BIZPROC_OPTIONS_NAME_IN_SITE_FORMAT"] = "Svetain�s formatas";
$MESS["BIZPROC_EMPLOYEE_COMPATIBLE_MODE"] = "�jungti suderinamumo re�im� \"susieti su naudotoju\" tipui";
$MESS["BIZPROC_LOG_SKIP_TYPES"] = "Nesaugoti �vyki� �urnale";
$MESS["BIZPROC_LOG_SKIP_TYPES_1"] = "veiskmo prad�ia";
$MESS["BIZPROC_LOG_SKIP_TYPES_2"] = "veiksmo pabaiga";
$MESS["BIZPROC_LIMIT_SIMULTANEOUS_PROCESSES"] = "Maksimalus vienu metu vykdom� proces� skai�ius dokumentui ";
?>