<?
$MESS["BPCGDOC_ADD"] = "Prid�ti";
$MESS["BPCGDOC_INVALID_WF_ID"] = "Nepavyko rasti �ablono verslo procesui #ID#.";
$MESS["BPCGDOC_AUTO_EXECUTE_CREATE"] = "Sukurti";
$MESS["BPCGDOC_AUTO_EXECUTE_DELETE"] = "Pa�alinti";
$MESS["BPCGDOC_AUTO_EXECUTE_NONE"] = "Ne";
$MESS["BPCGDOC_NO"] = "Ne";
$MESS["BPCGDOC_INVALID_WF"] = "Nerasta  darbo verslo proces� �iam dokumentui.";
$MESS["BPCGDOC_EMPTY_WD_ID"] = "Truksta verslo proceso �ablono ID.";
$MESS["BPCGDOC_INVALID_TYPE"] = "Parametro tipas nenurodytas.";
$MESS["BPCGDOC_AUTO_EXECUTE_EDIT"] = "Atnaujinti";
$MESS["BPCGDOC_WAIT"] = "palaukite...";
$MESS["BPCGDOC_YES"] = "Taip";
?>