<?
$MESS["BPCGWTL_CANT_DELETE"] = "Nepavyko pa�alinti verslo proceso �ablon�, nes vienas ar keli aktyv�s verslo procesai egzistuoja naudojant �� �ablon�.";
$MESS["BPCGWTL_UNKNOWN_ERROR"] = "Ne�inoma klaida.";
$MESS["BPCGWTL_INVALID_WF_ID"] = "Verslo proceso �ablono '#ID#' buvo nerasta.";
$MESS["BPCGWTL_EMPTY_TEMPLATE"] = "Verslo proceso �ablono '#ID#' yra  tu��ia.";
$MESS["BPCGWTL_INVALID1"] = " '#NAME#' reik�m� n�ra sveikasis skai�ius.";
$MESS["BPCGWTL_INVALID2"] = " '#NAME#' reik�m� n�ra realus skai�ius.";
$MESS["BPCGWTL_INVALID3"] = " '#NAME#' reik�m� yra neteisinga.";
$MESS["BPCGWTL_INVALID4"] = "Reik�m� #NAME#' nevertinama � \"taip\" arba \"ne\".";
$MESS["BPCGWTL_INVALID5"] = "Datos formatas, panaudotas '#NAME#', neatitinka formato '#FORMAT#'.";
$MESS["BPCGWTL_INVALID6"] = "Naudotojo tipo parametras '#NAME#'  reikalauja, kad b�t� �diegtas Socialinio tinklo modulis. ";
$MESS["BPCGWTL_INVALID7"] = "#NAME#' parametro tipas nenurodytas.";
$MESS["BPCGWTL_INVALID8"] = "Laukas '#NAME#' yra privalomas.";
$MESS["BPCGWTL_WRONG_TEMPLATE"] = "Verslo proceso �ablonas yra neteisingas.";
$MESS["BPWTL_ERROR_MESSAGE_PREFIX"] = "Veiksmas #TITLE#':";
?>