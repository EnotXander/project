<?
$MESS["BPT_TTITLE"] = "Verslo kelion�s ";
$MESS["BPT_BT_PARAM_OP_READ"] = "Darbuotojai, kurie gali per�i�r�ti visus procesus";
$MESS["BPT_BT_PARAM_OP_CREATE"] = "Darbuotojai, kurie gali kurti naujus procesus";
$MESS["BPT_BT_PARAM_OP_ADMIN"] = "Darbuotojai, kurie gali valdyti procesus";
$MESS["BPT_BT_PARAM_BOSS"] = "Darbuotojai, kuriems reikia patvirtinti verslo keliones";
$MESS["BPT_BT_PARAM_BOOK"] = "Buhalterin�s apskaitos skyrius";
$MESS["BPT_BT_PARAM_FORM1"] = "Kelion�s i�laid� suvestin� forma";
$MESS["BPT_BT_PARAM_FORM2"] = "Verslo kelion�s patvirtinimo forma";
$MESS["BPT_BT_P_TARGET"] = "Darbuotojai";
$MESS["BPT_BT_T_PURPOSE"] = "Tikslas";
$MESS["BPT_BT_T_COUNTRY"] = "Paskirties �alis";
$MESS["BPT_BT_T_COUNTRY_DEF"] = "JAV";
$MESS["BPT_BT_T_CITY"] = "Paskirties miestas";
$MESS["BPT_BT_T_DATE_START"] = "Prad�ios data";
$MESS["BPT_BT_T_DATE_END"] = "Planuojama pabaigos data";
$MESS["BPT_BT_T_EXP"] = "Planuojamos i�laidos";
$MESS["BPT_BT_T_TICKETS"] = "Prikabinti failai";
$MESS["BPT_BT_SWA"] = "Nuoseklus verslo procesas";
$MESS["BPT_BT_SFA1_NAME"] = "Verslo kelion�  {=Template:TargetUser_printable}, {=Template:country}-{=Template:city}";
$MESS["BPT_BT_SFA1_TITLE"] = "I�saugoti kelion�s parametrus";
$MESS["BPT_BT_STA1_STATE_TITLE"] = "Projektas";
$MESS["BPT_BT_STA1_TITLE"] = "Nustatyti statuso tekst�";
$MESS["BPT_BT_AA1_NAME"] = "Patvirtinti verslo kelion� {=Template:TargetUser_printable}, {=Template:country} - {=Template:city}";
$MESS["BPT_BT_AA1_DESCR"] = "Verlso kelion� reikalauja patvirtinimo {=Template:TargetUser_printable}

Komandiruot�s �alis: {=Template:country}
Komandiruot�s miestas: {=Template:city}
Datos: {=Template:date_start} - {=Template:date_end}
Planuojamos i�laidos: {=Template:expenditures}

Tikslas:
{=Template:purpose}";
$MESS["BPT_BT_AA1_STATUS_MESSAGE"] = "Laukia patvirtinimo";
$MESS["BPT_BT_AA1_TITLE"] = "Verslo kelion�s patvirtinimas";
$MESS["BPT_BT_SA1_TITLE"] = "Veiksm� seka";
$MESS["BPT_BT_SSTA2_STATE_TITLE"] = "Registruoti verslo kelion�";
$MESS["BPT_BT_SSTA2_TITLE"] = "Nustatyti status�: Registravimas";
$MESS["BPT_BT_SNMA1_TEXT"] = "J�s� verslo kelion� patvirtinta.";
$MESS["BPT_BT_SNMA1_TITLE"] = "Socialinio tinklo prane�imas";
$MESS["BPT_BT_RA1_NAME"] = "Registruoti verslo kelion�  {=Template:TargetUser_printable}, {=Template:country}-{=Template:city}";
$MESS["BPT_BT_RA1_DESCR"] = "�is verslo kelion� buvo patvirtinta ir turi b�ti registruojama apskaitoje.

Darbuotojas: {=Template:TargetUser_printable}

I�vykimo �alis: {=Template:country}
I�vykimo miestas: {=Template:city}
Datos: {=Template:date_start} - {=Template:date_end}
Planuojamos i�laidos: {=Template:expenditures}

Tikslas:
{=Template:purpose}

Prisegti failai:
{=Template:tickets_printable}";
$MESS["BPT_BT_RA1_STATUS_MESSAGE"] = "Registravimas pagal apskait�";
$MESS["BPT_BT_RA1_TBM"] = "Verslo kelion� registruota";
$MESS["BPT_BT_RA1_TITLE"] = "Registravimas pagal apskait�";
$MESS["BPT_BT_RA2_NAME"] = "Verslo kelion�s dokumentacija";
$MESS["BPT_BT_RA2_DESCR"] = "Prie� i�vykdami � verslo kelion�:

1. [url={=Variable:ParameterForm1}]Atsisi�sti [/url] kelion�s i�laid� form�. 

�i forma turi b�ti u�pildyta j�s� kelion�s pabaigoje ir perduota � apskaitos skyri�. 

2 Atminkite, kad bet koks mok�jimas, vykdomas j�s� verslo kelion�s metu, turi b�ti patvirtintas atitinkamu kvitu arba s�skaita.";
$MESS["BPT_BT_RA2_STATUS_MESSAGE"] = "Instrukcijos";
$MESS["BPT_BT_RA2_TBM"] = "Susipa�in�s";
$MESS["BPT_BT_RA2_TITLE"] = "Pa�intis / Patvirtinimas";
$MESS["BPT_BT_RIA1_NAME"] = "Suvestin� ataskaita (pildoma atvykus i� verslo kelion�s)";
$MESS["BPT_BT_RIA1_DESCR"] = "Pokelionin� ataskaita. Pateikite s�naud� ataskait� apskaitos skyriui su originaliais kvitais. Para�ykite santrauk� �emiau pateiktame laukelyje ir �traukite nuorod� � piln� J�s� ataskait�.";
$MESS["BPT_BT_RIA1_DATE_END_REAL"] = "Pabaigos data";
$MESS["BPT_BT_RIA1_REPORT"] = "Suvestin� ataskaita";
$MESS["BPT_BT_RIA1_EXP_REAL"] = "I�laidos";
$MESS["BPT_BT_RIA1_TITLE"] = "Suvestin� ataskaita";
$MESS["BPT_BT_SSTA3_STATE_TITLE"] = "Suvestin� ataskaita";
$MESS["BPT_BT_SSTA3_TITLE"] = "Nustatyti statuso tekstas";
$MESS["BPT_BT_SFA2_TITLE"] = "I�saugoti ataskait�";
$MESS["BPT_BT_RA3_NAME"] = "Skaityti suvestin� ataskait�: {=Template:TargetUser_printable}";
$MESS["BPT_BT_RA3_DESCR"] = "Kelion�s ataskaita nuo  {=Template:TargetUser_printable}

Planuojamos datos: {=Template:date_start} - {=Template:date_end}
Kelion�s dienos: {=Variable:date_end_real}

I�laidos:
{=Variable:expenditures_real}

Tikslas:
{=Template:purpose}

Pokelionin� ataskaitat:
{=Variable:report}";
$MESS["BPT_BT_RA3_STATUS_MESSAGE"] = "Ataskaita vadovybei";
$MESS["BPT_BT_RA3_TBM"] = "Susipa�in�s";
$MESS["BPT_BT_RA4_NAME"] = "Suvestin� ataskaita {=Template:TargetUser_printable}";
$MESS["BPT_BT_RA4_DESCR"] = "Pokelionin� ataskaita apie �emiau nurodyt� kelion� patvirtinta ir turi b�ti �registruota apskaitos skyriuje.

Kelion�s ataskaita nuo {=Template:TargetUser_printable}

Tikslios kelion�s dienos: {=Template:date_start} - {=Variable:date_end_real}

Bendros i�laidos:
{=Variable:expenditures_real}

Suvestin� ataskaita:
{=Variable:report}";
$MESS["BPT_BT_RA4_STATUS_MESSAGE"] = "Registracija apskaitos departamente";
$MESS["BPT_BT_RA4_TMB"] = "Registruotas";
$MESS["BPT_BT_RA4_TITLE"] = "Registracija apskaitos departamente";
$MESS["BPT_BT_SSTA4_STATE_TITLE"] = "Atlikta";
$MESS["BPT_BT_SSTA4_TITLE"] = "Nustatyti statuso tekst�";
$MESS["BPT_BT_SA3_TITLE"] = "Veiksm� seka";
$MESS["BPT_BT_SSTA5_STATE_TITLE"] = "Atmesta vadovyb�s";
$MESS["BPT_BT_SSTA5_TITLE"] = "Nustatyti status�: atmesta";
$MESS["BPT_BT_SNMA2_TEXT"] = "J�s� verslas nebuvo patvirtintas vadovyb�s.";
$MESS["BPT_BT_SNMA2_TITLE"] = "Socialinio tinklo prane�imas";
$MESS["BPT_BT_DF_CITY"] = "Paskirties miestas";
$MESS["BPT_BT_DF_COUNTRY"] = "Paskirties �alis";
$MESS["BPT_BT_DF_TICKETS"] = "Prikabinti failai";
$MESS["BPT_BT_DF_DATE_END_REAL"] = "Realiai baigta";
$MESS["BPT_BT_DF_EXP_REAL"] = "I�laidos";
$MESS["BPT_BT_RA2_TITLE1"] = "Verslo kelion�s dokumentacija";
$MESS["BPT_BT_AA7_TITLE"] = "Nebuvimo grafikas";
$MESS["BPT_BT_AA7_NAME"] = "Verslo kelion�";
$MESS["BPT_BT_AA7_DESCR"] = "Tikslas: {=Template:purpose}";
$MESS["BPT_BT_AA7_STATE"] = "Verslo kelion�";
$MESS["BPT_BT_AA7_FSTATE"] = "Darbai";
?>