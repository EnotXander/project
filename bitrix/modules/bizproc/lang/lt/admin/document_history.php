<?
$MESS["BPADH_RECOVERY_SUCCESS"] = "�ra�as buvo s�kmingai atstatytas i� istorijos.";
$MESS["BPADH_F_AUTHOR_ANY"] = "bet koks";
$MESS["BPADH_DELETE_DOC_CONFIRM"] = "Ar tikrai norite pa�alinti �� �ra��?";
$MESS["BPADH_RECOVERY_DOC_CONFIRM"] = "Ar tikrai norite atstatyti dokument� i� �io �ra�o?";
$MESS["BPADH_AUTHOR"] = "Autorius";
$MESS["BPADH_F_AUTHOR"] = "Autorius";
$MESS["BPADH_RECOVERY_ERROR"] = "Nepavyko atstatyti �ra�o i� istorijos.";
$MESS["BPADH_DELETE_DOC"] = "Pa�alinti �ra��";
$MESS["BPADH_TITLE"] = "Dokumento istorija";
$MESS["BPADH_ERROR"] = "Klaida";
$MESS["BPADH_F_MODIFIED"] = "Pakeista";
$MESS["BPADH_MODIFIED"] = "Pakeista";
$MESS["BPADH_NAME"] = "Pavadinimas";
$MESS["BPADH_RECOVERY_DOC"] = "Atstatyti dokument�";
$MESS["BPADH_NO_ENTITY"] = "Truksta dokumento subjekto.";
$MESS["BPADH_NO_DOC_ID"] = "Truksta dokumento ID.";
$MESS["BPADH_USER_PROFILE"] = "Naudotojo profilis";
$MESS["BPADH_VIEW_DOC"] = "Per�i�r�ti dokument�";
$MESS["BPADH_NO_PERMS"] = "J�s neturite leidimo prieiti prie dokumento istorijos.";
?>