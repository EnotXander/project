<?
$MESS["BPABL_BACK"] = "بازگشت";
$MESS["BPABL_WF_TAB"] = "پروسه کاری";
$MESS["BPABL_TAB_TITLE"] = "پروسه کاری";
$MESS["BPABL_STATUS_4"] = "تکمیل شد";
$MESS["BPABL_STATE_MODIFIED"] = "تاریخ وضعیت فعلی";
$MESS["BPABL_ERROR"] = "خطا";
$MESS["BPABL_STATUS_5"] = "خطا";
$MESS["BPABL_RES_4"] = "خطا";
$MESS["BPABL_STATUS_2"] = "در حال انجام";
$MESS["BPABL_STATUS_1"] = "تعریف شده";
$MESS["BPABL_RES_1"] = "خیر";
$MESS["BPABL_RES_5"] = "تعریف نشده";
$MESS["BPABL_RES_2"] = "موفق";
$MESS["BPABL_STATUS_6"] = "تعیین نشده";
$MESS["BPABL_RES_6"] = "تعیین نشده";
?>