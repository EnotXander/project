<?
$MESS['SEARCH_OPTIONS_MASK_INC'] = '�traukimo kauk�:';
$MESS['SEARCH_OPTIONS_MASK_EXC'] = 'I�skirimo kauk�:';
$MESS['SEARCH_OPTIONS_USE_STEMMING'] = 'Naudoti morfologij�:';
$MESS['SEARCH_OPTIONS_LETTERS'] = 'Simboliai, kurie nenaudojami dokumente kaip �od�i� skirtukai (morfologiniam analiz�s procese)';
$MESS['SEARCH_OPTIONS_SITEMAP'] = 'Google sitemap';
$MESS['SEARCH_OPTIONS_REINDEX'] = 'Tinklapio perindeksavimas';
$MESS['SEARCH_OPTIONS_SITEMAP_TITLE'] = 'Spauskite patekti � Google sitemap suk�rimo form�';
$MESS['SEARCH_OPTIONS_REINDEX_TITLE'] = 'Spauskite rankiniu b�du perindeksuoti tinklap�';
$MESS['SEARCH_OPTIONS_MAX_RESULT_SIZE'] = 'Did�iausias byl� paie�kos rezultate skai�ius:';
$MESS['SEARCH_OPTIONS_REINDEX_MAX_SIZE'] = 'Maksimalus dydis indeksuojamo dokumento (kB):';
$MESS['SEARCH_OPTIONS_PAGE_PROPERTY'] = 'Puslapio savyb�s kodas, kuriame saugomos �ym�s:';
$MESS['SEARCH_OPTIONS_USE_TF_CACHE'] = 'Naudoti greit� paie�k� (supaptastintas reitingavimas):';
?>