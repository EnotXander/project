<?
$MESS['SEARCH_REINDEX_REINDEX_CHANGED'] = 'Perindeksuoti tik pakeitimus:';
$MESS['SEARCH_REINDEX_REINDEX_BUTTON'] = 'Perindeksuoti';
$MESS['SEARCH_REINDEX_TOTAL'] = 'Dokument� perideksuota:';
$MESS['SEARCH_REINDEX_COMPLETE'] = 'Perindeksavimas baigtas.';
$MESS['SEARCH_REINDEX_STEP'] = 'Etapas:';
$MESS['SEARCH_REINDEX_STEP_sec'] = 'sek';
$MESS['SEARCH_REINDEX_TITLE'] = 'Tinklapio perindeksavimas';
$MESS['SEARCH_REINDEX_TAB'] = 'Perindeksavimas';
$MESS['SEARCH_REINDEX_TAB_TITLE'] = 'Perindeksavimo parametrai';
$MESS['SEARCH_REINDEX_SITE'] = 'Tinklapis:';
$MESS['SEARCH_REINDEX_ALL'] = '(visk�)';
$MESS['SEARCH_REINDEX_MODULE'] = 'Modulis:';
$MESS['SEARCH_REINDEX_MAIN'] = 'Statiniai failai';
$MESS['SEARCH_REINDEX_IBLOCKS'] = 'Informaciniai blokai';
$MESS['SEARCH_REINDEX_FORUM'] = 'Forumai';
$MESS['SEARCH_REINDEX_CONTINUE'] = 'T�sti';
$MESS['SEARCH_REINDEX_STOP'] = 'Sustabdyti';
$MESS['SEARCH_REINDEX_BLOG'] = 'Tinklara��iai';
$MESS['SEARCH_REINDEX_IN_PROGRESS'] = 'Perindeksavimas...';
$MESS['SEARCH_REINDEX_NEXT_STEP'] = 'Sekantis �ingsnis';
?>