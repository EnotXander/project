<?
$MESS["LDAP_ERR_NAME"] = "Nombre";
$MESS["LDAP_ERR_SERVER"] = "Direcci�n del Servidor AD/LDAP ";
$MESS["LDAP_ERR_PORT"] = "Puerto del Servidor AD/LDAP ";
$MESS["LDAP_ERR_GROUP_FILT"] = "Filtro del grupo del usuario";
$MESS["LDAP_ERR_GROUP_ATTR"] = "Atributos del identificador del Grupo";
$MESS["LDAP_ERR_USER_FILT"] = "Filtro del Usuario";
$MESS["LDAP_ERR_USER_ATTR"] = "Atributo identificable del usuario";
$MESS["LDAP_ERR_EMPTY"] = "El campo est� lleno:";
$MESS["LDAP_ERR_BASE_DN"] = "Raiz del arbol (DN Base)";
?>