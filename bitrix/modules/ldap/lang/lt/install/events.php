<?
$MESS["LDAP_USER_CONFIRM_TYPE_NAME"] = "Registracijos patvirtinimas";
$MESS["LDAP_USER_CONFIRM_TYPE_DESC"] = "#USER_ID# - naudotojo ID
#EMAIL# - El.adresas
#LOGIN# - Prisijungimo vardas
#XML_ID# - I�orinis ID
#BACK_URL# - Gr��tamasis URL
";
$MESS["LDAP_USER_CONFIRM_EVENT_NAME"] = "#SITE_NAME#: Registracijos patvirtinimas";
$MESS["LDAP_USER_CONFIRM_EVENT_DESC"] = "Sveikinimai nuo #SITE_NAME#!
------------------------------------------
Sveiki,

J�s gavote �� lai�k�, nes j�s (ar kas nors kitas) panaudojote j�s� el.adres� registravimui #SERVER_NAME#.
Nor�dami patvirtinti registracij�, spauskite �i� nuorod� ir �veskite savo vard� ir slapta�od�, kur� naudojate vietiniame tinkle:

http://#SERVER_NAME#/bitrix/admin/ldap_user_auth.php?ldap_user_id=#XML_ID#&back_url=#BACK_URL#

Tai automatinis prane�imas.";
?>