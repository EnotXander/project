<?
$MESS["LDAP_MODULE_NAME"] = "AD/LDAP jungtis";
$MESS["LDAP_MODULE_DESC"] = "AD/LDAP jungties modulis ";
$MESS["LDAP_UNINSTALL_TITLE"] = "AD/LDAP jungties modulio �alinimas";
$MESS["LDAP_UNINSTALL_WARNING"] = "D�mesio!Modulis bus pa�alintas.";
$MESS["LDAP_UNINSTALL_SAVEDATA"] = "Nor�dami i�saugoti duomenis, saugomus duomen� baz�s lentel�se, pa�im�kite \"I�saugoti lenteles\" langel�";
$MESS["LDAP_UNINSTALL_SAVETABLE"] = "I�saugoti lenteles";
$MESS["LDAP_UNINSTALL_DEL"] = "Pa�alinti";
$MESS["LDAP_UNINSTALL_ERROR"] = "Klaida �alinant:";
$MESS["LDAP_UNINSTALL_COMPLETE"] = "�alinimas atliktas.";
$MESS["LDAP_INSTALL_BACK"] = "Gr��ti prie modulio valdymo";
$MESS["LDAP_MOD_INST_ERROR"] = "Klaida diegiant AD/LDAP jungties modul�";
$MESS["LDAP_MOD_INST_ERROR_PHP"] = "Teisingam modulio veikimui turi b�ti �diegta PHP su LDAP moduliu. Pra�ome susisiekti su sistemos administratoriumi.";
$MESS["LDAP_INSTALL_TITLE"] = "AD/LDAP jungties modulio diegimas";
?>