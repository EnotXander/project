<?
$MESS["LDAP_ADMIN_TITLE"] = "Activuoti direktorij� / LDAP serverio nustatymus";
$MESS["LDAP_ADMIN_DEL_ERR"] = "Klaida �alinant serverio nustatymus.";
$MESS["LDAP_ADMIN_F_ACT"] = "Aktyvuoti";
$MESS["LDAP_ADMIN_F_ACT_ANY"] = "(bet kuris)";
$MESS["LDAP_ADMIN_F_NAME"] = "Pavadinimas";
$MESS["LDAP_ADMIN_NAVSTRING"] = "Serveriai";
$MESS["LDAP_ADMIN_TSTAMP"] = "Pakeista";
$MESS["LDAP_ADMIN_NAME"] = "Pavadinimas";
$MESS["LDAP_ADMIN_ACT"] = "Akt.";
$MESS["LDAP_ADMIN_CODE"] = "Mnemoninis kodas";
$MESS["LDAP_ADMIN_SERV"] = "Serveris";
$MESS["SAVE_ERROR"] = "Klaida atnaujinant �ra�� #";
$MESS["LDAP_ADMIN_ACTIONS"] = "Veiksmai";
$MESS["LDAP_ADMIN_CHANGE"] = "Pakeisti serverio nustatymus";
$MESS["LDAP_ADMIN_CHANGE_LINK"] = "Pakeisti ";
$MESS["LDAP_ADMIN_DEL_ALT"] = "Pa�alinti server�";
$MESS["LDAP_ADMIN_DEL_LINK"] = "Pa�alinti ";
$MESS["LDAP_ADMIN_EMPTY"] = "S�ra�as yra tu��ias";
$MESS["LDAP_ADMIN_TOTAL"] = "I� viso:";
$MESS["LDAP_ADMIN_DEL_CONF"] = "Serveris bus pa�alintas,iro visi registruoti per j� naudotojai nebegal�s autorizuotis. Vis tiek pa�alinti?";
$MESS["LDAP_ADMIN_SYNC"] = "Sinchronizuoti";
$MESS["LDAP_ADMIN_SYNC_PERIOD"] = "Sinchronizacijos laikotarpis";
$MESS["LDAP_ADMIN_SYNC_LAST"] = "Paskutin� sinchronizacija";
?>