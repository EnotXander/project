<?
$MESS["LDAP_ERR_NAME"] = "نام";
$MESS["LDAP_FIELD_NAME"] = "نام";
$MESS["LDAP_FIELD_LAST_NAME"] = "نام خانوادگی";
$MESS["LDAP_FIELD_SECOND_NAME"] = "نام مستعار";
$MESS["LDAP_FIELD_GENDER"] = "جنسیت";
$MESS["LDAP_FIELD_BIRTHDAY"] = "تاریخ تولد";
$MESS["LDAP_FIELD_PROF"] = "عنوان شغلی";
$MESS["LDAP_FIELD_WWW"] = "وب سایت";
$MESS["LDAP_FIELD_PHONE"] = "تلفن";
$MESS["LDAP_FIELD_FAX"] = "فکس";
$MESS["LDAP_FIELD_MOB"] = "موبایل";
$MESS["LDAP_FIELD_PAGER"] = "پیجر";
$MESS["LDAP_FIELD_STREET"] = "آدرس";
$MESS["LDAP_FIELD_MAILBOX"] = "صندوق پستی";
$MESS["LDAP_FIELD_CITY"] = "شهر";
$MESS["LDAP_FIELD_STATE"] = "استان/ناحیه";
$MESS["LDAP_FIELD_ZIP"] = "کد پستی";
$MESS["LDAP_FIELD_COUNTRY"] = "کشور";
$MESS["LDAP_FIELD_COMPANY"] = "شرکت";
$MESS["LDAP_FIELD_DEP"] = "بخش";
$MESS["LDAP_FIELD_POS"] = "مکان";
$MESS["LDAP_FIELD_WORK_PHONE"] = "تلفن محل کار";
$MESS["LDAP_FIELD_WORK_FAX"] = "فاکس محل کار";
$MESS["LDAP_FIELD_WORK_PAGER"] = "پیجر محل کار";
$MESS["LDAP_FIELD_ADMIN_NOTES"] = "ملاحظات مدیر سایت";
?>