<?
$MESS["LDAP_ADMIN_F_NAME"] = "نام";
$MESS["LDAP_ADMIN_TSTAMP"] = "ویرایش شده";
$MESS["LDAP_ADMIN_NAME"] = "نام";
$MESS["LDAP_ADMIN_CODE"] = "کد نمونیک";
$MESS["LDAP_ADMIN_ACTIONS"] = "اقدامات";
$MESS["LDAP_ADMIN_CHANGE_LINK"] = "ویرایش";
$MESS["LDAP_ADMIN_DEL_LINK"] = "حذف";
$MESS["LDAP_ADMIN_EMPTY"] = "لیست خالی است";
$MESS["LDAP_ADMIN_TOTAL"] = "مجموع:";
?>