<?
$MESS["LDAP_EDIT_ACT"] = "فعال:";
$MESS["LDAP_EDIT_NAME"] = "نام:";
$MESS["LDAP_EDIT_DESC"] = "توضیحات:";
$MESS["LDAP_EDIT_CODE_ABS"] = "تعیین نشده";
$MESS["LDAP_EDIT_GROUP_MAP_DEL"] = "حذف";
$MESS["LDAP_EDIT_GROUP_MAP_MORE"] = "بیشتر...";
$MESS["LDAP_EDIT_TAB2"] = "گروه ها";
$MESS["LDAP_ERROR"] = "خطا";
$MESS["LDAP_EDIT_USER_FIELDS_DEL"] = "حذف";
$MESS["LDAP_EDIT_USER_FIELDS_ADD"] = "افزودن...";
$MESS["LDAP_EDIT_SYNC_PERIOD_H"] = "ساعت";
?>