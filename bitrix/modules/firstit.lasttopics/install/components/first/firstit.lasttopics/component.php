<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

if($this->StartResultCache(false, array($USER->GetGroups(), $arParams)))
{
	if(!CModule::IncludeModule("forum"))
	{
		$this->AbortResultCache();
		return;
	}

	if(!CModule::IncludeModule("firstit.lasttopics"))
	{
		$this->AbortResultCache();
		return;
	}
	
	if(empty($arParams["SORT_BY"])) $arParams["SORT_BY"]="ID";
	if(empty($arParams["SORT_ORDER"])) $arParams["SORT_ORDER"]="DESC";
	if(empty($arParams["NUM_MESSAGES"])) $arParams["NUM_MESSAGES"]=50;
	if(empty($arParams["FORUM_TOPIC"])) $arParams["FORUM_TOPIC"]="/forum#FID#/topic#TID#/";
	if(empty($arParams["FORUM_MESSAGE"])) $arParams["FORUM_MESSAGE"]="/messages/forum#FID#/topic#TID#/message#MID#/#message#MID#";
	if(empty($arParams["FORUM_MAIN"])) $arParams["FORUM_MAIN"]="/forum";

	$FilterGet=Array();
	if(is_array($arParams["CHOOSE_FORUM"]) && count($arParams["CHOOSE_FORUM"])>0) $FilterGet["@FORUM_ID"]=$arParams["CHOOSE_FORUM"];
	elseif($arParams["CHOOSE_FORUM"]>0) $FilterGet["FORUM_ID"]=$arParams["CHOOSE_FORUM"];
	$FilterGet["APPROVED"]="Y";

	//debug($FilterGet);

	$db_res = CForumMessage::GetList(array($arParams["SORT_BY"]=>$arParams["SORT_ORDER"]), $FilterGet, false, $arParams["NUM_MESSAGES"]);
	while ($ar_res = $db_res->Fetch())
	{
		$parser = new textParser();
		$arAllow = $Forum->ALLOW;                  
		$arAllow["SMILES"] = "Y";
		$arAllow["HTML"] = "Y";
		$arAllow["LIST"] = "Y";
		$arAllow["FONT"] = "Y";
		$arAllow["CODE"] = "Y";
		$arAllow["QUOTE"] = "Y";
		$arAllow["ANCHOR"] = "Y";
		$arAllow["BIU"] = "Y";
		$arAllow["IMG"] = "Y";
		$ar_res["POST_MESSAGE"]=$parser->convert($ar_res["POST_MESSAGE"], $arAllow);

		$ar_res["TOPIC_INFO"]=CFirstitLasttopics::get_topic($ar_res["TOPIC_ID"]);
		
		$DataGet["AUTHOR_NAME"]=$ar_res["AUTHOR_NAME"];
		$DataGet["POST_DATE"]=$ar_res["POST_DATE"];
		$DataGet["TITLE"]=CFirstitLasttopics::better_truncate(strip_tags($ar_res["TOPIC_INFO"]["TITLE"]),$arParams["NUM_MESSAGES_TRUN"]);
		$DataGet["MESSAGE"]=CFirstitLasttopics::better_truncate(strip_tags($ar_res["POST_MESSAGE"]),$arParams["NUM_MESSAGES_TRUN"]);
		$DataGet["TOPIC_URL"]=$arParams["FORUM_MAIN"].CFirstitLasttopics::topic_url($ar_res["FORUM_ID"], $ar_res["TOPIC_ID"], $arParams["FORUM_TOPIC"]);
		$DataGet["MESSAGE_URL"]=$arParams["FORUM_MAIN"].CFirstitLasttopics::message_url($ar_res["FORUM_ID"], $ar_res["TOPIC_ID"], $ar_res["ID"], $arParams["FORUM_MESSAGE"]);
		
		$arResult["ITEMS"][]=$DataGet;
		$DataGet=Array();
	}

	$this->SetResultCacheKeys(array(
		"ITEMS"
	));
	$this->IncludeComponentTemplate();
}
?>