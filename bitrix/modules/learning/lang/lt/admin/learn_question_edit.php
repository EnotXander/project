<?
$MESS['LEARNING_LESSON'] = 'Pamoka';
$MESS['LEARNING_ADMIN_TAB1'] = 'Klausimas';
$MESS['LEARNING_ADMIN_TAB1_EX'] = 'Klausimo nustatymai';
$MESS['LEARNING_ADMIN_TAB2'] = 'Atsakymai';
$MESS['LEARNING_ADMIN_TAB2_EX'] = 'Atsakym� variantai';
$MESS['LEARNING_ADMIN_TAB3'] = 'Apra�ymas';
$MESS['LEARNING_ADMIN_TAB3_EX'] = 'Detalus klausimo apra�ymas';
$MESS['LEARNING_ERROR'] = 'Klaida i�saugojant klausim�';
$MESS['LEARNING_QUESTION_LIST'] = 'Klausim� s�ra�as';
$MESS['LEARNING_QUESTION_NAME'] = 'Klausimas';
$MESS['LEARNING_CONFIRM_DEL_MESSAGE'] = 'Ar tikrai norite pa�alinti �i klausim�?';
$MESS['LEARNING_CONFIRM_CHECK_ANSWER'] = 'Klausimas neturi teising� atsakym�. T�sti?';
?>