<?
$MESS['LEARNING_ADMIN_TITLE'] = 'Bandym� s�ra�as';
$MESS['LEARNING_ADMIN_RESULTS'] = 'Bandymai';
$MESS['LEARNING_ADMIN_MENU_RESULTS'] = 'Rezultatai';
$MESS['LEARNING_ERROR'] = 'Klaida i�saugojant bandym�';
$MESS['SAVE_ERROR'] = 'Klaida atnaujinant bandym� #';
$MESS['LEARNING_ADMIN_DATE_START'] = 'Prad�ios data ';
$MESS['LEARNING_ADMIN_DATE_END'] = 'Pabaigos data';
$MESS['LEARNING_ADMIN_STUDENT'] = 'Studentas';
$MESS['LEARNING_ADMIN_STATUS'] = 'Statusas';
$MESS['LEARNING_ADMIN_SCORE'] = 'Ta�k�';
$MESS['LEARNING_ADMIN_MAX_SCORE'] = 'Maks. ta�k� ';
$MESS['LEARNING_ADMIN_TEST'] = 'Testas';
$MESS['LEARNING_ADMIN_QUESTIONS'] = 'Klausim�';
$MESS['LEARNING_ADMIN_COMPLETED'] = 'Testas baigtas';
?>