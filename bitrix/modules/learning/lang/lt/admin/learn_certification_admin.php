<?
$MESS['LEARNING_ADMIN_TITLE'] = 'Sertifikatas';
$MESS['LEARNING_ADMIN_RESULTS'] = 'Sertifikatai';
$MESS['LEARNING_ERROR'] = 'Klaida i�saugojant sertifikat�';
$MESS['SAVE_ERROR'] = 'Klaida atnaujinant sertifikat� #';
$MESS['LEARNING_ADMIN_APPROVED'] = 'I�laikytas';
$MESS['LEARNING_ADMIN_ATTEMPTS'] = 'Bandym�';
$MESS['LEARNING_ADMIN_STUDENT'] = 'Studentas';
$MESS['LEARNING_ADMIN_COURSE_ID'] = 'Kursas';
$MESS['LEARNING_ADMIN_SUMMARY'] = 'Ta�k�';
$MESS['LEARNING_ADMIN_MAX_SUMMARY'] = 'Maks. ta�k� ';
$MESS['LEARNING_ADMIN_FROM_ONLINE'] = 'Sertifikavosi On-line';
$MESS['LEARNING_ADMIN_ONLINE'] = 'On-line';
$MESS['LEARNING_ADMIN_PUBLIC'] = 'Vie�ai prieinamas studento profilyje';
?>