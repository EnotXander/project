<?
$MESS['LEARNING_TEST_TITLE'] = 'Testo nustatymai';
$MESS['LEARNING_DESC'] = 'Apra�ymas';
$MESS['LEARNING_DESC_TITLE'] = 'Testo apra�ymas';
$MESS['LEARNING_EDIT_TITLE1'] = 'Sukurti nauj� test�';
$MESS['LEARNING_EDIT_TITLE2'] = 'Redaguoti test� ##ID# ';
$MESS['LEARNING_RANDOM_QUESTIONS'] = 'Atsitiktin� klausim� tvarka';
$MESS['LEARNING_RANDOM_ANSWERS'] = 'Atsitiktin� atsakym� tvarka';
$MESS['LEARNING_ATTEMPT_LIMIT'] = 'Bandym� skai�ius';
$MESS['LEARNING_ATTEMPT_LIMIT_HINT'] = '(0 arba palikite tu��i� neribotam skai�iui)';
$MESS['LEARNING_INCLUDE_SELF_TEST'] = '�traukti savaranki�ko patikrinimo klausimus';
$MESS['LEARNING_TIME_LIMIT'] = 'Laiko apribojimai';
$MESS['LEARNING_TIME_LIMIT_HINT'] = 'minu�i� (0 arba tu��ia - be apribojim�)';
$MESS['LEARNING_QUESTIONS_FROM'] = 'Teste dalyvauja';
$MESS['LEARNING_QUESTIONS_FROM_ALL'] = 'visi kurso klausimai';
$MESS['LEARNING_QUESTIONS_FROM_COURSE'] = 'klausim� i� viso kurso';
$MESS['LEARNING_QUESTIONS_FROM_CHAPTERS'] = 'klausim� i� kiekvieno skyriaus';
$MESS['LEARNING_QUESTIONS_FROM_LESSONS'] = 'klausim� i� kiekvienos pamokos';
$MESS['LEARNING_APPROVED'] = 'Automatinis rezultat� tikrinimas';
$MESS['LEARNING_COMPLETED_SCORE'] = 'Tam kad i�laikytum�te test� privalote teisingai atsakyti �';
$MESS['LEARNING_COMPLETED_SCORE2'] = '% klausim�';
$MESS['LEARNING_PASSAGE_TYPE'] = 'Testo sprendimo tipas';
$MESS['LEARNING_PASSAGE_TYPE_0'] = 'U�drausti pereiti prie kito klausimo neatsakius � einam�j� klausim�, <b>negalima</b> pakeisti savo atsakym�.';
$MESS['LEARNING_PASSAGE_TYPE_1'] = 'Leisti pereiti � kit� klausim� neatsakius � einam�j�, <b>negalima</b> pakeisti savo atsakym�.';
$MESS['LEARNING_PASSAGE_TYPE_2'] = 'Leisti pereiti � kit� klausim� neatsakius � einam�j�, <b>galima</b> pakeisti savo atsakymus.';
?>