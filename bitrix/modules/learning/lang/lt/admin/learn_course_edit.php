<?
$MESS['LEARNING_EDIT_TITLE1'] = 'Sukurti nauj� kurs�';
$MESS['LEARNING_EDIT_TITLE2'] = 'Redaguoti kurs� ##ID# ';
$MESS['LEARNING_ADD'] = 'Prid�ti kurs�';
$MESS['LEARNING_ERROR'] = 'Klaida i�saugojant kurs�';
$MESS['LEARNING_ADMIN_TAB1'] = 'Kursas';
$MESS['LEARNING_ADMIN_TAB1_EX'] = 'Kurso nustatymai';
$MESS['LEARNING_ADMIN_TAB2'] = 'Prieiga';
$MESS['LEARNING_ADMIN_TAB2_EX'] = 'Kurso prieigos teis�s ';
$MESS['LEARNING_ADMIN_TAB3'] = 'Anonsas';
$MESS['LEARNING_ADMIN_TAB3_EX'] = 'Anonso apra�ymas';
$MESS['LEARNING_ADMIN_TAB4'] = 'Apra�ymas';
$MESS['LEARNING_ADMIN_TAB4_EX'] = 'Detalus apra�ymas';
$MESS['LEARNING_CONFIRM_DEL_MESSAGE'] = 'Ar tikrai norite i�trinti �� kurs�?';
$MESS['LEARNING_GROUP_ID_TITLE'] = 'Redaguoti naudotoj� grup�';
?>