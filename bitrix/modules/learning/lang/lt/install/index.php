<?
$MESS['LEARNING_MODULE_NAME'] = 'Apmokymas';
$MESS['LEARNING_MODULE_DESC'] = 'Nuotolini� apmokym� modulis ';
$MESS['LEARNING_INSTALL_COPY_PUBLIC'] = 'Kopijuoti vie�osios dalies skriptus';
$MESS['LEARNING_INSTALL_COMPLETE_OK'] = '�diegimas baigtas.';
$MESS['LEARNING_INSTALL_COMPLETE_ERROR'] = 'Diegimas baigtas su klaidom';
$MESS['LEARNING_INSTALL_PUBLIC_SETUP'] = '�diegti';
$MESS['LEARNING_INSTALL_TITLE'] = 'Diegti nuotolini� apmokym� modul� ';
$MESS['LEARNING_UNINSTALL_TITLE'] = 'Nuotolini� apmokym� modulio �alinimas';
$MESS['LEARNING_UNINSTALL_WARNING'] = 'D�mesio! Modulis bus pa�alintas i� sistemos.';
$MESS['LEARNING_UNINSTALL_SAVEDATA'] = 'J�s galite i�saugoti duomenis duomen� baz�s lentel�se, ';
$MESS['LEARNING_UNINSTALL_SAVETABLE'] = 'I�saugoti lenteles';
$MESS['LEARNING_UNINSTALL_DEL'] = 'Pa�alinti';
$MESS['LEARNING_UNINSTALL_ERROR'] = 'Nepavyko i�trinti. �vyko klaida. :';
$MESS['LEARNING_UNINSTALL_COMPLETE'] = '�alinimas �vykdytas.';
$MESS['LEARNING_INSTALL_BACK'] = 'Atgal � moduli� administravim�';
$MESS['LEARNING_INSTALL_TEMPLATE_NAME'] = '�ablono ID';
$MESS['COPY_PUBLIC_FILES'] = 'Kopijuoti tinklapio vie�osios dalies bylas bei tinklapio �ablonus';
?>