<?
$MESS["LEARNING_ADMIN_TAB1"] = "Intentos";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Par�metros de los intentos";
$MESS["LEARNING_ADMIN_TITLE"] = "Editar Intento";
$MESS["LEARNING_ERROR"] = "Error al guardar el intento.";
$MESS["LEARNING_ADMIN_DATE_START"] = "Fecha de Inicio";
$MESS["LEARNING_ADMIN_DATE_END"] = "Fecha Final";
$MESS["LEARNING_ADMIN_COMPLETED"] = "La prueba ha sido finalizada";
$MESS["LEARNING_ADMIN_SCORE"] = "Puntaje";
$MESS["LEARNING_ADMIN_STATUS"] = "Estado";
$MESS["LEARNING_ADMIN_MAX_SCORE"] = "Puntaje M�ximo";
$MESS["LEARNING_ADMIN_USER"] = "Etudiante";
$MESS["LEARNING_ADMIN_TEST"] = "Prueba";
$MESS["LEARNING_ADMIN_QUESTIONS"] = "Preguntas";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "Est� usted seguro que desea borrar este intento?";
$MESS["LEARNING_CHANGE_USER_PROFILE"] = "Editar el Perfil del Usuario";
$MESS["LEARNING_BACK_TO_ADMIN"] = "Regresar A los Intentos";
$MESS["LEARNING_BAD_ATTEMPT_ID_EX"] = "El intento no se encontr� o el acceso fue denegado.";
$MESS["LEARNING_ADMIN_USER_FIELDS"] = "Propiedades Adicionales";
?>