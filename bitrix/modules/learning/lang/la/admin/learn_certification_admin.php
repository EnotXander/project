<?
$MESS["LEARNING_ADMIN_TITLE"] = "Certificaci�n";
$MESS["LEARNING_ADMIN_RESULTS"] = "Certificados";
$MESS["LEARNING_ERROR"] = "Error al guardar el certificado";
$MESS["SAVE_ERROR"] = "Error actualizando el certificado #";
$MESS["LEARNING_ADMIN_APPROVED"] = "Aprobado";
$MESS["LEARNING_ADMIN_ATTEMPTS"] = "Alternativas";
$MESS["LEARNING_ADMIN_STUDENT"] = "Estudiente";
$MESS["LEARNING_ADMIN_COURSE_ID"] = "Curso";
$MESS["LEARNING_ADMIN_SUMMARY"] = "Puntaje";
$MESS["LEARNING_ADMIN_MAX_SUMMARY"] = "M�x. Puntaje";
$MESS["LEARNING_ADMIN_FROM_ONLINE"] = "Cetificado On-Line";
$MESS["LEARNING_ADMIN_ONLINE"] = "On-Line";
$MESS["LEARNING_ADMIN_PUBLIC"] = "Publicado en el perfil de usuario";
?>