<?
$MESS["LEARNING_ADMIN_TITLE"] = "Resultados de la capacitaci�n";
$MESS["LEARNING_ADMIN_RESULTS"] = "Resultados";
$MESS["LEARNING_ERROR"] = "Error al guardar gradebook";
$MESS["SAVE_ERROR"] = "Error al actualizar gradebook #";
$MESS["LEARNING_ADMIN_APPROVED"] = "Aprobado";
$MESS["LEARNING_ADMIN_ATTEMPTS"] = "Alternativas";
$MESS["LEARNING_ADMIN_STUDENT"] = "Estudiantes";
$MESS["LEARNING_ADMIN_TEST"] = "Prueba";
$MESS["LEARNING_ADMIN_RESULT"] = "Puntaje";
$MESS["LEARNING_ADMIN_MAX_RESULT"] = "M�x. Puntaje";
$MESS["MAIN_ADMIN_LIST_COMPLETED"] = "confirmar el resultado";
$MESS["MAIN_ADMIN_LIST_UNCOMPLETED"] = "resultado rechazado";
$MESS["LEARNING_ADMIN_EXTRA_ATTEMPTS"] = "Intentos Extra";
?>