<?
$MESS["LEARNING_INSTALL_BACK"] = "Back to module management section";
$MESS["COPY_PUBLIC_FILES"] = "Copy public files and template for site";
$MESS["LEARNING_INSTALL_COPY_PUBLIC"] = "Copy public section scripts";
$MESS["LEARNING_MODULE_NAME"] = "آموزش الکترونیک";
$MESS["LEARNING_INSTALL_TITLE"] = "e-Learning module installaion";
$MESS["LEARNING_UNINSTALL_TITLE"] = "e-Learning module uninstallation";
$MESS["LEARNING_MODULE_DESC"] = "e-Learning system";
$MESS["LEARNING_INSTALL_PUBLIC_SETUP"] = "نصب";
$MESS["LEARNING_INSTALL_COMPLETE_ERROR"] = "Installation completed with errors";
$MESS["LEARNING_INSTALL_COMPLETE_OK"] = "Installation completed.";
$MESS["LEARNING_UNINSTALL_SAVETABLE"] = "ذخیره جداول";
$MESS["LEARNING_INSTALL_TEMPLATE_NAME"] = "Template ID";
$MESS["LEARNING_UNINSTALL_SAVEDATA"] = "To save the data stored in the database tables, check the &quot;Save tables&quot; checkbox.";
$MESS["LEARNING_UNINSTALL_DEL"] = "Uninstall";
$MESS["LEARNING_UNINSTALL_ERROR"] = "Uninstall errors:";
$MESS["LEARNING_UNINSTALL_COMPLETE"] = "Uninstallation complete.";
$MESS["LEARNING_UNINSTALL_WARNING"] = "Warning! The module will be removed from the system.";
?>