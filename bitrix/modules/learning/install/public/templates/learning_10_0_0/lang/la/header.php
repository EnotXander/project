<?
$MESS["LEARNING_COURSE_DESCRIPTION"] = "Descripci�n del curso";
$MESS["LEARNING_FORWARD"] = "Adelante";
$MESS["LEARNING_BACK"] = "Atr�s ";
$MESS["LEARNING_LOGO_TEXT"] = "e-Learning";
$MESS["LEARNING_PRINT_PAGE"] = "Imprimir esta p�gina";
$MESS["LEARNING_ALL_COURSE_CONTENTS"] = "Todos el contenido del curso";
$MESS["LEARNING_PASS_TEST"] = "Fue examinado ";
$MESS["LEARNING_ALL_LESSONS"] = "Total de lecciones ";
$MESS["LEARNING_GRADEBOOK"] = "Libro de calificaciones";
$MESS["LEARNING_CURRENT_LESSON"] = "Lecciones completadas";
$MESS["LEARNING_EXPAND"] = "Expandir el cap�tulo";
$MESS["LEARNING_COLLAPSE"] = "Ocultar el cap�tulo";
?>