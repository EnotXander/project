<?
$MESS['LEARNING_MODULE_NOT_FOUND'] = 'Apmokym� modulis n�ra �diegtas';
$MESS['LEARNING_TEST_DENIED'] = 'Testas nerastas arba tr�ksta prieigos teisi�.';
$MESS['LEARNING_NO_AUTHORIZE'] = 'Nor�dami per�i�r�ti �� puslap�, �veskite naudotojo vard� ir slapta�od�.';
$MESS['LEARNING_LIMIT_ERROR'] = 'Vir�itas bandym� limitas.';
$MESS['LEARNING_ATTEMPT_CREATE_ERROR'] = 'Klaida bandant sukurti u�duot�.';
$MESS['LEARNING_ATTEMPT_NOT_FOUND_ERROR'] = 'Bandymas nerastas.';
$MESS['LEARNING_RESPONSE_SAVE_ERROR'] = 'Klaida bandant i�saugoti atsakym�.';
$MESS['LEARNING_TIME_LIMIT'] = 'Laikas skirtas testo sprendimui baig�si.';
?>