<?
$MESS['T_LEARNING_DETAIL_ID'] = 'Testo ID';
$MESS['LEARNING_DESC_YES'] = 'Taip';
$MESS['LEARNING_DESC_NO'] = 'Ne';
$MESS['LEARNING_CHECK_PERMISSIONS'] = 'Tikrinti prieigos teises';
$MESS['LEARNING_GRADEBOOK_TEMPLATE_NAME'] = 'URL, vedantis � puslap� su test� rezultatais';
$MESS['LEARNING_PAGE_WINDOW_NAME'] = 'Klausim� kiekis navigacijos grandin�je';
$MESS['LEARNING_SHOW_TIME_LIMIT'] = 'Rodyti laiko apribojimo skaitikl�';
$MESS['T_LEARNING_PAGE_NUMBER_VARIABLE'] = 'Klausimo ID';
$MESS['LEARNING_COURSE_ID'] = 'Kurso identifikatorius';
?>