<?
$MESS["LEARNING_BTN_START"] = "Inicio";
$MESS["LEARNING_BTN_CONTINUE"] = "Continuar";
$MESS["LEARNING_TEST_NAME"] = "Nombre del test";
$MESS["LEARNING_TEST_ATTEMPT_UNLIMITED"] = "n�mero ilimitado";
$MESS["LEARNING_TEST_TIME_LIMIT"] = "Tiempo l�mite";
$MESS["LEARNING_TEST_TIME_LIMIT_UNLIMITED"] = "no l�mite";
$MESS["LEARNING_TEST_TIME_LIMIT_MIN"] = "min.";
$MESS["LEARNING_PASSAGE_NO_FOLLOW_NO_EDIT"] = "no permitir ir a la siguiente pregunta sin contestar la actual. <b>Not allowed</b> para modificar respuestas. ";
$MESS["LEARNING_PASSAGE_FOLLOW_NO_EDIT"] = "permitir ir a la siguiente pregunta sin contestar la actual. <b>Not allowed</b> para modificar respuestas. ";
$MESS["LEARNING_PASSAGE_FOLLOW_EDIT"] = "permitir ir a la siguiente pregunta sin contestar la actual. <b>Allowed</b> para modificar respuestas. ";
$MESS["LEARNING_TEST_ATTEMPT_LIMIT"] = "Alternativas permitidas";
$MESS["LEARNING_PASSAGE_TYPE"] = "Modo de pasar la prueba";
$MESS["LEARNING_PREV_TEST_REQUIRED"] = "Para acceder a esta prueba, usted tiene que pasar la prueba #TEST_LINK# obtener al menos #TEST_SCORE#% del m�ximo total de marcas.";
?>