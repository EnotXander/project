<?
$MESS['PM_POST_FULLY'] = 'Panaudota vietos:';
$MESS['PM_HEAD_NAME_LOGIN'] = 'Vardas:';
$MESS['PM_HEAD_MESS'] = 'Prane�imas:';
$MESS['PM_HEAD_SUBJ'] = 'Tema:';
$MESS['PM_HEAD_MESS_TEXT'] = 'Prane�imo tekstas';
$MESS['PM_ACT_REPLY'] = 'Atsakyti';
$MESS['PM_ACT_EDIT'] = 'Redaguoti';
$MESS['PM_ACT_COPY'] = 'Nukopijuoti';
$MESS['PM_ACT_MOVE'] = 'Perkelti';
$MESS['PM_ACT_DELETE'] = 'Pa�alinti';
$MESS['PM_ACT_FOLDER_SELECT'] = 'aplankas (pasirinkti)';
$MESS['PM_OK'] = 'Gerai';
$MESS['PM_HEAD_SENDER'] = 'Siunt�jas:';
$MESS['PM_HEAD_RECIPIENT'] = 'Gav�jas:';
$MESS['PM_REQUEST_NOTIF'] = 'Siunt�jas u�klaus� lai�ko perskaitymo patvirtinimo.';
$MESS['PM_SEND_NOTIF'] = 'I�si�sti patvirtinim�';
$MESS['PM_AUTH'] = 'Nor�dami per�i�r�ti �� puslap�, �veskite naudotojo vard� ir slapta�od�.';
$MESS['P_PREV'] = 'Ankstesni';
$MESS['P_NEXT'] = 'Sekantis';
$MESS['PM_FROM'] = 'Siunt�jas:';
$MESS['PM_TO'] = 'Gav�jas';
$MESS['PM_DATA'] = 'Data';
$MESS['PM_IN'] = '�';
?>