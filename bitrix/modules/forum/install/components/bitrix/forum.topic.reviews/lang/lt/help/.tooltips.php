<?
$MESS["FORUM_ID_TIP"] = "Forumo ID";
$MESS["IBLOCK_TYPE_TIP"] = " ";
$MESS["IBLOCK_ID_TIP"] = " ";
$MESS["ELEMENT_ID_TIP"] = "Elemento ID";
$MESS["POST_FIRST_MESSAGE_TIP"] = "Prad�ti elemento tekstu";
$MESS["POST_FIRST_MESSAGE_TEMPLATE_TIP"] = "Teksto �ablonas pirmam temos prane�imui";
$MESS["URL_TEMPLATES_READ_TIP"] = "Forumo temos skaitymo puslapis";
$MESS["URL_TEMPLATES_DETAIL_TIP"] = "Informacinio bloko elemento puslapis";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = " ";
$MESS["MESSAGES_PER_PAGE_TIP"] = "Prane�im� skai�ius viename puslapyje";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "Navigacijos grandin�s �ablono pavadinimas";
$MESS["DATE_TIME_FORMAT_TIP"] = "Datos ir laiko atvaizdavimo formatas";
$MESS["PATH_TO_SMILE_TIP"] = "Kelias iki �ypsen�li� katalogo ";
$MESS["USE_CAPTCHA_TIP"] = "Naudoti CAPTCHA";
$MESS["PREORDER_TIP"] = "Atvaizduoti prane�imus tiesiogine tvarka";
$MESS["DISPLAY_PANEL_TIP"] = "Atvaizduoti �ios komponent�s valdymo mygtukus";
$MESS["CACHE_TYPE_TIP"] = "Pod�lio (cache) tipas";
$MESS["CACHE_TIME_TIP"] = "Pod�lio (cache) laikas (sek.)";
?>