<?
$MESS["F_DEFAULT_PATH_TO_ICON"] = "Ruta para el folder de �conos del tema (relativo a la ra�z)";
$MESS["F_DEFAULT_PATH_TO_SMILE"] = "Ruta para la carpeta de Smiles (relativo a la ra�z)";
$MESS["F_DEFAULT_MESSAGE_TYPE"] = "Modo moestrar  formulario de edici�n (respuesta, edici�n, nuevo tema)";
$MESS["F_DEFAULT_FID"] = "ID del foro";
$MESS["F_DEFAULT_TID"] = "ID del tema";
$MESS["F_DEFAULT_PAGE_NAME"] = "ID del llamador del componente";
$MESS["F_DEFAULT_MID"] = "ID del mensaje";
$MESS["F_LIST_TEMPLATE"] = "P�gina de listado de temas";
$MESS["F_MESSAGE_TEMPLATE"] = "P�gina de mensaje visto";
$MESS["F_RULES_TEMPLATE"] = "P�gina de las reglas del foro";
$MESS["F_HELP_TEMPLATE"] = "P�gina ayuda del foro";
$MESS["F_SHOW_VOTE"] = "Mostrar encuestas";
$MESS["F_VOTE_CHANNEL_ID"] = "Grupo de encuesta";
$MESS["F_VOTE_GROUP_ID"] = "Grupos de usuarios permitidos a crear encuestas";
$MESS["F_VOTE_SETTINGS"] = "Configuraciones de la encuesta";
$MESS["F_EDITOR_CODE_DEFAULT"] = "Editor predeterminado en modo texto plano";
?>