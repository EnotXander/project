<?
$MESS["F_CREATE_IN_FORUM"] = "Sukurti nauj� tem� forume";
$MESS["F_REPLY_FORM"] = "Atsakym� forma";
$MESS["F_EDIT_FORM"] = "Prane�imo pakeitimas";
$MESS["F_IN_FORUM"] = "forume";
$MESS["F_IN_TOPIC"] = "temoje";
$MESS["F_TYPE_NAME"] = "�veskite savo vard�";
$MESS["F_TYPE_EMAIL"] = "�veskite savo el. pa�to adres�";
$MESS["F_TOPIC_NAME"] = "Temos pavadinimas";
$MESS["F_TOPIC_DESCR"] = "Temos apra�ymas";
$MESS["F_TOPIC_TAGS"] = "�ym�s:";
$MESS["F_TOPIC_TAGS_DESCRIPTION"] = "Nurodykite �ymes, kurios pilnai apib�dina tem�.";
$MESS["F_TOPIC_ICON"] = "�inut�s veidukas";
$MESS["F_TOPIC_ICON_DESCRIPTION"] = "Pasirinkite piktogram�, kuri bus rodoma �alia temos pavadinimo.";
$MESS["F_EDIT_REASON"] = "Modifikavimo prie�astis:";
$MESS["F_EDIT_ADD_REASON"] = "�raukti informacij� apie redagavim�";
$MESS["F_MESSAGE_TEXT"] = "Prane�imo tekstas";
$MESS["F_BOLD"] = "Pary�kintas tekstas (alt + b)";
$MESS["F_ITAL"] = "Pakreiptas tekstas (alt + i)";
$MESS["F_STRIKE"] = "Perbrauktasis (alt+s)";
$MESS["F_UNDER"] = "Pabrauktas tekstas (alt + u)";
$MESS["F_FONT"] = "�riftas";
$MESS["F_COLOR_TITLE"] = "Spalva";
$MESS["F_FONT_TITLE"] = "�riftas";
$MESS["F_IMAGE_TITLE"] = "�terpti paveiksl� (alt + g)";
$MESS["F_VIDEO_TITLE"] = "�terpti vaizdo �ra�� (alt+v)";
$MESS["F_QUOTE_TITLE"] = "Cituoti (alt+q)";
$MESS["F_CODE_TITLE"] = "Kodinis re�imas (alt + p)";
$MESS["F_LIST_TITLE"] = "Sukurti s�ra�� (alt+l)";
$MESS["F_TRANSLIT_TITLE"] = "Translito / lotyni�kos ab�c�l�s perkodavimas (alt + t)";
$MESS["F_HYPERLINK_TITLE"] = "�terpti nuorod� (alt + h)";
$MESS["F_WANT_ALLOW_SMILES"] = "Ar norite <B>naudoti</B> �ypsniukus �iame prane�ime?";
$MESS["F_WANT_SUBSCRIBE_TOPIC"] = "Prenumeruoti naujas �ios temos �inutes";
$MESS["F_WANT_SUBSCRIBE_FORUM"] = "Prenumeruoti naujus prane�imus �iame forume";
$MESS["F_CAPTCHA_TITLE"] = "�veskite �od�, kur� matote paveiksl�lyje";
$MESS["F_CAPTCHA_PROMT"] = "�veskite tekst� i� paveiksl�lio";
$MESS["F_LOAD_IMAGE"] = "�kelkite nuotrauk� �iam prane�imui";
$MESS["F_LOAD_FILE"] = "�kelkite pried� �iam prane�imui";
$MESS["F_FILE_EXTENSION"] = "Leid�iami failai: #EXTENSION#.";
$MESS["F_FILE_SIZE"] = "failo dydis neturi vir�yti #SIZE#";
$MESS["F_VIEW"] = "Per�i�ra";
$MESS["FORUM_BUTTON_OK"] = "�terpti";
$MESS["FORUM_BUTTON_CANCEL"] = "Nutraukti";
$MESS["JQOUTE_AUTHOR_WRITES"] = "ra�o";
$MESS["JERROR_NO_TOPIC_NAME"] = "Turite �vesti pavadinim�";
$MESS["JERROR_NO_MESSAGE"] = "Turite �vesti prane�im�.";
$MESS["JERROR_MAX_LEN"] = "Did�iausias leid�iamas prane�imo ilgis yra #MAX_LENGTH# simboli�. I� viso simboli�: #LENGTH#.";
$MESS["FORUM_TEXT_ENTER_URL"] = "�veskite vis� adres� (URL)";
$MESS["FORUM_TEXT_ENTER_URL_NAME"] = "�ra�ykite svetain�s pavadinim�";
$MESS["FORUM_TEXT_ENTER_IMAGE"] = "�veskite piln� vaizdo adres� (URL)";
$MESS["FORUM_LIST_PROMPT"] = "�veskite s�ra�o element�. Spustel�kite �At�aukti� arba palikite tarpo simbol�, jei norite u�baigti s�ra��";
$MESS["FORUM_ERROR_NO_URL"] = "J�s turite �vesti adres� (URL)";
$MESS["FORUM_ERROR_NO_TITLE"] = "Turite �vesti pavadinim�";
$MESS["FORUM_ERROR_NO_PATH_TO_VIDEO"] = "Nenurodytas mar�rutas � vaizdo klip�.";
$MESS["FORUM_VIDEO"] = "Vaizdas";
$MESS["FORUM_PATH"] = "Kelias";
$MESS["FORUM_PREVIEW"] = "Per�i�r�ti nuotrauk� (http://)";
$MESS["FORUM_WIDTH"] = "Plotis";
$MESS["FORUM_HEIGHT"] = "Auk�tis";
$MESS["F_KB"] = " KB";
$MESS["F_MB"] = "Mb";
$MESS["F_B"] = " baitas";
$MESS["F_RULES"] = "Taisykl�s";
$MESS["F_DOWNLOAD"] = "Atsisi�sti";
$MESS["F_HIDE_SMILE"] = "Sl�pti";
$MESS["F_SHOW_SMILE"] = "Daugiau";
$MESS["F_ADD_VOTE"] = "Prid�ti balsavim�";
$MESS["F_VOTE"] = "Balsavimas";
$MESS["F_VOTE_ADD_ANSWER"] = "Prid�ti atsakym�";
$MESS["F_VOTE_DROP_ANSWER"] = "Pa�alinti atsakym�";
$MESS["F_VOTE_DROP_ANSWER_CONFIRM"] = "Ar tikrai norite i�trinti atsakym�?";
$MESS["F_VOTE_ADD_QUESTION"] = "Prid�ti klausim�";
$MESS["F_VOTE_DROP_QUESTION"] = "Pa�alinti klausim�";
$MESS["F_VOTE_DROP_QUESTION_CONFIRM"] = "Ar tikrai norite i�trinti klausim�?";
$MESS["F_VOTE_MULTI"] = "Leidti pasirinkti kelis atsakymus";
?>