<?
$MESS["TFD_TOPIC_LIST"] = "Forumo tem� s�ra�as";
$MESS["TFD_TOPIC_LIST_DESCR"] = "Rodyti forumo tem� s�ra��";
$MESS["TFD_PARAM_FID_NAME"] = "Forumas";
$MESS["TFD_PARAM_NUM_NAME"] = "Rodom� tem� skai�ius";
$MESS["TFD_PARAM_ORDER_BY_NAME"] = "R��iuojama";
$MESS["TFD_PARAM_ORDER_DIRECTION_NAME"] = "R��iavimo tvarka";
$MESS["TFD_PARAM_PATH2MESSAGES_NAME"] = "Mar�rutas � prane�imo puslap�";
$MESS["TFD_PARAM_SORT_TOPIC"] = "Tema";
$MESS["TFD_PARAM_SORT_POST"] = "Prane�im� skai�ius";
$MESS["TFD_PARAM_SORT_AUTHOR"] = "Temos autorius";
$MESS["TFD_PARAM_SORT_VIEWS"] = "Per�i�r�jim� skai�ius";
$MESS["TFD_PARAM_SORT_DATE"] = "Data";
$MESS["TFD_PARAM_SORT_POSTDATE"] = "Paskutinio prane�imo data";
$MESS["TFD_PARAM_SO_ASC"] = "Did�jimo tvarka";
$MESS["TFD_PARAM_SO_DESC"] = "Ma��jimo tvarka";
$MESS["TFD_COMMENT"] = "Klausimo forma";
$MESS["TFD_COMMENT_DESCR"] = "Klausimo forma vartotojo prane�im� i�saugo pasirinktame forume";
$MESS["TFD_PARAM_PRODUCT_ID_NAME"] = "Informacijos bloko elemento ID";
$MESS["TFD_REVIEW"] = "Informacijos bloko elemeno per�i�ros";
$MESS["TFD_REVIEW_DESCR"] = "Informacijos bloko elemento per�i�ros";
$MESS["TFD_PARAM_CACHE_TIME_NAME"] = "Talpinimo laikas (sek.)";
?>