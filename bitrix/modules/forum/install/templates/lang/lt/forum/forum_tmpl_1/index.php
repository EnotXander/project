<?
$MESS["FI_FORUM"] = "Forumai";
$MESS["FI_FORUM_LIST"] = "Forum� s�ra�as";
$MESS["FI_FORUM_NAME"] = "Forumo pavadinimas";
$MESS["FI_FORUM_TOPICS"] = "Temos";
$MESS["FI_FORUM_MESS"] = "Prane�imai";
$MESS["FI_FORUM_LAST_MESS"] = "Paskutinio prane�imo informacija";
$MESS["FI_HAVE_NEW_MESS"] = "Nauji prane�imai!";
$MESS["FI_NO_NEW_MESS"] = "Nauj� prane�im� n�ra";
$MESS["FI_TOPIC"] = "tema:";
$MESS["FI_AUTHOR"] = "autorius:";
$MESS["FI_NOW_ONLINE"] = "Aktyv�s vartotoji per paskutines 10 minu�i�";
$MESS["FI_USER_PROFILE"] = "Vartotojo profilis";
$MESS["FI_NONE"] = "n�ra";
$MESS["FI_MARK_AS_READED"] = "Visus prane�imus �ym�ti kaip perskaitytus";
$MESS["FI_MARK_AS_READED_DO"] = "[Visus prane�imus �ym�ti kaip perskaitytus]";
$MESS["FI_NO_MODULE"] = "Forumo modulis ne�diegtas";
$MESS["FL_MESSAGE_NOT_APPROVED"] = "Nepatvirtinti prane�imai";
$MESS["FI_TODAY_BIRTHDAY"] = "�ios dienos gimtadieniai";
?>