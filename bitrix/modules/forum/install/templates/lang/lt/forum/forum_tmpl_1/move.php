<?
$MESS["FM_TOPIC_MOVEMENT"] = "Tema perkeliama � kit� forum�";
$MESS["FM_MOVE_TOPIC"] = "Tema perkeliama � kit� forum�";
$MESS["FM_MOVE"] = "Perkelti tem� � forum�";
$MESS["FM_NO_MODULE"] = "Forumo modulis ne�diegtas";
$MESS["FM_NO_AUTHORIZE"] = "J�s neturite teisi�";
$MESS["FM_NO_FPERMS"] = "Neturite pakankamai teisi� �iai temai perkelti";
$MESS["FM_EMPTY_DEST_FORUM"] = "Ne�inomas paskirties forumas";
$MESS["FM_NO_DEST_FPERMS"] = "Neturite pakankamai teisi� �iai temai perkelti";
$MESS["FM_ERR_MOVE_TOPIC"] = "Perkeliant tem� �vyko klaida";
$MESS["FM_MOVE_TITLE"] = "Tema perkeliama � kit� forum�";
$MESS["FM_MAKE_NEW"] = "Prane�imus �ym�ti kaip naujus";
$MESS["FL_TOPIC_NAME"] = "Temos pavadinimas";
$MESS["FL_TOPIC_AUTHOR"] = "Temos autorius";
$MESS["FL_TOPIC_POSTS"] = "Atsakymai";
$MESS["FL_TOPIC_VIEWS"] = "Skaityti";
$MESS["FL_TOPIC_LAST_MESS"] = "Paskutinis prane�imas";
$MESS["FL_PINNED"] = "Prikabinta";
$MESS["FL_TOPIC_START"] = "Tema prad�ta";
$MESS["FL_TOPIC_AUTHOR1"] = "Autorius:";
$MESS["FL_FORUM"] = "Forumas:";
$MESS["FL_TOPICS_N"] = "&nbsp;Temos:";
?>