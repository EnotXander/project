<?
$MESS["FS_SEARCH"] = "Paie�ka";
$MESS["FS_KEYWORDS"] = "Paie�kos rakta�od�iai:";
$MESS["FS_FORUM"] = "Kur ie�koti:";
$MESS["FS_ALL_FORUMS"] = "Visi forumai";
$MESS["FS_DO_SEARCH"] = "Paie�ka";
$MESS["FS_PHRASE_ERROR"] = "Paie�kos u�klausoje yra klaida:";
$MESS["FS_PHRASE_ERROR_CORRECT"] = "Pataisykite paie�kos u�klaus� ir bandykite dar kart�.";
$MESS["FS_PHRASE_ERROR_SYNTAX"] = "Paie�kos u�klausos sintaks�:";
$MESS["FS_SEARCH_DESCR"] = "Bendr� paie�kos u�klaus� sudaro vienas ar daugiau �od�i�: <br/><i>kontaktin� informacija</i><br/>�i paie�kos u�klausa ras puslapius, kuriuose yra abu u�klausos �od�iai. <br/><br/>Loginiai operatoriai leid�ia sudaryti sud�tingesnes u�klausas, pavyzd�iui: <br/> <i>kontaktin� informacija arba telefonas</i><br/>�i paie�kos u�klausa ras puslapius su dviem �od�iais &quot;kontaktin�&quot; ir &quot;informacija&quot; arba su vienu �od�iu &quot;telefonas&quot;.<br/><br/> <i>kontaktin� informacija ne telefonas</i><br/>�i paie�kos u�klausa ras puslapius, kuriuose yra abu �od�iai &quot;kontaktin�&quot; ir &quot;informacija&quot;, bet ne �odis &quot;telefonas&quot;.<br/>Sud�tingesnes u�klausas taip pat galima sukurti naudojant skliaustelius.<br/><br/> <b>Loginiai operatoriai:</b> <table border=\"0\" cellpadding=\"5\"><tr><td align=\"center\" valign=\"top\">Operatorius</td><td valign=\"top\">Sinonimai</td><td>Apra�ymas</td></tr><tr><td align=\"center\" valign=\"top\">ir</td><td valign=\"top\">ir &amp;, +</td><td><i>Loginis &quot;ir&quot;</i> operatorius gali b�ti arba gali b�ti praleid�iamas: u�klausa &quot;kontaktin� informacija&quot; atitinka u�klaus� &quot;kontaktin� ir informacija&quot;.</td></tr><tr><td align=\"center\" valign=\"top\">or</td><td valign=\"top\">arba |</td><td><i>loginis &quot;arba&quot;</i> operatorius atlieka produkt� paie�k� bent su vienu i� operand�. </td></tr><tr><td align=\"center\" valign=\"top\">ne</td><td valign=\"top\">ne, ~</td><td><i>loginis &quot;ne&quot;</i> operatorius paie�k� apriboja iki puslapi�, kuriuose n�ra operando. </td></tr><tr><td align=\"center\" valign=\"top\">( )</td><td valign=\"top\">&nbsp;</td><td><i>Apval�s skliaustai</i> apibr��ia loginio operatoriaus vykdymo sek�. </font></td></tr></table></font>";
$MESS["FS_SEARCH_RESULTS"] = "Paie�kos rezultatai";
$MESS["FS_EMPTY"] = "Jokie dokumentai neatitiko j�s� paie�kos. Pabandykite naudoti kitus rakta�od�ius.";
$MESS["FS_NO_SEARCH_MODULE"] = "Paie�kos modulis ne�diegtas.";
$MESS["FS_NO_MODULE"] = "Forumo modulis ne�diegtas";
$MESS["FS_FTITLE"] = "Paie�ka";
?>