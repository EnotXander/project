<?
$MESS["FORUM_PE_ERROR_UPDATE"] = "Error mientras acrualizaba el puntaje";
$MESS["FORUM_PE_ERROR_ADD"] = "Error al agregar la puntuaci�n";
$MESS["FORUM_PE_TITLE_UPDATE"] = "Modificar puntaje ##ID#";
$MESS["FORUM_PE_TITLE_ADD"] = "A�adir una nueva puntuaci�n";
$MESS["FORUM_PE_MIN_POINTS"] = "N�mero m�nimo de puntos para dar la puntuaci�n";
$MESS["FORUM_PE_MNEMOCODE"] = "C�digo mnemot�cnico";
$MESS["FORUM_PE_VOTES"] = "N�mero de votos";
$MESS["FORUM_PE_NAME"] = "Nombre";
$MESS["FPN_2FLIST"] = "Rangos";
$MESS["FPN_NEW_POINT"] = "Nuevo rango";
$MESS["FPN_DELETE_POINT"] = "Borrar rango";
$MESS["FPN_DELETE_POINT_CONFIRM"] = "�Est�s seguro de que deseas eliminar este rango?";
$MESS["FPN_TAB_POINT"] = "Rango del foro";
$MESS["FPN_TAB_POINT_DESCR"] = "Rango del foro";
$MESS["FORUM_PE_RATING_VOTES"] = "Votos Requeridos Normalizados para Obtener el Rango";
$MESS["FORUM_PE_RATING_VALUE"] = "Autoridad Requerida para Obtener el Rango";
?>