<?
$MESS["FLOW_PAGE_TITLE"] = "تغییر سابقه";
$MESS["FLOW_F_DOCUMENT"] = "شناسه فایل:";
$MESS["FLOW_F_FILENAME"] = "نام فایل:";
$MESS["FLOW_F_TITLE"] = "عنوان:";
$MESS["FLOW_F_STATUS"] = "وضعیت فایل:";
$MESS["FLOW_F_DATE_MODIFY"] = "ویرایش شده";
$MESS["FLOW_F_MODIFIED_BY"] = "ویرایش شده با (شناسه):";
$MESS["FLOW_F_BODY"] = "محتوا:";
$MESS["FLOW_PAGES"] = "تغییرات";
$MESS["FLOW_DATE_MODIFY"] = "ویرایش شده";
$MESS["FLOW_MODIFIED_BY"] = "ویرایش شده با";
$MESS["FLOW_TITLE"] = "عنوان";
$MESS["FLOW_FILENAME"] = "نام فایل";
$MESS["FLOW_STATUS"] = "وضعیت";
$MESS["FLOW_F_LOGIC"] = "منطق بین فیلدها";
$MESS["FLOW_VIEW"] = "مشاهده";
$MESS["FLOW_DOCUMENT"] = "شناسه فایل";
$MESS["FLOW_DELETE_CONFIRM"] = "آیا این رکورد از سابقه فایل حذف شود؟";
$MESS["FLOW_USER_ALT"] = "View user information";
$MESS["FLOW_STATUS_ALT"] = "View status information";
$MESS["FLOW_COMPARE"] = "مقایسه";
$MESS["FLOW_COMPARE_ALERT"] = "دو فایل را برای تحلیل انتخاب کنید.";
$MESS["FLOW_DELETE"] = "حذف";
?>