<?
$MESS["FLOW_MENU_MAIN"] = "گردش کار";
$MESS["FLOW_MENU_DOCUMENTS"] = "فایلها";
$MESS["FLOW_MENU_DOCUMENTS_ALT"] = "Documents in the workflow processing";
$MESS["FLOW_MENU_STAGE"] = "وضعیتها";
$MESS["FLOW_MENU_STAGE_ALT"] = "Possible statuses for document";
$MESS["FLOW_MENU_HISTORY"] = "سابقه";
$MESS["FLOW_MENU_HISTORY_ALT"] = "Document change history";
$MESS["FLOW_MENU_MAIN_TITLE"] = "مدیریت فایلها";
?>