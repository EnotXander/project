<?
$MESS ['FLOW_CANNOT_DELETE_DOCUMENT'] = "Cannot access the document # #ID#";
$MESS ['FLOW_PAGE_TITLE'] = "Document list";
$MESS ['FLOW_F_ID'] = "Document ID:";
$MESS ['FLOW_F_FILENAME'] = "File name:";
$MESS ['FLOW_F_TITLE'] = "Title:";
$MESS ['FLOW_F_LOCK_STATUS'] = "Document access indicator:";
$MESS ['FLOW_RED'] = "red";
$MESS ['FLOW_YELLOW'] = "yellow";
$MESS ['FLOW_GREEN'] = "green";
$MESS ['FLOW_F_STATUS'] = "Status:";
$MESS ['FLOW_F_DATE_MODIFY'] = "Modified";
$MESS ['FLOW_F_MODIFIED_BY'] = "Modified by:";
$MESS ['FLOW_PAGES'] = "Documents";
$MESS ['FLOW_LOCK_STATUS'] = "Ind.";
$MESS ['FLOW_DATE_MODIFY'] = "Modified";
$MESS ['FLOW_MODIFIED_BY'] = "Modified by";
$MESS ['FLOW_TITLE_SHORT'] = "T";
$MESS ['FLOW_TITLE'] = "Title";
$MESS ['FLOW_FILENAME_SHORT'] = "F";
$MESS ['FLOW_FILENAME'] = "File name";
$MESS ['FLOW_STATUS_SHORT'] = "S";
$MESS ['FLOW_STATUS'] = "Status";
$MESS ['FLOW_GREEN_ALT'] = "modifiable";
$MESS ['FLOW_YELLOW_ALT'] = "locked by you (do not forget to unlock)";
$MESS ['FLOW_RED_ALT'] = "temporarily locked (being edited at the moment)";
$MESS ['FLOW_EDIT_ALT'] = "Edit document settings";
$MESS ['FLOW_DELETE_ALT'] = "Remove document from the workflow";
$MESS ['FLOW_DELETE_CONFIRM'] = "Are you sure you want to remove document from the workflow?";
$MESS ['FLOW_UNLOCK'] = "unlock";
$MESS ['FLOW_UNLOCK_ALT'] = "Unlock the document";
$MESS ['FLOW_UNLOCK_CONFIRM'] = "Are you sure you want to unlock the document?";
$MESS ['FLOW_HISTORY'] = "History";
$MESS ['FLOW_HISTORY_ALT'] = "Document change history";
$MESS ['FLOW_DOCUMENT_LOCKED'] = "The document # #DID# is temporarily locked by the user # #ID# (#DATE#)";
$MESS ['FLOW_DOCUMENT_IS_NOT_AVAILABLE'] = "The document # #ID# cannot be modified";
$MESS ['FLOW_VIEW_ALT'] = "View document preferences";
$MESS ['FLOW_VIEW'] = "View";
$MESS ['FLOW_PREVIEW'] = "Preview";
$MESS ['FLOW_PREVIEW_ALT'] = "Document preview";
$MESS ['FLOW_F_BODY'] = "Content:";
$MESS ['FLOW_LIST_SITE'] = "Site:";
$MESS ['FLOW_LIST_SITE_ALL'] = "(all)";
$MESS ['FLOW_LIST_SITE_ALT'] = "Site";
$MESS ['FLOW_SITE'] = "Site";
$MESS ['FLOW_SITE_SHORT'] = "S";
$MESS ['FLOW_UNLOCK_S'] = "unlock";
$MESS ['FLOW_F_SITE'] = "Site";
$MESS ['FLOW_F_LOGIC'] = "Logic between fields";
$MESS ['FLOW_FILE_VIEW'] = "View file";
$MESS ['FLOW_USER_EDIT'] = "Edit user profile";
$MESS ['FLOW_USER_ACTION'] = "Actions";
$MESS ['FLOW_SITE_EDIT'] = "Edit site settings";
$MESS ['FLOW_M_ADD'] = "New document";
$MESS ['FLOW_M_ADD_ALT'] = "Add new document";
?>