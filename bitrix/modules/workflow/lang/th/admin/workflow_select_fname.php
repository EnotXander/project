<?
$MESS ['FLOW_FILE_NAME'] = "File name:";
$MESS ['FLOW_UP'] = "Up one level";
$MESS ['FLOW_REFRESH'] = "Refresh file list";
$MESS ['FLOW_UNDEFINED'] = "(undefined)";
$MESS ['FLOW_ROOT'] = "site root";
$MESS ['FLOW_SAVE_IN'] = "Save to";
$MESS ['FLOW_ERROR_ENTER_FILENAME'] = "Please enter file name!";
$MESS ['FLOW_REWRITE'] = "File with the specified name already exists! Overwrite?";
?>
