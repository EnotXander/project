<?
$MESS["FLOW_SAVE"] = "I�saugoti";
$MESS["FLOW_RESET"] = "Atstatyti";
$MESS["FLOW_USE_HTML_EDIT"] = "Naudokite vaizdin� HTML redaktori�";
$MESS["FLOW_MAX_LOCK"] = "U�rakinti fail� redagavimui (min.):";
$MESS["FLOW_ADMIN"] = "Darbo proceso administratori� grup�:";
$MESS["FLOW_DAYS_AFTER_PUBLISHING"] = "Skai�ius, kiek dien� saugoti paskutin� failo versij�: <br> (neigiamos reik�m�s - saugoti neribotai):";
$MESS["FLOW_HISTORY_COPIES"] = "Maksimalus failo versij� skai�ius:";
$MESS["FLOW_HISTORY_DAYS"] = "Skai�ius, kiek dien� saugoti ankstesnes failo versijas: <br> (neigiamos reik�m�s - saugoti neribotai):";
$MESS["FLOW_CLEAR"] = "Pa�alinti dabar";
$MESS["FLOW_HISTORY_SIMPLE_EDITING"] = "Saugoti fail�, redaguot� tiesiogiai, versijos istorij� (ne Darbo eigos modulyje)";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Numatytieji nustatymai";
?>