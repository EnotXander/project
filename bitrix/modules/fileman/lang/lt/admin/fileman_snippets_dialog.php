<?
$MESS['FILEMAN_ED_SAVE'] = 'I�saugoti';
$MESS['FILEMAN_ED_CANC'] = 'At�aukti';
$MESS['FILEMAN_ED_ADD_SNIPPET'] = 'Prid�ti snipet�';
$MESS['FILEMAN_ED_EDIT_SNIPPET'] = 'Redaguoti snipet�';
$MESS['FILEMAN_ED_NAME'] = 'Bylos pavadinimas';
$MESS['FILEMAN_ED_TITLE'] = 'Pavadinimas';
$MESS['FILEMAN_ED_DESCRIPTION'] = 'Apra�ymas';
$MESS['FILEMAN_ED_TEMPLATE'] = '�ablonas';
$MESS['FILEMAN_ED_CODE'] = 'Snipeto kodas';
$MESS['FILEMAN_ED_FILE_EXISTS'] = 'Byla tokiu pavadinimu jau egzistuoja. Perra�yti byl�?';
$MESS['FILEMAN_ED_BASE_PARAMS'] = 'Baziniai parametrai';
$MESS['FILEMAN_ED_LOCATION'] = 'Vieta';
$MESS['FILEMAN_ED_ADD_PARAMS'] = 'Papildomi parametrai';
$MESS['FILEMAN_ED_SN_IMAGE'] = 'Paveiksl�lis';
$MESS['FILEMAN_ED_FILE_LOCATION'] = 'Vieta';
$MESS['FILEMAN_ED_CREATE_SUBGROUP'] = 'Sukurti nauj� grup�';
$MESS['FILEMAN_ED_SUBGROUP_NAME'] = 'Naujos grup�s pavadinimas';
$MESS['FILEMAN_ED_SN_DEL_IMG'] = '�alinti paveiksl�';
$MESS['FILEMAN_ED_SN_NEW_IMG'] = 'Naujas paveikslas';
$MESS['FILEMAN_ED_WRONG_PARAMS'] = 'Neteisingi snipeto parametrai';
$MESS['FILEMAN_ED_WRONG_PARAM_TITLE'] = 'Ne�vestas snipeto pavadinimas arba �vestas neteisingas';
$MESS['FILEMAN_ED_WRONG_PARAM_CODE'] = 'Ne�vestas snipeto kodas arba �vestas neteisingas';
$MESS['FILEMAN_ED_WRONG_PARAM_SUBGROUP'] = 'Klaida: pogrupis tokiu pavadinimu jau yra';
$MESS['FILEMAN_ED_WRONG_PARAM_SUBGROUP2'] = 'Snipet� struk�ra negali tur�ti daugiau 2 lygi�';
?>