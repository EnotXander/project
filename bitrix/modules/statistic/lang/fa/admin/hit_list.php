<?
$MESS["STAT_EDIT_USER"] = "پروفایل کاربر";
$MESS["STAT_F_IP"] = "آدرس آی.پی";
$MESS["STAT_F_PAGE"] = "صفحه";
$MESS["STAT_HIT_PAGES"] = "بازدید صفحه";
$MESS["STAT_NOT_REGISTERED"] = "ثبت نام نشده";
$MESS["STAT_SESSION"] = "نشست";
$MESS["STAT_DATE"] = "تاریخ";
$MESS["STAT_USER"] = "بیننده";
$MESS["STAT_IP"] = "آدرس آی.پی";
$MESS["STAT_PAGE"] = "صفحه";
$MESS["STAT_NOT_AUTH"] = "(وارد سایت نشده)";
$MESS["STAT_COUNTRY"] = "کشور";
$MESS["STAT_CITY"] = "شهر";
$MESS["STAT_F_COUNTRY"] = "کشور";
$MESS["STAT_F_REGION"] = "ناحیه";
$MESS["STAT_F_CITY"] = "شهر";
$MESS["STAT_F_COOKIE"] = "کوکی";
$MESS["STAT_REGION"] = "ناحیه";
$MESS["STAT_SITE"] = "سایت";
?>