<?
$MESS["STAT_SERVER_TIME"] = "زمان سرور:";
$MESS["STAT_GUESTS"] = "بینندگان";
$MESS["STAT_C_EVENTS"] = "رویدادها";
$MESS["STAT_BEFORE_YESTERDAY"] = "پریروز";
$MESS["STAT_RECORDS_LIST"] = "خیلی خلاصه";
$MESS["STAT_TODAY"] = "امروز";
$MESS["STAT_YESTERDAY"] = "دیروز";
$MESS["STAT_TOTAL_1"] = "مجموع";
$MESS["STAT_PERIOD"] = "بازه";
$MESS["STAT_HITS"] = "بازدید صفحه";
$MESS["STAT_VISIT"] = "محبوبیت";
$MESS["STAT_HOSTS"] = "میزبانها";
$MESS["STAT_SESSIONS"] = "نشست ها";
$MESS["STAT_REFERERS"] = "سایتهای ارجاع دهنده";
$MESS["STAT_SERVER"] = "سایت";
$MESS["STAT_PHRASES"] = "عبارت جستجو";
$MESS["STAT_PHRASE"] = "عبارت";
$MESS["STAT_NEW"] = "جدید";
$MESS["STAT_ONLINE"] = "آنلاین";
$MESS["STAT_EVENTS"] = "رویدادها";
$MESS["STAT_EVENTS_2"] = "انواع رویداد";
$MESS["STAT_EVENT"] = "نام";
$MESS["STAT_INDEXING"] = "بایگانی";
?>