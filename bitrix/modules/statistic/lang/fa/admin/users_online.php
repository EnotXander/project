<?
$MESS["STAT_EDIT_USER"] = "پروفایل کاربر";
$MESS["STAT_HOUR"] = "ساعت";
$MESS["STAT_SEC"] = "ثانیه";
$MESS["STAT_MIN"] = "دقیقه";
$MESS["STAT_REFRESH"] = "نوسازی";
$MESS["STAT_USER"] = "بیننده";
$MESS["STAT_IP"] = "آدرس آی.پی";
$MESS["STAT_USERS_PAGES"] = "بینندگان";
$MESS["STAT_HITS"] = "بازدید صفحه";
$MESS["STAT_NOT_REGISTERED"] = "ثبت نام نشده";
$MESS["STAT_VIEW_SESSION"] = "نشست";
$MESS["STAT_NOT_AUTH"] = "(وارد سایت نشده)";
$MESS["STAT_F_IP"] = "آدرس آی.پی";
$MESS["STAT_F_HITS"] = "بازدید صفحه";
$MESS["STAT_CITY"] = "شهر";
$MESS["STAT_COUNTRY"] = "کشور";
$MESS["STAT_REGION"] = "ناحیه";
$MESS["STAT_STOP"] = "فهرست ممنوعه";
?>