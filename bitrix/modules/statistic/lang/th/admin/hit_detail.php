<?
$MESS ['STAT_ADD_TO_STOPLIST'] = "stop list";
$MESS ['STAT_ADD_TO_STOPLIST_TITLE'] = "add IP to stop list";
$MESS ['STAT_EDIT_USER'] = "User profile";
$MESS ['STAT_VIEW_SESSION_LIST'] = "Visitor session list";
$MESS ['STAT_NEW_GUEST'] = "(first time)";
$MESS ['STAT_OLD_GUEST'] = "(returned)";
$MESS ['STAT_TITLE'] = "Hit details";
$MESS ['STAT_SESSION_ID'] = "Session:";
$MESS ['STAT_PAGE'] = "To:";
$MESS ['STAT_TIME'] = "Date:";
$MESS ['STAT_USER'] = "Visitor:";
$MESS ['STAT_AUTH'] = "[authorised]";
$MESS ['STAT_NOT_AUTH'] = "[not authorised]";
$MESS ['STAT_NOT_REGISTERED'] = "not registered";
$MESS ['STAT_IP'] = "IP address:";
$MESS ['STAT_REFERER'] = "From:";
$MESS ['STAT_USER_AGENT'] = "UserAgent:";
$MESS ['STAT_METHOD'] = "Query method:";
$MESS ['STAT_COOKIES'] = "Cookies:";
$MESS ['STAT_NOT_FOUND'] = "Not found";
$MESS ['STAT_NOT_AUTH'] = "(not authorized)";
$MESS ['STAT_CLOSE'] = "Close";
?>
