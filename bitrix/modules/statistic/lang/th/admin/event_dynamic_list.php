<?
$MESS ['STAT_RECORDS_LIST'] = "Event type daily dynamics";
$MESS ['STAT_WRONG_DATE_FROM'] = "Please enter the correct date \"from\" in the filter";
$MESS ['STAT_WRONG_DATE_TILL'] = "Please enter the correct date \"till\" in the filter";
$MESS ['STAT_FROM_TILL_DATE'] = "The \"till\" date must be higher than the \"from\" date in the filter";
$MESS ['STAT_INCORRECT_EVENT_ID'] = "Incorrect event type ID";
$MESS ['STAT_EVENT_LIST'] = "Event type list";
$MESS ['STAT_F_EVENT_ID'] = "Event type:";
$MESS ['STAT_F_PERIOD'] = "Period";
$MESS ['STAT_GRAPH'] = "Event type daily graph";
$MESS ['STAT_PAGES'] = "Records";
$MESS ['STAT_DATE'] = "Date";
$MESS ['STAT_COUNTER'] = "Count";
$MESS ['STAT_TOTAL_TIME'] = "Total days:";
$MESS ['STAT_EXCEL'] = "View data in Excel format";
$MESS ['STAT_MNU_GRAPH'] = "��ҿ";
$MESS ['STAT_CHOOSE_BTN'] = "���͡...";
?>