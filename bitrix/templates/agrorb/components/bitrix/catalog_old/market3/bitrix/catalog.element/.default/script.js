jQuery(function(){
   jQuery("a.lightbox").lightBox();
});

$(document).ready(function(){
   var width = 130;
   var count = 4;

   if($('#images').size())
   {
      var ul = document.getElementById('images');
      var imgs = ul.getElementsByTagName('li');

      var position = 0;

      if($('#prev').size())
      {
         document.getElementById('prev').onclick = function() {
            if (position >= 0) return false;

            position = Math.min(position + width*count, 0)
            ul.style.marginLeft = position + 'px';
            return false;
         }
      }

      if($('#next').size())
      {
         document.getElementById('next').onclick = function() {
            if (position <= -width*(imgs.length-count)) return false;

            position = Math.max(position-width*count, -width*(imgs.length-count));
            ul.style.marginLeft = position + 'px';
            return false;
         };
      }
   }
})

function showAllProducts()
{
   $('.middle_col .ads_row').show();
   $('.middle_col .ads_row .show_positions').hide();
}