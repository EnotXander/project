<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$obParser = new CTextParser;
$temp = array();
foreach ($arResult['SECTIONS'] as $key => $arItem)
{
   $arItem["NAME"] = $obParser->html_cut($arItem["NAME"], 50);
   //$arResult['SECTIONS'][$key] = $arItem;
   if($arItem["IBLOCK_SECTION_ID"])
      $temp[$arItem["IBLOCK_SECTION_ID"]]["SUB"][] = $arItem;
   else
      $temp[$arItem["ID"]] = $arItem;
   
}
$arResult['SECTIONS'] = $temp;
?>