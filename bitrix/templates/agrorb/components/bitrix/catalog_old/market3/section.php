<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
Check404SectionPage($component);
?>


<script type="text/javascript">
   $(document).ready(function(){
      $(".market_bread li").find('ul').hide();
      $(".market_bread li").click(function(){
         $(this).find('ul').toggle();
         $(this).find('img').toggle();
      })
   });
</script>

<div class="middle_col">
   <div class="mc_block">
      <div class="bread_market">
         <ul class="market_bread">
            <li><a href="/market/">������</a></li>
            <?
            if(!CModule::IncludeModule("iblock")) die();
            
            if(strlen($arResult["VARIABLES"]["SECTION_CODE"]))
            {
               $resChain = CIBlockSection::GetList(array(), array("CODE" => $arResult["VARIABLES"]["SECTION_CODE"], "IBLOCK_ID" => $arParams["IBLOCK_ID"]));
               if ($elementChain = $resChain->GetNext()) 
                  $arResult["VARIABLES"]["SECTION_ID"] = $elementChain["ID"];
            }
   
            $nav = CIBlockSection::GetNavChain(false, $arResult["VARIABLES"]["SECTION_ID"]);
            while ($arSect = $nav->GetNext()):?>
               <li style="float:left;"><span>\</span></li>
               <li class="marketf">
                  <div class="submarket">
                     <a href="#"><?= $arSect['NAME'] ?></a>
                     <img class="strelka" src="/bitrix/templates/energorb/components/bitrix/catalog/market/bitrix/catalog.section.list/bread/img/strelka.jpg">
                     <?
                     $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "bread", Array(
                         "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                         "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                         "SECTION_ID" => $arSect["ID"],
                         "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                         "DISPLAY_PANEL" => "N",
                         "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                         "CACHE_TIME" => $arParams["CACHE_TIME"],
                         "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                         "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                             ), $component
                     );
                     ?>
                     <div class="clearfix"></div>
                  </div>
               </li>
               <? //echo "<pre>"; print_r($arSect); echo "</pre>"; ?>
            <? endwhile; ?>
         </ul>
         <div class="clearfix"></div>
      </div>
   </div>
   <? if ($arParams["USE_FILTER"] == "Y"): ?>
      <?
      $APPLICATION->IncludeComponent("bitrix:catalog.filter", "", Array(
          "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
          "IBLOCK_ID" => $arParams["IBLOCK_ID"],
          "FILTER_NAME" => $arParams["FILTER_NAME"],
          "FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
          "PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
          "PRICE_CODE" => $arParams["FILTER_PRICE_CODE"],
          "OFFERS_FIELD_CODE" => $arParams["FILTER_OFFERS_FIELD_CODE"],
          "OFFERS_PROPERTY_CODE" => $arParams["FILTER_OFFERS_PROPERTY_CODE"],
          "CACHE_TYPE" => $arParams["CACHE_TYPE"],
          "CACHE_TIME" => $arParams["CACHE_TIME"],
          "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
              ), $component
      );
      ?>
      <br />
   <? endif ?>
   <? if ($arParams["USE_COMPARE"] == "Y"): ?>
      <?
      $APPLICATION->IncludeComponent("bitrix:catalog.compare.list", "", Array(
          "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
          "IBLOCK_ID" => $arParams["IBLOCK_ID"],
          "NAME" => $arParams["COMPARE_NAME"],
          "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
          "COMPARE_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["compare"],
              ), $component
      );
      ?>
      <br />
   <? endif ?>
   <?
   global $arrrFilter;
   if ($_REQUEST['FAVOR'] == 'Y')
      $arrrFilter['ID'] = $_SESSION['FAVOR' . $arParams['IBLOCK_ID']]['ITEMS'];
   if ((int)$_REQUEST['FIRM'] > 0)
      $arrrFilter['PROPERTY_FIRM'] = (int)$_REQUEST['FIRM'];
   if ((int)$_REQUEST['per_page'] > 0)
      $perPage = (int)$_REQUEST['per_page'];
   else
      $perPage = "20";
   ?>
   <?//���������� � ID ���� ��������� �������
   global $USER;
   $arFavTovars = array();
   if($USER->IsAuthorized())
   {
      $resFav = CIBlockElement::GetList(
              array(),
              array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_FAVORITES" => $USER->GetID())
         );
      while($elementFav = $resFav->GetNext()) 
      {
         $arFavTovars[] = $elementFav["ID"];
      }
   }
   ?>
   <?
   $APPLICATION->IncludeComponent("bitrix:catalog.section", "", Array(
       "FAVORITES_ARRAY" => $arFavTovars,
       "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
       "IBLOCK_ID" => $arParams["IBLOCK_ID"],
       "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
       "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
       "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
       "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
       "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
       "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
       "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
       "BASKET_URL" => $arParams["BASKET_URL"],
       "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
       "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
       "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
       "FILTER_NAME" => "arrrFilter", //$arParams["FILTER_NAME"],
       "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
       "CACHE_TYPE" => $arParams["CACHE_TYPE"],
       "CACHE_TIME" => $arParams["CACHE_TIME"],
       "CACHE_FILTER" => $arParams["CACHE_FILTER"],
       "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
       "SET_TITLE" => $arParams["SET_TITLE"],
       "SET_STATUS_404" => $arParams["SET_STATUS_404"],
       "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
       "PAGE_ELEMENT_COUNT" => $perPage,
       "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
       "PRICE_CODE" => $arParams["PRICE_CODE"],
       "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
       "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
       "SHOW_ALL_WO_SECTION" => "Y",
       "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
       "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
       "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
       "PAGER_TITLE" => $arParams["PAGER_TITLE"],
       "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
       "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
       "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
       "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
       "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
       "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
       "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
       "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
       "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
       "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
       "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
       "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
       "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
       "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
       "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
           ), $component
   );
   ?>
</div>