<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule("iblock");

if($arResult["IBLOCK_SECTION_ID"])//������������ �������
{
   $APPLICATION->SetTitle("{$arResult["NAME"]} ����, ������ � ������, �������� � ����������� ��������");
   $APPLICATION->SetPageProperty("description", "������� ����� � ������� {$arResult["NAME"]}, ����� � ������, ��������, �������������� ������, ����������, ����"); 
   $APPLICATION->AddHeadString('<meta name="abstract" content="������� ����� � ������� '.$arResult["NAME"].', ����� � ������, ��������, �������������� ������, ����������, ����">', true);
   $APPLICATION->SetPageProperty("keywords", "������ {$arResult["NAME"]}, {$arResult["NAME"]} � ������, ������� {$arResult["NAME"]}");
}
else//��������� ������
{
   $APPLICATION->SetPageProperty("description", "{$arResult["NAME"]} ���������� ��������, ������� ����� � ������, ������ � �������� �������, {$arResult["NAME"]}, ����, ���� � �������������� ������."); 
   $APPLICATION->AddHeadString('<meta name="abstract" content="'.$arResult["NAME"].' ���������� ��������, ������� ����� � ������, ������ � �������� �������, '.$arResult["NAME"].', ����, ���� � �������������� ������.">', true);
   $arKeywords = array();
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      if(strlen($arElement["PROPERTIES"]["KEYWORDS"]["VALUE"]["TEXT"]))
      {

         $newArray = explode(",", $arElement["PROPERTIES"]["KEYWORDS"]["VALUE"]["TEXT"]);
         $newestArray = array();
         foreach($newArray as $arrayElement)
         {
            $arrayElement = trim($arrayElement);
            if(strlen($arrayElement))
               $newestArray[] = $arrayElement;
         }
         $arKeywords = array_merge($arKeywords, $newArray);
         if(count($arKeywords) >= 10)
         {
            $arKeywords = array_slice($arKeywords, 0, 10);
            $arKeywords = " ".implode(", ", $arKeywords);
            break;
         }
      }
   }
   if(is_array($arKeywords))
   {
      if(count($arKeywords))
         $arKeywords = " ".implode(", ", $arKeywords);
      else
         $arKeywords = "";
   }
   $APPLICATION->SetPageProperty("keywords", "{$arResult["NAME"]}{$arKeywords}, ������� EnergoBelarus.by");
}

$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      $arElement["DETAIL_TEXT"] = strip_tags($arElement["DETAIL_TEXT"]);
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], 200);
      $arResult['ITEMS'][$key] = $arElement;
      if (is_array($arElement["PREVIEW_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
                         $arElement["PREVIEW_PICTURE"], array("width" => 100, "height" => 100), BX_RESIZE_IMAGE_EXACT, true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
	     "HEIGHT" => $arFileTmp["height"],
         );
      }
   }
}


// �������� ���������� �� ��������� � ������
$arResult["BRANDS"] = array();
$arResult["BRANDS_IDS"] = array();
$arSelect = array("ID", "NAME", "PROPERTY_FIRM", "IBLOCK_ID");
$arFilter = array(
    "IBLOCK_ID" => $arParams['IBLOCK_ID'],
    "SECTION_ID" => $arParams["SECTION_ID"],
    "INCLUDE_SUBSECTIONS" => "Y",
    "ACTIVE" => "Y"
);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
while ($arFields = $res->GetNext())
{
   if (!in_array($arFields['PROPERTY_FIRM_VALUE'], $arResult["BRANDS_IDS"]))
   {
      $arResult["BRANDS_IDS"][] = $arFields['PROPERTY_FIRM_VALUE'];
   }
}

if (count($arResult["BRANDS_IDS"])) {
    $arFilter = array(
        "IBLOCK_ID" => 17,
        "ID" => $arResult["BRANDS_IDS"],
        "ACTIVE" => "Y",
        "!PROPERTY_REMOVE_REL" => 63
    );
    $arResult["BRANDS_IDS"] = array();
    $res = CIBlockElement::GetList(array("NAME" => "ASC"), $arFilter);
    while ($arFields = $res->GetNext())
    {
       if (!in_array($arFields['ID'], $arResult["BRANDS_IDS"]))
       {
          $arFields["PREVIEW_PICTURE_FORMATED"] = CFile::ShowImage($arFields["PREVIEW_PICTURE"], 50, 50);
          $arResult["BRANDS_IDS"][] = $arFields["ID"];
          $arResult["BRANDS"][$arFields["ID"]] = $arFields;
       }
    }
}
