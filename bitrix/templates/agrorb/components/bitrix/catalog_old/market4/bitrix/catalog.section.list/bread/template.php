<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<ul>
   <?foreach ($arResult["SECTIONS"] as $arSection):
      $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
      $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
      //if($arSection["DEPTH_LEVEL"]>1)
      //continue;
      ?>
      <li>
         <a href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?></a><? if ($arParams["COUNT_ELEMENTS"]): ?><sup><?= $arSection["ELEMENT_CNT"] ?></sup><? endif; ?>
      </li>
   <?endforeach;?>
</ul>
