<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<link href="/css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery.lightbox-0.5.pack.js"></script>

<div class="left_col">
   <?if(count($arResult["BRANDS"]) > 0):?>
      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?$i=0;
            foreach($arResult["BRANDS"] as $arBrand):?>
               <?if($i < 50):?>
                  <?if ($arBrand["DETAIL_PAGE_URL"] != NULL):?>
                     <li>
                        <a href="<?=$arBrand["DETAIL_PAGE_URL"]?>"><?=$arBrand["NAME"]?></a>
                     </li>
                  <?endif?>
               <?endif?>
            <?$i++;
            endforeach?>
         </ul>
      </div>
   <?endif;?>
   
   <div class="lc_block">
      <div class="left_banner_185">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
               "TYPE" => "MARKET",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
</div>

<div class="middle_col">
   <div class="right_col">
      <div class="rc_block brdr_box" style="width:297px;margin-top:20px;">
         <div class="pr_c_top">
            <?if($arResult['DISPLAY_PROPERTIES']['COST']['DISPLAY_VALUE']):?>
               <span class="price"><?= $arResult['DISPLAY_PROPERTIES']['COST']['DISPLAY_VALUE'] ?></span>
               <?=strlen($arResult['PROPERTIES']['CURRENCY']['VALUE']) ? " ".$arResult['PROPERTIES']['CURRENCY']['VALUE'] : ""?>
            <?endif;?>
            <br/>
	         <noindex>
            <a href="javascript: void(0);" rel="nofollow" class="send_r" onclick="javascript:showPopup({id: $('.fast_order_popup'), productID: <?=$arResult["ID"]?>})"></a> <br/>
	         </noindex>
         </div>
         <?if ($arResult['DISPLAY_PROPERTIES']['FIRM']['VALUE'] > 0 && in_array($arResult['PROPERTIES']['FIRM']['VALUE'], $arResult["BRANDS_IDS"])):?>
            <div class="wrap">
               <h3 style="color:red;">���������� ����������</h3>
               <img src="<?=SITE_TEMPLATE_PATH?>/images/ne_zabud.png" style="margin-left:-30px;">
               <table width="100%" cellpadding="0" class="add_table">
                  <tr>
                     <td class="company_name" style="width:240px !important;"><?=$arResult['DISPLAY_PROPERTIES']['FIRM']['DISPLAY_VALUE']?></td>
                     <td width="70"><?=$arResult['DISPLAY_PROPERTIES']['FIRM']['PREVIEW_PICTURE'];?></td>
                  </tr>
               </table>
               <table width="100%" cellpadding="0" class="add_table">
                  <tr>
                     <td>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/addres.jpg">
                     </td>
                     <td>
                        <?=implode(", ", $arResult["ADRESS"])?>
                     </td>
                  </tr>
                  <? if (strlen($arResult["COMPANY"]["PROPERTIES"]["adress_store"]["VALUE"])): ?>
                     <tr>
                        <td>
                           <img src="<?= SITE_TEMPLATE_PATH ?>/img/addres.jpg">
                        </td>
                        <td>
                           ����� ������: <?=strip_tags($arResult["COMPANY"]["PROPERTIES"]["adress_store"]["VALUE"])?>
                        </td>
                     </tr>
                  <? endif; ?>
                  <?if (is_array($arResult["COMPANY"]["PROPERTIES"]["phone"]["VALUE"]) && count($arResult["COMPANY"]["PROPERTIES"]["phone"]["VALUE"])):?>
                     <tr>
                        <td>
                           <img src="<?=SITE_TEMPLATE_PATH?>/img/tel.jpg">
                        </td>
                        <td><noindex>
                           <? foreach ($arResult["COMPANY"]["PROPERTIES"]["phone"]["VALUE"] as $phoneKey => $phone): ?>
                              <b>
                                 <?= $phoneKey ? "<br>" : ""?>
                                 <?= $phone." ".$arResult["COMPANY"]["PROPERTIES"]["phone"]["DESCRIPTION"][$phoneKey]?>
                              </b>
                           <? endforeach; ?>
	                        </noindex>
                        </td>
                     </tr>
                  <?endif;?>
                  <?if (strlen($arResult["COMPANY"]["PROPERTIES"]["shedule"]["VALUE"])>0):?>
                     <tr>
                        <td>
                           <img src="<?=SITE_TEMPLATE_PATH?>/img/time.jpg">
                        </td>
                        <td>
                           <?echo $arResult["COMPANY"]["PROPERTIES"]["shedule"]["VALUE"];?>
                        </td>
                     </tr>
                  <?endif;?>
               </table>
               <br/>
               
               
               <?if (count($arResult["COMPANY_INFO"]["URL"]) > 0): ?>
                  <noindex>
                     <div>
                        <div class="fl_left" style="margin-right: 10px;">web-����(�):</div> 
                        <div class="fl_left">
                           <?foreach($arResult["COMPANY_INFO"]["URL"] as $key=>$site):?>
                              <a rel="nofollow" target="_blank" href="<?=$site["LINK"]?>"><?=$site["NAME"]?></a><?=(isset($arResult["PROPERTIES"]["URL"]["VALUE"][$key+1]) ? "<br>" : "" )?>
                           <?endforeach;?>
                        </div>
                     </div>
                     <div class="clear"></div>
                  </noindex>
               <?endif; ?>
                  
               <?$coord = explode(",",$arResult["COMPANY"]['PROPERTIES']['MAP']['VALUE']);
               if (count($coord)>1):?>
                  <?$arData = Array("yandex_lat" => $coord[0], "yandex_lon" => $coord[1], "yandex_scale" => 16, "PLACEMARKS" => Array(Array("LON" => $coord[1], "LAT" => $coord[0], "TEXT" => $arResult["COMPANY"]['NAME'])));?>
                  <br/>
                  <br/>
                  <?$APPLICATION->IncludeComponent("bitrix:map.yandex.view", ".default", array(
                        "KEY" => "AETqvU8BAAAApI_7dQMAlTDGwZ3hOJGhdVSFYEsVNSpuzTQAAAAAAAAAAACCDIEExtPcFpiKYkYYJlaNR8uKKw==",
                        "INIT_MAP_TYPE" => "MAP",
                        "MAP_DATA" => serialize($arData),
                        "MAP_WIDTH" => "270",
                        "MAP_HEIGHT" => "200",
                        "CONTROLS" => array(),
                        "OPTIONS" => array("ENABLE_SCROLL_ZOOM","ENABLE_DRAGGING"),
                        "MAP_ID" => ""
                     ),
                     false
                  );?>
               <?endif;?>
               <?if ($arResult["USER_ID"] > 0):?>
	               <noindex>
                  <br/>
                  <br/>
                  <?if ($USER->IsAuthorized()):?>
                     <a rel="nofollow" style="width: 140px; float:left;" href="/personal/messages/<?=$arResult['USER_ID']["ID"]?>/?product=<?=$arResult['ID']?>" class="btns gray_btn fl_right"><i></i><img style="margin-bottom: -3px;" src="/bitrix/templates/energorb/images/mail.jpg">&nbsp;�������� ���������</a><br/>
                  <?else:?>
                     <a style="width: 140px; float:left;" rel="nofollow" href="#" class="btns gray_btn fl_right" onclick="alert('���������� ��������������'); return false;"><i></i><img style="margin-bottom: -3px;" src="/bitrix/templates/energorb/images/mail.jpg">&nbsp;�������� ���������</a><br/>
                  <?endif;?></noindex>
               <?endif;?>
               <div class="clear"></div>
               <br/>
               <br/>
               <!--<a href="/market/0/?FIRM=<?=$arResult["COMPANY"]['ID']?>">��� ����������� ��������</a>-->
               <a href="<?=$arResult["COMPANY"]['DETAIL_PAGE_URL']?>#tabs">��� ����������� ��������</a>
            </div>
         <?endif;?>
      </div><!--rc_block-->
   </div>
   
   <div class="middle_col">
      <div class="mc_block">
         <div class="catalog-element">
            <h1><?=$arResult["NAME"]?></h1>
            <?if($USER->IsAuthorized()):?>
               <a href="javascript:void(0)" onclick="AddToFavorites({productId: <?=$arResult["ID"]?>, context: $(this).find('.icon_star'), inFavorites: function(){this.addClass('star_sel')}, outFavorites: function(){this.removeClass('star_sel')}})">
                  <i class="icons icon_star<? if(in_array($USER->GetID(), $arResult["PROPERTIES"]["FAVORITES"]["VALUE"])):?> star_sel<? endif; ?>"></i>
               </a>
            <?endif;?>
            <?=$arResult["PREVIEW_TEXT"]?>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
               <tr>
                  <?if(is_array($arResult["PREVIEW_PICTURE"]) || is_array($arResult["DETAIL_PICTURE"])):?>
                     <td width="0%" valign="top" align="center">
                        <?if(is_array($arResult["PREVIEW_PICTURE"]) && is_array($arResult["DETAIL_PICTURE"])):?>
                              <a href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" class="lightbox" style="display:block; text-align:center;"><img border="0" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" id="image_<?=$arResult["PREVIEW_PICTURE"]["ID"]?>" style="display:block;cursor:pointer;cursor: hand; margin:0 auto;" /><br /><img src="/bitrix/templates/energorb/components/bitrix/news/advert/bitrix/news.detail/.default/images/zoom.png"> ���������</a>
                        <?elseif(is_array($arResult["DETAIL_PICTURE"])):?>
                              <a href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" class="lightbox" style="display:block; text-align:center;"><img border="0" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" style="display:block;cursor:pointer;cursor: hand; margin:0 auto;" /><br /><img src="/bitrix/templates/energorb/components/bitrix/news/advert/bitrix/news.detail/.default/images/zoom.png"> ���������</a>
                        <?elseif(is_array($arResult["PREVIEW_PICTURE"])):?>
                              <a href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" class="lightbox" style="display:block; text-align:center;"><img border="0" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" style="display:block;cursor:pointer;cursor: hand; margin:0 auto;" /><br /><img src="/bitrix/templates/energorb/components/bitrix/news/advert/bitrix/news.detail/.default/images/zoom.png"> ���������</a>
                        <?endif?>
                        <br />
                     </td>
                  <?endif;?>
               </tr>
            </table>

            <? // additional photos
            $LINE_ELEMENT_COUNT = 2; // number of elements in a row
            if(count($arResult["MORE_PHOTO"])>0):?>
               <div class="carousel">
                  <table width="650">
                     <tr>
                        <?if(is_array($arResult["MORE_PHOTO_SMALL"]) && (count($arResult["MORE_PHOTO_SMALL"]) > 6)):?>
                           <td>
                              <a href="#" class="leftarrow left-arrow" id="prev"></a>
                           </td>
                        <?endif;?>
                        <td>  
                           <div class="galery">
                              <ul id="images">
                                 <?foreach($arResult["MORE_PHOTO_SMALL"] as $key => $PHOTO):?>
                                    <li><a href="<?=$arResult["MORE_PHOTO"][$key]["SRC"]?>" class="lightbox"><img border="0" src="<?=$PHOTO["SRC"]?>" width="<?=$PHOTO["WIDTH"]?>" height="<?=$PHOTO["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" /></a></li>
                                 <?endforeach?>
                              </ul>
                           </div>
                        </td>
                        <?if(is_array($arResult["MORE_PHOTO_SMALL"]) && (count($arResult["MORE_PHOTO_SMALL"]) > 6)):?>
                           <td>
                              <a href="#" class="rightarrow right-arrow" id="next"></a>
                           </td>
                        <?endif;?>
                     </tr>
                  </table>
               </div>
            <?endif?>
            
            <? if (is_array($arResult["OFFERS"]) && !empty($arResult["OFFERS"])): ?>
               <? foreach ($arResult["OFFERS"] as $arOffer): ?>
                  <? foreach ($arParams["OFFERS_FIELD_CODE"] as $field_code): ?>
                     <small><? echo GetMessage("IBLOCK_FIELD_" . $field_code) ?>:&nbsp;<? echo $arOffer[$field_code]; ?></small><br />
                     <? endforeach; ?>
                  <? foreach ($arOffer["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
                     <small><?= $arProperty["NAME"] ?>:&nbsp;<?
            if (is_array($arProperty["DISPLAY_VALUE"]))
               echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
            else
               echo $arProperty["DISPLAY_VALUE"];
                     ?></small><br />
                  <? endforeach ?>
                  <? foreach ($arOffer["PRICES"] as $code => $arPrice): ?>
                     <? if ($arPrice["CAN_ACCESS"]): ?>
                        <p><?= $arResult["CAT_PRICES"][$code]["TITLE"]; ?>:&nbsp;&nbsp;
                           <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                           <s><?= $arPrice["PRINT_VALUE"] ?></s> <span class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
                        <? else: ?>
                           <span class="catalog-price"><?= $arPrice["PRINT_VALUE"] ?></span>
                        <? endif ?>
                     </p>
                  <? endif; ?>
               <? endforeach; ?>
               <p>
                  <? if ($arParams["DISPLAY_COMPARE"]): ?>
                  <noindex>
                     <a href="<? echo $arOffer["COMPARE_URL"] ?>" rel="nofollow"><? echo GetMessage("CT_BCE_CATALOG_COMPARE") ?></a>&nbsp;
                  </noindex>
               <? endif ?>
               <? if ($arOffer["CAN_BUY"]): ?>
                  <? if ($arParams["USE_PRODUCT_QUANTITY"]): ?>
                     <form action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">
                        <table border="0" cellspacing="0" cellpadding="2">
                           <tr valign="top">
                              <td><? echo GetMessage("CT_BCE_QUANTITY") ?>:</td>
                              <td>
                                 <input type="text" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"] ?>" value="1" size="5">
                              </td>
                           </tr>
                        </table>
                        <input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"] ?>" value="BUY">
                        <input type="hidden" name="<? echo $arParams["PRODUCT_ID_VARIABLE"] ?>" value="<? echo $arOffer["ID"] ?>">
                        <input type="submit" name="<? echo $arParams["ACTION_VARIABLE"] . "BUY" ?>" value="<? echo GetMessage("CATALOG_BUY") ?>">
                        <input type="submit" name="<? echo $arParams["ACTION_VARIABLE"] . "ADD2BASKET" ?>" value="<? echo GetMessage("CT_BCE_CATALOG_ADD") ?>">
                     </form>
                  <? else: ?>
                     <noindex>
                        <a href="<? echo $arOffer["BUY_URL"] ?>" rel="nofollow"><? echo GetMessage("CATALOG_BUY") ?></a>
                        &nbsp;<a href="<? echo $arOffer["ADD_URL"] ?>" rel="nofollow"><? echo GetMessage("CT_BCE_CATALOG_ADD") ?></a>
                     </noindex>
                  <? endif; ?>
               <? elseif (count($arResult["CAT_PRICES"]) > 0): ?>
                  <?= GetMessage("CATALOG_NOT_AVAILABLE") ?>
               <? endif ?>
               </p>
            <? endforeach; ?>
         <? else: ?>
            <? foreach ($arResult["PRICES"] as $code => $arPrice): ?>
               <? if ($arPrice["CAN_ACCESS"]): ?>
                  <p><?= $arResult["CAT_PRICES"][$code]["TITLE"]; ?>&nbsp;
                     <? if ($arParams["PRICE_VAT_SHOW_VALUE"] && ($arPrice["VATRATE_VALUE"] > 0)): ?>
                        <? if ($arParams["PRICE_VAT_INCLUDE"]): ?>
                           (<? echo GetMessage("CATALOG_PRICE_VAT") ?>)
                        <? else: ?>
                           (<? echo GetMessage("CATALOG_PRICE_NOVAT") ?>)
                        <? endif ?>
                     <? endif; ?>:&nbsp;
                     <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                        <s><?= $arPrice["PRINT_VALUE"] ?></s> <span class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
                        <? if ($arParams["PRICE_VAT_SHOW_VALUE"]): ?><br />
                           <?= GetMessage("CATALOG_VAT") ?>:&nbsp;&nbsp;<span class="catalog-vat catalog-price"><?= $arPrice["DISCOUNT_VATRATE_VALUE"] > 0 ? $arPrice["PRINT_DISCOUNT_VATRATE_VALUE"] : GetMessage("CATALOG_NO_VAT") ?></span>
                        <? endif; ?>
                     <? else: ?>
                        <span class="catalog-price"><?= $arPrice["PRINT_VALUE"] ?></span>
                        <? if ($arParams["PRICE_VAT_SHOW_VALUE"]): ?><br />
                           <?= GetMessage("CATALOG_VAT") ?>:&nbsp;&nbsp;<span class="catalog-vat catalog-price"><?= $arPrice["VATRATE_VALUE"] > 0 ? $arPrice["PRINT_VATRATE_VALUE"] : GetMessage("CATALOG_NO_VAT") ?></span>
                        <? endif; ?>
                     <? endif ?>
                  </p>
               <? endif; ?>
            <? endforeach; ?>
            <? if (is_array($arResult["PRICE_MATRIX"])): ?>
               <table cellpadding="0" cellspacing="0" border="0" width="100%" class="data-table">
                  <thead>
                     <tr>
                        <? if (count($arResult["PRICE_MATRIX"]["ROWS"]) >= 1 && ($arResult["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_FROM"] > 0 || $arResult["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_TO"] > 0)): ?>
                           <td><?= GetMessage("CATALOG_QUANTITY") ?></td>
                        <? endif; ?>
                        <? foreach ($arResult["PRICE_MATRIX"]["COLS"] as $typeID => $arType): ?>
                           <td><?= $arType["NAME_LANG"] ?></td>
                        <? endforeach ?>
                     </tr>
                  </thead>
                  <? foreach ($arResult["PRICE_MATRIX"]["ROWS"] as $ind => $arQuantity): ?>
                     <tr>
                        <? if (count($arResult["PRICE_MATRIX"]["ROWS"]) > 1 || count($arResult["PRICE_MATRIX"]["ROWS"]) == 1 && ($arResult["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_FROM"] > 0 || $arResult["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_TO"] > 0)): ?>
                           <th nowrap>
                              <?
                              if (IntVal($arQuantity["QUANTITY_FROM"]) > 0 && IntVal($arQuantity["QUANTITY_TO"]) > 0)
                                 echo str_replace("#FROM#", $arQuantity["QUANTITY_FROM"], str_replace("#TO#", $arQuantity["QUANTITY_TO"], GetMessage("CATALOG_QUANTITY_FROM_TO")));
                              elseif (IntVal($arQuantity["QUANTITY_FROM"]) > 0)
                                 echo str_replace("#FROM#", $arQuantity["QUANTITY_FROM"], GetMessage("CATALOG_QUANTITY_FROM"));
                              elseif (IntVal($arQuantity["QUANTITY_TO"]) > 0)
                                 echo str_replace("#TO#", $arQuantity["QUANTITY_TO"], GetMessage("CATALOG_QUANTITY_TO"));
                              ?>
                           </th>
                        <? endif; ?>
                        <? foreach ($arResult["PRICE_MATRIX"]["COLS"] as $typeID => $arType): ?>
                           <td>
                              <?
                              if ($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
                                 echo '<s>' . FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]) . '</s> <span class="catalog-price">' . FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]) . "</span>";
                              else
                                 echo '<span class="catalog-price">' . FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]) . "</span>";
                              ?>
                           </td>
                        <? endforeach ?>
                     </tr>
                  <? endforeach ?>
               </table>
               <? if ($arParams["PRICE_VAT_SHOW_VALUE"]): ?>
                  <? if ($arParams["PRICE_VAT_INCLUDE"]): ?>
                     <small><?= GetMessage('CATALOG_VAT_INCLUDED') ?></small>
                  <? else: ?>
                     <small><?= GetMessage('CATALOG_VAT_NOT_INCLUDED') ?></small>
                  <? endif ?>
               <? endif; ?>
               <br />
            <? endif ?>
            <? if ($arResult["CAN_BUY"]): ?>
               <? if ($arParams["USE_PRODUCT_QUANTITY"] || count($arResult["PRODUCT_PROPERTIES"])): ?>
                  <form action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">
                     <table border="0" cellspacing="0" cellpadding="2">
                        <? if ($arParams["USE_PRODUCT_QUANTITY"]): ?>
                           <tr valign="top">
                              <td><? echo GetMessage("CT_BCE_QUANTITY") ?>:</td>
                              <td>
                                 <input type="text" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"] ?>" value="1" size="5">
                              </td>
                           </tr>
                        <? endif; ?>
                        <? foreach ($arResult["PRODUCT_PROPERTIES"] as $pid => $product_property): ?>
                           <tr valign="top">
                              <td><? echo $arResult["PROPERTIES"][$pid]["NAME"] ?>:</td>
                              <td>
                                 <?
                                 if (
                                         $arResult["PROPERTIES"][$pid]["PROPERTY_TYPE"] == "L"
                                         && $arResult["PROPERTIES"][$pid]["LIST_TYPE"] == "C"
                                 ):
                                    ?>
                                    <? foreach ($product_property["VALUES"] as $k => $v): ?>
                                       <label><input type="radio" name="<? echo $arParams["PRODUCT_PROPS_VARIABLE"] ?>[<? echo $pid ?>]" value="<? echo $k ?>" <? if ($k == $product_property["SELECTED"]) echo '"checked"' ?>><? echo $v ?></label><br>
                                    <? endforeach; ?>
                                 <? else: ?>
                                    <select name="<? echo $arParams["PRODUCT_PROPS_VARIABLE"] ?>[<? echo $pid ?>]">
                                       <? foreach ($product_property["VALUES"] as $k => $v): ?>
                                          <option value="<? echo $k ?>" <? if ($k == $product_property["SELECTED"]) echo '"selected"' ?>><? echo $v ?></option>
                                       <? endforeach; ?>
                                    </select>
                                 <? endif; ?>
                              </td>
                           </tr>
                        <? endforeach; ?>
                     </table>
                     <input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"] ?>" value="BUY">
                     <input type="hidden" name="<? echo $arParams["PRODUCT_ID_VARIABLE"] ?>" value="<? echo $arResult["ID"] ?>">
                     <input type="submit" name="<? echo $arParams["ACTION_VARIABLE"] . "BUY" ?>" value="<? echo GetMessage("CATALOG_BUY") ?>">
                     <input type="submit" name="<? echo $arParams["ACTION_VARIABLE"] . "ADD2BASKET" ?>" value="<? echo GetMessage("CATALOG_ADD_TO_BASKET") ?>">
                  </form>
               <? else: ?>
                  <noindex>
                     <a href="<? echo $arResult["BUY_URL"] ?>" rel="nofollow"><? echo GetMessage("CATALOG_BUY") ?></a>
                     &nbsp;<a href="<? echo $arResult["ADD_URL"] ?>" rel="nofollow"><? echo GetMessage("CATALOG_ADD_TO_BASKET") ?></a>
                  </noindex>
               <? endif; ?>
            <? elseif ((count($arResult["PRICES"]) > 0) || is_array($arResult["PRICE_MATRIX"])): ?>
               <?= GetMessage("CATALOG_NOT_AVAILABLE") ?>
            <? endif ?>
            <?endif?>
            <br />
            
            <h2>��������� �������� ������</h2><hr/>
            
            <div class="formated_text js-fancybox_table">
               <?if($arResult["DETAIL_TEXT"]):?>
                  <br /><?echo str_replace(Array("\"/images/","\"images/"), Array("\"http://energobelarus.by/images/", "\"http://energobelarus.by/images/"), $arResult["DETAIL_TEXT"]);?><br />
               <?elseif($arResult["PREVIEW_TEXT"]):?>
                  <br /><?=$arResult["PREVIEW_TEXT"]?><br />
               <?endif;?>
            </div>   
               
            <?if(count($arResult["LINKED_ELEMENTS"])>0):?>
               <br />
               <b><?=$arResult["LINKED_ELEMENTS"][0]["IBLOCK_NAME"]?>:</b>
               <ul>
                  <?foreach($arResult["LINKED_ELEMENTS"] as $arElement):?>
                        <li><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></li>
                  <?endforeach;?>
               </ul>
            <?endif?>
                  
            <?if (count($arResult['FILES']) > 0):?>               
               <div class="b-files"> 
                  <h3>��������� �����</h3>
                  <hr/>
                  <?foreach ($arResult['FILES'] as $file):?>
                     <div class="files-download">
                        <a href="<?=$file["SRC"]?>" target="_blank"><i class="icons <?=$file["CLASS"]?>"></i><span><?=strlen($file["DESCRIPTION"]) > 0 ? $file["DESCRIPTION"] : "������� ����"?></span><span class="size"> / <?=$file["FORMAT_SIZE"]?></span></a>
                     </div>                        
                  <?endforeach;?>
               </div>
            <?endif;?>
            <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
            <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed"></div> 
         </div>
      </div>
      <div class="middle_col">
         <?if (count($arResult['ITEMS']) > 0):?>
            <h1 style="color:red">�������� ������ ������ ��������</h1>
            <?foreach($arResult["ITEMS"] as $key => $arItem):?>
               <div class="ads_row" style="background-color:#fff;<?if ($key>2) echo 'display:none;';?>">
                  <table>
                     <tbody>
                        <tr>
                           <td class="image_box">
                              <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                                 <a title="���������� ���������" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                    <img alt="" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                                 </a>
                              <?else:?>
                                 <img src="<?=SITE_TEMPLATE_PATH?>/images/withoutphoto.jpg" width="72" height="72" align=""/> 
                              <?endif;?>
                           </td>
                           <td class="ads_info">
                              <div class="tdWrap">
                                 <div class="ads_title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                                 <div class="ads_text">���: <?echo $arItem['PROPERTIES']['NUMBER']['VALUE']?></div>
                                 <div class="meta"><?=$arItem['PREVIEW_TEXT']?></div>
                              </div>                      
                           </td>
                           <td class="ads_price">
                              <?=$arItem['PROPERTIES']['COST']['VALUE']?><br/>
                              <span style="color:#919191;">���/��.</span><br/>
                              <?if($arItem["USER_ID"] > 0):?>
                                 <br/>
                                 <?if($USER->IsAuthorized()):?>
                                    <a href="/personal/messages/<?=$arItem['USER_ID']?>/?product=<?=$arResult['ID']?>" style="text-decoration:none;">��������</a>
                                 <?else:?>
                                    <a href="#" onclick="alert('���������� ��������������'); return false;" style="text-decoration:none;">��������</a>
                                 <?endif;?>
                              <?endif;?>
                           </td>
                           <td class="ads_nav">
                              <?$arCompany = GetIblockElement($arItem['PROPERTIES']['FIRM']['VALUE']);?>
                              <?= $arItem["COMPANY"]['PREVIEW_PICTURE_FORMATED'];?>
                              <br/>
                              <a href="<?=$arItem["COMPANY"]['DETAIL_PAGE_URL']?>"><?=$arItem["COMPANY"]['NAME']?></a>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <?if ($key == 2):?>
                     <div style="padding-left:70px; padding-top:5px;">
                        <div class="show_positions" style="margin-left:90px;">
                           <a href="#" onclick="showAllProducts(); return false;">
                              <img src="<?=SITE_TEMPLATE_PATH?>/images/plus.png">
                              <span>�������� ��� �������: <?=count($arResult["ITEMS"])-3?></span>
                           </a>
                        </div>
                     </div>
                  <?endif;?>
               </div><!--ads_row-->
            <?endforeach;?>
            <br/>
            <a href="/market/<?=$arResult['IBLOCK_SECTION_ID']?>/">��� �������� ������</a>
         <?endif;?>
      </div>
   </div>
</div>
<div class="clear"></div>
<br/>

<?if(strlen($arResult["STAT_CODE"]) > 0): // ������� ������ ?>
   <script type="text/javascript"> 
   <!--
   Nnv=navigator;Nna=Nnv.appName;Nd=document;Nd.cookie="b=b";Nc=0;if(Nd.cookie)Nc=1;
   Nn=(Nna.substring(0,2)=="Mi")?0:1;Ns=screen;Npx=(Nn==0)?Ns.colorDepth:Ns.pixelDepth;
   str='<img src="http://c.energobelarus.by/<?=$arResult["STAT_CODE"]?>;'+Ns.width+'x'+Ns.height+';'+Npx+';'+Nc+';';
   str=str+escape(Nd.referrer)+';'+Math.random()+'" width="88" height="31" border="0">';
   document.write('<a href="http://www.energobelarus.by/" alt="Energobelarus" target="_blank">'+str+'</a>');
   // -->
   </script>
   <noscript><a href="http://www.energobelarus.by/" target="_blank"><img src="http://c.energobelarus.by/<?=$arResult["STAT_CODE"]?>;0x0;0;0;-;0" alt="Energobelarus" width="88" height="31" border="0"></a></noscript>
<? endif; ?>

<?if($arResult["COMPANY"]["PROPERTIES"]["METRIKA_ID"]["VALUE"])
    YandexMetrika::setCounter($arResult["COMPANY"]["PROPERTIES"]["METRIKA_ID"]["VALUE"]);?>
