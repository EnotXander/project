<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="middle_col">
   <div class="mc_block" >
      <ul class="mb_menu">
         <li <? if ($arParams['IBLOCK_ID'] == 58): ?>class="sel"<? endif; ?>><a style="font-size:20px" <? if ($arParams['IBLOCK_ID'] == 58): ?>name="market"<? else: ?>href="/market/"<? endif; ?>>������</a><div class="sclew"></div></li>
         <li <? if ($arParams['IBLOCK_ID'] <> 58): ?>class="sel"<? endif; ?>><a style="font-size:20px" <? if ($arParams['IBLOCK_ID'] <> 58): ?>name="services"<? else: ?>href="/services/"<? endif; ?>>������</a><div class="sclew"></div></li>
         <li style="background:none; float:right;" class="add_l_m"><a href="/personal/addcompany/">�������� ���� �������� � ������</a></li>
      </ul>
      <div style="border-bottom:2px solid #00A340;"></div>
      <div class="clear"></div> <br />
   </div>
   <div class="catalog-section-list">
      <ul>
         <?foreach ($arResult["SECTIONS"] as $arSection):?>
            <?if($arSection["ELEMENT_CNT"]):?>
               <li class="level_li_<?= $arSection['DEPTH_LEVEL'] ?>">
                  <a class="level<?= $arSection['DEPTH_LEVEL'] ?>" href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?></a><? if ($arParams["COUNT_ELEMENTS"]): ?><sup><?= $arSection["ELEMENT_CNT"] ?></sup><? endif; ?>
                  <?if(is_array($arSection["SUB"]) && count($arSection["SUB"])):?>
                     <ul>
                     <?foreach($arSection["SUB"] as $arSubSection):?>
                        <?if($arSubSection["ELEMENT_CNT"]):?>
                           <li class="level_li_<?= $arSubSection['DEPTH_LEVEL'] ?>">
                              <a class="level<?= $arSubSection['DEPTH_LEVEL'] ?>" href="<?= $arSubSection["SECTION_PAGE_URL"] ?>"><?= $arSubSection["NAME"] ?></a><? if ($arParams["COUNT_ELEMENTS"]): ?><sup><?= $arSubSection["ELEMENT_CNT"] ?></sup><? endif; ?>
                           </li>
                        <?endif;?>
                     <?endforeach;?>
                     </ul>
                  <?endif;?>
               </li>
               <li style="clear:both; width:100%; border-bottom:1px solid #ABABAB;margin: 20px 0; padding: 10px 0 0;"></li>
            <?endif;?>
         <?endforeach;?>
      </ul>
   </div>
</div>