<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$obParser = new CTextParser;
foreach ($arResult['SECTIONS'] as $key => $arItem)
{
   $arItem["NAME"] = $obParser->html_cut($arItem["NAME"], 50);
   $arResult['SECTIONS'][$key] = $arItem;
}
?>