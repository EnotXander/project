<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$objElement = new CIBlockElement();

$arResult["MORE_PHOTO_SMALL"] = Array();
if (is_array($arResult["MORE_PHOTO"]))
{
   foreach ($arResult["MORE_PHOTO"] as $key => $photo)
   {
      if (is_array($photo))
      {
         $arFileTmp = CFile::ResizeImageGet(
            $photo,
            array("width" => 75, "height" => 75),
            BX_RESIZE_IMAGE_EXACT,
            true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arResult["MORE_PHOTO_SMALL"][$key] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],//75, //IntVal($arSize[0]),
             "HEIGHT" => $arFileTmp["height"]//75, //IntVal($arSize[1]),
         );
      }
   }
}
if (is_array($arResult["PREVIEW_PICTURE"]))
{
   $arFileTmp = CFile::ResizeImageGet(
      $arResult["PREVIEW_PICTURE"],
      array("width" => 630, "height" => 500),
      BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
      true
   );

   $arResult["PREVIEW_PICTURE"] = array(
       "SRC" => $arFileTmp["src"],
       "WIDTH" => $arFileTmp["width"],//IntVal($arSize[0]),
       "HEIGHT" => $arFileTmp["height"]//IntVal($arSize[1]),
   );
}
$arItems = Array();
$i = 0;
$arFirms = Array($arResult['PROPERTIES']['FIRM']['VALUE']);
$arSelect = array(
    "ID",
    "NAME",
    "CODE",
    "DATE_CREATE",
    "ACTIVE_FROM",
    "ACTIVE_TO",
    "CREATED_BY",
    "IBLOCK_ID",
    "IBLOCK_SECTION_ID",
    "DETAIL_PAGE_URL",
    "DETAIL_TEXT",
    "DETAIL_TEXT_TYPE",
    "DETAIL_PICTURE",
    "PREVIEW_TEXT",
    "PREVIEW_TEXT_TYPE",
    "PREVIEW_PICTURE",
    "TAGS",
    "PROPERTY_*",
);
$arFilter = Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => $arResult["IBLOCK_SECTION_ID"], "ACTIVE" => "Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while ($ob = $res->GetNextElement())
{
   $arItem = $ob->GetFields();
   $arItem["PROPERTIES"] = $ob->GetProperties();
   $arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
   $arItem["DETAIL_PICTURE"] = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);
   if (!in_array($arItem['PROPERTIES']['FIRM']['VALUE'], $arFirms) && ($i < 5))
   {
      $arItems[] = $arItem;
      $arFirms[] = $arItem['PROPERTIES']['FIRM']['VALUE'];
      $i++;
   }
}
foreach ($arItems as $key => $arItem)
{
   if (is_array($arItem["PREVIEW_PICTURE"]))
   {
      $arFileTmp = CFile::ResizeImageGet(
         $arItem["PREVIEW_PICTURE"],
         array("width" => 75, "height" => 75),
         BX_RESIZE_IMAGE_EXACT,
         true
      );

      $arItems[$key]["PREVIEW_PICTURE"] = array(
          "SRC" => $arFileTmp["src"],
          "WIDTH" => $arFileTmp["width"],//IntVal($arSize[0]),
          "HEIGHT" => $arFileTmp["height"]//IntVal($arSize[1]),
      );
   }
}

$arResult['PREMIUM'] = $arPremium;
$arResult['ITEMS'] = $arItems;

$arResult["FILES"] = array();
if (is_array($arResult["PROPERTIES"]["FILES"]["VALUE"]) && count($arResult["PROPERTIES"]["FILES"]["VALUE"]) > 0)
{
   foreach ($arResult["PROPERTIES"]["FILES"]["VALUE"] as $file)
   {
      $file = CFile::GetFileArray($file);
      $file["TYPE"] = GetExtension($file['FILE_NAME']);
      $file["FORMAT_SIZE"] = FormatBytes($file['FILE_SIZE']);
      $file["CLASS"] = GetFileImageClass($file["TYPE"]);
      $arResult["FILES"][] = $file;
   }
}

$arResult["STAT_CODE"] = '';
if (strlen($arResult['PROPERTIES']['FIRM']['VALUE']) > 0)
{
   $res = $objElement->GetByID($arResult['PROPERTIES']['FIRM']['VALUE']);
   if ($obj = $res->GetNextElement())
      $temp = $obj->GetProperty("STAT_CODE");
   {
      if (strlen($temp["VALUE"]) > 0)
         $arResult["STAT_CODE"] = $temp["VALUE"];
   }
}

// �������� ���������� �� ��������� � ������
$arResult["BRANDS"] = array();
$arResult["BRANDS_IDS"] = array();
$arSelect = array("ID", "NAME", "PROPERTY_FIRM", "IBLOCK_ID");
$arFilter = array(
    "IBLOCK_ID" => $arParams['IBLOCK_ID'],
    "SECTION_ID" => $arResult["IBLOCK_SECTION_ID"],
    "INCLUDE_SUBSECTIONS" => "Y",
    "ACTIVE" => "Y"
);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
while ($arFields = $res->GetNext())
{
   if (!in_array($arFields['PROPERTY_FIRM_VALUE'], $arResult["BRANDS_IDS"]))
   {
      $arResult["BRANDS_IDS"][] = $arFields['PROPERTY_FIRM_VALUE'];
   }
}

$arFilter = array(
    "IBLOCK_ID" => 65,
    "ID" => $arResult["BRANDS_IDS"],
    "ACTIVE" => "Y",
    "!PROPERTY_REMOVE_REL" => 63
);
$arResult["BRANDS_IDS"] = array();
$res = CIBlockElement::GetList(array("NAME" => "ASC"), $arFilter);
while ($arFields = $res->GetNext())
{
   if (!in_array($arFields['ID'], $arResult["BRANDS_IDS"]))
   {
      $arResult["BRANDS_IDS"][] = $arFields["ID"];
      $arResult["BRANDS"][$arFields["ID"]] = $arFields;
   }
}


$filter = Array (
   "UF_COMPANY" => $arResult['DISPLAY_PROPERTIES']['FIRM']['VALUE'],
);
$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter); // �������� �������������
if($arUser = $rsUsers->Fetch())
   $arResult["USER_ID"] = $arUser['ID'];

if(ENABLE_PREDSTAVITEL_MODE)
{
   $arPredstavitel = PredstavitelGetByCompanyAgro($arResult['DISPLAY_PROPERTIES']['FIRM']['VALUE']);
   foreach($arPredstavitel["LIST"] as $key => $arEmployee)
   {
      if($arEmployee["STATUS"] == 144)//�������
      {
         $arResult["USER_ID"] = $USER->GetByID($arEmployee["USER"])->Fetch();
         break;
      }
   }
}

$arResult["COMPANY"] = GetIBlockElement($arResult['DISPLAY_PROPERTIES']['FIRM']['VALUE']);
$arResult['DISPLAY_PROPERTIES']['FIRM']['PREVIEW_PICTURE'] = CFile::ShowImage($arResult["COMPANY"]['PREVIEW_PICTURE'], 70, 70);

$arAdress = Array("index", "COUNTRY", "REGION", "CITY", "adress");
$arResult["ADRESS"] = array();
foreach($arAdress as $pid)
{
   $arVal = CIBlockFormatProperties::GetDisplayValue($arResult["COMPANY"], $arResult["COMPANY"]["PROPERTIES"][$pid], "news_out");
   if (strlen($arVal["DISPLAY_VALUE"]))
      $arResult["ADRESS"][] = strip_tags($arVal["DISPLAY_VALUE"]);
}


foreach($arResult["ITEMS"] as $key => $arItem)
{
   $userId = 0;
   $filter = array(
      "UF_COMPANY" => $arItem['PROPERTIES']['FIRM']['VALUE'],
   );
   $rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter); // �������� �������������
   if($arUser = $rsUsers->Fetch())
      $arResult["ITEMS"][$key]["USER_ID"] = $arUser['ID'];
   if(ENABLE_PREDSTAVITEL_MODE)
   {
      $arPredstavitel = PredstavitelGetByCompanyAgro($arItem['PROPERTIES']['FIRM']['VALUE']);
      foreach($arPredstavitel["LIST"] as $key => $arEmployee)
      {
         if($arEmployee["STATUS"] == 144)//�������
         {
            $arResult["ITEMS"][$key]["USER_ID"] = $arEmployee["USER"];
            break;
         }
      }
   }
   $rsUser = CUser::GetByID($arItem['PROPERTIES']['AUTHOR']['VALUE']);
   $arUser = $rsUser->Fetch();
   
   $arResult["ITEMS"][$key]["COMPANY"] = GetIblockElement($arItem['PROPERTIES']['FIRM']['VALUE']);
   $arResult["ITEMS"][$key]["COMPANY"]['PREVIEW_PICTURE_FORMATED'] = CFile::ShowImage($arResult["ITEMS"][$key]["COMPANY"]['PREVIEW_PICTURE'], 80, 50);
}

$arResult["COMPANY_INFO"]["URL"] = array();
if(is_array($arResult["COMPANY"]["PROPERTIES"]["URL"]["VALUE"]) && count($arResult["COMPANY"]["PROPERTIES"]["URL"]["VALUE"]) > 0)
{
   foreach($arResult["COMPANY"]["PROPERTIES"]["URL"]["VALUE"] as $key => $val)
   {
      $val = trim(str_replace(",", "", $val));
      
      if(substr_count($val, "http://") > 0)
      {
         $arResult["COMPANY_INFO"]["URL"][] = array(
             "LINK" => $val,
             "NAME" => $val
         );
      } else 
      {
         $arResult["COMPANY_INFO"]["URL"][] = array(
             "LINK" => "http://".$val,
             "NAME" => $val
         );
      }
   }
}

if(isset($arResult["PROPERTIES"][$arParams["BROWSER_TITLE"]]))
{
    $val = $arResult["PROPERTIES"][$arParams["BROWSER_TITLE"]]["VALUE"];
    if(is_array($val))
        $val = implode(" ", $val);
    $APPLICATION->SetPageProperty("title", $val);
}
elseif(isset($arResult[$arParams["BROWSER_TITLE"]]) && !is_array($arResult[$arParams["BROWSER_TITLE"]]))
{
    $APPLICATION->SetPageProperty("title", $arResult[$arParams["BROWSER_TITLE"]], $arTitleOptions);
}

if(isset($arResult["PROPERTIES"][$arParams["META_KEYWORDS"]]))
{
    $val = $arResult["PROPERTIES"][$arParams["META_KEYWORDS"]]["VALUE"];
    if(is_array($val))
        $val = implode(" ", $val);
    $APPLICATION->SetPageProperty("keywords", $val);
}

if(isset($arResult["PROPERTIES"][$arParams["META_DESCRIPTION"]]))
{
    $val = $arResult["PROPERTIES"][$arParams["META_DESCRIPTION"]]["VALUE"];
    if(is_array($val))
        $val = implode(" ", $val);
    $APPLICATION->SetPageProperty("description", $val);
}