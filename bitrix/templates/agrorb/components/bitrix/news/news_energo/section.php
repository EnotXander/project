<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["USE_RSS"]=="Y"):?>
	<?
	$rss_url = str_replace(
		array("#SECTION_ID#", "#SECTION_CODE#")
		,array(urlencode($arResult["VARIABLES"]["SECTION_ID"]), urlencode($arResult["VARIABLES"]["SECTION_CODE"]))
		,$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss_section"]
	);
	if(method_exists($APPLICATION, 'addheadstring'))
		$APPLICATION->AddHeadString('<link rel="alternate" type="application/rss+xml" title="'.$rss_url.'" href="'.$rss_url.'" />');
	?>
	<a href="<?=$rss_url?>" title="rss" target="_self"><img alt="RSS" src="<?=$templateFolder?>/images/gif-light/feed-icon-16x16.gif" border="0" align="right" /></a>
<?endif?>

<?if($arParams["USE_SEARCH"]=="Y"):?>
<!--
<?=GetMessage("SEARCH_LABEL")?><?$APPLICATION->IncludeComponent(
	"bitrix:search.form",
	"flat",
	Array(
		"PAGE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"]
	),
	$component
);?>
<br />
-->
<?endif?>
<?if($arParams["USE_FILTER"]=="Y"):?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.filter",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
 		"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	),
	$component
);
?>
<br />
<?endif?>
          <div class="right_col">

            <div class="rc_block">
              <ul class="mb_menu">
                <li class="nohover"><span class="gray">������� �������</span><div class="sclew"></div></li>
              </ul>
              <div class="rc_sponsor"><a href="#"><img src="/uploads/banner_300x120_1.jpg" alt=""></a></div>
            </div>

            <div class="rc_block brdr_box">
              <div class="wrap">
                <div class="title"><a href="#">���������� � �������</a></div>
                <div class="faces_row">
                  <div class="image_box"><a href="#"><img src="/uploads/foto_1.jpg" alt=""></a></div>
                  <div class="row">
                    <div class="name"><a href="#">������� ����� ����� ��� ���� �� ����� ������ � �������� �����</a> <span class="date">\ 15.09  10:40</span></div>
                  </div>
                </div><!--faces_row-->

                <div class="faces_row">
                  <div class="image_box"><a href="#"><img src="/uploads/foto_1.jpg" alt=""></a></div>
                  <div class="row">
                    <div class="name"><a href="#">������� ����� ����� ��� ���� �� ����� ������ � �������� �����</a> <span class="date">\ 15.09  10:40</span></div>
                  </div>
                </div><!--faces_row-->

                <div class="faces_row">
                  <div class="image_box"><a href="#"><img src="/uploads/foto_1.jpg" alt=""></a></div>
                  <div class="row">
                    <div class="name"><a href="#">������� ����� ����� ��� ���� �� ����� ������ � �������� �����</a> <span class="date">\ 15.09  10:40</span></div>
                  </div>
                </div><!--faces_row-->

                <div class="faces_row">
                  <div class="image_box"><a href="#"><img src="/uploads/foto_1.jpg" alt=""></a></div>
                  <div class="row">
                    <div class="name"><a href="#">������� ����� ����� ��� ���� �� ����� ������ � �������� �����</a> <span class="date">\ 15.09  10:40</span></div>
                  </div>
                </div><!--faces_row-->

                
              </div>
            
            </div><!--rc_block-->

            <div class="rc_block">
              <div class="right_banner_300x120"><a href="#"><img src="/uploads/banner_300x120_1.jpg" alt=""></a></div>
              <div class="right_banner_300x120"><a href="#"><img src="/uploads/banner_300x120_2.jpg" alt=""></a></div>
              <div class="right_banner_300x120"><a href="#"><img src="/uploads/banner_300x120_3.jpg" alt=""></a></div>
            </div><!--rc_block-->

          </div><!--right_col-->
          <div class="middle_col">
          <div class="breadcrumbs"><?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "news", array(
	"START_FROM" => "0",
	"PATH" => "",
	"SITE_ID" => "s1"
	),
	false,
	array()
);?><a href="#">�������</a>&nbsp;&nbsp;&nbsp;\&nbsp;&nbsp;&nbsp;<a href="#">�������</a>&nbsp;&nbsp;&nbsp;\&nbsp;&nbsp;&nbsp;������������� �������</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"",
	Array(
		"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
		"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
		"NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
		"SORT_BY1"	=>	$arParams["SORT_BY1"],
		"SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
		"SORT_BY2"	=>	$arParams["SORT_BY2"],
		"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
		"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
		"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
		"SET_TITLE"	=>	$arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"ADD_SECTIONS_CHAIN"	=>	$arParams["ADD_SECTIONS_CHAIN"],
		"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
		"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
		"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
		"DISPLAY_NAME"	=>	"Y",
		"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME"	=>	$arParams["FILTER_NAME"],
		"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES"	=>	$arParams["CHECK_DATES"],

		"PARENT_SECTION"	=>	$arResult["VARIABLES"]["SECTION_ID"],
		"PARENT_SECTION_CODE"	=>	$arResult["VARIABLES"]["SECTION_CODE"],
		"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
	),
	$component
);?>

          </div><!--middle_col-->
        </div><!--content_box-->

        <div class="content_box">
          <div class="tags">�������������   <strong>������</strong>  <span class="font_14 gray">����</span>    �������������    <strong>��������</strong>   ����   ������    ����������  ������  �������������   ��������������     <span class="font_16">����������</span>   <span class="font_14 gray">�����</span>   �������������   ������   ����    <strong>�������������</strong>    ��������   ����   ������    <span class="font_18 gray">����������</span>  ������  �������������   ��������������     ����������   �����   <strong>�������������</strong>   ������   ����    �������������    ��������   ����   ������    ����������  ������    ���������  ������  ��������������     <strong>�����</strong>   �������������   ������   ����    �������������    <strong>��������</strong>   ����������  ������  �������������   ������  </div>
