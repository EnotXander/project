<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
	foreach ($arResult["ITEMS"] as $key => $arElement) 
	{
		
		if(is_array($arElement["PREVIEW_PICTURE"]))
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arElement["PREVIEW_PICTURE"],
				array("width" => 100, "height" => 100),
				BX_RESIZE_IMAGE_EXACT,
				true
			);
			//$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);

			$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
				"SRC" => $arFileTmp["src"],
				"WIDTH" => $arFileTmp["width"],
                                "HEIGHT" => $arFileTmp["height"],
			);

		}		
		$arItem["DETAIL_TEXT"] = strip_tags($arItem["DETAIL_TEXT"]);
		$arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["DETAIL_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
		$arResult['ITEMS'][$key] = $arItem;
	}
}
?>