<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<div class="mc_block">
	<a class="btns btn_h32 fl_right mar_15_left mar_5_top" style="width:162px;"><i></i>�������� ����������</a>
	<h1><?=$ar?><sup>1546</sup></h1>
</div><!--mc_block-->
<div class="mc_block">
	<ul class="mb_menu">
		<li class="sel"><a href="#1">��������</a><div class="sclew"></div></li>
		<li><a href="#1">������ � ������</a><div class="sclew"></div></li>
		<li><a href="#1">����������</a><div class="sclew"></div></li>
		<li><a href="#1">�������</a><div class="sclew"></div></li>
		<li style="background:none;"><a href="<?=ADD_COMPANY_URL?>" style="margin-left:15px; background: url(<?=SITE_TEMPLATE_PATH?>/images/plus_green.jpg) left center no-repeat;color: #266BAB !important;text-decoration: underline !important;">���������� ��������</a></li>
		<li style="background:none;color: grey; padding-top: 4px;"> - ��� ������ � ���������</li>
	</ul>
	<div class="clear"></div>
	<div class="paging brdr_bot_0 brdr_box">
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.filter",
			"",
			Array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"FILTER_NAME" => $arParams["FILTER_NAME"],
				"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
				"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			),
			$component
		);
		?>

		<div class="sub_search">
			<span class=" fl_left mar_10_right">�����:</span>
			<div class="input_box">
				<a class="icons search_submit"></a>
				<input class="notUsed" autocomplete="off" id="subSeachId_01" type="text" value="����� � �������">
			</div>
		</div>
		<div class="show_col">
			<span class="meta">���������� ��:</span>
			<select>
				<option>15</option>
				<option>25</option>
				<option>50</option>
			</select>
		</div>
			<?if($arParams["DISPLAY_TOP_PAGER"]):?>
				<?=$arResult["NAV_STRING"]?>
			<?endif;?>
	</div><!--paging-->
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="preview_picture" border="0" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" style="float:left" /></a>
			<?else:?>
				<img class="preview_picture" border="0" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" style="float:left" />
			<?endif;?>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<span class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a><br />
			<?else:?>
				<b><?echo $arItem["NAME"]?></b><br />
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<?echo $arItem["PREVIEW_TEXT"];?>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div style="clear:both"></div>
		<?endif?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
			<small>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			</small><br />
		<?endforeach;?>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<small>
			<?=$arProperty["NAME"]?>:&nbsp;
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
			</small><br />
		<?endforeach;?>
	</p>
<?endforeach;?>
	<div class="paging brdr_top_0 brdr_box mar_20_bot">
		<div class="sub_search">
			<span class=" fl_left mar_10_right">�����:</span>
			<div class="input_box">
				<a class="icons search_submit"></a>
				<input class="notUsed" autocomplete="off" id="subSeachId_02" type="text" value="����� � �������">
			</div>
		</div>
		<div class="show_col">
			<span class="meta">���������� ��:</span>
			<select>
				<option>15</option>
				<option>25</option>
				<option>50</option>
			</select>
		</div>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?>
		<?endif;?>
	</div>
</div>