<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?$APPLICATION->SetPageProperty("LEFT_VISIBLE", "style='display:none;'"); ?>
   <div class="middle_col">
      <table>
         <tr>
            <?if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["PREVIEW_PICTURE"])): ?>
               <td>
                  <img class="detail_picture" border="0" src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arResult["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arResult["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arResult["NAME"] ?>"  title="<?= $arResult["NAME"] ?>" style="float:left;"/>
               </td>
            <?endif;?>
               
         <td style="vertical-align:middle">
            <h1><?= $arResult["NAME"] ?></h1>
         </td>

         <td style="vertical-align:middle; padding-left:15px;">
            <?$APPLICATION->IncludeComponent("bitrix:iblock.vote", "ajax", Array(
                  "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                  "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                  "ELEMENT_ID" => $arResult['ID'],
                  "MAX_VOTE" => "5",
                  "VOTE_NAMES" => Array("1", "2", "3", "4", "5"),
                  "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                  "CACHE_TIME" => $arParams["CACHE_TIME"],
               ), $component
            );?>
         </td>
      </tr>
   </table>

   <div class="clear"></div>
</div>

<div class="right_col">
   <div class="rc_block brdr_box">
      <div class="wrap">
         <h3 style="color:#00A340;">���������� ����������</h3>
         <? $arAddres = Array("index", "COUNTRY", "REGION", "CITY", "adress"); ?>
         <table width="100%" cellpadding="0" class="add_table">
            <tr>
               <td>
                  <img src="<?= SITE_TEMPLATE_PATH ?>/img/addres.jpg">
               </td>
               <td>
                  <? foreach ($arAddres as $pidKey => $pid):
                     if (isset($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])):?><?= $pidKey ? ", " : ""?><?=trim(strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]))?><? endif;
                  endforeach; ?>
               </td>
            </tr>
            <? if (isset($arResult["DISPLAY_PROPERTIES"]["adress_store"]["DISPLAY_VALUE"])): ?>
               <tr>
                  <td>
                     <img src="<?= SITE_TEMPLATE_PATH ?>/img/addres.jpg">
                  </td>
                  <td>
                     <?=strip_tags($arResult["DISPLAY_PROPERTIES"]["adress_store"]["DISPLAY_VALUE"])?>
                  </td>
               </tr>
            <? endif; ?>
            <? if (isset($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])): ?>
               <tr>
                  <td>
                     <img src="<?= SITE_TEMPLATE_PATH ?>/img/tel.jpg">
                  </td>
                  <td><noindex>
                     <? foreach ($arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"] as $phoneKey => $phone): ?>
                        <?= $phoneKey ? "<br>" : ""?>
                        <?= $phone." ".$arResult["DISPLAY_PROPERTIES"]["phone"]["DESCRIPTION"][$phoneKey]?>
                     <? endforeach; ?>
	                  </noindex>
                  </td>
               </tr>
            <? endif; ?>
            <? if (isset($arResult["DISPLAY_PROPERTIES"]["shedule"]["DISPLAY_VALUE"])): ?>
               <tr>
                  <td>
                     <img src="<?= SITE_TEMPLATE_PATH ?>/img/time.jpg">
                  </td>
                  <td>
                     <? echo $arResult["DISPLAY_PROPERTIES"]["shedule"]["DISPLAY_VALUE"]; ?>
                  </td>
               </tr>
            <? endif; ?>
         </table>
         <br/>
         
         <?if (count($arResult["URL"]) > 0): ?>
            <noindex>
               <div>
                  <div class="fl_left" style="margin-right: 10px;">web-����(�):</div> 
                  <div class="fl_left">
                     <?foreach($arResult["URL"] as $key=>$site):?>
                        <a rel="nofollow" target="_blank" href="<?=$site["LINK"]?>"><?=$site["NAME"]?></a><?=(isset($arResult["PROPERTIES"]["URL"]["VALUE"][$key+1]) ? "<br>" : "" )?>
                     <?endforeach;?>
                  </div>
               </div>
               <div class="clear"></div>
               <br/>
               <br/>
            </noindex>
         <?endif; ?>
            
         <?if (is_array($arResult["MANAGER"])):?><noindex>
            <?if($USER->IsAuthorized()):?>
               <a style="width: 140px; float:left;" href="/personal/messages/<?=$arResult["MANAGER"]["ID"]?>/" class="btns gray_btn fl_right" target="_blank"><i></i><img style="margin-bottom: -3px;" src="/bitrix/templates/energorb/images/mail.jpg">&nbsp;�������� ���������</a><br/>
            <?else:?>
               <a style="width: 140px; float:left;" href="#" class="btns gray_btn fl_right" onclick="alert('���������� ��������������');
                  return false;"><i></i><img style="margin-bottom: -3px;" src="/bitrix/templates/energorb/images/mail.jpg">&nbsp;�������� ���������</a><br/>
            <?endif;?>
            <div class="clear"></div>
            <br/>
            <br/></noindex>
         <? endif; ?>

         <?$coord = explode(",", $arResult['PROPERTIES']['MAP']['VALUE']);
         if (count($coord) > 1):?>
            <? // ��������� ������ ��� �����
            $arData = Array("yandex_lat" => $coord[0], "yandex_lon" => $coord[1], "yandex_scale" => 16, "PLACEMARKS" => Array(Array("LON" => $coord[1], "LAT" => $coord[0], "TEXT" => $arResult['NAME'])));
            
            $APPLICATION->IncludeComponent("bitrix:map.yandex.view", ".default", array(
                  "KEY" => "AETqvU8BAAAApI_7dQMAlTDGwZ3hOJGhdVSFYEsVNSpuzTQAAAAAAAAAAACCDIEExtPcFpiKYkYYJlaNR8uKKw==",
                  "INIT_MAP_TYPE" => "MAP",
                  "MAP_DATA" => serialize($arData),
                  "MAP_WIDTH" => 270,
                  "MAP_HEIGHT" => 200,
                  "CONTROLS" => array(),
                  "OPTIONS" => array("ENABLE_SCROLL_ZOOM","ENABLE_DRAGGING",),
                  "MAP_ID" => ""
               ), false
            ); ?>
         <?endif;?>
      </div>
   </div><!--rc_block-->
   
   
   <?if (count($arResult['F_PRICES']) > 0 || count($arResult['F_CATALOG']) > 0 || count($arResult['F_CERTIFICATE']) > 0): ?>
      <h3 class="title" style="color:#00A340;">����� �������� <?= $arResult['NAME'] ?></h3>
      <div class="rc_block toog withoutTitleF b-files">
         <ul class="mb_menu">
            <? // ����
            $sel = true; 
            if(count($arResult['F_PRICES']) > 0):?>
               <li class="<?=$sel ? "sel" : "" ?>">
                  <a href="javascript: void(0)">�����-�����</a><div class="sclew"></div>
               </li>
               <?$sel = false;?>
            <?endif;?>
            <?if(count($arResult['F_CATALOG']) > 0):?>
               <li class="<?=$sel ? "sel" : "" ?>">
                  <a href="javascript: void(0)">��������</a>
                  <div class="sclew"></div>
               </li>
               <?$sel = false;?>
            <?endif; ?>
            <?if(count($arResult['F_CERTIFICATE'])):?>
               <li class="<?=$sel ? "sel" : "" ?>">
                  <a href="javascript: void(0)">�����������</a>
                  <div class="sclew"></div>
               </li>
            <?$sel = false;?>
            <?endif;?>
         </ul>
         
         <?if (count($arResult['F_PRICES']) > 0):?>
            <div class="brdr_box catalog_list tab">
               <div class="wrap">
                  <?foreach ($arResult['F_PRICES'] as $file):?>
                     <div class="files-download">
                        <a href="<?=$file["SRC"]?>" target="_blank"><i class="icons <?=$file["CLASS"]?>"></i><span><?=strlen($file["DESCRIPTION"]) > 0 ? $file["DESCRIPTION"] : "������� ����"?></span><span class="size"> / <?=$file["FORMAT_SIZE"]?></span></a>
                     </div>                        
                  <?endforeach;?>
               </div>
            </div>
         <?endif;?>
         <?if (count($arResult['F_CATALOG']) > 0):?>
            <div class="brdr_box catalog_list tab">
               <div class="wrap">
                  <?foreach ($arResult['F_CATALOG'] as $file):?>
                     <div class="files-download">
                        <a href="<?=$file["SRC"]?>" target="_blank"><i class="icons <?=$file["CLASS"]?>"></i><span><?=strlen($file["DESCRIPTION"]) > 0 ? $file["DESCRIPTION"] : "������� ����"?></span><span class="size"> / <?=$file["FORMAT_SIZE"]?></span></a>
                     </div>                        
                  <?endforeach;?>
               </div>
            </div>
         <?endif;?>
         <?if (count($arResult['F_CERTIFICATE']) > 0):?>
            <div class="brdr_box catalog_list tab">
               <div class="wrap">
                  <?foreach ($arResult['F_CERTIFICATE'] as $file):?>
                     <div class="files-download">
                        <a href="<?=$file["SRC"]?>" target="_blank"><i class="icons <?=$file["CLASS"]?>"></i><span><?=strlen($file["DESCRIPTION"]) > 0 ? $file["DESCRIPTION"] : "������� ����"?></span><span class="size"> / <?=$file["FORMAT_SIZE"]?></span></a>
                     </div>                        
                  <?endforeach;?>
               </div>
            </div>
         <?endif;?>
      </div>
   <?endif;?>
      
   <?if(true)://(is_array($arResult["MANAGER"])):?>
      <div class="rc_block brdr_box">
         <div class="wrap faces_box">
            <div class="title">
               <div class="mb_title_o">
                  <a id="nodec" href="javascript: void(0)">������������� <?= $arResult['NAME'] ?> �� �����</a>
               </div>
            </div>
            <div class="faces_row">
               <?if(is_array($arResult["MANAGER"]) && count($arResult["MANAGER"])):?>
                  <div class="image_box">
                     <img border="0" src="/images/search_placeholder.png" width="48" height="42">
                  </div>
                  <div class="row">
                     <div class="dropdown name">
                        <a class="account js-bubble-tooltip"><?= $arResult["MANAGER"]['NAME'] ?> <?= $arResult["MANAGER"]['SECOND_NAME'] ?> <?= $arResult["MANAGER"]['LAST_NAME'] ?></a>
                        <div class="submenu">
                           <ul class="root">
                              <li><a href="/personal/messages/<?=$arResult["MANAGER"]["ID"]?>/">�������� ���������</a></li>
                           </ul>
                        </div>
                     </div>
                     <!--<div class="post"></div>
                     <div class="company"></div>-->
                  </div>
               <?endif;?>
               <?if($USER->IsAuthorized()):?>
                  <?if($arResult["MANAGER"]["ID"] != $USER->GetID()):?>
                     <div class="js-make-predstavitel">
                        <?if(isset($arResult["PREDSTAVITEL_STATUS"])):?>
                           <?if($arResult["PREDSTAVITEL_STATUS"] == 68)://���������������?>
                              ���� ������ �� ������������
                           <?elseif($arResult["PREDSTAVITEL_STATUS"] == 70)://���������������?>
                              ���� ������ ���������
                           <?endif;?>
                        <?else:?>
                           <a data-company-id="<?=$arResult["ID"]?>" href="javascript:void(0);">����� ��������������</a>
                        <?endif;?>
                     </div>
                  <?endif;?>
               <?else:?>
                  <a href="/auth/?register=yes&backurl=<?=urlencode($arResult["DETAIL_PAGE_URL"])?>">����� ��������������</a>
               <?endif;?>
               <!-- <div class="socials"><a title="vk" class="icons icon_vk2" href="#"></a><a title="facebook" class="icons icon_face2" href="#"></a><a title="twitter" class="icons icon_twit2" href="#"></a></div>-->
            </div><!--faces_row-->
         </div>
      </div>
   <?endif;?>
   <?if(count($arResult["STAFF"])):?>
      <div class="rc_block brdr_box">
         <div class="wrap faces_box">
            <div class="title">
               <div class="mb_title_o">
                  <a id="nodec" href="javascript: void(0)">��������� <?= $arResult['NAME'] ?> �� �����</a>
               </div>
            </div>
            <?  foreach ($arResult["STAFF"] as $arUser):?>
               <div class="faces_row">
                  <div class="image_box"><?=CFile::ShowImage($arUser['PERSONAL_PHOTO'], 50, 50); ?></div>
                  <div class="row">
                     <div class="name"><?= $arUser['NAME'] ?> <?= $arUser['SECOND_NAME'] ?> <?= $arUser['LAST_NAME'] ?></div>
                     <div class="post"></div>
                     <div class="company"></div>
                  </div>
                  <!-- <div class="socials"><a title="vk" class="icons icon_vk2" href="#"></a><a title="facebook" class="icons icon_face2" href="#"></a><a title="twitter" class="icons icon_twit2" href="#"></a></div>-->
               </div><!--faces_row-->
            <?  endforeach;?>
         </div>
      </div>
   <?endif;?>
   <?//��������������������� �������
   /*
   $arFilter = array(
       "IBLOCK_ID" => 11,
       "IBLOCK_ACTIVE" => "Y",
       "SECTION_ID" => 0,
       "CODE" => $arResult["MANAGER"]["LOGIN"]
   );
   $db_res = CIBlockSection::GetList(array("ID" => "DESC"), $arFilter, false, array(
               "ID",
               "CREATED_BY",
               "ACTIVE",
               "IBLOCK_SECTION_ID",
               "NAME",
               "PICTURE",
               "DESCRIPTION",
               "DESCRIPTION_TYPE",
               "CODE",
               "SOCNET_GROUP_ID",
               "PICTURE",
               "UF_DEFAULT",
               "UF_GALLERY_SIZE",
               "UF_GALLERY_RECALC",
               "UF_DATE"
                   )
   );

   if (($res = $db_res->GetNext()) && is_array($arResult["MANAGER"])):?>
      <div class="rc_block brdr_box">
         <div class="wrap">
            <h3 style="color:#00A340;">���� <?= $arResult['NAME'] ?></h3>
            <ul class="company_photo">
               <?
               $arSelect = Array("ID", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID", "NAME");
               $arFilter = Array("IBLOCK_ID" => 11, "SECTION_ID" => $res['ID'], "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE" => "Y");
               $res = CIBlockElement::GetList(Array("rand" => "asc"), $arFilter, false, Array("nPageSize" => 6), $arSelect);
               while ($ob = $res->GetNextElement()):?>
                  <?$arFields = $ob->GetFields();?>
                  <li><a href="/gallery/<?= $arResult["MANAGER"]["LOGIN"] ?>/<?= $arFields['IBLOCK_SECTION_ID'] ?>/"><img src="<?= CFile::GetPath($arFields['PREVIEW_PICTURE']) ?>"></a></li>
               <?endwhile;?>	
            </ul>
            <div class="clear"></div>
            <br/>
            <a href="/gallery/<?= $arResult["MANAGER"]["LOGIN"] ?>/" class="nodecor bold">��� ����</a>
         </div>
      </div>
   <?endif;*/?>      

   <?if ($arResult['PROPERTIES']['VIDEO']["VALUE"]["path"] || strlen($arResult['PROPERTIES']['VIDEO_LINK']["VALUE"]) > 0): ?>
      <?if (strlen($arResult['PROPERTIES']['VIDEO_LINK']["VALUE"]) > 0):?>
         <? $link = $arResult['PROPERTIES']['VIDEO_LINK']["VALUE"];?>
      <?elseif ($arResult['PROPERTIES']['VIDEO']["VALUE"]["path"]):?>
         <? $link = $arResult['PROPERTIES']['VIDEO']["VALUE"]["path"];?>
      <?endif; ?>

      <div class="rc_block brdr_box"> 
         <div class="wrap"> 
            <h3 style="color:#00A340;">����� <?= $arResult['NAME'] ?></h3>
            <div class="video_box">
               <a href="javascript:void(0);" onclick="showPlayer('<?= $link ?>');"><?=$arResult['PROPERTIES']['VIDEO_IMG']["VALUE"] ? CFile::ShowImage($arResult['PROPERTIES']['VIDEO_IMG']["VALUE"], 270, 200) : $arResult['NAME']?></a><br>
            </div>
         </div>
      </div>
      <div style="position: relative;">
         <div id="flvplayer">
            <div class="swfframe" id="swfframe"></div>
         </div>	
      </div>
   <?endif;?>     
</div>
<div class="middle_col">
   <?if(strlen($arResult["PREVIEW_TEXT"]) || strlen($arResult["DETAIL_TEXT"]) || strlen($arResult["FIELDS"]["PREVIEW_TEXT"])):?>
      <div class="brdr_box company_description_block" style="border-color:#ffab09; background-color:#fdfee8;">
         <div style="padding:9px;">
            <? if ($arParams["DISPLAY_DATE"] != "N" && $arResult["DISPLAY_ACTIVE_FROM"]): ?>
               <span class="news-date-time"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
            <? endif; ?>
            <? if ($arParams["DISPLAY_NAME"] != "N" && $arResult["NAME"]): ?>
               <!-- <h2 style="color:#00A340;"><?= $arResult["NAME"] ?></h2> -->
            <? endif; ?>
            <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arResult["FIELDS"]["PREVIEW_TEXT"]): ?>
               <p class="js-fancybox_table"><?= $arResult["FIELDS"]["PREVIEW_TEXT"];
            unset($arResult["FIELDS"]["PREVIEW_TEXT"]); ?></p>
            <? endif; ?>
            <? if ($arResult["NAV_RESULT"]): ?>
               <? if ($arParams["DISPLAY_TOP_PAGER"]): ?><?= $arResult["NAV_STRING"] ?><br /><? endif; ?>
               <? echo $arResult["NAV_TEXT"]; ?>
               <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?><br /><?= $arResult["NAV_STRING"] ?><? endif; ?>
            <? elseif (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
               <div class="js-fancybox_table">
                  <? echo $arResult["DETAIL_TEXT"]; ?>
               </div>
            <? else: ?>
               <div class="js-fancybox_table">
               <? echo $arResult["PREVIEW_TEXT"]; ?>
               </div>
            <? endif ?>
            <div class="clearfix"></div>
         </div>
      </div>
   <?endif;?>
   
   <?if(count($arResult["CATEGORY"]) > 0):?>
      <div class="mc_block">
         <?if(strlen($arResult["PREVIEW_TEXT"]) || strlen($arResult["DETAIL_TEXT"]) || strlen($arResult["FIELDS"]["PREVIEW_TEXT"])):?>
            <br/>
         <?endif;?>
         <b>� ����������:</b>&nbsp;<?=implode(", ", $arResult["CATEGORY"]);?>
      </div>
   <?endif;?>   
   <div style="clear:both"></div><br />
   
   
   <?if($arResult["SHOW_TABS"]):?>
	<h3 class="title" style="font-family: Arial, Helvetica, sans-serif;
		   font-size: 12px; margin-left: 0; padding-left: 0;
		   font-weight: bold; margin-bottom: 10px">������ ��������� "<?= $arResult['NAME'] ?>"</h3>
      <a name="tabs"></a>
      <div class="mc_block withoutTitleM">
         <ul class="mb_menu">
            <?foreach($arResult["TABS"] as $tab):?>            
               <?if(strlen($tab["HTML"]) > 0):?>               
                  <li class="<?=$tab["ACTIVE"] || $arResult["FIRST_ACTIVE"] ? "sel" : ""?>">
                     <a href="javascript: void(0)"><?=$tab["NAME"]?></a>
                     <div class="sclew"></div>
                  </li>
                  <?if($arResult["FIRST_ACTIVE"])
                     $arResult["FIRST_ACTIVE"] = false;?>
               <?endif;?>
            <?endforeach;?>
         </ul>

         <?foreach($arResult["TABS"] as $tab):?>
            <?if(strlen($tab["HTML"]) > 0 && strlen($tab["SECTIONS_HTML"]) > 0):?> 
               <div class="tab">
                  <?=$tab["SECTIONS_HTML"]?>
                  <?=$tab["HTML"];?>
               </div>
            <?endif;?>
         <?endforeach;?>
      </div>
      <div style="clear:both"></div>
      <br />  
   <?endif;?>
      
      
      
   
   <?
   $na = false;
   $nn = true;
   $aa = true;
   $arSelectNA = Array("ID", "NAME", "DATE_ACTIVE_FROM");
   $arFilterNA = Array("IBLOCK_ID" => 3, "PROPERTY_FIRM" => $arResult['ID'], "ACTIVE" => "Y");
   $res = CIBlockElement::GetList(Array(), $arFilterNA, false, Array("nPageSize" => 50), $arSelectNA);
   if ($ob = $res->GetNextElement()) {
      $na = true;
      $nn = true;
   }
   $arSelectNA = Array("ID", "NAME", "DATE_ACTIVE_FROM");
   $arFilterNA = Array("IBLOCK_ID" => 2, "PROPERTY_FIRM" => $arResult['ID'], "ACTIVE" => "Y");
   $res = CIBlockElement::GetList(Array(), $arFilterNA, false, Array("nPageSize" => 50), $arSelectNA);
   if ($ob = $res->GetNextElement()) {
      $na = true;
      $aa = true;
   }
   ?>
         <? if ($na) {
            ?>
      <h3 class="title" style="color:#00A340;">������� � ������ <?= $arResult['NAME'] ?></h3>
      <div class="mc_block withoutTitle">
         <ul class="mb_menu">
            <? if ($nn): ?>
               <li class="sel"><a href="#">�������</a><div class="sclew"></div></li>
         <? endif; ?>
         <? if ($aa): ?>
               <li <? if (!$nn): ?>class="sel"<? endif; ?>><a href="#">������</a><div class="sclew"></div></li>
         <? endif; ?>
         </ul>
         <? if ($nn): ?>
            <? global $arCompanyFilter;
            $arCompanyFilter['PROPERTY_FIRM'] = $arResult['ID']
            ?>
            <?
            $APPLICATION->IncludeComponent("bitrix:news.list", "list-tabs", Array(
                "IBLOCK_TYPE" => "news", // ��� ��������������� ����� (������������ ������ ��� ��������)
                "IBLOCK_ID" => "3", // ��� ��������������� �����
                "NEWS_COUNT" => "3", // ���������� �������� �� ��������
                "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                "FILTER_NAME" => "arCompanyFilter", // ������
                "FIELD_CODE" => array(// ����
                    0 => "DETAIL_TEXT",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(// ��������
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                "AJAX_MODE" => "N", // �������� ����� AJAX
                "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                "CACHE_TYPE" => "N", // ��� �����������
                "CACHE_TIME" => "36000000", // ����� ����������� (���.)
                "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                "CACHE_GROUPS" => "Y", // ��������� ����� �������
                "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"], // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                "SET_TITLE" => "Y", // ������������� ��������� ��������
                "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // �������� �������� � ������� ���������
                "ADD_SECTIONS_CHAIN" => "N", // �������� ������ � ������� ���������
                "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                "PARENT_SECTION" => "", // ID �������
                "PARENT_SECTION_CODE" => "", // ��� �������
                "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                "PAGER_TITLE" => "�������", // �������� ���������
                "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                "PAGER_TEMPLATE" => "", // �������� �������
                "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                "DISPLAY_DATE" => "Y", // �������� ���� ��������
                "DISPLAY_NAME" => "Y", // �������� �������� ��������
                "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                    ), false
            );
            ?>
         <? endif; ?>
         <? if ($aa): ?>
            <?
            $APPLICATION->IncludeComponent("bitrix:news.list", "list-tabs", Array(
                "IBLOCK_TYPE" => "articles", // ��� ��������������� ����� (������������ ������ ��� ��������)
                "IBLOCK_ID" => "2", // ��� ��������������� �����
                "NEWS_COUNT" => "3", // ���������� �������� �� ��������
                "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                "FILTER_NAME" => "arCompanyFilter", // ������
                "FIELD_CODE" => array(// ����
                    0 => "DETAIL_TEXT",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(// ��������
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                "AJAX_MODE" => "N", // �������� ����� AJAX
                "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                "CACHE_TYPE" => "N", // ��� �����������
                "CACHE_TIME" => "36000000", // ����� ����������� (���.)
                "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                "CACHE_GROUPS" => "Y", // ��������� ����� �������
                "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"], // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                "ACTIVE_DATE_FORMAT" => "d.m h:m", // ������ ������ ����
                "SET_TITLE" => "Y", // ������������� ��������� ��������
                "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // �������� �������� � ������� ���������
                "ADD_SECTIONS_CHAIN" => "N", // �������� ������ � ������� ���������
                "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                "PARENT_SECTION" => "", // ID �������
                "PARENT_SECTION_CODE" => "", // ��� �������
                "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                "PAGER_TITLE" => "�������", // �������� ���������
                "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                "PAGER_TEMPLATE" => "", // �������� �������
                "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                "DISPLAY_DATE" => "Y", // �������� ���� ��������
                "DISPLAY_NAME" => "Y", // �������� �������� ��������
                "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                    ), false
            );
            ?>
   <? endif; ?>
      </div>
<? } ?>
   <!--mc_block-->
   <!--<ul class="mb_menu">
         <li class="mb_title"><a href="/news/5/">������</a><div class="sclew"></div></li>
         <li><a href="#3">������</a><div class="sclew"></div></li>
         <li><a href="#3">�������</a><div class="sclew"></div></li>
   </ul>	
   <img src="/upload/tov.jpg"><br/>
<ul class="mb_menu">
         <li class="mb_title"><a href="/news/5/">�������</a><div class="sclew"></div></li>
         <li><a href="#3">������</a><div class="sclew"></div></li>
   </ul>
   <img src="/upload/tov2.jpg">
   -->
   
   
      <?if($arResult['DISPLAY_PROPERTIES']['BRANDS']['DISPLAY_VALUE']):?>
         <h3 class="title" style="color:#00A340;">� �������������� ������� <?= $arResult['NAME'] ?></h3>
         <hr/>
         <table width="100%">
            <tr>
               <?foreach($arResult['PROPERTIES']['BRANDS']['VALUE'] as $val):?>
                  <?if ($val > 0):?>
                     <?$arBrand = GetIblockElement($val);?>
                     <td>
                        <? echo CFile::ShowImage($arBrand['PREVIEW_PICTURE'], 100, 100); ?>
                        <br/>
                        <a href="<?= $arBrand['DETAIL_PAGE_URL'] ?>"><?= $arBrand['NAME'] ?></a>
                     </td>
                  <? endif; ?>
               <?endforeach;?>
            </tr>
         </table>
      <?endif;?>
         
      <?if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y"):?>
         <div class="news-detail-share">
            <noindex>
               <?$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
                     "HANDLERS" => $arParams["SHARE_HANDLERS"],
                     "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                     "PAGE_TITLE" => $arResult["~NAME"],
                     "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                     "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                     "HIDE" => $arParams["SHARE_HIDE"],
                  ), $component, array("HIDE_ICONS" => "Y")
               ); ?>
            </noindex>
         </div>
      <?endif;?>
      <div style="clear:both"></div>
      
   <br/>
   
   
   <?if(strlen($arResult["STAT_CODE"]) > 0): // ������� ������ ?>
      <script type="text/javascript">
      <!--
      Nnv=navigator;Nna=Nnv.appName;Nd=document;Nd.cookie="b=b";Nc=0;if(Nd.cookie)Nc=1;
      Nn=(Nna.substring(0,2)=="Mi")?0:1;Ns=screen;Npx=(Nn==0)?Ns.colorDepth:Ns.pixelDepth;
      str='<img src="http://c.energobelarus.by/<?=$arResult["STAT_CODE"]?>;'+Ns.width+'x'+Ns.height+';'+Npx+';'+Nc+';';
      str=str+escape(Nd.referrer)+';'+Math.random()+'" width="88" height="31" border="0">';
      document.write('<a href="http://stat.energobelarus.by/c/cstats?<?=$arResult["STAT_CODE"]?>;main;0;0;1;2" alt="Energobelarus" target="_blank">'+str+'</a>');
      // -->
      </script>
      <noscript>
         <a href="http://stat.energobelarus.by/c/cstats?<?=$arResult["STAT_CODE"]?>;main;0;0;1;2" target="_blank"><img src="http://c.energobelarus.by/<?=$arResult["STAT_CODE"]?>;0x0;0;0;-;0" alt="Energobelarus" width="88" height="31" border="0"></a>
      </noscript>
   <?endif;?>

    <?if($arResult['PROPERTIES']['METRIKA_ID']["VALUE"])// ������� ������.�������
        YandexMetrika::setCounter($arResult['PROPERTIES']['METRIKA_ID']["VALUE"]);?>