<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?if ($_REQUEST['action'] == 'ADD_TO_COMPARE_LIST')
   $_SESSION['FAVOR' . $arParams['IBLOCK_ID']]['ITEMS'][] = (int)$_REQUEST['id'];?>

<?if(count($_REQUEST["error"])):
   $print = implode("\n", $_REQUEST["error"]);?>
   <script>
      $(document).ready(function(){
         alert("<?=$print?>");
      });
   </script>
<?endif;?>

<div class="middle_col" style="padding-bottom: 600px;">
   <div class="mc_block js-sortable-container">
      <form method="post" name="top_f" id="form_predstavitel_list" class="form_predstavitel_list">
         <?=bitrix_sessid_post()?>
         <div class="paging brdr_bot_0 brdr_box">
            <div class="show_col">
               <!--<input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">-->
               <span class="meta">���������� ��:</span>
               <select name="per_page" onchange="document.top_f.submit()">
                  <option value="20" <? if ($arResult["per_page"] == 20) echo "selected"; ?>>20</option>
                  <option value="50" <? if ($arResult["per_page"] == 50) echo "selected"; ?>>50</option>
                  <option value="100" <? if ($arResult["per_page"] == 100) echo "selected"; ?>>100</option>
               </select>
            </div>
            <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
               <?= $arResult["NAV_STRING"] ?>
            <? endif; ?>
         </div><!--paging-->

         <?if(is_array($arResult["SUCCESS"]) && count($arResult["SUCCESS"])):?>
            <div class="bds_row" style="background: #F3FFEF;">
               <?=implode("<br>", $arResult["SUCCESS"])?>
            </div>
         <?endif;?>
         <?if(is_array($arResult["ERROR"]) && count($arResult["ERROR"])):?>
            <div class="bds_row" style="background: #FFEFEF;">
               <?=implode("<br>", $arResult["ERROR"])?>
            </div>
         <?endif;?>
         <div class="bds_row" style="background: #f5f5f5;">
            <div class="row_date">���� ������</div>
            <div class="row_company">��������</div>
            <div class="row_user">����� ������</div>
            <div class="row_exists">������� �������������</div>
            <div class="row_control">V | X</div>
            <div class="clear"></div>
         </div>
         <? foreach ($arResult["ITEMS"] as $arItem): ?> 
            <div class="bds_row">
               <div class="row_date"><?=$arItem["DATE_CREATE"]?></div>
               <div class="row_company">
                  <a href="<?=$arItem["COMPANY"]["DETAIL_PAGE_URL"]?>"><?=$arItem["COMPANY"]["NAME"]?></a>
               </div>
               <div class="row_user"><?=$arItem["USER"]["NAME_FORMATTED"]?></div>
               <div class="row_exists"><?=(strlen($arItem["EXISTS"]["USER"]["NAME_FORMATTED"])) ? $arItem["EXISTS"]["USER"]["NAME_FORMATTED"] : '-'?></div>
               <div class="row_control">
                  <input type="checkbox" name="edit[<?=$arItem["ID"]?>]" value="<?=$arParams["PROPERTY_ID"]["ACCEPT"]?>">
                  <input type="checkbox" name="edit[<?=$arItem["ID"]?>]" value="<?=$arParams["PROPERTY_ID"]["DENY"]?>">
               </div>
               <div class="clear"></div>
            </div><!--bds_row-->
         <? endforeach; ?>
         <div class="bds_row bds_row_new js-row-new">
            �������� �������������
         </div>
         <div class="bds_row bds_row_form js-row-form">
            <div class="row_date"><?=$arItem["DATE_CREATE"]?></div>
            <div class="row_company">
               <select class="js-select-company" style="width: 90%;">
                  <option value="">�������� ��������</option>
                  <?foreach($arResult["COMPANY"] as $company):?>
                     <option value="<?=$company["ID"]?>" rel="<?=$arResult["EXISTS"][$company["ID"]]["USER"]["NAME_FORMATTED"]?>"><?=$company["NAME"]?></option>
                  <?endforeach;?>
               </select>
            </div>
            <div class="row_user">
               <select class="js-select-user" style="width: 90%;">
                  <option value="">�������� ������������</option>
                  <?foreach($arResult["USERS"] as $user):?>
                     <option value="<?=$user["ID"]?>"><?=$user["NAME_FORMATTED"]?></option>
                  <?endforeach;?>
               </select>
            </div>
            <div class="row_exists js-row-form-exists">&nbsp;</div>
            <div class="row_control">
               <input type="button" name="close" value="�������">
               <input type="hidden" name="new[COMPANY]" value="">
               <input type="hidden" name="new[USER]" value="">
            </div>
            <div class="clear"></div>
         </div><!--bds_row-->
         
         <div class="paging brdr_top_0 brdr_box mar_20_bot">
            <input type="submit" name="���������">
            <div class="show_col">
               <span class="meta">���������� ��:</span>
               <select name="per_page" onchange="document.bot_f.submit()">
                  <option value="20" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 20) echo "selected"; ?>>20</option>
                  <option value="50" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 50) echo "selected"; ?>>50</option>
                  <option value="100" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 100) echo "selected"; ?>>100</option>
               </select>
            </div>
            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
               <?= $arResult["NAV_STRING"] ?>
            <? endif; ?>
         </div>
      </form>
   </div>
</div>