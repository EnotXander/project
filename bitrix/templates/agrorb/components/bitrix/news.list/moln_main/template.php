<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//echo "<pre>"; print_r($arResult); echo "</pre>";?>
<?
  $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
  $numChars = strlen($chars);
  $keyForC = '';
  for ($i = 0; $i < 8; $i++) {
    $keyForC .= substr($chars, rand(1, $numChars) - 1, 1);
  }
?>
<div class="news_comp mc_block tab">

<ul class="news_link_list"> 


<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
	/*$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));*/
	?>
	<li id="<?=$arItem['ID']?>" <?if ($key == 0) echo 'class="active"';?>><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span><?=$arItem["NAME"]?></span></a></li>
<?endforeach;?>
</ul>
<?foreach($arResult["ITEMS"] as $key => $arItem):?>

<div class="news_image <?=$keyForC?>" <?if ($key<>0):?>style="display:none;"<?endif;?> id="img_<?=$arItem['ID']?>" >
<img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" >
</div>

<?endforeach;?>
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
<div class="news_text <?=$keyForC?>" <?if ($key<>0):?>style="display:none;"<?endif;?> id="txt_<?=$arItem['ID']?>">
<p><?echo $arItem["PREVIEW_TEXT"];?></p>
</div>
<?endforeach;?>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.news_comp .news_link_list li').hover(
		function () {
			$(this).parent().find('li').removeClass('active');
			$(this).addClass('active');
			var id = $(this).attr("id");
			$(this).parent().parent().find('.news_image').hide();
			$(this).parent().parent().find('.news_text').hide();
			$('#img_'+id).show();
			$('#txt_'+id).show();
		},
		function () {
		}
	);
});
</script>