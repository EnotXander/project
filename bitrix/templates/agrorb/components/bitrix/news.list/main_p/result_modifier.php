<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!CModule::IncludeModule("iblock")) die();
$obParser = new CTextParser;
$arResult['SECTION']['PATH'][0]['DESCRIPTION'] = $obParser->html_cut( $arResult['SECTION']['PATH'][0]['DESCRIPTION'], 50);
$arResult['SECTION']['LINK'] = "/photo/".$arResult["ITEMS"][0]["IBLOCK_SECTION_ID"]."/";

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement) 
   {
      if(($arParams["IBLOCK_ID"] == 10) && ($arElement["IBLOCK_SECTION_ID"]))
      {
         $rsSection = CIBlockSection::GetByID($arElement["IBLOCK_SECTION_ID"]);
         if($arSection = $rsSection->GetNext())
         {
            $arSection["DATE_CREATE"] = date("d.m Y", MakeTimeStamp($arSection["DATE_CREATE"], CLang::GetDateFormat("FULL")));
            $arResult["ITEMS"][$key]["SECTION"] = $arSection;
         }
      }
      else
      {
         $arResult["ITEMS"][$key]["SECTION"] = array(
             "NAME" => $arElement["NAME"],
             "DATE_CREATE" => date("d.m Y", MakeTimeStamp($arElement["DATE_CREATE"], CLang::GetDateFormat("FULL")))
         );
      }
      if(is_array($arElement["DETAIL_PICTURE"]) || is_array($arElement["PREVIEW_PICTURE"]))
      {
         $FILE = is_array($arElement["DETAIL_PICTURE"]) ? $arElement["DETAIL_PICTURE"] : $arElement["PREVIEW_PICTURE"];
         $arFileTmp = CFile::ResizeImageGet(
            $FILE,
            array("width" => 85, "height" => 85),
            BX_RESIZE_IMAGE_EXACT,
            true,
            array()
         );
         $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = array(
            "SRC" => $arFileTmp["src"],
            "WIDTH" => 85,
            "HEIGHT" => 85,
         );
      }		
   }
}