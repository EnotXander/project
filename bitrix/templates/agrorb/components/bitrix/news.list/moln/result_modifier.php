<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$arItem["DETAIL_TEXT"] = strip_tags($arItem["DETAIL_TEXT"]);
	$arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["DETAIL_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
	//echo $arItem["DETAIL_TEXT"];
	$arFileTmp = CFile::ResizeImageGet(
		$arItem["DETAIL_PICTURE"],
		array("width" => 250, "height" => 250),
		BX_RESIZE_IMAGE_EXACT,
		true
	);
	$arItem["DETAIL_PICTURE"] = array(
		"SRC" => $arFileTmp["src"],
		"WIDTH" => $arFileTmp["width"],
                "HEIGHT" => $arFileTmp["height"],
	);

	$arResult['ITEMS'][$key] = $arItem;
}
?>