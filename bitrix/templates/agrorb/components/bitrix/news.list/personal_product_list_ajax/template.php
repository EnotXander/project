<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<? foreach ($arResult["ITEMS"] as $arItem): ?> 
   <div class="bds_row<?= ($arItem["ACTIVE"] == "Y") ? '' : ' inactive' ?> js-fav-item ui-state-default js-sortable-item" data-id="<?= $arItem["ID"] ?>">
      <table>
         <tbody>
            <tr>
               <td class="ic_star" style="vertical-align:top;">
                  <input type="checkbox" class="js-check-item" name="check_item[]" value="<?= $arItem["ID"] ?>">
                  <!--<a title="<?= (in_array($USER->GetID(), $arItem["PROPERTIES"]["FAVORITES"]["VALUE"])) ? '������� �� ����������' : '�������� � ���������' ?>" href="javascript:void(0)" onclick="AddToFavorites({productId: <?= $arItem['ID'] ?>, context: $(this).find('.icon_star'), inFavorites: function(){this.addClass('star_sel').closest('a').attr('title', '������� �� ����������')}, outFavorites: function(){this.removeClass('star_sel').closest('a').attr('title', '�������� � ���������')}})">
                     <i class="icons icon_star<? if (in_array($USER->GetID(), $arItem["PROPERTIES"]["FAVORITES"]["VALUE"])): ?> star_sel<? endif; ?>"></i>
                  </a>-->
               </td>
               <td class="image_box">
                  <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                     <a title="���������� ���������" href="<?= ($arParams["IBLOCK_ID"] == 58) ? 'admin.php' : 'admin_serv.php' ?>?PRODUCT_ID=<?= $arItem["ID"] ?>">
                        <img alt="" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                     </a>
                  <? else: ?>
                     <img src="<?= SITE_TEMPLATE_PATH ?>/images/withoutphoto.jpg" width="72" height="72" align=""/>  
                  <? endif; ?>
               </td>
               <td class="bds_info">
                  <div class="tdWrap">
                     <div class="bds_title"><a href="<?= ($arParams["IBLOCK_ID"] == 58) ? 'admin.php' : 'admin_serv.php' ?>?PRODUCT_ID=<?= $arItem["ID"] ?>"><?= $arItem["NAME"] ?></a></div>
                     <div class="meta"><?= $arItem['PREVIEW_TEXT'] ?></div>
                  </div>                      
               </td>
               <td class="bds_price">
                  <? // PrintObject($arItem);?>
                  <b><span class="js-edit-field" data-code="COST"><?= $arItem['PROPERTIES']['COST']['VALUE'] ? $arItem['PROPERTIES']['COST']['VALUE'] : '0' ?></span>&nbsp;BYR<br></b>
                  <!--<b><span class="js-edit-field" data-code="SORT_COMPANY"><?= $arItem['PROPERTIES']['SORT_COMPANY']['VALUE'] ? $arItem['PROPERTIES']['SORT_COMPANY']['VALUE'] : '0' ?></span></b>&nbsp;� ����������-->
               </td>
               <td class="favorites-delete">
                  <a href="javascript:void(0)" class="personal-icon personal-icon-copy js-item-copy" title="����������"></a>
                  <a href="javascript:void(0)" class="personal-icon personal-icon-sort sortable-button js-sortable-button" title="����������"></a>
                  <a href="javascript:void(0)" class="personal-icon <?= ($arItem["ACTIVE"] == "Y") ? 'personal-icon-active js-item-disable' : 'personal-icon-deactive js-item-enable' ?>" title="<?= ($arItem["ACTIVE"] == "Y") ? '��������������' : '������������' ?>"></a>
                  <a href="<?= $arParams["EDIT_URL"] ?>?PRODUCT_ID=<?= $arItem["ID"] ?>" title="�������������" class="personal-icon personal-icon-edit js-item-edit"></a>
                  <a href="javascript:void(0)" class="personal-icon personal-icon-delete js-item-delete" title="�������"></a>
               </td>
            </tr>
         </tbody>
      </table>
   </div><!--bds_row-->
<? endforeach; ?>