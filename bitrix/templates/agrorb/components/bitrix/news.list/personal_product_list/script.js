$(document).ready(function(){
   var editMode = false;
   var UpdateCheckCounter = function(){
      var counterAll = $('.js-check-item').size();
      var counter = $('.js-check-item:checked').size();
      $('.js-check-counter').text(counter);
      if(counter){
         $('#form_product_list input[name=delete_product]').removeAttr("disabled");
         $('#form_product_list input[name=disable]').removeAttr("disabled");
         $('#form_product_list input[name=edit]').removeAttr("disabled");
         $('#form_product_list input[name=enable]').removeAttr("disabled");
      }else{
         $('#form_product_list input[name=delete_product]').attr("disabled", "disabled");
         $('#form_product_list input[name=disable]').attr("disabled", "disabled");
         $('#form_product_list input[name=edit]').attr("disabled", "disabled");
         $('#form_product_list input[name=enable]').removeAttr("disabled");
      }
      if(!$('.js-edit-field').size()){
         if(counter == 1){
            $('#form_product_list input[name=edit]').removeAttr("disabled");
         }else{
            $('#form_product_list input[name=edit]').attr("disabled", "disabled");
         }
      }
      if(counter == counterAll){
         $('.js-check_all').attr("checked", true);
      }else{
         $('.js-check_all').attr("checked", false);
      }
   }
   //��������� �������
   $('.js-check-item').live("click", function(){
      UpdateCheckCounter();
   });
   $('.js-check_all').live("click", function(){
      if($(this).attr("checked")){
         $('.js-check-item').attr("checked", "checked");
      }else{
         $('.js-check-item').removeAttr("checked");
      }
      console.log($(this).attr("checked"));
      UpdateCheckCounter();
   });
   //�������� �������
   $('#form_product_list input[name=add_product]').live("click", function(){
      location.href = $(this).attr("data-link");
   });
   //������������� ������� (����� ������)
   $('#form_product_list input[name=edit]').live("click", function(){
      if(editMode){//����� �������������� -> ���������
         editMode = false;
         //$(this).click();
      }else{//����� ��������� -> ���������������
         if($('.js-check-item:checked').size() == 1){
            location.href = $(this).attr("data-link")+"?PRODUCT_ID="+$('.js-check-item:checked').closest("tr").attr("data-id");
         }else if($('.js-check-item:checked').size() > 1){
            if($('.js-edit-field').size()){
               editMode = true;
               $(this).val("���������");
               $('.js-check-item:checked').each(function(index, domElement){
                  var $row = $(domElement).closest('tr');
                  var rowId = $row.attr("data-id");
                  $row.find('.js-edit-field').each(function(fieldIndex, fieldDomElement){
                     var code = $(fieldDomElement).attr("data-code");
                     var index = $(fieldDomElement).text();
                     $(fieldDomElement).html('<input type="text" name="save['+rowId+'][PROPERTY_VALUES]['+code+']" value="'+index+'">');
                  });
               });
            }
         }
         return false;
      }
   });
   //�������� �������
   $('.js-item-enable').live("click", function(){
      var $row = $(this).closest("tr");
      $('.js-check-item').removeAttr("checked");
      $row.find('.js-check-item').attr("checked", "checked");
      UpdateCheckCounter();
      $('#form_product_list input[name=enable]').click();
   });
   //��������� �������
   $('.js-item-disable').live("click", function(){
      var $row = $(this).closest("tr");
      $('.js-check-item').removeAttr("checked");
      $row.find('.js-check-item').attr("checked", "checked");
      UpdateCheckCounter();
      $('#form_product_list input[name=disable]').click();
   });
   //������� �������
   $('.js-item-delete').live("click", function(){
      var $row = $(this).closest("tr");
      $('.js-check-item').removeAttr("checked");
      $row.find('.js-check-item').attr("checked", "checked");
      UpdateCheckCounter();
      $('#form_product_list input[name=delete_product]').click();
   });
   //���������� �������
   $('.js-item-copy').live("click", function(){
      $('.js-check-item').removeAttr("checked");
      UpdateCheckCounter();
      SortAndSend({copyId: $(this).closest(".js-sortable-item").attr("data-id")});
   });
   //sortable
   $( ".js-sortable" ).sortable({
      containment: ".js-sortable-container",
      cursor: "move",
      delay: 50,
      forcePlaceholderSize: true,
      handle: '.js-sortable-button',
      update: function( event, ui ){
         SortAndSend();
      }
   });
   $( ".js-sortable" ).disableSelection();
});

function SortAndSend(data){
   var postData = {};
   if(typeof data != 'undefined') postData = data;
   postData.company = $('.js-sortable').attr("data-company");
   postData.pagen = $('.js-sortable').attr("data-pagen");
   postData.sizen = $('.js-sortable').attr("data-sizen");
   postData.iblock = $('.js-sortable').attr("data-iblock");
   postData.elements = [];
   $('.js-sortable .js-sortable-item').each(function(index, domElement){
      postData.elements.push($(domElement).attr("data-id"));
   });
   $.ajax({
      type: "POST",
      url: '/_ajax/personal/sort_items.php',
      data: postData,
      dataType: "json",
      success: function(data){
         if((!data.error.length) && (!data.resort.error.length)){
            if(postData.copyId && data.list.length)
               $('.js-sortable-item[data-id='+postData.copyId+']').after(data.list);
         }else{
            console.log("AJAX answer error:", data);
         }
      },
      error: function(){
         console.log("AJAX error!");
      }
   });
}