<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="login_block bx-system-auth-form">
   <? if ($arResult["FORM_TYPE"] == "login"): ?>
      <div class="login_form" id="loginFormId">
         <?if ($arResult['SHOW_ERRORS'] == 'Y' && $_POST['AUTH_FORM'] == "Y" && $_POST['TYPE'] == "AUTH"):?>
            <div style="margin-bottom: 5px;">
               <?ShowMessage($arResult['ERROR_MESSAGE']);?>
            </div>
            <script>
               $(document).ready(function(){
                  $('#loginFormId').toggle();
               })
            </script>
         <?endif;?>
         <form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
            <? if ($arResult["BACKURL"] <> ''): ?>
               <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
            <? endif ?>
            <? foreach ($arResult["POST"] as $key => $value): ?>
               <input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
            <? endforeach ?>
            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />
            <input type="checkbox" checked id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" style="display: none;" />
            
            <input type="submit" name="Login" value="<?= GetMessage("AUTH_LOGIN_BUTTON") ?>" style="text-indent:-9999px; width:0px; height:0px; border:none; background:none; padding:0; margin:0;"/>
            <div class="lf_header">
               <a class="dotted login_lnk" href="javascript:void(0)" rel="nofollow" onClick="jQuery('#loginFormId').toggle();">���� � �����������</a>
               <div class="lf_header_bottom"></div>
            </div>
            <div class="lb_label">������� ��� e-mail</div>
            <div class="lb_input_box">
               <input type="text" name="USER_LOGIN" value="<?= $arResult["USER_LOGIN"] ?>">
            </div>
            <div class="spaser"></div>
            <div class="lb_label">������� ������</div>
            <div class="lb_input_box">
               <input type="password" name="USER_PASSWORD" value="<?= $arResult["USER_PASSWORD"] ?>">
            </div>
            <!-- <a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" class="forgot_pswrd">������ ������?</a> -->
            <a href="/auth/?forgot_password=yes&backurl=%2Fauth%2F" class="forgot_pswrd">������ ������?</a>
            <div class="spaser"></div>
            <? if ($arResult["NEW_USER_REGISTRATION"] == "Y"): ?>
               <!-- <a class="btns gray_btn fl_right" href="<?= $arResult["AUTH_REGISTER_URL"] ?>"><i></i>������������������</a> -->
               <a class="btns gray_btn fl_right" href="/auth/?register=yes"><i></i>������������������</a> 
            <?endif;?>
            <a class="btns fl_left" href="javascript:void(0);" onclick="document.system_auth_form<?= $arResult["RND"] ?>.submit();"><i></i>�����</a>
            <div class="spaser clear"></div>
            <? if(false):// ($arResult["CAPTCHA_CODE"]): ?>
               <? echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:<br />
               <input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>" />
               <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
               <input type="text" name="captcha_word" maxlength="50" value="" />
            <? endif ?>
         </form>
      </div>
      <a class="dotted login_lnk" href="javascript:void(0)" rel="nofollow" onClick="jQuery('#loginFormId').toggle();">���� � �����������</a>
   <?//if($arResult["FORM_TYPE"] == "login")
   else:?>
      <form name="system_auth_logout_form<?= $arResult["RND"] ?>" action="<?= $arResult["AUTH_URL"] ?>">
         <? foreach ($arResult["GET"] as $key => $value): ?>
            <input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
         <? endforeach ?>
         <input type="hidden" name="logout" value="yes" />
         <input type="submit" name="logout_butt" style="display: none;" value="<?= GetMessage("AUTH_LOGOUT_BUTTON") ?>" />
         <span><a href="/personal/profile/" class="red nick"><?= (strlen($arResult["USER_NAME"]) > 0 ? $arResult["USER_NAME"] : $arResult["USER_LOGIN"]) ?></a></span>&nbsp|&nbsp<a href="javascript:void(0);" onclick="document.system_auth_logout_form<?= $arResult["RND"] ?>.submit();" class="goodbye"><?= GetMessage("AUTH_LOGOUT_BUTTON") ?></a>
      </form>
   <? endif ?>
</div>