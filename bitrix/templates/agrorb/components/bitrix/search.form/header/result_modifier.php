<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arSecrions = array("tov", "com", "news", "art", "meropr");
$selectedIndex = array_search($_REQUEST['w'], $arSecrions);
if($selectedIndex !== false)
   $arResult["SELECTED_SECTION"] = $arSecrions[$selectedIndex];
else
   $arResult["SELECTED_SECTION"] = "all";