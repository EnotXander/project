<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>

	   <meta content="text/html" charset="windows-1251"/>
	   <meta name="viewport" content="width=1243">
       <title><? $APPLICATION->ShowTitle(); ?></title>
       <script type="text/javascript" src="https://www.google.com/jsapi"></script>
       <?$APPLICATION->ShowMeta("keywords")?>
       <?$APPLICATION->ShowMeta("description")?>
       <?= ShowCanonical(); ?>
	   <script   type='application/javascript' src='/bitrix/templates/.default/js/jquery-1.7.2.min.js'></script>

	   <?
        use \Bitrix\Main\Page\Asset;
	    $jsbase = '/bitrix/templates/.default';
	   	$scripts = array(

           // $jsbase.'/js/jquery-1.6.2.min.js',
		    $jsbase.'/js/underscore.js',

      		    $jsbase.'/js/jquery-ui-1.10.3.custom.js',
      		    $jsbase.'/js/jquery.autoSuggest.js',
      		    $jsbase.'/js/chosen.jquery.js',
                $jsbase.'/js/jquery.bxslider.min.js',
                $jsbase.'/js/paging.js',
      		    $jsbase.'/js/jquery.lightbox-0.5.pack.js',
      		    $jsbase.'/js/pic.core.js',
      		    $jsbase.'/js/pic.jquery.js',
      		    $jsbase.'/js/jcarousellite_1.0.1.js',
      		    $jsbase.'/js/desaturate.js',
      		    $jsbase.'/js/hls.js',
      		    '/js/banner.js',
      		    $jsbase.'/js/jquery.mousewheel-3.0.6.pack.js',
      		    $jsbase.'/js/fancybox/jquery.fancybox.pack.js?v=2.1.4',
      		    $jsbase.'/js/flexslider.js',
      		    $jsbase.'/js/jquery.lightbox-0.5.pack.js',
      		    $jsbase.'/js/jquery.collapsorz_1.1.min.js', $jsbase.'/js/main_collapsorz.js',
      		    $jsbase.'/js/jquery.scrollTo.js',
      		    $jsbase.'/js/jquery.tools.js',
      		    //$jsbase.'/js/jquery.form.js',
      		    $jsbase.'/js/jquery.maskedinput-1.3.min.js',

            $jsbase.'/js/inputmask.min.js',
      		    $jsbase.'/js/jquery.bind-first-0.2.3.min.js',
      		    $jsbase.'/js/phone/jquery.inputmask-multi.js',

      		    $jsbase.'/js/redactor2/redactor.js',
      		    $jsbase.'/js/redactor2/ru.js',
                $jsbase.'/js/main.js',
                $jsbase.'/js/protect_text.js',
	   		);

	   		 foreach ($scripts as $s)
                 Asset::getInstance()->addJs($s);

	   		$csss = array(
                '/css/chosen.css',
	   			$jsbase.'/common.css',
	   			$jsbase.'/js/redactor2/redactor.css',
	   			SITE_TEMPLATE_PATH .'/css/styles.css',
	   			'/theme/red/style.css',
			    $jsbase.'/js/fancybox/jquery.fancybox.css?v=2.1.4'
	   		);
	        if ( strpos($APPLICATION->GetCurUri(), '/market/') === 0 ){
		        $csss []= $jsbase.'/components/bitrix/catalog/market2/catalog_styles.css';
		        $csss []= $jsbase.'/components/bitrix/catalog/market2/additional-v2.css"';
	        }

	   		 foreach ($csss as $s)
	   			 $APPLICATION->SetAdditionalCSS($s);

	   		 $APPLICATION->ShowCSS();

	    	    //��� ������� �������
	         $APPLICATION->ShowHeadStrings();
	    	 $APPLICATION->ShowHeadScripts();
    ?>
      <!--[if lte IE 8]><link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/ie.css" type="text/css"><![endif]-->
      
      <script type="text/javascript">
         VK.init({apiId: 3199275, onlyWidgets: true});
      </script>


      <? //$APPLICATION->ShowHead(); ?>
      </head>
      <body id="top">
         <div id="fb-root"></div>
         <script>(function(d, s, id) {
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) return;
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
         fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));</script>


         <?$APPLICATION->ShowPanel();?>
         
         <div class="middle_banner" style="width:1242px; margin: 0 auto;">
            <?
            $APPLICATION->IncludeComponent("bitrix:advertising.banner", "top_b", array(
				"TYPE" => "A1",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => 3600*24
				),
				false
			);
            ?>
         </div>
         
         <div class="main">
            <div class="header">
               <a class="logo" href="<?= SITE_DIR ?>" title="����������������� ������ ������������">
                  <img src="<?= SITE_TEMPLATE_PATH ?>/images/logo.png" alt="������������ �������">
               </a>
               
               <?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "energo2", array(
                     "REGISTER_URL" => "/auth/",
                     "PROFILE_URL" => "/personal/profile/",
                     "SHOW_ERRORS" => "Y"
                  ), false
               );?>
               
               <div class="search_block">
                  <?$APPLICATION->IncludeComponent("bitrix:search.form", "header", array(
                        "USE_SUGGEST" => "N",
                        "PAGE" => "#SITE_DIR#search/index.php",
                        "AJAX_PAGE" => "#SITE_DIR#_ajax/search.php"
                     )
                  );?> 
               </div><!--search_block-->
               <div class="clear"></div>
               
               <div class="main_memu">
                  <? GLOBAL $USER; if($USER->isAdmin() || true ) { ?>
                      <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu_agro_yarmarka", array(
                            "ROOT_MENU_TYPE" => "top",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "left",
                            "USE_EXT" => "Y",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => "",
    	                    "FIRST" => 4,
                            "SECOND" => 8,
                            "CLASSES" => Array("sm_market", "sm_companies", "sm_advertising", "sm_yarmarka")
                         ), false
                      );?>
                      <?} else {?>
                  <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => "",
	                    "FIRST" => 3,
                        "SECOND" => 7,
                        //"CLASSES" => Array("sm_market", "sm_companies", "sm_advertising")
                     ), false
                  );?> 
                  <? } ?>
                  <div class="mm_bottom">
                     <div class="custom_menu"><a href="/about/">� �������</a><a href="/reclame/">�������</a><a href="/feedback/">�������� �����</a></div>
                     <div class="add_menu">
                        <noindex>
                           <?global $USER;
                           if($USER->IsAuthorized()):?>
                              <?//���� ��������, � ������� �������������� ������� �
                              if(!CModule::IncludeModule("iblock")) return;
                              $rsCompany = CIBlockElement::GetList(
                                 array(),
                                 array(
                                    "IBLOCK_ID" => 65,
                                    //"ACTIVE" => "Y",
                                    "=PROPERTY_USER" => $USER->GetID()
                                 )
                              );

                              if($arCompany = $rsCompany->GetNext()):?>
                                 <a href="/personal/company/">���������� ���������</a>,<a href="/personal/company/prod_and_serv/">��� ������</a>,<a href="/personal/company/prod_and_serv/serv.php">��� ������</a>
                              <?else:?>
                                 ����������:<a href="/personal/company/" rel="nofollow">��������</a>,<a href="/personal/company/prod_and_serv/" rel="nofollow">�����</a>,<a href="/personal/company/prod_and_serv/serv.php" rel="nofollow">������</a>
                              <?endif;?>
                           <?else:?>
                              ����������:<a href="/login/?&backurl=%2Fpersonal%2Fcompany%2F" rel="nofollow">��������</a>,<a href="/login/?&backurl=%2Fpersonal%2Fcompany%2Fprod_and_serv%2F" rel="nofollow">�����</a>,<a href="/login/?&backurl=%2Fpersonal%2Fcompany%2Fprod_and_serv%2Fserv.php" rel="nofollow">������</a>
                           <?endif;?>
                        </noindex>
                     </div>
                     <span class="date">�������: <?= date('d') ?>.<?= date('m') ?></span>
                     <div class="clear"></div>
                  </div><!--mm_bottom-->
               </div><!--main_memu-->
            </div><!--header-->
            
            <div class="content">
               <div class="content_box">
                   <? if ((strpos($APPLICATION->GetCurUri(), '/personal/') === 0) && ($_REQUEST["forgot_password"] != "yes")): ?>
                           <div class="sw_breadcrumbs">
                               <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "news", array(
                                   "START_FROM" => "0",
                                   "PATH" => "",
                                   "SITE_ID" => LANG
                               ),
                                   false,
                                   array()
                               );?>
                           </div>
                       <? endif; ?>
	               <?if (!HIDE_LEFT) {?>
                       <? if (($_SERVER["REQUEST_URI"] != "/work.php")  && ($_REQUEST["forgot_password"] != "yes")): ?>
                           <?
                           $APPLICATION->IncludeComponent("bitrix:main.include", "left", Array(
                               "AREA_FILE_SHOW" => "sect",
                               "AREA_FILE_SUFFIX" => "work",
                               "AREA_FILE_RECURSIVE" => "Y",
                               "EDIT_MODE" => "html",
                           ), false, Array('HIDE_ICONS' => 'Y')
                           );
                           ?>
                           <?if(!(defined('ERROR_404') && ERROR_404 == 'Y')):?>
                               <?
                               $APPLICATION->IncludeComponent("bitrix:main.include", "left", Array(
                                   "AREA_FILE_SHOW" => "sect",
                                   "AREA_FILE_SUFFIX" => "inc",
                                   "AREA_FILE_RECURSIVE" => "N",
                                   "EDIT_MODE" => "html",
                               ), false, Array('HIDE_ICONS' => 'Y')
                               );
                               ?>
                           <? endif; ?>
                       <? endif; ?>
                    <? } ?>