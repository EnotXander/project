<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
 
?>



<div class="bx-auth">
<?
ShowMessage($arParams["~AUTH_RESULT"]);


//print_r($arResult[USER_PROPERTIES]);
?>

<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK"):?>
<p><?echo GetMessage("AUTH_EMAIL_SENT")?></p>
<?else:?>

<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
	<p><?echo GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></p>
<?endif?>
<noindex>
<form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform">
<?
if (strlen($arResult["BACKURL"]) > 0)
{
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="REGISTRATION" />

<table class="data-table bx-registration-table">
	<thead>
		<tr>
			<td colspan="3" style="color:red;"><b>��������������� ������</b></td>
		</tr>
			<tr>
			<td colspan="3">����, ���������� <span style="color:red">*</span>, ������������ ��� ����������.<br><br></td>
		</tr>
	</thead>
	<tbody class="bodyground">
		<tr>
			<td><span class="starrequired" style="color:red">*</span>��� ����� ����������� �����:</td>
			<td><input type="text" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" class="bx-auth-input" /></td>
			<td rowspan="2"><div><p>��� ����� ����� �������������� <b>��� ����� �� ����</b></p><br>
			<p>�� ������� �������� �� ���������� ����� ������ ��������� ����������:<br>
			 - ��������� �� ��������� ��������� ���� �������.<br>
			- ����������� � ������������ ��� ����� ������� � �������.<br>
			- �������������� �������� ������� <?= LANG=='s1'? 'EnergoBelarus.by' : 'AgroBelarus.by' ?>.
			</p>
			</div></td>
		</tr>
		<tr>
			<td><span class="starrequired" style="color:red;">*</span><?=GetMessage("AUTH_LOGIN_MIN")?></td>
			<td><input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" class="bx-auth-input" /></td>
			
		</tr>
		<tr>
			<td><span class="starrequired" style="color:red">*</span><?=GetMessage("AUTH_PASSWORD_REQ")?></td>
			<td><input type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" class="bx-auth-input" />
<?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
<script type="text/javascript">
document.getElementById('bx_auth_secure').style.display = 'inline-block';
</script>
<?endif?>
			</td>
			<td rowspan="2"><div>
			<p>
			<b>�����:</b> ��� ����� ������ ���� ������� ��������� ��������� � �������� �����. ����������� ����� ������ - 6 ��������. 
			</p>
			</div></td>
		</tr>

		<tr class="auth-last">
			<td><span class="starrequired" style="color:red">*</span>��������� ������:</td>
			<td><input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="bx-auth-input" /></td>
			
		</tr>
<tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
		<tr>
			<td><span class="starrequired" style="color:red;">*</span><?=GetMessage("AUTH_NAME")?></td>
			<td><input type="text" name="USER_NAME" maxlength="50" value="<?=$arResult["USER_NAME"]?>" class="bx-auth-input" /></td>
			<td rowspan="2"><div>
			<p>
			<b>������ ������</b><br>��� ������ �������� ��� ������������� ��������� �������� ���������� ��� ��� �������������� ����������. 
			��� � ������� ������������ ����� ��� �������� ����� � � ��������� ������ �������. 
			</p>
			</div></td>
		</tr>
		<tr class="auth-last">
			<td><span class="starrequired" style="color:red;">*</span><?=GetMessage("AUTH_LAST_NAME")?></td>
			<td><input type="text" name="USER_LAST_NAME" maxlength="50" value="<?=$arResult["USER_LAST_NAME"]?>" class="bx-auth-input" /></td>
			
		</tr>
		
<tr style="background-color:#ffffff;"><td colspan="3"></td></tr>		
		
<?// ********************* User properties ***************************************************?>
<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
	<tr><td colspan="2"><?=strLen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></td></tr>
	<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
	<tr><td><?if ($arUserField["MANDATORY"]=="Y"):?><span class="required">*</span><?endif;?>
		<?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td>
			<?$APPLICATION->IncludeComponent(
				"bitrix:system.field.edit",
				$arUserField["USER_TYPE"]["USER_TYPE_ID"],
				array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "bform"), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
	<?endforeach;?>
<?endif;?>
<?// ******************** /User properties ***************************************************

	/* CAPTCHA */
	if ($arResult["USE_CAPTCHA"] == "Y")
	{
		?>
		
		<tr>
			<td></td>
			<td>
				<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
				<div class="captcha"> <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="125" height="52" alt="CAPTCHA" /></div>
			</td>
			<td rowspan="2"><b><?=GetMessage("CAPTCHA_REGF_TITLE")?></b><p>
			���� �� �� ������ ��������� �������, ������� �������� ��� ��������� ����� "������!", ������� <a  class="captchab">�����</a> � �������� ���������.	
			</p></td>
		</tr>
		<tr class="auth-last">
			<td><span class="starrequired"  style="color:red;">*</span>��� � �����������:</td>
			<td><input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
		<?
	}
	/* CAPTCHA */
	?>
	</tbody>
	<tfoot>
		<tr>
			<td></td><td></td>
			<td><input type="submit" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>" style="float:right;"/></td>
		</tr>
	</tfoot>
</table>
<!--<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
<p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>-->

<p>
<a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_AUTH")?></b></a>
</p>

</form>
</noindex>
<script type="text/javascript">
document.bform.USER_NAME.focus();

/*RELOAD CAPTCHA*/
var symbols ="abcdefghjklmnopqrstuvwxyz0123456789"
var length = 32;
function generatePassword(symbols, length) {
var result = "";
for (var i=0; i<length; i++) {
result += symbols.charAt(Math.floor(Math.random()*symbols.length));
};
return result;
}

$(".captchab").click(function(){
var captcha_code = generatePassword(symbols, length);
var c = "/bitrix/tools/captcha.php?captcha_sid=" + captcha_code ;
$(".captcha img").attr("src", c)
$(".captcha input").attr("value", captcha_code)
})
</script>

<?endif?>
</div>