<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?
if ((int)$_REQUEST['month'] > 0)
   $curMonth = (int)$_REQUEST['month'];
else
   $curMonth = date('m');
if ((int)$_REQUEST['year'] > 0)
   $curYear = (int)$_REQUEST['year'];
else
   $curYear = date('Y');
?>

<div class="middle_col">
   <div class="subject">
      <p class="subject">������������� ��������� �� �������</p>
   </div>
   <!-- ---------------------- -->
   <div class="info">
      <table width="100%">
         <tr>
            <td width="540px">
               <p class="info">��� ������� ������� � ����������� �� ������ ������ ��������������� ������; 
                  �� ������ ������������ <a class="sale" href="#">���������� � �������</a>. ����������� � ������� ���� ������� � ����������� ������ ��� ����� ���. 
                  ����� ��������� ���������� � ������� �� energobelarus.by  �� ������ �������� � ����� ���������� �� �������� 8 (017) 239-57-01. </p>
            </td>
            <td>
               <table width="100%">
                  <tr>
                     <td align="center"><p class="grey">������������ �� ������� ��������� � 8:00 �� 19:00</p></td>
                  </tr>
                  <tr>
                     <td align="center"><p class="data">(017) 546-45-65, <a class="email" href="#">ad@energobelrus.by</a></p></td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </div>
   <!-- ---------------------- -->
   <div class="sections">
      <div class="tab">
         <p class="tab">�������� ������ ����� ��� �������</p>
      </div>
      <div class="select_section">
         <table width="100%" height="100%">
            <tr>
               <td width="25%">
                  <a class="highlight_section" href="#">������� �������� Energobelarus.by</a>
               </td>
               <td width="25%">
                  <a class="section" href="#">������ �������������</a>
               </td>
               <td width="25%">
                  <a class="section" href="#">������</a>
               </td>
               <td width="25%" rowspan="2">
                  <a href="#">
                     <table width="100%">
                        <tr>
                           <td rowspan="2" width="32"><img src="<?= $templateFolder ?>/images/pdf.png"></td>
                           <td><p class="price">������� �����������</p></td>
                        </tr>
                        <tr>
                           <td style="vertical-align: top"><p class="grey">PDF, 1.2 ��</p></td>
                        </tr>
                     </table>
                  </a>
               </td>
            </tr>
            <tr>
               <td>
                  <a class="section" href="#">���������� �����������</a>
               </td>
               <td>
                  <a class="section" href="#">��������� ����</a>
               </td>
               <td>
                  <a class="section" href="#">����������</a>
               </td>
            </tr>
         </table>
      </div>
   </div>
   <!-- ---------------------- -->
   <div class="subject2">
      <p class="subject2">��������� ��� ������� �������� �������� Energobelarus.by�</p>
   </div>
   <!-- ---------------------- -->
   <div class="legend">
      <div style="width: 100%; height: 100%; padding: 0px 20px;">
         <table height="100%">
            <tr>
               <td style="vertical-align: middle; padding-right: 10px">
                  <table>
                     <tr>
                        <td class="va"><img style="padding-right: 3px" src="<?= $templateFolder ?>/images/free.png"></td>
                        <td><p class="legend">��������</p></td>
                     </tr>
                  </table>
               </td>
               <td style="vertical-align: middle; padding-right: 10px">
                  <table>
                     <tr>
                        <td class="va"><img style="padding-right: 3px" src="<?= $templateFolder ?>/images/busy.png"></td>
                        <td><p class="legend">������</p></td>
                     </tr>
                  </table>
               </td>
               <td style="vertical-align: middle; padding-right: 10px">
                  <table>
                     <tr>
                        <td class="va"><img style="padding-right: 3px" src="<?= $templateFolder ?>/images/reserv.png"></td>
                        <td><p class="legend">�����</p></td>
                     </tr>
                  </table>
               </td>
               <td style="vertical-align: middle; padding-right: 10px">
                  <table>
                     <tr>
                        <td class="va"><img style="padding-right: 3px" src="<?= $templateFolder ?>/images/your_reserv.png"></td>
                        <td><p class="legend">���� �����</p></td>
                     </tr>
                  </table>
               </td>
               <td style="vertical-align: middle; padding-right: 10px">
                  <table>
                     <tr>
                        <td class="va"><img style="padding-right: 3px" src="<?= $templateFolder ?>/images/your_ad.png"></td>
                        <td><p class="legend">���� �������</p></td>
                     </tr>
                  </table>
               </td>
               <td style="vertical-align: middle; padding-right: 10px">
                  <table>
                     <tr>
                        <td class="va"><img style="padding-right: 3px" src="<?= $templateFolder ?>/images/sales.png"></td>
                        <td><p class="legend">������</p></td>
                     </tr>
                  </table>
               </td>
            </tr>
         </table>
      </div>
   </div>
   <!-- ---------------------- -->
   <div class="ad_space">
      <table class="ad_space" cellspacing="0px">
         <tr class="month">
            <td></td>
            <td></td>
            <?
            $day = 1;
            $month = $curMonth;
            $year = $curYear;
            $now = mktime(0, 0, 0, $month, $day, $year);
            $dayLenght = mktime(0, 0, 0, 10, 10, 2004) - mktime(0, 0, 0, 10, 9, 2004);
            $next = $now + (31 * $dayLenght);
            ?>
            <td colspan="31" style="border-left: 1px solid #5e7c0f"><p class="month"><?= GetMessage("IBL_NEWS_CAL_M_" . date('n', $now)) ?> <?= date('Y', $now) ?></p></td>
            <td colspan="4" style="text-align: right"><a href="<?= $APPLICATION->GetCurPageParam("month=" . date('n', $next + 2 * $dayLenght) . "&year=" . date('Y', $next + 2 * $dayLenght), Array('month', 'year')) ?>" class="next_month">><?= GetMessage("IBL_NEWS_CAL_M_" . date('n', $next + 2 * $dayLenght)) ?> <?= date('Y', $next + 2 * $dayLenght) ?> &#8594;</a></td>
         </tr>
         <?
         $day = 1;
         $month = $curMonth;
         $year = $curYear;
         $now = mktime(0, 0, 0, $month, $day, $year);
         $dayLenght = mktime(0, 0, 0, 10, 10, 2004) - mktime(0, 0, 0, 10, 9, 2004);
         $next = $now + (31 * $dayLenght);
         ?>

         <tr class="title">
            <th class="place"><p class="title">������������ ���������� �����</p></th>
         <th class="cost">
            <p class="title">��������� ��� ���</p>
         </th>
         <? while ($now < $next): ?>
            <th class="when"<? if ((date("w", $now) == '0') or (date("w", $now) == '6')): ?> style="border-top: 2px solid #de241c; background: #cde3ef"<? endif; ?>><p class="day"><?= GetMessage("IBL_NEWS_CAL_S_" . date('w', $now)) ?></p><p class="date"><?= date('d', $now) ?></p></th>
            <? $now = $now + $dayLenght; ?>
         <? endwhile; ?>
         <th style="background: none">
            <a href="#"><img src="<?= $templateFolder ?>/images/arrow_right.png"></a>
         </th>
         <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <tr class="one_ad">
               <td class="place">
                  <p class="desc"><?= $arItem["NAME"] ?>
                     <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                        ,<br/>
                        <b class="grey"><? echo $arItem["PREVIEW_TEXT"]; ?></b>
                     <? endif; ?>
                  </p>
               </td>
               <td class="cost">
                  <p class="desc"><?= $arItem["PROPERTIES"]['cost']['VALUE'] ?></p>
               </td>
               <?
               $arDates = Array();
               foreach ($arItem['PROPERTIES']['date']['VALUE'] as $key => $val)
                  $arDates[$val] = explode('/', $arItem['PROPERTIES']['date']['DESCRIPTION'][$key]);?>

               <?
               $day = 1;
               $month = $curMonth;
               $year = $curYear;
               $now = mktime(0, 0, 0, $month, $day, $year);
               $dayLenght = mktime(0, 0, 0, 10, 10, 2004) - mktime(0, 0, 0, 10, 9, 2004);
               $next = $now + (31 * $dayLenght);
               ?>
               <? while ($now < $next): ?>
                  <?
                  if ($arDates[date('d.m.Y', $now)][0] == '�����')
                     $str = '<img src="' . $templateFolder . '/images/busy_day.png">';
                  elseif ($arDates[date('d.m.Y', $now)][0] == '������')
                     $str = '<img src="' . $templateFolder . '/images/your_reserv_day.png">';
                  else
                     $str = '<a class="add" href="#" onclick="addInPuts(\'' . $arItem["NAME"] . '\', \'' . $arItem["PREVIEW_TEXT"] . '\', \'' . $arItem["PROPERTIES"]["cost"]["VALUE"] . '\', \'' . date('d.m.Y', $now) . '\')"></a>'
                     ?>
                  <td class="when" <? if ((date("w", $now) == '0') or (date("w", $now) == '6')): ?> style="background: #f6f6f6"<? endif; ?>><? echo $str ?></a></td>
                  <? $now = $now + $dayLenght; ?>
               <? endwhile; ?>
               <td class="when"></td>
            </tr>
         <? endforeach; ?>
      </table>
   </div>
</div>