<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<p><a href="<?=$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"]?>"><?=GetMessage("T_NEWS_DETAIL_BACK")?></a></p>

<div class="middle_col">
   <?
   if ($_REQUEST['sort'] == "date")
      $sort = "date";
   else
      $sort = "rank";

   $cnt = ((int) $_REQUEST["per_page"] > 0 ? $_REQUEST["per_page"] : 10);

   $APPLICATION->IncludeComponent("bitrix:search.page", "temp", array(
       "CHECK_DATES" => $arParams["CHECK_DATES"] !== "N" ? "Y" : "N",
       "arrWHERE" => Array("iblock_" . $arParams["IBLOCK_TYPE"]),
       "arrFILTER" => Array("iblock_" . $arParams["IBLOCK_TYPE"]),
       "SHOW_WHERE" => "N",
       //"PAGE_RESULT_COUNT" => "",
       "CACHE_TYPE" => $arParams["CACHE_TYPE"],
       "CACHE_TIME" => $arParams["CACHE_TIME"],
       "SET_TITLE" => $arParams["SET_TITLE"],
       "arrFILTER_iblock_" . $arParams["IBLOCK_TYPE"] => Array($arParams["IBLOCK_ID"]),
       "DEFAULT_SORT" => $sort,
       "THUMB_WIDTH" => 100,
       "THUMB_HEIGHT" => 100,
       "BODY_LENGTH" => 250,
       "IMAGE_MARGIN" => true,
       "DISPLAY_TOP_PAGER" => "Y",
       "DISPLAY_BOTTOM_PAGER" => "Y",
       "PAGER_TITLE" => "–езультаты поиска",
       "PAGER_SHOW_ALWAYS" => "N",
       "PAGER_TEMPLATE" => "search",
       "PAGE_RESULT_COUNT" => 10,
           ), $component
   );
   ?>

   <p>
      <a href="<?= $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"] ?>"><?= GetMessage("T_NEWS_DETAIL_BACK") ?></a>
   </p>
</div>
