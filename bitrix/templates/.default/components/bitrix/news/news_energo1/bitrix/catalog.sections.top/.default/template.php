<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
} ?>


<? $bC = 2; ?>
<? foreach ( $arResult["SECTIONS"] as $blockKey => $block ): ?><? /* if($blockKey):?>
      <div class="content_box">
         <div class="middle_banner">
            <?
            $APPLICATION->IncludeComponent("bitrix:advertising.banner", "news_main", Array(
                "TYPE" => "A" . $bC, // ��� �������
                "NOINDEX" => "N", // ��������� � ������ noindex/nofollow
                "CACHE_TYPE" => "A", // ��� �����������
                "CACHE_TIME" => "3600", // ����� ����������� (���.)
                    ), false
            );
            ?>
         </div>
      </div>
      <div class="clear"></div>
   <?endif; */ ?>
	<div class="content_box" style="width: 100%">
		<div class="middle_col">
			<? foreach ( $block as $stringKey => $string ): ?><? if ( $stringKey == 1 ): ?>					<!-- ad F3 -->
				<div class="mid_ad">
					<? $APPLICATION->IncludeComponent(
						"bitrix:advertising.banner",
						".default",
						array(
							"TYPE"        => "F3",
							"NOINDEX"     => "N",
							"CACHE_TYPE"  => "A",
							"CACHE_TIME"  => "0",
							"CACHE_NOTES" => ""
						),
						false
					); ?>
				</div>				<? endif; ?><? foreach ( $string as $sectionKey => $arSection ): ?>
				<div class="mc_block double_news_list">
					<ul class="mb_menu">
						<li class="mb_title">
							<a href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?></a>

							<div class="sclew"></div>
						</li>
						<li class="mb_clear"><a href="<?= $arSection["SECTION_PAGE_URL"] ?>">���
								������� �������</a></li>
					</ul>
					<div class="brdr_box_hmenu news_list">
						<? $cell = 0;
						foreach ( $arSection["ITEMS"] as $arElement ):?>
							<div class="item_row">
								<? if ( is_array( $arElement["PREVIEW_IMG"] ) ): ?>
									<div class="image_box">
										<a href="<?= $arElement["DETAIL_PAGE_URL"] ?>"><img border="0" src="<?= $arElement["PREVIEW_IMG"]["SRC"] ?>" width="<?= $arElement["PREVIEW_IMG"]["WIDTH"] ?>" height="<?= $arElement["PREVIEW_IMG"]["HEIGHT"] ?>" alt="<?= $arElement["NAME"] ?>" title="<?= $arElement["NAME"] ?>"/></a>
									</div><? endif ?>
								<div class="row">
									<div class="title">
										<a href="<?= $arElement["DETAIL_PAGE_URL"] ?>"><?= $arElement["NAME"] ?></a>
									</div>
									<p><?= $arElement["PREVIEW_TEXT"] ?>
										<span class="date">\ <?= $arElement['ACTIVE_FROM'] ?></span>
									</p>
								</div>
							</div><!--item_row--><? endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
					</div>
				</div><? endforeach; ?>
				<div class="clear"></div><? endforeach; ?>
		</div>
	</div><? endforeach; ?>
<div class="clear"></div>