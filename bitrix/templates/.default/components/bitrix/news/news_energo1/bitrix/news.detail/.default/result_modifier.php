<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult["TAGS_LIST"] = array();
if (strlen($arResult["TAGS"]) > 0){
	$arResult["TAGS_LIST"] = explode(',', $arResult["TAGS"]);
	foreach ($arResult["TAGS_LIST"] as $key => $val){
		$arResult["TAGS_LIST"][$key] = trim($val);
	}
}
if ($arResult["~DETAIL_PICTURE"] > 0) {
		$arFileTmp = CFile::ResizeImageGet(
			$arResult["~DETAIL_PICTURE"],
		    array("width" => 400, "height" => 800),
		    BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		    true
		 );
	//PrintAdmin($arFileTmp);
		 $arResult["DETAIL_PICTURE"] = array(
		     "SRC" => $arFileTmp["src"],
		     "WIDTH" => $arFileTmp["width"],
		     "HEIGHT" => $arFileTmp["height"],
		 );
}


// простаивить вотермарки
if ($arResult['PROPERTIES']['WATERMARK']['VALUE'] == 'Y'){

 $arResult["DETAIL_TEXT"] = SetWatermarkImg($arResult["DETAIL_TEXT"]);

}