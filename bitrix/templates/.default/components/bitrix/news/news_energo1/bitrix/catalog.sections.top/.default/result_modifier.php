<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$obParser = new CTextParser;

$sectionsInString = 2;
$stringsInBlock = 3;

foreach ($arResult["SECTIONS"] as $key => $arSection)
{
   foreach ($arSection["ITEMS"] as $keyI => $arElement)
   {
      $arElement["PREVIEW_TEXT"] = strip_tags($arElement["DETAIL_TEXT"]);
      
      if(is_array($arElement["PREVIEW_PICTURE"])) $arElement["DETAIL_PICTURE"] = $arElement["PREVIEW_PICTURE"];
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
            $arElement["DETAIL_PICTURE"],
            array("width" => 100, "height" => 100),
            BX_RESIZE_IMAGE_EXACT,
            true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
         $arResult["SECTIONS"][$key]["ITEMS"][$keyI]["PREVIEW_IMG"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
         if (strlen($arElement["ACTIVE_FROM"]) > 0)
            $arResult["SECTIONS"][$key]["ITEMS"][$keyI]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arElement["ACTIVE_FROM"], CSite::GetDateFormat()));
         else
            $arResult["SECTIONS"][$key]["ITEMS"][$keyI]["DISPLAY_ACTIVE_FROM"] = "";
         if ($arParams["PREVIEW_TRUNCATE_LEN"] > 0)
            $arResult["SECTIONS"][$key]["ITEMS"][$keyI]["PREVIEW_TEXT"] = $obParser->html_cut($arElement["PREVIEW_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
      }
   }
}

//����� ������� �� �����, � ����� ��������
$arResult["SECTIONS"] = array_chunk($arResult["SECTIONS"], $sectionsInString);
$arResult["SECTIONS"] = array_chunk($arResult["SECTIONS"], $stringsInBlock);
?>