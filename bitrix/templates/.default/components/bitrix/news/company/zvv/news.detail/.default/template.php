<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
} ?>
<?


?>
<div  itemscope itemtype="http://schema.org/Organization" >
	<? if ( isset( $arResult["DISPLAY_PROPERTIES"]["shedule"]["DISPLAY_VALUE"] ) ): ?>
         <meta itemprop="openingHours" content="<? echo $arResult["DISPLAY_PROPERTIES"]["shedule"]["DISPLAY_VALUE"]; ?>"/>
      <? endif; ?>

<div class="middle_col" style="width: 100% !important; margin: 10px 20px;" >

	<table>
		<tr>
			<? if ( $arParams["DISPLAY_PICTURE"] != "N" && is_array( $arResult["PREVIEW_PICTURE"] ) ): ?>
				<td itemprop="logo" content="http://<?= $_SERVER['HTTP_HOST'].$arResult["PREVIEW_PICTURE"]["SRC"] ?>">
					<img class="detail_picture" border="0" src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arResult["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arResult["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arResult["NAME"] ?>" title="<?= $arResult["NAME"] ?>" style="float:left;"/>
				</td>
			<? endif; ?>

			<td style="vertical-align:middle">
				<h1 itemprop="name"><?= $arResult["NAME"] ?></h1>
			</td>

			<td style="vertical-align:middle; padding-left:15px;">
				<? $APPLICATION->IncludeComponent( "zvv:iblock.vote", "ajax", Array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID"   => $arParams["IBLOCK_ID"],
					"ELEMENT_ID"  => $arResult['ID'],
					"MAX_VOTE"    => "5",
					"VOTE_NAMES"  => Array( "1", "2", "3", "4", "5" ),
					"CACHE_TYPE"  => $arParams["CACHE_TYPE"],
					"CACHE_TIME"  => $arParams["CACHE_TIME"],
				), $component
				); ?>
			</td>
		</tr>
	</table>

	<div class="clear"></div>
</div>

<div class="right_col">
	<div class="rc_block brdr_box">
		<? if ( $arResult["PROPERTIES"]['LIQUIDATED']['VALUE'] == '��') {?>

			<div class="wrap" style="">
				<div class="LIQUIDATED_red"></div>
			</div>

			<?} else {?>
            <div class="wrap ">
			<h3 style="">���������� ����������</h3>

				<meta itempro="email" content="<?= $arResult['PROPERTIES']['EMAIL_REQUEST']['VALUE'] ?>"/>
			<? $arAddres = Array( "index", "COUNTRY", "REGION", "CITY", "adress" );

			$strAdress = '';
				$last = false;
				foreach ( $arAddres as $pidKey => $pid ):
					if ( isset( $arResult["DISPLAY_PROPERTIES"][ $pid ]["DISPLAY_VALUE"] ) ) :
						$strAdress .=  $last ? ", " : "" ;
						$strAdress .= trim( strip_tags( $arResult["DISPLAY_PROPERTIES"][ $pid ]["DISPLAY_VALUE"] ) );
				    endif;
				    $last = true;
				endforeach;

			?>


			<table width="100%" cellpadding="0" class="add_table">
				<tr>
					<td>
						<img src="<?= SITE_TEMPLATE_PATH ?>/img/addres.jpg">
					</td>
					<td  itemprop="address" >
						<?= $strAdress ?>
					</td>
				</tr>
				<? if ( isset( $arResult["DISPLAY_PROPERTIES"]["adress_store"]["DISPLAY_VALUE"] ) ): ?>
					<tr>
						<td>
							<img src="<?= SITE_TEMPLATE_PATH ?>/img/addres.jpg">
						</td>
						<td>
							<?= strip_tags( $arResult["DISPLAY_PROPERTIES"]["adress_store"]["DISPLAY_VALUE"] ) ?>
						</td>
					</tr>
				<? endif; ?>
				<? if ( isset( $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] ) ): ?>
					<tr>
						<td>
							<img src="<?= SITE_TEMPLATE_PATH ?>/img/tel.jpg">
						</td>
						<td>
							<noindex>
								<div class="js-phone-hide-block" data-company="<?= $arResult["ID"] ?>">
									<? foreach ( $arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"] as $phoneKey => $phone ): ?>
										<div>
											<span class="lgrey phone-code">+375 </span>
											<noindex>
											<span class="phone-hide js-phone-hide">�������� �������</span>
											</noindex>
											<span class="phone-show"  itemprop="telephone"><?= phone($phone) . " " . $arResult["DISPLAY_PROPERTIES"]["phone"]["DESCRIPTION"][ $phoneKey ] ?></span>
										</div>
									<? endforeach; ?>
								</div>
							</noindex>
						</td>
					</tr>
				<? endif; ?>
				<? if ( isset( $arResult["DISPLAY_PROPERTIES"]["shedule"]["DISPLAY_VALUE"] ) ): ?>
					<tr>
						<td>
							<img src="<?= SITE_TEMPLATE_PATH ?>/img/time.jpg">
						</td>
						<td>
							<? echo $arResult["DISPLAY_PROPERTIES"]["shedule"]["DISPLAY_VALUE"]; ?>
						</td>
					</tr>
				<? endif; ?>
			</table>
			<br/>

			<? if ( count( $arResult["URL"] ) > 0 ): ?>
				<noindex>
					<div>
						<div class="fl_left" style="margin-right: 10px;">web-����(�):</div>
						<div class="fl_left">
							<? foreach ( $arResult["URL"] as $key => $site ): ?>
								<a rel="nofollow" target="_blank" href="<?= $site["LINK"] ?>"><?= $site["NAME"] ?></a><?= ( isset( $arResult["PROPERTIES"]["URL"]["VALUE"][ $key + 1 ] ) ? "<br>" : "" ) ?>
							<? endforeach; ?>
						</div>
					</div>
					<div class="clear"></div>
					<br/> <br/>
				</noindex>
			<? endif; ?>

			<? if ( is_array( $arResult["MANAGER"] ) ): ?>
				<noindex>
				<? if ( $USER->IsAuthorized() ): ?>
					<a style="width: 140px; float:left;" href="/personal/messages/<?= $arResult["MANAGER"]["ID"] ?>/" class="btns gray_btn fl_right" target="_blank"><i></i><img style="margin-bottom: -3px;" src="/bitrix/templates/energorb/images/mail.jpg">&nbsp;��������
						���������</a><br/>
				<? else: ?>
					<a style="width: 140px; float:left;" rel="nofollow" href="#" class="btns gray_btn fl_right" onclick="alert('���������� ��������������');
                  return false;"><i></i><img style="margin-bottom: -3px;" src="/bitrix/templates/energorb/images/mail.jpg">&nbsp;��������
						���������</a><br/>
				<? endif; ?>
				<div class="clear"></div>
				<br/>
				<br/></noindex>
			<? endif; ?>
	            <div itemprop="location" itemscope itemtype="http://schema.org/Place">
	            <span itemprop="address" content="<?= $strAdress ?>">
	            <span itemprop="name" content="<?= $arResult['NAME']; ?>">


	            <? foreach ( $arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"] as $phoneKey => $phone ): ?>
					<meta itemprop="telephone" content="<?= phone($phone) . " " . $arResult["DISPLAY_PROPERTIES"]["phone"]["DESCRIPTION"][ $phoneKey ] ?>"/>
                <? endforeach; ?>

			<? $coord = explode( ",", $arResult['PROPERTIES']['MAP']['VALUE'] );
			if ( count( $coord ) > 1 ):?>
				<? // ��������� ������ ��� �����
				$arData = Array( "yandex_lat"   => $coord[0],
				                 "yandex_lon"   => $coord[1],
				                 "yandex_scale" => 16,
				                 "PLACEMARKS"   => Array( Array( "LON"  => $coord[1],
				                                                 "LAT"  => $coord[0],
				                                                 "TEXT" => $arResult['NAME']
				                 )
				                 )
				);

				$APPLICATION->IncludeComponent( "bitrix:map.yandex.view", ".default", array(
					"KEY"           => "AETqvU8BAAAApI_7dQMAlTDGwZ3hOJGhdVSFYEsVNSpuzTQAAAAAAAAAAACCDIEExtPcFpiKYkYYJlaNR8uKKw==",
					"INIT_MAP_TYPE" => "MAP",
					"MAP_DATA"      => serialize( $arData ),
					"MAP_WIDTH"     => 270,
					"MAP_HEIGHT"    => 200,
					"CONTROLS"      => array(),
					"OPTIONS"       => array( "ENABLE_SCROLL_ZOOM", "ENABLE_DRAGGING", ),
					"MAP_ID"        => ""
				), false
				); ?>

				  <div  itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
				    <meta itemprop="latitude" content="<?= $coord[0]?>" />
				    <meta itemprop="longitude" content="<?=  $coord[1] ?>" />
				  </div>

	            </span>
			<? else:
				$adress='';
			?>
				<? foreach ( $arAddres as $pidKey => $pid ):
					if ( isset( $arResult["DISPLAY_PROPERTIES"][ $pid ]["DISPLAY_VALUE"] ) ) :
						$adress .=  trim( strip_tags( $arResult["DISPLAY_PROPERTIES"][ $pid ]["DISPLAY_VALUE"] ) ) . ', ';
				    endif;
				   //$last = trim( strip_tags( $arResult["DISPLAY_PROPERTIES"][ $pid ]["DISPLAY_VALUE"] ) );
				endforeach; ?>
				<?
				//echo $adress;
				?>
				<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
				<div id="map" style="width: 270px; height: 200px"></div>
				<script type='text/javascript'>
				        ymaps.ready(init);
				        function init(){
				            var geocoder = new ymaps.geocode(
				                // ������ � �������, ������� ����� �������������
				                '<?= $adress ?>',
				                // ��������� ���������� �����������
				                { results: 1 }
				            );
				            // ����� ����, ��� ����� ������ ���������, ���������� callback-�������
				            geocoder.then(
				                    function (res) {
				                        // ���������� �������
				                        var coord = res.geoObjects.get(0).geometry.getCoordinates();
				                        var map = new ymaps.Map('map', {
				                            // ����� ����� - ���������� ������� ��������
				                            center: coord,
				                            // ����������� ���������������
				                            zoom: 7,
				                            // �������� ��������������� ����� �������
				                            behaviors: ['default', 'scrollZoom'],
				                            controls: ['mapTools']
				                        });
				                        // ���������� ����� �� �����
				                        map.geoObjects.add(res.geoObjects.get(0));
				                        // ������������� ����������� ��������� ����������� ��������������� - 1
				                        map.zoomRange.get(coord).then(function(range){
				                            map.setCenter(coord, range[1] - 2)
				                        });
				                        // ���������� ������������ ������ ������
				                        map.controls.add('mapTools')
				                            // ���������� ������ ��������� ��������
				                                .add('zoomControl')
				                            // ���������� ������ ����� �����
				                                .add('typeSelector');
				                    }
				            );
				        }
				    </script>
			<? endif ?>
	            </div>
		</div>
		<?} ?>
	</div>
	<!--rc_block-->

	<? if ( count( $arResult['F_PRICES'] ) > 0 || count( $arResult['F_CATALOG'] ) > 0 || count( $arResult['F_CERTIFICATE'] ) > 0 ): ?>
		<h3 class="title" style="color:red;">����� �������� <?= $arResult['NAME'] ?></h3>
		<div class="rc_block toog withoutTitleF b-files">
			<ul class="mb_menu">
				<? // ����
				$sel = true;
				if ( count( $arResult['F_PRICES'] ) > 0 ):?>
					<li class="<?= $sel ? "sel" : "" ?>">
						<a href="javascript: void(0)">�����-�����</a>

						<div class="sclew"></div>
					</li>
					<? $sel = false;?>
				<? endif; ?>
				<? if ( count( $arResult['F_CATALOG'] ) > 0 ): ?>
					<li class="<?= $sel ? "sel" : "" ?>">
						<a href="javascript: void(0)">��������</a>

						<div class="sclew"></div>
					</li>
					<? $sel = false; ?>
				<? endif; ?>
				<? if ( count( $arResult['F_CERTIFICATE'] ) ): ?>
					<li class="<?= $sel ? "sel" : "" ?>">
						<a href="javascript: void(0)">�����������</a>

						<div class="sclew"></div>
					</li>
					<? $sel = false; ?>
				<? endif; ?>
			</ul>

			<? if ( count( $arResult['F_PRICES'] ) > 0 ): ?>
				<div class="brdr_box catalog_list tab">
					<div class="wrap">
						<? foreach ( $arResult['F_PRICES'] as $file ): ?>
							<div class="files-download">
								<a href="<?= $file["SRC"] ?>" target="_blank"><i class="icons <?= $file["CLASS"] ?>"></i><span><?= strlen( $file["DESCRIPTION"] ) > 0 ? $file["DESCRIPTION"] : "������� ����" ?></span><span class="size"> / <?= $file["FORMAT_SIZE"] ?></span></a>
							</div>
						<? endforeach; ?>
					</div>
				</div>
			<? endif; ?>
			<? if ( count( $arResult['F_CATALOG'] ) > 0 ): ?>
				<div class="brdr_box catalog_list tab">
					<div class="wrap">
						<? foreach ( $arResult['F_CATALOG'] as $file ): ?>
							<div class="files-download">
								<a href="<?= $file["SRC"] ?>" target="_blank"><i class="icons <?= $file["CLASS"] ?>"></i><span><?= strlen( $file["DESCRIPTION"] ) > 0 ? $file["DESCRIPTION"] : "������� ����" ?></span><span class="size"> / <?= $file["FORMAT_SIZE"] ?></span></a>
							</div>
						<? endforeach; ?>
					</div>
				</div>
			<? endif; ?>
			<? if ( count( $arResult['F_CERTIFICATE'] ) > 0 ): ?>
				<div class="brdr_box catalog_list tab">
					<div class="wrap">
						<? foreach ( $arResult['F_CERTIFICATE'] as $file ): ?>
							<div class="files-download">
								<a href="<?= $file["SRC"] ?>" target="_blank"><i class="icons <?= $file["CLASS"] ?>"></i><span><?= strlen( $file["DESCRIPTION"] ) > 0 ? $file["DESCRIPTION"] : "������� ����" ?></span><span class="size"> / <?= $file["FORMAT_SIZE"] ?></span></a>
							</div>
						<? endforeach; ?>
					</div>
				</div>
			<? endif; ?>
		</div>
	<? endif; ?>

	<? if ( $arResult["DISPLAY_PROPERTIES"]['LIQUIDATED']['VALUE'] == '' )://(is_array($arResult["MANAGER"])):?>
		<div class="rc_block brdr_box">
			<div class="wrap faces_box">
				<div class="title">
					<div class="mb_title_o">
						<a id="nodec" href="javascript: void(0)">������������� <?= $arResult['NAME'] ?>
							�� �����</a>
					</div>
				</div>
				<div class="faces_row">
					<? if ( is_array( $arResult["MANAGER"] ) && count( $arResult["MANAGER"] ) ): ?>
						<div class="image_box">
							<img border="0" src="/images/search_placeholder.png" width="48" height="42">
						</div>
						<div class="row">
							<div class="dropdown name">
								<a class="account js-bubble-tooltip"><?= $arResult["MANAGER"]['NAME'] ?> <?= $arResult["MANAGER"]['SECOND_NAME'] ?> <?= $arResult["MANAGER"]['LAST_NAME'] ?></a>

								<div class="submenu">
									<ul class="root">
										<li>
											<a href="/personal/messages/<?= $arResult["MANAGER"]["ID"] ?>/">��������
												���������</a></li>
									</ul>
								</div>
							</div>
						</div>
					<? endif; ?>
					<? if ( $USER->IsAuthorized() ): ?>
						<? if ( $arResult["MANAGER"]["ID"] != $USER->GetID() ): ?>
							<div class="js-make-predstavitel">
								<? if ( isset( $arResult["PREDSTAVITEL_STATUS"] ) ): ?>
									<? if ( $arResult["PREDSTAVITEL_STATUS"] == (LANG == 's1' ? 64 : 143) )://���������������?>
										���� ������ �� ������������
									<? elseif ( $arResult["PREDSTAVITEL_STATUS"] == (LANG == 's1' ? 66 : 145) )://���������������?>
										���� ������ ���������
									<? endif; ?>
								<? else: ?>
									<a data-company-id="<?= $arResult["ID"] ?>" href="javascript:void(0);">�����
										��������������</a>
								<? endif; ?>
							</div>
						<? endif; ?>
					<? else: ?>
						<a href="/auth/?register=yes&backurl=<?= urlencode( $arResult["DETAIL_PAGE_URL"] ) ?>">�����
							��������������</a>
					<? endif; ?>
				</div>
				<!--faces_row-->
			</div>
		</div>
	<? endif; ?>
	<?

		$asFOTO = [];

		// ������� ������ �������� �������� �� ��������� $IBLOCK_ID, � ������� ���� ��������
		// �� ��������� �������� SRC, ������������ � https://
		$arFilter = Array("IBLOCK_ID" => "10",'UF_FIRM_ID'=>$arResult['ID']);
		$db_list = CIBlockSection::GetList(Array(), $arFilter, true);
		if($ar_result = $db_list->GetNext())
		{
		 $asFOTO ['ID'] = $ar_result['ID'];
		}

	?>
	<? if (!empty($asFOTO ['ID'])) { ?>
		<div class="toog withTitlePhoto" style="overflow: hidden;">
		         <ul class="mb_menu">
		            <li class="mb_title sel" id="mb_title_f">
		               <a href="#1">���������</a>
		               <div class="sclew"></div>
		            </li>
<!--		            <li>
		               <a href="#3" data-onclick-once="lazyload_loadPhotoCarusel34">����������</a>
		               <div class="sclew"></div>
		            </li>-->
		         </ul>
		         <div class="gray_box catalog_list tab"  style="background-color: #fff;   border: 1px solid #e2e2e2; box-shadow: none;">
		            <?//����������
		            $APPLICATION->IncludeComponent(
			"zvv:catalog.sections.top",
			"main_p",
			array(
				"IBLOCK_TYPE" => "media_content",
				"IBLOCK_ID" => "10",
				"SECTION_IDS" => $asFOTO ['ID'],
				"SECTION_FIELDS" => array(
					0 => "",
					1 => "",
				),
				"SECTION_USER_FIELDS" => array(
					0 => "",
					1 => "",
				),
				"SECTION_SORT_FIELD" => "Sort",
				"SECTION_SORT_ORDER" => "desc",
				"ELEMENT_SORT_FIELD" => "sort",
				"ELEMENT_SORT_ORDER" => "asc",
				"FILTER_NAME" => "",
				"SECTION_URL" => "",
				"DETAIL_URL" => "",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"SECTION_COUNT" => "50",
				"ELEMENT_COUNT" => "1",
				"PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"USE_PRODUCT_QUANTITY" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "300",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"COMPONENT_TEMPLATE" => "main_p",
				"ELEMENT_SORT_FIELD2" => "id",
				"ELEMENT_SORT_ORDER2" => "desc",
				"HIDE_NOT_AVAILABLE" => "N",
				"LINE_ELEMENT_COUNT" => "3",
				"BASKET_URL" => "/personal/basket.php",
				"DISPLAY_COMPARE" => "N",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"PRICE_CODE" => array(
				),
				"USE_PRICE_COUNT" => "N",
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" => "Y",
				"PRODUCT_PROPERTIES" => array(
				),
				"CONVERT_CURRENCY" => "N"
			),
			false
		);?>

		         </div>
		         <div class="gray_box catalog_list tab"  style="background-color: #fff;   border: 1px solid #e2e2e2; box-shadow: none;">
		            <?//�����������
		            /*$APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
		                     "SALT" => LAZYLOAD_SALT,
		                     "COMPONENTS" => array(
		                         array(
		                             "NAME" => "bitrix:catalog.sections.top",
		                             "TEMPLATE" => "main_p",
		                             "PARAMETERS" => array(
		                                   "IBLOCK_TYPE" => "media_content",
		                                   "IBLOCK_ID" => 34,
		                                   "SECTION_FIELDS" => Array(""),
		                                   "SECTION_USER_FIELDS" => Array(""),
		                                   "SECTION_SORT_FIELD" => "TIMESTAMP_X",
		                                   "SECTION_SORT_ORDER" => "desc",
		                                   "ELEMENT_SORT_FIELD" => "sort",
		                                   "ELEMENT_SORT_ORDER" => "asc",
		                                   "FILTER_NAME" => "",
		                                   "SECTION_URL" => "",
		                                   "DETAIL_URL" => "",
		                                   "ACTION_VARIABLE" => "action",
		                                   "PRODUCT_ID_VARIABLE" => "id",
		                                   "PRODUCT_QUANTITY_VARIABLE" =>  "quantity",
		                                   "PRODUCT_PROPS_VARIABLE" =>  "prop",
		                                   "SECTION_ID_VARIABLE" => "SECTION_ID",
		                                   "SECTION_COUNT" => "7",
		                                   "ELEMENT_COUNT" => "1",
		                                   "PROPERTY_CODE" => Array(),
		                                   "USE_PRODUCT_QUANTITY" => "Y",
		                                   "CACHE_TYPE" => "A",
		                                   "CACHE_TIME" => "3600",
		                                   "CACHE_FILTER" => "N",
		                                   "CACHE_GROUPS" => "N"
		                             )
		                         )
		                     )
		                 )
		             );*/
		            ?>
		         </div>
		      </div>
	<? } ?>

	<? if ($arResult["DISPLAY_PROPERTIES"]['LIQUIDATED']['VALUE'] == '' && count( $arResult["STAFF"] ) ): ?>
		<div class="rc_block brdr_box">
			<div class="wrap faces_box">
				<div class="title">
					<div class="mb_title_o">
						<a id="nodec" href="javascript: void(0)">��������� <?= $arResult['NAME'] ?>
							�� �����</a>
					</div>
				</div>
				<? foreach ( $arResult["STAFF"] as $arUser ): ?>
					<div class="faces_row">
						<div class="image_box"><?= CFile::ShowImage( $arUser['PERSONAL_PHOTO'], 50, 50 ); ?></div>
						<div class="row">
							<div class="name"><?= $arUser['NAME'] ?> <?= $arUser['SECOND_NAME'] ?> <?= $arUser['LAST_NAME'] ?></div>
							<div class="post"></div>
							<div class="company"></div>
						</div>
						<!-- <div class="socials"><a title="vk" class="icons icon_vk2" href="#"></a><a title="facebook" class="icons icon_face2" href="#"></a><a title="twitter" class="icons icon_twit2" href="#"></a></div>-->
					</div><!--faces_row-->
				<? endforeach; ?>
			</div>
		</div>
	<? endif; ?>
	<?
	$arFilter = array(
		"IBLOCK_ID"     => 11,
		"IBLOCK_ACTIVE" => "Y",
		"SECTION_ID"    => 0,
		"=CODE"         => $arResult["MANAGER"]["LOGIN"]
	);
	$db_res   = CIBlockSection::GetList( array( "ID" => "DESC" ), $arFilter, false, array(
			"ID",
			"CREATED_BY",
			"ACTIVE",
			"IBLOCK_SECTION_ID",
			"NAME",
			"PICTURE",
			"DESCRIPTION",
			"DESCRIPTION_TYPE",
			"CODE",
			"SOCNET_GROUP_ID",
			"PICTURE",
			"UF_DEFAULT",
			"UF_GALLERY_SIZE",
			"UF_GALLERY_RECALC",
			"UF_DATE"
		)
	);

	if ( ( $res = $db_res->GetNext() ) && is_array( $arResult["MANAGER"] ) ):?>
		<div class="rc_block brdr_box">
			<div class="wrap">
				<h3 style="color:red;">���� <?= $arResult['NAME'] ?></h3>
				<ul class="company_photo">
					<?
					$arSelect = Array( "ID", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID", "NAME" );
					$arFilter = Array( "IBLOCK_ID"           => 11,
					                   "SECTION_ID"          => $res['ID'],
					                   "INCLUDE_SUBSECTIONS" => "Y",
					                   "ACTIVE"              => "Y"
					);
					$res      = CIBlockElement::GetList( Array( "rand" => "asc" ), $arFilter, false, Array( "nPageSize" => 6 ), $arSelect );
					while ( $ob = $res->GetNextElement() ):?>
						<? $arFields = $ob->GetFields();?>
						<li>
							<a href="/gallery/<?= $arResult["MANAGER"]["LOGIN"] ?>/<?= $arFields['IBLOCK_SECTION_ID'] ?>/"><img src="<?= CFile::GetPath( $arFields['PREVIEW_PICTURE'] ) ?>"></a>
						</li>
					<? endwhile;?>
				</ul>
				<div class="clear"></div>
				<br/> <a href="/gallery/<?= $arResult["MANAGER"]["LOGIN"] ?>/" class="nodecor bold">���
					����</a>
			</div>
		</div>
	<? endif; ?>

	<? if ( $arResult['PROPERTIES']['VIDEO']["VALUE"]["path"] || strlen( $arResult['PROPERTIES']['VIDEO_LINK']["VALUE"] ) > 0 ): ?>
		<? if ( strlen( $arResult['PROPERTIES']['VIDEO_LINK']["VALUE"] ) > 0 ): ?>
			<? $link = $arResult['PROPERTIES']['VIDEO_LINK']["VALUE"]; ?>
		<? elseif ( $arResult['PROPERTIES']['VIDEO']["VALUE"]["path"] ): ?>
			<? $link = $arResult['PROPERTIES']['VIDEO']["VALUE"]["path"]; ?>
		<? endif; ?>

		<div class="rc_block brdr_box">
			<div class="wrap">
				<h3 style="color:red;">����� <?= $arResult['NAME'] ?></h3>

				<div class="video_box">
					<a href="javascript:void(0);" onclick="showPlayer('<?= $link ?>');"><?= $arResult['PROPERTIES']['VIDEO_IMG']["VALUE"] ? CFile::ShowImage( $arResult['PROPERTIES']['VIDEO_IMG']["VALUE"], 270, 200 ) : $arResult['NAME'] ?></a><br>
				</div>
			</div>
		</div>
		<div style="position: relative;">
			<div id="flvplayer">
				<div class="swfframe" id="swfframe"></div>
			</div>
		</div>
	<? endif; ?>

	<!-- ad B2-1 -->
	<div class="mc_block">
		<? $APPLICATION->IncludeComponent( "bitrix:advertising.banner", "", array(
				"TYPE"        => "B2_1",
				"NOINDEX"     => "N",
				"CACHE_TYPE"  => "A",
				"CACHE_TIME"  => "0",
				"CACHE_NOTES" => ""
			)
		); ?>
	</div>
	<!-- ad B2-2 -->
	<div class="mc_block">
		<? $APPLICATION->IncludeComponent( "bitrix:advertising.banner", "", array(
				"TYPE"        => "B2_2",
				"NOINDEX"     => "N",
				"CACHE_TYPE"  => "A",
				"CACHE_TIME"  => "0",
				"CACHE_NOTES" => ""
			)
		); ?>
	</div>
	<!-- ad B2-3 -->
	<div class="mc_block">
		<? $APPLICATION->IncludeComponent( "bitrix:advertising.banner", "", array(
				"TYPE"        => "B2_3",
				"NOINDEX"     => "N",
				"CACHE_TYPE"  => "A",
				"CACHE_TIME"  => "0",
				"CACHE_NOTES" => ""
			)
		); ?>
	</div>
	<!-- ad B2-4 -->
	<div class="mc_block">
		<? $APPLICATION->IncludeComponent( "bitrix:advertising.banner", "", array(
				"TYPE"        => "B2_4",
				"NOINDEX"     => "N",
				"CACHE_TYPE"  => "A",
				"CACHE_TIME"  => "0",
				"CACHE_NOTES" => ""
			)
		); ?>
	</div>


<div id="yandex_ad"></div>
<script type="text/javascript">
(function(w, d, n, s, t) {
w[n] = w[n] || [];
w[n].push(function() {
Ya.Direct.insertInto(window.location.hostname=='agrobelarus.by'?135492:135481, "yandex_ad", {
ad_format: "direct",
type: "posterVertical",
border_type: "ad",
limit: 5,
title_font_size: 3,
links_underline: true,
site_bg_color: "FFFFFF",
border_color: "CCCCCC",
title_color: "266BAB",
url_color: "000099",
text_color: "000000",
hover_color: "0B9DF1",
sitelinks_color: "0000CC",
favicon: true,
no_sitelinks: false
});
});
t = d.getElementsByTagName("script")[0];
s = d.createElement("script");
s.src = "//an.yandex.ru/system/context.js";
s.type = "text/javascript";
s.async = true;
t.parentNode.insertBefore(s, t);
})(window, document, "yandex_context_callbacks");
</script>
</div>
<div class="middle_col">
	<? if ( strlen( $arResult["PREVIEW_TEXT"] ) || strlen( $arResult["DETAIL_TEXT"] ) || strlen( $arResult["FIELDS"]["PREVIEW_TEXT"] ) ): ?>
		<div class="brdr_box company_description_block" style="border-color:#ffab09; background-color:#fdfee8;">
			<div style="padding:9px;">
				<? if ( $arParams["DISPLAY_DATE"] != "N" && $arResult["DISPLAY_ACTIVE_FROM"] ): ?>
					<span class="news-date-time"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
				<? endif; ?>
				<? if ( $arParams["DISPLAY_NAME"] != "N" && $arResult["NAME"] ): ?>
					<!-- <h2 style="color:red;"><?= $arResult["NAME"] ?></h2> -->
				<? endif; ?>
				<? if ( $arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arResult["FIELDS"]["PREVIEW_TEXT"] ): ?>
					<p class="js-fancybox_table"><?= $arResult["FIELDS"]["PREVIEW_TEXT"];
						unset( $arResult["FIELDS"]["PREVIEW_TEXT"] ); ?></p>
				<? endif; ?>
				<? if ( $arResult["NAV_RESULT"] ): ?>
					<? if ( $arParams["DISPLAY_TOP_PAGER"] ): ?><?= $arResult["NAV_STRING"] ?>
						<br/><? endif; ?>
					<? echo $arResult["NAV_TEXT"]; ?>
					<? if ( $arParams["DISPLAY_BOTTOM_PAGER"] ): ?>
						<br/><?= $arResult["NAV_STRING"] ?><? endif; ?>
				<? elseif ( strlen( $arResult["DETAIL_TEXT"] ) > 0 ): ?>
					<div class="js-fancybox_table">
						<? echo $arResult["DETAIL_TEXT"]; ?>
					</div>
				<? else: ?>
					<div class="js-fancybox_table">
						<? echo $arResult["PREVIEW_TEXT"]; ?>
					</div>
				<? endif ?>
				<div class="clearfix"></div>
			</div>
		</div>
	<? endif; ?>

	<? if ( count( $arResult["CATEGORY"] ) > 0 ): ?>
		<div class="mc_block">
			<? if ( strlen( $arResult["PREVIEW_TEXT"] ) || strlen( $arResult["DETAIL_TEXT"] ) || strlen( $arResult["FIELDS"]["PREVIEW_TEXT"] ) ): ?>
				<br/>
			<? endif; ?>
			<b>� ����������:</b>&nbsp;<?= implode( ", ", $arResult["CATEGORY"] ); ?>
		</div>
	<? endif; ?>
	<div style="clear:both"></div>
	<br/>


	<? if ( $arResult["SHOW_TABS"] ): ?>

		<h3 class="title" style="font-family: Arial, Helvetica, sans-serif;
	   font-size: 12px; margin-left: 0; padding-left: 0;
	   font-weight: bold; margin-bottom: 10px">������ ��������� "<?= $arResult['NAME'] ?>"</h3>
		<a name="tabs"></a>
		<div class="mc_block withoutTitleM">
			<ul class="mb_menu">
				<? foreach ( $arResult["TABS"] as $tab ): ?>
					<? if ( strlen( $tab["HTML"] ) > 0 ): ?>
						<li class="<?= $tab["ACTIVE"] || $arResult["FIRST_ACTIVE"] ? "sel" : "" ?>">
							<a href="javascript: void(0)"><?= $tab["NAME"] ?></a>

							<div class="sclew"></div>
						</li>
						<? if ( $arResult["FIRST_ACTIVE"] ) {
							$arResult["FIRST_ACTIVE"] = false;
						} ?>
					<? endif; ?>
				<? endforeach; ?>
			</ul>

			<? foreach ( $arResult["TABS"] as $tab ): ?>
				<? if ( strlen( $tab["HTML"] ) > 0 && strlen( $tab["SECTIONS_HTML"] ) > 0 ): ?>
					<div class="tab">
						<noindex><?= $tab["SECTIONS_HTML"] ?></noindex>
						<?= $tab["HTML"]; ?>
					</div>
				<? endif; ?>
			<? endforeach; ?>
		</div>
		<div style="clear:both"></div>
		<br/>
	<? endif; ?>

	<!-- ad B1 -->
	<div class="mc_block">
		<? $APPLICATION->IncludeComponent( "bitrix:advertising.banner", "", array(
				"TYPE"        => "B1",
				"NOINDEX"     => "N",
				"CACHE_TYPE"  => "A",
				"CACHE_TIME"  => "0",
				"CACHE_NOTES" => ""
			)
		); ?>
	</div>


	<?
	$ia         = false;
	$in         = false;
	$na         = false;
	$nn         = false;
	$aa         = false;
	$arSelectNA = Array( "ID", "NAME", "DATE_ACTIVE_FROM" );
	$arFilterNA = Array( "IBLOCK_ID" => IBLOCK_NEWS, "=PROPERTY_FIRM" => $arResult['ID'], "ACTIVE" => "Y" );
	$res        = CIBlockElement::GetList( Array(), $arFilterNA, false, Array( "nPageSize" => 50 ), $arSelectNA );
	if ( $ob = $res->GetNextElement() ) {
		$na = true;
		$nn = true;
	}

	$arSelectNA = Array( "ID", "NAME", "DATE_ACTIVE_FROM" );
	$arFilterNA = Array( "IBLOCK_ID" => IBLOCK_ARTICLE, "=PROPERTY_FIRM" => $arResult['ID'], "ACTIVE" => "Y" );
	$res        = CIBlockElement::GetList( Array(), $arFilterNA, false, Array( "nPageSize" => 50 ), $arSelectNA );

	if ( $ob = $res->GetNextElement() ) {
		$na = true;
		$aa = true;
	}

	$arSelectNA = Array( "ID", "NAME", "DATE_ACTIVE_FROM" );
	$arFilterNA = Array( "IBLOCK_ID" => IBLOCK_INTERVIEW, "=PROPERTY_FIRM" => $arResult['ID'], "ACTIVE" => "Y" );
	$res        = CIBlockElement::GetList( Array(), $arFilterNA, false, Array( "nPageSize" => 50 ), $arSelectNA );

	if ( $ob = $res->GetNextElement() ) {
		$ia = true;
		$in = true;
	}
	?>
	<? if ( $na ) { ?>
		<h3 class="title" >������� � ������ <?= $arResult['NAME'] ?></h3>
		<div class="mc_block withoutTitle">
			<ul class="mb_menu">
				<? if ( $nn ): ?>
					<li class="sel"><a href="#">�������</a>

						<div class="sclew"></div>
					</li>
				<? endif; ?>
				<? if ( $aa ): ?>
					<li <? if ( ! $nn): ?>class="sel"<? endif; ?>><a href="#">������</a>

						<div class="sclew"></div>
					</li>
				<? endif; ?>
				<? if ( $ia ): ?>
					<li <? if ((!$nn) && (!$na)): ?>class="sel"<? endif; ?>><a href="#">��������</a>

						<div class="sclew"></div>
					</li>
				<? endif; ?>
			</ul>
			<?
			global $arCompanyFilter;
			$arCompanyFilter['PROPERTY_FIRM'] = $arResult['ID']
			?>
			<? if ( $nn ): ?>
				<?
				$APPLICATION->IncludeComponent( "bitrix:news.list", "list-tabs", Array(
					"IBLOCK_TYPE"                     => "information_part",
					// ��� ��������������� ����� (������������ ������ ��� ��������)
					"IBLOCK_ID"                       => IBLOCK_NEWS,
					// ��� ��������������� �����
					"NEWS_COUNT"                      => "3",
					// ���������� �������� �� ��������
					"SORT_BY1"                        => "ACTIVE_FROM",
					// ���� ��� ������ ���������� ��������
					"SORT_ORDER1"                     => "DESC",
					// ����������� ��� ������ ���������� ��������
					"SORT_BY2"                        => "SORT",
					// ���� ��� ������ ���������� ��������
					"SORT_ORDER2"                     => "ASC",
					// ����������� ��� ������ ���������� ��������
					"FILTER_NAME"                     => "arCompanyFilter",
					// ������
					"FIELD_CODE"                      => array(// ����
						0 => "DETAIL_TEXT",
						1 => "",
					),
					"PROPERTY_CODE"                   => array(// ��������
						0 => "",
						1 => "",
					),
					"CHECK_DATES"                     => "Y",
					// ���������� ������ �������� �� ������ ������ ��������
					"DETAIL_URL"                      => "",
					// URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
					"AJAX_MODE"                       => "Y",
					// �������� ����� AJAX
					"AJAX_OPTION_JUMP"                => "Y",
					// �������� ��������� � ������ ����������
					"AJAX_OPTION_STYLE"               => "Y",
					// �������� ��������� ������
					"AJAX_OPTION_HISTORY"             => "N",
					// �������� �������� ��������� ��������
					"CACHE_TYPE"                      => "A",
					// ��� �����������
					"CACHE_TIME"                      => "3600",
					// ����� ����������� (���.)
					"CACHE_FILTER"                    => "Y",
					// ���������� ��� ������������� �������
					"CACHE_GROUPS"                    => "N",
					// ��������� ����� �������
					"PREVIEW_TRUNCATE_LEN"            => $arParams["PREVIEW_TRUNCATE_LEN"],
					// ������������ ����� ������ ��� ������ (������ ��� ���� �����)
					"ACTIVE_DATE_FORMAT"              => "d.m H:i",
					// ������ ������ ����
					"SET_TITLE"                       => "N",
					// ������������� ��������� ��������
					"SET_STATUS_404"                  => "N",
					// ������������� ������ 404, ���� �� ������� ������� ��� ������
					"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
					// �������� �������� � ������� ���������
					"ADD_SECTIONS_CHAIN"              => "N",
					// �������� ������ � ������� ���������
					"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
					// �������� ������, ���� ��� ���������� ��������
					"PARENT_SECTION"                  => "",
					// ID �������
					"PARENT_SECTION_CODE"             => "",
					// ��� �������
					"DISPLAY_TOP_PAGER"               => "N",
					// �������� ��� �������
					"DISPLAY_BOTTOM_PAGER"            => "Y",
					// �������� ��� �������
					"PAGER_TITLE"                     => "�������",
					// �������� ���������
					"PAGER_SHOW_ALWAYS"               => "Y",
					// �������� ������
					"PAGER_TEMPLATE"                  => "defaultv2",
					// �������� �������
					"PAGER_DESC_NUMBERING"            => "N",
					// ������������ �������� ���������
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					// ����� ����������� ������� ��� �������� ���������
					"PAGER_SHOW_ALL"                  => "Y",
					// ���������� ������ "���"
					"DISPLAY_DATE"                    => "Y",
					// �������� ���� ��������
					"DISPLAY_NAME"                    => "Y",
					// �������� �������� ��������
					"DISPLAY_PICTURE"                 => "Y",
					// �������� ����������� ��� ������
					"DISPLAY_PREVIEW_TEXT"            => "Y",
					// �������� ����� ������
					"AJAX_OPTION_ADDITIONAL"          => "",
					// �������������� �������������
				), false
				);
				?>
			<? endif; ?>
			<? if ( $aa ): ?>
				<?
				$APPLICATION->IncludeComponent( "bitrix:news.list", "list-tabs", Array(
					"IBLOCK_TYPE"                     => "information_part",
					// ��� ��������������� ����� (������������ ������ ��� ��������)
					"IBLOCK_ID"                       => IBLOCK_ARTICLE,
					// ��� ��������������� �����
					"NEWS_COUNT"                      => "3",
					// ���������� �������� �� ��������
					"SORT_BY1"                        => "ACTIVE_FROM",
					// ���� ��� ������ ���������� ��������
					"SORT_ORDER1"                     => "DESC",
					// ����������� ��� ������ ���������� ��������
					"SORT_BY2"                        => "SORT",
					// ���� ��� ������ ���������� ��������
					"SORT_ORDER2"                     => "ASC",
					// ����������� ��� ������ ���������� ��������
					"FILTER_NAME"                     => "arCompanyFilter",
					// ������
					"FIELD_CODE"                      => array(// ����
						0 => "DETAIL_TEXT",
						1 => "PREVIEW_PICTURE",
						2 => "DETAIL_PICTURE",
					),
					"PROPERTY_CODE"                   => array(// ��������
						0 => "",
						1 => "",
					),
					"CHECK_DATES"                     => "Y",
					// ���������� ������ �������� �� ������ ������ ��������
					"DETAIL_URL"                      => "",
					// URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
					"AJAX_MODE"                       => "Y",
					// �������� ����� AJAX
					"AJAX_OPTION_JUMP"                => "Y",
					// �������� ��������� � ������ ����������
					"AJAX_OPTION_STYLE"               => "Y",
					// �������� ��������� ������
					"AJAX_OPTION_HISTORY"             => "N",
					// �������� �������� ��������� ��������
					"CACHE_TYPE"                      => "A",
					// ��� �����������
					"CACHE_TIME"                      => "36000",
					// ����� ����������� (���.)
					"CACHE_FILTER"                    => "Y",
					// ���������� ��� ������������� �������
					"CACHE_GROUPS"                    => "N",
					// ��������� ����� �������
					"PREVIEW_TRUNCATE_LEN"            => $arParams["PREVIEW_TRUNCATE_LEN"],
					// ������������ ����� ������ ��� ������ (������ ��� ���� �����)
					"ACTIVE_DATE_FORMAT"              => "d.m H:i",
					// ������ ������ ����
					"SET_TITLE"                       => "N",
					// ������������� ��������� ��������
					"SET_STATUS_404"                  => "N",
					// ������������� ������ 404, ���� �� ������� ������� ��� ������
					"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
					// �������� �������� � ������� ���������
					"ADD_SECTIONS_CHAIN"              => "N",
					// �������� ������ � ������� ���������
					"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
					// �������� ������, ���� ��� ���������� ��������
					"PARENT_SECTION"                  => "",
					// ID �������
					"PARENT_SECTION_CODE"             => "",
					// ��� �������
					"DISPLAY_TOP_PAGER"               => "N",
					// �������� ��� �������
					"DISPLAY_BOTTOM_PAGER"            => "Y",
					// �������� ��� �������
					"PAGER_TITLE"                     => "�������",
					// �������� ���������
					"PAGER_SHOW_ALWAYS"               => "Y",
					// �������� ������
					"PAGER_TEMPLATE"                  => "defaultv2",
					// �������� �������
					"PAGER_DESC_NUMBERING"            => "N",
					// ������������ �������� ���������
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					// ����� ����������� ������� ��� �������� ���������
					"PAGER_SHOW_ALL"                  => "Y",
					// ���������� ������ "���"
					"DISPLAY_DATE"                    => "Y",
					// �������� ���� ��������
					"DISPLAY_NAME"                    => "Y",
					// �������� �������� ��������
					"DISPLAY_PICTURE"                 => "Y",
					// �������� ����������� ��� ������
					"DISPLAY_PREVIEW_TEXT"            => "Y",
					// �������� ����� ������
					"AJAX_OPTION_ADDITIONAL"          => "",
					// �������������� �������������
				), false
				);
				?>
			<? endif; ?>
			<? if ( $ia ): ?>
				<?
				$APPLICATION->IncludeComponent( "bitrix:news.list", "list-tabs", Array(
					"IBLOCK_TYPE"                     => "information_part",
					// ��� ��������������� ����� (������������ ������ ��� ��������)
					"IBLOCK_ID"                       => IBLOCK_INTERVIEW,
					// ��� ��������������� �����
					"NEWS_COUNT"                      => "3",
					// ���������� �������� �� ��������
					"SORT_BY1"                        => "ACTIVE_FROM",
					// ���� ��� ������ ���������� ��������
					"SORT_ORDER1"                     => "DESC",
					// ����������� ��� ������ ���������� ��������
					"SORT_BY2"                        => "SORT",
					// ���� ��� ������ ���������� ��������
					"SORT_ORDER2"                     => "ASC",
					// ����������� ��� ������ ���������� ��������
					"FILTER_NAME"                     => "arCompanyFilter",
					// ������
					"FIELD_CODE"                      => array(// ����
						0 => "DETAIL_TEXT",
						1 => "PREVIEW_PICTURE",
						2 => "DETAIL_PICTURE",
					),
					"PROPERTY_CODE"                   => array(// ��������
						0 => "",
						1 => "",
					),
					"CHECK_DATES"                     => "Y",
					// ���������� ������ �������� �� ������ ������ ��������
					"DETAIL_URL"                      => "",
					// URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
					"AJAX_MODE"                       => "Y",
					// �������� ����� AJAX
					"AJAX_OPTION_JUMP"                => "Y",
					// �������� ��������� � ������ ����������
					"AJAX_OPTION_STYLE"               => "Y",
					// �������� ��������� ������
					"AJAX_OPTION_HISTORY"             => "N",
					// �������� �������� ��������� ��������
					"CACHE_TYPE"                      => "A",
					// ��� �����������
					"CACHE_TIME"                      => "36000",
					// ����� ����������� (���.)
					"CACHE_FILTER"                    => "Y",
					// ���������� ��� ������������� �������
					"CACHE_GROUPS"                    => "N",
					// ��������� ����� �������
					"PREVIEW_TRUNCATE_LEN"            => $arParams["PREVIEW_TRUNCATE_LEN"],
					// ������������ ����� ������ ��� ������ (������ ��� ���� �����)
					"ACTIVE_DATE_FORMAT"              => "d.m H:i",
					// ������ ������ ����
					"SET_TITLE"                       => "N",
					// ������������� ��������� ��������
					"SET_STATUS_404"                  => "N",
					// ������������� ������ 404, ���� �� ������� ������� ��� ������
					"INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
					// �������� �������� � ������� ���������
					"ADD_SECTIONS_CHAIN"              => "N",
					// �������� ������ � ������� ���������
					"HIDE_LINK_WHEN_NO_DETAIL"        => "N",
					// �������� ������, ���� ��� ���������� ��������
					"PARENT_SECTION"                  => "",
					// ID �������
					"PARENT_SECTION_CODE"             => "",
					// ��� �������
					"DISPLAY_TOP_PAGER"               => "N",
					// �������� ��� �������
					"DISPLAY_BOTTOM_PAGER"            => "Y",
					// �������� ��� �������
					"PAGER_TITLE"                     => "�������",
					// �������� ���������
					"PAGER_SHOW_ALWAYS"               => "Y",
					// �������� ������
					"PAGER_TEMPLATE"                  => "defaultv2",
					// �������� �������
					"PAGER_DESC_NUMBERING"            => "N",
					// ������������ �������� ���������
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					// ����� ����������� ������� ��� �������� ���������
					"PAGER_SHOW_ALL"                  => "Y",
					// ���������� ������ "���"
					"DISPLAY_DATE"                    => "Y",
					// �������� ���� ��������
					"DISPLAY_NAME"                    => "Y",
					// �������� �������� ��������
					"DISPLAY_PICTURE"                 => "Y",
					// �������� ����������� ��� ������
					"DISPLAY_PREVIEW_TEXT"            => "Y",
					// �������� ����� ������
					"AJAX_OPTION_ADDITIONAL"          => "",
					// �������������� �������������
				), false
				);
				?>
			<? endif; ?>
		</div>
	<? } ?>

	<? if ( $arResult['DISPLAY_PROPERTIES']['BRANDS']['DISPLAY_VALUE'] ): ?>
		<h3 class="title" style="color:red;">� �������������� ������� <?= $arResult['NAME'] ?></h3>
		<hr/>
		<table width="100%">
			<tr>
				<? foreach ( $arResult['PROPERTIES']['BRANDS']['VALUE'] as $val ): ?>
					<? if ( $val > 0 ): ?>
						<? $arBrand = GetIblockElement( $val ); ?>
						<td>
							<? echo CFile::ShowImage( $arBrand['PREVIEW_PICTURE'], 100, 100 ); ?>
							<br/>
							<a href="<?= $arBrand['DETAIL_PAGE_URL'] ?>"><?= $arBrand['NAME'] ?></a>
						</td>
					<? endif; ?>
				<? endforeach; ?>
			</tr>
		</table>
	<? endif; ?>


	<? if ( $arResult['PROPERTIES']['METRIKA_ID']["VALUE"] )// ������� ������.�������
	{
		YandexMetrika::setCounter( $arResult['PROPERTIES']['METRIKA_ID']["VALUE"] );
	} ?>
</div>