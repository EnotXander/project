<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
Check404SectionPage($component);


if ($arResult["VARIABLES"]["SECTION_CODE"]) {
	$arFilter = array('CODE' => $arResult["VARIABLES"]["SECTION_CODE"]);
	$rsSections = CIBlockSection::GetList(array(), $arFilter);
	if (!$arSection = $rsSections->Fetch())
	{
		@define("ERROR_404", "Y");
				if($arParams["SET_STATUS_404"]==="Y")
					CHTTP::SetStatus("404 Not Found");
		return true;
	}
}

?>
<div class="middle_col">
<?if($arParams["USE_RSS"]=="Y"):?>
	<?
	$rss_url = str_replace(
		array("#SECTION_ID#", "#SECTION_CODE#")
		,array(urlencode($arResult["VARIABLES"]["SECTION_ID"]), urlencode($arResult["VARIABLES"]["SECTION_CODE"]))
		,$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss_section"]
	);
	if(method_exists($APPLICATION, 'addheadstring'))
		$APPLICATION->AddHeadString('<link rel="alternate" type="application/rss+xml" title="'.$rss_url.'" href="'.$rss_url.'" />');
	?>
	<a href="<?=$rss_url?>" title="rss" target="_self"><img alt="RSS" src="<?=$templateFolder?>/images/gif-light/feed-icon-16x16.gif" border="0" align="right" /></a>
<?endif?>
<?if($arParams["USE_SEARCH"]=="Y"):?>
	<?=GetMessage("SEARCH_LABEL")?>
	   <?$APPLICATION->IncludeComponent(
		"bitrix:search.form",
		"flat",
		Array(
			"PAGE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"]
		),
		$component
	);?>
	<br />
<?endif?>
<div class="breadcrumbs">
	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "news", array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
		),
		false,
		array()
	);?>
</div>
<?global $arCFilter;
if ($_REQUEST['region'] > 0)
	$arCFilter['PROPERTY_REGION'] = $_REQUEST['region'];
if ($_REQUEST['city'] > 0)
	$arCFilter['PROPERTY_CITY'] = $_REQUEST['city'];
if ($_REQUEST['SECTION_ID'] > 0)
	$arResult["VARIABLES"]["SECTION_ID"] = $_REQUEST['SECTION_ID'];
if ($_REQUEST['TYPE_P'] > 0)
	$arCFilter['PROPERTY_TYPE_P'] = $_REQUEST['TYPE_P'];
if ((strlen($_REQUEST['name']) > 0) && ($_REQUEST['name'] <> '����� � �������'))
	$arCFilter['NAME'] = '%'.$_REQUEST['name'].'%';
?>
<?$APPLICATION->IncludeComponent(
	"whipstudio:news.list.sorted",
	"",
	Array(
		"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
		"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
		"NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
		/*"SORT_BY1"	=>	$arParams["SORT_BY1"],
		"SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
		"SORT_BY2"	=>	$arParams["SORT_BY2"],
		"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],*/
                /*"SORT_BY1" => "PROPERTY_PREMIUM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "PROPERTY_TARIF.PROPERTY_SEARCH_SORT",
                "SORT_ORDER1" => "DESC",*/
		"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
		"SET_TITLE"	=>	$arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
		"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
		"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
		"DISPLAY_NAME"	=>	"Y",
		"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME"	=>	$arParams["FILTER_NAME"],
		"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES"	=>	$arParams["CHECK_DATES"],
		"PARENT_SECTION"	=>	$arResult["VARIABLES"]["SECTION_ID"],
        "PARENT_SECTION_CODE"	=>	$arResult["VARIABLES"]["SECTION_CODE"],
		"MAX_VOTE" => $arParams["MAX_VOTE"],
		"VOTE_NAMES" => $arParams["VOTE_NAMES"],

                "BROWSER_TITLE" => $arParams["SECTION_BROWSER_TITLE"],
                "META_DESCRIPTION" => $arParams["SECTION_META_DESCRIPTION"],
                "META_KEYWORDS" => $arParams["SECTION_META_KEYWORDS"],
                "SORT" => array(
                     "rating" => array(
                         "NAME" => "�������",
                         "CODE" => array(
                             array(
                                 "CODE" => "PROPERTY_PREMIUM",
                                 "TYPE" => "desc"
                             ),
                             array(
                                 "CODE" => "PROPERTY_TARIF.PROPERTY_SEARCH_SORT",
                                 "TYPE" => "desc"
                             ),
                             array(
                                 "CODE" => "PROPERTY_rating",
                                 "TYPE" => "desc"
                             )
                         ),
                         "TYPE" => "desc"
                         
                     )
                 ),
                "SORT_DEFAULT" => "rating"
	),
	$component
);?>
</div>
