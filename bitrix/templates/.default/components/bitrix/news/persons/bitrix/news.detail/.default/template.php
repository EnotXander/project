<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="mc_block">
   <? if ($arParams["DISPLAY_NAME"] != "N" && $arResult["NAME"]): ?>
      <h1><?= $arResult["NAME"] ?></h1>
   <? endif; ?>
   <div class="news_item">
      <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])): ?>
         <img class="detail_picture" border="0" src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arResult["NAME"] ?>"  title="<?= $arResult["NAME"] ?>" />
      <? endif ?>
      <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arResult["FIELDS"]["PREVIEW_TEXT"]): ?>
         <p class="js-fancybox_table"><?=
         $arResult["FIELDS"]["PREVIEW_TEXT"];
         unset($arResult["FIELDS"]["PREVIEW_TEXT"]);
         ?></p>
      <? endif; ?>
      <? if ($arResult["NAV_RESULT"]): ?>
         <? if ($arParams["DISPLAY_TOP_PAGER"]): ?><?= $arResult["NAV_STRING"] ?><br /><? endif; ?>
         <? echo $arResult["NAV_TEXT"]; ?>
         <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?><br /><?= $arResult["NAV_STRING"] ?><? endif; ?>
      <? elseif (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
         <div class="js-fancybox_table">
            <? echo str_replace(Array("\"/images/", "\"images/"), Array("\"http://energobelarus.by/images/", "\"http://energobelarus.by/images/"), $arResult["DETAIL_TEXT"]); ?>
         </div>
      <? else: ?>
         <div class="js-fancybox_table">
            <? echo $arResult["PREVIEW_TEXT"]; ?>
         </div>
      <? endif ?>
      <div style="clear:both"></div>
      <br />
      <? foreach ($arResult["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
         <?= $arProperty["NAME"] ?>:&nbsp;
         <? if (is_array($arProperty["DISPLAY_VALUE"])): ?>
            <?= implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]); ?>
         <? else: ?>
            <?= $arProperty["DISPLAY_VALUE"]; ?>
         <? endif ?>
         <br />
      <? endforeach; ?>
      <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
      <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed"></div> 
      <? if ($arParams["DISPLAY_DATE"] != "N" && $arResult["DISPLAY_ACTIVE_FROM"]): ?>
         <div class="meta a_autor"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?>  \  <a href="<?= $arResult['SECTION_URL'] ?>"><?= $arResult['SECTION']['PATH'][0]['NAME'] ?></a></div>
      <? endif; ?>
      <? foreach ($arResult["FIELDS"] as $code => $value): ?>
         <div class="meta a_tags"><?= GetMessage("IBLOCK_FIELD_" . $code) ?>:&nbsp;<?= $value; ?></div>
         <br />
      <? endforeach; ?>
      <? if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y"): ?>
         <div class="news-detail-share">
            <noindex>
               <?
               $APPLICATION->IncludeComponent("bitrix:main.share", "", array(
                   "HANDLERS" => $arParams["SHARE_HANDLERS"],
                   "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                   "PAGE_TITLE" => $arResult["~NAME"],
                   "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                   "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                   "HIDE" => $arParams["SHARE_HIDE"],
                       ), $component, array("HIDE_ICONS" => "Y")
               );
               ?>
            </noindex>
         </div>
      <? endif; ?>
   </div>
</div>