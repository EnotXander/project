<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="mc_block">
   <h1><? $APPLICATION->ShowTitle(); ?></h1>
   <span class="font_11 fl_left mar_10_right mar_5_top">����������� ��:</span>
   <ul class="mb_menu">
      <li class="sel"><a href="#1">����</a><div class="sclew"></div></li>
      <li><a href="#1">��������</a><div class="sclew"></div></li>
      <li><a href="#1">���������</a><div class="sclew"></div></li>
   </ul>
   <div class="clear"></div>
   <div class="paging">
      <div class="show_col">
         <form method="get" name="top_f">
            <span class="meta">���������� ��:</span>
            <select name="per_page" onchange="document.top_f.submit()">
               <option value="15" <? if ($arParams['NEWS_COUNT'] == 15) echo "selected"; ?>>15</option>
               <option value="25" <? if ($arParams['NEWS_COUNT'] == 25) echo "selected"; ?>>25</option>
               <option value="50" <? if ($arParams['NEWS_COUNT'] == 50) echo "selected"; ?>>50</option>
            </select>
         </form>
      </div>
      <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
         <?= $arResult["NAV_STRING"] ?><br />
      <? endif; ?>
   </div>
   <div class="peoples_table">
      <div class="brdr_w_top"></div>
      <div class="brdr_w_right"></div>
      <div class="brdr_w_bottom"></div>
      <div class="brdr_w_left"></div>
      <? foreach ($arResult["ITEMS"] as $arItem): ?>
         <?
         $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
         $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
         ?>
         <div class="item_row">
            <div class="image_box">
               <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                  <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                     <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"/></a>
                  <? else: ?>
                     <img border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"/>
                  <? endif; ?>
               <? endif ?>
            </div>
            <div class="row">
               <div class="title"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a></div>
               <p><? echo $arItem["PREVIEW_TEXT"]; ?> <?=$arItem["DISPLAY_PROPERTIES"]['DISPLAY_VALUE']?></p>
            </div>
         </div><!--item_row-->
      <? endforeach; ?>
   </div>
</div>
<div class="paging mar_20_bot">
   <div class="show_col">
      <form method="get" name="bot_f">
         <span class="meta">���������� ��:</span>
         <select name="per_page" onchange="document.bot_f.submit()">
            <option value="15" <? if ($arParams['NEWS_COUNT'] == 15) echo "selected"; ?>>15</option>
            <option value="25" <? if ($arParams['NEWS_COUNT'] == 25) echo "selected"; ?>>25</option>
            <option value="50" <? if ($arParams['NEWS_COUNT'] == 50) echo "selected"; ?>>50</option>
         </select>
      </form>
   </div>
   <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
      <br /><?= $arResult["NAV_STRING"] ?>
   <? endif; ?>
</div>
