<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      $arElement["DETAIL_TEXT"] = strip_tags($arElement["DETAIL_TEXT"]);
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
      $arResult['ITEMS'][$key] = $arElement;
      
      if(is_array($arElement["PREVIEW_PICTURE"])) $arElement["DETAIL_PICTURE"] = $arElement["PREVIEW_PICTURE"];
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
            $arElement["DETAIL_PICTURE"],
            array("width" => 250, "height" => 250),
            BX_RESIZE_IMAGE_EXACT,
            true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
         $arResult["ITEMS"][$key]["PREVIEW_PICTURE_BIG"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
         $arFileTmp = CFile::ResizeImageGet(
            $arElement["DETAIL_PICTURE"],
            array("width" => 100, "height" => 100),
            BX_RESIZE_IMAGE_EXACT,
            true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
         $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
      foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty)
      {
         $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["DISPLAY_VALUE"] = $arProperty['DISPLAY_VALUE'];
         break;
      }
   }
}
?>