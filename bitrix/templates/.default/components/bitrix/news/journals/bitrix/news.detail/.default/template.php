<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="middle_col">
   <div class="breadcrumbs">
      <?
      $APPLICATION->IncludeComponent("bitrix:breadcrumb", "news", array(
          "START_FROM" => "0",
          "PATH" => "",
          "SITE_ID" => "s1"
              ), false, array()
      );
      ?>
   </div>
   <div class="brand-detail">
      <h2 style="border-bottom:2px solid red;"><?= $arResult["NAME"] ?>&nbsp
      </h2>
      <br/>
      <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])): ?>
         <img class="detail_picture" border="0" src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arResult["NAME"] ?>"  title="<?= $arResult["NAME"] ?>" style="float:left; margin-right:10px;"/>
      <? endif ?>
      <br/>
      <? foreach ($arResult["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
         <? if (in_array($pid, Array("pr01", "pr02"))): ?>
            <b><?= $arProperty['NAME'] ?></b>: <?= $arProperty["DISPLAY_VALUE"] ?> <br/>
         <? endif; ?>
      <? endforeach; ?>
      <br />
      <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arResult["FIELDS"]["PREVIEW_TEXT"]): ?>
         <p>
         <?=
         $arResult["FIELDS"]["PREVIEW_TEXT"];
         unset($arResult["FIELDS"]["PREVIEW_TEXT"]);
         ?>
         </p>
      <? endif; ?>
      <? if ($arResult["NAV_RESULT"]): ?>
            <? if ($arParams["DISPLAY_TOP_PAGER"]): ?><?= $arResult["NAV_STRING"] ?><br /><? endif; ?>
            <? echo $arResult["NAV_TEXT"]; ?>
         <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?><br /><?= $arResult["NAV_STRING"] ?><? endif; ?>
      <? elseif (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
         <? echo $arResult["DETAIL_TEXT"]; ?>
      <? else: ?>
         <? echo $arResult["PREVIEW_TEXT"]; ?>
      <? endif ?>
      <br/>
      <br/>
      <? foreach ($arResult["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
         <? if (in_array($pid, Array("pr03"))): ?>
            <b><?= $arProperty['NAME'] ?></b>: <?= $arProperty["DISPLAY_VALUE"] ?> <br/>
         <? endif; ?>
      <? endforeach; ?>
      <? foreach ($arResult["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
         <? if (in_array($pid, Array("pr04"))): ?>
            <br/>
            <table>
               <tr>
                  <td valign="top">
                     <img src="<?= SITE_TEMPLATE_PATH ?>/img/pdf.jpg">
                  </td>
                  <td style="padding-top:7px; padding-left:5px;">
                     <a href="<?= $arProperty["FILE_VALUE"]["SRC"] ?>" style=""><?= $arProperty["NAME"] ?></a>
                  </td>
               </tr>
            </table>
         <? endif; ?>
      <? endforeach; ?>
      <div style="clear:both"></div>
      <?if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y"):?>
         <div class="news-detail-share">
            <noindex>
               <?
               $APPLICATION->IncludeComponent("bitrix:main.share", "", array(
                   "HANDLERS" => $arParams["SHARE_HANDLERS"],
                   "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                   "PAGE_TITLE" => $arResult["~NAME"],
                   "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                   "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                   "HIDE" => $arParams["SHARE_HIDE"],
                       ), $component, array("HIDE_ICONS" => "Y")
               );
               ?>
            </noindex>
         </div>
      <?endif;?>
   </div>
</div>