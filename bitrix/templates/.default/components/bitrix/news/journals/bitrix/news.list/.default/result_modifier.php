<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$obParser = new CTextParser;
$arPremium = Array();
$arItems = Array();

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      if (is_array($arElement["PREVIEW_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
            $arElement["PREVIEW_PICTURE"],
            array("width" => 75, "height" => 150),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arElement["PREVIEW_IMG"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
      if ($key < 3)
         $arPremium[] = $arElement;
      else
         $arItems[] = $arElement;
   }
}

$arItems = array_chunk($arItems, 3);

$arResult['PREMIUM'] = $arPremium;
$arResult['ITEMS'] = $arItems;
?>