<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


CUtil::InitJSCore(array('ajax', 'fx'));
// ************************* Input params***************************************************************
$arParams["SHOW_LINK_TO_FORUM"] = ($arParams["SHOW_LINK_TO_FORUM"] == "N" ? "N" : "Y");
$arParams["FILES_COUNT"] = intVal(intVal($arParams["FILES_COUNT"]) > 0 ? $arParams["FILES_COUNT"] : 1);
$arParams["IMAGE_SIZE"] = (intVal($arParams["IMAGE_SIZE"]) > 0 ? $arParams["IMAGE_SIZE"] : 100);
if (LANGUAGE_ID == 'ru'):
   $path = str_replace(array("\\", "//"), "/", dirname(__FILE__) . "/ru/script.php");
   include($path);
endif;?>

<div class="news_comments brdr_box">
   <div class="total_comments">������������ <span class="icons icon-comment"></span> <?= count($arResult["MESSAGES"]) ?></div>

   <?if (empty($arResult["ERROR_MESSAGE"]) && !empty($arResult["OK_MESSAGE"])):?>
      <div class="reviews-note-box reviews-note-note">
         <a name="reviewnote"></a>
         <div class="reviews-note-box-text"><?= ShowNote($arResult["OK_MESSAGE"]); ?></div>
      </div>
   <?endif;
   if ($arResult["SHOW_POST_FORM"] == "Y"):?>
      <form name="REPLIER<?= $arParams["form_index"] ?>" id="REPLIER<?= $arParams["form_index"] ?>" action="<?= POST_FORM_ACTION_URI ?>#postform"<? ?> method="POST" enctype="multipart/form-data" onsubmit="return ValidateForm(this, '<?= $arParams["AJAX_TYPE"] ?>');"<? ?> onkeydown="if(null != init_form){init_form(this)}" onmouseover="if(init_form){init_form(this)}" class="forum-form">
         <input type="hidden" name="back_page" value="<?= $arResult["CURRENT_PAGE"] ?>" />
         <input type="hidden" name="ELEMENT_ID" value="<?= $arParams["ELEMENT_ID"] ?>" />
         <input type="hidden" name="SECTION_ID" value="<?= $arParams["SECTION_ID"] ?>" />
         <input type="hidden" name="save_product_review" value="Y" />
         <input type="hidden" name="preview_comment" value="N" />
         <?= bitrix_sessid_post() ?>
         <div class="title font_18"  style="padding:20px; color:red">�������� �����������</div>
         <textarea rows="5" cols="4" name="REVIEW_TEXT" id="REVIEW_TEXT"></textarea>
         <a style="width: 80px;" href="#" class="btns gray_btn fl_right" onclick="document.REPLIER<?= $arParams["form_index"] ?>.submit();"><i></i>&nbsp;���������</a>
      </form>
   <? endif; ?>
   <?$iCount = 0;
   foreach ($arResult["MESSAGES"] as $res):
      $iCount++;?>
      <div class="comment_row" <? if ($iCount > 3): ?>style="display:none;"<? endif; ?>>
         <div class="autor fl_left">
            <b><?= $res["AUTHOR_NAME"] ?></b>
            <div class="date"><?= $res["POST_DATE"] ?></div>
         </div>
         <div class="row line_18">
            <?= $res["POST_MESSAGE_TEXT"] ?>
         </div>
      </div>
   <?endforeach;?>
   <div class="commens_info_row">
      <?= ($iCount > 3) ? '3' : $iCount; ?> �� <?= count($arResult['MESSAGES']); ?> ������������ <a class="nodecor bold" href="javascript:void();" onclick="$('.comment_row').show();">���</a>
   </div>
</div>	
