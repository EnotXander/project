<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="mc_block">
   <? if ($arParams["DISPLAY_NAME"] != "N" && $arResult["NAME"]): ?>
      <h1><?= $arResult["NAME"] ?></h1>
   <? endif; ?>
   <div class="news_item formated_text">
      <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])): ?>
         <img class="detail_picture" border="0" src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arResult["NAME"] ?>"  title="<?= $arResult["NAME"] ?>" />
      <? endif ?>
      <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arResult["FIELDS"]["PREVIEW_TEXT"]): ?>
         <p class="js-fancybox_table"><?=$arResult["FIELDS"]["PREVIEW_TEXT"];
            unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
      <? endif; ?>
      <? if ($arResult["NAV_RESULT"]): ?>
            <? if ($arParams["DISPLAY_TOP_PAGER"]): ?><?= $arResult["NAV_STRING"] ?><br /><? endif; ?>
            <? echo $arResult["NAV_TEXT"]; ?>
         <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?><br /><?= $arResult["NAV_STRING"] ?><? endif; ?>
      <? elseif (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
         <div class="js-fancybox_table">
            <? echo str_replace(Array("\"/images/", "\"images/"), Array("\"http://energobelarus.by/images/", "\"http://energobelarus.by/images/"), $arResult["DETAIL_TEXT"]); ?>
         </div>
      <? else: ?>
         <div class="js-fancybox_table">
            <? echo $arResult["PREVIEW_TEXT"]; ?>
         </div>
      <? endif ?>
      <div style="clear:both"></div>
      <br />
      <? foreach ($arResult["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
         <?= $arProperty["NAME"] ?>:&nbsp;
         <? if (is_array($arProperty["DISPLAY_VALUE"])): ?>
            <?= implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]); ?>
         <? else: ?>
            <?= $arProperty["DISPLAY_VALUE"]; ?>
         <? endif ?>
         <br />
      <? endforeach; ?>
      <?if ((is_array($arResult["PROPERTIES"]["VIDEO"]["VALUE"]) && count($arResult["PROPERTIES"]["VIDEO"]["VALUE"]) > 0)
              || (strlen($arResult["PROPERTIES"]["VIDEO_LINK"]["VALUE"]) > 0)):?>
         <div class="video_block">
            <?
            // �������������
            if (strlen($arResult["PROPERTIES"]["VIDEO_LINK"]["VALUE"]) > 0)
            {
               $videoInfo = array(
                   "provider" => "youtube",
                   "path" => $arResult["PROPERTIES"]["VIDEO_LINK"]["VALUE"],
                   "width" => "675",
                   "height" => "420",
                   "title" => "",
                   "duration" => "",
                   "author" => "",
                   "date" => "",
                   "desc" => "",
                   "CONTROLBAR" => "bottom",
                   "BUFFER_LENGTH" => 10,
                   "VOLUME" => 90,
                   "SKIN" => "stijl"
               );
            } else
            {
               $videoInfo = array(
                   "provider" => "video",
                   "path" => $arResult["PROPERTIES"]["VIDEO"]["VALUE"]["path"],
                   "width" => $arResult["PROPERTIES"]["VIDEO"]["VALUE"]["width"],
                   "height" => $arResult["PROPERTIES"]["VIDEO"]["VALUE"]["height"],
                   "title" => $arResult["PROPERTIES"]["VIDEO"]["VALUE"]["title"],
                   "duration" => $arResult["PROPERTIES"]["VIDEO"]["VALUE"]["duration"],
                   "author" => $arResult["PROPERTIES"]["VIDEO"]["VALUE"]["author"],
                   "date" => $arResult["PROPERTIES"]["VIDEO"]["VALUE"]["date"],
                   "desc" => $arResult["PROPERTIES"]["VIDEO"]["VALUE"]["desc"],
                   "CONTROLBAR" => $arResult["PROPERTIES"]["VIDEO"]["USER_TYPE_SETTINGS"]["CONTROLBAR"],
                   "BUFFER_LENGTH" => $arResult["PROPERTIES"]["VIDEO"]["USER_TYPE_SETTINGS"]["BUFFER_LENGTH"],
                   "VOLUME" => $arResult["PROPERTIES"]["VIDEO"]["USER_TYPE_SETTINGS"]["VOLUME"],
                   "SKIN" => "stijl"
               );
            }

            $APPLICATION->IncludeComponent("bitrix:player", "", array(
                "PLAYER_TYPE" => "auto",
                "USE_PLAYLIST" => "N",
                "PATH" => $videoInfo["path"],
                "PLAYLIST_DIALOG" => "",
                "PROVIDER" => $videoInfo["provider"],
                "STREAMER" => "",
                "WIDTH" => $videoInfo["width"],
                "HEIGHT" => $videoInfo["height"],
                "PREVIEW" => "",
                "FILE_TITLE" => $videoInfo["title"],
                "FILE_DURATION" => $videoInfo["duration"],
                "FILE_AUTHOR" => $videoInfo["author"],
                "FILE_DATE" => $videoInfo["date"],
                "FILE_DESCRIPTION" => $videoInfo["desc"],
                "SKIN_PATH" => "/bitrix/components/bitrix/player/mediaplayer/skins",
                "SKIN" => $videoInfo["SKIN"],
                "CONTROLBAR" => $videoInfo["CONTROLBAR"],
                "WMODE" => "transparent",
                "PLAYLIST" => "",
                "PLAYLIST_SIZE" => "",
                "LOGO" => "",
                "LOGO_LINK" => "",
                "LOGO_POSITION" => "",
                "PLUGINS" => array(),
                "PLUGINS_TWEETIT-1" => "",
                "PLUGINS_FBIT-1" => "",
                "ADDITIONAL_FLASHVARS" => "",
                "WMODE_WMV" => "window",
                "SHOW_CONTROLS" => "Y",
                "PLAYLIST_TYPE" => "xspf",
                "PLAYLIST_PREVIEW_WIDTH" => "",
                "PLAYLIST_PREVIEW_HEIGHT" => "",
                "SHOW_DIGITS" => "Y",
                "CONTROLS_BGCOLOR" => "FFFFFF",
                "CONTROLS_COLOR" => "000000",
                "CONTROLS_OVER_COLOR" => "000000",
                "SCREEN_COLOR" => "000000",
                "AUTOSTART" => "N",
                "REPEAT" => "none",
                "VOLUME" => $videoInfo["VOLUME"],
                "MUTE" => "N",
                "HIGH_QUALITY" => "Y",
                "SHUFFLE" => "N",
                "START_ITEM" => 1,
                "ADVANCED_MODE_SETTINGS" => "N",
                "PLAYER_ID" => "",
                "BUFFER_LENGTH" => $videoInfo["BUFFER_LENGTH"],
                "DOWNLOAD_LINK" => "",
                "DOWNLOAD_LINK_TARGET" => "_self",
                "ADDITIONAL_WMVVARS" => "",
                "ALLOW_SWF" => "Y",
                    )
            );
            ?>                  
         </div> 
      <? endif; ?>      

      <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
      <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed"></div> 
      <? if ($arParams["DISPLAY_DATE"] != "N" && $arResult["DISPLAY_ACTIVE_FROM"]): ?>
         <div class="meta a_autor">
				<?= $arResult["DISPLAY_ACTIVE_FROM"] ?>  \
				<a href="<?= $arResult['SECTION_URL'] ?>"><?= $arResult['SECTION']['PATH'][0]['NAME'] ?></a>
				<?if ($arResult['PROPERTIES']['AUTHOR']['VALUE']):?>
					\ �����: <?=$arResult['PROPERTIES']['AUTHOR']['_USER']['NAME']?> <?=$arResult['PROPERTIES']['AUTHOR']['_USER']['LAST_NAME']?>
				<?endif;?>
			</div>
      <? endif; ?>

      <?if(count($arResult["TAGS_LIST"]) > 0):?>
         <div class="meta a_tags">
            ����: 
            <?foreach($arResult["TAGS_LIST"] as $keyTag=>$tag):?>
               <a href="/articles/search/?tags=<?=$tag?>"><?=$tag?></a><?=isset($arResult["TAGS_LIST"][$keyTag+1]) ? ", " : "";?>
            <?endforeach;?>
         </div>
      <?endif;?>
      <?if ($arResult['PROPERTIES']['PODPIS']['VALUE']):?>
         <div class="meta a_tags">
            <? foreach ($arResult['PROPERTIES']['PODPIS']['DESCRIPTION'] as $k=>$value):

                if (strpos('http://', $arResult['PROPERTIES']['PODPIS'][$k]['VALUE'] ) !== false ) {
                    $arResult['PROPERTIES']['PODPIS']['VALUE'][$k]  = '<a href="'.$arResult['PROPERTIES']['PODPIS']['VALUE'][$k] .'">'.$arResult['PROPERTIES']['PODPIS']['VALUE'][$k] .'</a>';
                }

                ?><?= $value ?>: <?= $arResult['PROPERTIES']['PODPIS']['VALUE'][$k] ?>.
            <? endforeach; ?>
         </div>
       <?endif;?>
      <? /* foreach($arResult["FIELDS"] as $arResult["ELEMENT"]["TAGS_LIST"]):?>
        <div class="meta a_tags">
        ����: <?=$value;?>
        <?endforeach;?>
        <?
        $arrTags = explode(',', $arResult["ELEMENT"]["TAGS_LIST"]);
        $count = count($arrTags);
        $i = 0;

        foreach($arrTags as $value):
        $i++;
        $value = trim($value);
        echo '<a href="/articles/search/?tags='.str_replace(' ', '+', $value).'">'.$value.'</a>';
        if($i != $count) echo ', ';
        endforeach; */
      ?>

      <? if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y"): ?>
         <div class="news-detail-share">
            <noindex>
               <?
               // �������
               $APPLICATION->IncludeComponent("bitrix:main.share", "", array(
                   "HANDLERS" => $arParams["SHARE_HANDLERS"],
                   "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                   "PAGE_TITLE" => $arResult["~NAME"],
                   "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                   "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                   "HIDE" => $arParams["SHARE_HIDE"],
                       ), $component, array("HIDE_ICONS" => "Y")
               );
               ?>
            </noindex>
         </div>
      <? endif; ?>
   </div>
     <?
     //printAdmin($arParams["IBLOCK_ID"]);

      
      ?>
      
</div>