<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die();
$obParser = new CTextParser;

if(strlen($arResult["SECTION"]["PATH"][0]["NAME"]))
{
   SetSectionMeta($arResult["SECTION"]["PATH"][0], array(
      "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
      "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
      "META_KEYWORDS" => $arParams["META_KEYWORDS"]
   ));
}

/*if(strlen($arResult["SECTION"]["PATH"][0]["NAME"]))
{
   $titleSection = $arResult["SECTION"]["PATH"][0]["NAME"];
   $descriptionSection = " {$arResult["SECTION"]["PATH"][0]["NAME"]}";
}
else
{
   $titleSection = "�������";
   $descriptionSection = "";
}
$APPLICATION->SetTitle("{$titleSection} - ������� �������, ������, ������������ �� EnergoBelarus.by");
$APPLICATION->SetPageProperty("description", "������� �������{$descriptionSection} �� EnergoBelarus.by");
$APPLICATION->AddHeadString('<meta name="abstract" content="������� �������'.$descriptionSection.' �� EnergoBelarus.by">', true);
$APPLICATION->SetPageProperty("keywords", "����������, ������� ����������, ������ ������, ������������, ��������, ���������");
 */

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      $arElement["DETAIL_TEXT"] = strip_tags($arElement["DETAIL_TEXT"]);
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
      $arResult['ITEMS'][$key] = $arElement;
      
      if(is_array($arElement["PREVIEW_PICTURE"])) $arElement["DETAIL_PICTURE"] = $arElement["PREVIEW_PICTURE"];
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
            $arElement["DETAIL_PICTURE"],
            array("width" => 250, "height" => 250),
            BX_RESIZE_IMAGE_EXACT,
            true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arResult["ITEMS"][$key]["DETAIL_PICTURE_BIG"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
         $arFileTmp = CFile::ResizeImageGet(
            $arElement["DETAIL_PICTURE"],
            array("width" => 100, "height" => 100),
            BX_RESIZE_IMAGE_EXACT,
            true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
      //��������� �������
      foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty)
      {
         $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"][$pid]["USER"] = CUser::GetByID($arProperty['DISPLAY_VALUE'])->Fetch();
      }
   }
}
?>