function fToggleCommentsForm(link, forceOpen)
{
   if (forceOpen == null)
      forceOpen = false;
      forceOpen = !!forceOpen;
      var form = BX.findChild(link.parentNode.parentNode, {'class':'reviews-reply-form'}, true);
      var bHidden = (form.style.display != 'block') || forceOpen;
      form.style.display = (bHidden ? 'block' : 'none');
      link.innerHTML = (bHidden ? oText['MINIMIZED_MINIMIZE_TEXT'] : oText['MINIMIZED_EXPAND_TEXT']);
      var classAdd = (bHidden ? 'reviews-expanded' : 'reviews-minimized');
      var classRemove = (bHidden ? 'reviews-minimized' : 'reviews-expanded');
      BX.removeClass(BX.addClass(link.parentNode, classAdd), classRemove);
      BX.scrollToNode(BX.findChild(form, {'attribute': { 'name' : 'send_button' }}, true));
         if (window.oLHE)
            setTimeout(function() {
               if (!BX.browser.IsIE())
                  window.oLHE.SetFocusToEnd();
               else
                  window.oLHE.SetFocus();
            }, 100);
}

function reply2author(name)
{ 
   name = name.replace(/&lt;/gi, "<").replace(/&gt;/gi, ">").replace(/&quot;/gi, "\"");
   replyForumFormOpen();
   if (window.oLHE)
   {
      var content = '';
      if (window.oLHE.sEditorMode == 'code')
         content = window.oLHE.GetCodeEditorContent();
      else
         content = window.oLHE.GetEditorContent();

      /*<? if ($arResult["FORUM"]["ALLOW_BIU"] == "Y") { ?>*/
         content += "[B]"+name+"[/B]";
      /*<?// } ?>*/
      content += " \n";
      if (window.oLHE.sEditorMode == 'code')
         window.oLHE.SetContent(content);
      else
         window.oLHE.SetEditorContent(content);
         setTimeout(function() { window.oLHE.SetFocusToEnd();}, 300);
      }
      return false;
}


