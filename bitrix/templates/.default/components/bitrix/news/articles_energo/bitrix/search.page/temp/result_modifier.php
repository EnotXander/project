<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$objElement = new CIBlockElement();

foreach ($arResult["SEARCH"] as $arKey => $arItem)
{
   $res = $objElement->GetByID($arItem["ITEM_ID"]);
   if($arItemReal = $res->GetNext())
   {
      if($arItemReal["PREVIEW_PICTURE"] > 0)
      {
         $FILE = CFile::GetFileArray($arItemReal["PREVIEW_PICTURE"]);
         if (is_array($FILE))
         {                  
            $arFilterImg = array();
            $arFileTmp = CFile::ResizeImageGet(
                $FILE,
                array("width" => $arParams["THUMB_WIDTH"], "height" => $arParams["THUMB_HEIGHT"]),
                BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                true, 
                $arFilterImg
            );

            $FILE = array(
                'SRC' => $arFileTmp["src"],
                'WIDTH' => $arFileTmp["width"],
                'HEIGHT' => $arFileTmp["height"],
                'DESCRIPTION' => strlen($FILE["DESCRIPTION"]) > 0 ? $FILE : $arItemReal["NAME"]
            );

            $arItemReal["PREVIEW_PICTURE"] = $FILE;
         }
      }
      
      $arResult["SEARCH"][$arKey]["REAL_ELEMENT"] = $arItemReal;

      if($arResult["SEARCH"][$arKey]["MODULE_ID"] == "blog")
      {
         $arResult["SEARCH"][$arKey]["RIBBON"] = array(
              "CLASS" => "rib_default",
              "TITLE" => "�����",
              "URL" => "/blogs/"
            );
      }
      elseif($arResult["SEARCH"][$arKey]["MODULE_ID"] == "forum")
      {
         $arResult["SEARCH"][$arKey]["RIBBON"] = array(
              "CLASS" => "rib_default",
              "TITLE" => "������",
              "URL" => "/forum/"
            );
      }
      else
      {
         $rsSection = CIBlockSection::GetByID($arItemReal["IBLOCK_SECTION_ID"]);
         if($arSection = $rsSection->GetNext())
         {
            $arResult["SEARCH"][$arKey]["RIBBON"] = array(
              "CLASS" => "rib_default",
              "TITLE" => $arSection["NAME"],
              "IDD" => $arItemReal["IBLOCK_SECTION_ID"],
              "URL" => $arSection["SECTION_PAGE_URL"]
            );
            if($arResult["SEARCH"][$arKey]["REAL_ELEMENT"]["IBLOCK_ID"] == 3)
               $arResult["SEARCH"][$arKey]["RIBBON"]["URL"] .= $arResult["SEARCH"][$arKey]["REAL_ELEMENT"]["IBLOCK_SECTION_ID"]."/";
         }
      }
   }
   
   $arResult["SEARCH"][$arKey]["BODY_FORMATED"] = html_entity_decode(trim($arResult["SEARCH"][$arKey]["BODY_FORMATED"]));
}

//sort
$arResult["SORT"]["RELEV"] = $APPLICATION->GetCurPageParam("sort=rank", array("PAGEN_2", "sort"));
$arResult["SORT"]["DATE"] = $APPLICATION->GetCurPageParam("sort=date", array("PAGEN_2", "sort"));
if($_REQUEST['sort'] == "date")
{
   $arResult["SORT"]["TYPE"] = "date";
   $arResult["SORT"]["DATE"] = "javascript:void(0)";
}
else
{
   $arResult["SORT"]["TYPE"] = "rank";
   $arResult["SORT"]["RELEV"] = "javascript:void(0)";
}