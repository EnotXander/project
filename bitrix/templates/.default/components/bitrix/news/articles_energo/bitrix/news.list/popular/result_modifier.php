<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die();
$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {

      $arElement["DETAIL_TEXT"] = strip_tags($arElement["DETAIL_TEXT"]);
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
      $arResult['ITEMS'][$key] = $arElement;
      
      if(is_array($arElement["PREVIEW_PICTURE"])) $arElement["DETAIL_PICTURE"] = $arElement["PREVIEW_PICTURE"];
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
            $arElement["DETAIL_PICTURE"],
            array("width" => 50, "height" => 50),
            BX_RESIZE_IMAGE_EXACT,
            true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
   }
}
?>