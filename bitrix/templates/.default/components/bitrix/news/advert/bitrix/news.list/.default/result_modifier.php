<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(count($arResult["ITEMS"]) == 0)
{
    $page = $APPLICATION->GetCurPage();
   /// header( 'Location: ' . $page );
}

global $USER;
//���������� ������������, �������� ������������ ���������
foreach($arResult["ITEMS"] as $key => &$arItem)
{
   $messageId = 0;
	$companyName = "";
   if($arItem["PROPERTIES"]["FIRM"]["VALUE"] > 0)
   {
      if(!CModule::IncludeModule("iblock"))die();
      $rsFirmUser = CIBlockElement::GetProperty(IBLOCK_COMPANY, $arItem["PROPERTIES"]["FIRM"]["VALUE"], $by="sort", $order="asc",  array("CODE" => "USER"));
      if($arFirmUser = $rsFirmUser->GetNext())
      {
	      $messageId = $arFirmUser["VALUE"];
      }
   }
   else
   {
      $messageId = $arItem["PROPERTIES"]["AUTHOR"]["VALUE"];
   }

	$arUserInfo = $USER->GetByID($arItem["PROPERTIES"]["AUTHOR"]["VALUE"])->Fetch();
	//get user company name
	$arOrder = array("SORT" => "ASC");
	$arFilter = array("PROPERTY_USER" => $arItem["PROPERTIES"]["AUTHOR"]["VALUE"], "PROPERTY_STATUS" => 65, "ACTIVE" => "Y", "IBLOCK_ID" => 49);
	$arSelectFields = array("ID", "IBLOCK_ID", "ACTIVE", "NAME", "PROPERTY_COMPANY");
	$rsElements = CIBlockElement::GetList($arOrder, $arFilter, FALSE, FALSE, $arSelectFields);
	if ($arElement = $rsElements->GetNext())
	{
		$arCompany = CIBlockElement::GetByID($arElement["PROPERTY_COMPANY_VALUE"])->Fetch();
		$companyName = $arCompany["NAME"];
	}

	//echo "<pre>"; print_r($arUserInfo); echo "</pre>";
	/*if (!empty($arUserInfo["UF_COMPANY"]) && $arUserInfo["UF_COMPANY"] > 0)
	{
		$res = CIBlockElement::GetByID($arUserInfo["UF_COMPANY"]);
		if($ar_res = $res->GetNext())
		{
			$companyName = $ar_res["NAME"];
		}
	}*/

	$arImg = CFile::ResizeImageGet($arUserInfo["PERSONAL_PHOTO"], array("width" => 75, "height" => 75), BX_RESIZE_IMAGE_EXACT);
	if ($arImg)	{
		$personalPhotoSrs = $arImg["src"];
	} else {
		$personalPhotoSrs = SITE_TEMPLATE_PATH."/img/nophoto.png";
	}

	$arImg = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array("width" => 72, "height" => 72), BX_RESIZE_IMAGE_EXACT);
	if ($arImg)	{
		$arItem["PREVIEW_PICTURE"]["SRC"] = $arImg["src"];
	}

	if (CUser::IsOnLine($arItem["PROPERTIES"]["AUTHOR"]["VALUE"], 600))	{
		$status = "������";
	} else {
		$status = "�������";
	}

	$arResult["ITEMS"][$key]["USER_INFO"] = array(
		"LOGIN" => substr($arUserInfo["LOGIN"], 0, strpos($arUserInfo["LOGIN"], '@')),
		"NAME" => $arUserInfo["NAME"],
		"LAST_NAME" => $arUserInfo["LAST_NAME"],
		"PERSONAL_PHOTO" => $personalPhotoSrs,
		"COMPANY_NAME" => $companyName,
		"STATUS" => $status
		);

   $arResult["ITEMS"][$key]['SEND_MESSAGE'] = array(
       "ID" => $messageId,
       //"MESSAGE" => "����� �� ���������� '{$arResult["NAME"]}' :"
   );
	if (strlen($arItem["NAME"]) > 60){
		$arItem["NAME"] = substr($arItem["NAME"], 0, 57)."...";
	}
}
if (is_array($arResult["SECTION"])){
	$arResult["NAME"] = '���������� � ������� "'.$arResult["SECTION"]["PATH"][0]["NAME"].'"';
}
if (isset($_GET["user"]) && !empty($_GET["user"])){
	//$arUserInfo = $USER->GetByID($_GET["user"])->Fetch();
	$login = substr($arUserInfo["LOGIN"], 0, strpos($arUserInfo["LOGIN"], '@'));
	$arResult["NAME"] = '���������� ������������ '.$login;
}

$arResult["MENU_TAGS"] = array(
    "TAG_BUY" => "N",
    "TAG_SALE" => "N",
    "TAG_SERVICES" => "N",
    "TAG_PREMIUM" => "N",
    "TAG_FAVORITES" => "N",
);



if(empty($arResult["SECTION"]))
{
    $arFilter = Array("IBLOCK_ID"=>IntVal(IBLOCK_ADVARE), "ACTIVE"=>"Y", "PROPERTY_TYPE_VALUE" => "�����");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), Array("ID", "NAME"));
    while($ob = $res->GetNextElement())
    {
        $arResult["MENU_TAGS"]["TAG_BUY"] = "Y";
    }

    $arFilter = Array("IBLOCK_ID"=>IntVal(IBLOCK_ADVARE), "ACTIVE"=>"Y", "PROPERTY_TYPE_VALUE" => "������");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), Array("ID", "NAME"));
    while($ob = $res->GetNextElement())
    {
        $arResult["MENU_TAGS"]["TAG_SALE"] = "Y";
    }

    $arFilter = Array("IBLOCK_ID"=>IntVal(IBLOCK_ADVARE), "ACTIVE"=>"Y", "PROPERTY_TYPE_VALUE" => "������");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), Array("ID", "NAME"));
    while($ob = $res->GetNextElement())
    {
        $arResult["MENU_TAGS"]["TAG_SERVICES"] = "Y";
    }


    $arFilter = Array("IBLOCK_ID"=>IntVal(IBLOCK_ADVARE), "ACTIVE"=>"Y", "PROPERTY_FAVORITES" => $USER->GetID());
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), Array("ID", "NAME"));
    while($ob = $res->GetNextElement())
    {
        $arResult["MENU_TAGS"]["TAG_FAVORITES"] = "Y";
    }
}
else
{
    $arFilter = Array("IBLOCK_ID"=>IntVal(IBLOCK_ADVARE), "SECTION_ID" => $arResult["SECTION"]["PATH"]["0"]["ID"], "ACTIVE"=>"Y", "PROPERTY_TYPE_VALUE" => "�����");

    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), Array("ID", "NAME"));
    if($ob = $res->GetNextElement())
    {
        $arResult["MENU_TAGS"]["TAG_BUY"] = "Y";
    }

    $arFilter = Array("IBLOCK_ID"=>IntVal(IBLOCK_ADVARE), "SECTION_ID" => $arResult["SECTION"]["PATH"]["0"]["ID"], "ACTIVE"=>"Y", "PROPERTY_TYPE_VALUE" => "������");

    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), Array("ID", "NAME"));
    if($ob = $res->GetNextElement())
    {
        $arResult["MENU_TAGS"]["TAG_SALE"] = "Y";
    }

    $arFilter = Array("IBLOCK_ID"=>IntVal(IBLOCK_ADVARE), "SECTION_ID" => $arResult["SECTION"]["PATH"]["0"]["ID"], "ACTIVE"=>"Y", "PROPERTY_TYPE_VALUE" => "������");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), Array("ID", "NAME"));
    if($ob = $res->GetNextElement())
    {
        $arResult["MENU_TAGS"]["TAG_SERVICES"] = "Y";
    }

    $arFilter = Array("IBLOCK_ID"=>IntVal(IBLOCK_ADVARE), "SECTION_ID" => $arResult["SECTION"]["PATH"]["0"]["ID"], "ACTIVE"=>"Y", "PROPERTY_PREMIUM_VALUE" => "��");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), Array("ID", "NAME"));
    if($ob = $res->GetNextElement())
    {
        $arResult["MENU_TAGS"]["TAG_PREMIUM"] = "Y";
    }

    $arFilter = Array("IBLOCK_ID"=>IntVal(IBLOCK_ADVARE), "SECTION_ID" => $arResult["SECTION"]["PATH"]["0"]["ID"], "ACTIVE"=>"Y", "PROPERTY_FAVORITES" => $USER->GetID());
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), Array("ID", "NAME"));
    if($ob = $res->GetNextElement())
    {
        $arResult["MENU_TAGS"]["TAG_FAVORITES"] = "Y";
    }

}

