$(document).ready(function() {
    $('.author-wrap > a').click(function(){
        if ($(this).hasClass('act')) {
            $(this).parent().find('div').fadeOut(200);
            $(this).removeClass('act');
        }
        else {
            $('.author-wrap a.act').parent().find('div').fadeOut(200);
            $('.author-wrap a.act').removeClass('act');
            $(this).parent().find('div').fadeIn(200);
            $(this).addClass('act');
        }
        return false;
    });
    $('.to-mark span').click(function(){
        $(this).toggleClass('act');
        return false;
    });
    $(document).click(function(event){
        if($(event.target).closest('.author-wrap > div').length)
            return;
        $('.author-wrap > div').fadeOut(200);
        $('.author-wrap a').removeClass('act');
        event.stopPropagation();
    });

    /// search
    var timeout;
    var req=false;
    $('#subSeachId_02').live('keyup', function(){

        var text = $(this).val();

        if(timeout) {  if (req != false) req.abort(); clearTimeout(timeout); }

        timeout = setTimeout(function() {
            showOverlay();

            req = $.get('/_ajax/ajax_advert.php', {
                       s: text, TYPE: $('#TYPE').val()
                   }, function(data){
                       $('#ajax_content').html(data);
                   }).always(function(){
                            HideOverlay();
                   });

        }, 800);



    });

});
function showOverlay(){


    var w = $('#ajax_content').width();
    var h = $('#ajax_content').height();
    var e = $('#ajax_content').offset();
    $('.overlay #img_bk').css('margin', '10px auto');
    $('#overlay').css('height', h).css('left', e.left).css('top', e.top).css('width', w).show();
}
function HideOverlay(){

    $('#overlay').hide();
}