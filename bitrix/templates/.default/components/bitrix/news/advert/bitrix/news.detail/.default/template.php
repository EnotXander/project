<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?global $USER;
$user = $USER->GetID();?>

<?
$APPLICATION->SetTitle($arResult["NAME"]." �� ������� ".$arParams['SITE_NAME']);
$descriptionPage = substr(strip_tags($arResult["DETAIL_TEXT"]), 0, 200);
$APPLICATION->SetPageProperty("description", $descriptionPage);
$APPLICATION->SetPageProperty("keywords", "������, �������, ��������, ��������, ".$arResult["NAME"].", ".$arResult["PROPERTIES"]["CITY"]["CITY_NAME"]);

function filesize2bytes($bytes, $precision = 2) {
   $units = array('�', '��', '��', '��', '��');

   $bytes = max($bytes, 0);
   $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
   $pow = min($pow, count($units) - 1);

   $bytes /= pow(1024, $pow);

   return round($bytes, $precision) . ' ' . $units[$pow];
}
?>

<script type="text/javascript">
   jQuery(function(){
      jQuery("a.lightbox").lightBox();
   });
</script>
<?
 //echo "<pre>"; print_r($arResult); echo "</pre>";
$sectId = $arResult['IBLOCK_SECTION_ID'];
$arTypes = Array(1760 => '<span class="icons bds_type icon_service"></span>', 1757 => '<span class="icons bds_type icon_buyer"></span>', 1755 => '<span class="icons bds_type icon_seller"></span>');
while ($sectId > 0)
{
   $forIcon = $sectId;
   $arSect = GetIblockSection($sectId);
   $sectId = $arSect['IBLOCK_SECTION_ID'];
}
switch ($arResult['PROPERTIES']['TYPE']['VALUE_XML_ID'])
{
	case 'sale':
		$sectionClass = 'sell';
		break;
	case 'buy':
		$sectionClass = 'buy';
		break;
	case 'services':
		$sectionClass = 'services';
		break;
	default:
		$sectionClass = '';
		break;
}
?>

<!-- ------------------------New template------------------------------ -->
	<div class="middle_col adv-inner with-bar detail_adv">
		<h1><?= $arResult["NAME"]; ?></h1>
		<div class="meta author-wrap">
			<span class="adv-type <?= $sectionClass; ?>"></span>
			<? if (empty($arResult["USER_INFO"]["COMPANY_NAME"])): ?>
				<a href="#"><?= $arResult["USER_INFO"]["NAME"] . " " . $arResult["USER_INFO"]['LAST_NAME']; ?></a>
				\ ���������� ���������  <?= $arResult["DISPLAY_ACTIVE_FROM"]; ?>. ����������: <?= $arResult["SHOW_COUNTER"]; ?>
			<? else: ?>
				<a href="#"><?= $arResult["USER_INFO"]['NAME'] . ", " . $arResult["USER_INFO"]["COMPANY_NAME"]; ?></a>
				\ ���������� ���������  <?= $arResult["DISPLAY_ACTIVE_FROM"]; ?>. ����������: <?= $arResult["SHOW_COUNTER"]; ?>
			<? endif; ?>
			<div style="display: none;">
				<span class="user-name"><?= $arResult['USER_INFO']['LOGIN']; ?></span>
				<span class="user-org"><?= $arResult['USER_INFO']['COMPANY_NAME']; ?></span>
				<table class="user-info">
					<tbody><tr>
						<td>
							<img src="<?= $arResult['USER_INFO']['PERSONAL_PHOTO']; ?>" alt="">
						</td>
						<td>
							<?/*<a href="#">�������</a>*/?>
							<? if ($USER->IsAuthorized()): ?>
								<a href="/personal/messages/<?= $arResult['SEND_MESSAGE']["ID"] ?>/?advert=<?= $arResult['ID'] ?>" target="_blank">��������� ���������</a>
							<? else: ?>
								<a href="#" onclick="alert('���������� ��������������'); return false;">��������� ���������</a>
							<? endif; ?>
						</td>
					</tr>
					</tbody>
				</table>
				<a href="/advert?user=<?= $arResult['SEND_MESSAGE']["ID"]; ?>" class="all-adv">��� ����������</a>
				<span class="status"><?= $arResult['USER_INFO']['STATUS']; ?></span>
			</div>
		</div>

		<table class="img-price">
			<tbody>
			<tr>
				<td>
					<div class="big-imgs">
						<? foreach($arResult["PROPERTIES"]["more_photo"]["BIG_SRC"] as $i => $src): ?>
							<div id="img<?= $i+1; ?>" class="img-wrap">
								<?if(($arResult["PROPERTIES"]["more_photo"]["ORIGINAL_SRC"][$i]["WIDTH"] > 400) || ($arResult["PROPERTIES"]["more_photo"]["ORIGINAL_SRC"][$i]["HEIGHT"] > 400)):?>
                           <a href="<?= $arResult["PROPERTIES"]["more_photo"]["ORIGINAL_SRC"][$i]["SRC"]; ?>" class="fancybox" rel="group"><img src="<?= $src["src"]; ?>" style="<?if($src["width"] < 400):?>margin-left: <?echo $width = (400 - $src["width"])/2?>px; <?endif?>"><div style="<?if($src["width"] < 400):?>margin-left: <?echo $width = (400 - $src["width"])/2?>px; <?endif?>">���������</div></a>
								<?else:?>
                           <img src="<?= $src["src"]; ?>" style="<?if($src["width"] < 400):?>margin-left: <?echo $width = (400 - $src["width"])/2?>px;<?endif?>">
                        <?endif;?>
							</div>
						<? endforeach; ?>
					</div>
				</td>
				<td>
					<span class="price <?= strlen(number_format(str_replace(" ","",$arResult["PROPERTIES"]["COST"]["VALUE"]), 0 , '.', ' ')) > 12 ? 'wide':''; ?>" >
                        <?if (!empty($arResult["PROPERTIES"]["COST"]["VALUE"])):?>
							<span style="float: left; margin-top: 5px;">����:</span>
							<span class="price_nums"><?= number_format(str_replace(" ","",$arResult["PROPERTIES"]["COST"]["VALUE"]), 0 , '.', ' '); ?></span>
							<span class="val">
								<?= $arResult["PROPERTIES"]["CURRENCY"]["VALUE"]; ?>
								<? if (!empty($arResult["PROPERTIES"]["UNITS"]["VALUE"])): ?>
									/ <?= $arResult["PROPERTIES"]["UNITS"]["VALUE"]; ?>
								<? endif; ?>
							</span>
                        <?else:?>
                            <span>���� ���������</span>
                        <?endif;?>
					</span>
				</td>
			</tr>
			</tbody>
		</table>

      <?if(count($arResult["PROPERTIES"]["more_photo"]["SMALL_SRC"])>1):?>
   		<div class="carousel">
   			<ul class="slides">
   				<? foreach($arResult["PROPERTIES"]["more_photo"]["SMALL_SRC"] as $i => $src): ?>
   					<li><a href="#" data-num="<?= $i+1; ?>">
   						<img src="<?= $src['src'];?>"  style="<?if($src["width"] < 88):?>margin-left: <?echo $width = (88 - $src["width"])/2?>px; <?endif?><?if($src["height"] < 64):?>margin-top: <?echo $height = (64 - $src["height"])/2?>px;<?endif?>">
   						</a>
   					</li>
   				<? endforeach; ?>
   			</ul>
   		</div>
      <?endif;?>

		<div class="nd_detailed_description">
			<h3>��������� ��������</h3>
			<?= $arResult["DETAIL_TEXT"]; ?>
		</div>
		<?
		$filesCount = count($arResult["PROPERTIES"]["LOAD"]["VALUE"]) - 1;
		?>
		<? if (is_array($arResult["PROPERTIES"]["LOAD"]["VALUE"]) && count($arResult["PROPERTIES"]["LOAD"]["VALUE"]) > 0): ?>
		<div class="nd_attached_files">
			<h3>��������� �����</h3>
			<ul class="nd_float-left">
				<? for($i = 0; $i<=(int)($filesCount/2); $i++): ?>
					<?

					switch ($arResult["PROPERTIES"]["LOAD"]["VALUE"][$i]["EXTENSION"])
					{
						case "xls":
							$css = "nd_xls";
							break;
						case "xlsx":
							$css = "nd_xlsx";
							break;
						case "doc":
							$css = "nd_doc";
							break;
						case "docx":
							$css = "nd_docx";
							break;
						case "pdf":
							$css = "nd_pdf";
							break;
						case "djvu":
							$css = "nd_djv";
							break;

						case "epub":
							$css = "nd_epub";
							break;
						case "gif":
							$css = "nd_gif";
							break;
						case "jpeg":
							$css = "nd_jpeg";
							break;
						case "jpg":
							$css = "nd_jpg";
							break;
						case "png":
							$css = "nd_png";
							break;
						case "ppt":
							$css = "nd_ppt";
							break;
						case "pptx":
							$css = "nd_ppt";
							break;
						case "rtf":
							$css = "nd_rtf";
							break;
						default:
							$css = "nd_file";
					}
					?>
					<li>
						<a class="<?= $css; ?>" href="<?= $arResult["PROPERTIES"]["LOAD"]["VALUE"][$i]["SRC"]; ?>" target="_blank">
							<?= $arResult["PROPERTIES"]["LOAD"]["VALUE"][$i]["ORIGINAL_NAME"]; ?>
							<span> \ <?= $arResult["PROPERTIES"]["LOAD"]["VALUE"][$i]["FILE_SIZE"]; ?> ��</span>
						</a>
					</li>
				<? endfor; ?>
			</ul><!--nd_float-left-->

			<ul class="nd_float-right">
				<? for($j = $i; $j<=$filesCount; $j++): ?>
					<?
					switch ($arResult["PROPERTIES"]["LOAD"]["VALUE"][$j]["EXTENSION"])
					{
						case "xls":
							$css = "nd_xls";
							break;
						case "xlsx":
							$css = "nd_xlsx";
							break;
						case "doc":
							$css = "nd_doc";
							break;
						case "docx":
							$css = "nd_docx";
							break;
						case "pdf":
							$css = "nd_pdf";
							break;
						case "djvu":
							$css = "nd_djv";
							break;

						case "epub":
							$css = "nd_epub";
							break;
						case "gif":
							$css = "nd_gif";
							break;
						case "jpeg":
							$css = "nd_jpeg";
							break;
						case "jpg":
							$css = "nd_jpg";
							break;
						case "png":
							$css = "nd_png";
							break;
						case "ppt":
							$css = "nd_ppt";
							break;
						case "pptx":
							$css = "nd_ppt";
							break;
						case "rtf":
							$css = "nd_rtf";
							break;
						default:
							$css = "nd_file";
					}
					?>
					<li>
						<a class="<?= $css; ?>" href="<?= $arResult["PROPERTIES"]["LOAD"]["VALUE"][$j]["SRC"]; ?>" target="_blank">
							<?= $arResult["PROPERTIES"]["LOAD"]["VALUE"][$j]["ORIGINAL_NAME"]." "; ?>
							<span>\ <?= $arResult["PROPERTIES"]["LOAD"]["VALUE"][$j]["FILE_SIZE"]; ?> ��</span>
						</a>
					</li>
				<? endfor; ?>
			</ul><!--nd_float-right-->
		</div>

		<? endif; ?>
        <br/>
        <div class="nd_attached_files">
            <?$APPLICATION->IncludeComponent(
				"bitrix:advertising.banner", 
				".default", 
				array(
					"TYPE" => "G2",
					"NOINDEX" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "0",
					"CACHE_NOTES" => ""
				),
				false
			);?>
		</div>
        <br/>

        <?if (count($arResult['pagess'])==3): ?>
            <a href="<?=$arResult['pagess'][0]?>" style="float:left">&lt;&lt;���������� ���������� </a>
            <a href="<?=$arResult['pagess'][2]?>" style="float:right">��������� ����������&gt;&gt;</a>
        <?elseif(count($arResult['pagess'])==2):?>
            <a href="<?=$arResult['pagess'][1]?>" style="float:right">��������� ����������&gt;&gt;</a>
        <?endif;?>
	</div>
	<div class="content_right_col">
		<div class="user-contacts">
			<span class="title">���������� ����������</span>
			<ul>
				<? if(count($arResult["PROPERTIES"]["pr1"]["VALUE"]) > 0): ?>
					<? foreach ($arResult["PROPERTIES"]["pr1"]["VALUE"] as $phone): ?>
						<?
						if ( $phone[0] != '+')
							$phone = '+375'.$phone;
						?>
						<li class="nd_phone">
							<strong><?= phone($phone); ?></strong>
						</li>
					<? endforeach; ?>
					<? if ($arResult["PROPERTIES"]["CONTACT"]["VALUE"] != '') { ?>
					<li class="nd_person">
						<?= $arResult["PROPERTIES"]["CONTACT"]["VALUE"]; ?>
					</li>
					<? } ?>
				<? endif; ?>
				<? if(!empty($arResult["PROPERTIES"]["SKYPE"]["VALUE"])): ?>
					<li class="nd_skype_n"><?= $arResult["PROPERTIES"]["SKYPE"]["VALUE"]; ?></li>
				<? endif; ?>
				<? if(!empty($arResult["PROPERTIES"]["ADDRESS"]["VALUE"])): ?>
					<li class="nd_address"><?= $arResult["PROPERTIES"]["ADDRESS"]["VALUE"]; ?></li>
				<? endif; ?>
				
				<? if($arResult['SEND_MESSAGE']["ID"] != $user): ?>
					<li><a href="/advert?user=<?= $arResult['SEND_MESSAGE']["ID"];?>">��� ���������� ������</a></li>
				<? else: ?>	
					<li><a href="/advert?user=<?= $arResult['SEND_MESSAGE']["ID"];?>">���� ����������</a></li>
				<? endif; ?>
			</ul>
			<? if ($USER->IsAuthorized() && false ): ?>
				<? if($arResult['SEND_MESSAGE']["ID"] != $user): ?>
					<a class="nd_contact_user" href="/personal/messages/<?= $arResult['SEND_MESSAGE']["ID"] ?>/?advert=<?= $arResult['ID'] ?>" target="_blank">��������� � �������������</a>
				<? else: ?>
					<a class="nd_edit_user_advert" href="/personal/advert/admin.php?PRODUCT_ID=<?= $arResult['ID'] ?>">������������� ����������</a>
				<? endif; ?>
			<? else: ?>
				<a class="nd_contact_user" href="#" onclick="alert('���������� ��������������'); return false;">��������� � �������������</a>
			<? endif; ?>
			<? if($arResult['SEND_MESSAGE']["ID"] != $user): ?>
				<div class="nd_forget">�� �������� ��������  ����������, ��� ����� ��� �� <strong><?= $arParams['SITE_NAME'] ?></strong></div>
			<? endif; ?>
		</div>

			<div class="sw_rc_block">
				<div class="sw_right_banner">
					<?$APPLICATION->IncludeComponent(
						"bitrix:advertising.banner", 
						".default", 
						array(
							"TYPE" => "G1_4",
							"NOINDEX" => "N",
							"CACHE_TYPE" => "N",
							"CACHE_TIME" => "3600",
							"CACHE_NOTES" => ""
						),
						false
					);?>
				</div>
				<div class="sw_right_banner">
					<?$APPLICATION->IncludeComponent(
						"bitrix:advertising.banner", 
						".default", 
						array(
							"TYPE" => "G1_3",
							"NOINDEX" => "N",
							"CACHE_TYPE" => "N",
							"CACHE_TIME" => "3600",
							"CACHE_NOTES" => ""
						),
						false
					);?>
				</div>
				<div class="sw_right_banner">
					<?$APPLICATION->IncludeComponent(
						"bitrix:advertising.banner", 
						".default", 
						array(
							"TYPE" => "G1_2",
							"NOINDEX" => "N",
							"CACHE_TYPE" => "N",
							"CACHE_TIME" => "3600",
							"CACHE_NOTES" => ""
						),
						false
					);?>
				</div>
				<div class="sw_right_banner">
					<?$APPLICATION->IncludeComponent(
						"bitrix:advertising.banner", 
						".default", 
						array(
							"TYPE" => "G1_1",
							"NOINDEX" => "N",
							"CACHE_TYPE" => "N",
							"CACHE_TIME" => "3600",
							"CACHE_NOTES" => ""
						),
						false
					);?>
				</div>
			</div>

<div id="yandex_ad"></div>
<script type="text/javascript">
(function(w, d, n, s, t) {
    w[n] = w[n] || [];
    w[n].push(function() {
Ya.Direct.insertInto(window.location.hostname=='agrobelarus.by'?135492:135481, "yandex_ad", {
            ad_format: "direct",
            type: "adaptive",
            border_type: "block",
            limit: 2,
            title_font_size: 3,
            border_radius: true,
            links_underline: true,
            site_bg_color: "FFFFFF",
            border_color: "CCCCCC",
            title_color: "266BAB",
            url_color: "000099",
            text_color: "000000",
            hover_color: "0B9DF1",
            sitelinks_color: "0000CC",
            favicon: true,
            no_sitelinks: false,
            height: 300,
            width: 300
        });
    });
    t = d.getElementsByTagName("script")[0];
    s = d.createElement("script");
    s.src = "//an.yandex.ru/system/context.js";
    s.type = "text/javascript";
    s.async = true;
    t.parentNode.insertBefore(s, t);
})(window, document, "yandex_context_callbacks");
</script>
		</div>

<!-- ----------------------------End----------------------------------- -->