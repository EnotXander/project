<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$rs = CIBlockElement::GetList(
    array("active_from" => "desc"),
    array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
        array(
            "LOGIC" => "OR",
            array("PROPERTY_ACTIVE" => "Y"),
            array("PROPERTY_ACTIVE" => false)
        ),
    ),
    false,
    array("nElementID" => $arResult["ID"], "nPageSize" => 1),
    array("DETAIL_PAGE_URL"));
while($ar=$rs->GetNext())
{
    $arResult['pagess'][] = $ar["DETAIL_PAGE_URL"];
}
//print_r($page);

/*$arResult["MORE_PHOTO_SMALL"] = Array();
if (is_array($arResult["MORE_PHOTO"]))
{
   foreach ($arResult["MORE_PHOTO"] as $key => $photo)
   {
      if (is_array($photo))
      {
         $arFileTmp = CFile::ResizeImageGet(
                         $photo, array("width" => 75, "height" => 75), BX_RESIZE_IMAGE_EXACT, true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);

         $arResult["MORE_PHOTO_SMALL"][$key] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
   }
}
if (is_array($arResult["PREVIEW_PICTURE"]))
{
   $arFileTmp = CFile::ResizeImageGet(
                   $arResult["PREVIEW_PICTURE"], array("width" => 160, "height" => 250), BX_RESIZE_IMAGE_EXACT, true
   );
   //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);

   $arResult["PREVIEW_PICTURE"] = array(
       "SRC" => $arFileTmp["src"],
       "WIDTH" => $arFileTmp["width"],
       "HEIGHT" => $arFileTmp["height"],
   );
}*/
$arResult['PREMIUM'] = $arPremium;
$arResult['ITEMS'] = $arItems;

//���������� ������������, �������� ������������ ���������
if($arResult["PROPERTIES"]["FIRM"]["VALUE"] > 0)
{
   //�������� ���������� � ������������� ��������
   if(ENABLE_PREDSTAVITEL_MODE)
   {
      $arPredstavitel = PredstavitelGetByCompany($arResult["PROPERTIES"]["FIRM"]["VALUE"]);
      foreach($arPredstavitel["LIST"] as $key => $arEmployee)
      {
         if($arEmployee["STATUS"] == 65)//�������
         {
            $messageId = $arEmployee["USER"];
            break;
         }
      }
   }
   else
   {
      if(!CModule::IncludeModule("iblock"))die();
      $rsFirmUser = CIBlockElement::GetProperty(17, $arResult["PROPERTIES"]["FIRM"]["VALUE"], $by="sort", $order="asc",  array("CODE" => "USER"));
      if($arFirmUser = $rsFirmUser->GetNext())
         $messageId = $arFirmUser["VALUE"];
   }
}
else
{
   $messageId = $arResult["PROPERTIES"]["AUTHOR"]["VALUE"];
   $arResult["AUTHOR"] = $USER->GetByID($messageId)->Fetch();
   if(is_array($arResult["AUTHOR"]))
   {
      if($arResult["AUTHOR"]["UF_COUNTRY"])
         $arResult["AUTHOR"]["UF_COUNTRY"] = CIBlockElement::GetByID($arResult["AUTHOR"]["UF_COUNTRY"])->GetNext();
      if($arResult["AUTHOR"]["UF_REGION"])
         $arResult["AUTHOR"]["UF_REGION"] = CIBlockElement::GetByID($arResult["AUTHOR"]["UF_REGION"])->GetNext();
      if($arResult["AUTHOR"]["UF_CITY"])
         $arResult["AUTHOR"]["UF_CITY"] = CIBlockElement::GetByID($arResult["AUTHOR"]["UF_CITY"])->GetNext();
   }
}

$arUserInfo = $USER->GetByID($arResult["PROPERTIES"]["AUTHOR"]["VALUE"])->Fetch();
$arOrder = array("SORT" => "ASC");
$arFilter = array("PROPERTY_USER" => $arResult["PROPERTIES"]["AUTHOR"]["VALUE"], "PROPERTY_STATUS" => 65, "ACTIVE" => "Y", "IBLOCK_ID" => 49);
$arSelectFields = array("ID", "IBLOCK_ID", "ACTIVE", "NAME", "PROPERTY_COMPANY");
$rsElements = CIBlockElement::GetList($arOrder, $arFilter, FALSE, FALSE, $arSelectFields);
if ($arElement = $rsElements->GetNext())
{
	$arCompany = CIBlockElement::GetByID($arElement["PROPERTY_COMPANY_VALUE"])->Fetch();
	$companyName = $arCompany["NAME"];
}
//echo "<pre>"; print_r($arUserInfo); echo "</pre>";
/*if (!empty($arUserInfo["UF_COMPANY"]) && $arUserInfo["UF_COMPANY"] > 0)
{
	$res = CIBlockElement::GetByID($arUserInfo["UF_COMPANY"]);
	if($ar_res = $res->GetNext())
	{
		$companyName = $ar_res["NAME"];
	}
}*/

$arImg = CFile::ResizeImageGet($arUserInfo["PERSONAL_PHOTO"], array("width" => 75, "height" => 75), BX_RESIZE_IMAGE_EXACT);
if ($arImg)	{
	$personalPhotoSrs = $arImg["src"];
} else {
	$personalPhotoSrs = SITE_TEMPLATE_PATH."/img/nophoto.png";
}

if (CUser::IsOnLine($arItem["PROPERTIES"]["AUTHOR"]["VALUE"], 600))	{
	$status = "������";
} else {
	$status = "�������";
}

$arResult["USER_INFO"] = array(
	"LOGIN" => substr($arUserInfo["LOGIN"], 0, strpos($arUserInfo["LOGIN"], '@')),
	"NAME" => $arUserInfo["NAME"],
	"LAST_NAME" => $arUserInfo["LAST_NAME"],
	"PERSONAL_PHOTO" => $personalPhotoSrs,
	"COMPANY_NAME" => $companyName,
	"STATUS" => $status
);

$arResult['SEND_MESSAGE'] = array(
	"ID" => $messageId,
	//"MESSAGE" => "����� �� ���������� '{$arResult["NAME"]}' :"
);

foreach ($arResult["PROPERTIES"]["more_photo"]["VALUE"] as $i => $imgId)
{
	if ($imgId > 0)
	{
		$arImg = CFile::ResizeImageGet($imgId, array('width' => 400, 'height' => 280), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$arResult["PROPERTIES"]["more_photo"]["BIG_SRC"][] = $arImg;
		$arImg = CFile::ResizeImageGet($imgId, array('width' => 88, 'height' => 64), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$arResult["PROPERTIES"]["more_photo"]["SMALL_SRC"][] = $arImg;
		$arResult["PROPERTIES"]["more_photo"]["ORIGINAL_SRC"][] = CFile::GetFileArray($imgId);
	}
}

foreach ($arResult["PROPERTIES"]["LOAD"]["VALUE"] as $i => &$file)
{
	$file = CFile::GetFileArray($file);
	$file["EXTENSION"] = end(explode(".", $file["FILE_NAME"]));
	$file["FILE_SIZE"] = (int)($file["FILE_SIZE"] / 1024);
}

$arCurCity = CIBlockElement::GetByID($arResult["PROPERTIES"]["CITY"]["VALUE"])->Fetch();
$arResult["PROPERTIES"]["CITY"]["CITY_NAME"] = $arCurCity["NAME"];

if (empty($arResult["SHOW_COUNTER"]))
{
   $arResult["SHOW_COUNTER"] = 0;
}