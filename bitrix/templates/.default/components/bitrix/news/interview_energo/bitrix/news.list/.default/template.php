<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="mc_block">
   <h1>��������</h1>
   <? $arItem = $arResult["ITEMS"][0]; ?>
   <div class="main_news">
      <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["DETAIL_PICTURE"])): ?>
         <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="imgBox">
               <img border="0" src="<?= $arItem["DETAIL_PICTURE_BIG"]["SRC"] ?>" width="<?= $arItem["DETAIL_PICTURE_BIG"]["WIDTH"] ?>" height="<?= $arItem["DETAIL_PICTURE_BIG"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"/>
            </a>
         <? else: ?>
            <img border="0" src="<?= $arItem["DETAIL_PICTURE_BIG"]["SRC"] ?>" width="<?= $arItem["DETAIL_PICTURE_BIG"]["WIDTH"] ?>" height="<?= $arItem["DETAIL_PICTURE_BIG"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"/>
         <? endif; ?>
      <? endif ?>
      <div class="name">
         <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="font_18"><? echo $arItem["NAME"] ?></a>
      </div>
      <p>
         <span class="date">
            <b><? echo $arItem["DISPLAY_PROPERTIES"]['FIO']['DISPLAY_VALUE']; ?></b>
         </span>
         <br/>
         <span class="date"><? echo strip_tags($arItem["DISPLAY_PROPERTIES"]['WORK']['DISPLAY_VALUE']); ?></span>
      </p>
      <p><? echo $arItem["PREVIEW_TEXT"]; ?><span class="date">\  <? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span></p>
      <? foreach ($arItem["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
         <? if ($pid == "AUTHOR"): ?>
            <div class="meta a_autor">
               <?= $arProperty["NAME"] ?>: <?= $arProperty["USER"]["LAST_NAME"] . " " . $arProperty["USER"]['NAME']; ?>
            </div>
         <? endif; ?>
      <? endforeach; ?>
      <?$arTags = explode(", ", $arItem['TAGS']);
      foreach ($arTags as $key => $tag)
         $arTags[$key] = "<a href='/articles/search/?tags=" . urlencode($tag) . "'>" . $tag . "</a>";?>
      <? if (strlen($arItem['TAGS']) > 0): ?>
         <div class="meta a_tags">����: <? echo implode(", ", $arTags); ?></div>
      <? endif; ?>
   </div>
   <div class="clear"></div>
</div><!--mc_block--><? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
<div class="paging">
   <div class="show_col">
      <form method="get" name="top_f">
         <span class="meta">���������� ��:</span>
         <select name="per_page" onchange="document.top_f.submit()">
            <option value="15" <? if ($arParams['NEWS_COUNT'] == 15) echo "selected"; ?>>15</option>
            <option value="25" <? if ($arParams['NEWS_COUNT'] == 25) echo "selected"; ?>>25</option>
            <option value="50" <? if ($arParams['NEWS_COUNT'] == 50) echo "selected"; ?>>50</option>
         </select>
      </form>
   </div>

      <?= $arResult["NAV_STRING"] ?><br />

</div><? endif; ?>
<div class="mc_block">
   <div class="news_list">
      <? $nomer = 1; ?>
      <? foreach ($arResult["ITEMS"] as $arItem): ?>
         <? if ($nomer != 1): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="item_row" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
               <div class="image_box">
                  <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["DETAIL_PICTURE"])): ?>
                     <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img class="preview_picture" border="0" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arItem["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" style="float:left" /></a>
                     <? else: ?>
                        <img class="preview_picture" border="0" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arItem["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" style="float:left" />
                     <? endif; ?>
                  <? endif ?>
               </div>
               <div class="row">
                  <div class="title">
                     <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                  </div>
                  <p><span class="date" style="padding-left:0px;"><b><?= $arItem["DISPLAY_PROPERTIES"]['FIO']['DISPLAY_VALUE']; ?></b><?=(strlen($arItem["DISPLAY_PROPERTIES"]['FIO']['DISPLAY_VALUE']) && strlen($arItem["DISPLAY_PROPERTIES"]['WORK']['DISPLAY_VALUE'])) ? ' \ ' : ''?><?= strip_tags($arItem["DISPLAY_PROPERTIES"]['WORK']['DISPLAY_VALUE']); ?></span></p>
                  <p><? echo $arItem["PREVIEW_TEXT"]; ?><span class="date">\ <? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span></p>
                  <? foreach ($arItem["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
                     <? if ($pid == "AUTHOR"): ?>
                        <div class="meta a_autor"><?= $arProperty["NAME"] ?>: <?= $arProperty["USER"]["LAST_NAME"] . " " . $arProperty["USER"]['NAME']; ?></div>
                     <? endif; ?>
                  <? endforeach; ?>
                  <?
                  $arTags = explode(", ", $arItem['TAGS']);
                  foreach ($arTags as $key => $tag)
                     $arTags[$key] = "<a href='/articles/search/?tags=" . urlencode($tag) . "'>" . $tag . "</a>";?>
                  <? if (strlen($arItem['TAGS']) > 0): ?><div class="meta a_tags">����: <? echo implode(", ", $arTags); ?></div><? endif; ?>
               </div>
            </div><!--item_row-->
         <? endif; ?>
      <? $nomer++; ?>
      <? endforeach; ?>
   </div>
</div>
<div class="paging mar_20_bot">
   <div class="show_col">
      <form method="get" name="bot_f">
         <span class="meta">���������� ��:</span>
         <select name="per_page" onchange="document.bot_f.submit()">
            <option value="15" <? if ($arParams['NEWS_COUNT'] == 15) echo "selected"; ?>>15</option>
            <option value="25" <? if ($arParams['NEWS_COUNT'] == 25) echo "selected"; ?>>25</option>
            <option value="50" <? if ($arParams['NEWS_COUNT'] == 50) echo "selected"; ?>>50</option>
         </select>
      </form>
   </div>
   <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
      <?= $arResult["NAV_STRING"] ?>
   <? endif; ?>
</div>
<? //echo "<pre>"; print_r($arResult); echo "</pre>"; ?>
