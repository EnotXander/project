<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<? if (count($arResult["ITEMS"]) > 0): ?>
   <div class="rc_block brdr_box">
      <div class="wrap">
         <div class="title">
            <a href="/news/" title="<?= $arParams['TITLE'] ?>"><?= $arParams['TITLE'] ?></a>
         </div>
         <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <div class="faces_row">
               <div class="image_box">
                  <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                     <img border="0" src="<?= $arItem["THUMB"]["SRC"] ?>" width="<?= $arItem["THUMB"]["WIDTH"] ?>" height="<?= $arItem["THUMB"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"/>
                  </a>
               </div>
               <div class="row">
                  <div class="name">
                     <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a> <span class="date">\ <? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
                  </div>
               </div>
            </div><!--faces_row-->
         <? endforeach; ?>
      </div>
   </div><!--rc_block-->
<?endif?>
