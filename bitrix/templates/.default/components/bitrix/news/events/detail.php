<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="left_col">
   <!--lc_block-->

   <?
   $APPLICATION->IncludeComponent("bitrix:news.list", "left_grey", array(
       "IBLOCK_TYPE" => "information_part",
       "IBLOCK_ID" => IBLOCK_NEWS,
       "NEWS_COUNT" => "3",
       "TITLE" => "�� ����������!",
       "SORT_BY1" => "shows",
       "SORT_ORDER1" => "DESC",
       "SORT_BY2" => "NAME",
       "SORT_ORDER2" => "DESC",
       "FILTER_NAME" => "",
       "FIELD_CODE" => array("DETAIL_PICTURE"),
       "PROPERTY_CODE" => array(
           0 => "",
           1 => "",
       ),
       "CHECK_DATES" => "N",
       "DETAIL_URL" => "",
       "AJAX_MODE" => "N",
       "AJAX_OPTION_JUMP" => "N",
       "AJAX_OPTION_STYLE" => "Y",
       "AJAX_OPTION_HISTORY" => "N",
       "CACHE_TYPE" => "A",
       "CACHE_TIME" => "3600",
       "CACHE_FILTER" => "N",
       "CACHE_GROUPS" => "N",
       "PREVIEW_TRUNCATE_LEN" => "100",
       "ACTIVE_DATE_FORMAT" => "d-m-Y",
       "SET_TITLE" => "N",
       "SET_STATUS_404" => "N",
       "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
       "ADD_SECTIONS_CHAIN" => "N",
       "HIDE_LINK_WHEN_NO_DETAIL" => "N",
       "PARENT_SECTION" => LANG == 's1'?"6":"3198",
       "PARENT_SECTION_CODE" => "",
       "DISPLAY_TOP_PAGER" => "N",
       "DISPLAY_BOTTOM_PAGER" => "N",
       "PAGER_TITLE" => "",
       "PAGER_SHOW_ALWAYS" => "N",
       "PAGER_TEMPLATE" => "",
       "PAGER_DESC_NUMBERING" => "N",
       "PAGER_DESC_NUMBERING_CACHE_TIME" => "",
       "PAGER_SHOW_ALL" => "N",
       "AJAX_OPTION_ADDITIONAL" => ""
           ), false
   );
   ?>

   <?
   $APPLICATION->IncludeComponent("bitrix:news.list", "left_grey", array(
       "IBLOCK_TYPE" => "information_part",
       "IBLOCK_ID" => IBLOCK_INTERVIEW,
       "NEWS_COUNT" => "3",
       "TITLE" => "��������",
       "SORT_BY1" => "ACTIVE_FROM",
       "SORT_ORDER1" => "DESC",
       "SORT_BY2" => "NAME",
       "SORT_ORDER2" => "DESC",
       "FILTER_NAME" => "",
       "FIELD_CODE" => array("DETAIL_PICTURE"),
       "PROPERTY_CODE" => array(
           0 => "",
           1 => "",
       ),
       "CHECK_DATES" => "N",
       "DETAIL_URL" => "",
       "AJAX_MODE" => "N",
       "AJAX_OPTION_JUMP" => "N",
       "AJAX_OPTION_STYLE" => "Y",
       "AJAX_OPTION_HISTORY" => "N",
       "CACHE_TYPE" => "N",
       "CACHE_TIME" => "3600",
       "CACHE_FILTER" => "N",
       "CACHE_GROUPS" => "N",
       "PREVIEW_TRUNCATE_LEN" => "100",
       "ACTIVE_DATE_FORMAT" => "d-m-Y",
       "SET_TITLE" => "N",
       "SET_STATUS_404" => "N",
       "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
       "ADD_SECTIONS_CHAIN" => "N",
       "HIDE_LINK_WHEN_NO_DETAIL" => "N",
       "PARENT_SECTION" => "",
       "PARENT_SECTION_CODE" => "",
       "DISPLAY_TOP_PAGER" => "N",
       "DISPLAY_BOTTOM_PAGER" => "N",
       "PAGER_TITLE" => "",
       "PAGER_SHOW_ALWAYS" => "N",
       "PAGER_TEMPLATE" => "",
       "PAGER_DESC_NUMBERING" => "N",
       "PAGER_DESC_NUMBERING_CACHE_TIME" => "",
       "PAGER_SHOW_ALL" => "N",
       "AJAX_OPTION_ADDITIONAL" => ""
           ), false
   );
   ?>

   <?
   $APPLICATION->IncludeComponent("bitrix:news.list", "left_grey", array(
       "IBLOCK_TYPE" => "information_part",
       "IBLOCK_ID" => IBLOCK_ARTICLE,
       "NEWS_COUNT" => "3",
       "TITLE" => "������ � ���������",
       "SORT_BY1" => "ACTIVE_FROM",
       "SORT_ORDER1" => "DESC",
       "SORT_BY2" => "NAME",
       "SORT_ORDER2" => "DESC",
       "FILTER_NAME" => "",
       "FIELD_CODE" => array("DETAIL_PICTURE"),
       "PROPERTY_CODE" => array(
           0 => "",
           1 => "",
       ),
       "CHECK_DATES" => "N",
       "DETAIL_URL" => "",
       "AJAX_MODE" => "N",
       "AJAX_OPTION_JUMP" => "N",
       "AJAX_OPTION_STYLE" => "Y",
       "AJAX_OPTION_HISTORY" => "N",
       "CACHE_TYPE" => "A",
       "CACHE_TIME" => "3600",
       "CACHE_FILTER" => "N",
       "CACHE_GROUPS" => "N",
       "PREVIEW_TRUNCATE_LEN" => "100",
       "ACTIVE_DATE_FORMAT" => "d-m-Y",
       "SET_TITLE" => "N",
       "SET_STATUS_404" => "N",
       "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
       "ADD_SECTIONS_CHAIN" => "N",
       "HIDE_LINK_WHEN_NO_DETAIL" => "N",
       "PARENT_SECTION" => "",
       "PARENT_SECTION_CODE" => "",
       "DISPLAY_TOP_PAGER" => "N",
       "DISPLAY_BOTTOM_PAGER" => "N",
       "PAGER_TITLE" => "",
       "PAGER_SHOW_ALWAYS" => "N",
       "PAGER_TEMPLATE" => "",
       "PAGER_DESC_NUMBERING" => "N",
       "PAGER_DESC_NUMBERING_CACHE_TIME" => "",
       "PAGER_SHOW_ALL" => "N",
       "AJAX_OPTION_ADDITIONAL" => ""
           ), false
   );
   ?>

   <div class="lc_block">
      <div class="left_banner_185">
         <?
         $APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
             "TYPE" => "COMPANY",
             "NOINDEX" => "N",
             "CACHE_TYPE" => "A",
             "CACHE_TIME" => "0",
             "CACHE_NOTES" => ""
                 )
         );
         ?>
      </div>
   </div><!--lc_block-->
</div>
<div class="middle_col">
   <div class="breadcrumbs">
      <?
      $APPLICATION->IncludeComponent("bitrix:breadcrumb", "news", array(
          "START_FROM" => "0",
          "PATH" => "",
          "SITE_ID" => "s1"
              ), false, array()
      );
      ?>
   </div>
</div>
<div class="middle_col">
   <div class="mc_block">
      <?
      $ElementID = $APPLICATION->IncludeComponent("bitrix:news.detail", "", Array(
          "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
          "DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
          "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
          "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
          "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
          "IBLOCK_ID" => $arParams["IBLOCK_ID"],
          "FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
          "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
          "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
          "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
          "META_KEYWORDS" => $arParams["META_KEYWORDS"],
          "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
          "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
          "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
          "SET_TITLE" => $arParams["SET_TITLE"],
          "SET_STATUS_404" => $arParams["SET_STATUS_404"],
          "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
          "ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
          "ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
          "CACHE_TYPE" => $arParams["CACHE_TYPE"],
          "CACHE_TIME" => $arParams["CACHE_TIME"],
          "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
          "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
          "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
          "DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
          "DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
          "PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
          "PAGER_SHOW_ALWAYS" => "N",
          "PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
          "PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
          "CHECK_DATES" => $arParams["CHECK_DATES"],
          "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
          "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
          "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
          "USE_SHARE" => $arParams["USE_SHARE"],
          "SHARE_HIDE" => $arParams["SHARE_HIDE"],
          "SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
          "SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
          "SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
          "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
              ), $component
      );
      ?>
      <p><a href="<?= $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"] ?>"><?= GetMessage("T_NEWS_DETAIL_BACK") ?></a></p>
      <? if ($arParams["USE_RATING"] == "Y" && $ElementID): ?>
         <?
         $APPLICATION->IncludeComponent("bitrix:iblock.vote", "ajax", Array(
             "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
             "IBLOCK_ID" => $arParams["IBLOCK_ID"],
             "ELEMENT_ID" => $ElementID,
             "MAX_VOTE" => $arParams["MAX_VOTE"],
             "VOTE_NAMES" => $arParams["VOTE_NAMES"],
             "CACHE_TYPE" => $arParams["CACHE_TYPE"],
             "CACHE_TIME" => $arParams["CACHE_TIME"],
                 ), $component
         );
         ?>
      <? endif ?>
      <?
      if ($arParams["USE_CATEGORIES"] == "Y" && $ElementID):
         global $arCategoryFilter;
         $obCache = new CPHPCache;
         $strCacheID = $componentPath . LANG . $arParams["IBLOCK_ID"] . $ElementID . $arParams["CATEGORY_CODE"];
         if (($tzOffset = CTimeZone::GetOffset()) <> 0)
            $strCacheID .= "_" . $tzOffset;
         if ($arParams["CACHE_TYPE"] == "N" || $arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "N")
            $CACHE_TIME = 0;
         else
            $CACHE_TIME = $arParams["CACHE_TIME"];
         if ($obCache->StartDataCache($CACHE_TIME, $strCacheID, $componentPath))
         {
            $rsProperties = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $ElementID, "sort", "asc", array("ACTIVE" => "Y", "CODE" => $arParams["CATEGORY_CODE"]));
            $arCategoryFilter = array();
            while ($arProperty = $rsProperties->Fetch())
            {
               if (is_array($arProperty["VALUE"]) && count($arProperty["VALUE"]) > 0)
               {
                  foreach ($arProperty["VALUE"] as $value)
                     $arCategoryFilter[$value] = true;
               } elseif (!is_array($arProperty["VALUE"]) && strlen($arProperty["VALUE"]) > 0)
                  $arCategoryFilter[$arProperty["VALUE"]] = true;
            }
            $obCache->EndDataCache($arCategoryFilter);
         }
         else
         {
            $arCategoryFilter = $obCache->GetVars();
         }
         if (count($arCategoryFilter) > 0):
            $arCategoryFilter = array(
                "PROPERTY_" . $arParams["CATEGORY_CODE"] => array_keys($arCategoryFilter),
                "!" . "ID" => $ElementID,
            );
            ?>
            <hr />
            <h3><?= GetMessage("CATEGORIES") ?></h3>
            <? foreach ($arParams["CATEGORY_IBLOCK"] as $iblock_id): ?>
               <?
               $APPLICATION->IncludeComponent("bitrix:news.list", $arParams["CATEGORY_THEME_" . $iblock_id], Array(
                   "IBLOCK_ID" => $iblock_id,
                   "NEWS_COUNT" => $arParams["CATEGORY_ITEMS_COUNT"],
                   "SET_TITLE" => "N",
                   "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                   "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                   "CACHE_TIME" => $arParams["CACHE_TIME"],
                   "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                   "FILTER_NAME" => "arCategoryFilter",
                   "CACHE_FILTER" => "Y",
                   "DISPLAY_TOP_PAGER" => "N",
                   "DISPLAY_BOTTOM_PAGER" => "N",
                       ), $component
               );
               ?>
            <? endforeach ?>
         <? endif ?>
      <? endif ?>
      <? if ($arParams["USE_REVIEW"] == "Y" && IsModuleInstalled("forum") && $ElementID): ?>
         <?
         $APPLICATION->IncludeComponent("bitrix:forum.topic.reviews", "", Array(
             "CACHE_TYPE" => $arParams["CACHE_TYPE"],
             "CACHE_TIME" => $arParams["CACHE_TIME"],
             "MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
             "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
             "PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
             "FORUM_ID" => $arParams["FORUM_ID"],
             "URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
             "SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
             "ELEMENT_ID" => $ElementID,
             "AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
             "IBLOCK_ID" => $arParams["IBLOCK_ID"],
             "POST_FIRST_MESSAGE" => $arParams["POST_FIRST_MESSAGE"],
             "URL_TEMPLATES_DETAIL" => $arParams["POST_FIRST_MESSAGE"] === "Y" ? $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"] : "",
                 ), $component
         );
         ?>
      <? endif ?>
   </div>
</div>
