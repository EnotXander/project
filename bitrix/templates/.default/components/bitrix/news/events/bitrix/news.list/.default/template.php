<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

?>
<div class="mc_block">

   <ul class="mb_menu" style="float: left; width:300px">
      <li class="<?= $_GET['old'] != 'true' ? 'sel' :'' ?>">
         <a href="/events/">����������</a>
         <div class="sclew"></div>
      </li>
      <li class="<?= $_GET['old'] != 'true' ? '' :'sel' ?>">
         <a href="/events/?old=true" >����� �������</a>
         <div class="sclew"></div>
      </li>
   </ul>
   <? if ($_GET['old'] == 'true') :?>
   <ul class="mb_menu" style=" float: left; width: 300px">

         <li<? if ($arParams['PARENT_SECTION'] <= 0): ?> class="sel"<? endif; ?>>
            <a href="/events/?old=<?=  $_GET['old'] ?>">��� ����</a>
            <div class="sclew"></div>
         </li>
         <?foreach($arResult["EVENT_TYPES"] as $type):?>
            <li<?= $type["SELECTED"] ? ' class="sel"' : ''?>>
               <a href="/events/<?= $type['ID']; ?>/?old=<?=  $_GET['old'] ?>"><?= $type['NAME'] ?></a>
               <div class="sclew"></div>
            </li>
         <?endforeach;?>

   </ul>
   <? endif; ?>
   <div class="clear"></div>

   <table class="event_list" style="width:100%;">
      <tr  id="collap">
         <th class="date_ev">
            ����
         </th>
         <th class="name_ev">
            �������� �����������
         </th>
         <th class="info_ev">
            ����� ����������
         </th>
         <th class="place_ev">
            ����� ����������
         </th>
         <th class="org_ev">
            �����������
         </th>
      </tr>
      <?
      $wt = array('1'=>'��','2'=>'��','3'=>'��','4'=>'��','5'=>'��','6'=>'��','7'=>'��',);
      ?>
      <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
         <?
         $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
         $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
         ?>
         <tr <? if ($arItem['DISPLAY_PROPERTIES']['PREMIUM']): ?> class="premium"<? endif; ?>>
            <td class="date_ev">
               <? echo date( 'd.m.Y', strtotime($arItem["DISPLAY_ACTIVE_FROM"])) ,' ', $wt[date('N', strtotime($arItem["DISPLAY_ACTIVE_FROM"]))] ?> <br/>-<br/>
               <? echo date( 'd.m.Y', strtotime($arItem["DATE_ACTIVE_TO"])) ,' ', $wt[date('N', strtotime($arItem["DATE_ACTIVE_TO"]))]  ?>
            </td>
            <td class="name_ev">
               <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                  <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                     <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" style="float:left" />
                     </a>
                  <? else: ?>
                     <img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" style="float:left" />
                  <? endif; ?>
               <? endif ?>
               <div class="tdWrap">
                  <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" style="font-size:14px;"><b><? echo $arItem["NAME"] ?></b></a>					
               </div>
            </td>
            <td class="info_ev">
               <div class="tdWrap">
                  <div class="bds_text"><?= $arItem["PREVIEW_TEXT"]; ?> <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">���������</a></div>
               </div>                      
            </td>
            <td class="place_ev">
               <div class="tdWrap">
                 <b><? echo $arItem['PROPERTIES']['REGION']['VALUE']; ?>,
                  <? echo $arItem['PROPERTIES']['CITY']['VALUE']; ?><br/></b><br/>
                  <? echo $arItem['PROPERTIES']['PLACE']['VALUE']; ?>
               </div>                      
            </td>
            <td class="org_ev">
               <div class="tdWrap">
                  <? foreach ($arItem['DISPLAY_PROPERTIES'] as $pid => $arProperty): ?>
                     <? if ($pid <> "PREMIUM"): ?>
                        <? if (is_array($arProperty["DISPLAY_VALUE"])): ?>
                           <?= implode("<br/>", $arProperty["DISPLAY_VALUE"]); ?>
                        <? else: ?>
                           <?= $arProperty['FILE'];?>
                           <br/>
                           <?= $arProperty["DISPLAY_VALUE"] == '' ? $arProperty["VALUE"] : $arProperty["DISPLAY_VALUE"] ?>
                        <? endif ?>
                     <? endif; ?>

                  <? endforeach; ?>
                  <? if ( !empty($arItem['PROPERTIES']['ORG_TXT']['VALUE']) ) {?>
                     <?= $arItem['PROPERTIES']['ORG_TXT']['VALUE']  ?>
                  <? } ?>
                  <? if ( !empty($arItem['PROPERTIES']['ORG_LINK']['VALUE'])  ) { ?>
                     <a href="<?= $arItem['PROPERTIES']['ORG_LINK']['VALUE'] ?>">���������</a>
                  <? } ?>
	               <?
	               //PrintAdmin($arProperty);
	               ?>
               </div>                      
            </td>
         </tr>
      <? endforeach; ?>
   </table>
   <div class="paging brdr_top_0 brdr_box mar_20_bot">

      <div class="show_col">
         <form method="get" name="bot_f">
            <span class="meta">���������� ��:</span>
            <input type="hidden" name="SECTION_ID" value="<?= $_REQUEST['SECTION_ID'] ?>">
            <select name="per_page" onchange="document.bot_f.submit()">
               <option value="15" <? if ($arParams["NEWS_COUNT"] == 15) echo "selected"; ?>>15</option>
               <option value="25" <? if ($arParams["NEWS_COUNT"] == 25) echo "selected"; ?>>25</option>
               <option value="50" <? if ($arParams["NEWS_COUNT"] == 50) echo "selected"; ?>>50</option>
            </select>
         </form>
      </div>
      <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
         <?= $arResult["NAV_STRING"] ?>
      <? endif; ?>
   </div>
</div>
<? //PrintAdmin($arResult);