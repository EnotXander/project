<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("iblock")) die();


;
// REGION
$rsItems = CIBlockElement::GetList(Array(),
    array('IBLOCK_ID'=>$arParams['IBLOCK_ID']),
    array('PROPERTY_REGION')
    );
while($arItem = $rsItems->GetNext())
{
   //PrintAdmin($arItem);
}

//получение видов мероприятий
$rsItems = GetIBlockSectionList($arResult['ID'], 0, Array("sort" => "asc"));
while($arItem = $rsItems->GetNext())
{
   $elem = array("ID" => $arItem['ID'], "NAME" => $arItem['NAME'], "SELECTED" => false);
   if ($arItem['ID'] == $arParams['PARENT_SECTION'])
      $elem["SELECTED"] = true;
   $arResult["EVENT_TYPES"][] = $elem;
}

//получение стран
$arResult["COUNTRIES"] = array();
$rsItems = GetIBlockElementList(26, 0, Array("SORT" => "ASC", "NAME" => "ASC"));
while($arItem = $rsItems->GetNext())
{
   $elem = array("ID" => $arItem['ID'], "NAME" => $arItem['NAME'], "SELECTED" => false);
   if ($arItem['ID'] == $_REQUEST["country"])
      $elem["SELECTED"] = true;
   $arResult["COUNTRIES"][] = $elem;
}

//получение свойств
foreach($arResult["ITEMS"] as $key => $arItem)
{
   foreach($arItem['DISPLAY_PROPERTIES'] as $pid => $arProperty)
   {
      if (($pid <> "PREMIUM") && (!is_array($arProperty["DISPLAY_VALUE"])))
      {
         $arComp = GetIblockElement($arProperty['VALUE']);
         $arResult["ITEMS"][$key]['PROPERTIES'][$pid]['FILE'] = CFile::ShowImage($arComp['PREVIEW_PICTURE'], 100, 100);
      }
      if ($pid == 'REGION'  && is_int($arResult["ITEMS"][$key]['PROPERTIES'][$pid]['VALUE'])>0 )
      {

         $arResult["ITEMS"][$key]['PROPERTIES'][$pid]['VALUE'] = en_getName($arResult["ITEMS"][$key]['PROPERTIES'][$pid]['VALUE']);
      }
      if ($pid == 'CITY' && is_int($arResult["ITEMS"][$key]['PROPERTIES'][$pid]['VALUE'])>0)
      {

         $arResult["ITEMS"][$key]['PROPERTIES'][$pid]['VALUE'] = en_getName($arResult["ITEMS"][$key]['PROPERTIES'][$pid]['VALUE']);
      }
   }



}
