<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;
$arPremium = Array();
$arItems = Array();

if (is_array($arResult["ITEMS"]))
{
	foreach ($arResult["ITEMS"] as $key => $arElement) 
	{
		if(is_array($arElement["PREVIEW_PICTURE"]))
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arElement["PREVIEW_PICTURE"],
				array("width" => 75, "height" => 75),
				BX_RESIZE_IMAGE_EXACT,
				true
			);
			//$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);

			$arElement["PREVIEW_IMG"] = array(
				"SRC" => $arFileTmp["src"],
				"WIDTH" => $arFileTmp["width"],
				"HEIGHT" => $arFileTmp["height"],
			);

		}		
		if ($key < 3)
			$arPremium[] = $arElement;
		else
			$arItems[] = $arElement;
	}
}

$arResult['PREMIUM'] = $arPremium;
$arResult['ITEMS'] = $arItems;
?>