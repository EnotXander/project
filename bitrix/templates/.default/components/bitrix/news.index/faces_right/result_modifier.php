<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$obParser = new CTextParser;

//сортировка
$arSortList = array();
foreach($arResult['IBLOCKS'] as $iBlockIndex => $arIBlock)
{
   foreach($arIBlock['ITEMS'] as $elementIndex => $arElement)
   {
      $arSortList[BXToTimestamp($arElement['ACTIVE_FROM'])] = array(
          'IBLOCK_INDEX' => $iBlockIndex,
          'ELEMENT_INDEX' => $elementIndex,
          'ID' => $arElement['ID'],
          'NAME' => $arElement['NAME'],
      );
   }
}
krsort($arSortList);
$arResult["ITEMS"] = array();
$count = 0;
foreach($arSortList as $arElement)
{
   $arResult["ITEMS"][] = $arResult['IBLOCKS'][$arElement['IBLOCK_INDEX']]['ITEMS'][$arElement['ELEMENT_INDEX']];
   if($arResult['IBLOCKS'][$arElement['IBLOCK_INDEX']]['ITEMS'][$arElement['ELEMENT_INDEX']]['ID'] !== $arElement['ID']) PrintAdmin ('Несоответствие!');
   $count++;
   if($count >= $arParams['NEWS_COUNT']) break;
}

//PrintAdmin($arResult["ITEMS"]);

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      $arElement["DETAIL_TEXT"] = strip_tags($arElement["DETAIL_TEXT"]);
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], 100);
      switch($arElement['IBLOCK_ID'])
      {
         case 29://interview
            $arElement["DETAIL_PAGE_URL"] = "/interview/{$arElement["CODE"]}/";
            break;
         case 33://persons
            $arElement["DETAIL_PAGE_URL"] = "/persons/{$arElement["ID"]}/";
            break;
      }
      $arResult['ITEMS'][$key] = $arElement;

      if (is_array($arElement["PREVIEW_PICTURE"]))
         $arElement["DETAIL_PICTURE"] = $arElement["PREVIEW_PICTURE"];
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
                 $arElement["DETAIL_PICTURE"],
                 array("width" => 50, "height" => 50),
                 BX_RESIZE_IMAGE_EXACT,
                 true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);

         $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
   }
}
?>