<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="wrap faces_box">
   <div class="title">
	   <div class="mb_title_o">
		   <a href="/interview/" id="nodec">��������</a>
	   </div>
   </div>
   <? foreach ($arResult["ITEMS"] as $arItem): ?>
      <div class="faces_row">
         <div class="image_box">
	         <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt=""></a>
         </div>
         <div class="row">
            <div class="name"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem['NAME'] ?></a></div>
            <? if (strlen($arItem['DISPLAY_PROPERTIES']['WORK']['DISPLAY_VALUE'])): ?>
               <div class="post"><?= strip_tags($arItem['DISPLAY_PROPERTIES']['WORK']['DISPLAY_VALUE']) ?></div>
            <? endif; ?>
            <div class="company"><?= (strlen($arItem['DISPLAY_PROPERTIES']['FIRM']['DISPLAY_VALUE']) > 0) ? $arItem['DISPLAY_PROPERTIES']['FIRM']['DISPLAY_VALUE'] : $arItem['DISPLAY_PROPERTIES']['FIRM_N']['DISPLAY_VALUE'] ?></div>
         </div>
      </div><!--faces_row-->
   <? endforeach; ?>
</div>
