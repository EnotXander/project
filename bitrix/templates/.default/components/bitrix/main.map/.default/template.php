<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!is_array($arResult["arMap"]) || count($arResult["arMap"]) < 1)
	return;

$arRootNode = Array();
foreach($arResult["arMap"] as $index => $arItem)
{
	if ($arItem["LEVEL"] == 0)
		$arRootNode[] = $index;
}

$allNum = count($arRootNode);
$colNum = ceil($allNum / $arParams["COL_NUM"]);
$tree = [];
$lef = '';
$prevlevel = 0;
$key = '';
$root='';
$new_arr=array();

foreach ($arResult["arMap"] as $arItem) {

	if ($arItem["LEVEL"] == 0) {
		$new_arr[$arItem["ID"]] = $arItem;
		$parent = $arItem["ID"];
	}elseif ($arItem["LEVEL"] == 1) {
		$new_arr[$parent]["ITEMS"] [$arItem["ID"]] = $arItem;
		$second_parent = $arItem["ID"];
	}
	else if ($arItem["LEVEL"] == 2) {
		$new_arr[$parent]["ITEMS"] [$second_parent]["ITEMS"] [$arItem["ID"]] = $arItem;
		$t_parent = $arItem["ID"];
	}
	else if ($arItem["LEVEL"] == 3) {
		$new_arr[$parent]["ITEMS"] [$second_parent]["ITEMS"] [$t_parent]["ITEMS"] [$arItem["ID"]] = $arItem;
	}
}

?>
<div class="middle_col">
	<ul class="level-map">
		<h1 style="    margin-left: -20px;
		">����� �����</h1>
		<?
		foreach ($new_arr as $arItem){
			?>
				<li ><a href="#<?= $arItem['NAME'] ?>"><?= $arItem['NAME'] ?></a></li>
			<?
		}
		?>
	</ul>
<div><br style="clear: left"/></div>
       <ul class="map-level-0">
       <?
       //PrintAdmin($new_arr);
            showChild($new_arr);
       ?>

       </ul>

</div>
<?
function showChild($items){
	foreach ($items as $arItem){
		?><li class="level<?=  $arItem["LEVEL"] ?>"><?
		if ($arItem['LEVEL'] == 0) {
			?>
			 <ul class="mb_menu" style="margin-bottom: 10px; border-bottom: 2px solid red;">
	            <li class="sel">
	                <a style="font-size:20px" name="<?=$arItem["NAME"]?>" href="<?=$arItem["FULL_PATH"]?>"><?=$arItem["NAME"]?></a>
	                <div class="sclew"></div>
	            </li>
	         </ul>
			<?
		} else {
			?>
				<a href="<?=$arItem["FULL_PATH"]?>"><?=$arItem["NAME"]?></a>
			<?
		}
		if ( isset($arItem["ITEMS"]) ){
			?><ul class="map-level-<?=$arItem["LEVEL"]+1?>"><?
			   showChild($arItem["ITEMS"]);
			?></ul><br style="clear: left"/><?
		}
		?></li><?
	}
}