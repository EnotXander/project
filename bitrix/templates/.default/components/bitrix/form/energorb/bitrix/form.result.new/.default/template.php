<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="page-feedback">
<?if($_REQUEST['formresult'] == 'addok'){?>
	<p style="color: green; font-size: 18px;">�������! ���� ������ �������.<br/><br/><br/></p>
<?}?>
<?if ($arResult["isFormErrors"] == "Y"):?>
	<?=$arResult["FORM_ERRORS_TEXT"];?>

<?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y"){?>
<?=$arResult["FORM_HEADER"]?>

<table class="form_header">
<?
if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
{
?>
	<tr>
		<td><?
/***********************************************************************************
					form header
***********************************************************************************/
if ($arResult["isFormTitle"])
{
?>
	<h1><?=$arResult["FORM_TITLE"]?></h1>
<?
} //endif ;

	if ($arResult["isFormImage"] == "Y")
	{
	?>
	<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
	<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
	<?
	} //endif
	?>

			<p><?=$arResult["FORM_DESCRIPTION"]?></p>
			<p>����, ���������� <span class="form-required"><?=$arResult["REQUIRED_SIGN"]?></span>, ����������� ��� ����������. </p>
		</td>
	</tr>
	<?
} // endif
	?>
</table>
<br />
<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>

<div class="feedback-note">
	<h3>�������� ��������!</h3>
	<p>�������� ����� ��������� ��������� �������� ���������� � ������ �������. ��� ��������� � ����������� ������������� � �������� ����� ���������� ������ �����������, ������� ������� � ������� ����� ��������. ����� ����� �������� ���������� ������������ �������� ��������������� �����, ������� �� ��������� ��������� ���� �����. �� ��� � ���� ������ ������, ������� ���� ����� � ������ �������.</p>
</div>

<table class="form-table data-table">
	<!-- <thead>
		<tr>
			<th colspan="3">�</th>
		</tr>
	</thead> -->
	<tbody>
	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'){
			echo $arQuestion["HTML_CODE"];
		}
		else
		{
	?>
		<tr>
			<td class="lefttd">
				<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
				<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
				<?endif;?>
				<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
				<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
			</td>
			<td class="righttd"<?if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea'):?> colspan="2"<?endif;?>>
				<?=$arQuestion["HTML_CODE"]?>
			</td>
			<?if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] != 'textarea'):?>
				<td class="commenttd">
					<?if (
						$arResult['arQuestions'][$FIELD_SID]['COMMENTS'] &&
						stripos($arResult['arQuestions'][$FIELD_SID]['COMMENTS'], 'separator') === false
					):?>
						<div class="field-comment"><?=$arResult['arQuestions'][$FIELD_SID]['COMMENTS']?></div>
					<?endif;?>
				</td>
			<?endif;?>
		</tr>
		
		<?if (stripos($arResult['arQuestions'][$FIELD_SID]['COMMENTS'], 'separator') !== false):?>
			<tr class="withth"><th colspan="3"></th></tr>
		<?endif;?>
	<?
		}
	} //endwhile
	?>
<?if($arResult["isUseCaptcha"] == "Y") {?>
		<tr class="withth"><th colspan="3"></th></tr>
		<tr>
			<td class="lefttd">�</td>
			<td class="righttd"><input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" /></td>
			<td></td>
		</tr>
		<tr>
			<td class="lefttd"><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></td>
			<td class="righttd"><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" /></td>
			<td></td>
		</tr>
<?} // isUseCaptcha ?>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="3">
				<!--<input class="button_green" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=htmlspecialchars(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
				-->
				�<input type="hidden" name="web_form_apply" value="Y" />
				      <input class="button_green" type="submit" name="web_form_apply" value="���������" />

				<!-- �<input class="button_green" type="reset" value="<?=GetMessage("FORM_RESET");?>" />-->
			</th>
		</tr>
	</tfoot>
</table>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>
</div>