<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if ($arParams["SHOW_INPUT"] !== "N"): ?>
   <div class="search_selectors js-search-selectors">
      <a href="javascript:void(0)" rel="all"<?=($arResult["SELECTED_SECTION"] == "all") ? ' class="sel"' : ""?>>��� �������</a><a href="javascript:void(0)" rel="tov"<?=($arResult["SELECTED_SECTION"] == "tov") ? ' class="sel"' : ""?>>������</a><a href="javascript:void(0)" rel="com"<?=($arResult["SELECTED_SECTION"] == "com") ? ' class="sel"' : ""?>>��������</a>
      <!-- <a href="javascript:void(0)" rel="tend"<?=($arResult["SELECTED_SECTION"] == "tend") ? ' class="sel"' : ""?>>�������</a>-->
      <a href="javascript:void(0)" rel="objav"<?=($arResult["SELECTED_SECTION"] == "objav") ? ' class="sel"' : ""?>>����������</a><a href="javascript:void(0)" rel="news"<?=($arResult["SELECTED_SECTION"] == "news") ? ' class="sel"' : ""?>>�������</a>
      <!--<a href="javascript:void(0)" rel="art"<?=($arResult["SELECTED_SECTION"] == "art") ? ' class="sel"' : ""?>>���������</a>-->
      <!--<a href="javascript:void(0)" rel="meropr"<?=($arResult["SELECTED_SECTION"] == "meropr") ? ' class="sel"' : ""?>>�����������</a>-->
      <!--<a href="javascript:void(0)" rel="term"<?=($arResult["SELECTED_SECTION"] == "term") ? ' class="sel"' : ""?>>����������</a>-->
   </div>
   <div class="search_form" id="<? echo $CONTAINER_ID ?>">
      <form action="<? echo $arResult["FORM_ACTION"] ?>">
         <input type="hidden" class="js-input-iblock" name="w" value="<?=$arResult["SELECTED_SECTION"]?>">
         <div class="search_form_suggest">
            <div class="input_box js-input-box">
                <a name="s" class="serm" onclick="javascript:$(this).closest('form').submit()">�����</a>
                <div class="search_preloader"></div>
               <div class="row">
                   <input class="top_search_input notUsed js-input-search" type="text" name="q" value="" placeholder="������� ����� ��� ������" size="40" maxlength="500" autocomplete="off" />
               </div>
            </div>
         </div>
      </form>
   </div>
<? endif ?>
<script>
   $(document).ready(function(){
      //������������ ��������
      $(document).on("click", '.js-search-selectors a', function(){
         $('.js-search-selectors a').removeClass("sel");
         $('.search_form .js-input-iblock').val($(this).attr("rel"));
         $(this).addClass("sel");
         if($('.js-input-search').val().length)
            $('.js-input-search').trigger("keydown");
      });
      //��������� ����� ������
      $(document).on("focus", '.js-input-search', function(event){
         //console.log("ddd", $(this).val().length);
         $(this).removeClass("notUsed");
      })
      //���� �� ������ ����������
      $(document).on("click", '.as-result-item', function(){
         location.href = $(this).find('.search_row .title').attr("href");
      })
      // ��������� ��� ������
      $(".js-input-box").autoSuggest("/_ajax/search.php", {
         minChars: 3,
         keyDelay: 700,
         resultsHighlight: false,
         selectionAdded: function(data){ 
            //location.href = data.link;
            //console.log("selectionAdded", data);
         },
         formatList: function(data, elem){   
            //console.log("formatList", data, elem);
            
            if((data.ribbon_class) && (data.ribbon_class.length) && (!data.is_section)){
               var $news_section = $("<div>", {class: "news_section "+data.ribbon_class}).html("<p>"+data.ribbon_title+"</p>");
               var $news_section_link = $("<a href='"+data.ribbon_url+"'></a>").html($news_section);
               var $inline = $("<div>", {class: "inline"}).append($news_section_link);
            }else{
               var $inline = $("<span>");
            }
            var $date = $("<span>", {class: "date"}).html(data.date);
            
            if(data.is_section){
               var $descr_left = $("<div>", {class: "descr_left"}).append($("<ul>", {class: ""}));
               var $descr_right = $("<div>", {class: "descr_right"}).append($("<ul>", {class: ""}));
               for(var index in data.sub){
                  var $subsect_span = $("<span>", {class: "descr_subsect_span"}).html(data.sub[index].name);
                  var $subsect_gradient = $("<div>", {class: "title_gradient"});
                  var $subsect = $("<a>", {class: "descr_subsect", href: data.sub[index].url}).append($subsect_span).append($subsect_gradient);
                  var $subsect_wrap = $("<li>", {class: "", type: "disc"}).append($subsect);
                  if(index < 2){
                     $descr_left.find('ul').append($subsect_wrap);
                  }else{
                     $descr_right.find('ul').append($subsect_wrap);
                  }
               }
               var $descr = $("<div>", {class: "descr", style: "height: 45px;"}).append($descr_left).append($descr_right);
            }else{
               var $more = $("<a>", {href: data.link, style: "margin-left: 7px; text-decoration: none !important;"}).html("�����...");
               if (data.type.length>0)
                 var $descr =  $("<div>", {class: "descr"}).append(data.type).append($more);
              else
                 var $descr =  $("<div>", {class: "descr"}).append(data.type);
            }
            var $gradient = $("<div>", {class: "title_gradient"});
            var $title = $("<a>", {class: "title", href: data.link}).html("<span class='title_fullwidth'>"+data.name+"</span>").prepend($gradient);
            var $row = $("<div>", {class: "row"}).append($title).append($descr).append($inline).append($date);
            //***
            if(!data.image_margin) $row.css("margin-left", "72px");
            var $image = '';
            if (data.image.length > 0)
               $image = $("<img>", {src: data.image});
            var $image_box = $("<a>", {class: "image_box", href: data.link, style: "margin-top: "+(75-data.image_h)/2+"px;"}).append($image);
            var $search_row = $("<div>", {class: "search_row"}).append($image_box).append($row);
            if(data.firm)
            {
               var firm = $("<a>", {class: "image_box", href: data.firm.LINK, title: data.firm.LOGO.DESCRIPTION, style: "float: right; margin-top:"+(75-data.firm.LOGO.HEIGHT)/2+"px;"}).append($("<img>", {src: data.firm.LOGO.SRC}));
               $search_row.prepend(firm);
            }
            if(data.is_section)
               $search_row.addClass("row_section");
            
            return elem.append($search_row);
         }
      });
   });
</script>