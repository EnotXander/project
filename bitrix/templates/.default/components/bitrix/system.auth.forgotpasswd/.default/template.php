<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="bx-auth">
   <form id="forgotpass-form" name="bform" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
      <?
      if (strlen($arResult["BACKURL"]) > 0)
      {
         ?>
         <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
         <?
      }
      ?>
      <input type="hidden" name="AUTH_FORM" value="Y">
      <input type="hidden" name="TYPE" value="SEND_PWD">

      <table class="data-table bx-forgotpass-table bx-registration-table">
         <thead>
            <tr> 
               <td colspan="3" style="color:red;"><b><?= GetMessage("AUTH_FORGOT_PASSWORD_1") ?></b></td>
            </tr>
            <tr> 
               <td colspan="3"><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></td>
            </tr>
         </thead>
         <tbody class="bodyground">
            <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
            <tr class="auth-last"> 
               <td><?= GetMessage("AUTH_EMAIL") ?></td>
               <td>
                  <input id="forgotpass-form-email" type="text" name="USER_EMAIL" maxlength="255" />
                  <div id="forgotpass-errortext-email" class="errortext"><?= $arResult["VALID"]["EMAIL"] ?></div>
               </td>
               <td class="bordered">
                  <div>
                     <p><?= GetMessage("AUTH_MESSAGE") ?></p>
                  </div>
               </td>
               <td rowspan="3" class="register-cell-info" style="padding: 11px 15px 11px 15px !important;">
                  <b>��� ������������ ������?</b><br />
                  ���� ������� ����� ������� ��� �� ��� ����� ����������� �����.
               </td>
            </tr>
            <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
         </tbody>
         <tfoot>
            <tr> 
               <td colspan="2">
                  <?if(is_array($arResult["STATUS"]) && count($arResult["STATUS"])):?>
                     <b style="color: <?=$arResult["STATUS"]["TYPE"] ? '#009933' : 'red'?>; font-size: 14px;"><?=$arResult["STATUS"]["TEXT"]?></b>
                  <?endif;?>
               </td>
               <td>
                  <input type="submit" style="display: none;" name="send_account_info" value="<?= GetMessage("AUTH_SEND") ?>" />
                  <a class="btns_big" style="float: right;" href="javascript:void(0);" onclick="$('#forgotpass-form').submit();"><i></i>������� ������</a>
               </td>
            </tr>
         </tfoot>
      </table>
   </form>
</div>