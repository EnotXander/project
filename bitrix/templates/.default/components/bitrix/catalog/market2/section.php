<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
Check404SectionPage($component);
?>
<div class="middle_col">
   <div class="mc_block">
      <div class="bread_market">
         <ul class="market_bread">
            <li class="">
					<a href="/">�������</a>
				</li>
				<?//������ ����� (�������� ������)?>
				<li class="marketf">
					<div class="submarket haschild">
						<?if ($arParams["IBLOCK_ID"] == $arParams["IBLOCK_PRODUCT_ID"]):?>
								<a href="/market/" class="submarket-a"><?= $arParams["PAGE_NAME"]?></a>
							<?elseif($arParams["IBLOCK_ID"] == $arParams["IBLOCK_USLUGI_ID"]):?>
								<a href="/services/" class="submarket-a">������</a>
							<?endif;?>
						<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "bread", Array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $arSect["ID"],
							"SECTION_CODE" => $arSect["CODE"],
							"DISPLAY_PANEL" => "N",
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							//"CACHE_TYPE" => 'N',
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
							'TOP_DEPTH' => 1,
							'NAME_CUT' => 50,
							'CURRENT_SECTION' => $arResult["VARIABLES"]["SECTION_ID"]
							), $component
						);?>
					</div>
				</li>
            <?
            if(!CModule::IncludeModule("iblock")) die();
            
            if(strlen($arResult["VARIABLES"]["SECTION_CODE"])){
               $resChain = CIBlockSection::GetList(
	               array(),
	               array("=CODE" => $arResult["VARIABLES"]["SECTION_CODE"], "IBLOCK_ID" => $arParams["IBLOCK_ID"])
               );
               if ($elementChain = $resChain->GetNext()) 
                  $arResult["VARIABLES"]["SECTION_ID"] = $elementChain["ID"];
            }
   
            $nav = CIBlockSection::GetNavChain(false, $arResult["VARIABLES"]["SECTION_ID"]);
            ?>
				<?while ($arSect = $nav->GetNext()):?>
					<?
					$subsectCount = CIBlockSection::GetCount(array(
						'IBLOCK_ID' => $arParams["IBLOCK_ID"],
						'SECTION_ID' => $arSect["ID"]
					));
					$subsectCountClass = $subsectCount > 0 ? ' haschild' : '';
					?>
                    <?
                       // /market/litiy_ionnye_elementy_i_sborki/
                       $path = parse_url($APPLICATION->GetCurDir(), PHP_URL_PATH);
                       ?>
               <li class="marketf">
                  <div class="submarket<?=$subsectCountClass?>">
                      <? if ($path == $arSect["SECTION_PAGE_URL"] && !isset($_GET['CURPAGE'])) {?>
                        <span class="submarket-a"><?=$arSect["NAME"]?></span>
                       <? } else { ?>
                           <a href="<?=$arSect['SECTION_PAGE_URL']?>" class="submarket-a"><?=$arSect['NAME']?></a>
                       <? } ?>
                     <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "bread", Array(
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"SECTION_ID" => $arSect["ID"],
								"SECTION_CODE" => $arSect["CODE"],
								"DISPLAY_PANEL" => "N",
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								//"CACHE_TYPE" => 'N',
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
								'TOP_DEPTH' => 1,
								'NAME_CUT' => 50,
								'CURRENT_SECTION' => $arResult["VARIABLES"]["SECTION_ID"]
								), $component
                     );?>
                  </div>
               </li>
            <?endwhile;?>
         </ul>
      </div>
   </div>


   <? if($arParams["USE_FILTER"] == "Y"): ?>
      <?
      $APPLICATION->IncludeComponent("bitrix:catalog.filter", "", Array(
          "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
          "IBLOCK_ID" => $arParams["IBLOCK_ID"],
          "FILTER_NAME" => $arParams["FILTER_NAME"],
          "FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
          "PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
          "PRICE_CODE" => $arParams["FILTER_PRICE_CODE"],
          "OFFERS_FIELD_CODE" => $arParams["FILTER_OFFERS_FIELD_CODE"],
          "OFFERS_PROPERTY_CODE" => $arParams["FILTER_OFFERS_PROPERTY_CODE"],
          "CACHE_TYPE" => $arParams["CACHE_TYPE"],
          "CACHE_TIME" => $arParams["CACHE_TIME"],
          "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
              ), $component
      );
      ?>
      <br />
   <? endif ?>
   <?
   global $arrrFilter;
   if ($_REQUEST['FAVOR'] == 'Y')
      $arrrFilter['ID'] = $_SESSION['FAVOR' . $arParams['IBLOCK_ID']]['ITEMS'];
   if ((int)$_REQUEST['FIRM'] > 0)
      $arrrFilter['PROPERTY_FIRM'] = (int)$_REQUEST['FIRM'];
   if ((int)$_REQUEST['per_page'] > 0)
      $perPage = (int)$_REQUEST['per_page'];
   else
      $perPage = "20";
   ?>
   <?//���������� � ID ���� ��������� �������
   global $USER;
   $arFavTovars = array();
   if($USER->IsAuthorized())
   {
      $resFav = CIBlockElement::GetList(
              array(),
              array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_FAVORITES" => $USER->GetID())
         );
      while($elementFav = $resFav->GetNext()) 
      {
         $arFavTovars[] = $elementFav["ID"];
      }
   }
   ?>
   <?
   if($_REQUEST["FAVORITES"] == "Y") $template = "favorites";
   else $template = "";
   $APPLICATION->IncludeComponent("whipstudio:catalog.multisection", $template, Array(
       "FAVORITES_ARRAY" => $arFavTovars,
       "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
       "IBLOCK_ID" => $arParams["IBLOCK_ID"],
       "INCLUDE_SUBLISTS" => true,//���������� �� ��������� ������ ��� ������� ����������
       //"FILTER_NAME" => "arrrFilter", //$arParams["FILTER_NAME"],
       "CACHE_TYPE" => $arParams["CACHE_TYPE"],
       "CACHE_TIME" => $arParams["CACHE_TIME"],
       "CACHE_FILTER" => $arParams["CACHE_FILTER"],
       "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
       "PAGE_ELEMENT_COUNT" => $perPage,
       "COMPANY_ELEMENT_COUNT" => 3,
       "COMPANY_ELEMENT_MORE_COUNT" => 20,
       "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
       "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
       "PAGER_TITLE" => $arParams["PAGER_TITLE"],
       "SHOW_GOODS" => 'N',
       "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
       "PAGER_TEMPLATE" => "market",//$arParams["PAGER_TEMPLATE"],
       "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
       "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
       "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
       "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
       "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
       "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
       "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
       "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
       "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
       "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
       "COMPANY_NAME_CUT" => 15,
       "DETAIL_TEXT_CUT" => 155,
       "SORT" => array(
            "company" => array(
                "NAME" => "��������",
                "TYPE" => "desc",
                "CODE" => "PROPERTY_FIRM.PROPERTY_EXT_SORT"
            ),
            "price" => array(
                "NAME" => "����",
                "TYPE" => "asc,nulls",
                "CODE" => "PROPERTY_COST"
            ),
            "date" => array(
                "NAME" => "����",
                "TYPE" => "desc",
                "CODE" => "DATE_CREATE"
            ),
            "rate" => array(
                "NAME" => "�������",
                "TYPE" => "asc",
                "CODE" => "shows"
            )
        ),
       "SORT_DEFAULT" => "company",
       "SHOW_SEO_TEXT" => "Y",
       "SHOW_RUBRIKATOR" => "N"
           ), $component
   );
   ?><div class="clear"></div>
</div>