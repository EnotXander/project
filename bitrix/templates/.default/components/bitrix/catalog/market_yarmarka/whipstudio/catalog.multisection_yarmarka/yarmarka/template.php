<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $GBL_SHOW_FAST_ORDER;
$GBL_SHOW_FAST_ORDER = true;
?>
<?foreach ($arResult["LISTS"] as $listKey => $arList){
    $cat_ids []= $arList['ID'];
}?>

<span id="CURPAGE"  data-type="<?= 'yarmarka' ?>" data-cat-list="[<?= implode(',', $cat_ids) ?>]"  data-region="<?= $_GET['region'] ?>" data-id="<?=$_GET['CURPAGE']?>" data-url='<?= serialize($_GET) ?>'></span>
<div class="left_col">
    <br>
    <a class="nd_place_adv_yellow" href="/yarmarka/promo/" target="_blank">�������� �����</a>
    <?if(count($arResult["LISTS"])>1):?>
      <div class="lc_block">
         <div class="title">���������</div>
         <ul class="left_menu">
            <?foreach ($arResult["LISTS"] as $arList):?>
               <li><a href="<?=$arList["SECTION_PAGE_URL"]?>"><?=$arList["NAME"]?></a></li>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
   <?if(count($arResult["COMPANY_LIST"])):?>
      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?foreach ($arResult["COMPANY_LIST"] as $arCompany):?>
               <?if(!$arCompany['REMOVE_REL']):?>
                  <li><a href="<?=$arCompany["DETAIL_PAGE_URL"]?>"><?=$arCompany["NAME"]?></a></li>
               <?endif;?>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
   <?if(count($arResult["BRANDS_LIST"])):?>
      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?foreach ($arResult["BRANDS_LIST"] as $arBrands):?>
               <li><a href="<?=$arBrands["DETAIL_PAGE_URL"]?>"><?=$arBrands["NAME"]?></a> <img src="<?=$arBrands["PREVIEW_PICTURE"]?>"> </li>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
	
	<!-- ad E4-1 -->
	<div class="lc_block">
      <div class="left_banner_185">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E4_1",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E4-2 -->
	<div class="lc_block">
      <div class="left_banner_185">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E4_2",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
</div>

<div class="middle_col">
   <h1><?=$arResult["THIS_SECTION"]["NAME"]?></h1>
   <div class="mc_block">
		<!-- ad E1 -->
      <div class="mc_block_banner">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "MARKET_LIST_TOP",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
        <?
        // ������ ��������
        $title = "������";
        $iblock_id = 22;
        $rsPlace = CIBlockElement::GetList(
           array("SORT" => "ASC", "NAME" => "ASC"),
           array(
              "IBLOCK_ID" => $iblock_id,
              "ACTIVE" => "Y",
              "PROPERTY_COUNTRY" => 438
           ),
           false,
           false,
           array("ID", "NAME"));
        while($arPlace = $rsPlace->GetNext())
        {
           $arPlaces[] = $arPlace;
        }
        ?>
      <ul class="mb_menu ml">
         <li class="<?=  empty($_GET['region']) ? 'sel' : ''  ?>">
            <a data-id=""   href="?"><i class="icons icon_list list_sel"></i>��� �������</a>
            <div class="sclew"></div>
         </li>
         <? foreach ($arPlaces as $reg) {
             if (!isset($arResult["REGION_COUNT"][$reg['ID']])) continue; // ����  � ������� ��� ������� �� �� ����������
          ?>
         <li class="<?= $_GET['region'] == $reg['ID'] ? 'sel' :'' ?>">
            <a data-id="<?= $reg['ID'] ?>"  href="?region=<?= $reg['ID'] ?>#region=<?= $reg['ID'] ?>"><?= $reg['NAME'] ?></a>
            <div class="sclew"></div>
         </li>
          <?}?>
      </ul>

      <div class="clear"></div>
      <div class="">

			<!-- ad E3 -->
			<?$APPLICATION->IncludeComponent("bitrix:advertising.banner","market-popup",array(
				"TYPE" => "E3",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0",
				"CACHE_NOTES" => ""
			));?>
      </div><!--paging-->

      <?if(count($arResult["LISTS"])):?>
         <?if(count($arResult["LISTS"]) > 1):?>
            <script>
               $(function(){
                  $(".accordion_block").accordion({
                     heightStyle: "content",
                     active: parseInt($('.accordion_block').attr("data-active"))
                  });
               });
            </script>
         <?endif;?>
         <ul class="accordion_block" data-active="<?=$arResult["ACCORDION_ACTIVE"]?>">
            <?foreach ($arResult["LISTS"] as $listKey => $arList):?>
               <li class="js-section_list">
			   
                  <?if(count($arResult["LISTS"]) > 1):?>
                     <div id="tab<?=$listKey+1?>" data-category="<?=$arList['ID'];?>" class="h3-title ajax-catalog"><?=$arList["NAME"]?>
                         <sup><?=$arList["ITEMS_COUNT"]?> </sup>
                     </div>
                  <?endif;?>
                  <ul class="accordion-content" id="AC<?= $arList['ID']; ?>">
                     <div class="ads_row_wrapblock" style="    border-top: 1px solid #e7e7e7;">

                        <div class="ads_row_rowsblock  my-box-<?=$arList['ID']?>">
                            <? if (empty($arList["GROUPS"] )) : ?>
                                   <div>� ������� ��� �������!</div>
                               <? endif ?>
                           <?foreach ($arList["GROUPS"] as $arGroup):?>
                              <div class="ads_3_row js-hideitems-group">
                                 <div class="ads_row_left ">

                                    <?foreach ($arGroup["ITEMS"] as $arItemKey => $arItem):?>
                                        <? $premium =  $arResult["COMPANY_DETAIL"][$arGroup["COMPANY"]]['PREMIUM']  == '��' ?>
                                       <div class="ads_row <?if($premium){?>premium<?}?> <?=$arItem["HIDE"] ? ' item_hide' : ''?>">
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <td class="ic_star" style="vertical-align:top;"> </td>
                                                   <td class="image_box">
                                                      <a title="���������� ���������" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                                         <img title="������ <?=$arItem["NAME"]?>"  alt="<?=$arItem["NAME"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>">
                                                      </a>
                                                   </td>
                                                   <td class="ads_info">
                                                      <div class="tdWrap">
                                                         <div class="ads_title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?> </a></div>
                                                         <?if($arGroup["COMPANY"]):?>
                                                             <div class="ads_text"><?=$arItem["PROPERTY_FIRM_NAME"]?></div>
                                                         <?endif;?>
                                                          <div> / <?=date("d.m.Y", strtotime($arItem["DATE_CREATE"])); ?>  / ���������� : <?=$arItem["SHOW_COUNTER"]+1?>
                                                          <?= $arItem["PROPERTY_OPT_VALUE"] == '��' ?  ' / <span class="bkgreen">���</span>' : ''  ?><?= $arItem["PROPERTY_ROZNICA_VALUE"] == '��' ? ' / <span class="bkgreen">�������</span>' : ''  ?>
                                                          </div>
	                                                      <noindex>
                                                            <div class="meta"><?=$arItem["PREVIEW_TEXT"]?></div>
	                                                      </noindex>
                                                      </div>                      
                                                   </td>
                                                   <td class="ads_price">
                                                      <div class="tdWrap">
                                                         <?if($arItem["PROPERTY_COST_VALUE"]):?>
                                                            <div class="sum"><?=$arItem["PROPERTY_COST_VALUE"]?></div>
                                                            <div class="curr-val"><?=$arItem["PROPERTY_CURRENCY_FORMATED"]?></div>
                                                         <?else:?>

	                                                         <img alt="���� ���������" src="/bitrix/templates/energorb/images/pricedetail.png" width="77" height="29">
                                                         <?endif;?>
                                                         <a href="javascript: void(0);" class="buy-button" onclick="javascript:showPopup({id: $('.fast_order_popup'), productID: <?=$arItem["ID"]?>, sender: $(this)})"></a>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <?if(($arItem["HIDE_PANEL"]) && $arGroup["ITEMS"][$arItemKey+1]["HIDE"]):?>
                                                   <tr>
                                                      <td colspan="3">
                                                         <div class="show_position">
                                                            <a href="javascript:void(0)" class="js-hideitems-show" data-more="<?=$arParams["COMPANY_ELEMENT_MORE_COUNT"]?>">
                                                               <i class="plus_icon"></i>
                                                               <span>�������� <?=$arParams["COMPANY_ELEMENT_MORE_COUNT"] ? "���"/*." {$arParams["COMPANY_ELEMENT_MORE_COUNT"]} ������"*/ : " ��� �������"?></span></a>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                <?endif;?>
                                             </tbody>
                                          </table>
                                       </div><!--ads_row-->
                                    <?endforeach;?>
                                 </div>

                                 <?if($arGroup["COMPANY"]):
                                    $arCompany = $arResult["COMPANY_DETAIL"][$arGroup["COMPANY"]];?>
                                    <div class="ads_row_right">
                                       <div class="ads_row_rheader" onclick="window.open('<?=$arCompany["DETAIL_PAGE_URL"]?>','_blank'); return false;">
                                          <?if(is_array($arCompany["PREVIEW_PICTURE"])):?>
                                             <img src="<?=$arCompany["PREVIEW_PICTURE"]["SRC"]?>" alt="" />
                                          <?else:?>
                                             <img src="/images/search_placeholder.png" />
                                          <?endif;?>
                                          <span class="company-name"<?=(strlen($arCompany["NAME_CUT"]) != strlen($arCompany["NAME"])) ? " title='".$arCompany["NAME"]."'" : ''?>><?=$arCompany["NAME_CUT"]?><sup><?=$arCompany["ITEMS_COUNT"][$arList["ID"]]?></sup></span>
                                          <span class="after"></span>
                                       </div>
                                       <div class="ads_row_rcontent">
                                          <!--<a class="show" href="javascript:void(0)" onClick="jQuery(this).parent().find('.all_company_info').toggle();">�������� �������� <i></i></a>-->
                                          <div class="all_company_info" id="allCompanyInfoID" ><noindex>
                                             <a class="btns" rel="nofollow" style="margin-top: 10px;" href="<?=$arCompany["PREDSTAVITEL_MESSAGE_LINK"]?>">
	                                             <i></i>��������
                                             </a></noindex>
                                             <a class="c_name" href="<?=$arCompany["DETAIL_PAGE_URL"]?>">�������� ��������</a>
	                                          <noindex>
	                                             <ul class="js-phone-hide-block" data-company="<?=$arCompany['ID']?>">
	                                                <?foreach ($arCompany["PROPERTY_phone"] as $phone):?>
	                                                   <li>
	                                                      <span class="js-phone-code">+375</span>
	                                                      <span class="phone-hide js-phone-hide">�������� �������</span>
	                                                      <span class="phone-show"><?= phone($phone["VALUE"])?></span>
	                                                   </li>
	                                                <?endforeach;?>
	                                                <?foreach ($arCompany["PROPERTY_URL"] as $url):?>
	                                                   <li><a class="c_site" rel='nofollow' href="<?=$url["VALUE_HREF"]?>"><?=$url["VALUE"]?></a></li>
	                                                <?endforeach;?>
	                                             </ul>
	                                          </noindex>
                                             <a class="c_mess"  rel="nofollow"  href="javascript:void(0);">
                                                <div>
                                                   �������� ����������,<br> ��� ����� ��� �� <?= (LANG == 's1' ? 'EnergoBelarus.by' : 'AgroBelarus.by')?>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                 <?endif;?>
                                 <div class="clear"></div>
                              </div>
                           <?endforeach;?>
                           
                           <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                              <?= $arList["NAV_STRING"] ?>
                           <? endif; ?>
                        </div>

                     </div>

                  </ul>
               </li>
            <?endforeach;?>
         </ul>
      <?endif;?>

      <div class="mc_block_banner bottom"><!-- ad MARKET_LIST_BOTTOM --></div>
		
   </div>
   <?if(($arParams['SHOW_SEO_TEXT'] == 'Y') && strlen($arResult["THIS_SECTION"]["DESCRIPTION"])):?>
      <div class="rc_block">
         <div class="rc_block_info rc_block_description">
            <div><?=$arResult["THIS_SECTION"]["DESCRIPTION"]?></div>
         </div>
      </div>
   <?endif;?>
</div>
<div class="content_right_col">

        <? global $filter_smart ; echo $filter_smart ?>

   <?if($arParams["SHOW_RUBRIKATOR"] == "Y"):?>
      <div class="rc_block">
         <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "filament", Array(
                         "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                         "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                         "SECTION_ID" => $arResult["THIS_SECTION"]["ID"],
                         "SECTION_CODE" => "",
                         "SECTION_URL" => "",
                         "COUNT_ELEMENTS" => "Y",
                         "TOP_DEPTH" => "10",
                         "SECTION_FIELDS" => "",
                         "SECTION_USER_FIELDS" => "",
                         "ADD_SECTIONS_CHAIN" => "Y",
                         "CACHE_TYPE" => "N",
                         "CACHE_TIME" => "36000000",
                         "CACHE_NOTES" => "",
                         "CACHE_GROUPS" => "Y"
                 )		
         );?>
      </div>
   <?endif;?>
	<!-- ad E2-1 -->
   <div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "market-rightcol", Array(
               "TYPE" => "MARKET_LIST_RIGHT",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-2 -->
   <div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_2",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-3 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_3",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-4 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_4",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-5 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_5",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	
	<!-- ad E2-6 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_6",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>

   <div class="rc_block">
      <div class="right_banner_220">
			<!-- Яндекс.Директ -->
			<div id="yandex_ad"></div>
			<script type="text/javascript">
			(function(w, d, n, s, t) {
			w[n] = w[n] || [];
			w[n].push(function() {
			Ya.Direct.insertInto(135481, "yandex_ad", {
			stat_id: 1,
			ad_format: "direct",
			type: "240x400",
			border_type: "block",
			border_radius: true,
			site_bg_color: "FFFFFF",
			border_color: "CCCCCC",
			title_color: "266BAB",
			url_color: "000099",
			text_color: "000000",
			hover_color: "0B9DF1",
			favicon: true,
			no_sitelinks: false
			});
			});
			t = d.getElementsByTagName("script")[0];
			s = d.createElement("script");
			s.src = "//an.yandex.ru/system/context.js";
			s.type = "text/javascript";
			s.async = true;
			t.parentNode.insertBefore(s, t);
			})(window, document, "yandex_context_callbacks");
			</script>
      </div>
   </div>
</div>

