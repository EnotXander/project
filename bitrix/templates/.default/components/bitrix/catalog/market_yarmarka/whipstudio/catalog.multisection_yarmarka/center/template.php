<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $GBL_SHOW_FAST_ORDER;
$GBL_SHOW_FAST_ORDER = true;
?>
<?foreach ($arResult["LISTS"] as $listKey => $arList){
    $cat_ids []= $arList['ID'];
}?>

<span id="CURPAGE"  data-type="<?= 'yarmarka' ?>" data-cat-list="[<?= implode(',', $cat_ids) ?>]"  data-region="<?= $_GET['region'] ?>" data-id="<?=$_GET['CURPAGE']?>" data-url='<?= serialize($_GET) ?>'></span>
<div class="left_col">
    <br>
    <a class="nd_place_adv_yellow" href="/yarmarka/promo/" target="_blank">�������� �����</a>
    <?if(count($arResult["LISTS"])>1 && false):?>
      <div class="lc_block">
         <div class="title">���������</div>
         <ul class="left_menu">
            <?foreach ($arResult["LISTS"] as $arList):?>
               <li><a href="<?=$arList["SECTION_PAGE_URL"]?>"><?=$arList["NAME"]?></a></li>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
   <?if(count($arResult["COMPANY_LIST"])):?>
      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?foreach ($arResult["COMPANY_LIST"] as $arCompany):?>
               <?if(!$arCompany['REMOVE_REL']):?>
                  <li><a href="<?=$arCompany["DETAIL_PAGE_URL"]?>"><?=$arCompany["NAME"]?></a></li>
               <?endif;?>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
   <?if(count($arResult["BRANDS_LIST"])):?>
      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?foreach ($arResult["BRANDS_LIST"] as $arBrands):?>
               <li><a href="<?=$arBrands["DETAIL_PAGE_URL"]?>"><?=$arBrands["NAME"]?></a> <img src="<?=$arBrands["PREVIEW_PICTURE"]?>"> </li>
            <?endforeach;?>
         </ul>
      </div>
   <?endif;?>
	
	<!-- ad E4-1 -->
	<div class="lc_block">
      <div class="left_banner_185">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E4_1",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E4-2 -->
	<div class="lc_block">
      <div class="left_banner_185">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E4_2",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
</div>

<div class="middle_col">
   <h1><?=$arResult["THIS_SECTION"]["NAME"]?></h1>
   <div class="mc_block">
		<!-- ad E1 -->
      <div class="mc_block_banner">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "MARKET_LIST_TOP",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>


      <div class="clear"></div>
      <div class="">

			<!-- ad E3 -->
			<?$APPLICATION->IncludeComponent("bitrix:advertising.banner","market-popup",array(
				"TYPE" => "E3",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0",
				"CACHE_NOTES" => ""
			));?>
      </div>
      <!--paging-->
      <?
        $blocks  = [];
        $blocks []= array('name'=>'���������', 'link'=>'/yarmarka/kartofel/', 'img'=>'/images/yarmarka/kartofel.jpg');
        $blocks []= array('name'=>'�������', 'link'=>'/yarmarka/morkov/', 'img'=>'/images/yarmarka/morkov.jpg');
        $blocks []= array('name'=>'������', 'link'=>'/yarmarka/svyekla/', 'img'=>'/images/yarmarka/svyekla.jpg');
      ?>
      <div>
          <? foreach ($blocks as $item ) {?>

              <div class="center_block"> <a href="<?= $item['link'] ?>">
                    <div class="cb_im_b">
		              <img src="<?= $item['img'] ?>" width="100%"/>
                    </div>
                    <div class="cb_text"><?= $item['name'] ?></div> </a>
              </div>

          <? } ?>
          <div class="clear"></div>
      </div>

      <div class="mc_block_banner bottom"><!-- ad MARKET_LIST_BOTTOM --></div>
		
   </div>

</div>
<div class="content_right_col">

        <? global $filter_smart ; echo $filter_smart ?>

   <?if($arParams["SHOW_RUBRIKATOR"] == "Y"):?>
      <div class="rc_block">
         <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "filament", Array(
                         "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                         "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                         "SECTION_ID" => $arResult["THIS_SECTION"]["ID"],
                         "SECTION_CODE" => "",
                         "SECTION_URL" => "",
                         "COUNT_ELEMENTS" => "Y",
                         "TOP_DEPTH" => "10",
                         "SECTION_FIELDS" => "",
                         "SECTION_USER_FIELDS" => "",
                         "ADD_SECTIONS_CHAIN" => "Y",
                         "CACHE_TYPE" => "N",
                         "CACHE_TIME" => "36000000",
                         "CACHE_NOTES" => "",
                         "CACHE_GROUPS" => "Y"
                 )		
         );?>
      </div>
   <?endif;?>
	<!-- ad E2-1 -->
   <div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "market-rightcol", Array(
               "TYPE" => "MARKET_LIST_RIGHT",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-2 -->
   <div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_2",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-3 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_3",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-4 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_4",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	<!-- ad E2-5 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_5",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>
	
	<!-- ad E2-6 -->
	<div class="rc_block">
      <div class="right_banner_220">
         <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
               "TYPE" => "E2_6",
               "NOINDEX" => "N",
               "CACHE_TYPE" => "A",
               "CACHE_TIME" => "0",
               "CACHE_NOTES" => ""
           )
         );?>
      </div>
   </div>

   <div class="rc_block">
      <div class="right_banner_220">
			<!-- Яндекс.Директ -->
			<div id="yandex_ad"></div>
			<script type="text/javascript">
			(function(w, d, n, s, t) {
			w[n] = w[n] || [];
			w[n].push(function() {
			Ya.Direct.insertInto(135481, "yandex_ad", {
			stat_id: 1,
			ad_format: "direct",
			type: "240x400",
			border_type: "block",
			border_radius: true,
			site_bg_color: "FFFFFF",
			border_color: "CCCCCC",
			title_color: "266BAB",
			url_color: "000099",
			text_color: "000000",
			hover_color: "0B9DF1",
			favicon: true,
			no_sitelinks: false
			});
			});
			t = d.getElementsByTagName("script")[0];
			s = d.createElement("script");
			s.src = "//an.yandex.ru/system/context.js";
			s.type = "text/javascript";
			s.async = true;
			t.parentNode.insertBefore(s, t);
			})(window, document, "yandex_context_callbacks");
			</script>
      </div>
   </div>
</div>

