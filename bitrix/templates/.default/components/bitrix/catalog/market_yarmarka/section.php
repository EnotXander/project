<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>
<div class="middle_col">
   <div class="mc_block">
      <div class="bread_market">
         <ul class="market_bread">
            <li class="">
                <a href="/">�������</a>
            </li>
            <?
            if(!CModule::IncludeModule("iblock")) die();
            
            if(strlen($arResult["VARIABLES"]["SECTION_CODE"])){
               $resChain = CIBlockSection::GetList(
	               array(),
	               array("=CODE" => $arResult["VARIABLES"]["SECTION_CODE"], "IBLOCK_ID" => $arParams["IBLOCK_ID"])
               );
               if ($elementChain = $resChain->GetNext()) 
                  $arResult["VARIABLES"]["SECTION_ID"] = $elementChain["ID"];
            }
   
            $nav = CIBlockSection::GetNavChain(false, $arResult["VARIABLES"]["SECTION_ID"]);
            ?>
				<?while ($arSect = $nav->GetNext()):?>
					<?
					$subsectCount = CIBlockSection::GetCount(array(
						'IBLOCK_ID' => $arParams["IBLOCK_ID"],
						'SECTION_ID' => $arSect["ID"]
					));
					$subsectCountClass = $subsectCount > 0 ? ' haschild' : '';
					?>
               <li class="marketf">
                  <div class="submarket<?=$subsectCountClass?>">
                     <a href="<?=$arSect['SECTION_PAGE_URL']?>" class="submarket-a"><?=$arSect['NAME']?></a>
                     <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "bread", Array(
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"SECTION_ID" => $arSect["ID"],
								"SECTION_CODE" => $arSect["CODE"],
								"DISPLAY_PANEL" => "N",
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								//"CACHE_TYPE" => 'N',
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
								'TOP_DEPTH' => 1,
								'NAME_CUT' => 50,
								'CURRENT_SECTION' => $arResult["VARIABLES"]["SECTION_ID"]
								), $component
                     );?>
                  </div>
               </li>
            <?endwhile;?>
         </ul>
      </div>
   </div>

   <?
   global $arrrFilter;

   if ((int)$_REQUEST['FIRM'] > 0)
      $arrrFilter['PROPERTY_FIRM'] = (int)$_REQUEST['FIRM'];

   if ((int)$_REQUEST['per_page'] > 0)
      $perPage = (int)$_REQUEST['per_page'];
   else
      $perPage = "20";

   //   PrintAdmin($arParams["IBLOCK_ID"]);
   global $filter_smart;
   if ( $_REQUEST['ajax'] != 'y')
       ob_start();

   ?><?$APPLICATION->IncludeComponent(
   			"bitrix:catalog.smart.filter",
   			"visual_vertical",
   			Array(
   				"COMPONENT_TEMPLATE" => "visual_vertical",
   				"IBLOCK_TYPE" => "services",
   				"IBLOCK_ID" => "81",
   				"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
   				"SECTION_CODE" => false,
   				"FILTER_NAME" => "arrrFilter",
   				"HIDE_NOT_AVAILABLE" => "N",
   				"TEMPLATE_THEME" => "",
   				"FILTER_VIEW_MODE" => "vertical",
   				"DISPLAY_ELEMENT_COUNT" => "Y",
   				"SEF_MODE" => "Y",
   				"CACHE_TYPE" => "A",
   				"CACHE_TIME" => "36000000",
   				"CACHE_GROUPS" => "Y",
   				"SAVE_IN_SESSION" => "N",
   				"INSTANT_RELOAD" => "N",
   				"PRICE_CODE" => array(),
   				"CONVERT_CURRENCY" => "N",
   				"XML_EXPORT" => "N",
   				"SECTION_TITLE" => "-",
   				"SECTION_DESCRIPTION" => "-",
   				"POPUP_POSITION" => "left",
   				"SEF_RULE" => "/yarmarka/#SECTION_CODE#/",
   				"SECTION_CODE_PATH" => "",
   				"SMART_FILTER_PATH" => "",
   				"VARIABLE_ALIASES" => Array(
   				)
   			), $component
   		);?>
   		<?
   		//global $arrFilter;
   		//PrintAdmin($arrrFilter);
   		?><?
    if ( $_REQUEST['ajax'] != 'y') {
        $filter_smart = ob_get_contents();
       ob_end_clean();
    }

   ?>

   <?
   $APPLICATION->IncludeComponent("whipstudio:catalog.multisection_yarmarka", 'yarmarka', Array(
       "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
       "IBLOCK_ID" => $arParams["IBLOCK_ID"],
       "INCLUDE_SUBLISTS" => true,//���������� �� ��������� ������ ��� ������� ����������
       "FILTER_NAME" => "arrrFilter", //$arParams["FILTER_NAME"],
       "CACHE_TYPE" => $arParams["CACHE_TYPE"],
       "CACHE_TIME" => $arParams["CACHE_TIME"],
       "CACHE_FILTER" => $arParams["CACHE_FILTER"],
       "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
       "PAGE_ELEMENT_COUNT" => $perPage,
       "COMPANY_ELEMENT_COUNT" => 3,
       "COMPANY_ELEMENT_MORE_COUNT" => 20,
       "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
       "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
       "PAGER_TITLE" => $arParams["PAGER_TITLE"],
       "SHOW_GOODS" => 'N',
       "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
       "PAGER_TEMPLATE" => "market",//$arParams["PAGER_TEMPLATE"],
       "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
       "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
       "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
       "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
       "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
       "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
       "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
       "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
       "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
       "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
       "COMPANY_NAME_CUT" => 15,
       "DETAIL_TEXT_CUT" => 155,
       "SORT" => array(
            "company" => array(
                "NAME" => "��������",
                "TYPE" => "DESC",
                "CODE" => "PROPERTY_FIRM.PROPERTY_EXT_SORT"
            ),
            "price" => array(
                "NAME" => "����",
                "TYPE" => "asc,nulls",
                "CODE" => "PROPERTY_COST"
            ),
            "date" => array(
                "NAME" => "����",
                "TYPE" => "desc",
                "CODE" => "DATE_CREATE"
            ),
            "rate" => array(
                "NAME" => "�������",
                "TYPE" => "asc",
                "CODE" => "shows"
            )
        ),
       "SORT_DEFAULT" => "company",
       "SHOW_SEO_TEXT" => "Y",
       "SHOW_RUBRIKATOR" => "N"
           ), $component
   );
   ?>
	<br/><br/><br/>
    <br/><br/><br/><br/>
	<div class="clear"></div>
</div>
