//����� ��������������
$(function () {
    jQuery("a.lightbox").lightBox();

    var width = 130;
    var count = 4;

    if ($('#images').size()) {
        var ul = document.getElementById('images');
        var imgs = ul.getElementsByTagName('li');

        var position = 0;

        if ($('#prev').size()) {
            document.getElementById('prev').onclick = function () {
                if (position >= 0) return false;

                position = Math.min(position + width * count, 0)
                ul.style.marginLeft = position + 'px';
                return false;
            }
        }

        if ($('#next').size()) {
            document.getElementById('next').onclick = function () {
                if (position <= -width * (imgs.length - count)) return false;

                position = Math.max(position - width * count, -width * (imgs.length - count));
                ul.style.marginLeft = position + 'px';
                return false;
            };
        }
    }


    function showAllProducts() {
        $('.middle_col .ads_row').show();
        $('.middle_col .ads_row .show_positions').hide();
    }

///�� template//
    jQuery('.wrong_block a').click(function () {
        jQuery(this).closest('.wrong_block').addClass('clicked');
    });

    jQuery('.mc_body .ads_wrap:nth-child(4n+4)').addClass('hoverBr');
    jQuery('.all_c_info').hide();

    jQuery('.ads_nav').hover(
            function () {
                jQuery(this).closest('.ads_wrap').addClass('hoverB2');
                jQuery(this).closest('.ads_wrap').find('.all_c_info').show();
            }
    )
    jQuery('.ads_wrap').mouseleave(function () {
        jQuery(this).removeClass('hoverB2');
        jQuery(this).find('.all_c_info').hide();
    });


    $('.js-make-predstavitel a').live("click", function () {
        var $button = $(this).closest('.js-make-predstavitel');
        var companyId = $(this).attr("data-company-id");
        $button.html("�������� ������...");
        $.ajax({
            type: "POST",
            url: "/_ajax/personal/make_predstavitel.php",
            data: "companyId=" + companyId,
            dataType: "json",
            success: function (data) {
                if (!data.error.length) {
                    if (data.answer.REQUEST_CREATE)
                        $button.html("���� ������ �� ������������");
                } else {
                    $button.html("");
                    for (var index in data.error) {
                        $button.append(data.error[index] + "<br>");
                    }
                }
            }
        });
    });
});


function BX_SetPlacemarks_MAP_HMQpL5SEAD(map) {
    var arObjects = {PLACEMARKS: [], POLYLINES: []};
    arObjects.PLACEMARKS[arObjects.PLACEMARKS.length] = BX_YMapAddPlacemark(map, {
        'LON': '27.390851',
        'LAT': '53.840819',
        'TEXT': '&quot;������&quot; ����'
    });
}


////


$(document).ready(function () {
    $('.author-wrap > a').click(function () {
        if ($(this).hasClass('act')) {
            $(this).parent().find('div').fadeOut(200);
            $(this).removeClass('act');
        }
        else {
            $('.author-wrap a.act').parent().find('div').fadeOut(200);
            $('.author-wrap a.act').removeClass('act');
            $(this).parent().find('div').fadeIn(200);
            $(this).addClass('act');
        }
        return false;
    });
    $('.to-mark span').click(function () {
        $(this).toggleClass('act');
        return false;
    });
    $(document).click(function (event) {
        if ($(event.target).closest('.author-wrap > div').length)
            return;
        $('.author-wrap > div').fadeOut(200);
        $('.author-wrap a').removeClass('act');
        event.stopPropagation();
    });

    $('.carousel').flexslider({
        animation: "slide",
        slideshow: false,
        animationLoop: false,
        controlNav: false,
        itemWidth: 102,
        itemMargin: 0,
        maxItems: 6,
        minItems: 6,
        move: 6
    });
    $('.fancybox').fancybox({
        tpl: {
            next: '<a title="" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
            prev: '<a title="" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>',
            closeBtn: '<a class="fancybox-item fancybox-close" href="javascript:;"></a>'
        }
    });
    $('.carousel .slides a').click(function () {
        $('.carousel .slides a').removeClass('act');
        $(this).addClass('act');
        $('.img-wrap').hide();
        $('#img' + $(this).data('num')).show();
        return false;
    });
});
