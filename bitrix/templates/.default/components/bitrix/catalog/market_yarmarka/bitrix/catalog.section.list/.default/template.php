<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.collapsorz_1.1.min.js');

//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/main_collapsorz.js');
//$arResult["MARKET_ID"];
//$arResult["USLUGI_ID"];
$isMarket = $arParams['IBLOCK_ID'] == $arParams["IBLOCK_PRODUCT_ID"];
$isService = $arParams['IBLOCK_ID'] == $arParams["IBLOCK_USLUGI_ID"];
//PrintAdmin($arParams);
?>

<div id='middle_col' class="middle_col">
   <div class="mc_block" >
      <ul class="mb_menu">
         <li <? if ($isMarket): ?>class="sel"<? endif; ?>><a style="font-size:20px" <? if ($isMarket): ?>name="market"<? else: ?>href="/market"<? endif; ?>>������</a><div class="sclew"></div></li>
         <li <? if ($isService): ?>class="sel"<? endif; ?>><a style="font-size:20px" <? if ($isService): ?>name="services"<? else: ?>href="/services"<? endif; ?>>������</a><div class="sclew"></div></li>
         <li style="background:none; float:right;" class="add_l_m">
		         <a href='/personal/company' rel='nofollow'>�������� ���� �������� � ������</a>
         </li>
      </ul>
      <? if( $arParams['HIDE_TOP'] == false ) { ?>
      <!--���������� ����������� ������ �������� ������� ������-->
      <ul id="hide-list" class="product-list">

         <?foreach ($arResult as $arSection):?>
            <?if($arSection["ELEMENT_CNT"]):?>
               <li>
                  <a href="#<?= $arSection['CODE'] ?>">
                      <img alt="<?= $arSection['NAME'] ?>" src="<?=($arSection['PICTURE']['src']) ? $arSection['PICTURE']['src'] : '/images/search_placeholder.png'?>" />
                  </a>
                  <a class="product-title" href="#<?=$arSection['CODE']?>"><?= $arSection["NAME"] ?>
                  </a>
               </li>
            <?endif;?>
         <?endforeach;?>

      </ul>
      <? } else {?> <div style=" border-top: 2px solid #00A340;"></div> <? } ?>
      <div class="clear"></div>
      <br />
   </div>
   

   <div class="catalog-section-list">
      <ul>	  
         <?foreach ($arResult as $key1 => $arSection):?>
            <?if($arSection["ELEMENT_CNT"]):?>
               <li<?= ($arSection['DEPTH_LEVEL'] == 1) ? ' id="'.$arSection["CODE"].'"' : ''?> class="level_li_<?= $arSection['DEPTH_LEVEL'] ?>">
	              <? if(!empty($arSection['PICTURE']['src'])): ?>
		              <a href="<?= $arSection["SECTION_PAGE_URL"] ?>">
	                   <img  alt="������� � <?= $arSection["NAME"] ?>" src="<?= ($arSection['PICTURE']['src']) ? $arSection['SMALL_PICTURE']['src'] : '/images/small_search_placeholder.png' ?>" />
			          </a>
	              <? endif; ?>
                  <h2 class="h2_clear"><a class="level<?= $arSection['DEPTH_LEVEL'] ?>" href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?></a></h2>
                  <sup><?= $arSection["ELEMENT_CNT"] ?></sup>
                  <div class="clear"></div>
                  
                  <!--����� ������� �� �������� ������� ������-->
                  <?if(is_array($arSection["SUB-2"]) && count($arSection["SUB-2"])):?>
                     <? foreach($arSection["SUBSECT"] as $arSubSect): ?>
                        <ul class="ul_col">
                        
                        <!-- ����� ������ ������� -->
                        <? foreach($arSubSect as $key3 => $SubSect):?>
                           <?if($SubSect["ELEMENT_CNT"]):?>
                              <li class="level_li_<?=$SubSect['DEPTH_LEVEL'] ?>">
                                 <h3 class="h3_clear"><a class="level<?= $SubSect['DEPTH_LEVEL'] ?>" href="<?= $SubSect["SECTION_PAGE_URL"] ?>"><?= $SubSect["NAME"] ?></a></h3>
                                <sup><?= $SubSect["ELEMENT_CNT"] ?></sup>
                                 <?if($SubSect['SUB-3']):?>
                                    <a title="������� ����������" href="javascript:void(0)" class="toggle-arrow"></a>
                                 <?endif;?>

                                 <?if($arResult[$key1]['SUB-2'][$key3]['SUB-3']):?>   
                                 <!--����� ���� �������� ������-->
                                    <ul class="toggle-menu"> 
                                    <?foreach($arResult[$key1]['SUB-2'][$key3]['SUB-3'] as $arSub2Section):?>
                                       <?if($SubSect["ELEMENT_CNT"]):?>
                                          <li class="level_li_<?= $arSub2Section['DEPTH_LEVEL']?>">
                                             <a class="level<?= $arSub2Section['DEPTH_LEVEL'] ?>" href="<?= $arSub2Section["SECTION_PAGE_URL"] ?>"><?= $arSub2Section["NAME"] ?></a>
                                            <sup><?= $arSub2Section["ELEMENT_CNT"] ?></sup>
                                          </li>
                                       <?endif;?>
                                    <?endforeach;?>
                                    </ul>
                                 <?endif;?>
                              </li>
                           <?endif;?>  
                        <?endforeach;?>
                        </ul>
                     <?endforeach;?>
                  <?endif;?>
               </li>
               <li style="clear:both; width:100%; border-bottom:1px solid #ABABAB;margin: 20px 0; padding: 10px 0 0;"></li>
            <?endif;?>
         <?endforeach;?>
      </ul>
   </div>
   
</div>