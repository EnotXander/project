<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//������� ���, ����� ���� ������ ��� ������� �� ����� script.js, ������� ����� ��� ��������
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.localScroll.min.js');


$arResult['SECTION_COUNT'] = array();
$rsSectionCount = CIBlockSection::GetList(array(), array('CNT_ACTIVE' => 'Y'), true, array('ID', 'IBLOCK_ID', 'ELEMENT_CNT'));
while ($arSectionCount = $rsSectionCount->GetNext()) {
    $arResult['SECTION_COUNT'][$arSectionCount['ID']] = $arSectionCount['ELEMENT_CNT'];
//    PrintAdmin($arSectionCount);
}

$obParser = new CTextParser;

#####������ �������� �������� ������� ������#####
foreach ($arResult['SECTIONS'] as $key => $arItem)
{
   $arResult['SECTIONS'][$key]["NAME"] = $obParser->html_cut($arItem["NAME"], 50);
  
   $arResult['SECTIONS'][$key]['PICTURE'] = CFile::ResizeImageGet(
                                                         $arItem["PICTURE"],
                                                         array("width" => 74, "height" => 76),
                                                         BX_RESIZE_IMAGE_PROPORTIONAL,
                                                         true
                                                          );

   $arResult['SECTIONS'][$key]['SMALL_PICTURE'] = CFile::ResizeImageGet(
                                                         $arItem["PICTURE"],
                                                         array("width" => 38, "height" => 35),
                                                         BX_RESIZE_IMAGE_PROPORTIONAL,
                                                         true);
    $arResult['SECTIONS'][$key]["ELEMENT_CNT"] = $arResult['SECTION_COUNT'][$arItem['ID']];
}
###������ �������� �������� ������� ������###
  
##### �������� ����������� ��������� #####
//������ ����� ArResult//
foreach($arResult['SECTIONS'] as $key => $arSection)
{
//   $arSection['ELEMENT_CNT'] = '&nbsp;';

   if($arSection['ELEMENT_CNT'])
   {
     $arTemp[$arSection['ID']]=$arSection;
   }
   else
   {   
      unset($arTemp[$arSection['ID']]);
   }
}

//�������� ����������� �������� ������ �� ������//
foreach($arTemp as $key1 => $arSection)
{
   if ($arSection['DEPTH_LEVEL']==3)
   {
      $arTemp[$arSection['IBLOCK_SECTION_ID']]['SUB-3'][]=$arSection;
      unset($arTemp[$key1]);
   }
}

//�������� ����������� ������� ������ � ������//
foreach($arTemp as $key2 => $arSection)
{
   if ($arSection['DEPTH_LEVEL']==2)
   {
      $arTemp[$arSection['IBLOCK_SECTION_ID']]['SUB-2'][]=$arSection;
      unset($arTemp[$key2]);
   }
}
$arResult=$arTemp;
### �������� ����������� ��������� ###

##### ��������� ������� �������� 2-�� ������ �� ���������� �� 3 ��������#####
foreach ($arResult as $key => $arSection)
{
   $countSub=0;
   foreach($arSection['SUB-2'] as $key2=>$arSubSect):
      if($arSubSect['ELEMENT_CNT'])
      {
         $countSub++;
      }
      else
      {
         unset($arResult[$key]["SUB-2"][$key2]["SUB-3"]);
      }
   endforeach;
   
   
   $arResult[$key]["SUBSECT"]=array_chunk($arResult[$key]["SUB-2"],ceil($countSub/3),TRUE);

}
### ��������� ������� �������� 3-�� ������ �� ���������� �� 3 ��������###
