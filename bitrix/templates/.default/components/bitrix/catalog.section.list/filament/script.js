$(function() {
   // BUTTONS
   $('.fg-button').hover(
           function() {
              $(this).removeClass('ui-state-default').addClass('ui-state-focus');
           },
           function() {
              $(this).removeClass('ui-state-focus').addClass('ui-state-default');
           }
   );

   // MENUS    	
   $('#hierarchybreadcrumb').menu({
      content: $('#hierarchybreadcrumb').next().html(),
      backLink: false
   });

});