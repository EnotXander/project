<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>

<?if($arResult["AUTH_SERVICES"]):?>
	<div class="bx-auth-title"><?echo GetMessage("AUTH_TITLE")?></div>
<?endif?>

<div class="content_wrp">
	<div class="autorization_text">
		<p class="red_text">�����������</p>
		<p class="login_text">����������, ������� ��� �-mail � ������:</p>
	</div>
	<div class="autorization_block">
		<div class="autorization_form_wrp">
			<form novalidate name="form_auth" method="post" class="autorization_form" id="data" action="<?=$arResult["AUTH_URL"]?>">
				<input type="hidden" name="AUTH_FORM" value="Y" />
				<input type="hidden" name="TYPE" value="AUTH" />
				<?if (strlen($arResult["BACKURL"]) > 0):?>
					<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?endif?>
				<?foreach ($arResult["POST"] as $key => $value):?>
					<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
				<?endforeach?>
				<div class="mb-21 p-r">
					<label>�����/e-mail: <input id="email" type="email" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>"/></label><br/>
					<span id="plashka_quest" class="quest_disable"></span>
					<div id="emailInfo" class="info_error correct">
						<span class="info_error_arrow"></span>
						<p class="info_error_text">�����/e-mail ������ �������. ����������, ��������� ������������ ���������.</p>
					</div>
				</div>

				<div class="ml-31">
					<label>������: <input class="ml-17" type="password" name="USER_PASSWORD" /></label>
				</div>
				<div class="mt-12 ml-104">
					<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
						<label class="chekbox_label_text"><input class="mr-10 remember_checkbox" type="checkbox" value="1" name="USER_REMEMBER"><span></span>��������� ����</label>
					<?endif?>
					
					<?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
						<noindex>
							<p>
								<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="password_link" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_3")?></a>
							</p>
						</noindex>
					<?endif?>
				</div>
				<div class="autorization_button">
					<input type="submit" value="" class="autorization_submit">
					<p><a class="registration_button" href="/auth/?register=yes"></a></p>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	<?if (strlen($arResult["LAST_LOGIN"])>0):?>
	try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
	<?else:?>
	try{document.form_auth.USER_LOGIN.focus();}catch(e){}
	<?endif?>
</script>

<?if($arResult["AUTH_SERVICES"]):?>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
	array(
		"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
		"CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
		"AUTH_URL" => $arResult["AUTH_URL"],
		"POST" => $arResult["POST"],
		"SHOW_TITLES" => $arResult["FOR_INTRANET"]?'N':'Y',
		"FOR_SPLIT" => $arResult["FOR_INTRANET"]?'Y':'N',
		"AUTH_LINE" => $arResult["FOR_INTRANET"]?'N':'Y',
	),
	$component,
	array("HIDE_ICONS"=>"Y")
);
?>
<?endif?>