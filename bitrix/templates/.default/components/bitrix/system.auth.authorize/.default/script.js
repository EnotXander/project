$(document).ready(function()
{
    var emailInfo = $('#emailInfo'),
        ele = $('#email'),
        quest = $('#plashka_quest'),
        patt = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,6}$/;

    ele.live('click keydown focus blur keypress propertychange input cut copy paste', function(){
        if(!patt.test(ele.val())) {
//            jVal.errors = true;
            emailInfo.removeClass('correct').addClass('error');
            ele.removeClass('normal').addClass('wrong');
            quest.removeClass('quest_disable').addClass('quest_enable');
        } else {
            emailInfo.removeClass('error').addClass('correct');
            ele.removeClass('wrong').addClass('normal');
            quest.removeClass('quest_enable').addClass('quest_disable');
        };
    }).live('focus click', function(){
        if (ele.hasClass('wrong')) {
            emailInfo.removeClass('correct').addClass('error');
        };
    }).live('blur', function (){
        emailInfo.removeClass('error').addClass('correct');
    })
});
