<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!CModule::IncludeModule("iblock")) die();


foreach ($arResult["SECTIONS"] as $keySection => $arSection) 
{
   foreach ($arSection["ITEMS"] as $key => $arElement) 
   {
      $arResult["SECTIONS"][$keySection]["NAME"] = TruncateText($arSection["NAME"], 45);
      $arResult["SECTIONS"][$keySection]["DATE_CREATE"] = ConvertDateTime($arSection["DATE_CREATE"], "DD.MM YYYY", "ru");
      if(is_array($arElement["DETAIL_PICTURE"]) || is_array($arElement["PREVIEW_PICTURE"]))
      {
         $FILE = is_array($arElement["DETAIL_PICTURE"]) ? $arElement["DETAIL_PICTURE"] : $arElement["PREVIEW_PICTURE"];
         $arFileTmp = CFile::ResizeImageGet(
            $FILE,
            array("width" => 85, "height" => 71),
            BX_RESIZE_IMAGE_EXACT,
            true,
            array()
         );
         $arResult["SECTIONS"][$keySection]["ITEMS"][$key]["DETAIL_PICTURE"] = array(
            "SRC" => $arFileTmp["src"],
            "WIDTH" => $arFileTmp["width"],
            "HEIGHT" => $arFileTmp["height"],
         );
      }
      else
         unset($arResult["SECTIONS"][$keySection]);
   }
}