<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
global $USER;
if (!$USER->IsAuthorized()) {
	return;
}
?>
<script type="text/javascript">
	$(document).ready(function () {
		$('.cab-sections-nav > li > a').click(function () {
			if ($(this).parent().find('ul').length == 0) return;
			$(this).removeAttr('href');
			var element = $(this).parent('li');
			element.find('ul.toogle').slideToggle();
			element.toggleClass('act');
			//this.html('<span>' + element.html() + '</span>');

		});

	});
</script>
<? if (!empty($arResult)): ?>
<ul class="cab-sections-nav">

	<? $previousLevel = 0;?>
	<? foreach ($arResult as $arItem): ?>

	<? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
		<?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
	<? endif ?>

	<? if ($arItem["IS_PARENT"]): ?>
		<? if ($arItem["DEPTH_LEVEL"] == 1): ?>
			<li <?= $arItem["SELECTED"] ? 'class="act"' : ''; ?>>
				<a href="#" class="<? if ($arItem["SELECTED"]): ?>act<? endif ?>">
					<span><?= $arItem["TEXT"] ?></span>
				</a>
				<ul style="display: <?= $arItem["SELECTED"] ? 'block' : 'none'; ?>;" class="toogle">
		<? else: ?>
			<li>
				<a href="<?= $arItem["LINK"] ?>" class="<? if ($arItem["SELECTED"]): ?>act<? endif ?>">
					<?= $arItem["TEXT"] ?>
				</a>
				<ul>
		<? endif; ?>

	<? else: ?>
		<? if ($arItem["PERMISSION"] > "D"): ?>
			<? if ($arItem["DEPTH_LEVEL"] == 1): ?>
				<li>
					<a href="<?= $arItem["LINK"]; ?>" <?= $arItem["SELECTED"] ? 'class="act"' : ''; ?>><?= $arItem["TEXT"] ?></a>
				</li>
			<? else: ?>
				<li>
					<a href="<?= $arItem["LINK"] ?>" <?= $arItem["SELECTED"] ? 'class="act"' : ''; ?>><?= $arItem["TEXT"] ?></a>
				</li>
			<? endif; ?>
		<? else: ?>
			<? if ($arItem["DEPTH_LEVEL"] == 1): ?>
				<li><a href="" title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a></li>
			<? else: ?>
				<li><a href="" title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a></li>
			<? endif; ?>
		<? endif; ?>
	<? endif; ?>

	<? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>

	<? endforeach; ?>

					<? if ($previousLevel > 1)://close last item tags?>
						<?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
					<? endif ?>

</ul>
<? endif; //If !empty(arResult) ?>







