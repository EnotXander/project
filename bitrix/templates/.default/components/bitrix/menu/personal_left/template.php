<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
   <ul class="left_menu">
      <?foreach ($arResult as $arItem):
         if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;?>
         <? if ($arItem["PERMISSION"] > "D"): ?>
            <li<?=($arItem["SELECTED"]) ? ' class="selected"' : ''?><?=($arItem["DEPTH_LEVEL"]>1) ? ' style="margin-left: 10px;"' : ''?>><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
         <? endif; ?>
      <? endforeach ?>
   </ul>
<?endif?>