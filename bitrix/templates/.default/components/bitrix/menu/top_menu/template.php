<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
} ?>
<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();

$arClasses = is_array($arParams['CLASSES']) ? $arParams['CLASSES'] : Array( "sm_market", "sm_companies", "sm_advertising", "sm_tenders");
if ($arResult[3]['TEXT'] == '�������� �������'){
	unset($arResult[3]);
	$arResult = array_values($arResult);
}

?>
<div class="mm_top">
	<div class="all_services">
		<div class="all_services_list" id="allServicesListID">
			<div class="as_header">
				<a class="as_lnk" href="javascript:void(0)" onClick="jQuery('#allServicesListID').toggle();">
					<span class="icons"></span> <span class="dotted">��� �������</span></a>

				<div class="as_header_bottom"></div>
			</div>
			<!--as_header-->
			<ul class="as_list">
				<?
				$previousLevel = 0;
				foreach ( $arResult as $key => $arItem ):?>
					<?if ($key <= $arParams['SECOND']) continue; ?>
					<li>
						<div class="ttt">
							<?if ( $dir == $arItem["LINK"] ) {?>
								<span class="ra"><?= $arItem["TEXT"] ?></span>
							<? } else { ?>
								<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
							<?} ?>
						</div>
					</li>
				<? endforeach ?>
			</ul>
		</div>
		<!--all_services_list-->
		<a class="as_lnk" href="javascript:void(0)" onClick="jQuery('#allServicesListID').toggle();">
			<span class="icons"></span><span class="dotted">��� �������</span>
		</a>
	</div>
	<!--all_services-->
	<?
	$previousLevel = 0;
	foreach ($arResult as $key => $arItem):?>
		<? if ($key == 0): ?>
			<div class="mm_static_menu">
		<? elseif ($key == $arParams['FIRST']): ?>
			</div>
			<!--static_menu-->
			<div class="mm_sub_menu">
		<? endif; ?>

		<? if ( $key <  $arParams['FIRST'] ): ?>
			<?if ( $arItem["LINK"] == $dir ) {?>
				<span class="ar <?= $arClasses[ $key ] ?> <? if ( $arItem["SELECTED"] ): ?> sel<? endif; ?>">
				<i class="icons icon_mm_arr_sel"></i><?= $arItem["TEXT"] ?></span>
			<? } else { ?>
				<a href="<?= $arItem["LINK"] ?>" class="<?= $arClasses[ $key ] ?> <? if ( $arItem["SELECTED"] ): ?> sel<? endif; ?>">
				<i class="icons icon_mm_arr_sel"></i><?= $arItem["TEXT"] ?></a>
			<?} ?>
		<? else: ?>
			<?if ( $arItem["LINK"] == $dir  ) {?>
				<span class="ar sel" ><?= $arItem["TEXT"] ?></span>
			<? } else { ?>
				<a href="<?= $arItem["LINK"] ?>" class="<? if ( $arItem["SELECTED"] ): ?> sel<? endif; ?>"><?= $arItem["TEXT"] ?></a>
			<?} ?>
		<? endif;?>

		<?if ($key >= $arParams['SECOND']) break; ?>

	<? endforeach; ?>

	</div>
</div><!--mm_top-->
