<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<? if (count($arResult["ITEMS"])): ?>
   <ul class="left_menu" style="margin-bottom: 0px;">
      <?foreach ($arResult["ITEMS"] as $arItem):?>
         <? if ($arItem["PERMISSION"] > "D"): ?>
            <li class="<?=$arItem["SELECTED"] ? 'selected' : ''?><?=($arItem["DEPTH_LEVEL"] == 1) ? ' submenu-box js-submenu-open' : ''?>">
               <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
               <?if(count($arItem["SUB"])):?>
                  <ul class="popup_menu js-popup_menu">
                     <?foreach ($arItem["SUB"] as $arSubItem):?>
                        <li><a href="<?= $arSubItem["LINK"] ?>"><?= $arSubItem["TEXT"] ?></a></li>
                     <? endforeach ?>
                  </ul>
               <? endif; ?>
            </li>
         <? endif; ?>
      <? endforeach ?>
   </ul>
<?endif?>

   
<? if (count($arResult["ITEMS_UNVISIBLE"])): ?>
   <ul class="left_menu" id="js_show_sections" style="display: none;">
      <?foreach ($arResult["ITEMS_UNVISIBLE"] as $arItem):?>
         <? if ($arItem["PERMISSION"] > "D"): ?>
            <li class="<?=$arItem["SELECTED"] ? 'selected' : ''?><?=($arItem["DEPTH_LEVEL"] == 1) ? ' submenu-box js-submenu-open' : ''?>">
               <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
               <?if(count($arItem["SUB"])):?>
                  <ul class="popup_menu js-popup_menu">
                     <?foreach ($arItem["SUB"] as $arSubItem):?>
                        <li><a href="<?= $arSubItem["LINK"] ?>"><?= $arSubItem["TEXT"] ?></a></li>
                     <? endforeach ?>
                  </ul>
               <? endif; ?>
            </li>
         <? endif; ?>
      <? endforeach ?>
   </ul>
   <a class="btn_show_sections js_btn_show_sections closed" id="<?=$arResult['SECTION']['PATH'][0]['ID']?>" ><span>Развернуть</span><i class="icons"></i></a>
<?endif?>