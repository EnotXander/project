<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$items = array();
$counter = array(1 => 0);
foreach ($arResult as $arItem)
{
   $depth = $arItem["DEPTH_LEVEL"];
   if ($depth <= $arParams["MAX_LEVEL"])
   {
      if($depth == 1)
      {
         $arItem["SUB"] = array();
         $items[$counter[1]] = $arItem;
         $counter[2] = 0;
         $counter[1]++;
      }
      elseif($depth == 2)
      {
         $items[$counter[1]-1]["SUB"][] = $arItem;
      }
   }
}

$arResult["ITEMS_UNVISIBLE"] = array();
if ($arParams["VISIBLE_COUNT"] && (count($items) > $arParams["VISIBLE_COUNT"]))
{
   $arResult["ITEMS_UNVISIBLE"] = array_slice($items, $arParams["VISIBLE_COUNT"]);
   $arResult["ITEMS"] = array_splice($items, 0, $arParams["VISIBLE_COUNT"]);
}
else
   $arResult["ITEMS"] = $items;