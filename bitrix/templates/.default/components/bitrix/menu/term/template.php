<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="left_menu">

<?
$previousLevel = 0;
foreach($arResult as $key => $arItem):
	/*if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;*/
?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>
	<?if($arItem["IS_PARENT"]):?>
		<li>
			<div class="parent_title">
				<div class="roll title_line"><a href="#"><div id="close"></div></a></div>
				<span class="title_line"><?=$arItem["TEXT"]?></span>
			</div>
			<ul class="sub_left_menus">
	<?else:?>
		<?if ($arItem['DEPTH_LEVEL'] == 1):?>
			<li>
				<div class="parent_title">
					<div class="roll title_line"></div>
					<span class="title_line"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></span>
				</div>
			</li>
		<?else:?>
			<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
		<?endif;?>
	<?endif?>
	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
	
<?endforeach?>
<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
</ul>
<?endif?>