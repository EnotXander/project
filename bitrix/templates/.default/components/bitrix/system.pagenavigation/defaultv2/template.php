<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//$APPLICATION->AddHeadString('<script type="text/javascript" src="http://'.SITE_SERVER_NAME.SITE_TEMPLATE_PATH.'/js/jquery.tools.js"></script>', true);
//$APPLICATION->AddHeadString('<script type="text/javascript" src="http://'.SITE_SERVER_NAME.SITE_TEMPLATE_PATH.'/js/paging.js"></script>', true);

$arResult["NavQueryString"] = trim(str_replace($APPLICATION->GetCurPage(), "", $APPLICATION->GetCurPageParam("CURPAGE={$arResult["NavNum"]}", array("SECTION_ID", "PAGEN_".$arResult["NavNum"], "CURPAGE"))), "?");
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

if ($arResult["NavPageCount"] > 1) {
   ?>
      <div class="b-pages">
         <span class="meta"></span>
         <ul class="pages-nav">
            <?
            if ($arResult["bDescPageNumbering"] === true):
               $bFirst = true;
               if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                  if ($arResult["bSavePage"]):
                     ?>

                     <li class="page-prev"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><i class="off"></i></a></li>
                     <?
                  else:
                     if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)):
                        ?>
                        <li class="page-prev"><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><i class="off"></i></a></li>
                        <?
                     else:
                        ?>
                        <li class="page-prev"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><i class="off"></i></a></li>
                     <?
                     endif;
                  endif;
                  ?>
                  <?
                  if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
                     $bFirst = false;
                     if ($arResult["bSavePage"]):
                        ?>
                        <li><a class="blog-page-first asz" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= $arResult["NavPageCount"] ?>">1</a></li>
                        <?
                     else:
                        ?>
                        <li><a class="blog-page-first azx" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a></li>
                     <?
                     endif;
                     ?>
                     <?
                     if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):
                        ?>
                        <li class="exppages">
                           <a href="#" class="exppages-ttl js-exppages-id-<?= $arResult["NavNum"] ?>">
                              ...
                              <i class="ar"></i>
                           </a>
                        </li>
                        <script type="text/javascript">
                           PaginationSlider(<?=$arResult["NavPageCount"]?>, 1, <?=$arResult["NavPageNomer"]?>, "<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=", <?= $arResult["NavNum"] ?>);
                        </script>
                        <?
                     endif;
                  endif;
               endif;
               do {
                  $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

                  if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                     ?>
                     <li><a class="hr" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= $NavRecordGroupPrint ?>"><?= $NavRecordGroupPrint ?></a></li>
                     <?
                  elseif ($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
                     ?>
                     <li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="<?= ($bFirst ? "blog-page-first" : "") ?>"><?= $NavRecordGroupPrint ?></a></li>
                     <?
                  else:
                     ?>
                     <li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= $arResult["nStartPage"] ?>"<?
                     ?> class="<?= ($bFirst ? "blog-page-first" : "") ?>"><?= $NavRecordGroupPrint ?></a></li>

                  <?
                  endif;
                  ?>
                  <?
                  $arResult["nStartPage"]--;
                  $bFirst = false;
               } while ($arResult["nStartPage"] >= $arResult["nEndPage"]);

               if ($arResult["NavPageNomer"] > 1):
                  if ($arResult["nEndPage"] > 1):
                     if ($arResult["nEndPage"] > 2):
                        ?>
                        <li class="exppages">
                           <a href="#" class="exppages-ttl js-exppages-id-<?= $arResult["NavNum"] ?>">
                              ...
                              <i class="ar"></i>
                           </a>
                        </li>
                        <script type="text/javascript">
                           PaginationSlider(<?=$arResult["NavPageCount"]?>, 1, <?=$arResult["NavPageNomer"]?>, "<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=", <?= $arResult["NavNum"] ?>);
                        </script>
                        <?
                     endif;
                     ?>
                     <li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=1"><?= $arResult["NavPageCount"] ?></a></li>
                     <?
                  endif;
                  ?>
                  <li class="page-next"><a class="blog-page-next" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><i class="off"></i></a></li>
                  <?
               endif;

            else:
               $bFirst = true;

               if ($arResult["NavPageNomer"] > 1):
                  if ($arResult["bSavePage"]):
                     ?>
                     <li class="page-prev"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><i class="off"></i></a></li>
                     <?
                  else:
                     if ($arResult["NavPageNomer"] > 1):
                        ?>
                        <li class="page-prev"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><i class="off"></i></a></li>
                        <?
                     else:
                        ?>
                        <li class="page-prev"><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><i class="off"></i></a></li>
                     <?
                     endif;

                  endif;
                  ?>
                  <?
                  if ($arResult["nStartPage"] > 1):
                     $bFirst = false;
                     if ($arResult["bSavePage"]):
                        ?>
                        <li><a class="blog-page-first" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a></li>
                        <?
                     else:
                        ?>
                        <li><a class="blog-page-first" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a></li>
                     <?
                     endif;
                     ?>
                     <?
                     if ($arResult["nStartPage"] > 2):
                        ?>
                        <li class="exppages">
                           <a href="#" class="exppages-ttl js-exppages-id-<?= $arResult["NavNum"] ?>">
                              ...
                              <i class="ar"></i>
                           </a>
                        </li>
                        <script type="text/javascript">
                           PaginationSlider(<?=$arResult["NavPageCount"]?>, 1, <?=$arResult["NavPageNomer"]?>, "<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=", <?= $arResult["NavNum"] ?>);
                        </script>
                        <?
                     endif;
                  endif;
               endif;

               do {
                  if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                     ?>
                     <li><a class="hr" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a></li>
                     <?
                  elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
                     ?>
                     <li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= $arResult["nStartPage"] ?>" class="<?= ($bFirst ? "blog-page-first" : "") ?>"><?= $arResult["nStartPage"] ?></a></li>
                     <?
                  else:
                     ?>
                     <li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= $arResult["nStartPage"] ?>"<?
                     ?> class="<?= ($bFirst ? "blog-page-first" : "") ?>"><?= $arResult["nStartPage"] ?></a></li>
                     <?
                     endif;
                     ?>
                     <?
                     $arResult["nStartPage"]++;
                     $bFirst = false;
                  } while ($arResult["nStartPage"] <= $arResult["nEndPage"]);

                  if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                     if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
                        if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):
                           ?>
                        <li class="exppages">
                           <a href="#" class="exppages-ttl js-exppages-id-<?= $arResult["NavNum"] ?>">
                              ...
                              <i class="ar"></i>
                           </a>
                        </li>
                        <script type="text/javascript">
                           PaginationSlider(<?=$arResult["NavPageCount"]?>, 1, <?=$arResult["NavPageNomer"]?>, "<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=", <?= $arResult["NavNum"] ?>);
                        </script>
                        <?
                     endif;
                     ?>
                     <li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a></li>
                     <?
                  endif;
                  ?>
                  <li class="page-next"><a class="blog-page-next" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?=$arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><i class="off"></i></a></li>
                     <?
                  endif;
               endif;
               ?>
         </ul>
         <div class="b-pageslider">
            <div class="b-pageslider-i">
               <ul class="pagesslider__ul">

                  <li><a href="#">8</a></li>
                  <li><a class="slider-hr" href="#">9</a></li>
                  <li><a href="#">10</a></li>

               </ul>
            </div>
            <div class="pages-slider-track">
               <i class="pages-slider-drag"></i>
            </div>
            <i class="b-pageslider-shd-l"></i>
            <i class="b-pageslider-shd-r"></i>     
         </div>
      </div>
   <?
}
