<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 

<?  ob_start();
$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
    "TYPE" => "MAIN_M",
    "NOINDEX" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "0"
        ), false
);
$bannerMainLentaHTML = ob_get_clean();
if(strlen($bannerMainLentaHTML)):
   $bannerMainLentaExists = true;?>
   <div class="catalog_list_banner" style="height:85px; background-color:#cad7c5;;">
      <?=$bannerMainLentaHTML;?>
   </div>
<?else:
   $bannerMainLentaExists = false;
endif;?>

<ul class="title_list"<?=!$bannerMainLentaExists ? ' style="height: 330px;"' : ''?>>
   <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
      <li style="margin-bottom: 5px; clear:both;">
         <p class="image_box" style="float:left;">
            <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
               <a title="Посмотреть полностью" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img alt="" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" ></a>
            <? else: ?> <!-- style="width:36px; height:36px; opacity:0.5; margin-right:10px;"  style="width:36px; height:36px; opacity:0.5;margin-right:10px;" -->
               <img src="<?= SITE_TEMPLATE_PATH ?>/images/withoutphoto36x36.jpg"  />
            <? endif; ?>
         </p><a href="<? echo $arItem["DETAIL_PAGE_URL"]; ?>" ><? echo $arItem["NAME"] ?></a><span class="date"></span>
      </li>
      <div class="clearfix"></div>
   <? endforeach; ?>
</ul>
