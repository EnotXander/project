<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
	foreach ($arResult["ITEMS"] as $key => $arElement) 
	{
		if(is_array($arElement["PREVIEW_PICTURE"]))
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arElement["PREVIEW_PICTURE"],
				array("width" => 36, "height" => 36),
				BX_RESIZE_IMAGE_EXACT,
				true
			);
			//$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);

			$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
				"SRC" => $arFileTmp["src"],
				"WIDTH" => $arFileTmp["width"],
				"HEIGHT" => $arFileTmp["height"],
			);

		}
		//$arResult["ITEMS"][$key]["NAME"] = TruncateText($arElement["NAME"], 60);
		
		if(strlen($arElement["NAME"]) > 60)
			$arResult["ITEMS"][$key]["NAME"] = cropStr($arElement["NAME"], 60)." ...";

		
	}
}

?>