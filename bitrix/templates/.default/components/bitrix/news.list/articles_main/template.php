<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="gray_box news_list tab">
   <?foreach($arResult["ITEMS"] as $arItem):?>
      <?
      $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
      ?>
      <div class="item_row">
      <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["DETAIL_PICTURE"])):?>
         <div class="image_box"><div class="image_box_m"><a href="<?echo $arItem["DETAIL_PAGE_URL"];?>" ><img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"  /></a></div></div>
      <?endif;?>
         <div class="row"> 						 
            <div class="title"><a href="<?echo $arItem["DETAIL_PAGE_URL"];?>" ><?echo $arItem["NAME"]?></a></div>
            <p>
               <?if(is_array($arItem["RIB"]) && count($arItem["RIB"])):?>
                  <a href="/articles/<?=$arItem["RIB"]["CODE"]?>/" class="news_section <?=(!strlen($arItem["RIB"]["COLOR"])) ? $arItem["RIB"]["CLASS"] : ''?>"<?=(strlen($arItem["RIB"]["COLOR"])) ? 'style="background: '.$arItem["RIB"]["COLOR"].'"' : ''?>><span><?=$arItem["RIB"]["NAME"]?></span></a>
               <?endif;?>
               <?echo $arItem["PREVIEW_TEXT"];?><span class="date">\ <?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span></p>
         </div>
      </div>
   <?endforeach;?>
   <div class="nav_row mar_0_top"><a href="<?=str_replace('#SITE_DIR#','',$arResult['LIST_PAGE_URL'])?>" class="bold" style="text-decoration:none;">��� ����������<?//=strtolower($arResult['NAME'])?></a></div>
</div>
