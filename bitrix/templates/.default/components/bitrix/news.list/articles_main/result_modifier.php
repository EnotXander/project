<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(!CModule::IncludeModule("iblock")) die();
$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      //$rsSection = CIBlockSection::GetByID($arElement["IBLOCK_SECTION_ID"]);
      $rsSection = CIBlockSection::GetList(
              array("SORT"=>"ASC"),
              array("IBLOCK_ID" => $arElement["IBLOCK_ID"], "ID" => $arElement["IBLOCK_SECTION_ID"]),
              false,
              array("ID", "NAME", "CODE", "UF_COLOR", "UF_SHORT_TITLE")
              );
      if($arSection = $rsSection->GetNext())
      {
         $arElement["RIB"] = array(
             "ID" => $arSection["ID"],
             "CODE" => $arSection["CODE"],
             "NAME" => $arSection["NAME"],
             "CLASS" => "rib_blue",
             "COLOR" => $arSection["UF_COLOR"]
         );
         if(strlen($arSection["UF_SHORT_TITLE"]))
            $arElement["RIB"]["NAME"] = $arSection["UF_SHORT_TITLE"];
      }
      $arElement["DETAIL_TEXT"] = strip_tags($arElement["DETAIL_TEXT"]);
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], 180);
      $arResult['ITEMS'][$key] = $arElement;
      
      if(is_array($arElement["PREVIEW_PICTURE"])) $arElement["DETAIL_PICTURE"] = $arElement["PREVIEW_PICTURE"];
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
                         $arElement["DETAIL_PICTURE"], array("width" => 250, "height" => 250), BX_RESIZE_IMAGE_EXACT, true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arResult["ITEMS"][$key]["DETAIL_PICTURE_BIG"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
         $arFileTmp = CFile::ResizeImageGet(
                         $arElement["DETAIL_PICTURE"], array("width" => 100, "height" => 100), BX_RESIZE_IMAGE_EXACT, true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
   }
}
?>