<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(count($arResult["ITEMS"])>0):?>
	<div style="text-align: center;">
	   <?if(is_array($arResult["PICTURE_RESIZED"]) && count($arResult["PICTURE_RESIZED"])):?>
	         <img style="border-radius: 5px;" src="<?=$arResult["PICTURE_RESIZED"]["src"]?>" alt="" />
	   <?endif;?>
	</div>	
<?endif;?>