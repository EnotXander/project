<?

if (!CModule::IncludeModule("iblock"))
   return;

$resizeParams = array('width' => $arParams["PICTURE_WIDTH"], 'height' => $arParams["PICTURE_HEIGHT"]);

if(count($arResult["ITEMS"])>0):
	foreach ($arResult["ITEMS"] as $key => $item)
	{
	
	   if (is_array($item["DETAIL_PICTURE"]))
	   {
	      $image = $item["DETAIL_PICTURE"];
	      //PrintObject($image["WIDTH"]);
	      //PrintObject($arParams["MINIMUM_WIDTH"]);
	      if ($image["WIDTH"] >= $arParams["MINIMUM_WIDTH"])
	      {
	         $arFileTmp = CFile::ResizeImageGet(
	                         $image, $resizeParams, BX_RESIZE_IMAGE_EXACT, true
	         );
	         //PrintObject("BREAK!");
	         $arResult["PICTURE_RESIZED"] = $arFileTmp;
	      }
	      else
	      {
	         $arResult["PICTURE_RESIZED"] = $image;
	      }
	   }
	}
endif;
?>
