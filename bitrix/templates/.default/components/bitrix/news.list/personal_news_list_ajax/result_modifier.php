<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule("iblock");

$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      $arElement["DETAIL_TEXT"] = $arElement["DETAIL_TEXT"];
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], 200);
      $arResult['ITEMS'][$key] = $arElement;
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
                         $arElement["DETAIL_PICTURE"], array("width" => 100, "height" => 100), BX_RESIZE_IMAGE_EXACT, true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
   }
}

//пагинация
if((int)$_REQUEST["per_page"])
{
   $arResult["per_page"] = $_REQUEST["per_page"];
}