<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="middle_col">
    <h1><?$APPLICATION->ShowTitle();?></h1>
    <div class="clearfix"></div>
<?//PrintAdmin($arResult);?>
<div class="paging">
   <div class="show_col">
      <form method="get" name="top_f">
         <span class="meta">���������� ��:</span>
         <select name="per_page" onchange="document.top_f.submit()">
            <option value="15" <? if ($arParams['NEWS_COUNT'] == 15) echo "selected"; ?>>15</option>
            <option value="25" <? if ($arParams['NEWS_COUNT'] == 25) echo "selected"; ?>>25</option>
            <option value="50" <? if ($arParams['NEWS_COUNT'] == 50) echo "selected"; ?>>50</option>
         </select>
      </form>
   </div>
   <div style="float: left;">
      <form method="post" name="top_f">
         <?=bitrix_sessid_post()?>
         <input type="submit" name="enable_all" value="������������ ���">
         <input type="submit" name="delete_all" value="������� ���">
      </form>
   </div>
   <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
      <?= $arResult["NAV_STRING"] ?>
   <? endif; ?>
</div>
<div class="mc_block">
   <div class="news_list">
      <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
         <?
         $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
         $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
         ?>
         <div class="item_row" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="image_box">
               <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["DETAIL_PICTURE"])): ?>
                  <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                     <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <img class="preview_picture" border="0" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arItem["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" style="float:left" />
                     </a>
                  <? else: ?>
                     <img class="preview_picture" border="0" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arItem["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" style="float:left" />
                  <? endif; ?>
               <? endif ?>
            </div>
            <div class="row">
               <div class="title">
                  <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
               </div>
               <p><? echo $arItem["PREVIEW_TEXT"]; ?><span class="date">\ <? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span></p>
               <? foreach ($arItem["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
                  <div class="meta a_autor"><?= $arProperty["NAME"] ?>: <?= $arProperty["USER"]["LAST_NAME"] . " " . $arProperty["USER"]['NAME']; ?></div>
               <? endforeach; ?>
               <?
               $arTags = explode(", ", $arItem['TAGS']);
               foreach ($arTags as $key => $tag)
                  $arTags[$key] = "<a href='/articles/search/?tags=" . urlencode($tag) . "'>" . $tag . "</a>";
               ?>
               <? if (strlen($arItem['TAGS']) > 0): ?><div class="meta a_tags">����: <? echo implode(", ", $arTags); ?></div><? endif; ?>
            </div>
         </div><!--item_row-->
      <? endforeach; ?>
   </div>
</div>
<div class="paging mar_20_bot">
   <div class="show_col">
      <form method="get" name="bot_f">
         <span class="meta">���������� ��:</span>
         <select name="per_page" onchange="document.bot_f.submit()">
            <option value="15" <? if ($arParams['NEWS_COUNT'] == 15) echo "selected"; ?>>15</option>
            <option value="25" <? if ($arParams['NEWS_COUNT'] == 25) echo "selected"; ?>>25</option>
            <option value="50" <? if ($arParams['NEWS_COUNT'] == 50) echo "selected"; ?>>50</option>
         </select>
      </form>
   </div>
   <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
      <?= $arResult["NAV_STRING"] ?>
   <? endif; ?>
</div>
</div>
<? //echo "<pre>"; print_r($arResult); echo "</pre>"; ?>
