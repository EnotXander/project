<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
global $GBL_SHOW_FAST_ORDER;
$GBL_SHOW_FAST_ORDER = true;
?>

<?if ($_REQUEST['action'] == 'ADD_TO_COMPARE_LIST')
   $_SESSION['FAVOR' . $arParams['IBLOCK_ID']]['ITEMS'][] = (int)$_REQUEST['id'];?>

<div class="mc_block">
   <!--<ul class="mb_menu">
      <li<? if ($_REQUEST['FAVOR'] <> 'Y'): ?> class="sel"<? endif; ?>>
         <a href="?FAVOR=N">� ���������</a>
         <div class="sclew"></div>
      </li>
      <li>
         <a href="#1">������ ����</a>
         <div class="sclew"></div>
      </li>
      <li<? if ($_REQUEST['FAVOR'] == 'Y'): ?> class="sel"<? endif; ?>>
         <a href="?FAVOR=Y"><i class="icons icon_star star_sel"></i>����������</a>
         <div class="sclew"></div>
      </li>
   </ul>
   <div class="clear"></div>-->
   <div class="paging brdr_bot_0 brdr_box">
      <div class="show_col">
         <form method="get" name="top_f">
            <!--<input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">-->
            <span class="meta">���������� ��:</span>
            <select name="per_page" onchange="document.top_f.submit()">
               <option value="20" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 20) echo "selected"; ?>>20</option>
               <option value="50" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 50) echo "selected"; ?>>50</option>
               <option value="100" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 100) echo "selected"; ?>>100</option>
            </select>
         </form>
      </div>
      <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
         <?= $arResult["NAV_STRING"] ?>
      <? endif; ?>
   </div><!--paging-->


   <? foreach ($arResult["ITEMS"] as $arItem): ?>
   <div class="ads_3_row ads_row_wrapblock">
      <div class="ads_row_left ">
         <div class="ads_row premium<?=$arItem["HIDE"] ? ' item_hide' : ''?>">
            <table>
               <tbody>
                  <tr>
                     <td class="ic_star" style="vertical-align:top;">
                        <?if($USER->IsAuthorized()):?>
                           <a href="javascript:void(0)" onclick="AddToFavorites({
                                       productId: <?=$arItem["ID"]?>,
                                      context: $(this).find('i'),
                                      inFavorites: function(){this.addClass('marked')},
                                      outFavorites: function(){this.removeClass('marked')}
                           })">
                              <i class="i_star<? if(in_array($USER->GetID(), $arItem['PROPERTIES']["FAVORITES"]['VALUE'])):?> marked<? endif; ?>"></i>
                           </a>
                        <?endif;?>
                     </td>
                     <td class="image_box">
                        <a title="���������� ���������" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                           <?if(strlen($arItem["PREVIEW_PICTURE"]["SRC"]) && file_exists($_SERVER['DOCUMENT_ROOT'].$arItem["PREVIEW_PICTURE"]["SRC"])):?>
                              <img alt="" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                           <?else:?>
                              <img src="/images/search_placeholder.png" />
                           <?endif;?>
                        </a>
                     </td>
                     <td class="ads_info">
                        <div class="tdWrap">
                           <div class="ads_title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?> </a></div>
                           <div class="ads_text"><?= $arResult['BRANDS'][$arItem['PROPERTIES']['FIRM']['VALUE']]['NAME'] ?></div>
                           <div class="meta"><?=$arItem["PREVIEW_TEXT"]?> </div>
                        </div>                      
                     </td>
                     <td class="ads_price">
                        <div class="tdWrap">
                           <?if($arItem["PROPERTY_COST_VALUE"]):?>
                              <div class="sum"><?=$arItem["PROPERTY_COST_VALUE"]?></div>
                              <div class="curr-val"><?=$arItem["PROPERTY_CURRENCY_FORMATED"]?></div>
                           <?else:?>
                              <div class="sum">���� ���������</div>
                           <?endif;?>
									<!--<a href="<?=$arItem["PREDSTAVITEL_MESSAGE_LINK"]?>" class="buy-button">������</a>-->
									<a href="javascript: void(0);" class="buy-button" onclick="javascript:showPopup({id: $('.fast_order_popup'), productID: <?=$arItem["ID"]?>, sender: $(this)})"></a>
                        </div>
                     </td>
                  </tr>
                  <?if(($arItem["HIDE_PANEL"]) && $arGroup["ITEMS"][$arItemKey+1]["HIDE"]):?>
                     <tr>
                        <td colspan="3">
                           <div class="show_position">
                              <a href="javascript:void(0)" class="js-hideitems-show" data-more="<?=$arParams["COMPANY_ELEMENT_MORE_COUNT"]?>">
                                 <i class="plus_icon"></i>
                                 <span>�������� <?=$arParams["COMPANY_ELEMENT_MORE_COUNT"] ? "���"/*." {$arParams["COMPANY_ELEMENT_MORE_COUNT"]} ������"*/ : " ��� �������"?></span></a>
                           </div>
                        </td>
                     </tr>
                  <?endif;?>
               </tbody>
            </table>
         </div><!--ads_row-->
      </div>
      
      <?if($arItem['PROPERTIES']['FIRM']['VALUE']):
         $arCompany = $arResult["BRANDS"][$arItem['PROPERTIES']['FIRM']['VALUE']];?>
         <?if(is_array($arCompany) && count($arCompany)):?>
            <div class="ads_row_right">
               <div class="ads_row_rheader"onclick="window.open('<?=$arCompany["DETAIL_PAGE_URL"]?>','_blank'); return false;">
                  <?if(is_array($arCompany["PREVIEW_PICTURE"]) && file_exists($_SERVER['DOCUMENT_ROOT'].$arCompany["PREVIEW_PICTURE"]["SRC"])):?>
                     <img src="<?=$arCompany["PREVIEW_PICTURE"]["SRC"]?>" alt="" />
                  <?else:?>
                     <img src="/images/search_placeholder.png" />
                  <?endif;?>
                  <span class="company-name"<?=(strlen($arCompany["NAME_CUT"]) != strlen($arCompany["NAME"])) ? " title='".$arCompany["NAME"]."'" : ''?>><?=$arCompany["NAME_CUT"]?><sup><?=$arCompany["ITEMS_COUNT"][$arList["ID"]]?></sup></span>
						<span class="company-link">�������� ��������</span>
               </div>
            </div>
         <?endif;?>
      <?endif;?>
      <div class="clear"></div>
      
   </div>
   
      <?/* <!--<div class="bds_row premium js-fav-item">
         <table>
            <tbody>
               <tr>
                  <td class="ic_star" style="vertical-align:top;"></td>
                  <td class="image_box">
                     <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                        <a title="���������� ���������" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                           <img alt="" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                        </a>
                     <? else: ?>
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/withoutphoto.jpg" width="72" height="72" align=""/>  
                     <? endif; ?>
                  </td>
                  <td class="bds_info">
                     <div class="tdWrap">
                        <div class="bds_title"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></div>
                        <div class="bds_text">���: <? echo $arItem['DISPLAY_PROPERTIES']['NUMBER']['DISPLAY_VALUE'] ?></div>
                        <div class="meta"><?= $arItem['PREVIEW_TEXT'] ?></div>
                     </div>                      
                  </td>
                  <td class="bds_price">
                     <b><?= $arItem['DISPLAY_PROPERTIES']['COST']['DISPLAY_VALUE'] ?> BYR</b>
                  </td>
                  <td class="bds_nav">
                     <? if (in_array($arItem['PROPERTIES']['FIRM']['VALUE'], $arResult["BRANDS_IDS"])): ?>
                        <?=$arResult["BRANDS"][$arItem['PROPERTIES']['FIRM']['VALUE']]["PREVIEW_PICTURE_FORMATED"];?>
                        </br>
                        <?= $arItem['DISPLAY_PROPERTIES']['FIRM']['DISPLAY_VALUE'] ?>
                     <? endif; ?>
                  </td>
                  <td class="favorites-delete"><span onclick="AddToFavorites({productId: <?=$arItem['ID']?>, context: $(this).closest('.bds_row'), outFavorites: function(){this.remove(); if(!$('.js-fav-item').size()) $('.js-fav-noitem').show()}})">X</span></td>
               </tr>
            </tbody>
         </table>
      </div>--> */?>
   <? endforeach; ?>
   <div class="bds_row js-fav-noitem" style="padding: 25px;<?=(is_array($arResult["ITEMS"]) && count($arResult["ITEMS"])) ? ' display: none;' : ''?>">
      <?if($arParams["IBLOCK_ID"] == 58):?>
         <span>�� �� �������� �� ������ <a href="/market/">������</a> � ���������.</span>
      <?else:?>
         <span>�� �� �������� �� ���� <a href="/services/">������</a> � ���������.</span>
      <?endif;?>
   </div>         

   <div class="paging brdr_top_0 brdr_box mar_20_bot">
      <div class="show_col">
         <form method="get" name="bot_f">
            <!--<input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">-->
            <span class="meta">���������� ��:</span>
            <select name="per_page" onchange="document.bot_f.submit()">
               <option value="20" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 20) echo "selected"; ?>>20</option>
               <option value="50" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 50) echo "selected"; ?>>50</option>
               <option value="100" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 100) echo "selected"; ?>>100</option>
            </select>
         </form>
      </div>
      <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
         <?= $arResult["NAV_STRING"] ?>
      <? endif; ?>
   </div>
</div>
