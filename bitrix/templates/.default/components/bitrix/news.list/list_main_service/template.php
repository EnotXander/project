<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<div class="catalog_list_banner" style="height:80px; background-color:#cad7c5;;">

<?
$APPLICATION->IncludeComponent(
        "bitrix:advertising.banner", "", Array(
    "TYPE" => "MAIN_M",
    "NOINDEX" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600"
        ), false
);
?></div>
<ul class="title_list" style="height: 250px">
   <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
	  <?
	   $eto = 67;
	   if ( strlen($arItem["NAME"]) > $eto )
		   $arItem["NAME"] = substr($arItem["NAME"], 0, $eto).'...';
	  ?>
      <li style="margin-bottom: 5px; clear:both;">
         <p class="image_box" style="float:left;">
            <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
               <a title="Посмотреть полностью" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img alt="" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" style="width:36px; height:36px; margin-right:10px;"></a>
            <? else: ?>
               <img src="<?= SITE_TEMPLATE_PATH ?>/images/withoutphoto36x36.jpg"  align=""  style="width:36px; height:36px; margin-right:10px;"/>
            <? endif; ?>
         </p><a href="<? echo $arItem["DETAIL_PAGE_URL"]; ?>" ><? echo $arItem["NAME"] ?></a><span class="date"></span>
      </li>
      <div class="clearfix"></div>
   <? endforeach; ?>
</ul>
