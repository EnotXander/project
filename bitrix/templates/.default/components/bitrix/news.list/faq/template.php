<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div>
    <h1><?$APPLICATION->ShowTitle();?></h1>
    <div class="clearfix"></div>

<?foreach($arResult["ITEMS"] as $element):?>
   <div class="link js-faq-block js-faq-block-<?=$element["ID"]?>">
      <a onclick="$('.js-faq-content-<?=$element["ID"]?>').slideToggle('slow');" href="javascript://" class="splLink" > 
         <h2><b><?=$element["NAME"]?></b></h2>
      </a> 
      <div>
         <?=$element["PREVIEW_TEXT"]?>
      </div>
      <div class="js-faq-content-<?=$element["ID"]?>" style="display: none;"> 
         <?=$element["DETAIL_TEXT"]?>
      </div>
   </div>
<?endforeach;?>

</div>