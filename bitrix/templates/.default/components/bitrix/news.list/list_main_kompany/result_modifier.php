<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {

		if(is_array($arElement["PREVIEW_PICTURE"]))
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arElement["PREVIEW_PICTURE"],
				array("width" => 150, "height" => 62),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true
			);


			$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
				"SRC" => $arFileTmp["src"],
				"WIDTH" => $arFileTmp["width"],
				"HEIGHT" => $arFileTmp["height"],
			);

		}
		$arResult["ITEMS"][$key]["NAME"] = TruncateText($arElement["NAME"], 70);
   }
}
