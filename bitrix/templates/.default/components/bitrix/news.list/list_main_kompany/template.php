<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<ul class="title_list company">
   <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
	  <?
	   $eto = 67;
	   if ( strlen($arItem["NAME"]) > $eto )
		   $arItem["NAME"] = substr($arItem["NAME"], 0, $eto).'...';
	  ?>
      <li>
        <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
           <a  class="image_box company" title="Посмотреть полностью" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
               <img style="margin-top: <?= (80 - $arItem["PREVIEW_PICTURE"]["HEIGHT"]) / 2 ?>px" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" >
           </a>
        <? else: ?>
           <img src="<?= SITE_TEMPLATE_PATH ?>/images/withoutphoto36x36.jpg"/>
        <? endif; ?>
	    <a class="bt_text" href="<? echo $arItem["DETAIL_PAGE_URL"]; ?>" ><? echo $arItem["NAME"] ?></a><span class="date"></span>
      </li>
   <? endforeach; ?>
</ul>
