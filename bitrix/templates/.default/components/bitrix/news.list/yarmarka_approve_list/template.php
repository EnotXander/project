<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
} ?>

<div class="news_list">
	<table style="width: 100%; " border="1">
		<tr class="" style="" id="">
			<td>
					��������
			</td>
			<td>
				���� �����
			</td>
			<td style="padding-bottom: 5px;padding-top: 5px;">
				����������
			</td>
			<td>
				����������
			</td>
		</tr>
		<!--item_row--><? foreach ( $arResult["ITEMS"] as $key => $arItem ): ?><?
			$this->AddEditAction( $arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID( $arItem["IBLOCK_ID"], "ELEMENT_EDIT" ) );
			$this->AddDeleteAction( $arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID( $arItem["IBLOCK_ID"], "ELEMENT_DELETE" ), array( "CONFIRM" => GetMessage( 'CT_BNL_ELEMENT_DELETE_CONFIRM' ) ) );
			?>
			<tr class="" style="border-top: 2px solid black; " id="<?= $this->GetEditAreaId( $arItem['ID'] ); ?>">
				<td style="padding-top: 5px;">
					<div class="">
						<? echo $arItem["NAME"] ?>
					</div>
				</td>
				<td style="padding-top: 5px;">
					<p><? echo $arItem["DATE_CREATE"] ?></p>

				</td>
				<td style="padding-bottom: 5px;padding-top: 5px;">
					<ul style="margin-left: 20px">
						<? foreach ( $arItem["DISPLAY_PROPERTIES"] as $pid => $arProperty ): ?>
							<li><?= $arProperty["NAME"] ?>
								: <?= $arProperty["VALUE"] ?></li><? endforeach; ?>
					</ul>
				</td>
				<td>
					<input class="set_state" value="<?= $arItem['ID'] ?>" type="checkbox" <? if ( $arItem["DISPLAY_PROPERTIES"]['CLOSED']['VALUE'] == '��' ) { ?>checked="checked"<? } ?>/>
				</td>
			</tr><!--item_row--><? endforeach; ?>
	</table>
</div>

<div class="paging mar_20_bot">
	<div class="show_col">
		<form method="get" name="bot_f">
			<span class="meta">���������� ��:</span>
			<select name="per_page" onchange="document.bot_f.submit()">
				<option value="15" <? if ( $arParams['NEWS_COUNT'] == 15 ) {
					echo "selected";
				} ?>>15
				</option>
				<option value="25" <? if ( $arParams['NEWS_COUNT'] == 25 ) {
					echo "selected";
				} ?>>25
				</option>
				<option value="50" <? if ( $arParams['NEWS_COUNT'] == 50 ) {
					echo "selected";
				} ?>>50
				</option>
			</select>
		</form>
	</div><? if ( $arParams["DISPLAY_BOTTOM_PAGER"] ): ?>
		<?= $arResult["NAV_STRING"] ?><? endif; ?>
</div>
<script type="application/javascript">
	$(function(){
		$('.set_state').bind('click', function (){
			// �������� ��������� ������
			if ( $(this).is(':checked') ) {
				$.post('setstate.php', {id: $(this).val(), state: 'on'}, function(){

				});
			} else {
				$.post('setstate.php', {id: $(this).val(), state: 'off'}, function(){

				});
			}

		})
	})
</script>