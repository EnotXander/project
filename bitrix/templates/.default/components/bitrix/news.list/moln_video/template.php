<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["ITEMS"] as $key => $arItem):?>
   <?if ($key == 0):?>
      <div class="mc_block">
         <div class="main_news">
            
            <?if( (is_array($arItem["PROPERTIES"]["VIDEO"]["VALUE"]) && count($arItem["PROPERTIES"]["VIDEO"]["VALUE"]) > 0) 
                  || (strlen($arItem["PROPERTIES"]["VIDEO_LINK"]["VALUE"]) > 0)):?>
               
               <div class="video_block">

                  <? // проигрыватель
                  if(strlen($arItem["PROPERTIES"]["VIDEO_LINK"]["VALUE"]) > 0)
                  { 
                     $videoInfo = array(
                        "provider" => "youtube",
                        "path" => $arItem["PROPERTIES"]["VIDEO_LINK"]["VALUE"],
                        "width" => "675",
                        "height" => "420", 
                        "title" => "",
                        "duration" => "",
                        "author" => "",
                        "date" => "",
                        "desc" => "",
                        "CONTROLBAR" => "bottom",
                        "BUFFER_LENGTH" => 10,
                        "VOLUME" => 90,
                        "SKIN" => "stijl"
                     );
                  } else
                  {
                     $videoInfo = array(
                        "provider" => "video",
                        "path" => $arItem["PROPERTIES"]["VIDEO"]["VALUE"]["path"],
                        "width" => 675,
                        "height" => 420,
                        "title" => $arItem["PROPERTIES"]["VIDEO"]["VALUE"]["title"],
                        "duration" => $arItem["PROPERTIES"]["VIDEO"]["VALUE"]["duration"],
                        "author" => $arItem["PROPERTIES"]["VIDEO"]["VALUE"]["author"],
                        "date" => $arItem["PROPERTIES"]["VIDEO"]["VALUE"]["date"],
                        "desc" => $arItem["PROPERTIES"]["VIDEO"]["VALUE"]["desc"],
                        "CONTROLBAR" => $arItem["PROPERTIES"]["VIDEO"]["USER_TYPE_SETTINGS"]["CONTROLBAR"],
                        "BUFFER_LENGTH" => $arItem["PROPERTIES"]["VIDEO"]["USER_TYPE_SETTINGS"]["BUFFER_LENGTH"],
                        "VOLUME" => $arItem["PROPERTIES"]["VIDEO"]["USER_TYPE_SETTINGS"]["VOLUME"],
                        "SKIN" => "stijl"
                     );
                  }

                  $APPLICATION->IncludeComponent("bitrix:player", "", array(
                        "PLAYER_TYPE" => "auto",
                        "USE_PLAYLIST" => "N",
                        "PATH" => $videoInfo["path"],
                        "PLAYLIST_DIALOG" => "",
                        "PROVIDER" => $videoInfo["provider"],
                        "STREAMER" => "",
                        "WIDTH" => $videoInfo["width"],
                        "HEIGHT" => $videoInfo["height"],
                        "PREVIEW" => "",
                        "FILE_TITLE" => $videoInfo["title"],
                        "FILE_DURATION" => $videoInfo["duration"],
                        "FILE_AUTHOR" => $videoInfo["author"],
                        "FILE_DATE" => $videoInfo["date"],
                        "FILE_DESCRIPTION" => $videoInfo["desc"],
                        "SKIN_PATH" => "/bitrix/components/bitrix/player/mediaplayer/skins",
                        "SKIN" => $videoInfo["SKIN"],
                        "CONTROLBAR" => $videoInfo["CONTROLBAR"],
                        "WMODE" => "transparent",
                        "PLAYLIST" => "",
                        "PLAYLIST_SIZE" => "",
                        "LOGO" => "",
                        "LOGO_LINK" => "",
                        "LOGO_POSITION" => "",
                        "PLUGINS" => array(),
                        "PLUGINS_TWEETIT-1" => "",
                        "PLUGINS_FBIT-1" => "",
                        "ADDITIONAL_FLASHVARS" => "",
                        "WMODE_WMV" => "window",
                        "SHOW_CONTROLS" => "Y",
                        "PLAYLIST_TYPE" => "xspf",
                        "PLAYLIST_PREVIEW_WIDTH" => "",
                        "PLAYLIST_PREVIEW_HEIGHT" => "",
                        "SHOW_DIGITS" => "Y",
                        "CONTROLS_BGCOLOR" => "FFFFFF",
                        "CONTROLS_COLOR" => "000000",
                        "CONTROLS_OVER_COLOR" => "000000",
                        "SCREEN_COLOR" => "000000",
                        "AUTOSTART" => "N",
                        "REPEAT" => "none",
                        "VOLUME" => $videoInfo["VOLUME"],
                        "MUTE" => "N",
                        "HIGH_QUALITY" => "Y",
                        "SHUFFLE" => "N",
                        "START_ITEM" => 1,
                        "ADVANCED_MODE_SETTINGS" => "N",
                        "PLAYER_ID" => "",
                        "BUFFER_LENGTH" => $videoInfo["BUFFER_LENGTH"],
                        "DOWNLOAD_LINK" => "",
                        "DOWNLOAD_LINK_TARGET" => "_self",
                        "ADDITIONAL_WMVVARS" => "",
                        "ALLOW_SWF" => "Y",
                     )
                  );?>                  
               </div> 
            <?endif;?> 
            
            <div class="title"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a></div>
            <?if($arItem["PREVIEW_TEXT"]):?>
               <?echo $arItem["PREVIEW_TEXT"];?>
            <?endif;?>
         </div>
         <div class="clear"></div>
      </div><!--mc_block-->
   <?else:?>
   <?endif;?>
<?endforeach;?>
