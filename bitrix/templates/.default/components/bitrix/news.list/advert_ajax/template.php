



	<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
		<?

		switch ($arItem['PROPERTIES']['TYPE']['VALUE_XML_ID'])
		{
			case 'sale':
				$sectionClass = 'sell';
				break;
			case 'buy':
				$sectionClass = 'buy';
				break;
			case 'services':
				$sectionClass = 'services';
				break;
			default:
				$sectionClass = '';
				break;
		}
		?>

		<? if ($key == 10): ?>
			<div class="midban">
				<?$APPLICATION->IncludeComponent(
	"bitrix:advertising.banner",
	".default",
	array(
		"TYPE" => "H1_2",
		"NOINDEX" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "0",
		"CACHE_NOTES" => ""
	),
	false
);?>
			</div>
		<? endif; ?>


		<div class="bds_row<? if (strlen($arItem['PROPERTIES']['PREMIUM']['VALUE']) > 0): ?> premium<? endif; ?> adv-wrap">
			<table>
				<tbody>
				<tr>
					<td class="to-mark">
						<?if($USER->IsAuthorized()):?>
							<span title="<?=(in_array($USER->GetID(), $arItem["PROPERTIES"]["FAVORITES"]["VALUE"]))? '������� �� ����������' : '�������� � ���������'?>"
							   onclick="AddToFavorites({
								   productId: <?=$arItem['ID']?>,
								   iblockId: <?=$arParams['IBLOCK_ID']?>,
								   context: $(this).find('.icon_star'),
								   inFavorites: function() {
								    this.addClass('star_sel').closest('a').attr('title', '������� �� ����������')
								   },
								   outFavorites: function(){
								    this.removeClass('star_sel').closest('a').attr('title', '�������� � ���������')
								   }
								  })"
								<?= in_array($USER->GetID(), $arItem["PROPERTIES"]["FAVORITES"]["VALUE"]) ? 'class="act"' : ''; ?>>
							</span>
						<?endif;?>
						<!--<span title="��������"></span>-->
					</td>
					<td class="image_box">
						<?

						if (is_array($arItem["PREVIEW_PICTURE"])) {
							$preview = '<img alt="'.$arItem['NAME'].'" src="'.$arItem["PREVIEW_PICTURE"]["SRC"].'">';
						}

						if ( is_array($arItem['PROPERTIES']['more_photo']['VALUE']) ) {
							$file = CFile::ResizeImageGet($arItem['PROPERTIES']['more_photo']['VALUE'][0], array('width'=>72, 'height'=>72), BX_RESIZE_IMAGE_PROPORTIONAL, true);
							$preview = '<img src="'.$file['src'].'" width="'.$file['width'].'" height="'.$file['height'].'" />';
						}

						?>

						<? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem['PROPERTIES']['more_photo']['VALUE'])): ?>
							<a title="���������� ���������" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
								<?= $preview ?>
							</a>
						<? else: ?>
							<img src="/images/logo75.png" width="72" height="72"
							     align=""/>
						<?endif; ?>
					</td>
					<td class="bds_info">
						<div class="tdWrap">
							<div class="bds_titles">
                                <div class="adv-type <?= $sectionClass; ?>"></div>
                                <div class="adv-title">
                                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                                </div>
							</div>
							<div class="bds_text"><?= $arItem['PREVIEW_TEXT'] ?></div>
							<div class="meta author-wrap">
								<? if (empty($arItem["USER_INFO"]["COMPANY_NAME"])): ?>
									<a href="#"><?= $arItem["USER_INFO"]["NAME"] . " " . $arItem["USER_INFO"]['LAST_NAME']; ?></a>
									\  <?= $arItem["DISPLAY_ACTIVE_FROM"]; ?>
								<? else: ?>
									<a href="#"><?= $arItem["USER_INFO"]['NAME'] . ", " . $arItem["USER_INFO"]["COMPANY_NAME"]; ?></a>
									\  <?= $arItem["DISPLAY_ACTIVE_FROM"]; ?>
								<? endif; ?>
								<div style="display: none;">
									<span class="user-name"><?= $arItem['USER_INFO']['LOGIN']; ?></span>
									<span class="user-org"><?= $arItem['USER_INFO']['COMPANY_NAME']; ?></span>
									<table class="user-info">
										<tbody><tr>
											<td>
												<img src="<?= $arItem['USER_INFO']['PERSONAL_PHOTO']; ?>" alt="">
											</td>
											<td>
												<?/*<a href="#">�������</a>*/?>
												<? if ($USER->IsAuthorized()): ?>
													<a href="/personal/messages/<?= $arItem['SEND_MESSAGE']["ID"] ?>/?advert=<?= $arItem['ID'] ?>" target="_blank">��������� ���������</a>
												<? else: ?>
													<a href="#" onclick="alert('���������� ��������������'); return false;">��������� ���������</a>
												<? endif; ?>
											</td>
										</tr>
										</tbody>
									</table>
									<a href="?user=<?=$arItem['SEND_MESSAGE']["ID"];?>" class="all-adv">��� ����������</a>
									<span class="status"><?= $arItem['USER_INFO']['STATUS']; ?></span>
								</div>
							</div>
						</div>
					</td>
					<td class="bds_price">

							<? if (!empty($arItem['DISPLAY_PROPERTIES']['COST']['DISPLAY_VALUE'])): ?>
								<b><?=number_format(str_replace(" ","",$arItem['DISPLAY_PROPERTIES']['COST']['DISPLAY_VALUE']), 0, '.', ' ');?></b> <?= $arItem["PROPERTIES"]["CURRENCY"]["VALUE"]; ?>
								<? if (!empty($arItem["PROPERTIES"]["UNITS"]["VALUE"])): ?>/<?= $arItem["PROPERTIES"]["UNITS"]["VALUE"]; ?>
								<? endif; ?>
							<? else: ?>
								���� ���������
							<? endif; ?>

						<? if (!empty($arItem['PROPERTIES']['TORG']['VALUE'])): ?>
							<span class="red">����</span>
						<? endif; ?>
						<? if ($USER->IsAuthorized()): ?>
							<? if($arItem['SEND_MESSAGE']["ID"] != $user): ?>
								<a href="/personal/messages/<?= $arItem['SEND_MESSAGE']["ID"] ?>/?advert=<?= $arItem['ID'] ?>" class="write" target="_blank">��������</a>
							<? else: ?>
								<a href="/personal/advert/admin.php?PRODUCT_ID=<?= $arItem['ID'] ?>" class="write no-before">�������������</a>
							<? endif; ?>
						<? else: ?>
							<? if($arItem['SEND_MESSAGE']["ID"] != $user): ?>
								<a href="#" class="write" onclick="alert('���������� ��������������'); return false;">��������</a>
							<? else: ?>
								<a href="#" class="write no-before" onclick="alert('���������� ��������������'); return false;" >�������������</a>
							<? endif; ?>
						<? endif; ?>
					</td>
					<?/*<td class="bds_nav">
						<div class="tdWrap">
							<a href="?action=ADD_TO_COMPARE_LIST&id=<?= $arItem['ID'] ?>" rel="nofollow"
							   class="nodecor star_lnk"><i
									class="icons icon_star<? if (in_array($arItem['ID'], $_SESSION['FAVOR' . $arParams['IBLOCK_ID']]['ITEMS'])): ?> star_sel<? endif; ?>"></i><span>��������</span></a>
							<?
							global $USER;
							if ($USER->IsAuthorized()) {
								?>
								<a href="/personal/messages/<?= $arItem['SEND_MESSAGE']["ID"] ?>/?advert=<?= $arItem['ID'] ?>"
								   class="nodecor" target="_blank">��������</a>
							<? } else { ?>
								<a href="#" class="nodecor" onclick="alert('���������� ��������������'); return false;">��������</a>
							<? } ?>
							<a href="#" class="nodecor">����</a>
						</div>
					</td>*/?>
				</tr>
				</tbody>
			</table>
		</div><!--bds_row-->
	<? endforeach; ?>

<div class="paging brdr_box brdr_top_0 pg_nav">
	<div class="show_col">
		<form method="get" name="bottom_f">
			<span class="meta">���������� ��:</span>
			<select name="per_page" onchange="document.bottom_f.submit()">
				<option value="20" <? if ($arParams['NEWS_COUNT'] == 20) echo "selected"; ?>>20</option>
				<option value="50" <? if ($arParams['NEWS_COUNT'] == 50) echo "selected"; ?>>50</option>
				<option value="100" <? if ($arParams['NEWS_COUNT'] == 100) echo "selected"; ?>>100</option>
			</select>
		</form>
	</div>
	<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
		<?= $arResult["NAV_STRING"] ?>
	<? endif; ?>
</div>
