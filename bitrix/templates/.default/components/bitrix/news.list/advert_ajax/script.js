$(document).ready(function() {
    $('.author-wrap > a').click(function(){
        if ($(this).hasClass('act')) {
            $(this).parent().find('div').fadeOut(200);
            $(this).removeClass('act');
        }
        else {
            $('.author-wrap a.act').parent().find('div').fadeOut(200);
            $('.author-wrap a.act').removeClass('act');
            $(this).parent().find('div').fadeIn(200);
            $(this).addClass('act');
        }
        return false;
    });
    $('.to-mark span').click(function(){
        $(this).toggleClass('act');
        return false;
    });
    $(document).click(function(event){
        if($(event.target).closest('.author-wrap > div').length)
            return;
        $('.author-wrap > div').fadeOut(200);
        $('.author-wrap a').removeClass('act');
        event.stopPropagation();
    });

    /// search

    $('#subSeachId_02').live('keyup', function(){

        var text = $(this).val();
        console.log('starts preloader');


        $.get('/_ajax/ajax_advert.php', {
            s: text, TYPE: $('#TYPE').val()
        }, function(data){
            $('#oblist').html(data);
        }).always(function(){
            console.log('hideloader');
        });
    });

});
