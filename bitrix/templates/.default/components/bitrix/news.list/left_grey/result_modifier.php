<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die();

$obParser = new CTextParser;
if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      //���� ������� ��������� � ��������, �������� ��� ��������� - ���������� ��������� ��������
      /*if(($arElement["IBLOCK_ID"] == 3) || ($arElement["IBLOCK_ID"] == 29) || ($arElement["IBLOCK_ID"] == 2))
         $pictureField = "DETAIL_PICTURE";
      else
         $pictureField = "PREVIEW_PICTURE";*/

      $arElement["DETAIL_TEXT"] = strip_tags($arElement["DETAIL_TEXT"]);
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
      $arElement["NAME"] = $obParser->html_cut($arElement["NAME"], "50");
      $arResult['ITEMS'][$key] = $arElement;
      
      if(is_array($arElement["PREVIEW_PICTURE"])) $arElement["DETAIL_PICTURE"] = $arElement["PREVIEW_PICTURE"];
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
            $arElement["DETAIL_PICTURE"],
            array("width" => 50, "height" => 50),
            BX_RESIZE_IMAGE_EXACT,
            true,
            array()     
         );
         $arResult["ITEMS"][$key]["THUMB"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }    
   }
}
?>