<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (count($arResult["ITEMS"]) > 0): ?>
   <div class="lc_block gray_box">
      <div class="wrap">
         <div class="title">
            <a href="<?= str_replace("#SITE_DIR#", "", $arResult['LIST_PAGE_URL']) ?>"><?= $arParams['TITLE'] ?></a>
            <sup><?= GetCountElements($arParams['IBLOCK_ID']); ?></sup><!--<sup>1321</sup>-->
         </div>
         <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <div class="faces_row">
               <?if(is_array($arItem["THUMB"])):?>
                  <div class="image_box">
                     <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <img border="0" src="<?= $arItem["THUMB"]["SRC"] ?>" width="<?= $arItem["THUMB"]["WIDTH"] ?>" height="<?= $arItem["THUMB"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"/>
                     </a>
                  </div>
               <?endif;?>
               <div class="row">
                  <div class="name">
                     <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                  </div>
               </div>
            </div><!--faces_row-->
         <? endforeach; ?>
      </div>  
   </div><!--lc_block-->
<?endif?>
