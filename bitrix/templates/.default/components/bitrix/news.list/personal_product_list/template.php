<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?if ($_REQUEST['action'] == 'ADD_TO_COMPARE_LIST')
   $_SESSION['FAVOR' . $arParams['IBLOCK_ID']]['ITEMS'][] = (int)$_REQUEST['id'];?>

<?if(count($_REQUEST["error"])):
   $print = implode("\n", $_REQUEST["error"]);?>
   <script>
      $(document).ready(function(){
         alert("<?=$print?>");
      });
   </script>
<?endif;?>

<div class="middle_col">
   <div class="mc_block " id="block_for_content">
      <ul class="mb_menu">
         <li<?=($arParams["IBLOCK_ID"] == 27) ? ' class="sel"' : ''?>>
            <a href="/personal/company/prod_and_serv/">������</a>
            <div class="sclew"></div>
         </li>
         <li<?=($arParams["IBLOCK_ID"] == 28) ? ' class="sel"' : ''?>>
            <a href="/personal/company/prod_and_serv/serv.php">������</a>
            <div class="sclew"></div>
         </li>
      </ul>

      <div class="clear"></div>
        <div class="paging brdr_bot_0 brdr_box company_s">
           <form method="get">
              <input type="submit" value="�����" style="float:right; margin-left:10px;">
              <div class="sub_search">
                 <div class="input_box">
                    <a class="search_submit_blue"></a>
                    <input class=""
                    onblur="if(this.value=='') this.value='������� �������� ������';"
                    onclick="if(this.value=='������� �������� ������') this.value='';"
                    autocomplete="off" id="subSeachId_01"
                    type="text" value="<?= (strlen($_REQUEST['name']) > 0) ? $_REQUEST['name'] : '������� �������� ������' ?>"
                    name="name">

                 </div>
              </div>
           </form>
        </div><!--paging-->


      <div class="clear"></div>
      
      <form method="get" name="top_f" id="form_product_list">


         <?=bitrix_sessid_post()?>
         <div class="paging brdr_bot_0 brdr_box">
            <div class="show_controls">
               <input type="checkbox" class="js-check_all">�������: <span class="js-check-counter">0</span>
               <input type="button" name="add_product" data-link="<?=$arParams["EDIT_URL"]?>" value="�������� <?=($arParams["IBLOCK_ID"] == 27) ? '�����' : '������'?>">
               <input type="submit" name="enable" disabled value="��������">
               <input type="submit" name="disable" disabled value="���������">
               <input type="submit" name="edit" data-link="<?=$arParams["EDIT_URL"]?>" disabled value="�������������">
               <input type="submit" name="delete_product" disabled value="�������">
            </div>
            <div class="show_col">
               <!--<input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">-->
               <span class="meta">���������� ��:</span>
               <select name="per_page" onchange="document.top_f.submit()">
                  <option value="20" <? if ($arResult["per_page"] == 20) echo "selected"; ?>>20</option>
                  <option value="50" <? if ($arResult["per_page"] == 50) echo "selected"; ?>>50</option>
                  <option value="100" <? if ($arResult["per_page"] == 100) echo "selected"; ?>>100</option>
               </select>
            </div>
            <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
               <?= $arResult["NAV_STRING"] ?>
            <? endif; ?>
         </div><!--paging-->

         <div class="sortable js-sortable" data-sizen="<?=$arResult["NAV_RESULT"]->SIZEN;?>" data-pagen="<?=$arResult["NAV_RESULT"]->PAGEN;?>" data-company="<?=$arParams["COMPANY_ID"]?>" data-iblock="<?=$arParams["IBLOCK_ID"]?>">
            <? foreach ($arResult["ITEMS"] as $arItem):

               if ($arItem['PROPERTIES']['ACTIVE']['VALUE'] == '')
               {
                  $arItem['PROPERTIES']['ACTIVE']['VALUE'] = 'Y';
               }

               ?>
               <div class="bds_row<?=($arItem['PROPERTIES']['ACTIVE']['VALUE'] == "Y") ? '' : ' inactive'?> js-fav-item ui-state-default js-sortable-item" data-id="<?=$arItem["ID"]?>">
                  <table>
                     <tbody>
                        <tr>
                           <td class="ic_star" style="vertical-align:top;">
                              <input type="checkbox" class="js-check-item" name="check_item[]" value="<?=$arItem["ID"]?>">
                              <!--<a title="<?=(in_array($USER->GetID(), $arItem["PROPERTIES"]["FAVORITES"]["VALUE"]))? '������� �� ����������' : '�������� � ���������'?>" href="javascript:void(0)" onclick="AddToFavorites({productId: <?=$arItem['ID']?>, context: $(this).find('.icon_star'), inFavorites: function(){this.addClass('star_sel').closest('a').attr('title', '������� �� ����������')}, outFavorites: function(){this.removeClass('star_sel').closest('a').attr('title', '�������� � ���������')}})">
                                 <i class="icons icon_star<? if(in_array($USER->GetID(), $arItem["PROPERTIES"]["FAVORITES"]["VALUE"])):?> star_sel<? endif; ?>"></i>
                              </a>-->
                           </td>
                           <td class="image_box">
                              <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                                 <a title="���������� ���������"
                                    href="<?=($arParams["IBLOCK_ID"] == 27) ? 'admin.php' : 'admin_serv.php'?>?PRODUCT_ID=<?= $arItem["ID"] ?>">
                                    <img alt="" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                                 </a>
                              <? else: ?>
                                 <img src="<?= SITE_TEMPLATE_PATH ?>/images/withoutphoto.jpg" width="72" height="72" align=""/>  
                              <? endif; ?>
                           </td>
                           <td class="bds_info">
                              <div class="tdWrap">
                                 <div class="bds_title"><a href="<?=($arParams["IBLOCK_ID"] == 27) ? 'admin.php' : 'admin_serv.php'?>?PRODUCT_ID=<?= $arItem["ID"] ?>"><?= $arItem["NAME"] ?></a></div>
                                 <div class="meta"><?= $arItem['PREVIEW_TEXT'] ?></div>
                              </div>                      
                           </td>
                           <td class="bds_price">
                              <? // PrintObject($arItem);?>
                              <b><span class="js-edit-field" data-code="COST"><?= $arItem['PROPERTIES']['COST']['VALUE'] ? $arItem['PROPERTIES']['COST']['VALUE'] : '0' ?></span>&nbsp;<?=$arItem['PROPERTIES']['CURRENCY']['VALUE']?><br></b>
                              <!--<b><span class="js-edit-field" data-code="SORT_COMPANY"><?= $arItem['PROPERTIES']['SORT_COMPANY']['VALUE'] ? $arItem['PROPERTIES']['SORT_COMPANY']['VALUE'] : '0' ?></span></b>&nbsp;� ����������-->
                           </td>
                           <td class="favorites-delete">
                              <a href="javascript:void(0)" class="personal-icon personal-icon-copy js-item-copy" title="����������"></a>
                              <a href="javascript:void(0)" class="personal-icon personal-icon-sort sortable-button js-sortable-button" title="����������"></a>
                              <a href="javascript:void(0)" class="personal-icon <?=($arItem['PROPERTIES']['ACTIVE']['VALUE'] == "Y") ? 'personal-icon-active js-item-disable' : 'personal-icon-deactive js-item-enable'?>" title="<?=($arItem['PROPERTIES']['ACTIVE']['VALUE'] == "Y") ? '��������������' : '������������'?>"></a>
                              <a href="<?=$arParams["EDIT_URL"]?>?PRODUCT_ID=<?=$arItem["ID"]?>" title="�������������" class="personal-icon personal-icon-edit js-item-edit"></a>
                              <a href="javascript:void(0)" class="personal-icon personal-icon-delete js-item-delete" title="�������"></a>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div><!--bds_row-->
            <? endforeach; ?>
         </div>
      </form>

      <div class="paging brdr_top_0 brdr_box mar_20_bot">
         <div class="show_col">
            <form method="get" name="bot_f">
               <input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">
               <span class="meta">���������� ��:</span>
               <select name="per_page" onchange="document.bot_f.submit()">
                  <option value="20" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 20) echo "selected"; ?>>20</option>
                  <option value="50" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 50) echo "selected"; ?>>50</option>
                  <option value="100" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 100) echo "selected"; ?>>100</option>
               </select>
            </form>
         </div>
         <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <?= $arResult["NAV_STRING"] ?>
         <? endif; ?>
      </div>
   </div>
</div>