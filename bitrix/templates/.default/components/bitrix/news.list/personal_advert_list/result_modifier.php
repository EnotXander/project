<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule("iblock");

//�������� ������ �� 48 �����
foreach ($arResult["ITEMS"] as $key => $arElement)
{
    $datetime1 = date_create($arElement["ACTIVE_FROM"]);
    $datetime2 = date_create("now");
    $interval = date_diff($datetime1, $datetime2);
    $time=$interval->format('%R%a');
    $mas_temp[$key]=$arElement;
    if ($time != "+0" && $time != "+1")
    {
        $mas_temp[$key]["48h"]="Y";
	    CIBlockElement::SetPropertyValueCode($key, "UP", "N");
    }
    else
    {
        $mas_temp[$key]["48h"]="N";
    }
	if (strlen($arElement["NAME"]) > 120){
		$mas_temp[$key]["NAME"] = substr($arElement["NAME"], 0, 117)."...";
	}
	$arImg = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array("width" => 72, "height" => 72), BX_RESIZE_IMAGE_EXACT);
	if ($arImg)	{
		$mas_temp[$key]["PREVIEW_PICTURE"]["SRC"] = $arImg["src"];
	}
}
$arResult["ITEMS"]=$mas_temp;

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      $arElement["DETAIL_TEXT"] = $arElement["DETAIL_TEXT"];
      $arResult['ITEMS'][$key] = $arElement;
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
                         $arElement["DETAIL_PICTURE"], array("width" => 100, "height" => 100), BX_RESIZE_IMAGE_EXACT, true
         );

         $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
   }
}

//���������
if((int)$_REQUEST["per_page"])
{
   $arResult["per_page"] = $_REQUEST["per_page"];
}

//��������� �����
$arResult["FORM_ERROR"] = array();
if(check_bitrix_sessid() && 
        (strlen($_REQUEST["disable"]) || 
        strlen($_REQUEST["enable"]) || 
        strlen($_REQUEST["delete_product"]) ||
            strlen($_REQUEST["UP"])
   ))
{
   $objElement = new CIBlockElement();
   //�������� ��� ��� ������
   $arMyTovars = array();
   $arMyTovarData = array();
   $res = $objElement->GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_FIRM" => $arParams["COMPANY_ID"]));
   while($element = $res->GetNext())
   {
      $arMyTovars[] = $element["ID"];
      $arMyTovarData[$element["ID"]] = $element;
   }

   foreach($_REQUEST["check_item"] as $element)
   {
      $element = (int)$element;

      if(($element) && in_array($element, $arMyTovars))
      {

          if($_REQUEST["UP"]=="Y")
          {

              $objElement->Update($element, array("DATE_ACTIVE_FROM" => date("d.m.Y H:i:s")));

              CIBlockElement::SetPropertyValueCode($element, "UP", "Y");
          }

         if(strlen($_REQUEST["disable"]))
         {
             $objElement->SetPropertyValueCode($element, "ACTIVE", "N");
         }elseif(strlen($_REQUEST["enable"]))
         {
            $arLimit = CheckTarifLimit($arParams["COMPANY_ID"], $arParams["IBLOCK_ID"]);
            if($arLimit["ADDABLE"])
            {
               if(stripos($arMyTovarData[$element]["NAME"], "����� ") === false)
               {
                   $objElement->SetPropertyValueCode($element, "ACTIVE", "Y");
               }
               else
               {
                  $arResult["FORM_ERROR"][] = "������������ {$arMyTovarData[$element]["NAME"]}, ����� ������������ ���!";
               }
            }
            else
            {
               $arResult["FORM_ERROR"][] = "�������� ����� ��������� ��� ������ ��������� ����� ({$arLimit["LIMIT"]} �������� ���������)";
            }
         }elseif(strlen($_REQUEST["delete_product"]))
         {
            $objElement->Delete($element);
         }
      }
   }
   if(!count($arResult["FORM_ERROR"]))
      LocalRedirect($_SERVER["PHP_SELF"]);
   else
   {
      $url = $_SERVER["PHP_SELF"]."?".http_build_query(array("error" => $arResult["FORM_ERROR"]));
      LocalRedirect($url);
   }
}

if(check_bitrix_sessid() && strlen($_REQUEST["edit"]))
{
   $objElement = new CIBlockElement();
   //�������� ��� ��� ������
   $arMyTovars = array();
   $res = $objElement->GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_FIRM" => $arParams["COMPANY_ID"]));
   while($element = $res->GetNext())
      $arMyTovars[] = $element["ID"];
   
   //$saveElements
   foreach($_REQUEST["save"] as $elementId => $element)
   {
      $elementId = (int)$elementId;
      if(($elementId) && in_array($elementId, $arMyTovars))
      {
         //����������
         foreach ($element["PROPERTY_VALUES"] as $propName => $propVal)
         {
            if(!(int)$propVal)
               unset ($element["PROPERTY_VALUES"][$propName]);
         }
         if(count($element["PROPERTY_VALUES"]))
         {
            $objElement->SetPropertyValuesEx($elementId, false, $element["PROPERTY_VALUES"]);
         }
      }
   }

   LocalRedirect($_SERVER["PHP_SELF"]);
}