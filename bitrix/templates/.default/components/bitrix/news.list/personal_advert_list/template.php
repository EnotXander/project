<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if ($_REQUEST['action'] == 'ADD_TO_COMPARE_LIST')
    $_SESSION['FAVOR' . $arParams['IBLOCK_ID']]['ITEMS'][] = (int)$_REQUEST['id'];?>

<?if(count($_REQUEST["error"])):
    $print = implode("\n", $_REQUEST["error"]);?>
    <script>
        $(document).ready(function(){
            alert("<?=$print?>");
        });
    </script>
<?endif;?>

<?
//������� ������
function cutString($string, $maxlen) {
    $len = (mb_strlen($string) > $maxlen)
        ? mb_strripos(mb_substr($string, 0, $maxlen), ' ')
        : $maxlen
    ;
    $cutStr = mb_substr($string, 0, $len);
    return (mb_strlen($string) > $maxlen)
        ?  $cutStr . ' ...'
        :  $cutStr
        ;
}
?>
  <div class="middle_col<? if (strpos($APPLICATION->GetCurUri(), '/personal/advert/') === 0): ?> with-bar<? endif; ?>">
                    <h1><?$APPLICATION->ShowTitle();?></h1>
                    <div class="clearfix"></div>
    <div id="oblist" class="mc_block">
        <div class="ads-search">
            <form method="get" name="bottom_t_f">
                <div class="inp-txt">
                    <input autocomplete="off" id="subSeachId_02" type="text" name="s"
                           value="<?= (strlen($_REQUEST['s']) > 0) ? $_REQUEST['s'] : '����� � �������'; ?>">
                </div>
                <input type="submit" value="">
            </form>
        </div>
        <ul class="mb_menu">
            <li <?= isset($_REQUEST['ACTIVE']) ? '' : 'class="sel"'; ?>><a href="/personal/advert/">��� ����������</a>
                <div class="sclew"></div>
            </li>

            <li <?= isset($_REQUEST['ACTIVE']) ? 'class="sel"' : ''; ?>><a class="disabled_ads_tab" href="/personal/advert/?ACTIVE=N"><i class="icons icon_star star_sel"></i>����������</a>
                <div class="sclew"></div>
            </li>

            <!--<li><a href="?FAVOR=Y"><i class="icons icon_star star_sel"></i>??????????</a><div class="sclew"></div></li>-->
        </ul>
        <div class="clear"></div>

        <form method="get" name="top_f" id="form_product_list">
            <?=bitrix_sessid_post()?>

            <input type="hidden" class="js-check_all">
            <input type="hidden" name="add_product" data-link="<?=$arParams["EDIT_URL"]?>" value="�������� ����������">
            <input type="hidden" name="enable" disabled value="��������">
            <input type="hidden" name="disable" disabled value="���������">
            <input type="hidden" name="UP" disabled value="Y">
            <input type="hidden" name="edit" data-link="<?=$arParams["EDIT_URL"]?>" disabled value="�������������">
            <input type="hidden" name="delete_product" disabled value="�������">

            <div class="paging brdr_bot_0 brdr_box my-adv-head">
                <ul>
                    <li class="nd_chosen"><input type="checkbox" class="js-check_all"/>������� <strong class="js-check-counter">0</strong></li>
                    <li class="nd_adv_add_li"><a class="nd_adv_add" name="add_product"  href="<?=$arParams["EDIT_URL"]?>">�������� ����������</a></li>
                    <li class="nd_adv_shut_down"><a class="nd_adv_off" name="enable" href="javascript:void(0)"><?= isset($_REQUEST['ACTIVE']) ? '��������' : '���������'; ?> ���������</a></li>
                    <li class="nd_adv_del_li"><a class="nd_adv_del" name="delete_product"  href="javascript:void(0)">������� ���������</a></li>
                    <? if(!(isset($_REQUEST['ACTIVE']))):?>
						<li class="nd_up_chosen"><a href="javascript:void(0)"><strong>UP</strong> ���������</a></li>
					<?endif;?>
                </ul>
            </div>
            <!--paging-->
            <div class="sortable js-sortable" data-sizen="<?=$arResult["NAV_RESULT"]->SIZEN;?>" data-pagen="<?=$arResult["NAV_RESULT"]->PAGEN;?>" data-company="<?=$arParams["COMPANY_ID"]?>" data-iblock="<?=$arParams["IBLOCK_ID"]?>">
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <div class="bds_row adv-wrap nd_my_adv<?= ($arItem["PROPERTIES"]["ACTIVE"]["VALUE"] == "Y" || $arItem["PROPERTIES"]["ACTIVE"]["VALUE"] == "") ? '' : ' nd_disabled inactive'?>" data-id="<?=$arItem["ID"]?>">
                        <?//print_r($arItem);?>
                            <table>
                                <tbody>
                                <tr>
                                    <td class="to-mark"><input type="checkbox" class="js-check-item" name="check_item[]" value="<?=$arItem["ID"]?>" /> </td>
                                    <td class="image_box">

	                                    <?
	                                    $preview = '<img src="'.SITE_TEMPLATE_PATH .'/img/nophoto.png" width="72" height="72" align=""/>';

	                                    if (is_array($arItem["PREVIEW_PICTURE"])) {
                                            $preview = '<img alt="'.$arItem['NAME'].'" src="'.$arItem["PREVIEW_PICTURE"]["SRC"].'">';
                                        }

                                        if ( is_array($arItem['PROPERTIES']['more_photo']['VALUE']) ) {
                                            $file = CFile::ResizeImageGet($arItem['PROPERTIES']['more_photo']['VALUE'][0], array('width'=>72, 'height'=>72), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                                            $preview = '<img src="'.$file['src'].'" width="'.$file['width'].'" height="'.$file['height'].'" />';
                                        }


	                                    ?>

	                                    <a href="admin.php?PRODUCT_ID=<?= $arItem["ID"] ?>" title="���������� ���������">
		                                    <?= $preview ?>
	                                    </a>
                                    </td>
                                    <td class="bds_info">
                                        <div class="tdWrap">
                                            <div class="bds_title"><a href="admin.php?PRODUCT_ID=<?= $arItem["ID"] ?>"><?= $arItem["NAME"] ?></a></div>

                                            <div class="meta author-wrap">
	                                            <?= FormatDate("d.m.Y", MakeTimeStamp($arItem["DATE_CREATE"]))." \ ".$arItem["PROPERTIES"]["TYPE"]["VALUE_ENUM"]; ?>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="bds_price" data-code="COST">
                                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['COST']['DISPLAY_VALUE'])): ?>
                                            <?= $arItem['DISPLAY_PROPERTIES']['COST']['DISPLAY_VALUE'] ?>&nbsp;<?= $arItem["PROPERTIES"]["CURRENCY"]["VALUE"]; ?>
                                            <? if (!empty($arItem["PROPERTIES"]["UNITS"]["VALUE"])): ?>
                                                /<?= $arItem["PROPERTIES"]["UNITS"]["VALUE"]; ?>
                                            <? endif; ?>
                                        <?else:?>
                                            ���� ���������
                                        <?endif;?>
                                    </td>
                                    <td class="nd_controls">
                                        <ul>
                                            <? if(isset($_REQUEST['ACTIVE'])): ?>
												<li><a class="nd_ctrl_up nd_dis" href="javascript:void(0)"></a><p class="nd_avail no_clock">���������� ��� ���������� ���������</p></li>
											<? else: ?>
												<?if(($arItem["48h"]!="Y")&&($arItem["PROPERTIES"]["UP"]["VALUE"]=="Y")):?>
													<li><a class="nd_ctrl_up nd_dis" href="javascript:void(0)"></a><p class="nd_avail">�������� ��� � 48 �����</p></li>
												<?elseif(($arItem["PROPERTIES"]["UP"]["VALUE"]!="Y")||($arItem["48h"]=="Y")):?>
													<li><a class="nd_ctrl_up" href="javascript:void(0)"></a></li>
												<?endif;?>
											<? endif; ?>

                                            <li><a class="nd_ctrl_view <?=($arItem["PROPERTIES"]["ACTIVE"]["VALUE"] == "Y") ? 'js-item-disable' : 'js-item-enable'?>"
                                                   href="javascript:void(0)"
                                                   title="<?=($arItem["PROPERTIES"]["ACTIVE"]["VALUE"] == "Y") ? '��������������' : '������������'?>"></a>
                                            </li>
                                            <li><a class="nd_ctrl_edit" href="<?=$arParams["EDIT_URL"]?>?PRODUCT_ID=<?=$arItem["ID"]?>"  title="��������"></a></li>
                                            <li><a class="nd_ctrl_del js-item-delete" href="javascript:void(0)"  title="�������"></a></li>
                                        </ul>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        <!--bds_row-->
                    </div>
                <? endforeach; ?>
            </div>
        </form>

	    <div class="paging brdr_box brdr_top_0 pg_nav">
		    <div class="show_col">
			    <form method="get" name="bot_f">
				    <span class="meta">���������� ��:</span>
				    <? if(isset($_GET["ACTIVE"])): ?>
					    <input type="hidden" name="ACTIVE" value="<?= $_GET["ACTIVE"]; ?>" />
				    <? endif; ?>
				    <select name="per_page" onchange="document.bot_f.submit()">
					    <option value="20" <? if ($arParams['NEWS_COUNT'] == 20) echo "selected"; ?>>20</option>
					    <option value="50" <? if ($arParams['NEWS_COUNT'] == 50) echo "selected"; ?>>50</option>
					    <option value="100" <? if ($arParams['NEWS_COUNT'] == 100) echo "selected"; ?>>100</option>
				    </select>
			    </form>
		    </div>
		    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
			    <?= $arResult["NAV_STRING"] ?>
		    <? endif; ?>
	    </div>
        <?/*
	    <div class="paging brdr_top_0 brdr_box mar_20_bot">
            <div class="sub_search">
                <form name="bottom_t_f" method="get">
                    <!--<span class=" fl_left mar_10_right">?????:</span>
        <div class="input_box">
        <a class="icons search_submit" onclick="document.bottom_t_f.submit();"></a>
        <input class="notUsed" autocomplete="off" id="subSeachId_02" name="s" value="????? ? ???????" style="width:270px;" type="text">
        </div> -->
                </form>
            </div>
            <div class="show_col">
                <form name="bot_f" method="get">
                    <span class="meta">���������� ��:</span>
                    <div style="display: inline-block; position: relative; z-index:100" class="jq-selectbox jqselect">
                        <input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">
                        <select onchange="document.bot_f.submit()" name="per_page" >
                            <option <?if ($_REQUEST["per_page"]==20):?>selected="selected"<?endif;?> value="20">20</option>
                            <option <?if ($_REQUEST["per_page"]==50):?>selected="selected"<?endif;?> value="50">50</option>
                            <option <?if ($_REQUEST["per_page"]==100):?>selected="selected"<?endif;?> value="100">100</option>
                        </select>
                        <!--                <div style="position: relative" class="jq-selectbox__select">-->
                        <!--                    <div class="jq-selectbox__select-text" style="width: 21px;">20</div>-->
                        <!--                    <div class="jq-selectbox__trigger">-->
                        <!--                        <div class="jq-selectbox__trigger-arrow"></div>-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div style="position: absolute; display: none;" class="jq-selectbox__dropdown">-->
                        <!--                    <ul style="position: relative; list-style: none; overflow: auto; overflow-x: hidden">-->
                        <!--                        <li class="selected sel" style="display: block; white-space: nowrap;">20</li>-->
                        <!--                        <li class="" style="display: block; white-space: nowrap;">50</li>-->
                        <!--                        <li class="" style="display: block; white-space: nowrap;">100</li>-->
                        <!--                    </ul>-->
                        <!--                </div>-->
                    </div>
                </form>

            </div>

            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                <?= $arResult["NAV_STRING"] ?>
            <? endif; ?>


        </div>*/?>
    </div>
</div>

<div id="confirmBox">
	<div class="message">�� ������������� ��������?</div>
	<span class="button yes">��</span>
	<span class="button no">���</span>
</div>
