<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
   <? if ($key == 0): ?>
      <div class="mc_block">
         <div class="title"><a href="#">&nbsp;</a></div>
         <div class="main_news">
            <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["DETAIL_PICTURE"])): ?>
               <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                  <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="imgBox">
                     <img border="0" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arItem["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" />
                  </a>
               <? else: ?>
                  <img border="0" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arItem["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" />
               <? endif; ?>
            <? endif ?>
            <div class="name">
               <span class="date wimage"><span class="icons"></span> <? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
               <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
            </div>
            <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
               <? echo $arItem["PREVIEW_TEXT"]; ?>
            <? endif; ?>
         </div>
         <div class="clear"></div>
      </div><!--mc_block-->
   <? else: ?>
      <div class="mc_block">
         <div class="double_news">
            <div class="name">
               <span class="date"><span class="icons"></span> <? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
               <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
            </div>
            <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
               <p><? echo $arItem["PREVIEW_TEXT"]; ?></p>
            <? endif; ?>
         </div>
      </div>
   <? endif; ?>
<? endforeach; ?>
<div class="clear"></div>

