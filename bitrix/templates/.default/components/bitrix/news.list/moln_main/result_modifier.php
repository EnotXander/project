<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$arItem["DETAIL_TEXT"] = strip_tags($arItem["DETAIL_TEXT"]);
	$arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["DETAIL_TEXT"], 300);
	//echo $arItem["DETAIL_TEXT"];
        
        if(is_array($arItem["PREVIEW_PICTURE"])) $arItem["DETAIL_PICTURE"] = $arItem["PREVIEW_PICTURE"];
	$arFileTmp = CFile::ResizeImageGet(
		$arItem["DETAIL_PICTURE"],
		array("width" => 280, "height" => 220),
		BX_RESIZE_IMAGE_EXACT,
		true
	);
	//$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);
	$arItem["DETAIL_PICTURE"] = array(
		"SRC" => $arFileTmp["src"],
		"WIDTH" => $arFileTmp["width"],
                "HEIGHT" => $arFileTmp["height"],
	);

	$arResult['ITEMS'][$key] = $arItem;
}
?>