<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?if ($_REQUEST['action'] == 'ADD_TO_COMPARE_LIST')
   $_SESSION['FAVOR' . $arParams['IBLOCK_ID']]['ITEMS'][] = (int)$_REQUEST['id'];?>

<?
//PrintAdmin($arResult);
$alert = "";
if(count($_REQUEST["error"]))
{
   $alert = implode("\n", $_REQUEST["error"]);
}
if(strlen($_REQUEST["strIMessage"]))
{
   $alert = $_REQUEST["strIMessage"];
}
if(strlen($alert)):?>
   <script>
      $(document).ready(function(){
         alert("<?=$alert?>");
      });
   </script>
<?endif;?>
   
<div class="middle_col">
   <div class="mc_block js-sortable-container">
      <!--<ul class="mb_menu">
         <li<?=($arParams["IBLOCK_ID"] == IBLOCK_PRODUCTS) ? ' class="sel"' : ''?>>
            <a href="/personal/company/news_and_articles/">�������</a>
            <div class="sclew"></div>
         </li>
         <li<?=($arParams["IBLOCK_ID"] == IBLOCK_USLUGI) ? ' class="sel"' : ''?>>
            <a href="/personal/company/news_and_articles/articles.php">������</a>
            <div class="sclew"></div>
         </li>
      </ul>-->
      <div class="clear"></div>
      
      <form method="get" name="top_f" id="form_product_list">
         <?=bitrix_sessid_post()?>
         <div class="paging brdr_bot_0 brdr_box">
            <div class="show_controls">
               <input type="checkbox" class="js-check_all">�������: <span class="js-check-counter">0</span>
               <input type="button" name="add_product" data-link="<?=$arParams["EDIT_URL"]?>" value="�������� <?=($arParams["IBLOCK_ID"] == 3) ? '�������' : '������'?>">
               <input type="submit" name="edit" data-link="<?=$arParams["EDIT_URL"]?>" disabled value="�������������">
            </div>
            <div class="show_col">
               <!--<input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">-->
               <span class="meta">���������� ��:</span>
               <select name="per_page" onchange="document.top_f.submit()">
                  <option value="20" <? if ($arResult["per_page"] == 20) echo "selected"; ?>>20</option>
                  <option value="50" <? if ($arResult["per_page"] == 50) echo "selected"; ?>>50</option>
                  <option value="100" <? if ($arResult["per_page"] == 100) echo "selected"; ?>>100</option>
               </select>
            </div>
            <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
               <?= $arResult["NAV_STRING"] ?>
            <? endif; ?>
         </div><!--paging-->

         <div class="sortable js-sortable" data-sizen="<?=$arResult["NAV_RESULT"]->SIZEN;?>" data-pagen="<?=$arResult["NAV_RESULT"]->PAGEN;?>" data-company="<?=$arParams["COMPANY_ID"]?>" data-iblock="<?=$arParams["IBLOCK_ID"]?>">
            <? foreach ($arResult["ITEMS"] as $arItem): ?> 
               <div class="bds_row<?=($arItem["ACTIVE"] == "Y") ? '' : ' inactive'?> js-fav-item ui-state-default js-sortable-item" data-id="<?=$arItem["ID"]?>">
                  <table>
                     <tbody>
                        <tr>
                           <td class="ic_star" style="vertical-align:top;">
                              <input type="checkbox" class="js-check-item" name="check_item[]" value="<?=$arItem["ID"]?>">
                           </td>
                           <td class="image_box">
                              <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["DETAIL_PICTURE"])): ?>
                                 <a title="���������� ���������" href="admin.php?PRODUCT_ID=<?= $arItem["ID"] ?>">
                                    <img alt="" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>">
                                 </a>
                              <? else: ?>
                                 <img src="<?= SITE_TEMPLATE_PATH ?>/images/withoutphoto.jpg" width="72" height="72" align=""/>  
                              <? endif; ?>
                           </td>
                           <td class="bds_info">
                              <div class="tdWrap">
                                 <div class="bds_title"><a href="admin.php?PRODUCT_ID=<?= $arItem["ID"] ?>"><?= $arItem["NAME"] ?></a></div>
                                 <div class="bds_text">������ ����������: <? echo $arItem['ACTIVE_FROM'] ?></div>
                                 <div class="meta"><?= $arItem['PREVIEW_TEXT'] ?></div>
                              </div>                      
                           </td>
                           <td class="bds_price">
                              <?//  PrintAdmin($arItem["PROPERTIES"]["MODERATE_DENY"])?>
                              <?if($arItem["ACTIVE"] == "Y"):?>
                                 <span style="color: green;">��������</span>
                              <?elseif($arItem["PROPERTIES"]["MODERATE_DENY"]["VALUE"] == "Y"):?>
                                 <span style="color: red;">���������</span>
                              <?else:?>
                                 <span style="color: gray;">�� ������������</span>
                              <?endif;?>
                              
                              <?if(false)://(isset($arItem['PROPERTIES']['SORT_COMPANY'])):?>
                                 <b><span class="js-edit-field" data-code="SORT_COMPANY"><?= $arItem['PROPERTIES']['SORT_COMPANY']['VALUE'] ? $arItem['PROPERTIES']['SORT_COMPANY']['VALUE'] : '0' ?></span></b>&nbsp;� ����������
                              <?endif;?>
                           </td>
                           <td class="favorites-delete">
                              <?if(count($arResult["ITEMS"]) > 1):?>
                                 <a href="javascript:void(0)" class="personal-icon personal-icon-sort sortable-button js-sortable-button" title="����������"></a>
                              <?endif;?>
                              <a href="<?=$arParams["EDIT_URL"]?>?PRODUCT_ID=<?=$arItem["ID"]?>" title="�������������" class="personal-icon personal-icon-edit js-item-edit"></a>
                              <?if($arItem["ACTIVE"] != "Y"):?>
                                 <a href="javascript:void(0)" class="personal-icon personal-icon-delete js-item-delete" title="�������"></a>
                              <?endif;?>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div><!--bds_row-->
            <? endforeach; ?>
         </div>
      </form>

      <div class="paging brdr_top_0 brdr_box mar_20_bot">
         <div class="show_col">
            <form method="get" name="bot_f">
               <input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">
               <span class="meta">���������� ��:</span>
               <select name="per_page" onchange="document.bot_f.submit()">
                  <option value="20" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 20) echo "selected"; ?>>20</option>
                  <option value="50" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 50) echo "selected"; ?>>50</option>
                  <option value="100" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 100) echo "selected"; ?>>100</option>
               </select>
            </form>
         </div>
         <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <?= $arResult["NAV_STRING"] ?>
         <? endif; ?>
      </div>
   </div>
</div>