<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule("iblock");


$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], 200);
      $arResult['ITEMS'][$key] = $arElement;
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = Resizer(
              [$arElement["DETAIL_PICTURE"], $arElement["PREVIEW_PICTURE"]],
              array("width" => 75, "height" => 36)
          );
      }
   }
}


//���������
if((int)$_REQUEST["per_page"])
{
   $arResult["per_page"] = $_REQUEST["per_page"];
}

//PrintObject($arResult);

//��������� �����
$arResult["FORM_ERROR"] = array();
if(check_bitrix_sessid() && 
        (strlen($_REQUEST["disable"]) || 
        strlen($_REQUEST["enable"]) || 
        strlen($_REQUEST["delete_product"])
   ))
{
   $objElement = new CIBlockElement();
   //�������� ��� ��� ������
   $arMyTovars = array();
   $arMyTovarData = array();
   $res = $objElement->GetList(array(), array(
       "IBLOCK_ID" => $arParams["IBLOCK_ID"],
       "PROPERTY_FIRM" => $arParams["COMPANY_ID"])
   );
   while($element = $res->GetNext())
   {
      $arMyTovars[] = $element["ID"];
      $arMyTovarData[$element["ID"]] = $element;
   }
   
   foreach($_REQUEST["check_item"] as $element)
   {
      $element = (int)$element;
      if(($element) && in_array($element, $arMyTovars))
      {
         if(strlen($_REQUEST["disable"]))
         {
            $objElement->Update($element, array("ACTIVE" => false));
         }elseif(strlen($_REQUEST["enable"]))
         {
            $arLimit = CheckTarifLimit($arParams["COMPANY_ID"], $arParams["IBLOCK_ID"]);
            if($arLimit["ADDABLE"])
            {
               if(stripos($arMyTovarData[$element]["NAME"], "����� ") === false)
               {
                  //PrintAdmin("ACTIVE");
                  $objElement->Update($element, array("ACTIVE" => "Y"));
               }
               else
               {
                  //PrintAdmin("BLOCK");
                  $arResult["FORM_ERROR"][] = "������������ {$arMyTovarData[$element]["NAME"]}, ����� ������������ ���!";
               }
            }
            else
            {
               $arResult["FORM_ERROR"][] = "�������� ����� ��������� ��� ������ ��������� ����� ({$arLimit["LIMIT"]} �������� ���������)";
            }
         }elseif(strlen($_REQUEST["delete_product"]))
         {
            $objElement->Delete($element);
         }
      }
   }
   //PrintObject($_REQUEST);
   if(!count($arResult["FORM_ERROR"]))
      LocalRedirect($_SERVER["PHP_SELF"]);
   else
   {
      $url = $_SERVER["PHP_SELF"]."?".http_build_query(array("error" => $arResult["FORM_ERROR"]));
      LocalRedirect($url);
   }
}

if(check_bitrix_sessid() && strlen($_REQUEST["edit"]))
{
   $objElement = new CIBlockElement();
   //�������� ��� ��� ������
   $arMyTovars = array();
   $res = $objElement->GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_FIRM" => $arParams["COMPANY_ID"]));
   while($element = $res->GetNext())
      $arMyTovars[] = $element["ID"];
   
   //$saveElements
   foreach($_REQUEST["save"] as $elementId => $element)
   {
      $elementId = (int)$elementId;
      if(($elementId) && in_array($elementId, $arMyTovars))
      {
         //����������
         foreach ($element["PROPERTY_VALUES"] as $propName => $propVal)
         {
            if(!(int)$propVal)
               unset ($element["PROPERTY_VALUES"][$propName]);
         }
         if(count($element["PROPERTY_VALUES"]))
         {
            $objElement->SetPropertyValuesEx($elementId, false, $element["PROPERTY_VALUES"]);
         }
      }
   }
   //PrintObject($_REQUEST);
   LocalRedirect($_SERVER["PHP_SELF"]);
}