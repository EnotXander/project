<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;

if (is_array($arResult["ITEMS"]))
{
        $arSections = array();
	foreach ($arResult["ITEMS"] as $key => $arElement) 
	{
		if(!in_array($arElement['IBLOCK_SECTION_ID'], array_keys($arSections)))
                {
                   $rsSection = CIBlockSection::GetByID($arElement['IBLOCK_SECTION_ID']);
                   if($arThisSection = $rsSection->GetNext())
                   {
                      $arSections[$arThisSection['ID']] = $arThisSection;
                   }
                }
                $arElement["LIST_PAGE_URL"] = $arSections[$arElement['IBLOCK_SECTION_ID']]['SECTION_PAGE_URL'];
		$arElement["DETAIL_TEXT"] = strip_tags($arElement["DETAIL_TEXT"]);
		$arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["DETAIL_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
		$arResult['ITEMS'][$key] = $arElement;
		if(is_array($arElement["PREVIEW_PICTURE"]))
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arElement["PREVIEW_PICTURE"],
				array("width" => 50, "height" => 50),
				BX_RESIZE_IMAGE_EXACT,
				true
			);

			$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
				"SRC" => $arFileTmp["src"],
				"WIDTH" => $arFileTmp["width"],
                "HEIGHT" => $arFileTmp["height"],
			);

		}else{

        }
	}
}

$FIRM_IDS = [];
//��������� ���������� � ���������//
foreach($arResult["ITEMS"] as $Item)
{
         $FIRM_IDS[]=$Item["PROPERTIES"]["FIRM"]["VALUE"];
}


$arFilter1 = array(
    "ID" => $FIRM_IDS,
    "IBLOCK_ID" => IBLOCK_COMPANY,
    "ACTIVE"=>"Y",
);

$result=CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter1, false ,array());
while($ob = $result->GetNextElement())
{
  $arFields = $ob->GetFields();
  $arProperties = $ob->GetProperties();
  foreach ($arResult["ITEMS"] as $key=>$Item):
     if ($arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["VALUE"]==$arFields["ID"]):
         
         //������������ ������� ����������� ���� � ��������//
         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["ID"]=$arFields["ID"];
         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["NAME"]=$arFields["NAME"];
         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["PREVIEW_PICTURE"]=$arFields["PREVIEW_PICTURE"];
         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["DETAIL_PICTURE"]=$arFields["DETAIL_PICTURE"];
         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["DETAIL_PAGE_URL"]=$arFields["DETAIL_PAGE_URL"];

         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["USER"]=$arProperties["USER"];
         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["MANAGER"]=$arProperties["MANAGER"];
         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["EMAIL"]=$arProperties["EMAIL"];
         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["PHONE"]['VALUE']=array_slice($arProperties["phone"]['VALUE'], 0, 2);
         $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["SITE"]=$arProperties["URL"];
     endif;
  endforeach;
}

//��������� ���������� � ��������� ��������//
 foreach ($arResult["ITEMS"] as $key=>$Item):
    $rsFile = CFile::GetByID($Item["PROPERTIES"]["FIRM"]["PREVIEW_PICTURE"]);
    $arFile = $rsFile->Fetch();
     
   $arResult["ITEMS"][$key]["PROPERTIES"]["FIRM"]["LOGO"]=CFile::ResizeImageGet( 
        $Item["PROPERTIES"]["FIRM"]["PREVIEW_PICTURE"], 
        array('width'=>$arFile["WIDTH"], 'height'=>35), 
        BX_RESIZE_IMAGE_PROPORTIONAL, 
        true
);
endforeach;

