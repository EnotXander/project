<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?global $quantity;?>

<?if(count($arResult["ITEMS"])>0):?>
     <div class="mc_block product-sl">
        <div class="mc_head">
           <h2>�������� ������</h2>
           <span class="all-product">
              <a href="<?=$arResult["ITEMS"][0]["LIST_PAGE_URL"]?>">��� �������� ������</a>
              <sup><?=$quantity?></sup>
           </span>
           <div class="clear"></div>
        </div>

        <div class="mc_body">
            <?foreach($arResult["ITEMS"] as $key => $arItem):?>   
            <div class="ads_wrap">
               <div class="ads_row" style="background-color:#fff;">
                  <div class="ads_row_inner">
                  <table>
                     <tbody>
                        <tr>
                           <td class="image_box" style="text-align:center;">
                              <a title="���������� ���������" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                 <img alt="" src='<?=$arItem["PREVIEW_PICTURE"]["SRC"]!=''?$arItem["PREVIEW_PICTURE"]["SRC"]:'/images/search_placeholder_30x30.png';?>'/>
                              </a>
                           </td>
                        </tr>
                        <tr>
                           <td class="ads_info" style="text-align:center;">
                              <div class="tdWrap">
                                 <div class="ads_title">
                                    <a href='<?=$arItem['DETAIL_PAGE_URL']?>'>
                                         <?=substr($arItem["NAME"],0,40)?><?=(strlen($arItem["NAME"]) > 40) ? '...' : ''?>
                                    </a>
                                 </div>
                              </div>                      
                           </td>
                        </tr>
                        
                        <tr>
                           <td class="ads_nav">
                              <?//if($arItem["PROPERTIES"]["FIRM"]["LOGO"]["src"]):?>
                                 <img alt="" src="<?=($arItem["PROPERTIES"]["FIRM"]["LOGO"]["src"]) ? $arItem["PROPERTIES"]["FIRM"]["LOGO"]["src"] : '/images/search_placeholder_30x30.png'?>">

                              <?//endif?>
                              <span style="display: block;
                                     overflow: hidden;
                                     height: 34px;"><?=$arItem["PROPERTIES"]["FIRM"]["NAME"]?></span>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  </div>
               </div><!--ads_row-->
               <div class="all_c_info" id="allCompanyInfoID">
                   <? if (false) : ?>
                      <?if ($USER->IsAuthorized()):?>
                        <a class="wt-to-comp"
                           href="/personal/messages/<?=$arItem["PROPERTIES"]["FIRM"]["USER"]["VALUE"]?>/?product=<?=$arResult['ID']?>" >
                           ��������</a>
                        <br/>
                     <?else:?>
                        <a class="wt-to-comp" href="#" onclick="alert('���������� ��������������'); return false;">��������</a><br/>
                     <?endif;?>
                  <? endif; ?>
                    <a class='c_name' href='<?=$arItem["PROPERTIES"]["FIRM"]["DETAIL_PAGE_URL"]?>'>�������� ��������</a>
                     <ul class="js-phone-hide-block" data-company="<?=$arItem["PROPERTIES"]["FIRM"]["ID"]?>">
                        <?  foreach ($arItem["PROPERTIES"]["FIRM"]["PHONE"]["VALUE"] as $number):?>
                           <li>
                              <span class="phone-code">+375</span>
                              <span>
                                 <span class="phone-hide js-phone-hide">�������� �������</span>
                              </span>
                              <span class="phone-show"><?=$number?></span>
                           </li>
                        <?endforeach;?>
                        <?  foreach ($arItem["PROPERTIES"]["FIRM"]["SITE"]["VALUE"] as $url):?>
                           <?if(strlen($url) && $url != 'http://'):?>
                              <li>
                                 <a rel="nofollow" class="c_site" href='<?=(strlen($url) && strpos($url, 'http://') === false) ? 'http://' : '' ?><?=$url?>'><?=$url?></a>
                              </li>
                              <?break;//only 1 position?>
                           <?endif;?>
                        <?endforeach;?>
                     </ul>
                     <span class="c_mess" href="javascript:void()"><img src="/images/mess_img.png"></span>
               </div>
            </div>
            <?endforeach?>
            <div class="clear"></div>
			</div>
               <!--<div class="mc_footer">-->
               <?// if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                  <?//= $arResult["NAV_STRING"] ?>
               <?// endif; ?>
               <!--</div>-->
         </div>
   <script type="application/javascript">
      $(function() {
         var max = 0;
         $('.mc_body .ads_row').each(function(i, e){
            if (max < $(e).height())
               max = $(e).height()
         });

         $('.mc_body .ads_row').each(function(i, e){
            $(e).height(max);
         });
         $('.all_c_info').height(max);
         $('.ads_row_inner').css('height', '100%');
      });
   </script>
<?endif;?>
