<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 


<ul class="title_list">
   <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
      <li><a class="mainlist_hover_underline" href="<? echo $arItem["DETAIL_PAGE_URL"]; ?>" ><? echo $arItem["NAME"] ?></a><span class="date">\ <? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span></li>
   <? endforeach; ?>
</ul>
<style>
.mainlist_hover_underline:hover{
   text-decoration: underline !important;
}
</style>