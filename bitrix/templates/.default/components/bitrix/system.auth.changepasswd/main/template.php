<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="bx-auth">
   <?if(($_REQUEST["AUTH_FORM"] == "Y") && (strlen($arResult["IS_OK"])) && ($_REQUEST["TYPE"] == "CHANGE_PWD_CUSTOM") && !count($arResult["VALID"])):?>
      <b><?= GetMessage("AUTH_CHANGE_PASSWORD") ?></b>
      <p style="color: #009933;"><?=$arResult["IS_OK"]?></p>
   <?else:?>
      <form id="changepass-form" method="post" action="<?= $arResult["AUTH_FORM"] ?>" name="bform">
         <? if (strlen($arResult["BACKURL"]) > 0): ?>
            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
         <? endif ?>
         <input type="hidden" name="AUTH_FORM" value="Y">
         <input type="hidden" name="TYPE" value="CHANGE_PWD_CUSTOM">
         <table class="data-table bx-changepass-table bx-registration-table">
            <thead>
               <tr> 
                  <td colspan="3" style="color:red;"><b><?= GetMessage("AUTH_CHANGE_PASSWORD") ?></b></td>
               </tr>
               <tr>
                  <td colspan="3">����, ���������� <span style="color:red">*</span>, ������������ ��� ����������.</td>
               </tr>
            </thead>
            <tbody class="bodyground">
               <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
               <tr<?= strlen($arResult["VALID"]["EMAIL"]) ? ' class="error"' : '' ?>>
                  <td><span class="starrequired">*</span><?= GetMessage("AUTH_LOGIN") ?></td>
                  <td>
                     <input id="changepass-form-email" type="text" name="USER_LOGIN" maxlength="50" value="<?= $arResult["LAST_LOGIN"] ?>" class="bx-auth-input" />
                     <div id="changepass-errortext-email" class="errortext"><?= $arResult["VALID"]["EMAIL"] ?></div>
                  </td>
                  <td class="bordered"></td>
               </tr>
               <tr<?= strlen($arResult["VALID"]["CHECKWORD"]) ? ' class="error"' : '' ?>>
                  <td><span class="starrequired">*</span><?= GetMessage("AUTH_CHECKWORD") ?></td>
                  <td>
                     <input id="changepass-form-checkword" type="text" name="USER_CHECKWORD" maxlength="50" value="<?= $arResult["USER_CHECKWORD"] ?>" autocomplete="off" class="bx-auth-input" />
                     <div id="changepass-errortext-checkword" class="errortext"><?= $arResult["VALID"]["CHECKWORD"] ?></div>
                  </td>
                  <td class="bordered"></td>
               </tr>
               <tr<?= strlen($arResult["VALID"]["PASSWORD"]) ? ' class="error"' : '' ?>>
                  <td><span class="starrequired">*</span><?= GetMessage("AUTH_NEW_PASSWORD_REQ") ?></td>
                  <td>
                     <input id="changepass-form-password" type="password" name="USER_PASSWORD" maxlength="50" value="" autocomplete="off" class="bx-auth-input" />
                     <div id="changepass-errortext-password" class="errortext"><?= $arResult["VALID"]["PASSWORD"] ?></div>
                  </td>
                  <td class="bordered" style="width: 400px;">
                     <p><? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>
                  </td>
               </tr>
               <tr class="auth-last<?= strlen($arResult["VALID"]["CONFIRM"]) ? ' error' : '' ?>">
                  <td><span class="starrequired">*</span><?= GetMessage("AUTH_NEW_PASSWORD_CONFIRM") ?></td>
                  <td>
                     <input id="changepass-form-confirm" type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="" autocomplete="off" class="bx-auth-input"  />
                     <div id="changepass-errortext-confirm" class="errortext"><?= $arResult["VALID"]["CONFIRM"] ?></div>
                  </td>
                  <td class="bordered"></td>
               </tr>
               <tr style="background-color:#ffffff;"><td colspan="3"></td></tr>
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
                  <td><input type="submit" id="changepass-form-submit" style="display: none;" name="change_pwd" value="<?= GetMessage("AUTH_CHANGE") ?>" /></td>
                  <td><a class="btns_big" style="float: right;" href="javascript:void(0);" onclick="$('#changepass-form').submit();"><i></i>�������� ������</a></td>
               </tr>
            </tfoot>
         </table>
      </form>
   <?endif;?>
</div>