<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $USER;
$arResult["VALID"] = array();
if(($_REQUEST["AUTH_FORM"] == "Y") && ($_REQUEST["TYPE"] == "CHANGE_PWD_CUSTOM"))
{
   $email = $_REQUEST["USER_LOGIN"];
   $word = $_REQUEST["USER_CHECKWORD"];
   $pass = $_REQUEST["USER_PASSWORD"];
   $conf = $_REQUEST["USER_CONFIRM_PASSWORD"];
   
   if(strlen($email))
   {
      if(checkEmailPHP($email))
      {
         $resUser = CUser::GetList(($by="id"), ($order="desc"), array("EMAIL" => $email));
         if(!$arUser = $resUser->Fetch())
            $arResult["VALID"]["EMAIL"] = "������������ � ����� ������� ����������� ����� �� ���������������";
      }
      else
         $arResult["VALID"]["EMAIL"] = "������������ ����� ����������� �����";
   }
   else
      $arResult["VALID"]["EMAIL"] = "����� ����������� ����� �� ��������";
   
   if(strlen($pass))
   {
      if(strlen($pass) < 6)
         $arResult["VALID"]["PASSWORD"] = "����������� ����� ������ - 6 ��������";
   }
   else
      $arResult["VALID"]["PASSWORD"] = "������ �� ��������";
   
   if(strlen($conf))
   {
      if($conf !== $pass)
         $arResult["VALID"]["CONFIRM"] = "������ �� ���������";
   }
   else
      $arResult["VALID"]["CONFIRM"] = "��������� ������";
   
   if(!strlen($word))
      $arResult["VALID"]["CHECKWORD"] = "����������� ������ �� ���������";
   if(!count($arResult["VALID"]))
   {
      $arChange = $USER->ChangePassword(
         $email,
         $word,
         $pass,
         $conf
      );
      if($arChange["TYPE"] != "OK")
      {
         $arResult["VALID"][$arChange["FIELD"]] = $arChange["MESSAGE"];
      }
      else
      {
         $arResult["IS_OK"] = $arChange["MESSAGE"];
      }
   }
}