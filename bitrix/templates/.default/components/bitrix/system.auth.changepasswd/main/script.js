//���������
//������
function showErrorFunc($element, text){
   var $tr = $element.closest('tr');
   $tr.addClass("error");
   $tr.find('.errortext').text(text);
}
function hideErrorFunc($element){
   var $tr = $element.closest('tr');
   if($tr.hasClass("error")){
      $tr.removeClass("error");
      $tr.find('.errortext').text("");
   }
}
//��������
function fieldEmptyFunc($element){
   console.log("fieldEmptyFunc", $element);
   return $element.val().length
}
function fieldEmailFunc($element){
   return checkEmailJS($element.val())
}
function fieldShortPassFunc($element){
   return ($element.val().length >= 6)
}
function fieldConfirmPassFunc($element){
   return ($element.val() == $('#changepass-form-password').val())
}
//����������
validEmail = validation({//�����
   selector: "#changepass-form-email",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "����� ����������� ����� �� ��������"
   },{
      compare: fieldEmailFunc,
      errortext: "������������ ����� ����������� �����"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
})
validPassword = validation({
   selector: "#changepass-form-password",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "������ �� ��������"
   },{
      compare: fieldShortPassFunc,
      errortext: "����������� ����� ������ - 6 ��������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
})
validConfirm = validation({
   selector: "#changepass-form-confirm",
   require: [
   {
      compare: fieldEmptyFunc,
      errortext: "��������� ������"
   },{
      compare: fieldConfirmPassFunc,
      errortext: "������ �� ���������"
   }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
})
validCheckword = validation({
   selector: '#changepass-form-checkword',
   require: [
      {
         compare: fieldEmptyFunc,
         errortext: "����������� ������ �� ���������"
      }
   ],
   showError: showErrorFunc,
   hideError: hideErrorFunc
})

$(document).ready(function(){
   //���������
   //�����
   $('#changepass-form-email').live("focusout", function(){
      validEmail();
   })
   //Password
   $('#changepass-form-password').live("focusout", function(){
      validPassword();
   })
   //Password
   $('#changepass-form-confirm').live("focusout", function(){
      validConfirm();
   })
   //Checkword
   $('#changepass-form-checkword').live("focusin", function(){
      validCheckword();
   })
            
   //������
   $('#changepass-form').live("submit", function(){
      var $this = $(this);
      var $form = $this.closest('form');
      //�������� ������
      validEmail();
      validPassword();
      validConfirm();
      validCheckword();
      if($form.find('.error').size()) return false;
   })

})