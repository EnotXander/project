<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
} ?>
<?
if (!CModule::IncludeModule("iblock")) die();
$allId =[];

foreach ( $arResult["SEARCH"] as $i => $arItem ) {

	$allId []= $arItem ['ITEM_ID'] ;// tovar

}
// ������� ��� �������� � ��������
// FIRM
if (!empty($allId)) {


    $arFilter   = array( 'ID' => $allId );
    $rsElements = CIBlockElement::GetList(
        array('PROPERTY_FIRM.PROPERTY_EXT_SORT'=>'DESC'),
        $arFilter,
        false,
        $arNavParams,
        array(
            "ID",
            "NAME",
            "PREVIEW_PICTURE",
            // "PREVIEW_TEXT",
            "DETAIL_PICTURE",
            // "DETAIL_TEXT",
            "DETAIL_PAGE_URL",
            "PROPERTY_COST",
            "PROPERTY_CURRENCY",
            "PROPERTY_unit_tov",
            "PROPERTY_FIRM.ID",
            "PROPERTY_FIRM.IBLOCK_ID",
            "PROPERTY_FIRM.NAME",
            "PROPERTY_FIRM.PREVIEW_PICTURE",
            "PROPERTY_FIRM.DETAIL_PICTURE",
            "PROPERTY_FIRM.DETAIL_PAGE_URL",
            "PROPERTY_FIRM.PROPERTY_USER",
            "PROPERTY_FIRM.PROPERTY_EMAIL",
            "PROPERTY_FIRM.PROPERTY_MANAGER",
            "PROPERTY_FIRM.PROPERTY_REMOVE_REL"
        )
    );

    while ( $arElements = $rsElements->GetNext() ) {
        $picture_id                                  = $arElements['DETAIL_PICTURE'] == '' ? $arElements['PREVIEW_PICTURE'] : $arElements['DETAIL_PICTURE'];
        $arElements['DETAIL_PICTURE']                = CFile::ResizeImageGet( $picture_id, array(
            'width'  => 50,
            'height' => 50
        ), BX_RESIZE_IMAGE_PROPORTIONAL, true );
        $arElements['PROPERTY_FIRM_PREVIEW_PICTURE'] = CFile::ResizeImageGet( $arElements['PROPERTY_FIRM_PREVIEW_PICTURE'], array(
            'width'  => 50,
            'height' => 50
        ), BX_RESIZE_IMAGE_PROPORTIONAL, true );

        $arResult["SEARCH_ITEMS"][ $arElements['ID'] ] = $arElements;

    }
}

/*
[SEARCH] => Array
       (
           [0] => Array
               (
                   [ID] => 57408
                   [~ID] => 57408
                   [MODULE_ID] => iblock
                   [~MODULE_ID] => iblock
                   [ITEM_ID] => 195257