<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
   die(); ?>

<?$APPLICATION->SetTitle("�������������� - �����.");?>

<? if ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false): ?>
<? elseif ($arResult["ERROR_CODE"] != 0): ?>
   <p><?= GetMessage("SEARCH_ERROR") ?></p>
   <? ShowError($arResult["ERROR_TEXT"]); ?>
   <p><?= GetMessage("SEARCH_CORRECT_AND_CONTINUE") ?></p>
   <br /><br />
<? elseif (count($arResult["SEARCH"]) > 0): ?>
   <ul class="mb_menu" style="margin-top: 15px;">
      <li<?=$arResult["SORT"]["TYPE"]=="rank" ? ' class="sel"' : ''?>><a href="<?=$arResult["SORT"]["RELEV"]?>">�������������</a><div class="sclew"></div></li>
      <li<?=$arResult["SORT"]["TYPE"]=="date" ? ' class="sel"' : ''?>><a href="<?=$arResult["SORT"]["DATE"]?>">����</a><div class="sclew"></div></li>
   </ul>

   <div class="search_results">
      <? foreach ($arResult["SEARCH_ITEMS"] as $arItem):?>
          <?
         $img = $arItem['PREVIEW_PICTURE'];
         $arItemO = $arItem;
         $arItem = $arResult["SEARCH"][$arItem['ID']];
         //$arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]["PREVIEW_PICTURE"]
         ?>
         <div class="search_row" data-rate="<?= $arItemO['PROPERTY_FIRM_PROPERTY_EXT_SORT_VALUE'] ?>">
            <? if (is_array($img)): ?>
               <a class="image_box" href="<?= $arItem["URL"] ?>">
                  <img src="<?= $img["SRC"] ?>" alt="">
               </a>
            <? endif; ?>
            <div class="row"<?=($arParams["IMAGE_MARGIN"]) ? '' : ' style="margin-left: 110px"'?>>
               <a class="title" href="<?= $arItem["URL"] ?>"><?= $arItemO["NAME"] ?></a>
               <div class="descr">
                  <?if(is_array($arItem["RIBBON"]) && count($arItem["RIBBON"])):?>
                     <div class="inline">
                        <a href="<?=$arItem["RIBBON"]["URL"]?>">
                           <div class="news_section <?=$arItem["RIBBON"]["CLASS"]?>">
                              <p><?=$arItem["RIBBON"]["TITLE"]?></p>
                           </div>
                        </a>
                     </div>
                  <?endif;?>

                  <?= $arItem["BODY_FORMATED"] == '' ? $arItemO["DETAIL_TEXT"] : $arItem["BODY_FORMATED"] ; ?>
                  <?if($arItem["REAL_ELEMENT"]["SHOW_DATE"]):?>
                     <span class="date">\ <?echo $arItem["REAL_ELEMENT"]["DATE_ACTIVE_FROM"]?></span>
                  <?endif;?>
               </div>
            </div>
         </div>
      <? endforeach; ?>
   </div>
   <div class="paging brdr_top_0 brdr_box mar_20_bot">
            <div class="show_col">

            </div>
            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
               <?= $arResult["NAV_STRING"] ?>
            <? endif; ?>
         </div>
<? else: ?>
   <p><?= GetMessage("SEARCH_NOTHING_TO_FOUND"); ?></p>
<?endif;?>
<!--
<? //print_r($arResult) ?>
-->

