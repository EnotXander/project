<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$response = array();
if(count($arResult["SEARCH"]) > 0)
{
	$all = 0;

	foreach($arResult["SEARCH_SECTION"] as $arItem)
   {
	   //if (!($arItem["REAL_ELEMENT"]["IS_SECTION"])) continue;
//print_r($arItem);
      $json = array();
      $json['value'] = iconv("cp1251", "UTF-8", $arItem["NAME"]);

      $json['id'] = $arItem["ID"];
      $json['name'] = iconv("cp1251", "UTF-8", $arItem["NAME"]);
      $json['type'] = iconv("cp1251", "UTF-8", $arItem["BODY_FORMATED"]);
      $json['image'] = iconv("cp1251", "UTF-8", $arItem["PREVIEW_PICTURE"]["SRC"]);
      $json['image_h'] = $arItem["PREVIEW_PICTURE"]["HEIGHT"];
      $json['link'] = $arItem["URL"];
      $json['ribbon_class'] = $arItem["RIBBON"]["CLASS"];
      $json['ribbon_url'] = $arItem["RIBBON"]["URL"];
      $json['ribbon_title'] = iconv("cp1251", "UTF-8", $arItem["RIBBON"]["TITLE"]);
      if($arItem["REAL_ELEMENT"]["SHOW_DATE"])
      {
          $timestamp = BXToTimestamp($arItem["REAL_ELEMENT"]["DATE_ACTIVE_FROM"]);
          $dateFormat = date('d.m.Y H:i', $timestamp);
         $json['date'] = "\ ".$dateFormat;
      }
      else $json['date'] = "";
      $json['is_section'] = $arItem["REAL_ELEMENT"]["IS_SECTION"];
      if($arItem["REAL_ELEMENT"]["IS_SECTION"])
      {
         $sub = array();
         foreach($arItem["REAL_ELEMENT"]["SUB"] as $subItem)
         {
            $sub[] = array(
                "name" => iconv("cp1251", "UTF-8", $subItem["NAME"]),
                "url" => $subItem["URL"]
            );
         }
         $json['sub'] = $sub;
      }

      $json['image_margin'] = $arParams["IMAGE_MARGIN"];
      $response[] = $json;
	   $all ++;
   }

   $firm_name = '';
   $firm_2 = 0;

   foreach($arResult["SEARCH_ITEMS"] as $arItem)
   {
      //$text = $arItem['DETAIL_TEXT'];
      $product = $arItem['IBLOCK_ID'] == IBLOCK_PRODUCTS;
      $arItem = $arResult["SEARCH"][$arItem['ID']];


      if ($firm_name != $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]['PROPERTY_FIRM_NAME']
      && $firm_2 >= 2 ) $firm_2 = 0;

      if ($firm_name == $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]['PROPERTY_FIRM_NAME']
      && $firm_2 >= 2 && $firm_name !='') continue;

      $firm_2++;


      $firm_name = $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]['PROPERTY_FIRM_NAME'];

      $all++;

      $img = $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]["PREVIEW_PICTURE"]["SRC"] != '' ? $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]["PREVIEW_PICTURE"]["SRC"] : $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]["DETAIL_PICTURE"]["SRC"];
      $img = $img == '' ? '/images/search_placeholder.png' :$img ;
      $img_h = $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]["PREVIEW_PICTURE"]["HEIGHT"] != '' ? $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]["PREVIEW_PICTURE"]["HEIGHT"] : $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]["DETAIL_PICTURE"]["HEIGHT"];
      $img_h = $img_h == '' ? 42 : $img_h ;

      $json = array();
      $json['value'] = iconv("cp1251", "UTF-8", $arItem["TITLE_FORMATED"]);

      $json['id'] = $arItem["ID"];
      $json['name'] = iconv("cp1251", "UTF-8", $arItem["TITLE_FORMATED"]);
      $json['type'] = !$product?iconv("cp1251", "UTF-8", $arItem["BODY_FORMATED"]):'';
      $json['image'] = iconv("cp1251", "UTF-8", $img);
      $json['image_h'] = $img_h;
      $json['link'] = $arItem["URL"];
      $json['ribbon_class'] = $arItem["RIBBON"]["CLASS"];
      $json['ribbon_url'] = $arItem["RIBBON"]["URL"];
      $json['ribbon_title'] = iconv("cp1251", "UTF-8", $arItem["RIBBON"]["TITLE"]);
      if($arItem["REAL_ELEMENT"]["SHOW_DATE"])
      {
          $timestamp = BXToTimestamp($arItem["REAL_ELEMENT"]["DATE_ACTIVE_FROM"]);
          $dateFormat = date('d.m.Y H:i', $timestamp);
          $json['date'] = "\ ".$dateFormat;
      }
      else
         $json['date'] = "";
      $json['is_section'] = $arItem["REAL_ELEMENT"]["IS_SECTION"];
      if($arItem["REAL_ELEMENT"]["IS_SECTION"])
      {
         $sub = array();
         foreach($arItem["REAL_ELEMENT"]["SUB"] as $subItem)
         {
            $sub[] = array(
                "name" => iconv("cp1251", "UTF-8", $subItem["NAME"]),
                "url" => $subItem["URL"]
            );
         }
         $json['sub'] = $sub;
      }

      if(empty($arItem["REAL_ELEMENT"]["FIRM"]["LINK"]))
      {
          $frm = $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ];

	      $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]['PROPERTY_FIRM_PREVIEW_PICTURE']['SRC'] =
		      iconv("cp1251", "UTF-8", $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]["PROPERTY_FIRM_PREVIEW_PICTURE"]["SRC"]);

        if ($arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]['PROPERTY_FIRM_PREVIEW_PICTURE']['SRC'] == '' && $frm['PROPERTY_FIRM_NAME'] !='')
           $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]['PROPERTY_FIRM_PREVIEW_PICTURE']['SRC'] = '/images/search_placeholder.png';

         $json['firm'] = array(
             "LINK" => $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]['PROPERTY_FIRM_DETAIL_PAGE_URL'],
             "LOGO" => $arResult["SEARCH_ITEMS"][ $arItem['ITEM_ID'] ]['PROPERTY_FIRM_PREVIEW_PICTURE']
         );

	      $json['firm']["LOGO"]["DESCRIPTION"] =
              html_entity_decode(iconv("cp1251", "UTF-8", $frm['PROPERTY_FIRM_NAME']));

          $prem = $frm['PROPERTY_FIRM_PROPERTY_PREMIUM_VALUE'] ==  '��' ? '������� ' : '';
          $TARIF = $arResult['TARIFS'] [ $frm['PROPERTY_FIRM_PROPERTY_TARIF_VALUE'] ]  ;
          $RATING = '������� '.$frm['PROPERTY_FIRM_PROPERTY_RATING_VALUE'] ;
	      $json['firm']["LOGO"]["DESCRIPTION"] .= "\n".'('. html_entity_decode(iconv("cp1251", "UTF-8", $prem))
                                                  . html_entity_decode(iconv("cp1251", "UTF-8", $TARIF))." ". html_entity_decode(iconv("cp1251", "UTF-8", $RATING)).')';
      }
      $json['image_margin'] = $arParams["IMAGE_MARGIN"];
      $response[] = $json;

      if ($all >= 10) {
         break;
      }

   }
}

//header("Content-type: application/json");

echo json_encode($response, JSON_ERROR_UTF8);


