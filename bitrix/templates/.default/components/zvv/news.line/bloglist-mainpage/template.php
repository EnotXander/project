<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
} ?>
<div class="news-line">
	<? foreach ( $arResult["ITEMS"] as $i => $arItem ): ?>
		<div class="faces_row">
			<div class="image_box">
				<? if ( $arItem['my_USER']['PERSONAL_PHOTO'] ): ?>
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
						<img width="<?= $arItem['my_USER']['PERSONAL_PHOTO_PARAMS']['width'] ?>" height="<?= $arItem['my_USER']['PERSONAL_PHOTO_PARAMS']['height'] ?>" border="0" alt="" src="<?= $arItem['my_USER']['PERSONAL_PHOTO_PARAMS']['src'] ?>">
					</a>
				<? endif; ?>
			</div>
			<div class="name">
				<a href="/blogs/author/<?= $arItem['my_SECTION']['UF_USER_ID'] ?>/"><?= $arItem['my_USER']['NAME'] ?> <?= $arItem['my_USER']['LAST_NAME'] ?></a>
			</div>
			<div class="post">
				<a href="/blogs/author/<?= $arItem['my_SECTION']['UF_USER_ID'] ?>/"><?= $arItem['my_SECTION']['NAME'] ?></a>
			</div>
			<div class="company">
				<span class="blog-post-date-formated"><!-- 15/02 16:55 --><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>&nbsp;&nbsp;
				<a title="<?= $arItem["NAME"] ?>" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
			</div>
		</div>
	<? endforeach; ?>
</div><a href="/blogs/" class="nodecor bold">��� ������</a>
