<? if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
} ?>
<?
foreach ( $arResult["ITEMS"] as $i => $arItem ) {

	if ( $arItem['IBLOCK_SECTION_ID'] ) {
		$resSection                            = CIBlockSection::GetList(
			array(),
			array(
				'IBLOCK_ID' => $arItem['IBLOCK_ID'],
				'ID'        => $arItem['IBLOCK_SECTION_ID']
			),
			false,
			array( 'ID', 'NAME', 'UF_USER_ID' )
		);
		$arResult["ITEMS"][ $i ]['my_SECTION'] = $resSection->GetNext();
		//user
		$resUser      = CUser::GetByID( $arResult["ITEMS"][ $i ]['my_SECTION']['UF_USER_ID'] );
		$arrUser      = $resUser->GetNext();

		if ( intval($arrUser['PERSONAL_PHOTO']) > 0 ) {

			$tmpImage = CFile::ResizeImageGet($arrUser['PERSONAL_PHOTO'], array("width" => 50, "height" => 50), BX_RESIZE_IMAGE_PROPORTIONAL, true);

			$arrUser['PERSONAL_PHOTO_PARAMS']  = $tmpImage;
		}
		$arResult["ITEMS"][ $i ]['my_USER'] = $arrUser;
	}

}
?>