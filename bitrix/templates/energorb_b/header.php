<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="text/html" charset="windows-1251"/>
    <meta name="viewport" content="width=1243">
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <?$APPLICATION->ShowMeta("keywords")?>
    <?$APPLICATION->ShowMeta("description")?>
	    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<?
	    use \Bitrix\Main\Page\Asset;
	    $jsbase = '/bitrix/templates/.default';
	   	$scripts = array(

		    $jsbase.'/js/jquery-1.6.2.min.js',
		    $jsbase.'/js/underscore.js',
		    $jsbase.'/js/jquery-ui-1.10.3.custom.js',
		    $jsbase.'/js/jquery.autoSuggest.js',
		    $jsbase.'/js/jquery.bxslider.min.js',
		    $jsbase.'/js/jquery.rateit.min.js',
		    $jsbase.'/js/jquery.localScroll.min.js',

		    $jsbase.'/js/chosen.jquery.js',
		    $jsbase.'/js/main.js',
		    $jsbase.'/js/jquery.lightbox-0.5.pack.js',
		    $jsbase.'/js/pic.core.js',
		    $jsbase.'/js/pic.jquery.js',
		    $jsbase.'/js/jcarousellite_1.0.1.js',
		    $jsbase.'/js/desaturate.js',
		    $jsbase.'/js/hls.js',
		    $jsbase.'/js/banner.js',
		    $jsbase.'/js/jquery.mousewheel-3.0.6.pack.js',
		    $jsbase.'/js/fancybox/jquery.fancybox.pack.js?v=2.1.4',
		    $jsbase.'/js/flexslider.js',
		    $jsbase.'/js/jquery.lightbox-0.5.pack.js',
		    $jsbase.'/js/jquery.collapsorz_1.1.min.js',
		    $jsbase.'/js/jquery.scrollTo.js',
		    $jsbase.'/js/jquery.tools.js',
		    $jsbase.'/js/jquery.maskedinput-1.3.min.js',
		    $jsbase.'/js/paging.js',
		    //$jsbase.'/js/paging_default.js',
		    $jsbase.'/js/redactor2/redactor.js',
		    $jsbase.'/js/redactor2/ru.js',
		    $jsbase.'/js/additional.js',
		    $jsbase.'/js/main_collapsorz.js',
		    $jsbase.'/js/protect_text.js',
	   		);
		 foreach ($scripts as $s)
         {
                Asset::getInstance()->addJs($s);
         }
        if ( strpos($APPLICATION->GetCurUri(), '/market/') === 0 ){

            $csss []= $jsbase.'/components/bitrix/catalog/market2/catalog_styles.css';
            $csss []= $jsbase.'/components/bitrix/catalog/market2/additional-v2.css"';
            //$APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/components/bitrix/catalog/market2/additional-v2.css");
        }
		$csss = array(
			'/css/chosen.css',
	   		$jsbase.'/common.css',
	   		$jsbase.'/js/redactor2/redactor.css',
			SITE_TEMPLATE_PATH .'/css/styles.css',
			'/theme/red/style.css',
			$jsbase.'/js/fancybox/jquery.fancybox.css?v=2.1.4'
		);
		 foreach ($csss as $s)
			 {$APPLICATION->SetAdditionalCSS($s);}
		$APPLICATION->ShowCSS();

 	//��� ������� �������
        $APPLICATION->ShowHeadStrings();
 		$APPLICATION->ShowHeadScripts();
    ?><!--[if lte IE 8]><link rel="stylesheet" href="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/css/ie.css" type="text/css"><![endif]-->

</head>
<body id="top" class="mkt blog">
<? $APPLICATION->ShowPanel(); ?>
  <div class="middle_banner" style="width:100%; margin: 0 auto; position: relative; z-index: 199; overflow: visible;">
    <div style="display: inline-block; text-align: center;">
        <?
        $APPLICATION->IncludeComponent("bitrix:advertising.banner", "", array(
                "TYPE" => "ENERGO_A1",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "CACHE_NOTES" => ""
            )
        );
        ?>
    </div>
</div>
<div class="main">
    <div class="header">
        <a class="logo" href="<?= SITE_DIR ?>" title="�������������� ������ ��������������">
            <img src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/images/logo.png" alt="�������������� �������">
        </a>
        <?
        $APPLICATION->IncludeComponent("bitrix:system.auth.form", "energo2", Array(
            "REGISTER_URL" => "/auth/", // �������� �����������
            "PROFILE_URL" => "/personal/profile/", // �������� �������
            "SHOW_ERRORS" => "Y"
        ), false
        );
        ?>
        <div class="search_block">
            <?
            $APPLICATION->IncludeComponent("bitrix:search.form", "header", Array(
                    "USE_SUGGEST" => "N",
                    "PAGE" => "#SITE_DIR#search/index.php",
                    "AJAX_PAGE" => "#SITE_DIR#_ajax/search.php"
                )
            );
            ?>
        </div><!--search_block-->
        <div class="clear"></div>
        <div class="main_memu">
            <?
            $APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                "ROOT_MENU_TYPE" => "top", // ��� ���� ��� ������� ������
                "MAX_LEVEL" => "1", // ������� ����������� ����
                "CHILD_MENU_TYPE" => "left", // ��� ���� ��� ��������� �������
                "USE_EXT" => "Y", // ���������� ����� � ������� ���� .���_����.menu_ext.php
                "MENU_CACHE_TYPE" => "A", // ��� �����������
                "MENU_CACHE_TIME" => "86400", // ����� ����������� (���.)
                "MENU_CACHE_USE_GROUPS" => "N", // ��������� ����� �������
                "MENU_CACHE_GET_VARS" => "", // �������� ���������� �������
                "FIRST"=>3,
                "SECOND"=>11
            ), false
            );
            ?>
            <div class="mm_bottom">
                <div class="custom_menu"><a href="/about/">� �������</a><a href="/reclame/">�������</a><a href="/feedback/">�������� �����</a></div>
                <div class="add_menu">
                    <noindex>
                        <?global $USER;
                        if($USER->IsAuthorized()):?>
                            <?//���� ��������, � ������� �������������� ������� �
                            if(!CModule::IncludeModule("iblock")) return;
                            $ownCompanyCount = CIBlockElement::GetList(
                                array("NAME" => "ASC", "SORT" => "ASC"),
                                array(
                                    "IBLOCK_ID" => 17,
                                    "ACTIVE" => "Y",
                                    "PROPERTY_USER" => $USER->GetID()
                                ),
                                array()
                            );
                            if($ownCompanyCount):?>
                                <a href="/personal/company/">���������� ���������</a>,<a href="/personal/company/prod_and_serv/">��� ������</a>,<a href="/personal/company/prod_and_serv/serv.php">��� ������</a>
                            <?else:?>
                                ����������:<a href="/personal/company/" rel="nofollow">��������</a>,<a href="/personal/company/prod_and_serv/" rel="nofollow">�����</a>,<a href="/personal/company/prod_and_serv/serv.php" rel="nofollow">������</a>
                            <?endif;?>
                        <?else:?>
                            ����������:<a href="/login/?&backurl=%2Fpersonal%2Fcompany%2F" rel="nofollow">��������</a>,<a href="/login/?&backurl=%2Fpersonal%2Fcompany%2Fprod_and_serv%2F" rel="nofollow">�����</a>,<a href="/login/?&backurl=%2Fpersonal%2Fcompany%2Fprod_and_serv%2Fserv.php" rel="nofollow">������</a>
                        <?endif;?>
                    </noindex>
                </div>
                <span class="date">�������: <?= date('d') ?>.<?= date('m') ?></span>
                <div class="clear"></div>
            </div><!--mm_bottom-->
        </div><!--main_memu-->
    </div><!--header-->
    <div class="content">
        <div class="content_box">
            <? // if (strpos($APPLICATION->GetCurUri(), '/advert/') === 0 || strpos($APPLICATION->GetCurUri(), '/personal/advert/') === 0 || strpos($APPLICATION->GetCurUri(), '/profile/') === 0 || strpos($APPLICATION->GetCurUri(), '/personal/profile/') === 0 || strpos($APPLICATION->GetCurUri(), '/company/') === 0 || strpos($APPLICATION->GetCurUri(), '/personal/company/') === 0): ?>
            <? if ((strpos($APPLICATION->GetCurUri(), '/personal/') === 0) && ($_REQUEST["forgot_password"] != "yes")): ?>
                <div class="sw_breadcrumbs">
                    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "news", array(
                        "START_FROM" => "0",
                        "PATH" => "",
                        "SITE_ID" => "s1"
                    ),
                        false,
                        array()
                    );?>
                </div>
            <? endif; ?>
	        <?if (!HIDE_LEFT) {?>
            <? if (($_SERVER["REQUEST_URI"] != "/work.php")  && ($_REQUEST["forgot_password"] != "yes")): ?>
                <?
                $APPLICATION->IncludeComponent("bitrix:main.include", "left", Array(
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "work",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_MODE" => "html",
                ), false, Array('HIDE_ICONS' => 'Y')
                );
                ?>
                <?if(!(defined('ERROR_404') && ERROR_404 == 'Y')):?>
                    <?
                    $APPLICATION->IncludeComponent("bitrix:main.include", "left", Array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "inc",
                        "AREA_FILE_RECURSIVE" => "N",
                        "EDIT_MODE" => "html",
                    ), false, Array('HIDE_ICONS' => 'Y')
                    );
                    ?>
                <? endif; ?>
            <? endif; ?>
			<? } ?>