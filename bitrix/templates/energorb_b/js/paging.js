/* -------------------------------------------------- *
 * Project scripts
 * -------------------------------------------------- *
 * Author: Morozov Igor
 * URL: http://www.morozoff.info/
 * Copyright: 2010 Morozov Igor
** -------------------------------------------------- */

PaginationSlider = function(totalPages, itemsPerPage, currentPage, url, unique){
   var pagObj = 
   {
       totalPages: totalPages,
       itemsPerPage: itemsPerPage,
       currentPage: currentPage,
       url: url,
       unique: unique,
       sliderTemplate: '',

       /*init: function() {
           this.totalPages = totalPages;
           this.itemsPerPage = itemsPerPage;
           this.currentPage = currentPage;
           this.url = url;
           this.unique = unique;
           console.log("init", 'a.exppages-ttl.js-exppages-id-'+this.unique);

           var pThis = this;
           $(document).ready(function() {
               pThis.afterPageLoad();
           });
       },*/

       afterPageLoad: function() {
           //if(!totalPages) return;
           /*if (this.totalPages < 15) {
               return;
           }*/

           this.makeSlider();
           $('a.exppages-ttl.js-exppages-id-'+this.unique).click(jQuery.proxy(this, 'sliderActivation'));
           $(document).bind('click', this.sliderHide);
           console.log("afterPageLoad", 'a.exppages-ttl.js-exppages-id-'+this.unique);
       },

       sliderActivation: function(e) {
          console.log(
                  $(e.target).closest('.js-section_list').find('.h3-title').text(),
                  $(this).closest('.js-section_list').find('.h3-title').text(),
                  'a.exppages-ttl.js-exppages-id-'+this.unique
            )
           e.preventDefault();
           var el = $(e.target).parents('div.b-pages');
           el.addClass('active-droppages');
           $(this.sliderTemplate).appendTo(el)
           .imSlider({
               sliderScrollArea: '.b-pageslider-i',
               sliderScrollItems: 'a',
               sliderShadL: '.pageslider-shd-l',
               sliderShadR: '.pageslider-shd-r',
               sliderDrag: '.pages-slider-track'
           });

           return false;
       },

       sliderHide: function(e) {
           var el = $(e.target);
           if((!el.hasClass('b-pageslider')) && (!el.parents('.b-pageslider').hasClass('active-pageslider'))) {
               $('.b-pageslider').empty();
               $('.b-pageslider').remove();
               $('.b-pages').removeClass('active-droppages');
           }
       },

       makeSlider: function() {
           var li = '';
           for (var i=1; i<=this.totalPages; i++) {
               li += '<li><a href="'+ this.url + ((i) * this.itemsPerPage) +'"'
                   + (this.currentPage == i ? ' class="slider-drop slider-hr"' : '')
                   + '>' + (this.totalPages > 10 && !(i % 10) ? '<strong>'+ i +'</strong>' : i) + '</a></li>';
           }

           this.sliderTemplate = '<div class="b-pageslider active-pageslider"><div class="b-pageslider-i">'
               + '<ul class="pagesslider__ul">'+ li +'</ul></div>';
           if(this.totalPages >25) this.sliderTemplate += '<div class="pages-slider-track"><i class="pages-slider-drag"></i></div>';
           else this.sliderTemplate += '<div class="pages-slider-track" style="display: none;"><i class="pages-slider-drag"></i></div>';
           this.sliderTemplate += '<i class="pageslider-shd-l"></i><i class="pageslider-shd-r"></i></div>';
       }
   }
   $(document).ready(function() {
       pagObj.afterPageLoad();
   });
   return pagObj;
};