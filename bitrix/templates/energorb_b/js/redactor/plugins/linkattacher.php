<? require( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );?>
<?//$_GET["PID"]=641?>
<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function chooseLink(cnt)
            {
                $('#redactor_link_url').val('EXT_LINK' + $('#redactor_link_id_' + cnt).val());
            }
            $(document).ready(function()
            {
                chooseLink(0);
            });
        </script>
    </head>
    <body>
        <div>
            <?// Отрисовка внешних ссылок
            if(CModule::IncludeModule("iblock"))
            {
                $resArticle = CIBlockElement::GetByID($_GET["articleId"]);
                if($obResArticle = $resArticle->GetNextElement())
                {
                    $arArticleProperties = $obResArticle->GetProperty("EXT_LINKS");
                    $linkMax = count($arArticleProperties["VALUE"],0);
                    $arLink = $arArticleProperties["VALUE"];
                    //$arDescriptions = $arProperties["DESCRIPTION"];
                    //print_r($arLink);
                }
            }
            else
            {
                echo "NO!";
            }
            ?>
            
            <ol style="padding: 0px">
                <?for($linkCnt = 0; $linkCnt < $linkMax; $linkCnt++):?>
                    <?$resLink = CIBlockElement::GetByID($arLink[$linkCnt]);
                    if($arLinkProperties = $resLink->GetNext()):?>
                        <li>
                            <input type="radio" <?= $linkCnt==0 ? 'checked' : ''?> value="<?= $linkCnt?>" name="redactor_link_url_radio" id="redactor_link_url_radio_<?= $linkCnt?>" onclick="chooseLink(<?= $linkCnt?>)">
                            <span name="redactor_link_url_<?= $linkCnt?>" id="redactor_link_url_<?= $linkCnt?>"><?= $arLinkProperties["NAME"]?></span>
                            <span name="redactor_link_description_<?= $linkCnt?>" id="redactor_link_description_<?= $linkCnt?>"><?= $arLinkProperties["PREVIEW_TEXT"]?></span>
                            <input type="hidden" value="<?= $arLink[$linkCnt]?>" name="redactor_link_id_<?= $linkCnt?>" id="redactor_link_id_<?= $linkCnt?>">
                        </li>
                    <?endif;?>
                <?endfor;?>
            </ol>
            <input type="hidden" value="unset" name="#redactor_link_url" id="redactor_link_url">
            <input type="text" value="видел на этом сайте" name="#redactor_link_text" id="redactor_link_text">
            <input type="button" name="" id="" value="%RLANG.insert%" onclick="redactorActive.insertLinkCode();" />&nbsp;&nbsp;
            <input type="button" name="" onclick="redactorActive.modalClose();" value="%RLANG.cancel%"  /> 
        </div>
    </body>
</html>
