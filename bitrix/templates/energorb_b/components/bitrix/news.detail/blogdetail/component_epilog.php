<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
// �������� $arResult ������� ���������, ����������� � �������
if(isset($arResult['arResult'])) {
   $arResult =& $arResult['arResult'];
         // ���������� �������� ����
   global $MESS;
   include_once(GetLangFileName(dirname(__FILE__).'/lang/', '/template.php'));
} else {
   return;
}
?>

<div class="left_col single">
	<div class="lc_block mb60">
		<ul class="autor-block-head">
			<li class="lh18">����� �����:</li>
			<li><strong><a href="<?=$arResult['my_SECTION']['SECTION_PAGE_URL']?>" class="h"><?=$arResult['my_USER']['LAST_NAME']?> <?=$arResult['my_USER']['NAME']?> <?=$arResult['my_USER']['SECOND_NAME']?></a></strong></li>
			<li><em><?=$arResult['my_USER']['WORK_POSITION']?></em></li>
		</ul>
		<?if ($arResult['my_USER']['PERSONAL_PHOTO']):?>
			<a href="<?=$arResult['my_SECTION']['SECTION_PAGE_URL']?>"><img class="autor" src="<?=$arResult['my_USER']['PERSONAL_PHOTO_PARAMS']['SRC']?>" alt="" /></a>
		<?endif;?>
		<div class="title">����������</div>
		<ul class="left_menu">
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "bloglist-related", Array(
					"AJAX_MODE" => "N",
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"NEWS_COUNT" => "6",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "DATE_CREATE",
					"SORT_ORDER2" => "DESC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array("ID","NAME","DATE_CREATE"),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => $arParams["DETAIL_URL"],
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"PARENT_SECTION" => $arResult['my_SECTION']['ID'],
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					),
					false
				);?>
		</ul>
		<a class="all-articles" href="<?=$arResult['my_SECTION']['SECTION_PAGE_URL']?>">��� ����������</a> 
	</div>

	
	<div class="lc_block bgr">
		<div class="title">������ � ���������</div>
		<?$APPLICATION->IncludeComponent("bitrix:news.line","blogdetail-articles",Array(
				"IBLOCK_TYPE" => "information_part",
				"IBLOCKS" => Array(2),
				"NEWS_COUNT" => "3",
				"FIELD_CODE" => Array("ID", "CODE", 'PREVIEW_PICTURE'),
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				//"DETAIL_URL" => "",
				"ACTIVE_DATE_FORMAT" => "d.m H:i",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "300",
				"CACHE_GROUPS" => "Y"
			)
		);?>
	</div>

</div>

<div class="middle_col">
	<div class="blog-content single-blog">
		<div class="mc_block">

			
			<?$APPLICATION->IncludeComponent(
				"bitrix:iblock.vote",
				"ajax",
				Array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ELEMENT_ID" => $arResult['ID'],
					"MAX_VOTE" => 5,
					"VOTE_NAMES" => array('1','2','3','4','5'),
					"CACHE_TYPE" => 'N',
					"CACHE_TIME" => 0
				)
			);?>
			
			<h2><?=$arResult['NAME']?></h2>
			<?if ($arResult['DETAIL_PICTURE']):?>
				<?$detailPicture = CFile::ResizeImageGet($arResult['DETAIL_PICTURE']['SRC'], array('width'=>400, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true);
				if (!isset($detailPicture['src'])) $detailPicture['src'] = $arResult['DETAIL_PICTURE']['SRC'];
				?>
				<img class="b-preview" src="<?=$detailPicture['src']?>" alt="" style="float:left;margin:0 20px 15px 0;">
			<?endif;?>
			<div class="news_item formated text">
						<?=$arResult['~DETAIL_TEXT']?>
			</div>
			<ul class="meta">
				<li class=""><?=$arResult['my_DATE']?> \ <?=$arResult['my_SECTION']['NAME']?></li>
				<?if (strip_tags($arResult['TAGS'])):?>
					<li class="">����: <?=$arResult['TAGS']?></li>
				<?endif;?>
				<li class="social-buttons">
					<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
					<div class="yashare-auto-init"
						data-yashareL10n="ru"
						data-yashareType="button"
						data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,lj,gplus"
						data-yashareLink="<?=$_SERVER['HTTP_HOST'].$arResult['DETAIL_PAGE_URL']?>"
						data-yashareTitle="<?=$arResult['NAME']?>"
						data-yashareDescription="<?=$arResult['NAME']?>"
						data-yashareImage="<?=$_SERVER['HTTP_HOST'].$arResult['PREVIEW_PICTURE']['SRC']?>"
					></div>
				</li>
				<!--<li class="comments"><a href="#">������������<i class="number"></i>2</a></li>-->
			</ul>
			<div class="clear"></div>
		</div>         
		
		<?$APPLICATION->IncludeComponent("bitrix:forum.topic.reviews","blogdetail-comments",Array(
				"SHOW_LINK_TO_FORUM" => "N",
				"FILES_COUNT" => "0",
				"FORUM_ID" => "1",
				"IBLOCK_TYPE" => "information_part",
				"IBLOCK_ID" => "50",
				"ELEMENT_ID" => $arResult['ID'],
				"AJAX_POST" => "N", 
				"POST_FIRST_MESSAGE" => "Y",
				"POST_FIRST_MESSAGE_TEMPLATE" => "#IMAGE#[url=#LINK#]#TITLE#[/url]#BODY#",
				"URL_TEMPLATES_READ" => "read.php?FID=#FID#&TID=#TID#",
				"URL_TEMPLATES_DETAIL" => "photo_detail.php?ID=#ELEMENT_ID#",
				"URL_TEMPLATES_PROFILE_VIEW" => "profile_view.php?UID=#UID#",
				"MESSAGES_PER_PAGE" => "10",
				"PAGE_NAVIGATION_TEMPLATE" => "",
				"DATE_TIME_FORMAT" => "d.m.Y H:i:s",
				"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
				"EDITOR_CODE_DEFAULT" => "Y",
				"SHOW_AVATAR" => "Y",
				"SHOW_RATING" => "N",
				'SHOW_SUBSCRIBE' => 'Y',
				"RATING_TYPE" => "like",
				"SHOW_MINIMIZED" => "N",    
				"USE_CAPTCHA" => "Y",
				"PREORDER" => "Y",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "0"
			)
		);?>
		
		
	</div>
</div>