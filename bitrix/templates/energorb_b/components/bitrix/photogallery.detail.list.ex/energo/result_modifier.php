<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams1 = array(
	"MAX_VOTE" => intval($arParams["MAX_VOTE"])<=0? 5: intval($arParams["MAX_VOTE"]),
	"VOTE_NAMES" => is_array($arParams["VOTE_NAMES"])? $arParams["VOTE_NAMES"]: array(),
	"DISPLAY_AS_RATING" => $arParams["DISPLAY_AS_RATING"]);
$arResult["VOTE_NAMES"] = array();
foreach($arParams1["VOTE_NAMES"] as $k=>$v)
{
	if(strlen($v)>0)
		$arResult["VOTE_NAMES"][]=htmlspecialchars($v);
	if(count($arResult["VOTE_NAMES"])>=$arParams1["MAX_VOTE"])
		break;
}

//��� ��� ������
$replaceFields = array(
    "URL"
);
$replaceFieldsNew = array(
    "#SECTION_CODE#" => "CODE",
    "#SECTION_ID#" => "ID"
);
foreach($arResult["ELEMENTS_LIST"] as $resKey => $res)
{
   foreach($replaceFields as $field)
   {
      foreach($replaceFieldsNew as $replaceOld => $replaceNewField)
         $res[$field] = str_replace($replaceOld, $arResult["SECTION"][$replaceNewField], $res[$field]);
   }
   $arResult["ELEMENTS_LIST"][$resKey] = $res;
}