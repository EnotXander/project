<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<div class="mc_footer"><?=$arResult["NavTitle"]?> 

<?if($arResult["bDescPageNumbering"] === true):?>


	<font class="text">

	<?if ($arResult["NavPageNomer"] > 1):?>
		<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_next")?></a>
	<?else:?>
		<?=GetMessage("nav_next")?>&nbsp;&nbsp;<?=GetMessage("nav_end")?>
	<?endif?>

<?else:?>

        </font>

	<?if ($arResult["NavPageNomer"] > 1):?>
		<?if($arResult["bSavePage"]):?>
			<a class="prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><<  <?=GetMessage("nav_prev")?></a>
		<?else:?>
			<?if ($arResult["NavPageNomer"] > 2):?>
				<a class="prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><<  <?=GetMessage("nav_prev")?></a>
			<?else:?>
				<a class="prev" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><<  <?=GetMessage("nav_prev")?></a>
			<?endif?>
			
		<?endif?>

	<?else:?>
	<?endif?>


                                
                               
	<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<a class="next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_next")?>  >></a>
	<?else:?>
		<!--<?=GetMessage("nav_next")?>  >><?=GetMessage("nav_end")?>-->
	<?endif?>

<?endif?>          

</div>


    