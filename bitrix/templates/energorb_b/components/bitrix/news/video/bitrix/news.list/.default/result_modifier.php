<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$obParser = new CTextParser;

$APPLICATION->SetTitle("���� � ����� ��������, �����������, ����������� �� EnergoBelarus.by");
$APPLICATION->SetPageProperty("description", "������������ � ���� � ������������� ���������� �����������: Energy Expo, �������������. �����������,����������. ����, �������������."); 
//$APPLICATION->AddHeadString('<meta name="abstract" content="������������ � ���� � ������������� ���������� �����������: Energy Expo, �������������. �����������,����������. ����, �������������.">', true);
$APPLICATION->SetPageProperty("keywords", "Energy Expo, �������������. �����������,����������. ����, �������������, ����, ������, ��������.");

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      $arElement["PREVIEW_TEXT"] = strip_tags($arElement["PREVIEW_TEXT"]);
      $arElement["PREVIEW_TEXT"] = $obParser->html_cut($arElement["PREVIEW_TEXT"], 150);
      if (is_array($arElement["PREVIEW_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
                         $arElement["PREVIEW_PICTURE"], array("width" => 160, "height" => 80), BX_RESIZE_IMAGE_EXACT, true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

         $arElement["PREVIEW_IMG"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
         $arFileTmp = CFile::ResizeImageGet(
                         $arElement["PREVIEW_PICTURE"], array("width" => 400, "height" => 240), BX_RESIZE_IMAGE_EXACT, true
         );
         //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);
         $arElement["PREVIEW_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
      
      //duration
      $time = $arElement['PROPERTIES']['VIDEO_DURATION']['VALUE'];
      if($time)
      {
         if ($time > 3600)
            $strTime = sprintf('%02d:%02d:%02d', $time / 3600, ($time % 3600) / 60, ($time % 3600) % 60);
         else
            $strTime = sprintf('%02d:%02d', ($time % 3600) / 60, ($time % 3600) % 60);
         $arElement['DURATION'] = $strTime;
      }
      $arResult['ITEMS'][$key] = $arElement;
   }
}
?>