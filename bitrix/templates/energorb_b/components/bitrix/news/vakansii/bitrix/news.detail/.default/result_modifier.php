<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock")) die();

$arResult["COMPANY"] = GetIblockElement($arResult['PROPERTIES']['company']['VALUE']);
$arResult["REGION"] = GetIblockElement($arResult["COMPANY"]['PROPERTIES']['COUNTRY']['VALUE']);
$arResult["CITY"] = GetIblockElement($arResult["COMPANY"]['PROPERTIES']['CITY']['VALUE']);
$arResult["COMPANY"]['PREVIEW_PICTURE_FORMATED'] = CFile::ShowImage($arResult["COMPANY"]['PREVIEW_PICTURE'], 140, 40, 'class="logo"');