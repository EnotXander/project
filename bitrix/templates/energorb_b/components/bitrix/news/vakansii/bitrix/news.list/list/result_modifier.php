<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock")) die();

$arSfer = Array();
$items = GetIBlockElementList(40, 0, Array("SORT" => "ASC", "NAME" => "ASC"));
while ($arItem = $items->GetNext())
{
   $arResult["SFER"][] = $arItem;
}

$arResult["SPECS"] = array();
$items = GetIBlockElementList(30, 0, Array("SORT" => "ASC", "NAME" => "ASC"));
while ($arItem = $items->GetNext())
{
   if(((int)$_REQUEST['arrFilter_pf']["specialty"]) && ($arItem['ID'] == (int)$_REQUEST['arrFilter_pf']["specialty"]))
      $arItem["SELECTED"] = true;
   $arResult["SPECS"][] = $arItem;
}

$arResult["REGS"] = array();
$items = GetIBlockElementList(22, 0, Array("SORT" => "ASC", "NAME" => "ASC"));
while ($arItem = $items->GetNext())
{
   if(((int)$_REQUEST['arrFilter_pf']["region"]) && ($arItem['ID'] == (int)$_REQUEST['arrFilter_pf']["region"]))
      $arItem["SELECTED"] = true;
   $arResult["REGS"][] = $arItem;
}