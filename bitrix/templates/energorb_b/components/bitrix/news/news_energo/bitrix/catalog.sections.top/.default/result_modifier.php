<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;

foreach($arResult["SECTIONS"] as $key => $arSection)
{
	foreach ($arSection["ITEMS"] as $keyI => $arElement) 
	{
		
		if(is_array($arElement["PREVIEW_PICTURE"]))
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arElement["PREVIEW_PICTURE"],
				array("width" => 100, "height" => 100),
				BX_RESIZE_IMAGE_EXACT,
				true
			);
			//$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);

			$arResult["SECTIONS"][$key]["ITEMS"][$keyI]["PREVIEW_IMG"] = array(
				"SRC" => $arFileTmp["src"],
				"WIDTH" => $arFileTmp["width"],
                                "HEIGHT" => $arFileTmp["height"],
			);
			if(strlen($arElement["ACTIVE_FROM"])>0)
				$arResult["SECTIONS"][$key]["ITEMS"][$keyI]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arElement["ACTIVE_FROM"], CSite::GetDateFormat()));
			else
				$arResult["SECTIONS"][$key]["ITEMS"][$keyI]["DISPLAY_ACTIVE_FROM"] = "";
			if($arParams["PREVIEW_TRUNCATE_LEN"] > 0)
				$arResult["SECTIONS"][$key]["ITEMS"][$keyI]["PREVIEW_TEXT"] = $obParser->html_cut($arElement["PREVIEW_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);

		}		
	}
}


?>