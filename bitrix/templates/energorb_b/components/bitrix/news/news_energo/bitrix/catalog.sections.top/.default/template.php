<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["SECTIONS"] as $key => $arSection):?>
<?if ($key%4==0):?>
<div class="content_box">
	<div class="middle_col">
<?endif;?>
	<div class="mc_block double_news_list">
		<ul class="mb_menu">
			<li class="mb_title"><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a><div class="sclew"></div></li>
			<li class="mb_clear"><a href="<?=$arSection["SECTION_PAGE_URL"]?>">��� ������� �������</a></li>
		</ul>
		<div class="brdr_box_hmenu news_list">
		<?
		$cell = 0;
		foreach($arSection["ITEMS"] as $arElement):
		?>
		<?
		/*$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCST_ELEMENT_DELETE_CONFIRM')));*/
		?>
		<div class="item_row" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
			<?if(is_array($arElement["PREVIEW_IMG"])):?>
				<div class="image_box"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img border="0" src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="<?=$arElement["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /></a></div>
			<?endif?>
			<div class="row">
				<div class="title"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></div>
				<p><?=$arElement["PREVIEW_TEXT"]?><span class="date">\ <?=$arElement['DISPLAY_ACTIVE_FROM']?></span></p>
			</div>
		</div><!--item_row-->
		<?
		endforeach; // foreach($arResult["ITEMS"] as $arElement):
		?>
		</div>
	</div>
<?if ($key%2 == 1):?>
<div class="clear"></div>
<?endif;?>
<?if ($key%4 == 3):?>
	</div>
</div>
<div class="content_box">
	<div style="height:70px; background:#cad7c5;" class="middle_banner">
<?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "news_main", Array(
	"TYPE" => "A2",	// ��� �������
	"NOINDEX" => "N",	// ��������� � ������ noindex/nofollow
	"CACHE_TYPE" => "A",	// ��� �����������
	"CACHE_TIME" => "3600",	// ����� ����������� (���.)
	),
	false
);?>
	</div>
</div>
<div class="clear"></div>
<?endif;?>
<?endforeach?>
<div class="clear"></div>