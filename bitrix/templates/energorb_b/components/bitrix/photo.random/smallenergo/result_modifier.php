<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


if (is_array($arResult["PICTURE"]) && count($arResult["PICTURE"]))
{
   $arFileTmp = CFile::ResizeImageGet(
         $arResult["PICTURE"],
         array("width" => 163, "height" => 163),
         BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
         true
   );
   $arResult["PICTURE"] = array(
       "SRC" => $arFileTmp["src"],
       "WIDTH" => $arFileTmp["width"],
       "HEIGHT" => $arFileTmp["height"],
   );
}
?>