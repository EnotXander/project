<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="photo-random">
   <? if (is_array($arResult["PICTURE"])): ?>
      <a href="<?= $arResult["DETAIL_PAGE_URL"] ?>">
         <img border="0" src="<?= $arResult["PICTURE"]["SRC"] ?>" width="100%" alt="<?= $arResult['NAME'] ?>" title="<?= $arResult['NAME'] ?>" style="margin-bottom:7px;"/>
      </a>
   <br />
   <? endif ?>
   <?= $arResult["NAME"] ?>
</div>
