<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$APPLICATION->SetTitle("���� � ����� ��������, �����������, ����������� �� EnergoBelarus.by");
$APPLICATION->SetPageProperty("description", "������������ � ���� � ������������� ���������� �����������: Energy Expo, �������������. �����������,����������. ����, �������������."); 
$APPLICATION->AddHeadString('<meta name="abstract" content="������������ � ���� � ������������� ���������� �����������: Energy Expo, �������������. �����������,����������. ����, �������������.">', true);
$APPLICATION->SetPageProperty("keywords", "Energy Expo, �������������. �����������,����������. ����, �������������, ����, ������, ��������.");

//��� ��� ������
$replaceFields = array(
    "LINK",
    "NEW_LINK",
    "EDIT_LINK",
    "EDIT_ICON_LINK",
    "DROP_LINK"
);
$replaceFieldsNew = array(
    "#SECTION_CODE#" => "CODE",
    "#SECTION_ID#" => "ID"
);
foreach($arResult["SECTIONS"] as $resKey => $res)
{
   foreach($replaceFields as $field)
   {
      foreach($replaceFieldsNew as $replaceOld => $replaceNewField)
         $res[$field] = str_replace($replaceOld, $res[$replaceNewField], $res[$field]);
   }
   $arResult["SECTIONS"][$resKey] = $res;
}