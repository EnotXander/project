//���������� ��������
$(document).ready(function(){
   $('.js-row-new').live("click", function(){
      $(this).hide();
      var $form = $('.js-row-form');
      $form
         .show()
         .find('.js-select-company')
            .chosen({no_results_text: "������ �� ������� �� �������", search_contains: true})
            .change(function(event){
               $form
                  .find('input[name = new\\[COMPANY\\]]').val($(this).val())
                  .end();
               var exists = $(event.target).find('option:selected').attr("rel");
               if(!exists.length)
                  $form.find('.js-row-form-exists').html("&nbsp;");//exists = "<span style='display: none;'>&nbsp;<span>";
               else
                  $form.find('.js-row-form-exists').text(exists);
            })
         .end()
         .find('.js-select-user')
            .chosen({no_results_text: "������ �� ������� �� �������", search_contains: true})
            .change(function(event){
               $form.find('input[name = new\\[USER\\]]').val($(this).val());
            });
   });
   $('.js-row-form input[name = close]').live("click", function(){
      $(this)
         .closest('.js-row-form').hide()
         .find('.js-select-company, .js-select-user, .row_control input[type = hidden]').val('').trigger("liszt:updated");
      $('.js-row-new').show();
   })
});