<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$arItem["DETAIL_TEXT"] = strip_tags($arItem["DETAIL_TEXT"]);
	$arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["DETAIL_TEXT"], 100);
	$arResult['ITEMS'][$key] = $arItem;
	//PrintAdmin($arItem);
	if(is_array($arItem["DETAIL_PICTURE"]) || is_array($arItem["PREVIEW_PICTURE"]))
	      {
	         $FILE = is_array($arItem["DETAIL_PICTURE"]) ? $arItem["~DETAIL_PICTURE"] : $arItem["~PREVIEW_PICTURE"];
	         $arFileTmp = CFile::ResizeImageGet(
	            $FILE,
	            array("width" => 100, "height" => 100),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            array()
	         );
	         $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
	            "SRC" => $arFileTmp["src"],
	            "WIDTH" => $arFileTmp["width"],
	            "HEIGHT" => $arFileTmp["height"],
	         );
	      }
}


?>