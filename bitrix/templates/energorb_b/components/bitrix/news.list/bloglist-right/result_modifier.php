<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach ($arResult["ITEMS"] as $key=>$arItem){
	$itemDate = $arItem['DISPLAY_ACTIVE_FROM'] ?: $arItem['DATE_CREATE'];
	$itemDate = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($itemDate, CSite::GetDateFormat()));
	$arResult['ITEMS'][$key]['my_DATE'] = $itemDate;
	
	if ($arParams["PREVIEW_TRUNCATE_LEN"]){
		$obParser = new CTextParser;
		$arResult['ITEMS'][$key]['my_PREVIEW_TEXT'] = $obParser->html_cut($arItem["NAME"], $arParams["PREVIEW_TRUNCATE_LEN"]);
	}else{
		$arResult['ITEMS'][$key]['my_PREVIEW_TEXT'] = $arItem["NAME"];
	}
}
?>