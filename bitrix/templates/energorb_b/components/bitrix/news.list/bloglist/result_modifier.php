<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $APPLICATION;

foreach ($arResult["ITEMS"] as $key=>$arItem){
	//�������� ���� �������
	$resSection = CIBlockSection::GetList(
		array(),
		array(
			'ID' => $arItem['IBLOCK_SECTION_ID'],
			'IBLOCK_ID' => $arItem['IBLOCK_ID']
		),
		false,
		array(
			'ID', 'NAME', 'DESCRIPTION', 'SECTION_PAGE_URL', 'UF_USER_ID'
		)
	);
	$arResult['ITEMS'][$key]['my_SECTION'] = $resSection->GetNext();
	$arResult['ITEMS'][$key]['my_SECTION']['SECTION_PAGE_URL'] = str_replace(
		'#UF_USER_ID#',
		$arResult['ITEMS'][$key]['my_SECTION']['UF_USER_ID'],
		$arResult['ITEMS'][$key]['my_SECTION']['SECTION_PAGE_URL']
	);
	
	//�������� ����� ��� ������
	if ($arParams["PREVIEW_TRUNCATE_LEN"]){
		$obParser = new CTextParser;
		$arResult['ITEMS'][$key]['PREVIEW_TEXT'] = $obParser->html_cut($arItem["~PREVIEW_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
	}else{
		$arResult['ITEMS'][$key]['PREVIEW_TEXT'] = $arItem["~PREVIEW_TEXT"];
	}
	
	//�������� ���� �����
	$resUser = CUser::GetByID($arResult['ITEMS'][$key]['my_SECTION']['UF_USER_ID']);
	$arrUser = $resUser->GetNext();
	//$arrUserPhoto = CFile::GetFileArray($arrUser['PERSONAL_PHOTO']);

	$arrUserPhoto = CFile::ResizeImageGet(
		$arrUser['PERSONAL_PHOTO'], array("width" => 185, "height" => 184), BX_RESIZE_IMAGE_EXACT, true
	  );

	if ($arrUserPhoto) $arrUser['PERSONAL_PHOTO_PARAMS'] = $arrUserPhoto;

	$arResult['ITEMS'][$key]['my_USER'] = $arrUser;

	// �������� ������ ������������
	$resElementx = CIBlockElement::GetList(
			array("ACTIVE_FROM"=>"DESC"),
			array(
				'SECTION_ID' => $arResult['ITEMS'][$key]['my_SECTION']['ID'],
				'IBLOCK_ID' => $arParams["IBLOCK_ID"]
			),
			false,
		    array('nTopCount' => 6), array('DETAIL_PAGE_URL', 'DISPLAY_ACTIVE_FROM', 'NAME', 'DATE_CREATE')
		);
	$arResult['ITEMS'][$key]['ITEMSx'] = array();// ������� ������ ����� ������������
	while ( $Elementx = $resElementx->getNext() ) {

		$itemDate = $Elementx['DISPLAY_ACTIVE_FROM'] ?: $Elementx['DATE_CREATE'];
		$itemDate = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($itemDate, CSite::GetDateFormat()));
		$Elementx['my_DATE'] = $itemDate;

		$arResult['ITEMS'][$key]['ITEMSx'] []= $Elementx;
	};
	//������ ����
	$itemDate = $arItem['DISPLAY_ACTIVE_FROM'] ?: $arItem['DATE_CREATE'];
	$itemDate = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($itemDate, CSite::GetDateFormat()));
	$arResult['ITEMS'][$key]['my_DATE'] = $itemDate;
	
	//���� ������� ��������
	$arrTags = explode(',', $arItem['TAGS']);
	foreach ($arrTags as $k=>$v){
		$v = trim($v);
		$arrTags[$k] = '<a href="/blogs/tags/'.str_replace(' ', '+', $v).'">'.$v.'</a>';
	}
	$arResult['ITEMS'][$key]['TAGS'] = implode(', ', $arrTags);
	
}
//$arResult["NAV_STRING"] = str_replace('PAGEN=', 'PAGEN_1=',$arResult["NAV_STRING"]);
//PrintAdmin($arResult);