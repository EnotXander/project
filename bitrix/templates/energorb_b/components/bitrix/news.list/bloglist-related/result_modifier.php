<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach ($arResult["ITEMS"] as $key=>$arItem){
	$itemDate = $arItem['DISPLAY_ACTIVE_FROM'] ?: $arItem['DATE_CREATE'];
	$itemDate = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($itemDate, CSite::GetDateFormat()));
	$arResult['ITEMS'][$key]['my_DATE'] = $itemDate;
}