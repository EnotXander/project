<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $GBL_SHOW_FAST_ORDER;
$GBL_SHOW_FAST_ORDER = true;

?>
<div class="middle_col" style="overflow:visible;">
   <div class="mc_block"><div class="bread_market">
		<ul class="market_bread">
			<li class="">
				<a href="/">�������</a>
			</li>
			<?//������ ����� (�������� ������)?>
			<li class="marketf">
				<div class="submarket haschild">
					<?if ($arParams["IBLOCK_ID"] == $arParams["IBLOCK_PRODUCT_ID"]):?>
						<a href="/market/" class="submarket-a"><?= $arParams["PAGE_NAME"]?></a>
					<?elseif($arParams["IBLOCK_ID"] == $arParams["IBLOCK_USLUGI_ID"]):?>
						<a href="/services/" class="submarket-a">������</a>
					<?endif;?>
					<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "bread", Array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"SECTION_ID" => $arSect["ID"],
						"SECTION_CODE" => $arSect["CODE"],
						"DISPLAY_PANEL" => "N",
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						//"CACHE_TYPE" => 'N',
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						'TOP_DEPTH' => 1,
						'NAME_CUT' => 50,
						'CURRENT_SECTION' => $arResult["VARIABLES"]["SECTION_ID"]
						), $component
					);?>
				</div>
			</li>
			<?
			if(!CModule::IncludeModule("iblock")) die();
			
			if(strlen($arResult["VARIABLES"]["SECTION_CODE"])) {

				$resChain = CIBlockSection::GetList(
					array(),
					array("=CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					      "IBLOCK_ID" => $arParams["IBLOCK_ID"])
				);

				if ($elementChain = $resChain->GetNext()) 
					$arResult["VARIABLES"]["SECTION_ID"] = $elementChain["ID"];
			}

			$nav = CIBlockSection::GetNavChain(false, $arResult["VARIABLES"]["SECTION_ID"]);
			?>
			<?while ($arSect = $nav->GetNext()):?>
				<?
				$subsectCount = CIBlockSection::GetCount(array(
					'IBLOCK_ID' => $arParams["IBLOCK_ID"],
					'SECTION_ID' => $arSect["ID"]
				));
				$subsectCountClass = $subsectCount > 0 ? ' haschild' : '';
				?>
				<li class="marketf">
					<div class="submarket<?=$subsectCountClass?>">
						<a href="<?=$arSect['SECTION_PAGE_URL']?>" class="submarket-a"><?=$arSect['NAME']?></a>
						<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "bread", Array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $arSect["ID"],
							"SECTION_CODE" => $arSect["CODE"],
							"DISPLAY_PANEL" => "N",
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
							'TOP_DEPTH' => 1,
							'NAME_CUT' => 50,
							'CURRENT_SECTION' => $arResult["VARIABLES"]["SECTION_ID"]
							), $component
						);?>
					</div>
				</li>
			<?endwhile;?>
		</ul>
   </div> </div>
</div>
<?
/// ��� �������� ���� ���, ���� �� ������� ������, �����������
$brands_ar = cacheFunction( 3600, 'BRANDS' . $arParams['IBLOCK_ID'].$arResult["VARIABLES"]["SECTION_ID"], function () use ( $arParams, $arResult ) {
	$res           = [ ];

	// �������� ���������� �� ��������� � ������
	$res["BRANDS"]     = array();
	$res["BRANDS_IDS"] = array();
	$arSelect               = array( "ID", "NAME", "PROPERTY_FIRM", "IBLOCK_ID" );
	$arFilter               = array(
		"IBLOCK_ID"           => $arParams['IBLOCK_ID'],
		"SECTION_ID"          => $arResult["VARIABLES"]["SECTION_ID"],
		"INCLUDE_SUBSECTIONS" => "Y",
		"ACTIVE"              => "Y"
	);
	$resem                    = CIBlockElement::GetList( array(), $arFilter, false, false, $arSelect );
	while ( $arFields = $resem->GetNext() ) {
		if ( ! in_array( $arFields['PROPERTY_FIRM_VALUE'], $res["BRANDS_IDS"] ) ) {
			$res["BRANDS_IDS"][] = $arFields['PROPERTY_FIRM_VALUE'];
		}
	}
	if (!empty($res["BRANDS_IDS"])) {
		$arFilter               = array(
			"IBLOCK_ID"            => IBLOCK_COMPANY,
			"ID"                   => $res["BRANDS_IDS"],
			"ACTIVE"               => "Y",
			"!PROPERTY_REMOVE_REL" => LANG == 's1' ? 63 : 142
		);
		$res["BRANDS_IDS"] = array();
		$resem                    = CIBlockElement::GetList( array( "NAME" => "ASC" ), $arFilter );
		while ( $arFields = $resem->GetNext() ) {
			if ( ! in_array( $arFields['ID'], $res["BRANDS_IDS"] ) ) {
				$res["BRANDS_IDS"][]              = $arFields["ID"];
				$res["BRANDS"][ $arFields["ID"] ] = $arFields;
			}
		}
	}
return $res;
} );

?>
<?
$ElementID = $APPLICATION->IncludeComponent("bitrix:catalog.element", "", Array(
	'brands_ar' => $brands_ar,
	'firms' => $firms,
	"SITE_NAME" => $arParams["SITE_NAME"],
	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
    "META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
    "META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
    "BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
    "BASKET_URL" => $arParams["BASKET_URL"],
    "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
    "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
    "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
    "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    "SET_TITLE" => $arParams["SET_TITLE"],
    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
    "PRICE_CODE" => $arParams["PRICE_CODE"],
    "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
    "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
    "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
    "PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
    "LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
    "LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
    "LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
    "LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],
    "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
    "OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
    "OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
    "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
    "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
    "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
    "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
    "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
    "PAGEN_1" => $_GET["PAGEN_1"]
        ), $component
);
?>
<? if ($arParams["USE_REVIEW"] == "Y" && IsModuleInstalled("forum") && $ElementID): ?>
   <br />
   <?
   $APPLICATION->IncludeComponent("bitrix:forum.topic.reviews", "", Array(
       "CACHE_TYPE" => $arParams["CACHE_TYPE"],
       "CACHE_TIME" => $arParams["CACHE_TIME"],
       "MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
       "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
       "PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
       "FORUM_ID" => $arParams["FORUM_ID"],
       "URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
       "SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
       "ELEMENT_ID" => $ElementID,
       "IBLOCK_ID" => $arParams["IBLOCK_ID"],
       "AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
       "POST_FIRST_MESSAGE" => $arParams["POST_FIRST_MESSAGE"],
       "URL_TEMPLATES_DETAIL" => $arParams["POST_FIRST_MESSAGE"] === "Y" ? $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"] : "",
           ), $component
   );
   ?>
<? endif ?>
<? if ($arParams["USE_ALSO_BUY"] == "Y" && IsModuleInstalled("sale") && $ElementID): ?>

   <?
   $APPLICATION->IncludeComponent("bitrix:sale.recommended.products", ".default", array(
       "ID" => $ElementID,
       "MIN_BUYES" => $arParams["ALSO_BUY_MIN_BUYES"],
       "ELEMENT_COUNT" => $arParams["ALSO_BUY_ELEMENT_COUNT"],
       "LINE_ELEMENT_COUNT" => $arParams["ALSO_BUY_ELEMENT_COUNT"],
       "DETAIL_URL" => $arParams["DETAIL_URL"],
       "BASKET_URL" => $arParams["BASKET_URL"],
       "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
       "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
       "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
       "CACHE_TYPE" => $arParams["CACHE_TYPE"],
       "CACHE_TIME" => $arParams["CACHE_TIME"],
       "PRICE_CODE" => $arParams["PRICE_CODE"],
       "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
       "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
       "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
           ), $component
   );
   ?>
<?endif?>
<div class="clear"></div>