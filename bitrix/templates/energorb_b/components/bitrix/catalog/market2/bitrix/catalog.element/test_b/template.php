<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
   <div class="content-box">
      <div class="middle_col">
         <div class="left_col">
            <? if (count($arResult["BRANDS"]) > 0): ?>
               <div class="lc_block">
                  <div class="title">�������� � �������</div>
                  <ul class="left_menu">
                     <?
                     $i = 0;
                     foreach ($arResult["BRANDS"] as $arBrand):
                        ?>
                        <? if ($i < 50): ?>
                           <? if ($arBrand["DETAIL_PAGE_URL"] != NULL): ?>
                              <li>
                                 <a href="<?= $arBrand["DETAIL_PAGE_URL"] ?>"><?= $arBrand["NAME"] ?></a>
                              </li>
                           <? endif ?>
                        <? endif ?>
                        <? $i++; ?>
                     <? endforeach ?>
                  </ul>
               </div>
            <? endif; ?>
		   <!-- ad C3-1 -->
           <div class="lc_block">
               <div class="left_banner_185">
                  <?
                  $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
                      "TYPE" => "C3_1",
                      "NOINDEX" => "N",
                      "CACHE_TYPE" => "A",
                      "CACHE_TIME" => "0",
                      "CACHE_NOTES" => ""
                          )
                  );
                  ?>
               </div>
            </div>
				<!-- ad C3-2 -->
           <div class="lc_block">
               <div class="left_banner_185">
                  <?
                  $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
                      "TYPE" => "C3_2",
                      "NOINDEX" => "N",
                      "CACHE_TYPE" => "A",
                      "CACHE_TIME" => "0",
                      "CACHE_NOTES" => ""
                          )
                  );
                  ?>
               </div>
            </div>
				
         </div>

         <div class="middle_col" style="  width: 680px;">
            <div class="mc_block">
               <div class="catalog-element">
                  <h1>
                     <? if ($USER->IsAuthorized() && in_array($USER->GetID(), $arResult["PROPERTIES"]["FAVORITES"]["VALUE"])): ?>
                        <a href="javascript:void(0)" onclick="AddToFavorites({productId: <?= $arResult["ID"] ?>, context: $(this).find('.b_star'), inFavorites: function() {
                                       //this.addClass('star_sel')
                                    }, outFavorites: function() {
                                       //this.removeClass('star_sel')
                                    }})">
                           <i class="icons b_star<? if (in_array($USER->GetID(), $arResult["PROPERTIES"]["FAVORITES"]["VALUE"])): ?> star_sel<? endif; ?>"></i>
                        </a>
                     <? endif; ?>
                     <?= $arResult["NAME"] ?>
                  </h1>
                  <?if($arResult['COMPANY']['PROPERTIES']['REMOVE_REL']['VALUE_ENUM_ID'] ==false):?>
                      <div class="meta">
                         <i class="icon_map"></i>
                         <?foreach($arResult['ADRESS'] as $key1 => $address):?>
                            <?=$address?>
                            <?if (isset($arResult['ADRESS'][$key1+1])): echo ", "; else: echo "."; endif;//� ����� ������ ������� �����?>
                         <?endforeach;?>
                         <?=($arResult['PROPERTIES']['prod']['VALUE']) ? "|  �������������: " : " " ?><?= $arResult['PROPERTIES']['prod']['VALUE'] ?>
                      </div>
                  <?endif;?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="2">
                     <tbody>
                        <tr>
	                        <td width="0%" valign="top" align="center">
		                        <div class="big-imgs">
			                        <? $block_width = 450; $min_height=$arResult['MAX_H']+40; ?>
			                        <? foreach ( $arResult["PROPERTIES"]["more_photo"]["BIG_SRC"] as $i => $src ): ?>
                                        <?

                                        $alt = $arResult["NAME"] ; $title = '������  '.$arResult["NAME"] ;
                                        ?>
				                        <div id="img<?= $i + 1; ?>" class="img-wrap" style="height: <?= $min_height ?>px; position: relative;">
					                        <? if (    ( $arResult["PROPERTIES"]["more_photo"]["ORIGINAL_SRC"][ $i ]["WIDTH"] > $block_width )
					                                || ( $arResult["PROPERTIES"]["more_photo"]["ORIGINAL_SRC"][ $i ]["HEIGHT"] > $min_height ) ):
						                        ?>
						                        <a href="<?= $arResult["PROPERTIES"]["more_photo"]["ORIGINAL_SRC"][ $i ]["SRC"]; ?>" class="fancybox" rel="group">
							                        <img src="<?= $src["src"]; ?>" height="<?= $src["height"] ?>"
                                                         width="<?=  $src["width"] ?>"  alt="<?= $alt ?>" title="<?= $title ?>"
                                                         style="margin-top: <?echo $height = ($min_height -15 - $src["height"])/2?>px;<? if ( $src["width"] < 400 ): ?>margin-left: <?echo $width = ($block_width - $src["width"])/2?>px; <? endif ?>">

							                        <div style="position: absolute; bottom: 0px; <? if ( $src["width"] < $block_width ): ?>left: <?echo $width = ($block_width - 60)/2?>px; <? endif ?>">
								                        ���������
							                        </div>
						                        </a>
					                        <? else: ?>
						                        <img src="<?= $src["src"]; ?>" alt="<?= $alt ?>" title="<?= $title ?>" style="margin-top: <?echo $height = ($min_height - $src["height"])/2?>px; <? if ( $src["width"] < $block_width ): ?>margin-left: <?echo $width = ($block_width - $src["width"])/2?>px;<? endif ?>">
					                        <? endif; ?>
				                        </div>
			                        <? endforeach; ?>
                                </div></td>

                           <td>
                              <? if($arResult['COMPANY']['PROPERTIES']['REMOVE_REL']['VALUE_ENUM_ID'] ==false) { ?>
                              <div class="sum_block"><div class="sum_block-inner">

                                <?if (isset($arResult['PROPERTIES']['COST']['VALUE']) && ($arResult['PROPERTIES']['COST']['VALUE']) != 0):?>
                                    <h2 class="sum">
                                        <? $arResult['PROPERTIES']['COST']['VALUE'] = str_replace(' ', '', $arResult['PROPERTIES']['COST']['VALUE']); ?>
                                        <?= number_format($arResult['PROPERTIES']['COST']['VALUE'], 0, ',', ' '); ?>
                                        <span class="currency">
                                            <?=$arResult['PROPERTIES']['CURRENCY']['VALUE']?>
                                            <?if ($arResult['PROPERTIES']['unit_tov']['VALUE']):?>
                                                / <?=$arResult['PROPERTIES']['unit_tov']['VALUE']?>
                                            <?endif;?>
                                        </span>
                                        <span class="currency-is-nds">
                                            <?if ($arResult['PROPERTIES']['COST_NDS']['VALUE'][0] == 'Y'):?>��� ���<?else:?>� ���<?endif;?>
                                        </span>
                                    </h2>
                                <?else:?>
                                    <div style="text-align: center; margin-bottom: 10px">
                                    <img src="/bitrix/templates/.default/images/price-detail.png" alt="���� ���������" width="150" heught="28" />
                                    </div>
                                <?endif;?>

                                 <a href="javascript: void(0);" class="button_v2" style="      width: 195px;
                                     display: inline-block;
                                     padding: 10px 0px 5px 5px; text-align: center"
                                    onclick="javascript:showPopup({id: $('.fast_order_popup'), productID: <?= $arResult["ID"] ?>})">
                                     <img src="/bitrix/templates/.default/images/cart1.png"
                                          height="40"
                                          width="40"
                                          style=" display: inline-table; float:left;
                                                margin: -12px -27px -4px 10px;
                                                vertical-align: middle;">
                                     ������</a>
                              </div></div>
                               <? } // ����� �������� ?>
                              <div class="clear"></div>
                           </td>
                        </tr>
                  </table>

                      <?if(count($arResult["PROPERTIES"]["more_photo"]["SMALL_SRC"])>1):?>
                        <div class="carousel">
                            <ul class="slides" style="height: 72px;">
                                <? foreach($arResult["PROPERTIES"]["more_photo"]["SMALL_SRC"] as $i => $src): ?>
                                    <li><a href="#" data-num="<?= $i+1; ?>" style="height: 71px;">
                                            <span style="height: 64px; display: block;">
                                        <img src="<?= $src['src'];?>"  style="<?if($src["width"] < 88):?>margin-left: <?echo $width = (88 - $src["width"])/2?>px; <?endif?><?if($src["height"] < 64):?>margin-top: <?echo $height = (64 - $src["height"])/2?>px;<?endif?>">
                                            </span>
                                        </a>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                      <?endif;?>

                  <br />

                  <h2>��������� �������� ������</h2><hr/>
                  <div class="formated_text js-fancybox_table">
                      <?
                      $extLinksHandler = new \WS\Handler\ExtLink();
                      $extLinksHandler
                          ->addAllowDomain('energobelarus.by')
                          ->addAllowDomain('energobelarus.ru')
                          ->addAllowDomain('enb.by');
                      $description = new \WS\Text(array(
                          $arResult["DETAIL_TEXT"],
                          $arResult["PREVIEW_TEXT"]
                      ));
                      echo $description->addTextHandler($extLinksHandler)->handleAndGet();
                      ?>
                  </div>
                  <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
               </div>
               <div class="clear"></div>
            </div>

            <div class="mc_block">
               <? if (count($arResult['FILES']) != 0): ?>
                  <div class="mc_head">
                     <h2>��������� �����</h2>
                  </div>

                  <div class="nested-files">
                     <? foreach ($arResult['FILES'] as $key => $file): ?>
                        <? $file_info = CFile::MakeFileArray($file["ID"]) ?>
                        <div class="<?= (($key % 2) === 0) ? 'l_col' : 'r_col' ?>">
                           <div class="nf_block">
                              <p class="nf">
                                  <a target="_blank" href="<?= CFile::GetPath($file["ID"]) ?>">
                                      <i class="<?= $file['TYPE'] ?>-file"></i><?= strlen($file["DESCRIPTION"]) ? $file["DESCRIPTION"] : $file["FILE_NAME"] ?>
                                  </a>
                                  <span>\ <?= (($file_info["size"]) ? substr($file_info["size"], 0, -3) : '0') ?> Kb</span>
                              </p>
                           </div>
                        </div>
                     <? endforeach; ?>
                     <div class="clear"></div>
                  </div>
               <? endif; ?>
            </div>

				<!-- ad C2 -->
            <div class="mc_block">
               <?
               $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
                   "TYPE" => "C2",
                   "NOINDEX" => "N",
                   "CACHE_TYPE" => "A",
                   "CACHE_TIME" => "0",
                   "CACHE_NOTES" => ""
                       )
               );
               ?>
            </div>

            <?
            if (!empty($arResult["BRANDS_IDS"])){ // ���� ��� �������� �� ������ �� ��������
                global $arFilter;
                $arFilter               = array(
                    "SECTION_ID"           => $arParams['SECTION_ID'],
            		"PROPERTY_FIRM"        => $arResult["BRANDS_IDS"],
            		"ACTIVE"               => "Y",
            		//"!PROPERTY_REMOVE_REL" => LANG == 's1' ? 63 : 142
                    //"!PROPERTY_REMOVE_REL" => LANG == 's1' ? 63 : 142
            	);

            $APPLICATION->IncludeComponent(
                    "bitrix:news.list", "similar_prod", Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "services",
                    "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                    "NEWS_COUNT" => "8",
                    "SORT_BY1" => "RAND",
                    "SORT_ORDER1" => "ASC",
                    "FILTER_NAME" => "arFilter",
                    "FIELD_CODE" => array('ID', 'IBLOCK_ID', 'DETAIL_PAGE_URL', 'LIST_PAGE_URL'),
                    "PROPERTY_CODE" => array("ALT"),
                    "PAGER_TEMPLATE" => "similar_prod",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    //"PAGER_TITLE" => "������",
                    "CHECK_DATES" => "Y",
                    //"DETAIL_URL" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                        )
                );
            }
            ?>

            <!--�������� ������-->
         </div>
			<div class="right_col" style="padding: 0; width: 295px;">
                <?// print_r($arResult['COMPANY']['PROPERTIES']['LIQUIDATED']) ?>
                <? if ( $arResult['COMPANY']['PROPERTIES']['LIQUIDATED']['VALUE'] == '��' ) :?>
                <div class="rc_block brd-b5"> <div class="wrap" style="">
                        <div class="c_inf">
                           <table width="100%" cellpadding="0" class="add_table" style="min-height: 75px;">
                              <tr>
                                 <td class="company_name" style="width:240px !important; vertical-align: middle; "><?=$arResult['DISPLAY_PROPERTIES']['FIRM']['DISPLAY_VALUE']?></td>
                                 <td width="70"><?=$arResult['DISPLAY_PROPERTIES']['FIRM']['PREVIEW_PICTURE'];?></td>
                              </tr>
                           </table>
                        </div>
                        <div class="LIQUIDATED_red"></div>
                    </div> </div>
				<? elseif($arResult['COMPANY']['PROPERTIES']['REMOVE_REL']['VALUE_ENUM_ID'] ==false):?>
                  <div class="rc_block brd-b5">
                  <div class="wrap">
                     <h3 style="padding-left: 0px">���������� ����������</h3>
                     <div class="c_inf">
                        <table width="100%" cellpadding="0" class="add_table" style="min-height: 75px;">
                           <tr>
                              <td class="company_name" style="width:240px !important; vertical-align: middle; "><?=$arResult['DISPLAY_PROPERTIES']['FIRM']['DISPLAY_VALUE']?></td>
                              <td width="70"><?=$arResult['DISPLAY_PROPERTIES']['FIRM']['PREVIEW_PICTURE'];?></td>
                           </tr>
                        </table>
                        <div class="contact_block">
                           <? if (is_array($arResult["COMPANY"]["PROPERTIES"]["phone"]["VALUE"]) && count($arResult["COMPANY"]["PROPERTIES"]["phone"]["VALUE"])): ?>
                              <div class="ph_block">
                                 <i class="phone"></i>
                                 <div class="js-phone-hide-block" data-company="<?=$arResult["COMPANY"]["ID"]?>" style="margin-left:0px;">
                                    <? foreach ($arResult["COMPANY"]["PROPERTIES"]["phone"]["VALUE"] as $key => $phone): ?>
                                       <div>
                                          <span class="lgrey phone-code">+375 </span>
	                                       <!--googleoff: all-->
	                                       <noindex>
                                            <span class="phone-hide js-phone-hide">�������� �������</span>
	                                       </noindex>
	                                       <!--googleon: all-->
                                          <span class="phone-show"><b><?= phone($arResult["COMPANY"]["PROPERTIES"]["phone"]["VALUE"][$key]) ?></b></span>
                                       </div>
                                    <? endforeach; ?>
                                 </div>
                              </div>
                           <? endif ?>

                           <? if (strlen($arResult["COMPANY"]["PROPERTIES"]["shedule"]["VALUE"]) > 0): ?>
                              <div class="ph_block"><i class="time"></i>
                                 <div>&nbsp;<?= $arResult["COMPANY"]["PROPERTIES"]["shedule"]["VALUE"] ?></div>
                              </div>
                           <? endif; ?>

                           <div class="ph_block">
                              <i class="address"></i><div><?= implode(", ", $arResult["ADRESS"]) ?></div>
                           </div>

                           <? if (strlen($arResult["COMPANY"]["PROPERTIES"]["adress_store"]["VALUE"])): ?>
                              <div class="ph_block">
                                 <i class="address"></i>
											<div><span class="lgrey">����� ������:</span> <?= strip_tags($arResult["COMPANY"]["PROPERTIES"]["adress_store"]["VALUE"]) ?></div>
										</div>
									<? endif ?>
								</div>

                        <div class="clear"></div>

                        <? if (count($arResult["COMPANY_INFO"]["URL"]) > 0): ?>
                           <? foreach ($arResult["COMPANY_INFO"]["URL"] as $arSites): ?>
                              <div class="site_info">
                                 <span class="lgrey">Web-����(�):</span><a rel='nofollow' href="<?= $arSites['LINK'] ?>"><?= $arSites['NAME'] ?></a>
                              </div>
                           <? endforeach; ?>
                        <? endif; ?>
                     </div>

                     <?
                     $coord = explode(",", $arResult['COMPANY']['PROPERTIES']['MAP']['VALUE']);

                     if (count($coord) > 1):
                        ?>
                        <div class="bx-yandex-view-layout">
                           <div class="bx-yandex-view-map">

                              <div id="BX_YMAP_MAP_HMQpL5SEAD" class="bx-yandex-map" style="height: 200px; width: 270px;">

                                 <?
                                 // ��������� ������ ��� �����
                                 $arData = Array("yandex_lat" => $coord[0], "yandex_lon" => $coord[1], "yandex_scale" => 16, "PLACEMARKS" => Array(Array("LON" => $coord[1], "LAT" => $coord[0], "TEXT" => $arResult['DISPLAY_PROPERTIES']['FIRM']['DISPLAY_VALUE'])));
                                 ?>

                                 <?
                                 $APPLICATION->IncludeComponent("bitrix:map.yandex.view", ".default", array(
                                     "KEY" => "AETqvU8BAAAApI_7dQMAlTDGwZ3hOJGhdVSFYEsVNSpuzTQAAAAAAAAAAACCDIEExtPcFpiKYkYYJlaNR8uKKw==",
                                     "INIT_MAP_TYPE" => "MAP",
                                     "MAP_DATA" => serialize($arData),
                                     "MAP_WIDTH" => 250,
                                     "MAP_HEIGHT" => 200,
                                     "CONTROLS" => array(),
                                     "OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DRAGGING",),
                                     "MAP_ID" => ""
                                         ), false
                                 );
                                 ?>
                              </div>

                           </div>

                        </div>

                     <? endif ?>

                     <br/>
                     <br/>

                     <? if ($arResult["USER_ID"] > 0): ?>
                        <a class="send_but" href="/personal/messages/<?= $arResult['USER_ID'] ?>/?product=<?= $arResult['ID'] ?>" >��������� � ���������</a><br/>
                     <? else: ?>
                        <? if ($USER->IsAuthorized() && false): ?>
                           <div class="js-make-predstavitel make-predstavitel">
                              <? if (isset($arResult["PREDSTAVITEL_STATUS"])): ?>
                                 <? if ($arResult["PREDSTAVITEL_STATUS"] == 64)://���������������?>
                                    ���� ������ �� ������������
                                 <? elseif ($arResult["PREDSTAVITEL_STATUS"] == 66)://���������������?>
                                    ���� ������ ���������
                                 <? endif; ?>
                              <? else: ?>
                                 <a data-company-id="<?= $arResult["COMPANY"]["ID"] ?>" href="javascript:void(0);">����� ��������������</a>
                              <? endif; ?>
                           </div>
                        <? endif; ?>
                     <? endif; ?>

                     <!--
                     <a href="#" class="send_but" onclick="alert('���������� ��������������'); return false;">��������� � ���������</a><br/>
                     -->
                     <span class="report-e">�� �������� ��������  ����������, ��� ����� ��� �� 	<?= $arParams['SITE_NAME']?></span>
                     <br>

                  </div>
               </div><!--rc_block-->
                <? else: ?> <!--  -->
                    <div class="rc_block" style="overflow: hidden;">
                      <div class="form_send_req">
                          <div class="f_s_q_title">��� ������?</div>
                          <div class="f_s_q_text">
                              ��������� �������� � ���� � ���������<br/> ����� � ����������� ��� �����������<br/>
                              ���������� �� ����, ������� ������<br/> � ���������� ������.
                          </div>
                          <div class="s_s_form2">

                              <div class="button_block" style="margin: 20px auto;">
                                  <a href="#" class="button_v2"
                                     onclick="javascript:showPopup({id: $('.fast_order_popup'), productID: <?= $arResult["ID"] ?>})"
                                     style="font-size: 20px; padding: 10px 10px; ">��������� �� ����</a>
                              </div>

                              <div class="f_f_q_text">
                                  �� ����� ������ ����� �� <?= $arParams['SITE_NAME'] ?><br/><br/>

                                  ������ ������ �����������:<br/>
                                  <? if ( $arParams['SITE_NAME'] == 'EnergoBelarus.by' ): ?>
                                      +375 (17) 290-22-25,<br/>
                                      +375 (17) 380-49-26&#160;
                                  <?else:?>
                                      +375 (17) 380-49-26,<br/>
                                      +375 (17) 290-22-25&#160;
                                  <?endif?>

                              </div>
                          </div>
                      </div>
                    </div>

	                <!-- Yandex.Metrika counter -->
	                <script type="text/javascript">
	                    (function (d, w, c) {
	                        (w[c] = w[c] || []).push(function() {
	                            try {
	                                w.yaCounter32687100 = new Ya.Metrika({
	                                    id:32687100,
	                                    clickmap:true,
	                                    trackLinks:true,
	                                    accurateTrackBounce:true
	                                });
	                            } catch(e) { }
	                        });

	                        var n = d.getElementsByTagName("script")[0],
	                            s = d.createElement("script"),
	                            f = function () { n.parentNode.insertBefore(s, n); };
	                        s.type = "text/javascript";
	                        s.async = true;
	                        s.src = "https://mc.yandex.ru/metrika/watch.js";

	                        if (w.opera == "[object Opera]") {
	                            d.addEventListener("DOMContentLoaded", f, false);
	                        } else { f(); }
	                    })(document, window, "yandex_metrika_callbacks");
	                </script>
	                <noscript><div><img src="https://mc.yandex.ru/watch/32687100" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	                <!-- /Yandex.Metrika counter -->
	                <script type="application/javascript">
		                $('#shoe_form_action').bind('click', function(){
			                yaCounter32687100.reachGoal('Show_Form')
		                });
		                $('#shoe_form_send').bind('click', function(){
			                yaCounter32687100.reachGoal('TARGET_NAME')
		                });
	                </script>

				<?endif;?>
            <!-- ad C1-1 -->
				<div class="rc_block" style="overflow: hidden;">
					<?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
						"TYPE" => "C1_1",
						"NOINDEX" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "0",
						"CACHE_NOTES" => ""
						)
					);?>
				</div>
				<!-- ad C1-2 -->
				<div class="rc_block" style="overflow: hidden;">
					<?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
						"TYPE" => "C1_2",
						"NOINDEX" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "0",
						"CACHE_NOTES" => ""
						)
					);?>
				</div>
				<!-- ad C1-3 -->
				<div class="rc_block" style="overflow: hidden;">
					<?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
						"TYPE" => "C1_3",
						"NOINDEX" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "0",
						"CACHE_NOTES" => ""
						)
					);?>
				</div>
				<!-- ad C1-4 -->
				<div class="rc_block" style="overflow: hidden;">
					<?$APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", Array(
						"TYPE" => "C1_4",
						"NOINDEX" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "0",
						"CACHE_NOTES" => ""
						)
					);?>
				</div>
<!-- ������.������ -->
<div id="yandex_ad"></div>
<script type="text/javascript">
(function(w, d, n, s, t) {
    w[n] = w[n] || [];
    w[n].push(function() {
Ya.Direct.insertInto(window.location.hostname=='agrobelarus.by'?135492:135481, "yandex_ad", {
            ad_format: "direct",
            type: "adaptive",
            border_type: "block",
            limit: 3,
            title_font_size: 3,
            border_radius: true,
            links_underline: true,
            site_bg_color: "FFFFFF",
            border_color: "CCCCCC",
            title_color: "266BAB",
            url_color: "000099",
            text_color: "000000",
            hover_color: "0B9DF1",
            sitelinks_color: "0000CC",
            favicon: true,
            no_sitelinks: false,
            height: 500,
            width: 295
        });
    });
    t = d.getElementsByTagName("script")[0];
    s = d.createElement("script");
    s.src = "//an.yandex.ru/system/context.js";
    s.type = "text/javascript";
    s.async = true;
    t.parentNode.insertBefore(s, t);
})(window, document, "yandex_context_callbacks");
</script>
			</div>

      </div>
      <div class="clear"></div>
      <br/>


      <? if (strlen($arResult["STAT_CODE"]) > 0): // ������� ������   ?>
         <? /* --<script type="text/javascript">
									/* Nnv = navigator;
                           Nna = Nnv.appName;
                           Nd = document;
                           Nd.cookie = "b=b";
                           Nc = 0;
                           if (Nd.cookie)
                              Nc = 1;
                           Nn = (Nna.substring(0, 2) == "Mi") ? 0 : 1;
                           Ns = screen;
                           Npx = (Nn == 0) ? Ns.colorDepth : Ns.pixelDepth;
                           str = '<img src="http://c.energobelarus.by/<?= $arResult["STAT_CODE"] ?>;' + Ns.width + 'x' + Ns.height + ';' + Npx + ';' + Nc + ';';
                           str = str + escape(Nd.referrer) + ';' + Math.random() + '" width="88" height="31" border="0">';
                           document.write('<a href="http://www.energobelarus.by/" alt="Energobelarus" target="_blank">' + str + '</a>'); *
         </script>-->
         <!--<noscript>
         <a href="http://www.energobelarus.by/" target="_blank"><img src="http://c.energobelarus.by/<?= $arResult["STAT_CODE"] ?>;0x0;0;0;-;0" alt="Energobelarus" width="88" height="31" border="0"></a>
         </noscript>--> */?>
      <? endif; ?>
   </div>   <!--end content-->
<?
if ($arResult["COMPANY"]["PROPERTIES"]["METRIKA_ID"]["VALUE"])
   YandexMetrika::setCounter($arResult["COMPANY"]["PROPERTIES"]["METRIKA_ID"]["VALUE"]);
?>
<div class="clear"></div>
</div>

<div id="_al_over2" class="_over">
    <div class="_win">
        <a class="_close_win" href="#" onclick="$('#_al_over2').hide(); return false"></a>
        <img  class="_logo_img" src="/images/_pop_logo.png" height="79" />
        <div class="_title">
            �����������
        </div>
        <div>

            <p id="data_text" style="margin-bottom: 10px; font-size: 14px; ;"></p>
            <noindex>

                <a rel="nofollow" href="#" onclick="$('#_al_over2').hide(); return false;" style="float: right;">�������</a>
            </noindex>
        </div>
    </div>
</div>
