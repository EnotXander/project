<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if ($arResult["SECTIONS"]):?>
	<div class="submarket-ul"><div class="submarket-bg">
		<ul class="submarket-col">
			<?$sectInCol = ceil(count($arResult["SECTIONS"])/3);?>
			<?$counter = 0;?>
			<?foreach ($arResult["SECTIONS"] as $arSection):?>
				<?if ($arSection["ELEMENT_CNT"] == 0):?>
					<!-- <?=$arSection['NAME']?> -->
					<?continue;?>
				<?endif;?>

				<?$counter++;?>
				<?$isCutClass = $arSection['my_ISCUT'] ? ' iscut' : '';?>
				<?$isCurrentClass = ($arSection['ID'] == $arParams['CURRENT_SECTION']) ? ' current' : '';?>
				<?if ($counter != 1 && (($counter-1) % $sectInCol) == 0):?>
					</ul>
				    <ul class="submarket-col">
				<?endif;?>
				<li>
					<a href="<?= $arSection["SECTION_PAGE_URL"] ?>" class="<?=$isCutClass?><?=$isCurrentClass?>"><?=$arSection["NAME"]?></a>
					<?if ($arParams["COUNT_ELEMENTS"]):?><sup><?=$arSection["ELEMENT_CNT"]?></sup><?endif;?>
				</li>
			<?endforeach;?>
		</ul>
	</div></div>
<?endif;?>