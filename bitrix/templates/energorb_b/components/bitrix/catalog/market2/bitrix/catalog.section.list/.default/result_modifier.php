<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//������� ���, ����� ���� ������ ��� ������� �� ����� script.js, ������� ����� ��� ��������
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.localScroll.min.js');

function CountEmSectionInIblock($ID)
{
    global $DB;

    $arResult['SECTION_COUNT'] = array();

    $strSql = "
       SELECT DISTINCT
           BS.ID AS ID,
           BS.IBLOCK_ID AS IBLOCK_ID,
           COUNT(DISTINCT BE.ID) AS ELEMENT_CNT
       FROM
           b_iblock_section BS
               INNER JOIN
           b_iblock B ON BS.IBLOCK_ID = B.ID
               INNER JOIN
           b_iblock_section BSTEMP ON BSTEMP.IBLOCK_ID = BS.IBLOCK_ID
               LEFT JOIN
           b_iblock_section_element BSE ON BSE.IBLOCK_SECTION_ID = BSTEMP.ID
               LEFT JOIN
           b_iblock_element BE ON (BSE.IBLOCK_ELEMENT_ID = BE.ID
               AND ((BE.WF_STATUS_ID = 1
               AND BE.WF_PARENT_ELEMENT_ID IS NULL)
               AND BE.IBLOCK_ID = BS.IBLOCK_ID)
               AND BE.ACTIVE = 'Y'
               AND (BE.ACTIVE_TO >= NOW()
               OR BE.ACTIVE_TO IS NULL)
               AND (BE.ACTIVE_FROM <= NOW()
               OR BE.ACTIVE_FROM IS NULL))
       WHERE
           1 = 1
               AND BSTEMP.IBLOCK_ID = BS.IBLOCK_ID
               AND BSTEMP.LEFT_MARGIN >= BS.LEFT_MARGIN
               AND BSTEMP.RIGHT_MARGIN <= BS.RIGHT_MARGIN
               AND ((((BS.IBLOCK_ID = '$ID'))))
               AND ((((B.ID = '$ID'))))
       GROUP BY BS.ID

        ";
    $err_mess ='';
    $res = $DB->Query($strSql, false, $err_mess.__LINE__);

    while ($item = $res->Fetch()){
        $arResult['SECTION_COUNT'][$item['ID']] = $item['ELEMENT_CNT'];
    }


    return $arResult['SECTION_COUNT'];
}

$arResult['SECTION_COUNT'] = CountEmSectionInIblock($arParams["IBLOCK_ID"]);

$obParser = new CTextParser;

#####������ �������� �������� ������� ������#####
foreach ($arResult['SECTIONS'] as $key => $arItem)
{
   $arResult['SECTIONS'][$key]["NAME"] = $obParser->html_cut($arItem["NAME"], 50);
  
   $arResult['SECTIONS'][$key]['PICTURE'] = CFile::ResizeImageGet(
                                                         $arItem["PICTURE"],
                                                         array("width" => 74, "height" => 76),
                                                         BX_RESIZE_IMAGE_PROPORTIONAL,
                                                         true
                                                          );

   $arResult['SECTIONS'][$key]['SMALL_PICTURE'] = CFile::ResizeImageGet(
                                                         $arItem["PICTURE"],
                                                         array("width" => 38, "height" => 35),
                                                         BX_RESIZE_IMAGE_PROPORTIONAL,
                                                         true);
    $arResult['SECTIONS'][$key]["ELEMENT_CNT"] = $arResult['SECTION_COUNT'][$arItem['ID']];
}
###������ �������� �������� ������� ������###
  
##### �������� ����������� ��������� #####
//������ ����� ArResult//
foreach($arResult['SECTIONS'] as $key => $arSection)
{
//   $arSection['ELEMENT_CNT'] = '&nbsp;';

   if($arSection['ELEMENT_CNT'])
   {
     $arTemp[$arSection['ID']]=$arSection;
   }
   else
   {   
      unset($arTemp[$arSection['ID']]);
   }
}

//�������� ����������� �������� ������ �� ������//
foreach($arTemp as $key1 => $arSection)
{
   if ($arSection['DEPTH_LEVEL']==3)
   {
      $arTemp[$arSection['IBLOCK_SECTION_ID']]['SUB-3'][]=$arSection;
      unset($arTemp[$key1]);
   }
}

//�������� ����������� ������� ������ � ������//
foreach($arTemp as $key2 => $arSection)
{
   if ($arSection['DEPTH_LEVEL']==2)
   {
      $arTemp[$arSection['IBLOCK_SECTION_ID']]['SUB-2'][]=$arSection;
      unset($arTemp[$key2]);
   }
}
$arResult=$arTemp;
### �������� ����������� ��������� ###

##### ��������� ������� �������� 2-�� ������ �� ���������� �� 3 ��������#####
foreach ($arResult as $key => $arSection)
{
   $countSub=0;
   foreach($arSection['SUB-2'] as $key2=>$arSubSect):
      if($arSubSect['ELEMENT_CNT'])
      {
         $countSub++;
      }
      else
      {
         unset($arResult[$key]["SUB-2"][$key2]["SUB-3"]);
      }
   endforeach;
   
   
   $arResult[$key]["SUBSECT"]=array_chunk($arResult[$key]["SUB-2"],ceil($countSub/3),TRUE);

}
### ��������� ������� �������� 3-�� ������ �� ���������� �� 3 ��������###
