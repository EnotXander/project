<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$obParser = new CTextParser;
foreach ($arResult['SECTIONS'] as $key => $arItem){
	//фильтр пустых разделов
	if ($arItem["ELEMENT_CNT"] == 0){
		unset($arResult['SECTIONS'][$key]);
		continue;
	}
	
	//обрезка текста заголовков
	if (strlen($arItem['NAME']) > $arParams['NAME_CUT']){
		$arItem["NAME"] = $obParser->html_cut($arItem["NAME"], $arParams['NAME_CUT']);
		$arItem["NAME"] = trim($arItem["NAME"], ' .');
		$arItem['my_ISCUT'] = '1';
		$arResult['SECTIONS'][$key] = $arItem;
	}
}
