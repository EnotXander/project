<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if ($_REQUEST['action'] == 'ADD_TO_COMPARE_LIST')
   $_SESSION['FAVOR' . $arParams['IBLOCK_ID']]['ITEMS'][] = (int)$_REQUEST['id'];?>

<div class="left_col">

   <? if (count($arResult["BRANDS"]) > 0): ?>
      <div class="lc_block">
         <div class="title">�������� � �������</div>
         <ul class="left_menu">
            <?$i = 0;
            foreach ($arResult["BRANDS"] as $arBrand):?>
               <? if ($i < 50): ?>
                  <? if ($arBrand["DETAIL_PAGE_URL"] != NULL): ?>
                     <li><a href="<?= $arBrand["DETAIL_PAGE_URL"] ?>"><?= $arBrand["NAME"] ?></a></li>
                  <? endif ?>
               <? endif ?>
               <?$i++;
            endforeach?>
         </ul>
      </div>
   <? endif; ?>
   <div class="lc_block">
      <div class="left_banner_185">
         <?
         $APPLICATION->IncludeComponent("bitrix:advertising.banner", "", array(
             "TYPE" => "MARKET",
             "NOINDEX" => "N",
             "CACHE_TYPE" => "A",
             "CACHE_TIME" => "0",
             "CACHE_NOTES" => ""
                 )
         );
         ?>
      </div>
   </div>
</div>

<div class="middle_col">
   <?if(0)://(strlen($arResult["DESCRIPTION"])):?>
      <div class="mc_block"><?=$arResult["DESCRIPTION"]?></div>
   <?endif;?>
   <div class="mc_block">
      <ul class="mb_menu">
         <li<? if ($_REQUEST['FAVOR'] <> 'Y'): ?> class="sel"<? endif; ?>>
            <a href="?FAVOR=N">� ���������</a>
            <div class="sclew"></div>
         </li>
      </ul>
      <div class="clear"></div>
      <div class="paging brdr_bot_0 brdr_box">
         <div class="show_col">
            <form method="get" name="top_f">
               <input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">
               <span class="meta">���������� ��:</span>
               <select name="per_page" onchange="document.top_f.submit()">
                  <option value="20" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 20) echo "selected"; ?>>20</option>
                  <option value="50" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 50) echo "selected"; ?>>50</option>
                  <option value="100" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 100) echo "selected"; ?>>100</option>
               </select>
            </form>
         </div>
         <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
            <?= $arResult["NAV_STRING"] ?>
         <? endif; ?>
      </div><!--paging-->

      <?if(strlen($arResult["DESCRIPTION"])):?>
         <div class="ads_row_wrapblock">
            <div class="ads_row_infoblock"><?=strip_tags($arResult["DESCRIPTION"], "<b><i><a><strong>")?></div>
            <div class="ads_row_rowsblock">
      <?endif;?>
         <? foreach ($arResult["ITEMS"] as $arItem): ?>      
            <div class="ads_row premium">
               <table>
                  <tbody>
                     <tr>
                        <td class="ic_star" style="vertical-align:top;">
                           <?if($USER->IsAuthorized()):?>
                              <a title="<?=(in_array($USER->GetID(), $arItem["PROPERTIES"]["FAVORITES"]["VALUE"]))? '������� �� ����������' : '�������� � ���������'?>" href="javascript:void(0)" onclick="AddToFavorites({productId: <?=$arItem['ID']?>, context: $(this).find('.icon_star'), inFavorites: function(){this.addClass('star_sel').closest('a').attr('title', '������� �� ����������')}, outFavorites: function(){this.removeClass('star_sel').closest('a').attr('title', '�������� � ���������')}})">
                                 <i class="icons icon_star<? if(in_array($USER->GetID(), $arItem["PROPERTIES"]["FAVORITES"]["VALUE"])):?> star_sel<? endif; ?>"></i>
                              </a>
                           <?endif;?>
                        </td>
                        <td class="image_box">
                           <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                              <a title="���������� ���������" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                 <img alt="" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                              </a>
                           <? else: ?>
                              <img src="<?= SITE_TEMPLATE_PATH ?>/images/withoutphoto.jpg" width="72" height="72" align=""/>  
                           <? endif; ?>
                        </td>
                        <td class="ads_info">
                           <div class="tdWrap">
                              <div class="ads_title"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></div>
                              <div class="ads_text">���: <? echo $arItem['DISPLAY_PROPERTIES']['NUMBER']['DISPLAY_VALUE'] ?></div>
                              <div class="meta"><?= $arItem['PREVIEW_TEXT'] ?></div>
                           </div>                      
                        </td>
                        <td class="ads_price">
                           <?if($arItem['DISPLAY_PROPERTIES']['COST']['DISPLAY_VALUE']):?>
                              <b><?= number_format($arResult['DISPLAY_PROPERTIES']['COST']['VALUE'], 0, ',', ' '); ?>
                                 <?=strlen($arItem['PROPERTIES']['CURRENCY']['VALUE']) ? " ".$arItem['PROPERTIES']['CURRENCY']['VALUE'] : ""?>
                              </b>
                           <?endif;?>
                        </td>
                        <td class="ads_nav">
                           <? if (in_array($arItem['PROPERTIES']['FIRM']['VALUE'], $arResult["BRANDS_IDS"])): ?>
                              <?=$arResult["BRANDS"][$arItem['PROPERTIES']['FIRM']['VALUE']]["PREVIEW_PICTURE_FORMATED"];?>
                              <br/>
                              <?= $arItem['DISPLAY_PROPERTIES']['FIRM']['DISPLAY_VALUE'] ?>
                           <? endif; ?>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div><!--ads_row-->
         <? endforeach; ?>
      <?if(strlen($arResult["DESCRIPTION"])):?>
            </div>
         </div>
      <?endif;?>

      <div class="paging brdr_top_0 brdr_box mar_20_bot">
         <div class="show_col">
            <form method="get" name="bot_f">
               <input type="hidden" name="FIRM" value="<?= (int)$_REQUEST['FIRM'] ?>">
               <span class="meta">���������� ��:</span>
               <select name="per_page" onchange="document.bot_f.submit()">
                  <option value="20" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 20) echo "selected"; ?>>20</option>
                  <option value="50" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 50) echo "selected"; ?>>50</option>
                  <option value="100" <? if ($arParams['PAGE_ELEMENT_COUNT'] == 100) echo "selected"; ?>>100</option>
               </select>
            </form>
         </div>
         <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <?= $arResult["NAV_STRING"] ?>
         <? endif; ?>
      </div>
   </div>
</div>