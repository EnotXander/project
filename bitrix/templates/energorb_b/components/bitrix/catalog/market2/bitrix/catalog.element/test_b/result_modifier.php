<?if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
}
$objElement = new CIBlockElement();

if ( is_array( $arResult["PREVIEW_PICTURE"] ) ) {
	$arFileTmp = CFile::ResizeImageGet(
		$arResult["PREVIEW_PICTURE"],
		array( "width" => 302, "height" => 200 ),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true
	);
	//$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

	$arResult["PREVIEW_PICTURE"] = array(
		"SRC"    => $arFileTmp["src"],
		"WIDTH"  => $arFileTmp["width"],//IntVal($arSize[0]),
		"HEIGHT" => $arFileTmp["height"]//IntVal($arSize[1]),
	);
}
// ������ ���������� � ������� �� ����������  ��� ���������� ��������
/* zvv */

$arResult["PROPERTIES"]["more_photo"]["VALUE"]    = array_merge(array($arResult["~PREVIEW_PICTURE"]),$arResult["MORE_PHOTO"]);

$max_height = 100;
foreach ( $arResult["PROPERTIES"]["more_photo"]["VALUE"] as $i => $imgId ) {
   if (is_array($imgId))
	$arResult["PROPERTIES"]["more_photo"]["ORIGINAL_SRC"][] = $imgId ;
	   else
    $arResult["PROPERTIES"]["more_photo"]["ORIGINAL_SRC"][] = CFile::GetFileArray( $imgId );

	if ( $imgId > 0 ) {
		$arImg                                                  = CFile::ResizeImageGet( $imgId, array(
			'width'  => 400,
			'height' => 280
		), BX_RESIZE_IMAGE_PROPORTIONAL, true );
		$arResult["PROPERTIES"]["more_photo"]["BIG_SRC"][]      = $arImg;
		if ($max_height < $arImg['height']) {
			$max_height = $arImg['height'];
		}
		$arImg                                                  = CFile::ResizeImageGet( $imgId, array(
			'width'  => 88,
			'height' => 64
		), BX_RESIZE_IMAGE_PROPORTIONAL, true );
		$arResult["PROPERTIES"]["more_photo"]["SMALL_SRC"][]    = $arImg;
	}
}
/*  zvv  */
$arResult["MAX_H"] = $max_height;

$arResult['PREMIUM'] = $arPremium;

$arResult["FILES"] = array();
if ( is_array( $arResult["PROPERTIES"]["FILES"]["VALUE"] ) && count( $arResult["PROPERTIES"]["FILES"]["VALUE"] ) > 0 ) {
	foreach ( $arResult["PROPERTIES"]["FILES"]["VALUE"] as $file ) {
		$iconedTypes = array( 'doc', 'xls', 'ppt', 'jpg', 'djvu', 'docs', 'xslt', 'pdf', 'jpeg' );

		$file                = CFile::GetFileArray( $file );
		$extension           = GetExtension( $file['FILE_NAME'] );
		$file["TYPE"]        = in_array( $extension, $iconedTypes ) ? $extension : 'file';
		$file["FORMAT_SIZE"] = FormatBytes( $file['FILE_SIZE'] );
		$file["CLASS"]       = GetFileImageClass( $file["TYPE"] );
		$arResult["FILES"][] = $file;
	}
}

$arResult["STAT_CODE"] = '';
if ( strlen( $arResult['PROPERTIES']['FIRM']['VALUE'] ) > 0 ) {
	$res = $objElement->GetByID( $arResult['PROPERTIES']['FIRM']['VALUE'] );
	if ( $obj = $res->GetNextElement() ) {
		$tempRelation = $obj->GetProperty( "REMOVE_REL" );
		$temp         = $obj->GetProperty( "STAT_CODE" );
		if ( ( strlen( $temp["VALUE"] ) > 0 ) && ! $tempRelation["VALUE"] ) {
			$arResult["STAT_CODE"] = $temp["VALUE"];
		}
	}
}


$arResult["BRANDS_IDS"] = $arParams['brands_ar']["BRANDS_IDS"] ;
$arResult["BRANDS"] = $arParams['brands_ar']["BRANDS"] ;
//unset($brands_ar);

$filter  = Array(
	"UF_COMPANY" => $arResult['DISPLAY_PROPERTIES']['FIRM']['VALUE'],
	"GROUPS_ID"  => Array( 12 )
);
$rsUsers = CUser::GetList( ( $by = "id" ), ( $order = "desc" ), $filter ); // �������� �������������
if ( $arUser = $rsUsers->Fetch() ) {
	$arResult["USER_ID"] = $arUser['ID'];
}


//�������� ���������� � ������������� ��������
if ( ENABLE_PREDSTAVITEL_MODE ) {
	$arPredstavitel = PredstavitelGetByCompany( $arResult['DISPLAY_PROPERTIES']['FIRM']['VALUE'] );
	foreach ( $arPredstavitel["LIST"] as $key => $arEmployee ) {
		if ( $arEmployee["STATUS"] == 65 )//�������
		{
			$arResult["USER_ID"] = $arEmployee["USER"];
		}
		if ( $arEmployee["USER"] == $USER->GetID() ) {
			$arResult["PREDSTAVITEL_STATUS"] = $arEmployee["STATUS"];
		}
	}
}

$arResult["COMPANY"]                                       = GetIBlockElement( $arResult['DISPLAY_PROPERTIES']['FIRM']['VALUE'] );
$arResult['DISPLAY_PROPERTIES']['FIRM']['PREVIEW_PICTURE'] = CFile::ShowImage( $arResult["COMPANY"]['PREVIEW_PICTURE'], 70, 70 );

$arAdress           = Array( "index", "COUNTRY", "REGION", "CITY", "adress" );
$arResult["ADRESS"] = array();
foreach ( $arAdress as $pid ) {
	$arVal = CIBlockFormatProperties::GetDisplayValue( $arResult["COMPANY"], $arResult["COMPANY"]["PROPERTIES"][ $pid ], "news_out" );
	if ( strlen( $arVal["DISPLAY_VALUE"] ) ) {
		$arResult["ADRESS"][] = strip_tags( $arVal["DISPLAY_VALUE"] );
	}
}


foreach ( $arResult["ITEMS"] as $key => $arItem ) {
	$userId  = 0;
	$filter  = array(
		"UF_COMPANY" => $arItem['PROPERTIES']['FIRM']['VALUE'],
		"GROUPS_ID"  => array( 12 )
	);
	$rsUsers = CUser::GetList( ( $by = "id" ), ( $order = "desc" ), $filter ); // �������� �������������
	if ( $arUser = $rsUsers->Fetch() ) {
		$arResult["ITEMS"][ $key ]["USER_ID"] = $arUser['ID'];
	}
	if ( ENABLE_PREDSTAVITEL_MODE ) {
		$arPredstavitel = PredstavitelGetByCompany( $arItem['PROPERTIES']['FIRM']['VALUE'] );
		foreach ( $arPredstavitel["LIST"] as $key => $arEmployee ) {
			if ( $arEmployee["STATUS"] == 65 )//�������
			{
				$arResult["ITEMS"][ $key ]["USER_ID"] = $arEmployee["USER"];
				break;
			}
		}
	}
	$rsUser = CUser::GetByID( $arItem['PROPERTIES']['AUTHOR']['VALUE'] );
	$arUser = $rsUser->Fetch();

	$arResult["ITEMS"][ $key ]["COMPANY"]                             = GetIblockElement( $arItem['PROPERTIES']['FIRM']['VALUE'] );
	$arResult["ITEMS"][ $key ]["COMPANY"]['PREVIEW_PICTURE_FORMATED'] = CFile::ShowImage( $arResult["ITEMS"][ $key ]["COMPANY"]['PREVIEW_PICTURE'], 80, 50 );
}

$arResult["COMPANY_INFO"]["URL"] = array();
if ( is_array( $arResult["COMPANY"]["PROPERTIES"]["URL"]["VALUE"] ) && count( $arResult["COMPANY"]["PROPERTIES"]["URL"]["VALUE"] ) > 0 ) {
	foreach ( $arResult["COMPANY"]["PROPERTIES"]["URL"]["VALUE"] as $key => $val ) {
		$val = trim( str_replace( ",", "", $val ) );

		if ( ( substr_count( $val, "http://" ) > 0 ) || ( substr_count( $val, "https://" ) > 0 ) ) {
			$arResult["COMPANY_INFO"]["URL"][] = array(
				"LINK" => $val,
				"NAME" => $val
			);
		} else {
			$arResult["COMPANY_INFO"]["URL"][] = array(
				"LINK" => "http://" . $val,
				"NAME" => $val
			);
		}
	}
}

//������������ ���������� ��������� � �������//
global $quantity;
global $arFilter;
$arFilter = array(
	"IBLOCK_ID"      => IBLOCK_PRODUCTS,
	"ACTIVE_DATE"    => "Y",
	"ACTIVE"         => "Y",
	"SECTION_ID"     => $arResult["IBLOCK_SECTION_ID"],
	"!PROPERTY_FIRM" => $arResult["PROPERTIES"]["FIRM"]["VALUE"]
);

$quantity = CIBlockElement::GetList( Array(), $arFilter, array() );


//��������� ����������� ������ ��������//
foreach ( $arResult["COMPANY"]["PROPERTIES"]["phone"]["VALUE"] as $key => $phone ):
	if ( strpos( $phone, ")" ) ):
		$pieces                                                           = explode( ")", $phone );
		$arResult["COMPANY"]["PROPERTIES"]["phone"]["PHONE_CODE"][ $key ] = $pieces[0] . ")";
	else:
		$pieces                                                           = explode( " ", $phone );
		$arResult["COMPANY"]["PROPERTIES"]["phone"]["PHONE_CODE"][ $key ] = $pieces[0] . " ";
	endif;
	$arResult["COMPANY"]["PROPERTIES"]["phone"]["NUMBER"][ $key ] = $pieces[1];
endforeach;

