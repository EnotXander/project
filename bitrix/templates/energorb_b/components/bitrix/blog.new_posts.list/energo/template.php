<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!$this->__component->__parent || empty($this->__component->__parent->__name) || $this->__component->__parent->__name != "bitrix:blog"):
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/style.css');
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/themes/blue/style.css');
endif;
?>
<?
if(count($arResult["POSTS"])>0)
{
	foreach($arResult["POSTS"] as $ind => $CurPost)
	{
		$arBlog = CBlog::GetByOwnerID($CurPost['BLOG_OWNER_ID']);
		$arUser = CBlogUser::GetByID($CurPost["AUTHOR_ID"], BLOG_BY_USER_ID);
?>	
<div class="faces_row">
		<div class="image_box"><?echo CFile::ShowImage($arUser['AVATAR'], 50, 50);?></div>
			<div class="name"><?echo $CurPost["~AUTHOR_LAST_NAME"]." ".$CurPost["~AUTHOR_NAME"]?></div>
			<div class="post"><?echo $arBlog['NAME']?></div>
			<div class="company"><span class="blog-post-date-formated"><?=$CurPost["DATE_PUBLISH_FORMATED"]?></span>&nbsp;&nbsp;<a href="<?=$CurPost["urlToPost"]?>" title="<?=$CurPost["TITLE"]?>"><?=$CurPost["TITLE"]?></a></div>
	</div>
		<?
	}
}
?>	
