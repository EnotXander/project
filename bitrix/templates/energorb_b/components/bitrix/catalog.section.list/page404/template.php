<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="catalog-404">
	<?$countSections = floor(count($arResult["SECTIONS"]) / 2);?>
	
	<ul>
		<?$counter = 0;?>
		<?foreach($arResult["SECTIONS"] as $arSection):?>
			<?$counter++;?>
			<?if ($counter == $countSections):?>
				</ul><ul>
			<?endif;?>
			<li><a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a></li>
		<?endforeach;?>
	</ul>
</div>