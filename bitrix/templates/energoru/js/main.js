function getClientWidth()
{
   return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientWidth : document.body.clientWidth;
}
var t;
function up() {
   var top = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
   if (top > 0) {
      window.scrollBy(0, -100);
      t = setTimeout('up()', 20);
   } else
      clearTimeout(t);
   return false;
}
function customStyles() {
   if (getClientWidth() <= 1200)
   {
      $('.main_news .name, .main_news .name a').css({"font-size": "18px", "line-height": "20px"});
   }
   else {
      $('.main_news .name, .main_news .name a').attr('style', '');
   }
}
;

function centredPopup() {
   var p_width = 0, c_width = 0;
   $('.popup_mask').each(function() {
      var el = $(this);
      if (!el.is(':hidden')) {
         p_width = el.width();
      }
   })

   $('.popup_style').each(function() {
      var el = $(this);
      if (!el.is(':hidden')) {
         c_width = el.outerWidth();
         p_left = (p_width - c_width) / 2;
         el.css("left", p_left)
      }
   })

}
function getClientHeight() {
   //return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientHeight : document.documentElement.clientHeight;
   return $(window).height();
}

function getScrollTop() { // �������� ���� ������� Safari
   /*if (window.devicePixelRatio) {
      return document.body.scrollTop;
   }
   else {
      return document.documentElement.scrollTop;
   }*/
   return $(window).scrollTop();
}

function showPlayer(myVideo) { // �������� ����������� � ��������� ����
   var myX = (document.body.clientWidth - 620) / 2 + 'px';
   var myY = (getScrollTop() + getClientHeight() / 2 - 264) + 'px';
   console.log("getScrollTop()", getScrollTop());
   console.log("getClientHeight()", getClientHeight());

   var myS = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="480" height="404" id="player1" name="player1">';
   myS += '<param name="movie" value="/jwplayer/player.swf">';
   myS += '<param name="allowfullscreen" value="true">';
   myS += '<param name="allowscriptaccess" value="always">';
   myS += '<param name="width" value="480">';
   myS += '<param name="height" value="404">';
   myS += '<param name="flashvars" value="width=480&height=404&file=' + myVideo + '&autostart=true">';
   myS += '<param name="wmode" value="opaque">';
   myS += '<embed id="player1"';
   myS += 'name="player1"';
   myS += 'src="/jwplayer/player.swf"';
   myS += 'width="480"';
   myS += 'height="404"';
   myS += 'allowscriptaccess="always"';
   myS += 'allowfullscreen="true"';
   myS += 'flashvars="width=480&height=404&file=' + myVideo + '&autostart=true"';
   myS += 'wmode="opaque" ';
   myS += '/>';
   myS += '</object>';

   myS += '<a href="javascript:void(0);"  onClick="closePlayer();">������� �����</a>';

   //document.getElementById('flvplayer').style.top = myY;
   //document.getElementById('flvplayer').style.left = myX;
   document.getElementById('swfframe').innerHTML = myS;
   document.getElementById('flvplayer').style.display = 'block';
   return true;
}


function closePlayer() { // �������� �����������
   document.getElementById('flvplayer').style.display = 'none';
   document.getElementById('swfframe').innerHTML = '';
   return true;
}

function lazyload_withTitleArt(){
   $(".withTitleArt .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   $(".withTitleArt .mb_menu li a").parent().parent().parent().find("div.tab").eq(0).show();
}

function lazyload_withTitlePhoto(){
   $(".withTitlePhoto .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   $(".withTitlePhoto .mb_menu li a").parent().parent().parent().find("div.tab").eq(0).show();
}

$('document').ready(function() {
   $('.to_top').click(function() {
      up();
      return false;
   });
   $('.roll a').click(function() {
      if ($(this).children().attr("id") == "open")
         $(this).children().attr("id", "close");
      else
         $(this).children().attr("id", "open");
      $(this).parent().parent().parent().find("ul").toggle();
      return false;
   });
   $(".withoutTitle .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   $(".withoutTitle .mb_menu li a").parent().parent().parent().find("div.tab").eq(0).show();
   $(".withoutTitle .mb_menu li a").click(function() {
      $(this).parent().parent().find("li").removeClass("sel");
      $(this).parent().addClass("sel");
      $(this).parent().parent().parent().find("div.tab").hide();
      $(this).parent().parent().parent().find("div.tab").eq($(this).parent().index()).show();//addClass("eewrwerw");//filter(this.hash).show();
      return false;
   });

   $(".withoutTitleF .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   $(".withoutTitleF .mb_menu li a").parent().parent().parent().find("div.tab").eq(0).show();
   $(".withoutTitleF .mb_menu li a").click(function() {
      $(this).parent().parent().find("li").removeClass("sel");
      $(this).parent().addClass("sel");
      $(this).parent().parent().parent().find("div.tab").hide();
      $(this).parent().parent().parent().find("div.tab").eq($(this).parent().index()).show();//addClass("eewrwerw");//filter(this.hash).show();
      return false;
   });

   $(".withoutTitleM .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   var index = $(".withoutTitleM .mb_menu li.sel").index();
   $(".withoutTitleM .mb_menu li a").parent().parent().parent().find("div.tab").eq(index).show();
   $(".withoutTitleM .mb_menu li a").click(function() {
      $(this).parent().parent().find("li").removeClass("sel");
      $(this).parent().addClass("sel");
      $(this).parent().parent().parent().find("div.tab").hide();
      $(this).parent().parent().parent().find("div.tab").eq($(this).parent().index()).show();//addClass("eewrwerw");//filter(this.hash).show();
      return false;
   });

   $(".withTitle .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   $(".withTitle .mb_menu li a").parent().parent().parent().find("div.tab").eq(0).show();
   $(".withTitle .mb_menu li a").click(function() {
      if ($(this).parent().index() == 0)
         return false;
      $(this).parent().parent().find("li").removeClass("sel");
      $(this).parent().addClass("sel");
      $(this).parent().parent().parent().find("div.tab").hide();
      $(this).parent().parent().parent().find("div.tab").eq($(this).parent().index() - 1).show();//addClass("eewrwerw");//filter(this.hash).show();
      return false;
   });

   $(".withTitleMoln .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   $(".withTitleMoln .mb_menu li a").parent().parent().parent().find("div.tab").eq(0).show();
   $(".withTitleMoln .mb_menu li a").click(function() {
      if ($(this).parent().index() == 0)
         return false;
      $(this).parent().parent().find("li").removeClass("sel");
      $(this).parent().addClass("sel");
      $(this).parent().parent().parent().find("div.tab").hide();
      $(this).parent().parent().parent().find("div.tab").eq($(this).parent().index() - 1).show();//addClass("eewrwerw");//filter(this.hash).show();
      return false;
   });

   lazyload_withTitleArt();
   /*$(".withTitleArt .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   $(".withTitleArt .mb_menu li a").parent().parent().parent().find("div.tab").eq(0).show();*/
   $(".withTitleArt .mb_menu li a").click(function() {
      /*if ($(this).parent().index() == 0)
         return false;*/
      $(this).parent().parent().find("li").removeClass("sel");
      $(this).parent().addClass("sel");
      $(this).parent().parent().parent().find("div.tab").hide();
      $(this).parent().parent().parent().find("div.tab").eq($(this).parent().index()).show();//addClass("eewrwerw");//filter(this.hash).show();
      return false;
   });

   $(".withTitleMarket .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   $(".withTitleMarket .mb_menu li a").parent().parent().parent().find("div.tab").eq(0).show();
   $(".withTitleMarket .mb_menu li a").click(function() {
      $(this).parent().parent().find("li").removeClass("sel");
      $(this).parent().addClass("sel");
      $(this).parent().parent().parent().find("div.tab").hide();
      $(this).parent().parent().parent().find("div.tab").eq($(this).parent().index() - 1).show();//addClass("eewrwerw");//filter(this.hash).show();
      return false;
   });
   
   //$(".withTitlePhoto .mb_menu li a").parent().parent().parent().find("div.tab").hide();
   //$(".withTitlePhoto .mb_menu li a").parent().parent().parent().find("div.tab").eq(0).show();
   lazyload_withTitlePhoto();
   $(".withTitlePhoto .mb_menu li a").click(function() {
      $(this).parent().parent().find("li").removeClass("sel");
      $(this).parent().addClass("sel");
      $(this).parent().parent().parent().find("div.tab").hide();
      $(this).parent().parent().parent().find("div.tab").eq($(this).parent().index()).show();//addClass("eewrwerw");//filter(this.hash).show();
      return false;
   });
   customStyles();
   centredPopup();

   $(window).resize(function() {
      customStyles();
      centredPopup();
   });

   $('.search_selectors a').bind("click", function() {
      $('.search_selectors a').removeClass('sel');
      $(this).addClass('sel');
      $('#wher_id').val($(this).attr('id'));
   })

   /*���������� � 1 ������� 
    <<<<*/
   $('.search_form_suggest input[type="text"]').click(function() {
      if ($(this).val() == '������� ����� ��� ������')
      {
         $(this).val('');
         $(this).removeClass('notUsed');
      }
   })
   $('.search_form_suggest input[type="text"]').blur(function() {
      if ($(this).val() == '')
      {
         $(this).val('������� ����� ��� ������');
         $(this).addClass('notUsed');
      }
   })

   $('#subscrInputId').click(function() {
      if ($(this).val() == '��� e-mail')
      {
         $(this).val('');
         $(this).removeClass('notUsed');
      }
   })

   $('#subscrInputId').blur(function() {
      if ($(this).val() == '')
      {
         $(this).val('��� e-mail');
         $(this).addClass('notUsed');
      }
   })

   $('#subSeachId').click(function() {
      if ($(this).val() == '������� ��� ��� �����������')
      {
         $(this).val('');
         $(this).removeClass('notUsed');
      }
   })

   $('#subSeachId').blur(function() {
      if ($(this).val() == '')
      {
         $(this).val('������� ��� ��� �����������');
         $(this).addClass('notUsed');
      }
   })

   $('#smlSrchInputId, #subSeachId_01, #subSeachId_02').click(function() {
      if ($(this).val() == '����� � �������')
      {
         $(this).val('');
         $(this).removeClass('notUsed');
      }
   })

   $('#smlSrchInputId, #subSeachId_01, #subSeachId_02').blur(function() {
      if ($(this).val() == '')
      {
         $(this).val('����� � �������');
         $(this).addClass('notUsed');
      }
   })

   /*>>>*/

   $('.btn_show').click(function() {
      var id = $(this).attr('id');
      if ($('#newsLentaId' + id).is(':hidden'))
      {
         $(this).removeClass('closed').addClass('opened');
         $('.btn_show span').text('��������');
         //$('.toog').animate({marginTop:(".news_lenta ul li").length*18 + "px"}, 'slow',  'linear');		
      }
      else
      {
         $(this).removeClass('opened').addClass('closed');
         $('.btn_show span').text('����������');
         //$('.toog').animate({marginTop:0+ "px"}, 'slow',  'linear');	
      }
      $('#newsLentaId' + id).slideToggle();
   })

   $('.faces_row .image_box, .item_row .image_box').bind("mouseover", function() {
      $(this).parent().find('.title a').addClass('hovered');
      $(this).parent().find('.row a').addClass('hovered');
   })

   $('.faces_row .image_box, .item_row .image_box').bind("mouseleave", function() {
      $(this).parent().find('.title a').removeClass('hovered');
      $(this).parent().find('.row a').removeClass('hovered');
   })

   $('.popup_style .icon_close').click(function() {
      $(this).parents('.popup_style').hide();
      $('.popup_mask').hide();
   });


   $("#sel_courses .selector").click(
           function() {
              $(this).find("ul.root_popup").slideToggle(180);
           }
   );
   $("#sel_courses .selector").mouseleave(
           function() {
              setTimeout(function() {
                 if (!$("#sel_courses .selector").find("ul.root_popup").is(".nothide"))
                    $("#sel_courses .selector").find("ul.root_popup").slideUp(180);
              }, 100);
           }
   );
   $(".rootitems").mouseover(function() {
      $(this).addClass("showpopup");
      if ($(this).find(".items_popup").length)
      {
         var heightMenu = $(this).find(".items_popup").height();
         var ofssetMenu = $(this).offset();
         var heightContent = $(".content_center").height();
         var ofssetContent = $(".content_center").offset();
         if ((ofssetMenu.top + heightMenu) > (ofssetContent.top + heightContent))
         {
            var setTop = (ofssetContent.top + heightContent) - (ofssetMenu.top + heightMenu) - 8;
            $(this).find(".items_popup").css({
               top: setTop + "px"
            });
         }
         else
         {
            $(this).find(".items_popup").css({
               top: 1 + "px"
            });
         }
      }
   }).mouseout(
           function() {
              $(this).removeClass("showpopup");
           }
   );
   $(".selector li a").click(
           function() {
              var city = parseInt($(this).attr("city"));
              var type = parseInt($(this).attr("type"));
              if (parseInt($("#CURCITY").val()) == city && parseInt($("#CURTYPE").val()) == type)
                 return false;
// alert(city);
              if (city == 710) //��� ���� �������
              {
                 $("#CURCITY").val(city);
                 $("#CURTYPE").val(type);
                 $(".selector span").html($(this).html());
                 $("#sel_courses .selector").mouseleave();
                 ReloadCourses();
              }
              else if (city) //��� ������������� ������
              {
                 $("#CURCITY").val(city);
                 $("#CURTYPE").val(type);
                 $(".selector span").html($(this).parents(".items_popup").next("a").html() + ", " + $(this).html());
                 $("#sel_courses .selector").mouseleave();
                 ReloadCourses();
              }
//$("#courses .table_box table").hide();
              return false;
           }
   );
   function ReloadCourses()
   {
      $(".rowsCourse").parents(".informer_body").append('<div class="waitwindowlocal">���������...</div>');
      $(".t_c_bank").fadeTo(200, 0);
      var city = $("#CURCITY").val();
      var type = $("#CURTYPE").val();
      var date = $("#CURDATE").val();
      $.ajax({
         type: "POST",
         dataType: "json",
         url: "/bitrix/api.php",
         data: "CITY=" + city + "&TYPE=" + type + "&DATE=" + date + "&ACTION=GETCOURSEBYDATE",
         success: function(date) {
            if (date.ERROR == "Y")
               return;
            $(".rowsCourse").each(function(i) {
               if (date.COURSES[i].VALTO != "BLR")
                  $(this).find(".corsname").html('<span>' + date.COURSES[i].VALIN.toUpperCase() + '/' + date.COURSES[i].VALTO.toUpperCase() + '</span>');
               else
                  $(this).find(".corsname").html('<img alt="' + date.COURSES[i].VALIN.toLowerCase() + '" \n\
src="/images/ico_' + date.COURSES[i].VALIN.toLowerCase() + '.png">\n\
<span>' + date.COURSES[i].VALIN.toUpperCase() + '</span>');
               $(this).find(".corsebuy").html(date.COURSES[i].VALUE);
               $(this).find(".corsesale").html(date.COURSES[i].VALUE2);
            }
            );
            $('#coursesshows').val(date.SHOWCHANGEONLY);
            CheckCoursesState();
            $(".t_c_bank").fadeTo(200, 1, function() {
               $(".waitwindowlocal").fadeTo(100, 0, function() {
                  $(this).remove()
               })
            });
         }
      });
   }
   function CheckCoursesState()
   {
      if (parseInt($("#coursesshows").val()))
      {
         $("#coursesshows").parents("form").find(".last").each(
                 function()
                 {
                    $(this).hide();
                    $(this).prev().css("border-right-width", '0px');
                 }
                 );
              }
      else
      {
         $("#coursesshows").parents("form").find(".last").each(
                 function()
                 {
                    $(this).show();
                    $(this).prev().css("border-right-width", '1px');
                 }
                 );
              }
      $(".t_c_bank tr").each(function(i) {
         if (i % 2)
            $(this).addClass("dark");
         else
            $(this).removeClass("dark");
      });
      $(".rselectedItems").removeClass("rselectedItems");
      $(".iselectedItems").removeClass("iselectedItems");
      var city = $("#CURCITY").val();
      var type = $("#CURTYPE").val();
      $("#TYPES" + type).addClass("rselectedItems");
      if (city != 710)
         $(".rselectedItems .CITIES" + city).addClass("iselectedItems");
   }
   CheckCoursesState();
   function ToggleDataPicer() {
      var obj = $('#courses a[rel="calendar"]');
      var offsetParents = $('#courses a[rel="calendar"]').parents('.informer_body').offset();
      var offset = obj.offset();
      var hidden = {
         left: "0px",
         top: (offset.top - offsetParents.top + obj.height()) + "px",
         width: obj.width(),
         height: obj.height()
      };
      var oppened = {
         left: '172px',
         top: '0px',
         width: '201px',
         height: '215px'
      };
      if (!dataPicerShowed)
      {
         $("#courses .calendar").css(hidden);
         $("#courses .calendar").show();
         $("#courses .calendar").animate(oppened, 300);
         $('#courses a[rel="calendar"]').html('������ �����');
         dataPicerShowed = 1
      }
      else
      {
         $("#courses .calendar").animate(hidden, 300, 'linear', function() {
            $(this).hide();
         });
         $('#courses a[rel="calendar"]').html('����� ������');
         dataPicerShowed = 0
      }
      return false;
   }
   var dataPicerShowed = 0;
   $('#courses a[rel="calendar"]').click(function() {
      return ToggleDataPicer();
   });
   $("#courses .calendar .close").click(function() {
      return ToggleDataPicer();
   });
   //���������
});


function checkStringJS(str) {
   var regex=/^[a-zA-Z�-��-�0-9]*$/;
   if(str.search(regex)==-1)
      return false;
   else
      return true;
}

function checkEmailJS(str) {
   var regex=/^[-a-z0-9!#$%&\'*+\/=?^_`{|}~]+\.?([-a-z0-9!#$%&'*+\/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)+[a-z]{1,6}$/i;
   if(str.search(regex)==-1)
      return false;
   else
      return true;
}

//���������
function validation(args){
   return function(){
      args.element = $(args.selector);
      var status = true;
      for(var index in args.require){
         if(!args.require[index].compare(args.element)){
            args.showError(args.element, args.require[index].errortext);
            status = false;
            break;
         }
      }
      if(status)
         args.hideError(args.element);
      return status;
   }
}

/*
 *������
 var isValid = validation({
   element: $(this),
   require: [
      {
         compare: function($element){
            return $element.val().length
         },
         errortext: "Name required"
      },{
         compare: function($element){
            return (($element.val().length >= 3) && ($element.val().length <= 50))
         },
         errortext: "From 3 to 50 letters"
      },{
         compare: function($element){
            return checkStringJS($element.val())
         },
         errortext: "Only A-Z, �-�, 0-9"
      }
   ],
   showError: function($element, text){
      var $tr = $element.closest('tr');
      $tr.addClass("error");
      $tr.find('.errortext').text(text);
   },
   hideError: function($element){
      var $tr = $element.closest('tr');
      if($tr.hasClass("error")){
         $tr.removeClass("error");
         $tr.find('.errortext').text("");
      }
   }
})
 */

//fancybox table
$(document).ready(function(){
   $('.js-fancybox_table').each(function(index, domElement){
      var $container = $(this)
      $container
         .find('table')
         .each(function(tableIndex, tableDomElement){
            if($(this).width() > $container.width()){
               $(this)
                  .wrap('<div class="fancybox_table_wrapper"></div>')
                  .before('<div class="fancybox_table_gradient" onclick="$(this).next().click()"></div>')
                  .fancybox({
                     maxWidth	: 1000,
                     fitToView	: false,
                     autoSize	: true,
                     closeClick	: false,
                     openEffect	: 'none',
                     closeEffect: 'none',
                     content	: $(this).html()
                  });
            }
         });
   })
});