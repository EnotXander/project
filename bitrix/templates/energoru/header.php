<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title><? $APPLICATION->ShowTitle(); ?></title>
      <link rel="stylesheet" href="http://<?=SITE_SERVER_NAME?>/css/chosen.css">
      <link href="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/css/styles.css" rel="stylesheet" type="text/css">
      <link href="http://<?=SITE_SERVER_NAME?>/theme/red/style.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/fancybox/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
      <!--[if lte IE 8]><link rel="stylesheet" href="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/css/ie.css" type="text/css"><![endif]-->
      <script type="text/javascript" src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/jquery-1.6.2.min.js"></script>
      <script type="text/javascript" src="http://<?=SITE_SERVER_NAME?>/js/jquery.autoSuggest.js"></script>
      <script type="text/javascript" src="http://<?=SITE_SERVER_NAME?>/js/chosen.jquery.js"></script>
      <script type="text/javascript" src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/main.js"></script>
      <script type="text/javascript" src="http://<?=SITE_SERVER_NAME?>/jwplayer/jwplayer.js"></script>
      <script src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/pic.core.js" type="text/javascript"></script>
      <script src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/pic.jquery.js" type="text/javascript"></script>
      <script src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/desaturate.js" type="text/javascript"></script>
      <script src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/hls.js" type="text/javascript"></script>
      <script type="text/javascript" src="http://<?=SITE_SERVER_NAME?>/js/banner.js"></script>
      <script type="text/javascript" src="//vk.com/js/api/openapi.js?62"></script>
      <script type="text/javascript" src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/jquery.mousewheel-3.0.6.pack.js"></script>
      <script type="text/javascript" src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/js/fancybox/jquery.fancybox.pack.js?v=2.1.4"></script>

      <script type="text/javascript">
         VK.init({apiId: 3199275, onlyWidgets: true});
      </script>
      <? $APPLICATION->ShowHead() ?>
      </head>
      <body id="top">
         <div id="fb-root"></div>
         <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));</script>
         <? $APPLICATION->ShowPanel(); ?>
         <div class="middle_banner" style="width:100%; margin: 0 auto;">
            <?
            $APPLICATION->IncludeComponent("bitrix:advertising.banner", "top_b", Array(
                "TYPE" => "A1", // ��� �������
                "NOINDEX" => "N", // ��������� � ������ noindex/nofollow
                "CACHE_TYPE" => "Y", // ��� �����������
                "CACHE_TIME" => 3600*24, // ����� ����������� (���.)
                "CACHE_NOTES" => ""
                    ), false
            );
            ?>
         </div>
         <div class="main">
            <div class="header">
               <a class="logo" href="http://<?=SITE_SERVER_NAME?>" title="�������������� ������ ��������������">
                  <img src="http://<?=SITE_SERVER_NAME?><?= SITE_TEMPLATE_PATH ?>/images/logo.png" alt="�������������� �������">
               </a>
               <div class="login_block bx-system-auth-form"></div>
               <div class="clear"></div>
               <div class="main_memu" style="height: 45px;">
                  <?
                  $APPLICATION->IncludeComponent("bitrix:menu", "energo_top", Array(
                      "ROOT_MENU_TYPE" => "top", // ��� ���� ��� ������� ������
                      "MAX_LEVEL" => "1", // ������� ����������� ����
                      "CHILD_MENU_TYPE" => "left", // ��� ���� ��� ��������� �������
                      "USE_EXT" => "Y", // ���������� ����� � ������� ���� .���_����.menu_ext.php
                      "MENU_CACHE_TYPE" => "A", // ��� �����������
                      "MENU_CACHE_TIME" => "3600", // ����� ����������� (���.)
                      "MENU_CACHE_USE_GROUPS" => "Y", // ��������� ����� �������
                      "MENU_CACHE_GET_VARS" => "", // �������� ���������� �������
                          ), false
                  );
                  ?> 
               </div><!--main_memu-->
            </div><!--header-->
            <div class="content">
               <div class="content_box">
                  <? if ($_SERVER["REQUEST_URI"] != "/work.php"): ?>
                     <?
                     $APPLICATION->IncludeComponent("bitrix:main.include", "left", Array(
                         "AREA_FILE_SHOW" => "sect",
                         "AREA_FILE_SUFFIX" => "work",
                         "AREA_FILE_RECURSIVE" => "Y",
                         "EDIT_MODE" => "html",
                             ), false, Array('HIDE_ICONS' => 'Y')
                     );
                     ?>
                     <?if(!(defined('ERROR_404') && ERROR_404 == 'Y')):?>
                        <?
                        $APPLICATION->IncludeComponent("bitrix:main.include", "left", Array(
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                                ), false, Array('HIDE_ICONS' => 'Y')
                        );
                        ?>
                     <? endif; ?>
                  <? endif; ?>