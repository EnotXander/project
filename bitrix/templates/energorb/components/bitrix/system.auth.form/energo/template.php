<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="login_block">

<?if ($arResult["FORM_TYPE"] == "login"):?>
	<div class="login_form" id="loginFormId">
	<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
	<input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" style="text-indent:-9999px; width:0px; height:0px; border:none; background:none; padding:0; margin:0;"/>
		<div class="lf_header">
			<a class="dotted login_lnk" href="javascript:void(0)" onClick="jQuery('#loginFormId').toggle();">���� � �����������</a>
			<div class="lf_header_bottom"></div>
		</div><!--lf_header-->
        
		<div class="lb_label">������� ��� e-mail</div>
		<div class="lb_input_box">
			<input type="text" name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>">
        </div>
        
        <div class="spaser"></div>
        
        <div class="lb_label">������� ������</div>
        <div class="lb_input_box">
			<input type="password" name="USER_PASSWORD" value="<?=$arResult["USER_PASSWORD"]?>">
        </div>
        
        <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="forgot_pswrd">������ ������?</a>
        
        <div class="spaser"></div>
        
        <a class="btns gray_btn fl_right" href="<?=$arResult["AUTH_REGISTER_URL"]?>"><i></i>������������������</a>
        <a class="btns fl_left" href="javascript:void(0);" onclick="document.system_auth_form<?=$arResult["RND"]?>.submit();"><i></i>�����</a>
        
        <div class="spaser clear"></div>
		<?
		$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons", 
			array(
				"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
				"SUFFIX"=>"form",
			), 
			$component, 
			array("HIDE_ICONS"=>"Y")
		);
		?>
        <!--<div class="socials">
          <a href="#" class="icons icon_vk" title="vk"></a>
          <a href="#" class="icons icon_twit" title="twitter"></a>
          <a href="#" class="icons icon_g_plus" title="google plus"></a>
          <a href="#" class="icons icon_face" title="facebook"></a>
          <a href="#" class="icons icon_mail_ru" title="mail.ru"></a>
          <a href="#" class="icons icon_odnolk" title="�������������"></a>
          <a href="#" class="icons icon_yand" title="yandex"></a>
        </div>-->
    </form>
	</div><!--login_form-->
	<a class="dotted login_lnk" href="javascript:void(0)" onClick="jQuery('#loginFormId').toggle();">���� � �����������</a>

<!--
<div id="login-form-window">
<div id="login-form-window-header">
<div onclick="return authFormWindow.CloseLoginForm()" id="close-form-window" title="<?=GetMessage('AUTH_CLOSE_WINDOW_TITLE')?>"><?=GetMessage('AUTH_CLOSE_WINDOW')?></div><b><?=GetMessage('AUTH_')?></b>
</div>
<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>

<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
	<input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" />
	<table width="95%">
		<tr>
			<td colspan="2">
			<?=GetMessage("AUTH_LOGIN")?>:<br />
			<input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" size="17" /></td>
		</tr>
		<tr>
			<td colspan="2">
			<?=GetMessage("AUTH_PASSWORD")?>:<br />
			<input type="password" name="USER_PASSWORD" maxlength="50" size="17" />
<?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
<script type="text/javascript">
document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
</script>
<?endif?>
			</td>
		</tr>
<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
		<tr>
			<td valign="top"><input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /></td>
			<td width="100%"><label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?echo GetMessage("AUTH_REMEMBER_SHORT")?></label></td>
		</tr>
<?endif?>
<?if ($arResult["CAPTCHA_CODE"]):?>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr>
<?endif?>
		<tr>
			<td colspan="2"><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></td>
		</tr>
<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a></noindex><br /></td>
		</tr>
<?endif?>

		<tr>
			<td colspan="2"><noindex><a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></noindex></td>
		</tr>
<?if($arResult["AUTH_SERVICES"]):?>
		<tr>
			<td colspan="2">
				<div class="bx-auth-lbl"><?=GetMessage("socserv_as_user_form")?></div>
			</td>
		</tr>
<?endif?>
	</table>
</form>

<?if($arResult["AUTH_SERVICES"]):?>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"Y",
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>
<?endif?>

</div>
<?if ($arResult["SHOW_ERRORS"] == "Y" && $arResult["ERROR"] === true):?>
	<span class="errortext"><?=(is_array($arResult["ERROR_MESSAGE"]) ? $arResult["ERROR_MESSAGE"]["MESSAGE"] : $arResult["ERROR_MESSAGE"])?></span>
<?endif?>
-->
<?else:

	$isNTLM = false;
	if (COption::GetOptionString("ldap", "use_ntlm", "N") == "Y")
	{
		$ntlm_varname = trim(COption::GetOptionString("ldap", "ntlm_varname", "REMOTE_USER"));
		if (array_key_exists($ntlm_varname, $_SERVER) && strlen($_SERVER[$ntlm_varname]) > 0)
			$isNTLM = true;
	}

	$params = DeleteParam(array("logout", "login", "back_url_pub"));
	$logoutUrl = $APPLICATION->GetCurPage()."?logout=yes".htmlspecialchars($params == ""? "":"&".$params);
?>
	<div  class="yetauth" style="display:none;"><?=GetMessage("AUTH_WELCOME_TEXT")?> <a href="#" class="red nick">������ �����</a><i>|</i><a href="<?=$logoutUrl?>" class="away">�����</a></div>

	 <span><a href="/personal/profile/" class="red nick"><?=(strlen($arResult["USER_NAME"]) > 0 ? $arResult["USER_NAME"] : $arResult["USER_LOGIN"])?></a></span>&nbsp|&nbsp<a href="<?=$logoutUrl?>" class="goodbye"><?=GetMessage("AUTH_LOGOUT_BUTTON")?></a>
<?endif?>
<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>

</div><!--login_block-->
