<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
   die();
$obParser = new CTextParser;

if(!$arParams["NEWS_COUNT_VISIBLE"])
   $arParams["NEWS_COUNT_VISIBLE"] = 5;

if (is_array($arResult["ITEMS"]))
{
   foreach ($arResult["ITEMS"] as $key => $arElement)
   {
      $rsSection = CIBlockSection::GetList(
              array("SORT"=>"ASC"),
              array("IBLOCK_ID" => $arElement["IBLOCK_ID"], "ID" => $arElement["IBLOCK_SECTION_ID"]),
              false,
              array("ID", "NAME", "UF_SHORT_TITLE", "SECTION_PAGE_URL")
              );
      if($arSection = $rsSection->GetNext())
      {
         $arElement["RIB"] = array(
             "ID" => $arSection["ID"],
             "NAME" => $arSection["NAME"],
             "SECTION_PAGE_URL" => $arSection["SECTION_PAGE_URL"],
             //"CLASS" => "rib_blue",
         );
         switch($arSection["ID"])
         {
            case 5:  $arElement["RIB"]["CLASS"] = "rib_red"; break;//���
            case 6:  $arElement["RIB"]["CLASS"] = "rib_purple"; break;//���� ���
            case 149:  $arElement["RIB"]["CLASS"] = "rib_yellow"; break;//��������
            case 152:  $arElement["RIB"]["CLASS"] = "rib_blue"; break;//���
            case 2695:  $arElement["RIB"]["CLASS"] = "rib_green"; break;//��������
         }
         if(strlen($arSection["UF_SHORT_TITLE"]))
            $arElement["RIB"]["NAME"] = $arSection["UF_SHORT_TITLE"];
      }
      //PrintAdmin($arElement["RIB"]);
      if(is_array($arElement["PREVIEW_PICTURE"])) $arElement["DETAIL_PICTURE"] = $arElement["PREVIEW_PICTURE"];
      $arResult['ITEMS'][$key] = $arElement;
      
      if (is_array($arElement["DETAIL_PICTURE"]))
      {
         $arFileTmp = CFile::ResizeImageGet(
                 $arElement["DETAIL_PICTURE"],
                 array("width" => 229, "height" => 171),
                 BX_RESIZE_IMAGE_EXACT,
                 true
         );
         $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
         $arFileTmp = CFile::ResizeImageGet(
                 $arElement["DETAIL_PICTURE"],
                 array("width" => 53, "height" => 53),
                 BX_RESIZE_IMAGE_EXACT,
                 true
         );
         $arResult["ITEMS"][$key]["DETAIL_PICTURE_MINI"] = array(
             "SRC" => $arFileTmp["src"],
             "WIDTH" => $arFileTmp["width"],
             "HEIGHT" => $arFileTmp["height"],
         );
      }
      //prewiew text detail text
      if(!strlen($arItem["PREVIEW_TEXT"]))
         $arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["DETAIL_TEXT"], 130);
   }
}
?>