<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="rc_block-body">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<div class="rc_block-body-item clearfix">
			<?$image = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_EXACT, true);?>
			<img src="<?=$image['src']?>" alt="" />
			<p><?=$arItem['my_DATE']?> \ <a href="#"><?=$arItem['my_PREVIEW_TEXT']?></a>
			</p>
		</div>
	<?endforeach;?>
</div>
