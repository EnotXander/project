<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule("iblock");
$objElement = new CIBlockElement();
$arResult["SUCCESS"] = array();
$arResult["ERROR"] = array();


//��������� ��������
$rsCompany = CIBlockElement::GetList(
   array("created" => "ASC", "NAME" => "ASC"),
   array(
      "IBLOCK_ID" => IBLOCK_COMPANY,
      "ACTIVE" => "Y",
   ),
   false,
   false,
   array("ID", "DETAIL_PAGE_URL")
);
while($arCompany = $rsCompany->GetNext())
   $arResult["COMPANY"][$arCompany["ID"]] = $arCompany;

//��������� ������
$rsUser = CUser::GetList();
while($arUser = $rsUser->GetNext())
{
   $arUser["NAME_FORMATTED"] = implode(" ", array($arUser["NAME"], $arUser["LAST_NAME"]))." ({$arUser["EMAIL"]})";
   $arResult["USERS"][$arUser["ID"]] = $arUser;
}

//��������� ���� �������������� ������
$arResult["EXISTS"] = array();
$arResult["EXISTS_REL"] = array();
$rsEmployee = CIBlockElement::GetList(
   array("ID" => "ASC", "NAME" => "ASC"),
   array(
      "IBLOCK_ID" => IBLOCK_PREPSTAVITEL,
      "ACTIVE" => "Y",
      "PROPERTY_PREDSTAVITEL_VALUE" => "Y",//�������������
      "PROPERTY_STATUS" => (LANG == 's1' ? 65 : 144)//�������
   ),
   false,
   false,
   array("PROPERTY_COMPANY", "PROPERTY_USER")
);
while($arEmployee = $rsEmployee->GetNext())
{
   $arResult["EXISTS"][$arEmployee["PROPERTY_COMPANY_VALUE"]] = array(
       "ID" => $arEmployee["ID"],
       "USER" => $arResult["USERS"][$arEmployee["PROPERTY_USER_VALUE"]],
   );
   $arResult["EXISTS_REL"][$arEmployee["ID"]] = $arEmployee["PROPERTY_COMPANY_VALUE"];
}

//���������� �������� ������� ������������ ������
if (is_array($arResult["ITEMS"]))
{
   $temp = array();
   foreach ($arResult["ITEMS"] as $arItem)
   {
      $arItem["USER"] = $arResult["USERS"][$arItem["PROPERTIES"]["USER"]["VALUE"]];
      $arItem["COMPANY"] = $arResult["COMPANY"][$arItem["PROPERTIES"]["COMPANY"]["VALUE"]];
      if(isset($arResult["EXISTS"][$arItem["PROPERTIES"]["COMPANY"]["VALUE"]]))
         $arItem["EXISTS"] = $arResult["EXISTS"][$arItem["PROPERTIES"]["COMPANY"]["VALUE"]];//$arResult["USERS"][$arResult["EXISTS"][$arItem["PROPERTIES"]["COMPANY"]["VALUE"]]];
      $temp[$arItem["ID"]] = $arItem;
   }
   $arResult["ITEMS"] = $temp;
}


//���������
if((int)$_REQUEST["per_page"])
{
   $arResult["per_page"] = $_REQUEST["per_page"];
}

//��������� �����
if(check_bitrix_sessid() && is_array($_POST["edit"]) && count($_POST["edit"]))
{
   foreach ($_POST["edit"] as $editId => $edit)
   {
      $edit = (int)$edit;
      if($edit)
      {
         $objElement->SetPropertyValues($editId, IBLOCK_PREPSTAVITEL, $edit, "STATUS");
         $companyId = $arResult["ITEMS"][$editId]["COMPANY"]["ID"];
         $userId = $arResult["ITEMS"][$editId]["USER"]["ID"];
         PredstavitelClearByCompany($companyId);
         PredstavitelClearByUser($userId);
         if($edit == (LANG == 's1' ? 65 : 144))//�������
         {
            unset($arResult["EXISTS"][$companyId]);
            $arResult["SUCCESS"][] = "{$arResult["ITEMS"][$editId]["USER"]["NAME_FORMATTED"]} ��������� - <a href='{$arResult["ITEMS"][$editId]["COMPANY"]["DETAIL_PAGE_URL"]}'>{$arResult["ITEMS"][$editId]["COMPANY"]["NAME"]}</a>";
         }
         else
            $arResult["SUCCESS"][] = "{$arResult["ITEMS"][$editId]["USER"]["NAME_FORMATTED"]} �������� - <a href='{$arResult["ITEMS"][$editId]["COMPANY"]["DETAIL_PAGE_URL"]}'>{$arResult["ITEMS"][$editId]["COMPANY"]["NAME"]}</a>";
         unset($arResult["ITEMS"][$editId]);
      }
   }
}
if(check_bitrix_sessid() && (int)$_POST["new"]["COMPANY"] && (int)$_POST["new"]["USER"])
{
   $companyId = (int)$_POST["new"]["COMPANY"];
   $userId = (int)$_POST["new"]["USER"];
   //�������� ������������ ������
   if(isset($arResult["EXISTS"][$companyId]))
   {
      $objElement->Delete($arResult["EXISTS"][$companyId]["ID"]);
      $userId = $arResult["EXISTS"][$companyId]["USER"]["ID"];
      PredstavitelClearByCompany($companyId);
      PredstavitelClearByUser($userId);
      $arResult["SUCCESS"][] = "{$arResult["EXISTS"][$companyId]["USER"]["NAME_FORMATTED"]} �������� - <a href='{$arResult["COMPANY"][$companyId]["DETAIL_PAGE_URL"]}'>{$arResult["COMPANY"][$companyId]["NAME"]}</a>";
   }
   //�������� ����� ������
   $props = array(
       "COMPANY" => $companyId,
       "USER" => $userId,
       "STATUS" => (LANG == 's1' ? 64 : 143), //�������
       "PREDSTAVITEL" => 75//"Y"
   );
   //���������� ������ ��� ���������� � �������
   $companyName = htmlspecialchars_decode($arResult["COMPANY"][$companyId]["NAME"]);
   $arToSave = array(
       "MODIFIED_BY" => $USER->GetID(), // ������� ������� ������� �������������
       "PROPERTY_VALUES" => $props,
       "IBLOCK_ID" => IBLOCK_PREPSTAVITEL,
       "NAME" => "{$arResult["USERS"][$userId]["NAME"]} {$arResult["USERS"][$userId]["LAST_NAME"]} - {$companyName}",
       "ACTIVE" => "Y", // �������
   );
   $newID = $objElement->Add($arToSave);
   if(intval($newID) > 0)
   {
      $arResult["EXISTS"][$companyId] = array(
         "ID" => $newID,
         "USER" => $arResult["USERS"][$userId],
      );
      PredstavitelClearByCompany($companyId);
      PredstavitelClearByUser($userId);
      $arResult["SUCCESS"][] = "{$arResult["USERS"][$userId]["NAME_FORMATTED"]} ��������� - <a href='{$arResult["COMPANY"][$companyId]["DETAIL_PAGE_URL"]}'>{$arResult["COMPANY"][$companyId]["NAME"]}</a>";



      CIBlockElement::SetPropertyValues(
          $companyId,
          IBLOCK_COMPANY,
          $userId,
          "USER"
      );

   }
   else
      $arResult["ERROR"][] = $objElement->LAST_ERROR;
}