<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->AddHeadString('<script src="/bitrix/templates/energorb/js/jcarousellite_1.0.1.js" type="text/javascript"></script>', true);
?>

<div class="photo_carusel_wrap">
   <span class="photo_carusel_nav nav_prev"></span>
   <span class="photo_carusel_nav nav_next"></span>
   <? if (count($arResult["ITEMS"]) > 0): ?>
      <div class="photo_carusel" id="photo_carusel_<?=$arParams["IBLOCK_ID"]?>">
         <ul>
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
               <li>
                  <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                     <span class="img_area"><img src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>"  width="<?= $arItem["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["SECTION"]["NAME"] ?>" /></span>
                     <span class="slide_title"><?= $arItem["SECTION"]["NAME"] ?></span>
                     <span class="date">\ <? echo $arItem["SECTION"]["DATE_CREATE"] ?></span>
                  </a>
               </li>
            <? endforeach; ?>
         </ul>
      </div>
   <? endif; ?>
</div>