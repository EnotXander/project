<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $APPLICATION;
//�������� ���� �������
$resSection = CIBlockSection::GetList(
	array(),
	array(
		'ID' => $arResult['IBLOCK_SECTION_ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID']
	),
	false,
	array(
		'ID', 'NAME', 'DESCRIPTION', 'SECTION_PAGE_URL', 'UF_USER_ID'
	)
);
$arResult['my_SECTION'] = $resSection->GetNext();
$arResult['my_SECTION']['SECTION_PAGE_URL'] = str_replace(
	'#UF_USER_ID#',
	$arResult['my_SECTION']['UF_USER_ID'],
	$arResult['my_SECTION']['SECTION_PAGE_URL']
);

//�������� ���� �����
$resUser = CUser::GetByID($arResult['my_SECTION']['UF_USER_ID']);
$arrUser = $resUser->GetNext();
$arrUserPhoto = CFile::GetFileArray($arrUser['PERSONAL_PHOTO']);
if ($arrUserPhoto) $arrUser['PERSONAL_PHOTO_PARAMS'] = $arrUserPhoto;
$arResult['my_USER'] = $arrUser;

//���� �������� ����� (��� �����������)
$resUser = CUser::GetByID($USER->GetID());
$arrUser = $resUser->Fetch();
$arrIsRated = explode(',', $arrUser['UF_ISRATED']);
$arResult['my_ISRATED'] = (in_array($arResult['ID'], $arrIsRated)) ? 1 : 0;

//������ ����
$itemDate = $arResult['DISPLAY_ACTIVE_FROM'] ?: $arResult['DATE_CREATE'];
$itemDate = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($itemDate, CSite::GetDateFormat()));
$arResult['my_DATE'] = $itemDate;

//���� ������� ��������
$arrTags = explode(',', $arResult['TAGS']);
foreach ($arrTags as $k=>$v){
	$v = trim($v);
	$arrTags[$k] = '<a href="/blogs/tags/'.str_replace(' ', '+', $v).'">'.$v.'</a>';
}
$arResult['TAGS'] = implode(', ', $arrTags);


//������������� �������� �������� �������� ���������
$userFullname = $arResult['my_USER']['NAME'].' '.$arResult['my_USER']['LAST_NAME'];
$APPLICATION->SetTitle($arResult['NAME'].' - '.$userFullname.' - ������ �������������� ��������');
$APPLICATION->SetPageProperty("description", '��������� ���������� �������� '.$userFullname.' �� ���������� �������� � ����������.');
