<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $USER;
if($USER->IsAuthorized())
{
   $arResult["USER"] = CUser::GetById($USER->GetID())->Fetch();
   $arResult["COMPANY"] = GetIblockElement($arResult["USER"]);
}