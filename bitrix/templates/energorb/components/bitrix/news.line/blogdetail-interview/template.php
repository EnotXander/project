<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="rc_block-body">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		
		<div class="rc_block-body-item clearfix">
			<?$image = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_EXACT, true);?>
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$image['src']?>" alt="" /></a>
			<p>
				<?=$arItem["DISPLAY_ACTIVE_FROM"];?> \ <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
			</p>
		</div>
		
	<?endforeach;?>
</div>