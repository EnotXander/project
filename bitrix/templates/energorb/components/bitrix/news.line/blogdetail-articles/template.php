<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul class="article-images clearfix">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?$image = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_EXACT, true);?>
		<li><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" id="rel-img-<?=$arItem['ID']?>" rel="rel-link-<?=$arItem['ID']?>"><img src="<?=$image['src']?>" alt="" /></a></li>
	<?endforeach;?>
</ul>
<ul class="left_menu">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<li><?=$arItem["DISPLAY_ACTIVE_FROM"];?> \ <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" id="rel-link-<?=$arItem['ID']?>" rel="rel-img-<?=$arItem['ID']?>"><?=$arItem["NAME"]?></a></li>
	<?endforeach;?>
</ul>