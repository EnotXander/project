<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="left_menu">

<?
foreach($arResult as $key => $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["LINK"] == ""):?>
		<?if ($key <> 0):?>
			</ul>
		</li>
		<?endif;?>
		<li>
			<div class="parent_title">
				<div class="roll title_line"><a href="#"><div id="close"></div></a></div>
				<span class="title_line"><?=$arItem["TEXT"]?></span>
			</div>
			<ul class="sub_left_menus">
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>
	</ul>
	</li>
</ul>
<?endif?>