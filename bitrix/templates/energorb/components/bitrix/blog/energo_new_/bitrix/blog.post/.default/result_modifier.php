<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$titleAuthor = "";
if(strlen($arResult["arUser"]["NAME"]) || strlen($arResult["arUser"]["LAST_NAME"]))
{
   $titleAuthor = $arResult["arUser"]["NAME"];
   if(strlen($titleAuthor) && strlen($arResult["arUser"]["LAST_NAME"]))
      $titleAuthor .= " ".$arResult["arUser"]["LAST_NAME"];
}
$APPLICATION->SetTitle("{$arResult["Post"]["TITLE"]} - {$titleAuthor} - ������ �������������� ��������");
$APPLICATION->SetPageProperty("description", "��������� ���������� �������� {$titleAuthor} �� ���������� �������� � ����������.");
$APPLICATION->AddHeadString('<meta name="abstract" content="��������� ���������� �������� '.$titleAuthor.' �� ���������� �������� � ����������.">', true);
$APPLICATION->SetPageProperty("keywords", "�����, ���������� �������, ����������");