<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$titleAuthor = "";
if(strlen($arResult["POST"][0]["arUser"]["NAME"]) || strlen($arResult["POST"][0]["arUser"]["LAST_NAME"]))
{
   $titleAuthor = $arResult["POST"][0]["arUser"]["NAME"];
   if(strlen($titleAuthor) && strlen($arResult["POST"][0]["arUser"]["LAST_NAME"]))
      $titleAuthor .= " ".$arResult["POST"][0]["arUser"]["LAST_NAME"];
}
$APPLICATION->SetTitle("<������� {$titleAuthor} �� ������� EnergoBelarus.by. ������, ���������, ��������� ������.");
$APPLICATION->SetPageProperty("description", "��������� ���������� �������� {$titleAuthor} �� ���������� �������� �������������� �������������� � �����.");
$APPLICATION->AddHeadString('<meta name="abstract" content="��������� ���������� �������� '.titleAuthor.' �� ���������� �������� �������������� �������������� � �����.">', true);
$APPLICATION->SetPageProperty("keywords", "����, {$titleAuthor} ���������� �������, ��������� ������, ����������");