<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obParser = new CTextParser;
$arSectionTop = Array();
$arSections = Array();
if (is_array($arResult["SECTIONS"]))
{
	foreach ($arResult["SECTIONS"] as $key => $arElement) 
	{
		if ($arElement['DEPTH_LEVEL'] > 1)
		{
			$arSections[$arElement['SECTION_ID']]['ITEMS'][] = $arElement;
		}
		else
		{
			global $arCompanyFilter;
			$arFil = Array("IBLOCK_ID" => $arParams['IBLOCK_ID'], 'SECTION_ID' => $arElement['ID'], 'INCLUDE_SUBSECTIONS' => "Y");
			$num = CIBlockElement::GetList(array(),array_merge($arFil, $arCompanyFilter),array());
			if ($num > 0)
			{
				$arElement['ELEMENT_CNT'] = $num;
				$arSectionTop[] = $arElement;
			}
		}
	}
}
$arResult['SECTIONS'] = $arSectionTop;
$arResult['CHILD'] = $arSections;
?>