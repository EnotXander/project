<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(is_array($arResult["PREVIEW_PICTURE"]))
{
   $arFileTmp = CFile::ResizeImageGet(
           $arResult["PREVIEW_PICTURE"],
           array("width" => 100, "height" => 100),
           BX_RESIZE_IMAGE_PROPORTIONAL,
           true
   );
   //$arSize = getimagesize($_SERVER["DOCUMENT_ROOT"] . $arFileTmp["src"]);

   $arResult["PREVIEW_PICTURE"] = array(
      "SRC" => $arFileTmp["src"],
      "WIDTH" => $arFileTmp["width"],
      "HEIGHT" => $arFileTmp["height"],
   );
}

$arResult["STAT_CODE"] = '';
if(strlen($arResult['PROPERTIES']['STAT_CODE']['VALUE']) > 0)
   $arResult["STAT_CODE"] = $arResult['PROPERTIES']['STAT_CODE']['VALUE']; 

//�������� ���������� � ����������� ��������
$arResult["STAFF"] = array();
$arStaff = EmployeeGetByCompany($arResult["ID"]);
foreach($arStaff["LIST"] as $key => $arEmployee)
{
   if($arEmployee["STATUS"] == 65)//�������
      $arResult["STAFF"][] = $USER->GetByID($arEmployee["USER"])->Fetch();
}
//�������� ���������� � ������������� ��������
if(ENABLE_PREDSTAVITEL_MODE)
{
   $arResult["MANAGER"] = array();
   $arPredstavitel = PredstavitelGetByCompany($arResult["ID"]);
   foreach($arPredstavitel["LIST"] as $key => $arEmployee)
   {
      if($arEmployee["STATUS"] == 65)//�������
      {
         $arResult["MANAGER"] = $USER->GetByID($arEmployee["USER"])->Fetch();
      }
      if($arEmployee["USER"] == $USER->GetID())
      {
         $arResult["PREDSTAVITEL_STATUS"] = $arEmployee["STATUS"];
      }
   }
}
else
{
   $rsUsers = CUser::GetByID($arResult["PROPERTIES"]["USER"]["VALUE"]);
   if ($arUser = $rsUsers->Fetch()) 
   {
      $arResult["MANAGER"] = $arUser; 
   }
}


$arResult["F_PRICES"] = array();
if(is_array($arResult["PROPERTIES"]["PRICES"]["VALUE"]) && count($arResult["PROPERTIES"]["PRICES"]["VALUE"]) > 0)
{
   foreach($arResult["PROPERTIES"]["PRICES"]["VALUE"] as $file)
   {
      $file = CFile::GetFileArray($file);
      $file["TYPE"] = GetExtension($file['FILE_NAME']);
      $file["FORMAT_SIZE"] = FormatBytes($file['FILE_SIZE']);
      $file["CLASS"] = GetFileImageClass($file["TYPE"]);
      $arResult["F_PRICES"][] = $file;
   }
}

$arResult["F_CATALOG"] = array();
if(is_array($arResult["PROPERTIES"]["CATALOG"]["VALUE"]) && count($arResult["PROPERTIES"]["CATALOG"]["VALUE"]) > 0)
{
   foreach($arResult["PROPERTIES"]["CATALOG"]["VALUE"] as $file)
   {
      $file = CFile::GetFileArray($file);
      $file["TYPE"] = GetExtension($file['FILE_NAME']);
      $file["FORMAT_SIZE"] = FormatBytes($file['FILE_SIZE']);
      $file["CLASS"] = GetFileImageClass($file["TYPE"]);
      $arResult["F_CATALOG"][] = $file;
   }
}

$arResult["F_CERTIFICATE"] = array();
if(is_array($arResult["PROPERTIES"]["CERTIFICATE"]["VALUE"]) && count($arResult["PROPERTIES"]["CERTIFICATE"]["VALUE"]) > 0)
{
   foreach($arResult["PROPERTIES"]["CERTIFICATE"]["VALUE"] as $file)
   {
      $file = CFile::GetFileArray($file);
      $file["TYPE"] = GetExtension($file['FILE_NAME']);
      $file["FORMAT_SIZE"] = FormatBytes($file['FILE_SIZE']);
      $file["CLASS"] = GetFileImageClass($file["TYPE"]);
      $arResult["F_CERTIFICATE"][] = $file;
   }
}

// �������� ������ ��������� � ������� ��������� ��������
$arResult["CATEGORY"] = array();
$res = CIBlockElement::GetElementGroups($arResult['ID'], true);
while ($category = $res->GetNext())
   $arResult["CATEGORY"][] = "<a href='" . $category['SECTION_PAGE_URL'] . "'>" . $category['NAME'] . "</a>&nbsp;";


// �������� ���������� ��� ���� ������ � ���� ������
$arResult["FIRST_ACTIVE"] = true;
$arResult["SHOW_TABS"] = false;
$arResult["TABS"] = array();

global $customFilter;
$customFilter['ACTIVE'] = "Y";
$customFilter['PROPERTY_FIRM'] = $arResult['ID'];

$perPage = ((int)$_REQUEST['per_page'] > 0 ? (int)$_REQUEST['per_page'] : 20);

if($arResult["PROPERTIES"]["REMOVE_REL"]["VALUE"] != "��") // !!!!!!!!!!!!!!!!!!!!!!!
{
   // ������
   $arResult["TABS"]["PRODUCTS"]["NAME"] = "������";
   $arResult["TABS"]["PRODUCTS"]["HTML"] = "";
   $arResult["TABS"]["PRODUCTS"]["SECTIONS_HTML"] = "";
   
   ob_start();  
   $APPLICATION->IncludeComponent("whipstudio:catalog.section", "company_tab", array(
         "AJAX_MODE" => "N",
         "IBLOCK_TYPE" => "services",
         "IBLOCK_ID" => 27,
         "SECTION_ID" => (isset($_GET["SECTION_P_ID"]) && (int)$_GET["SECTION_P_ID"] > 0 ? (int)$_GET["SECTION_P_ID"] : ""),
         "SECTION_CODE" => "",
         "SECTION_USER_FIELDS" => array(),
         "ELEMENT_SORT_FIELD" => "PROPERTY_SORT_COMPANY",
         "ELEMENT_SORT_ORDER" => "desc",
         "FILTER_NAME" => "customFilter",
         "INCLUDE_SUBSECTIONS" => "Y",
         "SHOW_ALL_WO_SECTION" => "Y",
         "SECTION_URL" => "",
         "DETAIL_URL" => "",
         "BASKET_URL" => "/personal/basket.php",
         "ACTION_VARIABLE" => "action",
         "PRODUCT_ID_VARIABLE" => "id",
         "PRODUCT_QUANTITY_VARIABLE" => "quantity",
         "PRODUCT_PROPS_VARIABLE" => "prop",
         "SECTION_ID_VARIABLE" => "SECTION_ID",
         "META_KEYWORDS" => "-",
         "META_DESCRIPTION" => "-",
         "BROWSER_TITLE" => "-",
         "ADD_SECTIONS_CHAIN" => "N",
         "DISPLAY_COMPARE" => "N",
         "SET_TITLE" => "N",
         "SET_STATUS_404" => "N",
         "PAGE_ELEMENT_COUNT" => $perPage,
         "LINE_ELEMENT_COUNT" => 3,
         "PROPERTY_CODE" => array(
             "FIRM",
             "COST",
             "BREND",
             "NUMBER",
             "SEARCH_NUMBER",
             "SPEC",
             "KEYWORDS"
         ),
         "PRICE_CODE" => array(),
         "USE_PRICE_COUNT" => "N",
         "SHOW_PRICE_COUNT" => "1",
         "PRICE_VAT_INCLUDE" => "Y",
         "PRODUCT_PROPERTIES" => array(),
         "USE_PRODUCT_QUANTITY" => "Y",
         "CACHE_TYPE" => "N",
         "CACHE_TIME" => "36000000",
         "CACHE_FILTER" => "N",
         "CACHE_GROUPS" => "N",
         "DISPLAY_TOP_PAGER" => "N",
         "DISPLAY_BOTTOM_PAGER" => "Y",
         "PAGER_TITLE" => "������",
         "PAGER_SHOW_ALWAYS" => "Y",
         "PAGER_TEMPLATE" => "",
         "PAGER_DESC_NUMBERING" => "Y",
         "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
         "PAGER_SHOW_ALL" => "Y",
         "AJAX_OPTION_JUMP" => "N",
         "AJAX_OPTION_STYLE" => "Y",
         "AJAX_OPTION_HISTORY" => "N",
         "CONVERT_CURRENCY" => "N",
         "CURRENCY_ID" => "",
         "THUMB_WIDTH" => 100,
         "THUMB_HEIGHT" => 100
      )
   );
   $arResult["TABS"]["PRODUCTS"]["HTML"] = ob_get_clean();
   
   if(strlen($arResult["TABS"]["PRODUCTS"]["HTML"]) > 0)
   {
      $data = array();
      $sections = array();
      $arFilter = array(
          "IBLOCK_ID" => 27
      );
      $arFilter = array_merge($arFilter, $customFilter);
      $res = CIBlockElement::GetList(array(),$arFilter, array("IBLOCK_SECTION_ID"));
      while($element = $res->GetNext())
      {
         $data[$element["IBLOCK_SECTION_ID"]] = $element;
         $sections[] = $element["IBLOCK_SECTION_ID"];
      }
      global $customSectionFilter;
      $customSectionFilter['ID'] = $sections;
      ob_start();   
      $APPLICATION->IncludeComponent("whipstudio:catalog.section.list", "company_tab", array(
            "IBLOCK_TYPE" => "services",
            "IBLOCK_ID" => 27,
            "DISPLAY_PANEL" => "N",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "N",
            "SECTION_URL" => "",
            "FILTER_NAME" => "customSectionFilter",
            "COUNT_ELEMENTS" => "N",
            "TOP_DEPTH" => 10,
            "SECTION_VAR" => "SECTION_P_ID",
            "COUNT_INFO" => $data
         )
      );
      $arResult["TABS"]["PRODUCTS"]["SECTIONS_HTML"] = ob_get_clean();
   }
  
   if(strlen($arResult["TABS"]["PRODUCTS"]["HTML"]) > 0)
      $arResult["SHOW_TABS"] = true;
   if(isset($_GET["SECTION_P_ID"]))
   {
      $arResult["TABS"]["PRODUCTS"]["ACTIVE"] = true;
      $arResult["FIRST_ACTIVE"] = false;
   }
}

// ������
if($arResult["PROPERTIES"]["REMOVE_REL"]["VALUE"] != "��") // !!!!!!!!!!!!!!!!!!!!!!!
{
   $arResult["TABS"]["SERVICES"]["NAME"] = "������";
   $arResult["TABS"]["SERVICES"]["HTML"] = "";
   $arResult["TABS"]["SERVICES"]["SECTIONS_HTML"] = "";
   ob_start();
   $APPLICATION->IncludeComponent("whipstudio:catalog.section", "company_tab", array(
         "AJAX_MODE" => "N",
         "IBLOCK_TYPE" => "services",
         "IBLOCK_ID" => 28,
         "SECTION_ID" => (isset($_GET["SECTION_S_ID"]) && (int)$_GET["SECTION_S_ID"] > 0 ? (int)$_GET["SECTION_S_ID"] : ""),
         "SECTION_CODE" => "",
         "SECTION_USER_FIELDS" => array(),
         "ELEMENT_SORT_FIELD" => "PROPERTY_SORT_COMPANY",
         "ELEMENT_SORT_ORDER" => "asc",
         "FILTER_NAME" => "customFilter",
         "INCLUDE_SUBSECTIONS" => "Y",
         "SHOW_ALL_WO_SECTION" => "Y",
         "SECTION_URL" => "",
         "DETAIL_URL" => "",
         "BASKET_URL" => "/personal/basket.php",
         "ACTION_VARIABLE" => "action",
         "PRODUCT_ID_VARIABLE" => "id",
         "PRODUCT_QUANTITY_VARIABLE" => "quantity",
         "PRODUCT_PROPS_VARIABLE" => "prop",
         "SECTION_ID_VARIABLE" => "SECTION_ID",
         "META_KEYWORDS" => "-",
         "META_DESCRIPTION" => "-",
         "BROWSER_TITLE" => "-",
         "ADD_SECTIONS_CHAIN" => "N",
         "DISPLAY_COMPARE" => "N",
         "SET_TITLE" => "N",
         "SET_STATUS_404" => "N",
         "PAGE_ELEMENT_COUNT" => $perPage,
         "LINE_ELEMENT_COUNT" => 3,
         "PROPERTY_CODE" => array(
             "FIRM",
             "COST",
             "BREND",
             "NUMBER",
             "SEARCH_NUMBER",
             "SPEC",
             "KEYWORDS"
         ),
         "PRICE_CODE" => array(),
         "USE_PRICE_COUNT" => "N",
         "SHOW_PRICE_COUNT" => "1",
         "PRICE_VAT_INCLUDE" => "Y",
         "PRODUCT_PROPERTIES" => array(),
         "USE_PRODUCT_QUANTITY" => "Y",
         "CACHE_TYPE" => "N",
         "CACHE_TIME" => "36000000",
         "CACHE_FILTER" => "N",
         "CACHE_GROUPS" => "N",
         "DISPLAY_TOP_PAGER" => "N",
         "DISPLAY_BOTTOM_PAGER" => "Y",
         "PAGER_TITLE" => "������",
         "PAGER_SHOW_ALWAYS" => "Y",
         "PAGER_TEMPLATE" => "",
         "PAGER_DESC_NUMBERING" => "Y",
         "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
         "PAGER_SHOW_ALL" => "Y",
         "AJAX_OPTION_JUMP" => "N",
         "AJAX_OPTION_STYLE" => "Y",
         "AJAX_OPTION_HISTORY" => "N",
         "CONVERT_CURRENCY" => "N",
         "CURRENCY_ID" => ""
      )
   );
   $arResult["TABS"]["SERVICES"]["HTML"] = ob_get_clean();

   if(strlen($arResult["TABS"]["SERVICES"]["HTML"]) > 0)
   {
      $data = array();
      $sections = array();
      $arFilter = array(
         "IBLOCK_ID" => 28
      );
      $arFilter = array_merge($arFilter, $customFilter);
      $res = CIBlockElement::GetList(array(),$arFilter, array("IBLOCK_SECTION_ID"));
      while($element = $res->GetNext())
      {
         $data[$element["IBLOCK_SECTION_ID"]] = $element;
         $sections[] = $element["IBLOCK_SECTION_ID"];
      }
      global $customSectionFilter;
      $customSectionFilter['ID'] = $sections;
      ob_start();   
      $APPLICATION->IncludeComponent("whipstudio:catalog.section.list", "company_tab", array(
            "IBLOCK_TYPE" => "services",
            "IBLOCK_ID" => 28,
            "DISPLAY_PANEL" => "N",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "N",
            "SECTION_URL" => "",
            "FILTER_NAME" => "customSectionFilter",
            "COUNT_ELEMENTS" => "N",
            "TOP_DEPTH" => 10,
            "SECTION_VAR" => "SECTION_S_ID",
            "COUNT_INFO" => $data
         )
      );
      $arResult["TABS"]["SERVICES"]["SECTIONS_HTML"] = ob_get_clean();
   }

   if(strlen($arResult["TABS"]["SERVICES"]["HTML"]) > 0)
      $arResult["SHOW_TABS"] = true;
   if(isset($_GET["SECTION_S_ID"]))
   {
      $arResult["TABS"]["SERVICES"]["ACTIVE"] = true;
      $arResult["FIRST_ACTIVE"] = false;
   }
}

$arResult["URL"] = array();
if(is_array($arResult["PROPERTIES"]["URL"]["VALUE"]) && count($arResult["PROPERTIES"]["URL"]["VALUE"]) > 0)
{
   foreach($arResult["PROPERTIES"]["URL"]["VALUE"] as $key => $val)
   {
      $val = trim(str_replace(",", "", $val));
      if(substr_count($val, "http://") > 0)
      {
         $arResult["URL"][] = array(
             "LINK" => $val,
             "NAME" => str_replace("http://", "", $val)
         );
      } else 
      {
         $arResult["URL"][] = array(
             "LINK" => "http://".$val,
             "NAME" => $val
         );
      }
   }
}


$titleCompanyName = html_entity_decode($arResult["NAME"]);
$APPLICATION->SetTitle("�������� {$titleCompanyName} �����, ����, ��������� � ����������� - EnergoBelarus.by");
$APPLICATION->SetPageProperty("description", "��������� ���������� � �������� {$titleCompanyName} - �����, �������, �������� ������������ �� ������� Energobelarus.by");
$APPLICATION->AddHeadString('<meta name="abstract" content="��������� ���������� � �������� '.$titleCompanyName.' - �����, �������, �������� ������������ �� ������� Energobelarus.by">', true);
if(strlen($arResult["PROPERTIES"]["keywords"]["VALUE"]["TEXT"]))
{
   $APPLICATION->SetPageProperty("keywords", $arResult["PROPERTIES"]["keywords"]["VALUE"]["TEXT"]);
}
else
{
   //������ ������
   $arSectionTree = array();
   $rsNavChain = CIBlockSection::GetNavChain($arResult["IBLOCK_ID"], $arResult["IBLOCK_SECTION_ID"]);
   while($arNavChain = $rsNavChain->GetNext())
      $arSectionTree[] = $arNavChain["NAME"];
   if(count($arSectionTree))
   {
      $arSectionTree = array_reverse($arSectionTree);
      $arSectionTree = ", ".implode(", ", $arSectionTree);
   }
   else
      $arSectionTree = "";
   $APPLICATION->SetPageProperty("keywords", "��������, {$titleCompanyName}{$arSectionTree}");
}