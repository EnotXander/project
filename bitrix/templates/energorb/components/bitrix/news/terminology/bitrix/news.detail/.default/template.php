<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="page">

   <!-- ��������� -->
   <div class="subject">
      <table width="100%" height="100%">
         <tr>

            <td><p class="subject">����������������� ����������</p></td>

            <td width="210px" style="vertical-align: middle">

               <!-- �������� ������ -->
               <a class="add" href="/terminology/my/?edit=Y">

                  <table>
                     <tr>
                        <td width="21px"><img src="<?= SITE_TEMPLATE_PATH ?>/images/add.png"></td>
                        <td><p class="add">�������� ������ � ����������</p></td>
                     </tr>
                  </table>

               </a>
               <!-- ^�������� ������^ -->

            </td>

         </tr>
      </table>
   </div>
   <!-- ^���������^ -->


   <!-- ����� -->
   <div class="search_element">

      <div class="sub_search_element">
         <table width="100%" height="100%">
            <tr>

               <td class="search">
                  <input class="search_field" type="text">
               </td>

               <td class="search">
                  <select class="search_option">
                     <option>� ����� ���������</option>
                     <option>��������� 1</option>
                     <option>��������� 2</option>
                     <option>��������� 3</option>
                  </select>
               </td>

               <td class="search">

                  <a class="search_button" href="#">
                     <div class="search_button">
                        <table width="100%" height="100%">
                           <tr>
                              <td class="search_button"><p class="search_button">�����</p></td>
                           </tr>
                        </table>
                     </div>
                  </a>

               </td>

            </tr>
         </table>
      </div>

   </div>
   <!-- ^�����^ -->
   <?//printAdmin($arParams)?>

   <!-- � � � � � .... -->
   <div class="ABCD">
      <table class="ABCD">
         <tr>
            <?for ($i=ord("�"); $i<=ord("�"); $i++):?>
            <td>
               <a class="char" href="<?= ($_REQUEST['let'] == chr($i)) ? $APPLICATION->GetCurPageParam("let=" . chr($i), array("let")) : $arParams['IBLOCK_URL'] . "?let=" . chr($i) ?>">
                  <div class="char">
                     <div class="sub_char">
                        <p class="char"><?= chr($i) ?></p>
                     </div>
                  </div>
               </a>
            </td>
            <?endfor;?>
         </tr>
         </tr>
      </table>
   </div>
   <!-- ^� � � � � ....^ -->

   <div class="sit">
      <table width="100%">
         <tr>
            <td><p class="sit"><?= $arResult["NAME"] ?></p></td>
            <td width="300px" style="vertical-align: middle"><p class="corr">�������� ����������? <a class="corr" href="#">���������� �������������</a></p></td>
         </tr>
      </table>
   </div>

   <div class="avar" itemscope itemtype="http://webmaster.yandex.ru/vocabularies/term-def.xml">
      <?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
      <?if ($pid == "AUTHOR"):?>
      <div class="who">
         <p class="who"><?= $arProperty["NAME"] ?>: 
            <b>
               <?$rsUser = CUser::GetByID($arProperty['DISPLAY_VALUE']);
               $arUser = $rsUser->Fetch();
               ?><?= $arUser["LAST_NAME"] . " " . $arUser['NAME']; ?>
            </b>
         </p>
      </div>
      <?endif;?>
      <?endforeach;?>

      <div class="desc">
         <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
         <img border="0" src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arResult["NAME"] ?>"  title="<?= $arResult["NAME"] ?>" align="right"/>
         <?endif?>
         <p class="desc">
            <b class="def" itemprop="term">
               <?= $arResult["NAME"] ?>
            </b> 
            <i class="eng">(����. Parallel operation of power systems)</i> �	
            <span itemprop="definition">
               <?if($arResult["NAV_RESULT"]):?>
               <?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
               <?= strip_tags($arResult["DETAIL_TEXT"]); ?>
               <?else:?>
               <?= strip_tags($arResult["PREVIEW_TEXT"]); ?>
               <?endif?>
            </span> 
         </p>
      </div>

      <?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
      <?if ($pid == "SOURCE"):?>
      <div class="source">
         <p class="source" >
            <?= $arProperty["NAME"] ?>: 
            <b itemprop="source">
               <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
               <?= implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]); ?>
               <?else:?>
               <?= $arProperty["DISPLAY_VALUE"]; ?>
               <?endif?>
            </b>
         </p>
      </div>
      <?endif;?>
      <?endforeach;?>
   </div>

</div>
<!--
<div class="links">

<div class="link">
<p class="link">������� �������</p>
<ul type="circle">
<li>������������� �������� �������</li>
<li>�������� �������</li>
<li>���������� �������������</li>
<li>���������� �������������</li>
<li>�������������� ������� ������ �����������</li>
<li>�������������� ������� ������������ ������</li>
</ul>
</div>

<div class="link">
<p class="link">������ �� ����</p>
<ul type="circle">
<li>��� ����������� ������ � ���������� �� ����</li>
<li>������� ���������� �������� ���������� �� �������� �����. ��� ������ ������?</li>
<li>������ ����������� ��� ���������� ��������� �������� ������ �� ��������</li>
</ul>
</div>

<div class="link">
<p class="link">������� �� ����</p>
<ul type="circle">
<li>��������� ������� �������� � �����</li>
<li>��������������, ���������� �� �������, �������� � ������� �����</li>
<li>� ��� ����������� �������� ��������� ������� �� �������� ��������</li>
<li>���������, ���������� �� ����, ������� ���������� ���������</li>
<li>����������� ��� � 1-� ��������� 2010 ���� ���������� � 2,5 ���� ������, ��� � ������� ����</li>
<li>����� �� ������� �������� � ������� ������ ������������</li>
</ul>
</div>

<div class="link">
<p class="link">������</p>
<ul type="circle">
<li>http://vizavsem.by/odnokratnaya_viza.html</li>
<li>http://www.bookryanair.com/SkySales/FRCustomSelect.aspx</li>
</ul>
</div>
</div>-->

<!--
<div class="new	s-detail">
   <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
      <img class="detail_picture" border="0" src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arResult["NAME"] ?>"  title="<?= $arResult["NAME"] ?>" />
   <?endif?>
   <?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
      <span class="news-date-time"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
   <?endif;?>
   <?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
      <h3><?= $arResult["NAME"] ?></h3>
   <?endif;?>
   <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
      <p><?= $arResult["FIELDS"]["PREVIEW_TEXT"];
               unset($arResult["FIELDS"]["PREVIEW_TEXT"]); ?></p>
   <?endif;?>
   <div style="clear:both"></div>
   <br />
   <?foreach($arResult["FIELDS"] as $code=>$value):?>
<?= GetMessage("IBLOCK_FIELD_" . $code) ?>:&nbsp;<?= $value; ?>
         <br />
   <?endforeach;?>
   <?
   if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
   {
      ?>
      <div class="news-detail-share">
         <noindex>
         <?
         $APPLICATION->IncludeComponent("bitrix:main.share", "", array(
               "HANDLERS" => $arParams["SHARE_HANDLERS"],
               "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
               "PAGE_TITLE" => $arResult["~NAME"],
               "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
               "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
               "HIDE" => $arParams["SHARE_HIDE"],
            ),
            $component,
            array("HIDE_ICONS" => "Y")
         );
         ?>
         </noindex>
      </div>
      <?
   }
   ?>
</div>
-->