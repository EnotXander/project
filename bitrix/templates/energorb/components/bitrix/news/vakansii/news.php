<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<? if (isset($_REQUEST['arrFilter_pf']['scope'])): ?>
   <div class="left_col">
      <? $_REQUEST['set_filter'] = 'Y'; ?>
      <?
      $APPLICATION->IncludeComponent("bitrix:catalog.filter", "left", Array(
          "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
          "IBLOCK_ID" => $arParams["IBLOCK_ID"],
          "FILTER_NAME" => $arParams["FILTER_NAME"],
          "FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
          "PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
          "CACHE_TYPE" => $arParams["CACHE_TYPE"],
          "CACHE_TIME" => $arParams["CACHE_TIME"],
          "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
              ), $component
      );
      ?>
      <div class="lc_block">
         <div class="left_banner_185">
            <?
            $APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
                "TYPE" => "ADDJOB",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0",
                "CACHE_NOTES" => ""
                    )
            );
            ?>
         </div>
      </div>
      <div class="lc_block">
         <div class="left_banner_185">
            <?
            $APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
                "TYPE" => "JOBFLASH",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0",
                "CACHE_NOTES" => ""
                    )
            );
            ?>
         </div>
      </div>
   </div>
   <?
   if ($_REQUEST['sort'] == 'date')
      $sort = 'ACTIVE_FROM';
   elseif ($_REQUEST['sort'] == 'salary')
      $sort = 'PROPERTY_salary';
   else
      $sort = 'SORT';
   ?>
   <?
   global ${$arParams["FILTER_NAME"]};
   ${$arParams["FILTER_NAME"]}['NAME'] = '%' . $_REQUEST['arrFilter_ff']['NAME'] . '%';
   if ((int)$_REQUEST['company'] > 0)
      ${$arParams["FILTER_NAME"]}['PROPERTY_company'] = (int)$_REQUEST['company'];
   ?>
   <div class="middle_col">
      <?
      $APPLICATION->IncludeComponent("bitrix:news.list", "list", Array(
          "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
          "IBLOCK_ID" => $arParams["IBLOCK_ID"],
          "NEWS_COUNT" => $arParams["NEWS_COUNT"],
          "SORT_BY1" => $arParams["SORT_BY1"],
          "SORT_ORDER1" => $arParams["SORT_ORDER1"],
          "SORT_BY2" => $sort,
          "SORT_ORDER2" => $arParams["SORT_ORDER2"],
          "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
          "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
          "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
          "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
          "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
          "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
          "SET_TITLE" => $arParams["SET_TITLE"],
          "SET_STATUS_404" => $arParams["SET_STATUS_404"],
          "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
          "CACHE_TYPE" => $arParams["CACHE_TYPE"],
          "CACHE_TIME" => $arParams["CACHE_TIME"],
          "CACHE_FILTER" => $arParams["CACHE_FILTER"],
          "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
          "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
          "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
          "PAGER_TITLE" => $arParams["PAGER_TITLE"],
          "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
          "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
          "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
          "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
          "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
          "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
          "DISPLAY_NAME" => "Y",
          "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
          "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
          "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
          "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
          "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
          "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
          "FILTER_NAME" => $arParams["FILTER_NAME"],
          "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
          "CHECK_DATES" => $arParams["CHECK_DATES"],
              ), $component
      );
      ?>
   </div>
<? else: ?>
   <div class="middle_col">
      <? if ($arParams["USE_RSS"] == "Y"): ?>
         <?if (method_exists($APPLICATION, 'addheadstring'))
            $APPLICATION->AddHeadString('<link rel="alternate" type="application/rss+xml" title="' . $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] . '" href="' . $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] . '" />');?>
         <a href="<?= $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] ?>" title="rss" target="_self"><img alt="RSS" src="<?= $templateFolder ?>/images/gif-light/feed-icon-16x16.gif" border="0" align="right" /></a>
      <? endif ?>
      <? if ($arParams["USE_SEARCH"] == "Y"): ?>
         <?= GetMessage("SEARCH_LABEL") ?>
         <?
         $APPLICATION->IncludeComponent("bitrix:search.form", "flat", Array(
             "PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"]
                 ), $component
         );
         ?>
         <br />
      <? endif ?>
      <div class="breadcrumbs">
         <?
         $APPLICATION->IncludeComponent("bitrix:breadcrumb", "news", array(
             "START_FROM" => "0",
             "PATH" => "",
             "SITE_ID" => "s1"
                 ), false, array()
         );
         ?>
      </div> 
      <? if ($arParams["USE_FILTER"] == "Y"): ?>
         <?
         $APPLICATION->IncludeComponent("bitrix:catalog.filter", "", Array(
             "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
             "IBLOCK_ID" => $arParams["IBLOCK_ID"],
             "FILTER_NAME" => $arParams["FILTER_NAME"],
             "FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
             "PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
             "CACHE_TYPE" => $arParams["CACHE_TYPE"],
             "CACHE_TIME" => $arParams["CACHE_TIME"],
             "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                 ), $component
         );
         ?>
         <br />
      <? endif ?>
      <?
      $APPLICATION->IncludeComponent("bitrix:news.list", "", Array(
          "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
          "IBLOCK_ID" => $arParams["IBLOCK_ID"],
          "NEWS_COUNT" => $arParams["NEWS_COUNT"],
          "SORT_BY1" => $arParams["SORT_BY1"],
          "SORT_ORDER1" => $arParams["SORT_ORDER1"],
          "SORT_BY2" => $arParams["SORT_BY2"],
          "SORT_ORDER2" => $arParams["SORT_ORDER2"],
          "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
          "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
          "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
          "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
          "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
          "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
          "SET_TITLE" => $arParams["SET_TITLE"],
          "SET_STATUS_404" => $arParams["SET_STATUS_404"],
          "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
          "CACHE_TYPE" => $arParams["CACHE_TYPE"],
          "CACHE_TIME" => $arParams["CACHE_TIME"],
          "CACHE_FILTER" => $arParams["CACHE_FILTER"],
          "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
          "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
          "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
          "PAGER_TITLE" => $arParams["PAGER_TITLE"],
          "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
          "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
          "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
          "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
          "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
          "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
          "DISPLAY_NAME" => "Y",
          "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
          "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
          "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
          "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
          "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
          "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
          "FILTER_NAME" => $arParams["FILTER_NAME"],
          "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
          "CHECK_DATES" => $arParams["CHECK_DATES"],
              ), $component
      );
      ?>
   </div>
<? endif; ?>