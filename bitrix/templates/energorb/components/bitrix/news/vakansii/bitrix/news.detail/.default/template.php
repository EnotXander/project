<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?global $USER;?>

<div class="middle_col vakansii_detail_block">
   <div class="page">
      <!-- ---------------------- -->
      <div class="subject">
         <table>
            <tr>
               <td width="35px"><img src="<?= $templateFolder ?>/images/star.png"></td>
               <td><p class="subject"><?= $arResult['NAME'] ?></p></td>
            </tr>
         </table>
      </div>
      <!-- ---------------------- -->
      <div class="info_field">
         <div class="sub_info_field">
            <table width="100%" height="100%" class="info_field">
               <tr>
                  <td class="info_field"><p class="grey">������� ��������</p></td>
                  <td class="info_field"><p class="grey">������</p></td>
                  <td class="info_field"><p class="grey">��������� ���� ������</p></td>
                  <td class="button" rowspan="2" align="right">
                     <div class="button1">
                        <a href="#">
                           <table width="100%" height="100%">
                              <tr>
                                 <td style="vertical-align: middle; text-align: center">
                                    <?if ($USER->IsAuthorized()):?>
                                       <a href="/mes/?MID=0&FID=1&UID=<?= $arResult['DISPLAY_PROPERTIES']['AUTHOR']['DISPLAY_VALUE'] ?>&mode=new" target="_blank"><p class="button">������������ �� ��������</p></a>
                                    <?else:?>
                                       <a href="#" onclick="alert('���������� ��������������'); return false;"><p class="button">������������ �� ��������</p></a>
                                    <?endif;?>
                                 </td>
                              </tr>
                           </table>
                        </a>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td class="info_field"><p class="bold"><? echo $arResult['DISPLAY_PROPERTIES']['salary']['DISPLAY_VALUE']; ?></p></td>
                  <td class="info_field"><p class="bold"><? echo strip_tags($arResult['DISPLAY_PROPERTIES']['region']['DISPLAY_VALUE']); ?></td>
                  <td class="info_field"><p class="bold"><? echo $arResult['DISPLAY_PROPERTIES']['experience']['DISPLAY_VALUE']; ?></td>
               </tr>
            </table>
         </div>
      </div>
      <!-- ---------------------- -->
      <div class="info_main">
         <table width="100%">
            <tr>
               <td valign="top">
                  <?= $arResult['DETAIL_TEXT'] ?>
                  <div class="up_button2">
                     <div class="button2">
                        <a href="#">
                           <table width="100%" height="100%">
                              <tr>
                                 <td style="vertical-align: middle; text-align: center">
                                    <?if ($USER->IsAuthorized()):?>
                                       <a href="/mes/?MID=0&FID=1&UID=<?= $arResult['DISPLAY_PROPERTIES']['AUTHOR']['DISPLAY_VALUE'] ?>&mode=new" target="_blank"><p class="button">������������ �� ��������</p></a>
                                    <?else:?>
                                       <a href="#" onclick="alert('���������� ��������������'); return false;"><p class="button">������������ �� ��������</p></a>
                                    <?endif;?>
                                 </td>
                              </tr>
                           </table>
                        </a>
                     </div>
                  </div>
               </td>
               <td width="305px">
                  <div class="logo">
                     <?=$arResult["COMPANY"]['PREVIEW_PICTURE_FORMATED']?>
                     <p class="grey">���� ���������� ��������  <? echo $arResult["DISPLAY_ACTIVE_FROM"] ?></p>
                  </div>
                  <div class="contact">
                     <div class="sub_contact">
                        <div class="address">
                           <table width="100%">
                              <tr>
                                 <td width="18px" class="image"><img class="image" src="<?= $templateFolder ?>/images/gps.png"></td>
                                 <td><p><?= $arResult["COMPANY"]['PROPERTIES']['index']['VALUE'] ?>, <?= $arResult["REGION"]['NAME'] ?>, <?= $arResult["CITY"]['NAME'] ?>,<br> <?= $arResult["COMPANY"]['PROPERTIES']['adress']['VALUE'] ?></p></td>
                              </tr>
                              <tr>
                                 <td><!--<a href="#">����� �������</a>--></td>
                              </tr>
                           </table>
                        </div>
                        <div class="number">
                           <table width="100%">
                              <tr>
                                 <td rowspan="2" width="18px" class="image"><img class="image" src="<?= $templateFolder ?>/images/phone.png"></td>
                                 <td>
                                    <p><?= $arResult["COMPANY"]['PROPERTIES']['phone']['VALUE'] ?></p>
                                 </td>
                              </tr>
                           </table>
                        </div>
                        <div class="time">
                           <table width="100%">
                              <tr>
                                 <td width="18px"><img class="image" src="<?= $templateFolder ?>/images/time.png"></td>
                                 <td><p><?= $arResult["COMPANY"]['PROPERTIES']['shedule']['VALUE'] ?></p></td>
                              </tr>
                           </table>
                        </div>
                        <div class="map">
                           <p>Web-c���: <a href="http://<?= $arResult["COMPANY"]['PROPERTIES']['URL']['VALUE'] ?>"><?= $arResult["COMPANY"]['PROPERTIES']['URL']['VALUE'] ?></a></p>
                           <?
                           //echo "<pre>"; print_r($arResult['PROPERTIES']['MAP']); echo "</pre>";
                           $coord = explode(",", $arResult["COMPANY"]['PROPERTIES']['MAP']['VALUE']);
                           if (count($coord) > 1):
                              $arData = Array("yandex_lat" => $coord[0], "yandex_lon" => $coord[1], "yandex_scale" => 16, "PLACEMARKS" => Array(Array("LON" => $coord[1], "LAT" => $coord[0], "TEXT" => $arResult["COMPANY"]['NAME'])));?>
                              <?
                              $APPLICATION->IncludeComponent("bitrix:map.yandex.view", ".default", array(
                                  "KEY" => "AETqvU8BAAAApI_7dQMAlTDGwZ3hOJGhdVSFYEsVNSpuzTQAAAAAAAAAAACCDIEExtPcFpiKYkYYJlaNR8uKKw==",
                                  "INIT_MAP_TYPE" => "MAP",
                                  "MAP_DATA" => serialize($arData),
                                  "MAP_WIDTH" => "275",
                                  "MAP_HEIGHT" => "209",
                                  "CONTROLS" => array(
                                  ),
                                  "OPTIONS" => array(
                                      0 => "ENABLE_SCROLL_ZOOM",
                                      1 => "ENABLE_DRAGGING",
                                  ),
                                  "MAP_ID" => ""
                                      ), false
                              );
                              ?>
                           <? endif; ?>
                           <div class="zoom">
                              <a class="zoom" href="#">
                                 <table width="100%">
                                    <tr>
                                       <td style="text-align: right" width="35%"><img src="<?= $templateFolder ?>/images/zoom.png"></td>
                                       <td style="text-align: left" width="65%">��������� �����</td>
                                    </tr>
                                 </table>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
         </table>
      </div>
   </div>
</div>
