<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="page">
   <!-- ---------------------- -->
   <div class="top">
      <table width="100%">
         <tr>
            <td width="220px">
               <div class="tabs">
                  <table width="100%" height="100%">
                     <tr>
                        <td width="110px" style="vertical-align: middle; text-align: center"><a class="tab" href="/job/"><p class="tab_red">��������</p></a></td>
                        <td width="110px" style="vertical-align: middle; text-align: center"><a class="tab" href="/resume/"><p class="tab_white">������</p></a></td>
                     </tr>
                  </table>
               </div>
            </td>
            <td style="vertical-align: middle">
               <table width="100%" height="100%">
                  <tr>
                     <td width="20px"><img src="<?= $templateFolder ?>/images/add.png"></td>
                     <td style="vertical-align: middle"><p class="add"><a class="add" href="/job/my/?edit=Y">���������� ��������</a> ��� <a class="add" href="/resume/my/?edit=Y">������</a> � ��� ������ � ���������</p></td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </div>
   <!-- ---------------------- -->
   <div class="search">
      <table width="100%" height="100%">
         <tr>
            <td style="vertical-align: middle; text-align: center">
               <form method="get" name="v_f" style="padding-left: 10px; padding-top: 8px;">
                  <input type="hidden" name="sort">
                  <table width="100%" height="100%" >
                     <tr>
                        <td width="180px">
                           <input class="search" type="search" name="arrFilter_ff[NAME]" value="<?= $_REQUEST['arrFilter_ff']['NAME'] ?>">
                        </td>
                        <td width="170px">
                           <select class="search_type_1" name="arrFilter_pf[scope]">
                              <option value="">��� ���������������� �������</option>
                              <? foreach ($arResult["SFER"] as $sfer): ?>
                                 <option value="<?= $sfer['ID'] ?>" <? if ($_REQUEST['arrFilter_pf']['scope'] == $sfer['ID']): $sferName = $sfer['NAME']; ?>selected<? endif; ?>><?= $sfer['NAME'] ?></option>
                              <? endforeach; ?>
                           </select>
                        </td>
                        <!--<td width="160px">
                           <select name='arrFilter_pf[specialty]' class='search_type_2'>
                              <option value=''>��� �������������</option>
                              <?foreach($arResult["SPECS"] as $arItem):?>
                                 <option value='<?=$arItem['ID']?>'<?=$arItem['SELECTED'] ? ' selected' : ''?>><?=$arItem['NAME']?></option>
                              <?endforeach;?>
                           </select>
                        </td>-->
                        <td width="160px">
                           <select name='arrFilter_pf[region]' class='search_type_3'>
                              <option value=''>��� �������</option>
                              <?foreach($arResult["REGS"] as $arItem):?>
                                 <option value='<?=$arItem['ID']?>'<?=$arItem['SELECTED'] ? ' selected' : ''?>><?=$arItem['NAME']?></option>
                              <?endforeach;?>
                           </select>
                        </td>
                        <td>
                           <a class="search_button" href="#" onclick="document.v_f.submit();">
                              <div class="search_button">
                                 <table width="100%" height="100%">
                                    <tr>
                                       <td style="vertical-align: middle; text-align: center"><p class="search_button">�����</p></td>
                                    </tr>
                                 </table>
                              </div>
                           </a>
                        </td>
                     </tr>
                  </table>
               </form>
            </td>
         </tr>
      </table>
   </div>
   <!-- ---------------------- -->

   <div class="found">
      <table>
         <tr>
            <td width="70px" style="vertical-align: middle; text-align: center"><a class="back" href="#">
                  <div class="back">
                     <table width="100%" height="100%">
                        <tr>
                           <td style="vertical-align: middle; text-align: center"><p class="back">�����</p></td>
                        </tr>
                     </table>
                  </div>
               </a>
            </td>
            <td><p class="found">������� <?= count($arResult['ITEMS']) ?> �������� � <?= $sferName ?></p></td>
            <td width="30px" style="vertical-align: middle; text-align: center">
               <a href="/job/rss/">
                  <img src="<?= $templateFolder ?>/images/rss.png" align="center">
               </a>
            </td>
         </tr>
      </table>
   </div>
   <!-- ---------------------- -->
   <div class="sort">
      <table height="100%">
         <tr>
            <td width="90px" style="vertical-align: middle; text-align: center"><p class="sort">�����������:</p></td>
            <td width="120px" style="vertical-align: middle; text-align: center"><a class="sort" href="<?= $APPLICATION->GetCurPageParam('', Array('sort')); ?>"><p class="<? if (strlen($_REQUEST['sort']) <= 0): ?>highlight<? else: ?>unlight<? endif; ?>"><? if (strlen($_REQUEST['sort']) > 0): ?><b class="dashed">�� �������������</b><? else: ?>�� �������������<? endif; ?></p></a></td>
            <td width="85px" style="vertical-align: middle; text-align: center"><a class="sort" href="<?= $APPLICATION->GetCurPageParam('sort=salary', Array('sort')); ?>"><p class="<? if ($_REQUEST['sort'] == 'salary'): ?>highlight<? else: ?>unlight<? endif; ?>"><? if ($_REQUEST['sort'] == 'salary'): ?>�� ��������<? else: ?><b class="dashed">�� ��������</b><? endif; ?></p></a></td>
            <td width="55px" style="vertical-align: middle; text-align: center"><a class="sort" href="<?= $APPLICATION->GetCurPageParam('sort=date', Array('sort')); ?>"><p class="<? if ($_REQUEST['sort'] == 'date'): ?>highlight<? else: ?>unlight<? endif; ?>"><? if ($_REQUEST['sort'] == 'date'): ?>�� ����<? else: ?><b class="dashed">�� ����</b><? endif; ?></p></a></td>
         </tr>
      </table>
   </div>
   <!-- ---------------------- -->
   <? foreach ($arResult["ITEMS"] as $arItem): ?>
      <div class="vacancy<? if (strlen($arItem['PROPERTIES']['PREMIUM']['VALUE']) > 0): ?>_premium<? else: ?>_main<? endif; ?>">
         <table width="100%" height="100%">
            <tr>
               <td width="25px" rowspan="2"  style="vertical-align: top; text-align: center">
                  <a href="#"><img src="<?= $templateFolder ?>/images/star.png" class="star"></a>
               </td>
               <td width="560px">
                  <a class="vacancy_name" href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><p class="vacancy_name"><? echo $arItem["NAME"] ?></p></a>
               </td>
               <td align="center">
                  <p class="salary"><? echo $arItem['DISPLAY_PROPERTIES']['salary']['DISPLAY_VALUE']; ?></p>
               </td>
            </tr>
            <tr>
               <td colspan="2">
                  <p class="grey"><b class="grey"><? echo strip_tags($arItem['DISPLAY_PROPERTIES']['company']['DISPLAY_VALUE']); ?></b>, <? echo $arItem["DISPLAY_ACTIVE_FROM"] ?>, <? echo strip_tags($arItem['DISPLAY_PROPERTIES']['region']['DISPLAY_VALUE']); ?></p>
               </td>
            </tr>
         </table>
      </div>
   <? endforeach; ?>
   <!-- ---------------------- -->
</div>