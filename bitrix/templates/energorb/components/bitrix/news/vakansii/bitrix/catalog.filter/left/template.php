<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<ul>
   <? foreach ($arResult["SFER"] as $sfer): ?>
      <li>
         <a class="list" href="?arrFilter_pf[scope]=<?= $sfer['ID'] ?>"><?= $sfer['NAME'] ?></a>
      </li>
   <? endforeach; ?>
</ul>
