<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock")) die();

$arSfer = Array();
$items = GetIBlockElementList(40, 0, Array("SORT" => "ASC", "NAME" => "ASC"));
while ($arItem = $items->GetNext())
{
   $arResult["SFER"][] = $arItem;
}