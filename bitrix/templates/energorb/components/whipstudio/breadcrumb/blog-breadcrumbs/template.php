<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul class="breadcrumb-navigation">
	<?foreach ($arResult as $i=>$nav):?>
		<?if ($nav['LINK']):?>
			<li>
				<?if ($i>0):?><span>&nbsp;&nbsp;\&nbsp;&nbsp;</span><?endif;?>
				<a href="<?=$nav['LINK']?>"><?=$nav['TITLE']?></a>
			</li>
		<?endif;?>
	<?endforeach;?>
</ul>
