<? require( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function chooseImage(cnt)
            {
                $('#redactor_image_url').val('IMAGE' + $('#redactor_image_id_' + cnt).val());
            }
            $(document).ready(function()
            {
                chooseImage(0);
            });
        </script>
    </head>
    <body>
        <div>
            <?// Отрисовка прикрепленных изображений
            if(CModule::IncludeModule("iblock"))
            {
                $resArticle = CIBlockElement::GetByID($_GET["articleId"]);
                if($obResArticle = $resArticle->GetNextElement())
                {
                    $arArticleProperties = $obResArticle->GetProperty("PHOTOS");
                    //PrintObject($arArticleProperties);
                    //print_r($arArticleProperties);
                    $imageMax = count($arArticleProperties["VALUE"],0);
                    $arImage = $arArticleProperties["VALUE"];
                    //$arDescriptions = $arProperties["DESCRIPTION"];
                    //PrintObject($arLink);
                }
            }
            else
            {
                echo "NO!";
            }
            ?>
            
            <ol style="padding: 0px">
                <?for($imageCnt = 0; $imageCnt < $imageMax; $imageCnt++):?>
                    <?$resImage = CIBlockElement::GetByID($arImage[$imageCnt]);
                    if($arImageProperties = $resImage->GetNext()):?>
                        <?$picture = CFile::GetFileArray($arImageProperties["DETAIL_PICTURE"]);
                        //print_r($arImageProperties);?>
                        <li>
                            <input type="radio" <?= $imageCnt==0 ? 'checked' : ''?> value="<?= $imageCnt?>" name="redactor_image_url_radio" id="redactor_image_url_radio_<?= $imageCnt?>" onclick="chooseImage(<?= $imageCnt?>)">
                            <img alt="<?=$picture["DESCRIPTION"]?>" src="<?=Resizer("/thumb/50x50xprop".$picture["SRC"])?>" />
                            <span name="redactor_image_url_<?= $imageCnt?>" id="redactor_image_url_<?= $imageCnt?>"><?= $arImageProperties["NAME"]?></span>
                            <input type="hidden" value="<?= $arImage[$imageCnt]?>" name="redactor_image_id" id="redactor_image_id_<?= $imageCnt?>">
                        </li>
                    <?endif;?>
                <?endfor;?>
            </ol>
            <input type="hidden" value="unset" name="#redactor_image_url" id="redactor_image_url">
            <input type="text" value="видел на этом сайте" name="#redactor_image_text" id="redactor_image_text">
            <input type="button" name="" id="" value="%RLANG.insert%" onclick="redactorActive.insertImageCode();" />&nbsp;&nbsp;
            <input type="button" name="" onclick="redactorActive.modalClose();" value="%RLANG.cancel%"  /> 
        </div>
    </body>
</html>
