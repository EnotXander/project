// JavaScript Document

(function( $ ) {
    $(function() {
        $('.banner-enlarge')
            .mouseenter(function(){
                $('.banner-enlarge-b').animate({
                    'top':'0'
                }, 300)
            });
        $('.banner-enlarge-b').mouseleave(function(){
                $('.banner-enlarge-b').css({
                    'top':'-180px'
                });
            });
    });
})(jQuery);