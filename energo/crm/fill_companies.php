<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

global $DB;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
$res = $objElement->GetList(
        array("ID" => "DESC"),
        array("IBLOCK_ID" => 17),
        false,
        false,
        array("ID", "NAME", "PROPERTY_S_ID", "PROPERTY_S_RS", "PROPERTY_S_URADRESS",
            "PROPERTY_UNN", "PROPERTY_TARIF", "PROPERTY_TARIF_DATE_START", "PROPERTY_TARIF_DATE_STOP")
);
$www = 100000;

//echo $res->SelectedRowsCount(); die();

while ($arItem = $res->GetNext()) {
   if(!$www) break;
   $www--;
   /*$query = "INSERT INTO  `a_crm_out` (  `COMPANY_ID` ,  `TARIF_ID` ,  `TARIF_DATE_START` ,  `TARIF_DATE_STOP` ) 
            VALUES (
               '{$DB->ForSql($arItem['ID'])}', 
               '{$DB->ForSql($arItem['PROPERTY_TARIF_VALUE'])}',
               '{$DB->ForSql(BXToTimestamp($arItem['PROPERTY_TARIF_DATE_START_VALUE']))}' , 
               '{$DB->ForSql(BXToTimestamp($arItem['PROPERTY_TARIF_DATE_STOP_VALUE']))}'
            );";*/
   //$timeStart = BXToTimestamp($arItem['PROPERTY_TARIF_DATE_START_VALUE']);
   //$timeStop = BXToTimestamp($arItem['PROPERTY_TARIF_DATE_STOP_VALUE']);
   if($arItem['PROPERTY_TARIF_VALUE'])
      $query = "REPLACE INTO  `a_crm_out` (  `COMPANY_ID` ,  `TP_ID`, `BX_NAME` )
               VALUES (
                  '{$DB->ForSql($arItem['PROPERTY_S_ID_VALUE'])}',
                  '{$DB->ForSql($arItem['PROPERTY_TARIF_VALUE'])}',
                  '{$DB->ForSql($arItem['NAME'])}'
               );";
   else
      $query = "REPLACE INTO  `a_crm_out` (  `COMPANY_ID`,  `BX_NAME` )
               VALUES (
                  '{$DB->ForSql($arItem['PROPERTY_S_ID_VALUE'])}',
                                  '{$DB->ForSql($arItem['NAME'])}'
               );";
   $DB->Query($query);
}
echo 'done '.$www;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>