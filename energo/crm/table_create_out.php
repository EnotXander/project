<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");


global $DB;
$query = "CREATE TABLE `a_crm_out` (
  `COMPANY_ID` int(11) NOT NULL,
  `MANAGER_ID` int(11) DEFAULT NULL,
  `TP_ID` int(11) DEFAULT NULL,
  `DT_START` timestamp NULL DEFAULT NULL,
  `DT_STOP` timestamp NULL DEFAULT NULL,
  `LIQUIDATED` int(11) DEFAULT NULL,
  `CRM_NAME` varchar(100) DEFAULT NULL,
  `BX_NAME` varchar(100) DEFAULT NULL,
  `RS` varchar(400) DEFAULT NULL,
  `UNP` varchar(15) DEFAULT NULL,
  `URADRESS` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`COMPANY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
";
$DB->Query($query);

PrintAdmin("TABLE CREATED!");
           

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>