<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");


global $DB;
$query = "CREATE TABLE IF NOT EXISTS `a_crm_stat_agro` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DIRECTION` varchar(128) NOT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `COUNT` int(11) DEFAULT NULL,
  `COMPANY_ID` int(11) DEFAULT NULL,
  `FIELDS_INFO` longtext,
  PRIMARY KEY (`ID`)
) ENGINE = INNODB;";
$DB->Query($query);

PrintAdmin("TABLE CREATED!");


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
