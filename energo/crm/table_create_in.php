<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use \Glifery\Crm\Logger;

class CrmTableGenerator
{
    const ERROR_EMPTY_QUERY = 'ERROR! empty query!';

    private $db;

    private $query;

    private $status;

    /** @var Logger */
    private $logger;

    public function __construct()
    {
        global $DB;

        $this->db = $DB;
        $this->query = array();
        $this->status = array();
        $this->logger = new Logger();
    }

    public function addQuery($name, $query)
    {
        $this->query[$name] = $query;

        return $this;
    }

    public function execute()
    {
        if (count($this->query)) {
            foreach ($this->query as $name => $query) {
                try {
                    $result = $this->db->Query($query);
                    $rowsAmount = $result->AffectedRowsCount();
                    $this->status[] = $name.': '.$rowsAmount.' strings';

                    if ($rowsAmount && $name == 'insert') {
                        $message = Logger::createMessage('in');
                        $message->setCount($rowsAmount);
                        $this->logger->log($message);
                    }
                } catch (Exception $e) {
                    $this->status[] = 'Exception: '.$e->getMessage();
                }
            }
        } else {
            $this->status[] = self::ERROR_EMPTY_QUERY;
        }

        return $this;
    }

    public function getStatus()
    {
        return implode('<br>', $this->status);
    }
}

$queryTruncate = "TRUNCATE TABLE `a_crm_in`";

$queryInsert =
   "INSERT INTO `a_crm_in`
      (SELECT
               `outtable`.`ID` as `COMPANY_ID`,
               `outtable`.`NAME` as `COMPANY_NAME`,
               CONCAT_WS(
                  ', ',
                  `t_index`.`VALUE`,
                  (
                     SELECT
                        `NAME`
                     FROM
                        `b_iblock_element` as `el_country`
                     WHERE
                        `el_country`.`ID` = `t_country`.`VALUE`
                  ),
                  `t_address`.`VALUE`
               ) as `COMPANY_ADDRESS`,
               `t_employee`.`PROPERTY_319` as `REPRESENTATIVE_ID`,
               CONCAT_WS(
                  ' ',
                  `t_employee_info`.`NAME`,
                  `t_employee_info`.`LAST_NAME`
               )as `REPRESENTATIVE_NAME`,
               `t_employee_info`.`EMAIL` as `REPRESENTATIVE_EMAIL`,
               (
                  SELECT
                     GROUP_CONCAT(
                        IF(t_site.`DESCRIPTION` = '', t_site.`VALUE`, concat(t_site.`VALUE`, ' - ', t_site.`DESCRIPTION`))
                        SEPARATOR '; '
                     )
                  FROM
                     `b_iblock_element_property` as `t_site`
                  WHERE
                     `outtable`.`ID` = `t_site`.`IBLOCK_ELEMENT_ID` AND `t_site`.`IBLOCK_PROPERTY_ID` = 69
               ) as `COMPANY_SITE`,
               `t_name_en`.`VALUE` as `COMPANY_NAME_EN`,
               `t_rs`.`VALUE` as `RS`,
               `t_unp`.`VALUE` as `UNP`,

               `t_srs`.`VALUE` as `S_RS`,
               `t_sadres`.`VALUE` as `S_URADRESS`


            FROM
               `b_iblock_element` as `outtable`
            LEFT JOIN `b_iblock_element_property` as `t_index`
               ON `outtable`.`ID` = `t_index`.`IBLOCK_ELEMENT_ID`
               AND `t_index`.`IBLOCK_PROPERTY_ID` = 195

            LEFT JOIN `b_iblock_element_property` as `t_predstov2`
               ON `outtable`.`ID` = `t_predstov2`.`IBLOCK_ELEMENT_ID`
               AND `t_predstov2`.`IBLOCK_PROPERTY_ID` = 305

            LEFT JOIN `b_iblock_element_property` as `t_country`
               ON `outtable`.`ID` = `t_country`.`IBLOCK_ELEMENT_ID`
               AND `t_country`.`IBLOCK_PROPERTY_ID` = 123

            LEFT JOIN `b_iblock_element_property` as `t_address`
               ON `outtable`.`ID` = `t_address`.`IBLOCK_ELEMENT_ID`
               AND `t_address`.`IBLOCK_PROPERTY_ID` = 99

            LEFT JOIN `b_iblock_element_prop_s49` as `t_employee`
               ON `outtable`.`ID` = `t_employee`.`PROPERTY_318`
               AND `t_employee`.`PROPERTY_322` = 65
               AND `t_employee`.`PROPERTY_339` = 71

            LEFT JOIN `b_user` as `t_employee_info`
               ON `t_employee`.`PROPERTY_319` = `t_employee_info`.`ID`
               AND `t_employee`.`PROPERTY_319` IS NOT NULL

            LEFT JOIN `b_iblock_element_property` as `t_name_en`
               ON `outtable`.`ID` = `t_name_en`.`IBLOCK_ELEMENT_ID`
               AND `t_name_en`.`IBLOCK_PROPERTY_ID` = 327

            LEFT JOIN `b_iblock_element_property` as `t_rs`
               ON `outtable`.`ID` = `t_rs`.`IBLOCK_ELEMENT_ID`
               AND `t_rs`.`IBLOCK_PROPERTY_ID` = 324

            LEFT JOIN `b_iblock_element_property` as `t_unp`
               ON `outtable`.`ID` = `t_unp`.`IBLOCK_ELEMENT_ID`
               AND `t_unp`.`IBLOCK_PROPERTY_ID` = 326


            LEFT JOIN `b_iblock_element_property` as `t_srs`
               ON `outtable`.`ID` = `t_srs`.`IBLOCK_ELEMENT_ID`
               AND `t_srs`.`IBLOCK_PROPERTY_ID` = 782


            LEFT JOIN `b_iblock_element_property` as `t_sadres`
               ON `outtable`.`ID` = `t_sadres`.`IBLOCK_ELEMENT_ID`
               AND `t_sadres`.`IBLOCK_PROPERTY_ID` = 783

            WHERE
               `outtable`.`IBLOCK_ID` =17
               and (`t_employee`.`PROPERTY_319` = `t_predstov2`.`VALUE`  or `t_employee`.`PROPERTY_319` is null)
               AND `outtable`.`WF_PARENT_ELEMENT_ID` IS NULL
            ORDER by `outtable`.`ID` desc)";

$creator = new CrmTableGenerator();
$status = $creator
    ->addQuery('truncate', $queryTruncate)
    ->addQuery('insert', $queryInsert)
    ->execute()
    ->getStatus()
;

PrintObject($status);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
