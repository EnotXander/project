<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use \Glifery\Crm\Logger;
$logger = new Logger();

global $DB;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();

//�������� �������� � ��������
$res = $objElement->GetList(
        array(),
        array("IBLOCK_ID" => 17),
        false,
        false,
        array("PROPERTY_S_ID",
              "ID",
              "PROPERTY_TARIF",
              "PROPERTY_TARIF_DATE_START",
              "PROPERTY_TARIF_DATE_STOP",
              "PROPERTY_PREMIUM",
              "PROPERTY_PREMIUM_DATE_START",
              "PROPERTY_PREMIUM_DATE_STOP",
              "PROPERTY_MANAGER",
              "PROPERTY_LIQUIDATED"
         )
);
$arCompanyBX = array();
while ($arItem = $res->GetNext()) { // �������� ������
	$id = $arItem["PROPERTY_S_ID_VALUE"]> 0 ? $arItem["PROPERTY_S_ID_VALUE"] : $arItem["ID"];
   $arCompanyBX[$id] = $arItem;
}

//�������� �������� �� CRM
$arCompanyCRM = array();
$query = "SELECT * FROM  `a_crm_out`";
$res = $DB->Query($query);
while($arQuery = $res->fetch())
{
   $arCompanyCRM[$arQuery["COMPANY_ID"]] = $arQuery;
}

//������ ���������� � ��������
$arStatus = array();
foreach($arCompanyCRM as $k=>$companyCRM)
{
   $companyBX = $arCompanyBX[$k];
   //  ������ ID �������� ��� ��� �������� �� ����� �� ���� S_ID  $companyBXId
   //  $oCOM = CIBlockElement::GetList([],['PROPERTY_S_ID' => $companyCRM['COMPANY_ID']]);
   //  $COM = $oCOM->Fetch();
   $COM['ID'] = $companyBX['ID'];
   $companyBXId = $COM['ID'];
   //$companyBX = $arCompanyCRM[$companyBXId];

   $status = array("ID" => $COM['ID']);


   if(isset($arCompanyBX[$companyBXId]))
   {
      $status["FOUND"] = true;
      $arSaveProps = array();

      //predstavitel
      if($companyBX["PROPERTY_MANAGER_VALUE"] != $companyCRM["MANAGER_ID"])
      {
         //$status["PREDSTAVITEL"] = PredstavitelSetStatus($companyCRM["COMPANY_ID"], $companyCRM["MANAGER_ID"], 65);
         $arSaveProps["MANAGER"] = $companyCRM["MANAGER_ID"];
         if(!$arSaveProps["MANAGER"]) $arSaveProps["MANAGER"] = false;
      }
      //tarif
      if($companyBX["PROPERTY_TARIF_VALUE"] != $companyCRM["TP_ID"])
      {
         $arSaveProps["TARIF"] = $companyCRM["TP_ID"];
         if(!$arSaveProps["TARIF"]) $arSaveProps["TARIF"] = false;
      }
      //date_start
      if($companyCRM["DT_START"] !== null)
      {
         if(BXToTimestamp($companyBX["PROPERTY_TARIF_DATE_START_VALUE"]) != DBToTimestamp($companyCRM["DT_START"]))
            $arSaveProps["TARIF_DATE_START"] = DBToBX($companyCRM["DT_START"]);
      }
      else
      {
         if($companyBX["PROPERTY_TARIF_DATE_START_VALUE"] !== null)
            $arSaveProps["TARIF_DATE_START"] = "";
      }
      //date_stop
      if($companyCRM["DT_STOP"] !== null)
      {
         if(BXToTimestamp($companyBX["PROPERTY_TARIF_DATE_STOP_VALUE"]) != DBToTimestamp($companyCRM["DT_STOP"]))
            $arSaveProps["TARIF_DATE_STOP"] = DBToBX($companyCRM["DT_STOP"]);
      }
      else {
         if ( $companyBX["PROPERTY_TARIF_DATE_STOP_VALUE"] !== null ) {
            $arSaveProps["TARIF_DATE_STOP"] = "";
         }
      }


      $companyCRM["PR_ID"] = false;
      //PREMIUM date_start
      if($companyCRM["PR_START"] !== null)
      {
         if(BXToTimestamp($companyBX["PROPERTY_PREMIUM_DATE_START_VALUE"]) != DBToTimestamp($companyCRM["PR_START"]))
            $arSaveProps["PREMIUM_DATE_START"] = DBToBX($companyCRM["PR_START"]);
         $companyCRM["PR_ID"] = 7;
      } else  {
         if($companyBX["PROPERTY_PREMIUM_DATE_START_VALUE"] !== null)
            $arSaveProps["PREMIUM_DATE_START"] = "";
      }
      //PREMIUM date_stop
      if($companyCRM["PR_STOP"] !== null)
      {
         if(BXToTimestamp($companyBX["PROPERTY_PREMIUM_DATE_STOP_VALUE"]) != DBToTimestamp($companyCRM["PR_STOP"]))
            $arSaveProps["PREMIUM_DATE_STOP"] = DBToBX($companyCRM["PR_STOP"]);

         $companyCRM["PR_ID"] = 7;
      } else  {
         if($companyBX["PROPERTY_PREMIUM_DATE_STOP_VALUE"] !== null)
            $arSaveProps["PREMIUM_DATE_STOP"] = "";
      }

      //PREMIUM
      if($companyBX["PROPERTY_PREMIUM_VALUE"] != $companyCRM["PR_ID"])
      {
         $arSaveProps["PREMIUM"] = $companyCRM["PR_ID"];
         if(!$arSaveProps["PREMIUM"]) $arSaveProps["PREMIUM"] = false;
      }

      //LIQUIDATED

      $LQ = ($companyCRM["LIQUIDATED"] == '1' ? '��' : '' );

      if($companyBX["PROPERTY_LIQUIDATED_VALUE"] != $LQ)
      {
         $arSaveProps["LIQUIDATED"] = ($LQ == '��' ? '2092' : '');
      }
      if ($companyCRM["COMPANY_ID"] == '215307' || $companyCRM["COMPANY_ID"] == '215307' ) {
         print_r($arSaveProps); print_r($companyCRM); print_r($companyBX);
      }
      //����������
      if(count($arSaveProps))
      {
          $message = Logger::createMessage('out');
          $message->setCompanyId($companyBXId);
          foreach($arSaveProps as $field => $value) {
            $message->addUpdatedField($field, $value);
          }
          $logger->log($message);

          $status["UPDATED"] = true;
          $status["FIELDS"] = $arSaveProps;

          CIBlockElement::SetPropertyValuesEx($companyBXId, 17, $arSaveProps);
      } else {
         $status["UPDATED"] = false;
      }

   }
   else
      $status["FOUND"] = false;
   $arStatus[$companyBXId] = $status;
}
//PrintAdmin($arStatus);

// �������� ������ ����������
$rsAllElements = CIBlockElement::GetList(
	array(  ),
	array(
		"IBLOCK_ID" => 17, //"ID"=>28552 27
	), false, false, array('ID','ACTIVE','IBLOCK_ID', 'NAME')
);

$el = new CIBlockElement;
while ( $arItem = $rsAllElements->GetNext() ) {

   $t1 = getElementPropertyValue($arItem['IBLOCK_ID'],$arItem['ID'] ,'TARIF');
   $p2 = itemFieldsValues( $t1, 47, 'SEARCH_SORT' );

   $p1 = getElementPropertyValue($arItem['IBLOCK_ID'],$arItem['ID'] ,'PREMIUM'); // ��� ������� ���� ���� ��� 7 ��� ������
   $p3 = getElementPropertyValue($arItem['IBLOCK_ID'],$arItem['ID'] ,'rating');// ��� ����� �� ���� � ��� �������
  // ��� ����� �� ���� � ��� �������
   $key = $p1.'-'.$p2['VALUE'].'-'.$p3;

   CIBlockElement::SetPropertyValues($arItem['ID'], $arItem['IBLOCK_ID'], $key, 'EXT_SORT');
}

echo 'end';

function getElementPropertyValue($iblock, $itemid, $code){
	$res = CIBlockElement::GetProperty($iblock, $itemid, "sort", "asc", array("CODE" => $code));
    if ($ob = $res->Fetch())
    {
        return $ob['VALUE'];
    }
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>