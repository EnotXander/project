<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");


global $DB;
$query = "CREATE TABLE `a_crm_out_agro` (
  `COMPANY_ID` int(11) NOT NULL,
  `MANAGER_ID` int(11) DEFAULT NULL,
  `TP_ID` int(11) DEFAULT NULL,
  `DT_START` timestamp NULL DEFAULT NULL,
  `DT_STOP` timestamp NULL DEFAULT NULL,
  `LIQUIDATED` int(11) DEFAULT NULL,
  `CRM_NAME` varchar(100) DEFAULT NULL,
  `BX_NAME` varchar(100) DEFAULT NULL,
  `PR_ID` int(11) DEFAULT NULL,
  `PR_START` timestamp NULL DEFAULT NULL,
  `PR_STOP` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`COMPANY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

";

PrintAdmin("TABLE CREATED!");
global $DB;
$query = "CREATE TABLE `a_crm_in_agro` (
  `COMPANY_ID` int(11) NOT NULL DEFAULT '0',
  `COMPANY_NAME` varchar(255) NOT NULL,
  `COMPANY_ADDRESS` longtext,
  `REPRESENTATIVE_ID` text,
  `REPRESENTATIVE_NAME` varchar(101) DEFAULT NULL,
  `REPRESENTATIVE_EMAIL` varchar(255) DEFAULT NULL,
  `COMPANY_SITE` text,
  `COMPANY_NAME_EN` text,
  `RS` text,
  `UNP` text,
  `S_RS` varchar(400) DEFAULT NULL,
  `S_URADRES` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

";
$DB->Query($query);

PrintAdmin("TABLE CREATED!");
           

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
