<?

define('HIDE_LEFT', substr_count($_SERVER['REQUEST_URI'], '/') >= 4 );// ���� 3 ���� �� �������

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//$APPLICATION->SetPageProperty("description", "������� �������� ������� � ����� ����������. ����������, ��������, ���������, �����-�����, ��������.");
$APPLICATION->SetTitle("������� ��������, ����, ����������� � ����� ����������");
?>
<?

if(isset($_REQUEST["company_id"])) // �������� �� ������ �������
{
   $companyID = (int)$_REQUEST["company_id"];
   
   if($companyID > 0)
   {
      if(!CModule::IncludeModule("iblock")) die();
      $objElement = new CIBlockElement();
      
      $res = $objElement->GetList(array(), array("IBLOCK_ID" => 17, "ACTIVE" => "Y", "EXTERNAL_ID" => $companyID));
      if($element = $res->GetNext()) // ���������� �� ������ � ����� ������
         LocalRedirect($element["DETAIL_PAGE_URL"], false, "301 Moved Permanently");
      else // ������ ����� �� ����������
         LocalRedirect("/404.php", false, "404 Not found");
   }
}

$iblockID = IBLOCK_COMPANY;
if (isset($_REQUEST["ELEMENT_ID"])) // �������� �� ������ �������
{
   if (!CModule::IncludeModule("iblock")) die();
   $objElement = new CIBlockElement();
   $res = $objElement->GetList(array(), array("ID" => $_REQUEST["ELEMENT_ID"], "IBLOCK_ID" => $iblockID));
   if ($element = $res->GetNext()) 
      LocalRedirect($element["DETAIL_PAGE_URL"], false, "301 Moved Permanently");
   else
      LocalRedirect("/404.php", false, "404 Not found");
} else if(isset($_REQUEST["SECTION_ID"]))
{
   if (!CModule::IncludeModule("iblock")) die();
   $objSection = new CIBlockSection();
   $res = $objSection->GetList(array(), array("ID" => $_REQUEST["SECTION_ID"], "IBLOCK_ID" => $iblockID));
   if ($section = $res->GetNext())
      LocalRedirect($section["SECTION_PAGE_URL"], false, "301 Moved Permanently");
   else 
      LocalRedirect("/404.php", false, "404 Not found");
}

if ((int)$_REQUEST['per_page'] > 0)
   $perPage = (int)$_REQUEST['per_page'];
else
   $perPage = 20;
?>

<?
$APPLICATION->IncludeComponent("bitrix:news","company", array(
      "IBLOCK_TYPE" => "directories",
      "IBLOCK_ID" => IBLOCK_COMPANY,
      "IBLOCK_PRODUCT_ID" => IBLOCK_PRODUCTS,
      "IBLOCK_USLUGI_ID" => IBLOCK_USLUGI,
      "NEWS_COUNT" => $perPage,
      "USE_SEARCH" => "N",
      "USE_RSS" => "N",
      "USE_RATING" => "Y",
      "MAX_VOTE" => 5,
      "VOTE_NAMES" => array(
         0 => "1",
         1 => "2",
         2 => "3",
         3 => "4",
         4 => "5",
         5 => "",
      ),
      "USE_CATEGORIES" => "N",
      "USE_REVIEW" => "N",
      "MESSAGES_PER_PAGE" => "0",
      "USE_CAPTCHA" => "N",
      "REVIEW_AJAX_POST" => "N",
      "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
      "FORUM_ID" => "10",
      "URL_TEMPLATES_READ" => "",
      "SHOW_LINK_TO_FORUM" => "N",
      "POST_FIRST_MESSAGE" => "N",
      "USE_FILTER" => "Y",
      "FILTER_NAME" => "arCFilter",
      "FILTER_FIELD_CODE" => array(),
      "FILTER_PROPERTY_CODE" => array(
         "REGION",
         "CITY",
         "TYPE_D"
      ),
      "SORT_BY1" => "PROPERTY_EXT_SORT",
      "SORT_ORDER1" => "ASC",
      "SORT_BY2" => "PROPERTY_rating",
      "SORT_ORDER2" => "DESC",
      "CHECK_DATES" => "Y",
      "SEF_MODE" => "Y",
      "SEF_FOLDER" => "/company/",
      "AJAX_MODE" => "N",
      "AJAX_OPTION_JUMP" => "N",
      "AJAX_OPTION_STYLE" => "Y",
      "AJAX_OPTION_HISTORY" => "N", 
      "CACHE_TYPE" => "A",
      "CACHE_TIME" => "3600",
      "CACHE_FILTER" => "Y",
      "CACHE_GROUPS" => "Y",
      "SET_TITLE" => "N",
      "SET_STATUS_404" => "Y",
      "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
      "ADD_SECTIONS_CHAIN" => "Y",
      "USE_PERMISSIONS" => "N",
      "PREVIEW_TRUNCATE_LEN" => "150",
      "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
      "LIST_FIELD_CODE" => array(),
      "LIST_PROPERTY_CODE" => array(
         "phone","LIQUIDATED"
      ),
      "HIDE_LINK_WHEN_NO_DETAIL" => "N",
      "DISPLAY_NAME" => "Y",
      "META_KEYWORDS" => "",
      "META_DESCRIPTION" => "DESCRIPTION",
      "BROWSER_TITLE" => "BROWSER_TITLE",
      "SECTION_META_KEYWORDS" => "UF_KEYWORDS",
      "SECTION_META_DESCRIPTION" => "UF_DESCRIPTION",
      "SECTION_BROWSER_TITLE" => "UF_SEO_TITLE",
      "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
      "DETAIL_FIELD_CODE" => array(
         "PREVIEW_PICTURE",
         "IBLOCK_SECTION_ID"
      ),
      "DETAIL_PROPERTY_CODE" => array(
         0 => "USER_ID",
         1 => "PREMIUM",
         2 => "EMAIL",
         3 => "COUNTRY",
         4 => "REGION",
         5 => "CITY",
         6 => "index",
         7 => "adress",
         8 => "phone",
         9 => "shedule",
         10 => "URL",
         11 => "MAP",
         12 => "BRANDS",
         13 => "discription",
         14 => "keywords",
         15 => "rating",
         16 => "vote_count",
         17 => "vote_sum",
         18 => "FORUM_MESSAGE_CNT",
         19 => "FORUM_TOPIC_ID",
         20 => "stat",
         21 => "TYPE_D",
         22 => "OBJAV",
         23 => "TYPE_P",
         24 => "TOVARY",
         25 => "news",
         26 => "articles",
         27 => "PRICES",
         28 => "CATALOG",
         29 => "CERTIFICATE",
         30 => "adress_store",
         31 => "METRIKA_ID",
         32 => "KEYWORDS",
         33 => "KEYWORDS_MANUAL", 34=>"LIQUIDATED"
      ),
      "DETAIL_DISPLAY_TOP_PAGER" => "N",
      "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
      "DETAIL_PAGER_TITLE" => "��������",
      "DETAIL_PAGER_TEMPLATE" => "",
      "DETAIL_PAGER_SHOW_ALL" => "Y",
      "DISPLAY_TOP_PAGER" => "Y",
      "DISPLAY_BOTTOM_PAGER" => "Y",
      "PAGER_TITLE" => "�������",
      "PAGER_SHOW_ALWAYS" => "Y",
      "PAGER_TEMPLATE" => "defaultv2",
      "PAGER_DESC_NUMBERING" => "N",
      "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
      "PAGER_SHOW_ALL" => "Y",
      "DISPLAY_DATE" => "N",
      "DISPLAY_PICTURE" => "Y",
      "DISPLAY_PREVIEW_TEXT" => "Y",
      "USE_SHARE" => "N",
      "SITE_TITLE" => "��������������",
      "AJAX_OPTION_ADDITIONAL" => "",
      "SEF_URL_TEMPLATES" => array(
         "news" => "",
         "section" => "#SECTION_CODE#/",
         "detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
      )
   ),
   false
);?>

<!-- (C)2000-2015 Gemius SA - gemiusTraffic / ver 11.1 / Company --> 
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var gemius_identifier = new String('zD6aCDPp48aWkLLnEzA_0eVD.KjF0tfjtg741Qy7zCv.L7');
//--><!]]>
</script>
<script type="text/javascript" src="http://energobelarus.by/js/gemius.js"></script> 






<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>