<? if(!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;

$arComponent = array();
$arComponent["NAME"] = $_POST["NAME"];
$arComponent["TEMPLATE"] = $_POST["TEMPLATE"];
$arComponent["PARAMETERS"] = unserialize($_POST["PARAMETERS"]);
$MD5 = $_POST["MD5"];

//�������� �� md5
if($MD5 == md5(LAZYLOAD_SALT.serialize($arComponent)))
{
   $error = false;
   //�����������
   ob_start();
   $www = $APPLICATION->IncludeComponent("whipstudio:lazyload", "ajax", array(
       "SALT" => LAZYLOAD_SALT,
       "COMPONENTS" => array(
           array(
               "NAME" => $arComponent["NAME"],
               "TEMPLATE" => $arComponent["TEMPLATE"],
               "PARAMETERS" => $arComponent["PARAMETERS"]
           )
       )
   ));
   
   $HTML = ob_get_clean();
   $HTML = iconv("cp1251", "UTF-8", $HTML);
}
else
{
   $error = true;
}

echo json_encode(array("error" => $error, "secure" => $MD5, "html" => $HTML));