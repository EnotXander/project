<?php
/**
 * User: ZvvAsyA
 * Date: 12.08.15 -  15:14
 */
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );

$_REQUEST['name'] = iconv("UTF-8", "cp1251", $_REQUEST['name']);
$companyId = (int)$_REQUEST['company'];
$arActiveCompany = null;
$result =[];
$result['text']=html_entity_decode(iconv("cp1251",  "UTF-8", $_REQUEST['name']));

$result['list'] = [];

if (strlen($_REQUEST['name']) <3 ) {

} else {

	if (!CModule::IncludeModule("iblock")) die();
	$objElement = new CIBlockElement();
	$res = $objElement->GetList(array(),
		array("IBLOCK_ID" => IBLOCK_COMPANY,
		      "ACTIVE" => "Y",
			  "%NAME" => $_REQUEST['name'],
			//	"ID"=>342579	,
		      "!METRIKA_ID" => false
		)
	);

$i=0;
	while($element = $res->GetNext())
	{
		$it = array("array_index"=> $i++,
					"classes"=> "",
					"search_match"=>true,
					"disabled"=> false,
					"group_array_index"=> null,
					"group_label"=> null,
					"html"=> html_entity_decode(iconv("cp1251",  "UTF-8", $element['NAME'])),
					"options_index"=> 0,
					"selected"=> false,
					"style"=> "",
					"text"=> html_entity_decode(iconv("cp1251",  "UTF-8", $element['NAME'])),
					"title"=> html_entity_decode(iconv("cp1251",  "UTF-8", $element['NAME'])),
					"search_text"=> html_entity_decode(iconv("cp1251",  "UTF-8", $element['NAME'])),
					"value"=>$element['ID']
				);
		$result['list'] [] = $it;
	}

}
//print_r($result);
echo json_encode($result, JSON_ERROR_UTF8);
//echo json_last_error_msg();
//$("#form_field").chosen().change( � );