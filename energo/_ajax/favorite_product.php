<?

if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
   die();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


if (!CModule::IncludeModule("iblock"))
{
   ShowError(GetMessage("SONET_MODULE_NOT_INSTALL"));
   return;
}

global $APPLICATION, $USER, $DB;
$isError = true;

$productId = (int)$_REQUEST["product"];

if ($USER->IsAuthorized())
{
   if (isset($_REQUEST["iblockId"]) && $_REQUEST["iblockId"] > 0){
      $iblockId = (int)$_REQUEST["iblockId"];
   } else {
      $iblockId = 27;
   }

   $isError = false;
   $rsProp = CIBlockElement::GetProperty(
       $iblockId,
       $productId,
       array("sort" => "asc"),
       array("CODE" => "FAVORITES")
   );
   $arExistsUsers = array();
   while($arProp = $rsProp->Fetch())
   {
      if(!strlen($arProp["VALUE"])) break;
      $arExistsUsers[] = $arProp["VALUE"];
   }
   if(!in_array($USER->GetID(), $arExistsUsers))
   {
      $arExistsUsers[] = (int)$USER->GetID();
      $inFav = true;
   }
   else
   {
      $index = array_search($USER->GetID(), $arExistsUsers);
      unset($arExistsUsers[$index]);
      $inFav = false;
   }

   //$arExistsUsers[] = 1;//$USER->GetID();
   //$arExistsUsers = array();

   CIBlockElement::SetPropertyValues(
       $productId,
       $iblockId,
       $arExistsUsers,
       "FAVORITES"
   );
}

echo json_encode(array('error' => $isError, 'inFavorites' => $inFav, "LOG" => $arExistsUsers));
