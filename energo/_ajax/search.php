<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){ echo 'error'; die(); };
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$_REQUEST['q'] = iconv("UTF-8", "cp1251", $_REQUEST['q']);
$arReplace = array("'", '"', "�", "�", "�", "�", ",",  "&", "+", "|",  "~", "(", ")");
$_REQUEST['q'] = str_replace($arReplace, "", $_REQUEST['q']);
$imageMargin = false;
$FilterName2 = 'void1';
$idIb2 = array();
$FilterName3 = 'void1';
$idIb3 = array();
$isSorting = false;//���������� ������� �� ��������� �����
$isSectionUp = true;//���������� ������� �����
$isTovarUp = false;//���������� ������ �����
$isCompanyUp = false;//���������� ������ �����

switch ($_REQUEST['w']){
    case 'tov' : {
        $arrFILTER = Array('iblock_services');
        $FilterName = 'arrFILTER_iblock_services';
        $idIb = array("0" => IBLOCK_PRODUCTS);
        $isSorting = true;
        $showDate = false;
        break;
    }
    case 'com' : {
        $arrFILTER = Array('iblock_directories');
          $FilterName = 'arrFILTER_iblock_directories';
          $idIb = array("0" => IBLOCK_COMPANY);
          $isSorting = true;
          $showDate = false;
        break;
    }
    case 'tend' : {
        $arrFILTER = Array('iblock_personal_cabinet');
        $FilterName = 'arrFILTER_iblock_personal_cabinet';
        $idIb = array("0" => 41);
        $showDate = true;
        break;
    }
    case 'objav' : {
        $arrFILTER = Array('iblock_services');
        $FilterName = 'arrFILTER_iblock_services';
        $idIb = array("0" => IBLOCK_ADVARE);
        $showDate = true;
        break;
    }
    case 'news' : {
        $arrFILTER = Array('iblock_information_part', 'blog', 'forum', 'iblock_information_bases', 'iblock_personal_cabinet');
        $FilterName = 'arrFILTER_iblock_information_part';
        $idIb = array("0" => 3, "1" => 25);
        $FilterName2 = 'iblock_information_part';
        $idIb2 = array("0" => 2, "1" => 29);
        $FilterName3 = 'iblock_personal_cabinet';
        $idIb3 = array("0" => 39);
        $showDate = true;
        break;
    }
    case 'term' : {
        $arrFILTER = Array('iblock_information_bases');
        $FilterName = 'arrFILTER_iblock_information_bases';
        $idIb = array("0" => 39);
        $imageMargin = true;
        $showDate = false;
        break;
    }
    case 'art' : {
        $arrFILTER = Array('iblock_information_part');
        $FilterName = 'arrFILTER_iblock_information_part';
        $idIb = array("0" => 2);
        $showDate = true;
        break;
    }
    case 'meropr' : {
        $arrFILTER = Array('iblock_information_bases');
        $FilterName = 'arrFILTER_iblock_information_bases';
        $idIb = array("0" => 25);
        $showDate = false;
        break;
    }
    default : {
        $arrFILTER = Array('no');
        $FilterName = 'nothing';
        $idIb = array("0" => 0);
        $isSorting = true;
        $isTovarUp = true;
        $isCompanyUp = true;
        $showDate = false;
    }
}

$APPLICATION->IncludeComponent("zvv:search.page", "ajax", array (
    "TAGS_SORT" => "NAME",
    "TAGS_PAGE_ELEMENTS" => "150",
    "TAGS_PERIOD" => "30",
    "TAGS_URL_SEARCH" => "/search/index.php",
    "TAGS_INHERIT" => "Y",
    "FONT_MAX" => "50",
    "FONT_MIN" => "10",
    "COLOR_NEW" => "000000",
    "COLOR_OLD" => "C8C8C8",
    "PERIOD_NEW_TAGS" => "",
    "SHOW_CHAIN" => "N",
    "COLOR_TYPE" => "Y",
    "WIDTH" => "100%",
    "USE_SUGGEST" => "N",
    "SHOW_RATING" => "N",
    "PATH_TO_USER_PROFILE" => "",
    "AJAX_MODE" => "N",
    "RESTART" => "N",
    "NO_WORD_LOGIC" => "Y",
    "USE_LANGUAGE_GUESS" => "N",
    "CHECK_DATES" => "N",
    "USE_TITLE_RANK" => "Y",
    "DEFAULT_SORT" => "rank",
    "FILTER_NAME" => "",
    "arrFILTER" => $arrFILTER, //array("no"),
    $FilterName => $idIb,
    $FilterName2 => $idIb2,
    $FilterName3 => $idIb3,
    "SHOW_WHERE" => "Y",
    //"arrWHERE" => array(
    //    0 => 'iblock_'.CATALOG_IBLOCK_ID
    //),
    "SHOW_WHEN" => "N",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "3600",
    "DISPLAY_TOP_PAGER" => "Y",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "���������� ������",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "",
    "AJAX_OPTION_SHADOW" => "Y",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "THUMB_WIDTH" => 52,
    "THUMB_HEIGHT" => 45,
    "BODY_LENGTH" => 40,
    "IMAGE_MARGIN" => $imageMargin,
    "PAGE_RESULT_COUNT_DISPLAYED" => 7,
    "IS_SORTING" => $isSorting,
    "IS_SECTION_UP" => $isSectionUp,
    "IS_TOVAR_UP" => $isTovarUp,
    "IS_COMPANY_UP" => $isCompanyUp,
    "SHOW_DATE" => $showDate
  )
);