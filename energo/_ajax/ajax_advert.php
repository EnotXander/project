<?php

require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );

if ($_REQUEST['per_page'] > 0)
    $perPage = $_REQUEST['per_page'];
else
    $perPage = "20";


$arParams = array( "HIDE_TOP" => true,
   	"SITE_NAME" => 'AgroBelarus.by',
   	"PAGE_NAME" => '����������',
    "IBLOCK_TYPE" => "services",
    "IBLOCK_ID" => IBLOCK_ADVARE,
    "NEWS_COUNT" => $perPage,
    "USE_SEARCH" => "N",
    "USE_RSS" => "Y",
    "NUM_NEWS" => "20",
    "NUM_DAYS" => "30",
    "YANDEX" => "N",
    "USE_RATING" => "N",
    "USE_CATEGORIES" => "N",
    "USE_REVIEW" => "N",
    "USE_FILTER" => "N",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_ORDER1" => "DESC",
    "SORT_BY2" => "SORT",
    "SORT_ORDER2" => "ASC",
    "CHECK_DATES" => "N",
    "SEF_MODE" => "Y",
    "SEF_FOLDER" => "/advert/",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "SET_TITLE" => "N",
    "SET_STATUS_404" => "Y",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "Y",
    "USE_PERMISSIONS" => "N",
    "PREVIEW_TRUNCATE_LEN" => "",
    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
    "LIST_FIELD_CODE" => array(
        0 => "DATE_ACTIVE_TO",
        1 => "",
    ),
    "LIST_PROPERTY_CODE" => array(
        0 => "AUTHOR",
        2 => "COST",
        3 => "CONTACT",
        5 => "",
    ),
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "DISPLAY_NAME" => "Y",
    "META_KEYWORDS" => "-",
    "META_DESCRIPTION" => "-",
    "BROWSER_TITLE" => "title",
    "DETAIL_ACTIVE_DATE_FORMAT" => "j F � G:i",
    "DETAIL_FIELD_CODE" => array(
        0 => "SHOW_COUNTER",
        1 => "",
    ),
    "DETAIL_PROPERTY_CODE" => array(
        0 => "AUTHOR",
        1 => "FIRM",
        2 => "COST",
        3 => "pr1",
        4 => "pr2",
        5 => "pr4",
        6 => "pr5",
        7 => "LOAD",
        8 => "more_photo",
        9 => "",
    ),
    "DETAIL_DISPLAY_TOP_PAGER" => "N",
    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
    "DETAIL_PAGER_TITLE" => "��������",
    "DETAIL_PAGER_TEMPLATE" => "",
    "DETAIL_PAGER_SHOW_ALL" => "Y",
    "DISPLAY_TOP_PAGER" => "Y",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "",
    "PAGER_SHOW_ALWAYS" => "Y",
    "PAGER_TEMPLATE" => "defaultv2",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "Y",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "USE_SHARE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "SEF_URL_TEMPLATES" => array(
        "news" => "",
        "section" => "#SECTION_ID#/",
        "detail" => "#SECTION_ID#/#ELEMENT_ID#/",
        "rss" => "rss/",
        "rss_section" => "#SECTION_ID#/rss/",
    ));

$_REQUEST['s'] = iconv("UTF-8", "cp1251", $_REQUEST['s']);

    global $arrrFilter;
    if ($_REQUEST['BRAND'] > 0)
    {
        $arrrFilter['PROPERTY_BRAND'] = $_REQUEST['BRAND'];
    }
    if ($_REQUEST['user'] > 0)
    {
        $arrrFilter['PROPERTY_AUTHOR'] = $_REQUEST['user'];
    }
    if ($_REQUEST['sect'] > 0)
    {
        $arrrFilter['SECTION_ID'] = $_REQUEST['sect'];
    }
    if ($_REQUEST['FAVOR'] == 'Y')
    {
        $arrrFilter['PROPERTY_FAVORITES'] = array($USER->GetID());
    }
    elseif($_REQUEST['PREMIUM'] == 'Y')
    {
        $arrrFilter['PROPERTY_PREMIUM'] = 56;

    }
    if (isset($_REQUEST["TYPE"]) && !empty($_REQUEST["TYPE"])){
	    $arrrFilter["PROPERTY_TYPE"] = $_REQUEST["TYPE"];
    }
    if (strlen($_REQUEST['s'])>0)
        $arrrFilter['NAME'] = '%'.$_REQUEST['s'].'%';
    $arParams["FILTER_NAME"] = "arrrFilter";


?>


<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"advert_ajax",
	Array(
		"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
		"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
		"NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
		"SORT_BY1"	=>	$arParams["SORT_BY1"],
		"SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
		"SORT_BY2"	=>	$arParams["SORT_BY2"],
		"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
		"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
		"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
		"SET_TITLE"	=>	$arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"ADD_SECTIONS_CHAIN"	=>	$arParams["ADD_SECTIONS_CHAIN"],
		"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
		"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
		"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
		"DISPLAY_NAME"	=>	"Y",
		"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME"	=>	"arrrFilter",//$arParams["FILTER_NAME"],
		"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES"	=>	$arParams["CHECK_DATES"],

		"AJAX_OPTION_JUMP" => "Y",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",

		"PARENT_SECTION"	=>	$arResult["VARIABLES"]["SECTION_ID"],
		"PARENT_SECTION_CODE"	=>	$arResult["VARIABLES"]["SECTION_CODE"],
		"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
	),
	$component
);?>