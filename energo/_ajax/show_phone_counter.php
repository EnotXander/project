<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
global $token;

require_once 'phar://yandex-php-library_master.phar/vendor/autoload.php';
use Yandex\Metrica\Management\ManagementClient;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include.php");

$companyId = (int)$_POST["companyId"];
$status = 'FAIL';
$message='';
$yaid = false;
if ($companyId)
{
    if(!CModule::IncludeModule("iblock")) return;
    $rsCompany = CIBlockElement::GetList(
        array("SORT" => "ASC", "NAME" => "ASC"),
        array(
            "IBLOCK_ID" => IBLOCK_COMPANY,
            "ID" => $companyId
        ),
        false,
        false,
        array('ID', 'IBLOCK_ID', 'PROPERTY_PHONE_CLICK_COUNT', "PROPERTY_METRIKA_ID" )
    );
    if($arCompany = $rsCompany->GetNext())
    {
        $counter = ($arCompany['PROPERTY_PHONE_CLICK_COUNT_VALUE']) ? $arCompany['PROPERTY_PHONE_CLICK_COUNT_VALUE'] : 0;
        $counter++;
        if (CIBlockElement::SetPropertyValueCode(
            $companyId,
	        "PHONE_CLICK_COUNT",
            $counter
        ))
			$status = 'OK';

        $yaid = $arCompany['PROPERTY_METRIKA_ID_VALUE']; /// �������� ��� ��������
        $yaMetrika = YandexMetrika::getInstance(array(
                  "ID" => METRIKA_CLIENT_ID,
                  "SECRET" => METRIKA_CLIENT_SECRET,
                  "USERNAME" => METRIKA_USERNAME,
                  "PASSWORD" => METRIKA_PASSWORD
        ));

        $token = $yaMetrika->getToken();

        $managementClient = new ManagementClient($token);

        // ��������� ������ ����� // ���� �� ��� �� �������
        try {

            $goals = $managementClient->goals()->getGoals(27097436); // ����� ���������� �� ����
            $ag = $goals->getAll();


           $goals = $managementClient->goals()->getGoals($yaid);

           if (count($goals->getAll())==0){
               $managementClient->goals()->addGoal($yaid, $ag[0]);
           }
            $message = 'mb';
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

    }
}

echo json_encode(array('STATUS' => $status, 'MESSAGE' => $message, "COUNTER"=>$yaid));
