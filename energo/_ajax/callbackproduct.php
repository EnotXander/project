<?php
/**
 * User: ZvvAsyA
 * Date: 24.09.15 -  16:07
 */
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );

$APPLICATION->IncludeComponent("whipstudio:main.feedback", "ajax", array(
 "USE_CAPTCHA" => "N",
 "OK_TEXT" => "",
 "REQUIRED_FIELDS" => array("NAME", "PHONE", "EMAIL", "PRODUCT_ID", "ADDRESS", "QUANTITY"),
 "EVENT_MESSAGE_ID" => array(82), "IBLOCK_ID"=>46,
 "EVENT_NAME" => "FAST_ORDER",
 "EVENT_NAME_TO_COMPANY" => "FAST_ORDER_TO_COMPANY",
 "LK_LINK" => "http://".SITE_SERVER_NAME."/personal/"
     )
);
