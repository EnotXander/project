<?
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );
include ( $_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/tools/companies.php');
require('../bitrix/components/whipstudio/catalog.multisection/templates/.default/line_template.php');
//CModule::IncludeModule("CFile");
//CModule::IncludeModule("CIBlock");
//CModule::IncludeModule("CIBlockElement");
global $USER;
global $APPLICATION;
//global $CACHE_MANAGER;
$obParser                   = new CTextParser;
$arParams["PAGER_TEMPLATE"] = "market";

$arList['ID'] = (int) $_POST['category'];
$srt          = unserialize( $_POST['url'] );
if ( $srt['CURPAGE'] ) {
	if ( $srt['PAGEN'] ) {

		$_REQUEST["PAGEN"] = (int) $srt['PAGEN'];

	} else {

		$_REQUEST["PAGEN"] = 1;

	}
} else {

	$_REQUEST["PAGEN"] = 1;

}

$arParams["DETAIL_TEXT_CUT"] = 135;
if ( $arList['ID'] != $srt['CURPAGE'] ) {

	$_REQUEST["CURPAGE"] = $arList['ID'];

} else {

	$_REQUEST["CURPAGE"] = (int) $srt['CURPAGE'];

}
$cmp = new Companies();
$arResult["COMPANY_LIST"] = $cmp->CompaniesInSection( IBLOCK_PRODUCTS, $arList['ID'] );
$arParams["COMPANY_ELEMENT_COUNT"] = 3;
$arParams["COMPANY_ELEMENT_MORE_COUNT"] = 20;

if ( (int) $_REQUEST["PAGEN"] && (int) $_REQUEST["CURPAGE"] == $arList["ID"] ) {
	$arNavParams["iNumPage"] = (int) $_REQUEST["PAGEN"];
} else {
	$arNavParams["iNumPage"] = 1;
}

if ( $srt['sort_by'] ) {

	$arResult["SORT_BY"] = $srt['sort_by'];

} else {

	$arResult["SORT_BY"] = 'company';

}

if ( $srt['sort_type'] ) {

	$arResult["SORT_TYPE"] = $srt['sort_type'];

} else {

	$arResult["SORT_TYPE"] = 'desc';

}
if ( ! $_GET['per_page'] ) {
	if ( $srt['per_page'] ) {
		$arParams["PAGE_ELEMENT_COUNT"] = $srt['per_page'];
	} else
	$arParams["PAGE_ELEMENT_COUNT"] = 20;

} else {

	$arParams["PAGE_ELEMENT_COUNT"] = $_GET['per_page'];

}

    $arResult["SORT_FOR_ITEMS"] = array(
	    $arResult["SORT_BY"]=> $arResult["SORT_TYPE"],
	    //"SORT" => "ASC",
	    //"NAME" => "ASC"
	);

   //��������� ������ ��� ��������
   if($arResult["SORT_BY"] != "company" )
   {
      //����������
      $arFilter = array();
      $arFilter["IBLOCK_ID"] = IBLOCK_PRODUCTS;
      $arFilter["ACTIVE"] = "Y";
      $arFilter["!=PROPERTY_ACTIVE"] = "N";
      $arFilter["SECTION_ID"] = $arList["ID"];

     //���������
      //$GLOBALS["NavNum"] = $arList["ID"];
      $arNavParams = array();//**************
      $arNavParams = array(
           "nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
           "bShowAll" => 'N',
      );
      if((int)$_REQUEST["PAGEN"] && (int)$_REQUEST["CURPAGE"] == $arList["ID"])
         $arNavParams["iNumPage"] = (int)$_REQUEST["PAGEN"];
      else
         $arNavParams["iNumPage"] = 1;
   
    
   }  else  {
        //����������

		$arFilter = array( // ������ ��������
			"ID"  => array_keys( $arResult["COMPANY_LIST"] )
		);

		//���������
		$GLOBALS["NavNum"] = $arList["ID"];
		$arNavParams = array();//**************
		$arNavParams = array(
			"nPageSize" => ceil( ( $arParams["PAGE_ELEMENT_COUNT"] / $arParams["COMPANY_ELEMENT_COUNT"] ) * 1 ),
			"bShowAll"  => $arParams["PAGER_SHOW_ALL"],
		);
		if ( (int) $_REQUEST["PAGEN"] ) {
			$arNavParams["iNumPage"] = (int) $_REQUEST["PAGEN"];
		} else {
			$arNavParams["iNumPage"] = 1;
		}

		//��������� ������ ��������
		$arResult["COMPANY_ONPAGE"] = array();
		$rsCompanyOnPage            = CIBlockElement::GetList(
			array(
				"PROPERTY_EXT_SORT" => $arResult["SORT_TYPE"],
				"SORT" => "asc"
			),
			$arFilter,
			false,
			$arNavParams,
			array(
				"ID",
				"NAME"
			)
		);
		$rsCompanyOnPage->SetUrlTemplates( "", $arParams["DETAIL_URL"] );
		while ( $arCompanyOnPage = $rsCompanyOnPage->GetNext() ) {
			$arResult["COMPANY_ONPAGE"][ $arCompanyOnPage["ID"] ] = $arCompanyOnPage;
		}

		//������ ���������
		$rsCompanyOnPage->NavNum   = $arList["ID"];
		$arList["NAV_STRING"]      = $rsCompanyOnPage->GetPageNavStringEx( $navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"] );
	    $arList["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();

		//���������� ������
		$arResult["SORT_FOR_ITEMS"] = array(
			"PROPERTY_FIRM.PROPERTY_EXT_SORT" => $arResult["SORT_TYPE"],
			"SORT"                            => "ASC",
			"NAME"                            => "ASC"
		);

		//���������� ������
		$arFilter                            = array();
		$arFilter["IBLOCK_ID"]               = IBLOCK_PRODUCTS;
		$arFilter["ACTIVE"]                  = "Y";
	   $arFilter["!=PROPERTY_ACTIVE"] = "N";

		$arFilter["SECTION_ID"]              = $arList["ID"];
		$arFilter["INCLUDE_SUBSECTIONS"]     = "Y";

        $arFilter["PROPERTY_FIRM"] = array_keys( $arResult["COMPANY_ONPAGE"] );
		//��������� ������
		$arNavParams = array();//**************
		$arNavParams = false;
   }

   $CM_SORTED = array_keys($arResult["COMPANY_ONPAGE"]);
   //��������� ������� � �������
   $arList["GROUPS"] = array();
   $groupIndex = -1; //����� ��������������� �� ��������� ������� � ������

   $rsElements = CIBlockElement::GetList(
           $arResult["SORT_FOR_ITEMS"],
           $arFilter,
           false,
            $arNavParams,
           array(
               "ID",
               "NAME",
               "PREVIEW_PICTURE",
               "PREVIEW_TEXT",
               "DETAIL_PICTURE",
               "DETAIL_TEXT",
               "DETAIL_PAGE_URL",
               "PROPERTY_COST",
               "PROPERTY_CURRENCY",
               "PROPERTY_unit_tov",
               "PROPERTY_FIRM.ID",
               "PROPERTY_FIRM.IBLOCK_ID",
               "PROPERTY_FIRM.NAME",
               "PROPERTY_FIRM.PREVIEW_PICTURE",
               "PROPERTY_FIRM.DETAIL_PICTURE",
               "PROPERTY_FIRM.DETAIL_PAGE_URL",
               "PROPERTY_FIRM.PROPERTY_USER",
               "PROPERTY_FIRM.PROPERTY_EMAIL",
               "PROPERTY_FIRM.PROPERTY_MANAGER",
               "PROPERTY_FIRM.PROPERTY_REMOVE_REL",
               "PROPERTY_FIRM.PROPERTY_PREMIUM"
           )
   );
   $rsElements->SetUrlTemplates("", $arParams["DETAIL_URL"]);
   while($arElements = $rsElements->GetNext())
   {
	 //  print_r($arElements['PROPERTY_FIRM_NAME']);
      //�������� ������������� ��������
      $arMultiPropList = array("FAVORITES");
      foreach ($arMultiPropList as $multiPropName)
      {
         $arElements["PROPERTY_".$multiPropName] = array();
         $rsMultiProp = CIBlockElement::GetProperty(
                  $arElements["IBLOCK_ID"],
                  $arElements["ID"],
                  array(),
                  array("CODE" => $multiPropName)
         );
         while($arMultiProp = $rsMultiProp->GetNext())
         {
            $arElements["PROPERTY_".$multiPropName][] = $arMultiProp;
            $arElements["PROPERTY_".$multiPropName."_VALUE"][] = $arMultiProp["VALUE"];
         }
      }
      
      //������ �����
      if(!strlen($arElements["PREVIEW_TEXT"]))
      {
         $arElements["PREVIEW_TEXT"] = strip_tags($obParser->html_cut($arElements["DETAIL_TEXT"], $arParams["DETAIL_TEXT_CUT"]));
      }
      
      //������
      $arElements["PREVIEW_PICTURE"] = Resizer(
              array($arElements["PREVIEW_PICTURE"], $arElements["DETAIL_PICTURE"]),
              array("width" => 55, "height" => 55),
              BX_RESIZE_IMAGE_EXACT
      );
      if(!strlen($arElements["PREVIEW_PICTURE"]["SRC"]))
         $arElements["PREVIEW_PICTURE"]["SRC"] = SITE_TEMPLATE_PATH."/images/withoutphoto54x44.png";
      
      //�������������� ������/������ ���������
      $arElements["PROPERTY_CURRENCY_FORMATED"] = StrUnion(array(
          $arElements["PROPERTY_CURRENCY_VALUE"],
          $arElements["PROPERTY_unit_tov_VALUE"]
      ), "/");
      
      //���� �������� ���������, �� ������� ���� � ��������
      if($arElements["PROPERTY_FIRM_PROPERTY_REMOVE_REL_ENUM_ID"] != false)
      {
         $arElements["PROPERTY_FIRM_ID"] = 0;
      }
      
      //��������� ���� � ��������
	   // ���������� ���� �� ������������
      if(!isset($arResult["COMPANY_DETAIL"][$arElements["PROPERTY_FIRM_ID"]]) && $arElements["PROPERTY_FIRM_ID"] !=0 )
      {
         //TODO: ������ ���������� �������� �������
         $arCompany = array(
             "ID" => $arElements["PROPERTY_FIRM_ID"],
             "IBLOCK_ID" => $arElements["PROPERTY_FIRM_IBLOCK_ID"],
             "NAME" => $arElements["PROPERTY_FIRM_NAME"],
             "PREVIEW_PICTURE" => $arElements["PROPERTY_FIRM_PREVIEW_PICTURE"],
             "DETAIL_PICTURE" => $arElements["PROPERTY_FIRM_DETAIL_PICTURE"],
             "DETAIL_PAGE_URL" => $arElements["PROPERTY_FIRM_DETAIL_PAGE_URL"],
             "PROPERTY_USER" => $arElements["PROPERTY_FIRM_PROPERTY_USER_VALUE"],
             "PROPERTY_EMAIL" => $arElements["PROPERTY_FIRM_PROPERTY_EMAIL_VALUE"],
             "PROPERTY_MANAGER" => $arElements["PROPERTY_FIRM_PROPERTY_MANAGER_VALUE"],
             "PREMIUM" => $arElements["PROPERTY_FIRM_PROPERTY_PREMIUM_VALUE"],
             "ITEMS_COUNT" => array($arList["ID"] => 1)
         );
         //������� �������� ��������
         if($arParams["COMPANY_NAME_CUT"] && (strlen(html_entity_decode($arCompany["NAME"])) > ($arParams["COMPANY_NAME_CUT"] + 2)))
            $arCompany["NAME_CUT"] = $obParser->html_cut(html_entity_decode($arCompany["NAME"]), $arParams["COMPANY_NAME_CUT"]);
         else
            $arCompany["NAME_CUT"] = $arCompany["NAME"];
         //������
         $arCompany["PREVIEW_PICTURE"] = Resizer(
                 array($arCompany["PREVIEW_PICTURE"], $arCompany["DETAIL_PICTURE"]),
                 array("width" => 148, "height" => 40),
                 BX_RESIZE_IMAGE_PROPORTIONAL_ALT
         );
         //�������� ������������� ��������
         $arMultiPropList = array("phone", "URL", "FAVORITES");
         foreach ($arMultiPropList as $multiPropName)
         {
            $arCompany["PROPERTY_".$multiPropName] = array();
            $rsMultiProp = CIBlockElement::GetProperty(
                     $arCompany["IBLOCK_ID"],
                     $arCompany["ID"],
                     array(),
                     array("CODE" => $multiPropName)
            );
            while($arMultiProp = $rsMultiProp->GetNext())
            {
               //$arCompany["PROPERTY_".$multiPropName][] = $arMultiProp;
               $arCompany["PROPERTY_".$multiPropName."_VALUE"][] = $arMultiProp["VALUE"];
            }
         }

         //�������� �������������
         if(ENABLE_PREDSTAVITEL_MODE)
         {
            $arPredstavitelInfo = PredstavitelGetByCompany($arCompany["ID"]);
            $arCompany["PROPERTY_USER"] = $arPredstavitelInfo["RELATED"];
         }
         if(!$arCompany["PROPERTY_USER"])
            $arCompany["PROPERTY_USER"] = $arCompany["PROPERTY_MANAGER"];
         if($arCompany["PROPERTY_USER"])
         {
            $arCompany["PREDSTAVITEL"] = $USER->GetByID($arCompany["PROPERTY_USER"])->Fetch();
            $arCompany["PREDSTAVITEL_MESSAGE_LINK"] = "/personal/messages/{$arCompany["PREDSTAVITEL"]["ID"]}/";
            $arElements["PREDSTAVITEL_MESSAGE_LINK"] = "{$arCompany["PREDSTAVITEL_MESSAGE_LINK"]}?product={$arElements["ID"]}";
         }
         else
         {
            $arCompany["PREDSTAVITEL_MESSAGE_LINK"] = "mailto:{$arCompany["PROPERTY_EMAIL"]}";
            $arElements["PREDSTAVITEL_MESSAGE_LINK"] = $arCompany["PREDSTAVITEL_MESSAGE_LINK"];
         }

         //���������� ��������
         $arResult["COMPANY_DETAIL"][$arElements["PROPERTY_FIRM_ID"]] = $arCompany;
      }
      else
	     $arResult["COMPANY_DETAIL"][$arElements["PROPERTY_FIRM_ID"]]["ITEMS_COUNT"][$arList["ID"]]++;

      $company = $arResult["COMPANY_DETAIL"][$arElements["PROPERTY_FIRM_ID"]];

      //������ �� ������� ������
      $arElements["PREDSTAVITEL_MESSAGE_LINK"] = $company["PREDSTAVITEL_MESSAGE_LINK"];
      if(strpos($company["PREDSTAVITEL_MESSAGE_LINK"], "mailto") === false)
         $arElements["PREDSTAVITEL_MESSAGE_LINK"] .= "?product={$arElements["ID"]}";
         
      if($arResult["SORT_BY"] != "company" )
      {
         //����������� �� ���������
         if(
            ($groupIndex < 0)//� ������ ��� ��� �� ����� ������
            || !$arElements["PROPERTY_FIRM_ID"]//����� �� ����������� ��������
            || ($arElements["PROPERTY_FIRM_ID"] != $arList["GROUPS"][$groupIndex]["COMPANY"])//�������� ����� ������ ���������� �� �������� ���� ������
         )
         {
            $groupIndex++;
         }
         $arList["GROUPS"][$groupIndex]["COMPANY"] = $arElements["PROPERTY_FIRM_ID"];
         $arList["GROUPS"][$groupIndex]["ITEMS"][] = $arElements;
      }
      else
      {
         //������ ������� ���� � �������� �������� ������ ������
         if($company["ITEMS_COUNT"][$arList["ID"]] > $arParams["COMPANY_ELEMENT_COUNT"])
	         $arElements["HIDE"] = true;
         //�������� ������ ������ ������� ���������
         if(!(($company["ITEMS_COUNT"][$arList["ID"]] - $arParams["COMPANY_ELEMENT_COUNT"]) % $arParams["COMPANY_ELEMENT_MORE_COUNT"]))
	         $arElements["HIDE_PANEL"] = true;

         //����������� �� ���������
	      if ($arElements["PROPERTY_FIRM_ID"] == 0 ) {
		      $groupKey = 999999;
	      } else {

		      // ����� �� �������� ������ � ���������
		      $groupKey = array_search($arElements["PROPERTY_FIRM_ID"], $CM_SORTED);
	      }

         $arList["GROUPS"][$groupKey]["COMPANY"] = $arElements["PROPERTY_FIRM_ID"];
         $arList["GROUPS"][$groupKey]["ITEMS"][] = $arElements;
      }
      
   }

   if($arResult["SORT_BY"] != "company" )
   {
      //������ ���������
      $rsElements->NavNum = $arList["ID"];
      $arList["NAV_STRING"] = $rsElements->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);

      $arList["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
      //$arList["NAV_RESULT"] = $rsElements;
   }
   else
   {
      //���������� ����� ��� ����������� ������� ��������
      ksort($arList["GROUPS"]);
   }
// ���� � ���������� ��������
   $arList["NAV_STRING"] = str_replace('/_ajax/catalog_ajax.php', (string)$_POST['path'], $arList["NAV_STRING"]);
   $arList["NAV_STRING"] = str_replace('&amp;PAGEN=1', '', $arList["NAV_STRING"]);

   //������ ���������� ������� � �������
   $arList["ITEMS_COUNT"] = 0;//CIBlockElement::GetList(array(), array_merge($arrFilter, $arFilter));
   $arListKey = $arList["ID"];
   //����������
   $arResult["LISTS"][$arListKey] = $arList;
 ?>

   
        <?if(count($arResult["LISTS"])):?>
         <?if(count($arResult["LISTS"]) > 1):?>
            <script>
               $(function(){
                  $(".accordion_block").accordion({
                     heightStyle: "content",
                     active: parseInt($('.accordion_block').attr("data-active"))
                  });
               });
            </script>
         <?endif;?>
         <ul class="accordion_block " data-active="<?=$arResult["ACCORDION_ACTIVE"]?>">
            <?foreach ($arResult["LISTS"] as $listKey => $arList):?>
                <? renderLine($arResult, $arList, $listKey, $arParams ) ?>
            <?endforeach;?>
         </ul>
      <?endif;?>