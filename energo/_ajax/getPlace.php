<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include.php");

$extID = (int)$_POST["ext_id"];
$type = $_POST["type"];

$arPlaces = array();
if(!CModule::IncludeModule("iblock")) return;
if($type == "region")
{
   $title = "������";
   $iblock_id = 22;
   $rsPlace = CIBlockElement::GetList(
      array("SORT" => "ASC", "NAME" => "ASC"),
      array(
         "IBLOCK_ID" => $iblock_id,
         "ACTIVE" => "Y",
         "PROPERTY_COUNTRY" => $extID
      ),
      false,
      false,
      array("ID", "NAME"));
   while($arPlace = $rsPlace->GetNext())
   {
      $arPlaces[] = $arPlace;
   }
}
elseif($type == "city")
{
   $title = "�����";
   $iblock_id = 23;
   $rsPlace = CIBlockElement::GetList(
      array("SORT" => "ASC", "NAME" => "ASC"),
      array(
         "IBLOCK_ID" => $iblock_id,
         "ACTIVE" => "Y",
         "PROPERTY_REGION" => $extID
      ),
      false,
      false,
      array("ID", "NAME"));
   while($arPlace = $rsPlace->GetNext())
   {
      $arPlaces[] = $arPlace;
   }
}
else return;

$response = array();
$response["listOptionHTML"] = array();
$response["listOptionHTML"][] = iconv("cp1251", "UTF-8", "<option value='0' selected>�������� {$title}</option>");

foreach($arPlaces as $arPlace)
{
   $html = "<option class='del' value='{$arPlace["ID"]}'>{$arPlace["NAME"]}</option>";
   $html = iconv("cp1251", "UTF-8", $html);
   $response["listOptionHTML"][] = $html;
}

echo json_encode($response);