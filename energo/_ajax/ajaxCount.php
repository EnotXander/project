<?
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );


$arFilter["IBLOCK_ID"] = $_REQUEST['TYPE'] == 'uslugi' ? IBLOCK_USLUGI : IBLOCK_PRODUCTS;
$arFilter["ACTIVE"]    = "Y";

$categories = explode( ',', $_POST['categories'] );
$result     = array();
foreach ( $categories as $category ) {

	$arFilter["SECTION_ID"] = $category;
	$arFilter["INCLUDE_SUBSECTIONS"]    = "Y";

	$res = CIBlockElement::GetList(
		false,
		$arFilter,
		array( 'IBLOCK_ID' )
	);

	if ( $el = $res->Fetch() ) {
		$result[ $category ] = $el['CNT'];
	} else {
		$result[ $category ] = 'remove';
	}
}

echo json_encode( $result );
