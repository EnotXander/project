<?php
/**
 * User: ZvvAsyA
 * Date: 18.02.16 -  13:18
 */
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );

$_REQUEST['NAME'] = trim(iconv("UTF-8", "cp1251", $_REQUEST['NAME']));

$arFilter = ['NAME'=>$_REQUEST['NAME']];

$res = CIBlockElement::GetList(
		false,
		$arFilter);

if ( $el = $res->GetNext() ) {
	echo json_encode(['URL'=>$el['DETAIL_PAGE_URL'], "ID"=>$el['ID']]);
} else {
	echo json_encode(['URL'=>'']);
}