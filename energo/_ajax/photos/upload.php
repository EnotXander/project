<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header("Content-type: text/html; charset=UTF-8");  
require $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/a.charset.php';

if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement;

// files storage folder
$dir = '/upload/photos/';
$iblockId = 10;

$_FILES['files']['type'][0] = strtolower($_FILES['files']['type'][0]);

$sectionId = (int)$_REQUEST["section_id"];
$iblockId = (int)$_REQUEST["iblock_id"];

if($sectionId)
{
   if ($_FILES['files']['type'][0] == 'image/png'
   || $_FILES['files']['type'][0] == 'image/jpg'
   || $_FILES['files']['type'][0] == 'image/gif'
   || $_FILES['files']['type'][0] == 'image/jpeg'
   || $_FILES['files']['type'][0] == 'image/pjpeg')
   {
       // setting file's mysterious name
       $file = $dir.md5(date('YmdHis')).'.jpg';

       // copying
       if(file_exists($_FILES['files']['tmp_name'][0]))
       {
          move_uploaded_file($_FILES['files']['tmp_name'][0], $_SERVER["DOCUMENT_ROOT"].$file);
          $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$file);//($_FILES['files']['tmp_name'][0]);
          $arReplace = array(
               "max_len" => 100,
               "change_case" => 'L',
               "replace_space" => '_',
               "replace_other" => '_',
               "delete_repeat_replace" => true,
               "use_google" => false
            );
          $arToSave = array(
               "MODIFIED_BY"    => $USER->GetID(), // ������� ������� ������� �������������
               "IBLOCK_ID"      => $iblockId,
               "NAME"           => $_FILES['files']['name'][0],
               "ACTIVE"         => "Y",            // �������
               "DETAIL_TEXT_TYPE" => "html",
               "PREVIEW_TEXT_TYPE" => "html",
               "CODE" => Cutil::translit($_FILES['files']['name'][0], "ru", $arReplace),
               "DETAIL_PICTURE" => $arFile,
               "PREVIEW_PICTURE" => $arFile,
               "IBLOCK_SECTION_ID"=> $sectionId
            );
            if($newID = $objElement->Add($arToSave))
            {
               unlink($_SERVER["DOCUMENT_ROOT"].$file);
               $element = $objElement->GetByID($newID)->GetNext();
               $array = array(
                   "ID" => $newID
               );
            }
            else
            {
                $error = $objElement->LAST_ERROR;
                $array = array(
                   "ERROR" => $error
               );
            }
       }

       echo stripslashes(json_encode(array(
               "error" => false,
               "data" => $array
       )));
   }
   else
   {
        echo stripslashes(json_encode(array(
            "error" => true,
            "errortext" => iconv("cp1251", "UTF-8", "���������������� ��� �����: {$_FILES['files']['type'][0]}")
        )));
   }
}
else
{
   echo stripslashes(json_encode(array(
       "error" => true,
       "errortext" => iconv("cp1251", "UTF-8", "������������ ID ����������: {$_REQUEST["section_id"]}"),
   )));
}