<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("\"�������� ������\" - \"�����\" - ������ �������������� ��������");
$APPLICATION->SetPageProperty("description", "��������� ���������� �������� \"�����\" �� ���������� �������� � ����������."); 
$APPLICATION->SetPageProperty("keywords", "�����, ���������� �������, ����������");
$APPLICATION->AddHeadString('<meta name="abstract" content="��������� ���������� ��������� �� ���������� �������� � ����������.">', true);

$APPLICATION->AddHeadString('<link href="http://'.$_SERVER['HTTP_HOST'].''.SITE_TEMPLATE_PATH.'/css/additional.css" rel="stylesheet" type="text/css">', true);
$APPLICATION->AddHeadString('<link href="http://'.$_SERVER['HTTP_HOST'].''.SITE_TEMPLATE_PATH.'/css/additional-v2.css" rel="stylesheet" type="text/css">', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="http://'.$_SERVER['HTTP_HOST'].''.SITE_TEMPLATE_PATH.'/js/jquery.bxslider.min.js"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="http://'.$_SERVER['HTTP_HOST'].''.SITE_TEMPLATE_PATH.'/js/jquery.rateit.min.js"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="http://'.$_SERVER['HTTP_HOST'].''.SITE_TEMPLATE_PATH.'/js/additional.js"></script>', true);
?>

<div class="middle_col">
	<div class="breadcrumbs">
		<?$APPLICATION->IncludeComponent("whipstudio:breadcrumb","blog-breadcrumbs",Array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "s1"
		));?>
	</div>
	<?$APPLICATION->IncludeComponent("bitrix:news.detail","blogdetail",Array(
			"AJAX_MODE" => "N",
			"IBLOCK_TYPE" => "information_part",
			"IBLOCK_ID" => "50",
			//"ELEMENT_ID" => $_REQUEST["id"],
			"ELEMENT_CODE" => $_REQUEST["ELEMENT_CODE"],
			"CHECK_DATES" => "Y",
			"FIELD_CODE" =>Array('ID','NAME','PREVIEW_PICTURE','DETAIL_TEXT','DATE_CREATE','TAGS'),
			"PROPERTY_CODE" => Array(),
			"IBLOCK_URL" => "/blogs/",
			"META_KEYWORDS" => "KEYWORDS",
			"META_DESCRIPTION" => "DESCRIPTION",
			"BROWSER_TITLE" => "BROWSER_TITLE",
			"DISPLAY_PANEL" => "Y",
			"SET_TITLE" => "Y",
			"SET_STATUS_404" => "Y",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"USE_PERMISSIONS" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600",
			"CACHE_GROUPS" => "Y",
			"DISPLAY_TOP_PAGER" => "Y",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => "",
			"PAGER_TEMPLATE" => "",
			"PAGER_SHOW_ALL" => "Y"
		)
	);?>
	<?$APPLICATION->IncludeFile('include/blog/detail_right_col.php');?>
</div>

<?if((defined('ERROR_404') && ERROR_404 == 'Y')) LocalRedirect("/404.php", false, "404 Not found");?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>