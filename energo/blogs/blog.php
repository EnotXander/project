<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<?
// redirect from blog.php?id=<ID>
if (!CModule::IncludeModule("iblock")) die();
$IBLOCK_ID = 50;

$resElement = CIBlockElement::GetList(array(), array(
	'ACTIVE' => 'Y',
	'IBLOCK_ID' => 50,
	'ID' => $_GET['id']
));
if ($arrElement = $resElement->GetNext()){
	LocalRedirect($arrElement['DETAIL_PAGE_URL'], false, "301 Moved Permanently");
}else{
	//LocalRedirect("/404.php", false, "404 Not found");
}

?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>