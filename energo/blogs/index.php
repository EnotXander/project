<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("����� �������� &quot;����������&quot; �� ������� EnergoBelarus.by");
$APPLICATION->SetPageProperty("description", "��������� ���������� ��������� �� ���������� �������� � ����������."); 
$APPLICATION->SetPageProperty("keywords", "�����, ���������� �������, ����������");
$APPLICATION->AddHeadString('<meta name="abstract" content="��������� ���������� ��������� �� ���������� �������� � ����������.">', true);

$APPLICATION->AddHeadString('<link href="http://'.$_SERVER['HTTP_HOST'].''.SITE_TEMPLATE_PATH.'/css/additional.css" rel="stylesheet" type="text/css">', true);
$APPLICATION->AddHeadString('<link href="http://'.$_SERVER['HTTP_HOST'].''.SITE_TEMPLATE_PATH.'/css/additional-v2.css" rel="stylesheet" type="text/css">', true);
CModule::IncludeModule("iblock");

$arrFilter = array();
if (isset($_GET['tags'])){
	$filter = trim($_GET['tags']);
	$filter = str_replace('+', ' ', $filter);
	$arrFilter['TAGS'] = '%'.$filter.'%';
}
if (isset($_GET['author'])){

	$filter = intval($_GET['author']);
	$res = CIBlockSection::GetList(Array('ID'=>'ASC'), array(
		'IBLOCK_ID' => '50',
		'UF_USER_ID' => $filter
	));
	$arr = $res->GetNext();

	LocalRedirect($arr['SECTION_PAGE_URL'], false, "301 Moved permanently");

	die();
	//$arrFilter['SECTION_ID'] = $arr['ID'];
}

if ( isset($_REQUEST['SECTION_CODE'])  ) {

		$res = CIBlockSection::GetList(Array('ID'=>'ASC'), array(
			'IBLOCK_ID' => '50',
			'=CODE' => $_REQUEST['SECTION_CODE']
		));
		$arr = $res->GetNext();

		$arrFilter['SECTION_ID'] = $arr['ID'];
}


if (isset($_GET['date'])){
	$filter = trim($_GET['date']);
	$unixtimeFrom = MakeTimeStamp($filter, CSite::GetDateFormat());
	$unixtimeTo = $unixtimeFrom + 24*60*60;
	//echo date('d.m.Y H:i:s',$unixtimeTo);
	//$arrFilter[">=DATE_ACTIVE_FROM"] = ConvertDateTime( date("d.m.Y H:i:s",$unixtimeFrom), "YYYY-MM-DD HH:MM:SS" );
	$arrFilter[">=DATE_ACTIVE_FROM"] = date($DB->DateFormatToPHP(CLang::GetDateFormat("LONG")), $unixtimeFrom);
	$arrFilter["<=DATE_ACTIVE_FROM"] = date($DB->DateFormatToPHP(CLang::GetDateFormat("LONG")), $unixtimeTo);
}
$per_page = 4;
if (isset($_GET['per_page'])) {
	$per_page = $_GET['per_page'];
}
?>

<div class="middle_col">
	<div class="breadcrumbs">
		<?$APPLICATION->IncludeComponent("whipstudio:breadcrumb","blog-breadcrumbs",Array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "s1"
		));?>
	</div>
	<?$APPLICATION->IncludeComponent("bitrix:news.list","bloglist",Array(
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"AJAX_MODE" => "N",
			"IBLOCK_TYPE" => "information_part",
			"IBLOCK_ID" => "50",
			"NEWS_COUNT" => $per_page,
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "DATE_CREATE",
			"SORT_ORDER2" => "DESC",
			"FILTER_NAME" => "arrFilter",
			"FIELD_CODE" => Array('ID','NAME','DETAIL_PICTURE','DETAIL_TEXT','DATE_CREATE','TAGS','SHOW_COUNTER'),
			"PROPERTY_CODE" => Array('RATING','FORUM_MESSAGE_CNT'),
			"CHECK_DATES" => "Y",
			//"DETAIL_URL" => "/blogs/#SECTION_CODE#/#ELEMENT_CODE#",
			"PREVIEW_TRUNCATE_LEN" => "1000",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"SET_TITLE" => "N",
			"SET_STATUS_404" => "Y",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
			"PARENT_SECTION" => $arrFilter['SECTION_ID'],
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600",
			"CACHE_FILTER" => "Y",
			"CACHE_GROUPS" => "Y",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => "Y",
			"PAGER_SHOW_ALWAYS" => "Y",
			"PAGER_TEMPLATE" => "defaultv2",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "Y",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_ADDITIONAL" => ""
		)
	);?>
	<?$APPLICATION->IncludeFile('include/blog/right_col.php');?>
</div>
<?if((defined('ERROR_404') && ERROR_404 == 'Y')) LocalRedirect("/404.php", false, "404 Not found");?>
<!-- (C)2000-2015 Gemius SA - gemiusTraffic / ver 11.1 / Blog -->
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var gemius_identifier = new String('bDiQBFShdxEsrf0NZIKBubbtDfaYhsOEce3wg8a09mr.X7');
//--><!]]>
</script>
<script type="text/javascript" src="http://energobelarus.by/js/gemius.js"></script> 



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>