<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle(" ���������� � �����");
?>

<?if (isset($_REQUEST["faces_id"])) // �������� �� ������ �������
{
   $facesID = (int) $_REQUEST["faces_id"];
   if ($facesID > 0)
   {
      if (!CModule::IncludeModule("iblock")) die();
      $objElement = new CIBlockElement();
      $res = $objElement->GetList(array(), array("IBLOCK_ID" => 33, "ACTIVE" => "Y", "EXTERNAL_ID" => $facesID));
      if ($element = $res->GetNext()) // ���������� �� ������ � ����� �������
         LocalRedirect($element["DETAIL_PAGE_URL"], false, "301 Moved Permanently");
      else // ������ ������ �� ����������
         LocalRedirect("/404.php", false, "404 Not found");
   }
}

$APPLICATION->IncludeComponent("bitrix:news", "persons", array(
    "IBLOCK_TYPE" => "lists",
    "IBLOCK_ID" => "33",
    "NEWS_COUNT" => "20",
    "USE_SEARCH" => "N",
    "USE_RSS" => "N",
    "USE_RATING" => "N",
    "USE_CATEGORIES" => "N",
    "USE_REVIEW" => "N",
    "USE_FILTER" => "N",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_ORDER1" => "DESC",
    "SORT_BY2" => "SORT",
    "SORT_ORDER2" => "ASC",
    "CHECK_DATES" => "Y",
    "SEF_MODE" => "Y",
    "SEF_FOLDER" => "/persons/",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "Y",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "Y",
    "USE_PERMISSIONS" => "N",
    "PREVIEW_TRUNCATE_LEN" => "60",
    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
    "LIST_FIELD_CODE" => array("DETAIL_TEXT", "DETAIL_PICTURE"),
    "LIST_PROPERTY_CODE" => array("FIRM"),
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "DISPLAY_NAME" => "Y",
    "META_KEYWORDS" => "-",
    "META_DESCRIPTION" => "-",
    "BROWSER_TITLE" => "-",
    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
    "DETAIL_FIELD_CODE" => array(),
    "DETAIL_PROPERTY_CODE" => array(),
    "DETAIL_DISPLAY_TOP_PAGER" => "N",
    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
    "DETAIL_PAGER_TITLE" => "��������",
    "DETAIL_PAGER_TEMPLATE" => "",
    "DETAIL_PAGER_SHOW_ALL" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "�������",
    "PAGER_SHOW_ALWAYS" => "Y",
    "PAGER_TEMPLATE" => "defaultv2",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "Y",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "USE_SHARE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "SEF_URL_TEMPLATES" => array(
        "news" => "",
        "section" => "",
        "detail" => "#ELEMENT_ID#/",
    )
        ), false
);
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>