<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
//session_start();
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
die();
$APPLICATION->IncludeComponent(
    "bitrix:forum.topic.reviews", "", Array(
    "CACHE_TYPE" => $_SESSION["arParams"]["CACHE_TYPE"],
    "CACHE_TIME" => $_SESSION["arParams"]["CACHE_TIME"],
    "MESSAGES_PER_PAGE" => $_SESSION["arParams"]["MESSAGES_PER_PAGE"],
    "USE_CAPTCHA" => $_SESSION["arParams"]["USE_CAPTCHA"],
    "PATH_TO_SMILE" => $_SESSION["arParams"]["PATH_TO_SMILE"],
    "FORUM_ID" => $_SESSION["arParams"]["FORUM_ID"],
    "URL_TEMPLATES_READ" => $_SESSION["arParams"]["URL_TEMPLATES_READ"],
    "SHOW_LINK_TO_FORUM" => $_SESSION["arParams"]["SHOW_LINK_TO_FORUM"],
    "ELEMENT_ID" => $_SESSION["ElementID"],
    "AJAX_POST" => $_SESSION["arParams"]["REVIEW_AJAX_POST"],
    "IBLOCK_ID" => $_SESSION["arParams"]["IBLOCK_ID"],
    "POST_FIRST_MESSAGE" => $_SESSION["arParams"]["POST_FIRST_MESSAGE"],
    "URL_TEMPLATES_DETAIL" => $_SESSION["arParams"]["POST_FIRST_MESSAGE"] === "Y" ? $_SESSION["arResult"]["FOLDER"] . $_SESSION["arResult"]["URL_TEMPLATES"]["detail"] : "",
        ), $_SESSION["component"]
);

unset($_SESSION["arParams"]);
unset($_SESSION["ElementID"]);
unset($_SESSION["arResult"]);
unset($_SESSION["component"]);
//LocalRedirect($_POST["back_page"], false);

