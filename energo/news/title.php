<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
   
<?
$APPLICATION->IncludeComponent("bitrix:catalog.sections.top", ".default", array(
    "IBLOCK_TYPE" => "information_part",
    "IBLOCK_ID" => "3",
    "SECTION_FIELDS" => array(
        0 => "",
        1 => "",
    ),
    "SECTION_USER_FIELDS" => array(
        0 => "",
        1 => "",
    ),
    "SECTION_SORT_FIELD" => "sort",
    "SECTION_SORT_ORDER" => "asc",
    "ELEMENT_SORT_FIELD" => "sort",
    "ELEMENT_SORT_ORDER" => "asc",
    "FILTER_NAME" => "arrFilter",
    "SECTION_COUNT" => "20",
    "ELEMENT_COUNT" => "3",
    "LINE_ELEMENT_COUNT" => "1",
    "PROPERTY_CODE" => array(
        0 => "",
        1 => "",
    ),
    "SECTION_URL" => "",
    "DETAIL_URL" => "",
    "BASKET_URL" => "/personal/basket.php",
    "ACTION_VARIABLE" => "action",
    "PRODUCT_ID_VARIABLE" => "id",
    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
    "PRODUCT_PROPS_VARIABLE" => "prop",
    "SECTION_ID_VARIABLE" => "SECTION_ID",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "DISPLAY_COMPARE" => "N",
    "PRICE_CODE" => array(
    ),
    "USE_PRICE_COUNT" => "N",
    "SHOW_PRICE_COUNT" => "1",
    "PRICE_VAT_INCLUDE" => "Y",
    "PRODUCT_PROPERTIES" => array(
    ),
    "USE_PRODUCT_QUANTITY" => "N"
        ), false
);
?>
   
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>