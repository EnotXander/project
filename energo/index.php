<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetPageProperty("keywords", "����������, ����������������, ��������, ��������, �������, ������, ������");
$APPLICATION->SetPageProperty("description", "���������� �������������-������������� ������, ����������� ���������� ��������. ���������� ������� � �������. ��������� ���������� � ���������, ������ � ������.");
$APPLICATION->SetPageProperty("title", "�������������� - ���������� � ���������������� ��������, ������� �������");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("�������������� - ���������� � ���������������� ��������, ������� �������");

if ($_SERVER["SCRIPT_NAME"] != $_SERVER["PHP_SELF"])
{
   @define("ERROR_404", "Y");
   CHTTP::SetStatus("404 Not Found");
   require $_SERVER['DOCUMENT_ROOT'] . '/404.php';
   exit();
}
?>

<style>
   .main
   {
      min-width:1243px;
   }
   /*������� �� �������*/
   .content_box{
      padding-bottom: 0px;
   }
</style>

<div class="content_box" style="overflow: visible;">
   <div class="right_col">
      <div class="rc_block">
			<!-- ad A2-1 -->
         <div class="right_banner_300x120">
            <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", array(
                "TYPE" => "A2_1",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "CACHE_NOTES" => ""
                    )
            );?>
         </div>
			<!-- ad A2-2 -->
         <div class="right_banner_300x120">
            <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", array(
                "TYPE" => "A2_2",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "CACHE_NOTES" => ""
                    )
            );?>
         </div>
			<!-- ad A2-3 -->
         <div class="right_banner_300x120">
            <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", array(
                "TYPE" => "A2_3",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "CACHE_NOTES" => ""
                    )
            );?>
         </div>
      </div>
      <!--rc_block-->
      <!-- <div class="rc_block brdr_box">
        <div class="wrap">
          <div class="title"><a href="#" class="font_18">�����</a><sup>0</sup></div>
          <div class="video_box"><a href="#"><img src="uploads/video_lb.jpg" alt=""></a></div>
          <div>������� ����� ����� ��� ���� �� ����� ������ ����� ����� ����� �������... </div>
          <div class="nav_row">
            <a href="#aLLV" class="nodecor bold">��� �����</a>
          </div>
        </div>
      </div> --><!--rc_block-->
      <div class="toog withTitlePhoto" style="overflow: hidden;">
         <ul class="mb_menu">
            <li class="mb_title sel" id="mb_title_f">
               <a href="#1">���������</a>
               <div class="sclew"></div>
            </li>
            <li>
               <a href="#3" data-onclick-once="lazyload_loadPhotoCarusel34">����������</a>
               <div class="sclew"></div>
            </li>
         </ul>
         <div class="gray_box catalog_list tab"  style="background-color: #fff;   border: 1px solid #e2e2e2; box-shadow: none;">
            <?//����������
            $APPLICATION->IncludeComponent(
	"bitrix:catalog.sections.top", 
	"main_p", 
	array(
		"IBLOCK_TYPE" => "media_content",
		"IBLOCK_ID" => "10",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_SORT_FIELD" => "Sort",
		"SECTION_SORT_ORDER" => "desc",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FILTER_NAME" => "",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_COUNT" => "50",
		"ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"USE_PRODUCT_QUANTITY" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"COMPONENT_TEMPLATE" => "main_p",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"HIDE_NOT_AVAILABLE" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"BASKET_URL" => "/personal/basket.php",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => array(
		),
		"CONVERT_CURRENCY" => "N"
	),
	false
);?>
            <?
            // ����������
            /*global $arrFilterPhoto;
            $arrFilterPhoto["SECTION_GLOBAL_ACTIVE"] = "Y";
            $APPLICATION->IncludeComponent("bitrix:news.list", "main_p", array(
                              "IBLOCK_TYPE" => "media_content",
                              "IBLOCK_ID" => 10,
                              "NEWS_COUNT" => 7,
                              "SORT_BY1" => "ACTIVE_FROM",
                              "SORT_ORDER1" => "DESC",
                              "SORT_BY2" => "SORT",
                              "SORT_ORDER2" => "ASC",
                              "FILTER_NAME" => "arrFilterPhoto",
                              "FIELD_CODE" => array("DETAIL_PICTURE", "PREVIEW_PICTURE", "DATE_CREATE"),
                              "PROPERTY_CODE" => array(0 => ""),
                              "CHECK_DATES" => "N",
                              "DETAIL_URL" => "",
                              "AJAX_MODE" => "N",
                              "AJAX_OPTION_JUMP" => "N",
                              "AJAX_OPTION_STYLE" => "Y",
                              "AJAX_OPTION_HISTORY" => "N",
                              "CACHE_TYPE" => "N",
                              "CACHE_TIME" => 3600*24,
                              "CACHE_FILTER" => "N",
                              "CACHE_GROUPS" => "Y",
                              "PREVIEW_TRUNCATE_LEN" => "",
                              "ACTIVE_DATE_FORMAT" => "d.m H:i",
                              "SET_TITLE" => "N",
                              "SET_STATUS_404" => "N",
                              "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                              "ADD_SECTIONS_CHAIN" => "N",
                              "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                              "PARENT_SECTION" => "",
                              "PARENT_SECTION_CODE" => "",
                              "DISPLAY_TOP_PAGER" => "N",
                              "DISPLAY_BOTTOM_PAGER" => "N",
                              "PAGER_TITLE" => "�������",
                              "PAGER_SHOW_ALWAYS" => "Y",
                              "PAGER_TEMPLATE" => "",
                              "PAGER_DESC_NUMBERING" => "N",
                              "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                              "PAGER_SHOW_ALL" => "Y",
                              "DISPLAY_DATE" => "N",
                              "DISPLAY_NAME" => "N",
                              "DISPLAY_PICTURE" => "Y",
                              "DISPLAY_PREVIEW_TEXT" => "N",
                              "AJAX_OPTION_ADDITIONAL" => ""
                                  )
                    );*/
            ?>
         </div>
         <div class="gray_box catalog_list tab"  style="background-color: #fff;   border: 1px solid #e2e2e2; box-shadow: none;">
            <?//�����������
            $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                     "SALT" => LAZYLOAD_SALT,
                     "COMPONENTS" => array(
                         array(
                             "NAME" => "bitrix:catalog.sections.top",
                             "TEMPLATE" => "main_p",
                             "PARAMETERS" => array(
                                   "IBLOCK_TYPE" => "media_content",
                                   "IBLOCK_ID" => 34,
                                   "SECTION_FIELDS" => Array(""),
                                   "SECTION_USER_FIELDS" => Array(""),
                                   "SECTION_SORT_FIELD" => "TIMESTAMP_X",
                                   "SECTION_SORT_ORDER" => "desc",
                                   "ELEMENT_SORT_FIELD" => "sort",
                                   "ELEMENT_SORT_ORDER" => "asc",
                                   "FILTER_NAME" => "",
                                   "SECTION_URL" => "",
                                   "DETAIL_URL" => "",
                                   "ACTION_VARIABLE" => "action",
                                   "PRODUCT_ID_VARIABLE" => "id",
                                   "PRODUCT_QUANTITY_VARIABLE" =>  "quantity",
                                   "PRODUCT_PROPS_VARIABLE" =>  "prop",
                                   "SECTION_ID_VARIABLE" => "SECTION_ID",
                                   "SECTION_COUNT" => "7",
                                   "ELEMENT_COUNT" => "1",
                                   "PROPERTY_CODE" => Array(),
                                   "USE_PRODUCT_QUANTITY" => "Y",
                                   "CACHE_TYPE" => "A",
                                   "CACHE_TIME" => "3600",
                                   "CACHE_FILTER" => "N",
                                   "CACHE_GROUPS" => "N"
                             )
                         )
                     )
                 )
             );
            ?>
         </div>
      </div>
   </div><!--VS right col 1 end-->
   <!--VS start midl1-->
   <div class="middle_col">
      <div class="mc_block withTitleMoln" style="overflow:hidden; height:300px;">
         <ul class="mb_menu">
            <li class="mb_title"><a href="#1">������� �������</a><div class="sclew"></div></li>
            <li class="sel"><a>���� ���</a><div class="sclew"></div></li>
            <li><a href="#3">��������</a><div class="sclew"></div></li>
            <li><a href="#3">������� ��������</a><div class="sclew"></div></li>
            <li><a href="#3">������ � ���</a><div class="sclew"></div></li>
            <li><a href="#3">� ����</a><div class="sclew"></div></li>
         </ul>
         <?
         $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"moln_main",
	array(
		"IBLOCK_TYPE" => "information_part",
		"IBLOCK_ID" => "3",
		"NEWS_COUNT" => "4",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m H:i",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "6",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "�������",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "moln_main",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                 "SALT" => LAZYLOAD_SALT,
                 "COMPONENTS" => array(
                     array(
                         "NAME" => "bitrix:news.list",
                         "TEMPLATE" => "moln_main",
                         "PARAMETERS" => array(
                             "IBLOCK_TYPE" => "information_part",
                             "IBLOCK_ID" => "3",
                             "NEWS_COUNT" => "4",
                             "SORT_BY1" => "ACTIVE_FROM",
                             "SORT_ORDER1" => "DESC",
                             "SORT_BY2" => "SORT",
                             "SORT_ORDER2" => "ASC",
                             "FILTER_NAME" => "",
                             "FIELD_CODE" => array("DETAIL_PICTURE", "PREVIEW_PICTURE"),
                             "PROPERTY_CODE" => array(
                                 0 => "",
                                 1 => "",
                             ),
                             "CHECK_DATES" => "Y",
                             "DETAIL_URL" => "",
                             "AJAX_MODE" => "N",

                             "AJAX_OPTION_JUMP" => "N",
                             "AJAX_OPTION_STYLE" => "Y",
                             "AJAX_OPTION_HISTORY" => "N",
                             "CACHE_TYPE" => "A",
                             "CACHE_TIME" => "300",
                             "CACHE_FILTER" => "N",
                             "CACHE_GROUPS" => "N",
                             "PREVIEW_TRUNCATE_LEN" => "",
                             "ACTIVE_DATE_FORMAT" => "d.m H:i",
                             "SET_TITLE" => "N",
                             "SET_STATUS_404" => "N",
                             "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                             "ADD_SECTIONS_CHAIN" => "Y",
                             "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                             "PARENT_SECTION" => "2695",
                             "PARENT_SECTION_CODE" => "",
                             "DISPLAY_TOP_PAGER" => "N",
                             "DISPLAY_BOTTOM_PAGER" => "Y",
                             "PAGER_TITLE" => "�������",
                             "PAGER_SHOW_ALWAYS" => "Y",
                             "PAGER_TEMPLATE" => "",
                             "PAGER_DESC_NUMBERING" => "N",
                             "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                             "PAGER_SHOW_ALL" => "Y",
                             "DISPLAY_DATE" => "Y",
                             "DISPLAY_NAME" => "Y",
                             "DISPLAY_PICTURE" => "Y",
                             "DISPLAY_PREVIEW_TEXT" => "Y",
                             "AJAX_OPTION_ADDITIONAL" => ""
                         )
                     )
                 )
             )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                 "SALT" => LAZYLOAD_SALT,
                 "COMPONENTS" => array(
                     array(
                         "NAME" => "bitrix:news.list",
                         "TEMPLATE" => "moln_main",
                         "PARAMETERS" => array(
                             "IBLOCK_TYPE" => "information_part",
                             "IBLOCK_ID" => "3",
                             "NEWS_COUNT" => "4",
                             "SORT_BY1" => "ACTIVE_FROM",
                             "SORT_ORDER1" => "DESC",
                             "SORT_BY2" => "SORT",
                             "SORT_ORDER2" => "ASC",
                             "FILTER_NAME" => "",
                             "FIELD_CODE" => array("DETAIL_PICTURE", "PREVIEW_PICTURE"),
                             "PROPERTY_CODE" => array(0 => "", 1 => "",),
                             "CHECK_DATES" => "Y",
                             "DETAIL_URL" => "",
                             "AJAX_MODE" => "N",
                             "AJAX_OPTION_JUMP" => "N",
                             "AJAX_OPTION_STYLE" => "Y",
                             "AJAX_OPTION_HISTORY" => "N",
                             "CACHE_TYPE" => "A",
                             "CACHE_TIME" => "3600",
                             "CACHE_FILTER" => "N",
                             "CACHE_GROUPS" => "N",
                             "PREVIEW_TRUNCATE_LEN" => "",
                             "ACTIVE_DATE_FORMAT" => "d.m H:i",
                             "SET_TITLE" => "N",
                             "SET_STATUS_404" => "N",
                             "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                             "ADD_SECTIONS_CHAIN" => "Y",
                             "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                             "PARENT_SECTION" => "149",
                             "PARENT_SECTION_CODE" => "",
                             "DISPLAY_TOP_PAGER" => "N",
                             "DISPLAY_BOTTOM_PAGER" => "Y",
                             "PAGER_TITLE" => "�������",
                             "PAGER_SHOW_ALWAYS" => "Y",
                             "PAGER_TEMPLATE" => "",
                             "PAGER_DESC_NUMBERING" => "N",
                             "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                             "PAGER_SHOW_ALL" => "Y",
                             "DISPLAY_DATE" => "Y",
                             "DISPLAY_NAME" => "Y",
                             "DISPLAY_PICTURE" => "Y",
                             "DISPLAY_PREVIEW_TEXT" => "Y",
                             "AJAX_OPTION_ADDITIONAL" => ""
                         )
                     )
                 )
             )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                 "SALT" => LAZYLOAD_SALT,
                 "COMPONENTS" => array(
                     array(
                         "NAME" => "bitrix:news.list",
                         "TEMPLATE" => "moln_main",
                         "PARAMETERS" => array(
                             "IBLOCK_TYPE" => "information_part",
                             "IBLOCK_ID" => "3",
                             "NEWS_COUNT" => "4",
                             "SORT_BY1" => "ACTIVE_FROM",
                             "SORT_ORDER1" => "DESC",
                             "SORT_BY2" => "SORT",
                             "SORT_ORDER2" => "ASC",
                             "FILTER_NAME" => "",
                             "FIELD_CODE" => array("DETAIL_PICTURE", "PREVIEW_PICTURE"),
                             "PROPERTY_CODE" => array(0 => "", 1 => "",),
                             "CHECK_DATES" => "Y",
                             "DETAIL_URL" => "",
                             "AJAX_MODE" => "N",
                             "AJAX_OPTION_JUMP" => "N",
                             "AJAX_OPTION_STYLE" => "Y",
                             "AJAX_OPTION_HISTORY" => "N",
                             "CACHE_TYPE" => "A",
                             "CACHE_TIME" => "3600",
                             "CACHE_FILTER" => "N",
                             "CACHE_GROUPS" => "N",
                             "PREVIEW_TRUNCATE_LEN" => "",
                             "ACTIVE_DATE_FORMAT" => "d.m H:i",
                             "SET_TITLE" => "N",
                             "SET_STATUS_404" => "N",
                             "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                             "ADD_SECTIONS_CHAIN" => "Y",
                             "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                             "PARENT_SECTION" => "152",
                             "PARENT_SECTION_CODE" => "",
                             "DISPLAY_TOP_PAGER" => "N",
                             "DISPLAY_BOTTOM_PAGER" => "Y",
                             "PAGER_TITLE" => "�������",
                             "PAGER_SHOW_ALWAYS" => "Y",
                             "PAGER_TEMPLATE" => "",
                             "PAGER_DESC_NUMBERING" => "N",
                             "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                             "PAGER_SHOW_ALL" => "Y",
                             "DISPLAY_DATE" => "Y",
                             "DISPLAY_NAME" => "Y",
                             "DISPLAY_PICTURE" => "Y",
                             "DISPLAY_PREVIEW_TEXT" => "Y",
                             "AJAX_OPTION_ADDITIONAL" => ""
                         )
                     )
                 )
             )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                 "SALT" => LAZYLOAD_SALT,
                 "COMPONENTS" => array(
                     array(
                         "NAME" => "bitrix:news.list",
                         "TEMPLATE" => "moln_main",
                         "PARAMETERS" => array(
                             "IBLOCK_TYPE" => "information_part",
                             "IBLOCK_ID" => "3",
                             "NEWS_COUNT" => "4",
                             "SORT_BY1" => "ACTIVE_FROM",
                             "SORT_ORDER1" => "DESC",
                             "SORT_BY2" => "SORT",
                             "SORT_ORDER2" => "ASC",
                             "FILTER_NAME" => "",
                             "FIELD_CODE" => array("DETAIL_PICTURE", "PREVIEW_PICTURE"),
                             "PROPERTY_CODE" => array(0 => "", 1 => "",),
                             "CHECK_DATES" => "Y",
                             "DETAIL_URL" => "",
                             "AJAX_MODE" => "N",
                             "AJAX_OPTION_JUMP" => "N",
                             "AJAX_OPTION_STYLE" => "Y",
                             "AJAX_OPTION_HISTORY" => "N",
                             "CACHE_TYPE" => "A",
                             "CACHE_TIME" => "3600",
                             "CACHE_FILTER" => "N",
                             "CACHE_GROUPS" => "N",
                             "PREVIEW_TRUNCATE_LEN" => "",
                             "ACTIVE_DATE_FORMAT" => "d.m H:i",
                             "SET_TITLE" => "N",
                             "SET_STATUS_404" => "N",
                             "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                             "ADD_SECTIONS_CHAIN" => "Y",
                             "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                             "PARENT_SECTION" => "5",
                             "PARENT_SECTION_CODE" => "",
                             "DISPLAY_TOP_PAGER" => "N",
                             "DISPLAY_BOTTOM_PAGER" => "Y",
                             "PAGER_TITLE" => "�������",
                             "PAGER_SHOW_ALWAYS" => "Y",
                             "PAGER_TEMPLATE" => "",
                             "PAGER_DESC_NUMBERING" => "N",
                             "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                             "PAGER_SHOW_ALL" => "Y",
                             "DISPLAY_DATE" => "Y",
                             "DISPLAY_NAME" => "Y",
                             "DISPLAY_PICTURE" => "Y",
                             "DISPLAY_PREVIEW_TEXT" => "Y",
                             "AJAX_OPTION_ADDITIONAL" => ""
                         )
                     )
                 )
             )
         );
         ?>
      </div>
      <div class="mc_block withTitle" style="width: 674px; position:relative;">
         <div class="lenta_title_1">
            <a href="/news/">����� ���� ��������</a>
         </div>
			<!-- ad A3 -->
         <div class="lenta_title_2">
            <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
                "TYPE" => "LENTA",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "CACHE_NOTES" => ""
                    )
            );?>
         </div>
         <?
         $APPLICATION->IncludeComponent("bitrix:news.list", "main_razv", array(
             "IBLOCK_TYPE" => "information_part",
             "IBLOCK_ID" => "3",
             "NEWS_COUNT" => "10",
             "NEWS_COUNT_VISIBLE" => "6",
             "SORT_BY1" => "ACTIVE_FROM",
             "SORT_ORDER1" => "DESC",
             "SORT_BY2" => "SORT",
             "SORT_ORDER2" => "ASC",
             "FILTER_NAME" => "",
             "FIELD_CODE" => array("DETAIL_PICTURE", "PREVIEW_PICTURE"),
             "PROPERTY_CODE" => array(
                 0 => "",
                 1 => "",
             ),
             "CHECK_DATES" => "Y",
             "DETAIL_URL" => "",
             "AJAX_MODE" => "N",
             "AJAX_OPTION_JUMP" => "N",
             "AJAX_OPTION_STYLE" => "Y",
             "AJAX_OPTION_HISTORY" => "N",
             "CACHE_TYPE" => "A",
             "CACHE_TIME" => "3600",
             "CACHE_FILTER" => "N",
             "CACHE_GROUPS" => "N",
             "PREVIEW_TRUNCATE_LEN" => "",
             "ACTIVE_DATE_FORMAT" => "d.m H:i",
             "SET_TITLE" => "N",
             "SET_STATUS_404" => "N",
             "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
             "ADD_SECTIONS_CHAIN" => "Y",
             "HIDE_LINK_WHEN_NO_DETAIL" => "N",
             "PARENT_SECTION" => "",
             "PARENT_SECTION_CODE" => "",
             "DISPLAY_TOP_PAGER" => "N",
             "DISPLAY_BOTTOM_PAGER" => "Y",
             "PAGER_TITLE" => "�������",
             "PAGER_SHOW_ALWAYS" => "Y",
             "PAGER_TEMPLATE" => "",
             "PAGER_DESC_NUMBERING" => "N",
             "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
             "PAGER_SHOW_ALL" => "Y",
             "DISPLAY_DATE" => "Y",
             "DISPLAY_NAME" => "Y",
             "DISPLAY_PICTURE" => "Y",
             "DISPLAY_PREVIEW_TEXT" => "Y",
             "AJAX_OPTION_ADDITIONAL" => ""
                 ), false
         );
         ?> 
      </div><!--mc_block-->
   </div>            
   <!--VS end midl1-->
</div>    
<div class="content_box"  style=" margin:0px 20px; overflow:hidden;">    
   <div class="middle_banner" id="1" style=" width:970px; background:#cad7c5;"></div>
   <div class="middle_banner" id="1" style=" width:970px; background:#cad7c5;">
      <?/*$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
          "TYPE" => "ENERGO_A3",
          "NOINDEX" => "N",
          "CACHE_TYPE" => "N",
          "CACHE_TIME" => "0",
          "CACHE_NOTES" => ""
              )
      );*/?>
   </div>        
</div>
	<div class="content_box" style=" margin:0px 20px; overflow:hidden; padding-bottom: 10px;">
	   <div class="middle_banner" id="2">
		   <?
		         $APPLICATION->IncludeComponent("bitrix:advertising.banner", "", Array(
		             "TYPE" => "ENERGO_A2",
		             "NOINDEX" => "N",
		             "CACHE_TYPE" => "N",
		             "CACHE_TIME" => "3600",
		             "CACHE_NOTES" => ""
		                 )
		         );?>
	   </div>
	</div>
<div class="content_box">



   <!--VS right col 2 start-->
   <div class="right_col brdr_box double_brdr_box fl_right" style="  width: 298px;  margin-right: 20px;  padding-right: 0;">
      <div class="wrap"> <div class="rc_block toog withTitleMarket" style="margin-bottom: 0">
         <a href="/advert/rss/" rel="nofollow" class="icons icon_rss" title="����������� �� �����"></a>
         <div class="title"><a href="/advert/">����������</a><sup><?= GetCountElements(IBLOCK_ADVARE); ?></sup></div>
         <div class="title_list_wrap" >
            <?

            global $iadfilter; $iadfilter = array('=PROPERTY_ACTIVE'=>'Y');

            $APPLICATION->IncludeComponent("bitrix:news.list", "list_main", array(
                              "IBLOCK_TYPE" => "services",
                              "IBLOCK_ID" => IBLOCK_ADVARE,
                              "NEWS_COUNT" => "20",
                              "SORT_BY1" => "ACTIVE_FROM",
                              "SORT_ORDER1" => "DESC",
                              "SORT_BY2" => "SORT",
                              "SORT_ORDER2" => "ASC",
                              "FILTER_NAME" => "iadfilter",
                              "FIELD_CODE" => array(
                                  0 => "",
                                  1 => "",
                              ),
                              "PROPERTY_CODE" => array(
                                  0 => "",
                                  1 => "",
                              ),
                              "CHECK_DATES" => "Y",
                              "DETAIL_URL" => "",
                              "AJAX_MODE" => "N",
                              "AJAX_OPTION_JUMP" => "N",
                              "AJAX_OPTION_STYLE" => "Y",
                              "AJAX_OPTION_HISTORY" => "N",
                              "CACHE_TYPE" => "Y",
                              "CACHE_TIME" => "3600",
                              "CACHE_FILTER" => "N",
                              "CACHE_GROUPS" => "N",
                              "PREVIEW_TRUNCATE_LEN" => "",
                              "ACTIVE_DATE_FORMAT" => "d.m H:i",
                              "SET_TITLE" => "N",
                              "SET_STATUS_404" => "N",
                              "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                              "ADD_SECTIONS_CHAIN" => "Y",
                              "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                              "PARENT_SECTION" => "",
                              "PARENT_SECTION_CODE" => "",
                              "DISPLAY_TOP_PAGER" => "N",
                              "DISPLAY_BOTTOM_PAGER" => "Y",
                              "PAGER_TITLE" => "�������",
                              "PAGER_SHOW_ALWAYS" => "Y",
                              "PAGER_TEMPLATE" => "",
                              "PAGER_DESC_NUMBERING" => "N",
                              "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                              "PAGER_SHOW_ALL" => "Y",
                              "DISPLAY_DATE" => "Y",
                              "DISPLAY_NAME" => "Y",
                              "DISPLAY_PICTURE" => "Y",
                              "DISPLAY_PREVIEW_TEXT" => "Y",
                              "AJAX_OPTION_ADDITIONAL" => ""
                                  )


            );
            ?>
         </div>
         <div class="nav_row">
            <a style="margin-left: -10px;" href="/personal/advert/" class="btns fl_right"><i></i>���������� ����������</a>
            <a href="/advert/" class="nodecor bold">��� ����������</a>
            <div class="clear"></div>
         </div>
      </div></div>
   </div>
   <!--VS right_col 2 end-->
   <!--VS start midl2-->
   <div class="middle_col">



      <div class="mc_block">
         <div class=" double_brdr_box fl_left" style="min-width: 320px; min-height: 400px;">

	            <div class="rc_block toog withTitleMarket" style="margin-bottom: 0px;">
                     <ul class="mb_menu">
                        <li class="mb_title"><span>������</span><div class="sclew"></div></li>
                        <li class="sel"><a href="#1">������</a><div class="sclew"></div></li>
                        <li><a href="#3">������</a><div class="sclew"></div></li>
                     </ul>
                     <div class="gray_box catalog_list tab" style="padding-bottom:1px;">
                        <?
                        $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                            "SALT" => LAZYLOAD_SALT,
                            "COMPONENTS" => array(
                                array(
                                    "NAME" => "bitrix:news.list",
                                    "TEMPLATE" => "list_main_service",
                                    "PARAMETERS" => array(
                                          "IBLOCK_TYPE" => "services",
                                          "IBLOCK_ID" => "27",
                                          "NEWS_COUNT" => "5",
                                          "SORT_BY1" => "ACTIVE_FROM",
                                          "SORT_ORDER1" => "DESC",
                                          "SORT_BY2" => "SORT",
                                          "SORT_ORDER2" => "ASC",
                                          "FILTER_NAME" => "",
                                          "FIELD_CODE" => array(
                                              0 => "",
                                              1 => "",
                                          ),
                                          "PROPERTY_CODE" => array(
                                              0 => "PROPERTY_FIRM",
                                              1 => "",
                                          ),
                                          "CHECK_DATES" => "Y",
                                          "DETAIL_URL" => "",
                                          "AJAX_MODE" => "N",
                                          "AJAX_OPTION_JUMP" => "N",
                                          "AJAX_OPTION_STYLE" => "Y",
                                          "AJAX_OPTION_HISTORY" => "N",
                                          "CACHE_TYPE" => "A",
                                          "CACHE_TIME" => "3600",
                                          "CACHE_FILTER" => "N",
                                          "CACHE_GROUPS" => "Y",
                                          "PREVIEW_TRUNCATE_LEN" => "",
                                          "ACTIVE_DATE_FORMAT" => "d.m H:i",
                                          "SET_TITLE" => "N",
                                          "SET_STATUS_404" => "N",
                                          "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                          "ADD_SECTIONS_CHAIN" => "Y",
                                          "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                          "PARENT_SECTION" => "",
                                          "PARENT_SECTION_CODE" => "",
                                          "DISPLAY_TOP_PAGER" => "N",
                                          "DISPLAY_BOTTOM_PAGER" => "Y",
                                          "PAGER_TITLE" => "�������",
                                          "PAGER_SHOW_ALWAYS" => "Y",
                                          "PAGER_TEMPLATE" => "",
                                          "PAGER_DESC_NUMBERING" => "N",
                                          "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                          "PAGER_SHOW_ALL" => "Y",
                                          "DISPLAY_DATE" => "Y",
                                          "DISPLAY_NAME" => "Y",
                                          "DISPLAY_PICTURE" => "Y",
                                          "DISPLAY_PREVIEW_TEXT" => "Y",
                                          "AJAX_OPTION_ADDITIONAL" => ""
                                              )
                                   )
                               )
                           )
                        );
                        ?>
                        <div class="nav_row">
                           <a class="btns fl_right" href="/personal/company/"><i></i>���������� ������</a>
                           <a href="/market/" class="nodecor bold">��� ������</a><sup><?= GetCountElements(27); ?></sup>
                           <div class="clear"></div>
                        </div>
                     </div>
                     <div class="gray_box catalog_list tab" style="padding-bottom: 1px;">
                        <?
                        $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                            "SALT" => LAZYLOAD_SALT,
                            "COMPONENTS" => array(
                                array(
                                    "NAME" => "bitrix:news.list",
                                    "TEMPLATE" => "list_main_service",
                                    "PARAMETERS" => array(
                                          "IBLOCK_TYPE" => "services",
                                          "IBLOCK_ID" => "28",
                                          "NEWS_COUNT" => "5",
                                          "SORT_BY1" => "ACTIVE_FROM",
                                          "SORT_ORDER1" => "DESC",
                                          "SORT_BY2" => "SORT",
                                          "SORT_ORDER2" => "ASC",
                                          "FILTER_NAME" => "",
                                          "FIELD_CODE" => array(
                                              0 => "",
                                              1 => "",
                                          ),
                                          "PROPERTY_CODE" => array(
                                              0 => "PROPERTY_FIRM",
                                              1 => "",
                                          ),
                                          "CHECK_DATES" => "Y",
                                          "DETAIL_URL" => "",
                                          "AJAX_MODE" => "N",
                                          "AJAX_OPTION_JUMP" => "N",
                                          "AJAX_OPTION_STYLE" => "Y",
                                          "AJAX_OPTION_HISTORY" => "N",
                                          "CACHE_TYPE" => "A",
                                          "CACHE_TIME" => "3600",
                                          "CACHE_FILTER" => "N",
                                          "CACHE_GROUPS" => "Y",
                                          "PREVIEW_TRUNCATE_LEN" => "",
                                          "ACTIVE_DATE_FORMAT" => "d.m H:i",
                                          "SET_TITLE" => "N",
                                          "SET_STATUS_404" => "N",
                                          "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                          "ADD_SECTIONS_CHAIN" => "Y",
                                          "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                          "PARENT_SECTION" => "",
                                          "PARENT_SECTION_CODE" => "",
                                          "DISPLAY_TOP_PAGER" => "N",
                                          "DISPLAY_BOTTOM_PAGER" => "Y",
                                          "PAGER_TITLE" => "�������",
                                          "PAGER_SHOW_ALWAYS" => "Y",
                                          "PAGER_TEMPLATE" => "",
                                          "PAGER_DESC_NUMBERING" => "N",
                                          "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                          "PAGER_SHOW_ALL" => "Y",
                                          "DISPLAY_DATE" => "Y",
                                          "DISPLAY_NAME" => "Y",
                                          "DISPLAY_PICTURE" => "Y",
                                          "DISPLAY_PREVIEW_TEXT" => "Y",
                                          "AJAX_OPTION_ADDITIONAL" => ""
                                              )
                                   )
                               )
                           )
                        );
                        ?>
                        <div class="nav_row">
                           <a class="btns fl_right" href="/personal/company/"><i></i>���������� ������</a>
                           <a href="/services/" class="nodecor bold">��� ������</a><sup><?= GetCountElements(28); ?></sup>
                           <div class="clear"></div>
                        </div>
                     </div>
                  </div><!--rc_block-->


         </div>
         <div class=" double_brdr_box fl_right">
	         <div class="rc_block toog " style="margin-bottom: 0px; min-width: 320px; min-height: 400px;">
                             <ul class="mb_menu">
                                <li class="mb_title"><span>��������</span><div class="sclew"></div></li>
                             </ul>

                             <div class="gray_box catalog_list tab" style="padding-bottom: 1px;">
                                <?
                                $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                                    "SALT" => LAZYLOAD_SALT,
                                    "COMPONENTS" => array(
                                        array(
                                            "NAME" => "bitrix:news.list",
                                            "TEMPLATE" => "list_main_kompany",
                                            "PARAMETERS" => array(
                                                  "IBLOCK_TYPE" => "directories",
                                                  "IBLOCK_ID" => "17",
                                                  "NEWS_COUNT" => "6",
                                                  "SORT_BY1" => "PROPERTY_EXT_SORT",
                                                  "SORT_ORDER1" => "DESC",
                                                  "SORT_BY2" => "SORT",
                                                  "SORT_ORDER2" => "ASC",
                                                  "FILTER_NAME" => "",
                                                  "FIELD_CODE" => array(
                                                      0 => "",
                                                      1 => "",
                                                  ),
                                                  "PROPERTY_CODE" => array(
                                                      0 => "PROPERTY_FIRM",
                                                      1 => "",
                                                  ),
                                                  "CHECK_DATES" => "Y",
                                                  "DETAIL_URL" => "",
                                                  "AJAX_MODE" => "N",
                                                  "AJAX_OPTION_JUMP" => "N",
                                                  "AJAX_OPTION_STYLE" => "Y",
                                                  "AJAX_OPTION_HISTORY" => "N",
                                                  "CACHE_TYPE" => "A",
                                                  "CACHE_TIME" => "3600",
                                                  "CACHE_FILTER" => "N",
                                                  "CACHE_GROUPS" => "Y",
                                                  "PREVIEW_TRUNCATE_LEN" => "",
                                                  "ACTIVE_DATE_FORMAT" => "d.m H:i",
                                                  "SET_TITLE" => "N",
                                                  "SET_STATUS_404" => "N",
                                                  "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                                  "ADD_SECTIONS_CHAIN" => "Y",
                                                  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                  "PARENT_SECTION" => "",
                                                  "PARENT_SECTION_CODE" => "",
                                                  "DISPLAY_TOP_PAGER" => "N",
                                                  "DISPLAY_BOTTOM_PAGER" => "Y",
                                                  "PAGER_TITLE" => "�������",
                                                  "PAGER_SHOW_ALWAYS" => "Y",
                                                  "PAGER_TEMPLATE" => "",
                                                  "PAGER_DESC_NUMBERING" => "N",
                                                  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                  "PAGER_SHOW_ALL" => "Y",
                                                  "DISPLAY_DATE" => "Y",
                                                  "DISPLAY_NAME" => "Y",
                                                  "DISPLAY_PICTURE" => "Y",
                                                  "DISPLAY_PREVIEW_TEXT" => "Y",
                                                  "AJAX_OPTION_ADDITIONAL" => ""
                                                      )
                                           )
                                       )
                                   )
                                );
                                ?>
                                <div class="nav_row">
                                   <a class="btns fl_right" href="/personal/company/"><i></i>���������� ��������</a>
                                   <a href="/company/" class="nodecor bold">��� ��������</a><sup><?= GetCountElements(17); ?></sup>
                                   <div class="clear"></div>
                                </div>
                             </div>
                          </div><!--rc_block-->
         </div>
         <div class="clear"></div>
      </div><!--mc_block-->
   </div>
   <!--VS end midl2-->
</div>          
<div class="content_box" style=" margin:0px 20px; overflow:hidden; padding-bottom: 10px;">
   <div class="middle_banner" id="2">
		<!--style="height:60px; background:#cad7c5;"-->
      <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			".default",
			array(
				"TYPE" => "A7",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "0",
				"CACHE_NOTES" => ""
			),
			false
		);
      ?>
   </div>
</div>
<div class="content_box">
   <!--VS right col 3 start-->

   <div class="right_col">
	   <div class="rc_block brdr_box">
	           <div class="wrap forum_list">
	              <div class="title"><a href="/blogs/">��������� �������</a></div>
	              <?  $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
	                  "SALT" => LAZYLOAD_SALT,
	                  "COMPONENTS" => array(
	                      array(
	                          "NAME" => "zvv:news.line",
	                          "TEMPLATE" => "bloglist-mainpage",
	                          "PARAMETERS" => array(
				                "IBLOCK_TYPE" => "information_part",
				                "IBLOCKS" => Array(50),
				                "NEWS_COUNT" => "3",
				                "FIELD_CODE" => Array("ID", "CODE"),
				                "SORT_BY1" => "ACTIVE_FROM",
				                "SORT_ORDER1" => "DESC",
				                "SORT_BY2" => "DATE_CREATE",
				                "SORT_ORDER2" => "DESC",
				                "DETAIL_URL" => "",
				                "ACTIVE_DATE_FORMAT" => "d.m.y",
				                "CACHE_TYPE" => "A",
				                "CACHE_TIME" => "3600",
				                "CACHE_GROUPS" => "N"
	                             )
	                         )
	                     )
	                 )
	              );
	              ?>
	           </div>
	        </div>

      <div class="rc_block brdr_box">

         <?$APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.index",
                     "TEMPLATE" => "faces_right",
                     "PARAMETERS" => array(
                           "IBLOCK_TYPE" => "information_part", 
                           "IBLOCKS" => Array("29", "33"), 
                           "NEWS_COUNT" => "3",
                           "IBLOCK_SORT_BY" => "SORT", 
                           "IBLOCK_SORT_ORDER" => "ASC", 
                           "SORT_BY1" => "date_active_from", 
                           "SORT_ORDER1" => "DESC", 
                           "SORT_BY2" => "ID", 
                           "SORT_ORDER2" => "ASC", 
                           "FIELD_CODE" => Array('PREVIEW_PICTURE', 'DETAIL_PICTURE'), 
                           "PROPERTY_CODE" => Array(), 
                           "FILTER_NAME" => "", 
                           "IBLOCK_URL" => "news.php?ID=#IBLOCK_ID#", 
                           "DETAIL_URL" => "news_detail.php?ID=#ELEMENT_ID#", 
                           "ACTIVE_DATE_FORMAT" => "d-m-Y", 
                           "CACHE_TYPE" => "Y", 
                           "CACHE_TIME" => "3600",
                           "CACHE_GROUPS" => "Y" 
                               )
                    )
                )
            )
         );?>
      </div><!--rc_block-->

      <div class="rc_block brdr_box">
         <div class="wrap forum_list">
            <div class="title"><a href="/forum/">����� �� ������</a></div>
            <?
            $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
                "SALT" => LAZYLOAD_SALT,
                "COMPONENTS" => array(
                    array(
                        "NAME" => "bitrix:forum.topic.last",
                        "TEMPLATE" => "energo",
                        "PARAMETERS" => array(
                              "FID" => array(3,4,5,18,21,22,26,27,28,29,31,32,33,34,35,36,37,38,39
                              ),
                              "SORT_BY" => "LAST_POST_DATE",
                              "SORT_ORDER" => "DESC",
                              "SORT_BY_SORT_FIRST" => "N",
                              "URL_TEMPLATES_INDEX" => "/forum/",
                              "URL_TEMPLATES_LIST" => "/forum/forum#FID#/",
                              "URL_TEMPLATES_READ" => "/forum/forum#FID#/topic#TID#/",
                              "URL_TEMPLATES_MESSAGE" => "/forum/forum#FID#/topic#TID#/message#MID#/",
                              "URL_TEMPLATES_PROFILE_VIEW" => "profile_view.php?UID=#UID#",
                              "CACHE_TYPE" => "A",
                              "CACHE_TIME" => "3600",
                              "TOPICS_PER_PAGE" => "4",
                              "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
                              "SHOW_FORUM_ANOTHER_SITE" => "N",
                              "SET_TITLE" => "N",
                              "SET_NAVIGATION" => "N",
                              "PAGER_DESC_NUMBERING" => "N",
                              "PAGER_SHOW_ALWAYS" => "N",
                              "PAGER_TITLE" => "����",
                              "PAGER_TEMPLATE" => "",
                              "SHOW_NAV" => array(
                                  0 => "BOTTOM",
                              ),
                              "SHOW_COLUMNS" => array(
                                  0 => "USER_START_NAME",
                                  1 => "POSTS",
                                  2 => "VIEWS",
                                  3 => "LAST_POST_DATE",
                              ),
                              "SHOW_SORTING" => "N",
                              "SEPARATE" => "� ������ #FORUM#"
                                  )
                       )
                   )
               )
            );
            ?>
         </div>
      </div><!--rc_block-->
      <!--<div class="rc_block brdr_box"> 
         <div class="wrap"> 
          <div class="title"><a href="/video/" class="font_18" >�����</a><sup><? //=GetCountElements(34);   ?></sup></div>
          <div class="video_box">-->
            <?
            //$APPLICATION->IncludeComponent("bitrix:photo.random", "energo", Array(
            //"IBLOCK_TYPE" => "media_content",    // ��� ���������
            //"IBLOCKS" => array(    // ��������
            //    0 => "34",
            //),
            //"PARENT_SECTION" => "",    // ID �������
            //"DETAIL_URL" => "",    // URL, ������� �� �������� � ���������� �������� �������
            //"CACHE_TYPE" => "A",    // ��� �����������
            //"CACHE_TIME" => "180",    // ����� ����������� (���.)
            //"CACHE_NOTES" => "",
            //"CACHE_GROUPS" => "Y",    // ��������� ����� �������
            //),
            //false
      //);
            ?>
            <!--</div>
          <div class="nav_row"> <a href="/video/" class="nodecor bold" >��� �����</a></div>
         </div>
      </div> --><!-- rc_block -->
   </div>
   <!--VS right col 3 end-->
   <!--VS start midl3-->
   <div class="middle_col">
      <div class="mc_block withTitleArt">
         <ul class="mb_menu">
            <li class="mb_title sel"><a href="javascript: void(0)">���������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">������������ ����������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">��������������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">����������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">����������������</a><div class="sclew"></div></li>
            <li><a href="javascript: void(0)">��������</a><div class="sclew"></div></li>
         </ul>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part",
                           "IBLOCK_ID" => 2,
                           "NEWS_COUNT" => 6,
                           "SORT_BY1" => "ACTIVE_FROM",
                           "SORT_ORDER1" => "DESC",
                           "SORT_BY2" => "SORT",
                           "SORT_ORDER2" => "ASC",
                           "FILTER_NAME" => "",
                           "FIELD_CODE" => array("DETAIL_TEXT", "DETAIL_PICTURE", "PREVIEW_PICTURE"),
                           "PROPERTY_CODE" => array(),
                           "CHECK_DATES" => "Y",
                           "DETAIL_URL" => "",
                           "AJAX_MODE" => "N",
                           "AJAX_OPTION_JUMP" => "N",
                           "AJAX_OPTION_STYLE" => "Y",
                           "AJAX_OPTION_HISTORY" => "N",
                           "CACHE_TYPE" => "Y",
                           "CACHE_TIME" => "3600",
                           "CACHE_FILTER" => "N",
                           "CACHE_GROUPS" => "N",
                           "PREVIEW_TRUNCATE_LEN" => "",
                           "ACTIVE_DATE_FORMAT" => "d.m H:i",
                           "SET_TITLE" => "N",
                           "SET_STATUS_404" => "N",
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                           "ADD_SECTIONS_CHAIN" => "N",
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                           "PARENT_SECTION" => "",
                           "PARENT_SECTION_CODE" => "",
                           "DISPLAY_TOP_PAGER" => "N",
                           "DISPLAY_BOTTOM_PAGER" => "N",
                           "PAGER_TITLE" => "�������",
                           "PAGER_SHOW_ALWAYS" => "Y",
                           "PAGER_TEMPLATE" => "",
                           "PAGER_DESC_NUMBERING" => "N",
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                           "PAGER_SHOW_ALL" => "Y",
                           "DISPLAY_DATE" => "Y",
                           "DISPLAY_NAME" => "Y",
                           "DISPLAY_PICTURE" => "Y",
                           "DISPLAY_PREVIEW_TEXT" => "Y",
                           "AJAX_OPTION_ADDITIONAL" => ""
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "2", // ��� ��������������� �����
                           "NEWS_COUNT" => "6", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                               2 => "PREVIEW_PICTURE"
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "3600", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "N", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "N", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "2720", // ID �������
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "2", // ��� ��������������� �����
                           "NEWS_COUNT" => "6", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                               2 => "PREVIEW_PICTURE"
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "3600", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "N", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "N", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "188", // ID �������
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "2", // ��� ��������������� �����
                           "NEWS_COUNT" => "6", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                               2 => "PREVIEW_PICTURE"
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "3600", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "N", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "N", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "2719", // ID �������
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "2", // ��� ��������������� �����
                           "NEWS_COUNT" => "6", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                               2 => "PREVIEW_PICTURE"
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "Y", // ��� �����������
                           "CACHE_TIME" => "3600", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "N", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "N", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "186", // ID �������
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
         <?
         $APPLICATION->IncludeComponent("whipstudio:lazyload", "", array(
             "SALT" => LAZYLOAD_SALT,
             "ONLOAD_SCRIPT" => "lazyload_withTitleArt();",
             "COMPONENTS" => array(
                 array(
                     "NAME" => "bitrix:news.list",
                     "TEMPLATE" => "articles_main",
                     "PARAMETERS" => Array(
                           "IBLOCK_TYPE" => "information_part", // ��� ��������������� ����� (������������ ������ ��� ��������)
                           "IBLOCK_ID" => "2", // ��� ��������������� �����
                           "NEWS_COUNT" => "6", // ���������� �������� �� ��������
                           "SORT_BY1" => "ACTIVE_FROM", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER1" => "DESC", // ����������� ��� ������ ���������� ��������
                           "SORT_BY2" => "SORT", // ���� ��� ������ ���������� ��������
                           "SORT_ORDER2" => "ASC", // ����������� ��� ������ ���������� ��������
                           "FILTER_NAME" => "", // ������
                           "FIELD_CODE" => array(// ����
                               0 => "DETAIL_TEXT",
                               1 => "DETAIL_PICTURE",
                               2 => "PREVIEW_PICTURE"
                           ),
                           "PROPERTY_CODE" => array(// ��������
                               0 => "",
                               1 => "",
                           ),
                           "CHECK_DATES" => "Y", // ���������� ������ �������� �� ������ ������ ��������
                           "DETAIL_URL" => "", // URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                           "AJAX_MODE" => "N", // �������� ����� AJAX
                           "AJAX_OPTION_JUMP" => "N", // �������� ��������� � ������ ����������
                           "AJAX_OPTION_STYLE" => "Y", // �������� ��������� ������
                           "AJAX_OPTION_HISTORY" => "N", // �������� �������� ��������� ��������
                           "CACHE_TYPE" => "A", // ��� �����������
                           "CACHE_TIME" => "3600", // ����� ����������� (���.)
                           "CACHE_FILTER" => "N", // ���������� ��� ������������� �������
                           "CACHE_GROUPS" => "N", // ��������� ����� �������
                           "PREVIEW_TRUNCATE_LEN" => "", // ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                           "ACTIVE_DATE_FORMAT" => "d.m H:i", // ������ ������ ����
                           "SET_TITLE" => "N", // ������������� ��������� ��������
                           "SET_STATUS_404" => "N", // ������������� ������ 404, ���� �� ������� ������� ��� ������
                           "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // �������� �������� � ������� ���������
                           "ADD_SECTIONS_CHAIN" => "Y", // �������� ������ � ������� ���������
                           "HIDE_LINK_WHEN_NO_DETAIL" => "N", // �������� ������, ���� ��� ���������� ��������
                           "PARENT_SECTION" => "193", // ID �������
                           "PARENT_SECTION_CODE" => "", // ��� �������
                           "DISPLAY_TOP_PAGER" => "N", // �������� ��� �������
                           "DISPLAY_BOTTOM_PAGER" => "Y", // �������� ��� �������
                           "PAGER_TITLE" => "�������", // �������� ���������
                           "PAGER_SHOW_ALWAYS" => "Y", // �������� ������
                           "PAGER_TEMPLATE" => "", // �������� �������
                           "PAGER_DESC_NUMBERING" => "N", // ������������ �������� ���������
                           "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // ����� ����������� ������� ��� �������� ���������
                           "PAGER_SHOW_ALL" => "Y", // ���������� ������ "���"
                           "DISPLAY_DATE" => "Y", // �������� ���� ��������
                           "DISPLAY_NAME" => "Y", // �������� �������� ��������
                           "DISPLAY_PICTURE" => "Y", // �������� ����������� ��� ������
                           "DISPLAY_PREVIEW_TEXT" => "Y", // �������� ����� ������
                           "AJAX_OPTION_ADDITIONAL" => "", // �������������� �������������
                               )
                    )
                )
            )
         );
         ?>
      </div>
   </div>
   <!--VS end midl3-->
</div>
<!--<div class="middle_banner" id="3" style="height:60px;" style=" margin:0px 20px; overflow:hidden;">-->
<!-- </div> -->
<!-- (C)2000-2015 Gemius SA - gemiusTraffic / ver 11.1 / Glavnaja stranitsa sajta --> 
<!-- (C)2000-2015 Gemius SA - gemiusTraffic / ver 11.1 / Glavnaja stranitsa sajta --> 
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var gemius_identifier = new String('zN6QAjRpd9pcg51yrKVx6uVDnH7F0hijti9OJuP23NP.S7');
//--><!]]>
</script>
<script type="text/javascript" src="http://energobelarus.by/js/gemius.js"></script> 







<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>