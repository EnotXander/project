<?php
/**
 * User: ZvvAsyA
 * Date: 23.09.15 -  14:14
 */
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include.php");
if(!CModule::IncludeModule("iblock")) return;

$ID = intval($_REQUEST['ID']);

$arFilter   = array( 'ID' =>$ID);
$rsElements = CIBlockElement::GetList(
	array(),//
	$arFilter,
	false,false,
	array(
		"ID",
		"NAME"
	)
);
if ( $arElements = $rsElements->Fetch() ) {

	//print_r($arElements);

	$arElements['NAME'] = html_entity_decode(iconv("cp1251", "UTF-8", $arElements['NAME']));

	echo json_encode(array('text'=>'found', 'info'=>$arElements));
} else {
	echo json_encode(array('text'=>'not found'));
}