<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

global $APPLICATION;
$APPLICATION->AddHeadString('<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />', true);
$APPLICATION->AddHeadString('<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="https://www.google.com/jsapi"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="http://'.SITE_SERVER_NAME.SITE_TEMPLATE_PATH.'/js/underscore.js"></script>', true);

$yaMetrika = YandexMetrika::getInstance(array(
            "ID" => METRIKA_CLIENT_ID,
            "SECRET" => METRIKA_CLIENT_SECRET,
            "USERNAME" => METRIKA_USERNAME,
            "PASSWORD" => METRIKA_PASSWORD
        ));
?>


<script>
   google.load("visualization", "1", {packages: ["corechart"]});
   
   //statistic
   var statistic = {
      //������ � ���������
      getVisits: {
         method: 'GET',
         url: '/stat/traffic/summary',
         dataFunc: function(request){
            return $.extend({}, request, {
               group: $('.js-grafic-visits input[name=group]:checked').val()
            });
         },
         grafic: {
            chart: function(objData){
               var visibleStat = {
                  visit: $('.js-grafic-visits input[name=visit]:checked').length ? true : false,
                  view: $('.js-grafic-visits input[name=view]:checked').length ? true : false
               }
               
               var objData = objData.data;
               console.log('draw', objData);

               var dataTable = new google.visualization.DataTable();//������������� ������ ������� ������
               dataTable.addColumn('date', 'Date');
               if(visibleStat.visit) dataTable.addColumn('number', '������');
               if(visibleStat.view) dataTable.addColumn('number', '���������');

               //���������� ������ � �������//
               for (i = 0; i < (objData.length); i++)
               {
                  var added = [new Date(objData[i].date.substr(0, 4), objData[i].date.substr(4, 2), objData[i].date.substr(6, 2))];
                  if(visibleStat.visit) added.push(objData[i].visits);
                  if(visibleStat.view) added.push(objData[i].page_views);
                  dataTable.addRow(added);
               }

               var dataView = new google.visualization.DataView(dataTable);
               //dataView.setColumns([{calc: function(data, row) { return data.getFormattedValue(row, 0); }, type:'string'}, 1]);
               var chart = new google.visualization.LineChart(document.getElementById('grafic_visits_chart'));
               var options = {//��������� ���������
                  title: '����������', //��������� �������
                  fontSize: 12,
                  height: 300,
                  width: 680,
                  lineWidth: 2,
                  chartArea: {height: 200, width: 460},
                  axisTitlesPosition: 'out', // ��������� �������� ����
                  backgroundColor: 'white', // ���� ����
                  curveType: 'none', //������������ �����. �� ���������: 'none'
                  hAxis: {format: 'MMM d, y', direction: 1, title: '����', titleTextStyle: {color: 'black', fontSize: 14, bold: 'true'}}, //��������� �������������� ���
                  vAxis: {title: '����������', titleTextStyle: {color: 'black', fontSize: 14, bold: 'true'}},
                  legend: {position: 'right', textStyle: {fontSize: 12}}
               };

               chart.draw(dataTable, options);
               console.log('draw end');
            }
         }
      },
      
      //��������� ���������
      getSources: {
         method: 'GET',
         url: '/stat/sources/summary',
         //template: _.template($('#grafic-sources-template').html()),
         templateSelector: '#grafic-sources-template',
         grafic: {
            list: function(objData){
               console.log('list', this, objData, $('#grafic-sources-template').html());
               $('#grafic_sources_list').html(this.template({data: objData.data}));
            },
            drawChart: function(objData) {
               objData = objData.data;
               var dataArray=[ ['System', 'Percent'] ];

                      //for(i = 0; i < objData.length; i++){  
                        dataArray.push([objData[0].name, objData[0].visits]);
                        dataArray.push([objData[1].name, objData[1].visits]);
                        dataArray.push([objData[2].name, objData[2].visits]);
                        dataArray.push([objData[4].name, objData[4].visits]);
                      //}
                 var dataTable = google.visualization.arrayToDataTable(dataArray);
                 var options = {
                   title: '��������� ���������',
                   pieHole: 0.4,
                   sliceVisibilityThreshold: 0//�������, ���� �������� ����� ������������ � Other
                 };

               var chart = new google.visualization.PieChart(document.getElementById('grafic_sources_chart'));
               chart.draw(dataTable, options);
            }
         }
      },
           
      //��������� �� �������
      getCountries: {
         method: 'GET',
         url: '/stat/geo',
         //template: _.template($('#grafic-sources-template').html()),
         templateSelector: '#grafic-countries-template',
         grafic: {
            list: function(objData){
               console.log('list', this, objData, $('#grafic-countries-template').html());
               $('#grafic_countries_list').html(this.template({data: objData.data}));
            },
            drawChart: function(objData) {
               var objData=objData.data;
               var dataArray=[ ['Country', 'Percent'] ];

                      for(i = 0; i < objData.length; i++){  
                        dataArray.push([objData[i].name, objData[i].visits]);
                      }
                 var dataTable = google.visualization.arrayToDataTable(dataArray);
                 var options = {
                   title: '��������� �� �������',
                   pieHole: 0.4,
                   sliceVisibilityThreshold: 0.003//�������, ���� �������� ����� ������������ � Other
                 };

               var chart = new google.visualization.PieChart(document.getElementById('grafic_countries_chart'));
               chart.draw(dataTable, options);
            }
         }
      },
           
      //��������� �������
      getSearchSystem: {
         method: 'GET',
         url: '/stat/sources/search_engines',
         //template: _.template($('#grafic-sources-template').html()),
         templateSelector: '#grafic-search-template',
         grafic: {
            list: function(objData){
               console.log('list', this, objData, $('#grafic-search-template').html());
               $('#grafic_search_list').html(this.template({data: objData.data}));
            },
            drawChart: function(objData) {
               objData = objData.data;
               var dataArray=[ ['System', 'Percent'] ];

                      for(i = 0; i < objData.length; i++){  
                        dataArray.push([objData[i].name, objData[i].visits]);
                      }
                 var dataTable = google.visualization.arrayToDataTable(dataArray);
                 var options = {
                   title: '��������� �������',
                   pieHole: 0.4,
                   sliceVisibilityThreshold: 0.03//�������, ���� �������� ����� ������������ � Other
                 };

               var chart = new google.visualization.PieChart(document.getElementById('grafic_search_chart'));
               chart.draw(dataTable, options);
            }
         }
      },
      
      //��������� �����
      getSearchPhrases: {
         method: 'GET',
         url: '/stat/sources/phrases',
         dataFunc: function(request){
            /*var localRequest = request;
            localRequest.per_page = $('.js-grafic-phrases-more').attr('data-per-page');
            console.log('request', localRequest);
            return localRequest*/
            return $.extend({}, request, {
               per_page: $('.js-grafic-phrases-more').attr('data-per-page')
            });
         },
         //template: _.template($('#grafic-sources-template').html()),
         templateSelector: '#grafic-phrases-template',
         grafic: {
            list: function(objData){
               console.log('list', this, objData, $('#grafic-phrases-template').html());
               $('#grafic_phrases_list').html(this.template({data: objData.data}));
            },
            drawChart: function(objData) {
               objData = objData.data;
               var dataArray=[ ['Phrase', 'Visits'] ];

                        for(i = 0; i < objData.length; i++){  
                          dataArray.push([objData[i].phrase, objData[i].visits]);
                        }
                   var dataTable = google.visualization.arrayToDataTable(dataArray);
                   var options = {
                     title: '��������� �����',
                     pieHole: 0.4,
                     /*legend: {
                        maxLines: $('.js-grafic-phrases-more').attr('data-more')
                     },*/
                     //sliceVisibilityThreshold: 0.03//�������, ���� �������� ����� ������������ � Other
                   };

                 var chart = new google.visualization.PieChart(document.getElementById('grafic_phrases_chart'));
                 chart.draw(dataTable, options);
            },
            moreControl: function(objData){
               var current = $('.js-grafic-phrases-more').attr('data-per-page');
               console.log(objData.data.length, current);
               if(objData.data.length < current) $('.js-grafic-phrases-more').hide();
            }
         }
      }
   }
   
   //��������
   var statisticHandler = {
      counter: {},
      onload: function() {
         //datepickers
         var datePickerOptions = {
            dateFormat: 'dd MM yy',
            monthNames: [ "������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������", "�������" ],
            altFormat: 'yymmdd',
            maxDate: -1,
            onSelect: statisticHandler.reloadAll
         }
         datePickerOptions.altField = '.js-dateinput-from';
         datePickerOptions.defaultDate = '-1m';
         datePickerOptions.setDate = '-1m';
         $('.js-datepicker-from').datepicker(datePickerOptions).datepicker( "setDate", datePickerOptions.setDate);
         datePickerOptions.altField = '.js-dateinput-to';
         datePickerOptions.defaultDate = -1;
         datePickerOptions.setDate = -1;
         $('.js-datepicker-to').datepicker(datePickerOptions).datepicker( "setDate", datePickerOptions.setDate);
         $('.js-statistic-reload').live('click', statisticHandler.reloadAll);
         for(var index in statistic){
            console.log('search template in', index);
            if(typeof statistic[index].templateSelector != 'undefined'){
               var htmlTemplate = $(statistic[index].templateSelector).html();
               console.log('html', htmlTemplate);
               statistic[index].template = _.template(htmlTemplate);
               console.log('template');
            }
         }
         statisticHandler.reloadAll();
      },
      reloadAll: function(){
         statisticHandler.counter = {
            date1: $('.js-dateinput-from').val(),
            date2: $('.js-dateinput-to').val(),
            id: $('.js-statistic-reload').attr('data-counter-id'),
            oauth_token: $('.js-statistic-reload').attr('data-oauth-token')
         }
         for(var statName in statistic){
            statisticHandler.reload(statName);
         }
      },
      reloadLocal: function(statName){
         var stat = statistic[statName];
         for(var index2 in stat.grafic){
            console.log('local', index2, stat.cachedData);
            stat.grafic[index2](stat.cachedData);
         }
      },
      reload: function(statName){
         var counter = statisticHandler.counter;
         if(counter.date1.length && counter.date2.length){
            var stat = statistic[statName];
            var dataFunc = function(cnt){return cnt;}
            if(typeof stat.dataFunc == 'function') dataFunc = stat.dataFunc
            console.log('ajax', stat.url, dataFunc(counter));
            $.ajax({
               type: stat.method,
               url: 'http://api-metrika.yandex.ru'+stat.url+'.json',
               data: dataFunc(counter),
               dataType: 'jsonp',
               success: function(responce){
                  if($.isArray(responce.errors) && responce.errors.length){
                     console.log('������ �� �������:', responce.errors);
                  }else{
                     stat.cachedData = responce;
                     for(var index2 in stat.grafic){
                        console.log('callback', index2, responce);
                        $.proxy(stat.grafic[index2], stat)(responce);
                        console.log('callback end', index2, responce);
                     }
                  }
               }
            });
         }
      }
   }
   
   $(statisticHandler.onload);
</script>

<div style="display: none">
   <script type="text/template" id="grafic-sources-template">
      <table>
         <thead>
            <tr>
               <td>���������</td>
               <td>������</td>
               <td>���������</td>
               <td>������</td>
               <td>������� ���������</td>
               <td>����� �� �����</td>
               <td>����������� ������</td>
            </tr>
         <thead>
         <tbody>
            <%for(var index in data){%>
               <tr>
                  <td><%=data[index]['name']%></td>
                  <td><%=data[index]['visits']%></td>
                  <td><%=data[index]['page_views']%></td>
                  <td><%=data[index]['denial']%></td>
                  <td><%=data[index]['depth']%></td>
                  <td><%=data[index]['visit_time']%></td>
                  <td><%=data[index]['visits_delayed']%></td>
               </tr>
            <%}%>
         </tbody>
      </table>
   </script>
   <script type="text/template" id="grafic-countries-template">
      <table>
         <thead>
            <tr>
               <td>���������</td>
               <td>������</td>
               <td>���������</td>
               <td>������</td>
               <td>������� ���������</td>
               <td>����� �� �����</td>
            </tr>
         <thead>
         <tbody>
            <%for(var index in data){%>
               <tr>
                  <td><%=data[index]['name']%></td>
                  <td><%=data[index]['visits']%></td>
                  <td><%=data[index]['page_views']%></td>
                  <td><%=data[index]['denial']%></td>
                  <td><%=data[index]['depth']%></td>
                  <td><%=data[index]['visit_time']%></td>
               </tr>
            <%}%>
         </tbody>
      </table>
   </script>
   <script type="text/template" id="grafic-search-template">
      <table>
         <thead>
            <tr>
               <td>���������</td>
               <td>������</td>
               <td>���������</td>
               <td>������</td>
               <td>������� ���������</td>
               <td>����� �� �����</td>
               <td>����������� ������</td>
            </tr>
         <thead>
         <tbody>
            <%for(var index in data){%>
               <tr>
                  <td><%=data[index]['name']%></td>
                  <td><%=data[index]['visits']%></td>
                  <td><%=data[index]['page_views']%></td>
                  <td><%=data[index]['denial']%></td>
                  <td><%=data[index]['depth']%></td>
                  <td><%=data[index]['visit_time']%></td>
                  <td><%=data[index]['visits_delayed']%></td>
               </tr>
            <%}%>
         </tbody>
      </table>
   </script>
   <script type="text/template" id="grafic-phrases-template">
      <table>
         <thead>
            <tr>
               <td>���������</td>
               <td>������</td>
               <td>���������</td>
               <td>������</td>
               <td>������� ���������</td>
               <td>����� �� �����</td>
            </tr>
         <thead>
         <tbody>
            <%for(var index in data){%>
               <tr>
                  <td><%=data[index]['phrase']%></td>
                  <td><%=data[index]['visits']%></td>
                  <td><%=data[index]['page_views']%></td>
                  <td><%=data[index]['denial']%></td>
                  <td><%=data[index]['depth']%></td>
                  <td><%=data[index]['visit_time']%></td>
               </tr>
            <%}%>
         </tbody>
      </table>
   </script>
</div>

<input type="text" class="js-datepicker js-datepicker-from" data-input="js-dateinput-from" />
<input type="text" class="js-datepicker js-datepicker-to" data-input="js-dateinput-to" />
<input type="hidden" class="js-dateinput js-dateinput-from" />
<input type="hidden" class="js-dateinput js-dateinput-to" />
<input type="button" class="js-statistic-reload" data-counter-id="22164409" data-oauth-token="<?= $yaMetrika->token ?>" value="��������" />

<div class="js-grafic-visits" style="height: 340px;">
   <div id="grafic_visits_chart" style="float: right"></div>
   <input type="radio" name="group" value="day" checked onclick="statisticHandler.reload('getVisits')">�� ����<Br>
   <input type="radio" name="group" value="week" onclick="statisticHandler.reload('getVisits')">�� �������<Br>
   <input type="radio" name="group" value="month" onclick="statisticHandler.reload('getVisits')">�� �������<Br>
   <input type="checkbox" name="visit" value="Y" checked onclick="statisticHandler.reloadLocal('getVisits')">������<Br>
   <input type="checkbox" name="view" value="Y" checked onclick="statisticHandler.reloadLocal('getVisits')">���������<Br>
</div>
<div class="js-grafic-sources" style="">
   <div id="grafic_sources_chart" style="float: right"></div>
   <div id="grafic_sources_list" style=""></div>
</div>
<div class="js-grafic-countries" style="">
   <div id="grafic_countries_chart" style="float: right"></div>
   <div id="grafic_countries_list" style=""></div>
</div>
<div class="js-grafic-search" style="">
   <div id="grafic_search_chart" style="float: right"></div>
   <div id="grafic_search_list" style=""></div>
</div>
<div class="js-grafic-phrases" style="">
   <div id="grafic_phrases_chart" style="float: right"></div>
   <div id="grafic_phrases_list" style=""></div>
   <button class="js-grafic-phrases-more" data-per-page="10" data-more="10" onclick="$(this).attr('data-per-page', ~~$(this).attr('data-per-page')+~~$(this).attr('data-more')); statisticHandler.reload('getSearchPhrases')">�������� ���</button>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>