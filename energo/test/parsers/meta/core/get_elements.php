<?
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();

if(!$USER->IsAuthorized())
{
   PrintObject("�������������!");
   die();
}

//�����
if(isset($_REQUEST["reset"]))
{
   PrintAdmin("������ �������!");
   $_SESSION["PARSE_META"] = array(
       "ITEMS" => array(),
       "ITERATIONS" => array()
   );
   die();
}
if(!isset($_REQUEST["parse"]))
{
   PrintAdmin("������:");
   PrintAdmin($_SESSION["PARSE_META"]);
   die();
}

$arParams["GETLIST_FILTER"]["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
$arParams["GETLIST_FILTER"]["!PROPERTY_META_AUTO"] = GetPropertyVariantId("Y", "META_AUTO", $arParams["IBLOCK_ID"]);
if(is_array($arParams["GETLIST_SELECT"]) && !in_array("ID", $arParams["GETLIST_SELECT"])) $arParams["GETLIST_SELECT"][] = "ID";

$res = $objElement->GetList(
      $arParams["GETLIST_SORT"],
      $arParams["GETLIST_FILTER"],
      false,
      array("nPageSize" => $arParams["MAXCOUNT"]),
      $arParams["GETLIST_SELECT"]
);
$counter = 0;
while($arItem = $res->GetNext())
{
   if($arParams["TEST_ID"])
   {
      $arItem = $objElement->GetByID($arParams["TEST_ID"])->GetNext();
   }
   
   $arItem = ParserHandler($arItem);
   $metaHandler = new $arParams["CLASS_NAME"]($arItem);
   //PrintAdmin($metaHandler->GetElement());
   
   $result = $metaHandler->Meta($arParams["SAVE"]);
   //PrintAdmin($metaHandler->GetSavedMeta());
   
   $counter++;
   if($arParams["SESSION"])
   {
      $_SESSION["PARSE_META"]["ITEMS"][] = $result;
   }
}

if($arParams["SESSION"])
{
   $_SESSION["PARSE_META"]["ITERATIONS"][] = "next...";
}


if($counter >= $arParams["MAXCOUNT"])
{
   if($arParams["RELOAD"])
   {
      LocalRedirect($APPLICATION->GetCurPageParam("parse=Y", array("parse")));
   }
   PrintAdmin($_SESSION["PARSE_META"]);
   PrintObject("temp end");
}
else
{
   PrintObject("END!!!");
}