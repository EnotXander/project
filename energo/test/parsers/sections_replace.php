<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

//die();

if(!CModule::IncludeModule("iblock")) die();
$objSection = new CIBlockSection();

$KEY = array(
    "IBLOCK_ID" => 17//3-news 2-articles 29-interviews 27-tovars 17-company 33-faces
);


$arSectionId = array();
$res = $objSection->GetList(
        array("ID" => "DESC"),
        array(
            "IBLOCK_ID" => $KEY["IBLOCK_ID"],
            "SECTION_ID" => 1767,
            //"!ID" => array(1767, 3079)
        ),
        false
   );
while($arItem = $res->GetNext())
{
   $arSectionId[] = (int)$arItem["ID"];
   $objSection->Update($arItem["ID"], array(
       "IBLOCK_SECTION_ID" => false
   ));
}
PrintAdmin($arSectionId);


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");