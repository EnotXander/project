<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 25,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
    "IBLOCK_ID" => 17
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 146936;//*******
      $res = $objElement->GetByID($currentId);
      if($obItem = $res->GetNextElement())
      {
         $arItem = $obItem->GetFields();
         $arItem["PROP"] = $obItem->GetProperties();
         //foreach($arItem["PROP"]["USER"]["VALUE"] as $userId)
         if($userId = $arItem["PROP"]["USER"]["VALUE"])
         {
            $props["COMPANY"] = $arItem["ID"];//���������������
            $props["USER"] = $userId;//���������������
            $props["STATUS"] = 65;//�������
            $props["STATUS"] = 65;//�������
            $props["PREDSTAVITEL"] = 71;//"Y"
            $arUser = $USER->GetByID($userId)->Fetch();
            $companyName = htmlspecialchars_decode ($arItem["NAME"]);
            $arToSave = array(
               //"MODIFIED_BY"    => $USER->GetID(), // ������� ������� ������� �������������
               "PROPERTY_VALUES"=> $props,
               "IBLOCK_ID"      => 49,
               "NAME"           => "{$arUser["NAME"]} {$arUser["LAST_NAME"]} - {$companyName} : parser",
               "ACTIVE"         => "Y",            // �������
            );
            if($KEY["SAVE"])
            {
               $newID = $objElement->Add($arToSave);
               if(intval($newID) > 0)
               {
                  $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                  $arChange[] = array(
                     "ID" => $arItem["ID"],
                     "NAME" => $arItem["NAME"],
                     "USER" => $userId,
                     "EMPLOYEE" => $newID,
                     "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
                  );
                  $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]}) --- $userId");
               }
               else
               {
                  $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
                  $arChange[] = array(
                     "ID" => $arItem["ID"],
                     "NAME" => $arItem["NAME"],
                     "USER" => $userId,
                     "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
                  );
                  $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
               }
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_company_predstavitel.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}