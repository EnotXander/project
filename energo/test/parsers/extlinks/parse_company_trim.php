<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "MAXCOUNT" => 1000,//1000 reliz
    "PRINT" => true,
    "SAVE" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => false,
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   //��������� ���� ��������
   $arCompany = array();
   $res = $objElement->GetList(
           array("ID" => "DESC"),
           array("IBLOCK_ID" => 17),
           false
      );
   while($arItem = $res->GetNext())
      $arCompany[$arItem["ID"]] = $arItem;
   
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 126210;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         $arParseFieldsChange = array();
         //����������
         $arItem["NAME"] = html_entity_decode($arItem["NAME"]);
         $nameFiltered = trim($arItem["NAME"]);
         $nameFiltered = preg_replace('/[\s	]+/', ' ', $nameFiltered);
         if($nameFiltered != $arItem["NAME"])
         {
            if(preg_match('/([a-z�-��-��-��-߸�0-9\\\'\"�])/i', substr($nameFiltered, 0, 1)))
            {
               $arParseFieldsChange["NAME"] = $nameFiltered;
               $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
               $arChange[] = array(
                   "ID" => $arItem["ID"],
                   "OLD" => $arItem["NAME"],
                   "NAME" => $nameFiltered,
                   "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
            }
            else
            {
               $arParseFieldsChange["NAME"] = $nameFiltered;
               $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
               $arChange[] = array(
                   "ID" => $arItem["ID"],
                   "OLD" => $arItem["NAME"],
                   "NAME" => $nameFiltered,
                   "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
            }
         }
         //����������
         if($KEY["SAVE"])
         {
            if(count($arParseFieldsChange))
            {
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]}) - {$nameFiltered}");
               }
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_company_trim.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}



require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");