<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "MAXCOUNT" => 1000,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true,
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   //��������� ���� ��������
   $arCompany = array();
   $res = $objElement->GetList(
           array("ID" => "DESC"),
           array("IBLOCK_ID" => 17),
           false
      );
   while($arItem = $res->GetNext())
      $arCompany[$arItem["ID"]] = $arItem;
   
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 117019;//*******
      $res = $objElement->GetByID($currentId);
      if($obItem = $res->GetNextElement())
      {
         $arItem = $obItem->GetFields();
         $arItem["PROP"] = $obItem->GetProperties();
         
         //�������� �� ����� � ���������
         /*if(!$arItem["PROP"]["FIRM"]["VALUE"])
         {
            $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
            $arChange[] = array(
               "ID" => $arItem["ID"],
               "NAME" => $arItem["NAME"],
               "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
            );
            $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
         }*/
         //�������� �� ����� � ��������
         /*if(!$arItem["IBLOCK_SECTION_ID"])
         {
            $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
            $arChange[] = array(
               "ID" => $arItem["ID"],
               "NAME" => $arItem["NAME"],
               "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
            );
            $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
         }*/
         
         //��������� ������
         $rsPath = GetIBlockSectionPath($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]);
         if($arPath = $rsPath->GetNext()) {
            //PrintObject($arPath);
            $company = $arCompany[$arItem["PROP"]["FIRM"]["VALUE"]];
            if(is_array($company))
            {
               $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
               if(!is_array($arChange[$company["ID"]]))
               {
                  $arChange[$company["ID"]] = array(
                     "ID" => $company["ID"],
                     "NAME" => $company["NAME"],
                     "LINK" => "http://".SITE_SERVER_NAME.$company["DETAIL_PAGE_URL"]
                  );
               }
               $arChange[$company["ID"]][$arPath["ID"]] = array(
                   /*"TOVAR_ID" => $arItem["ID"],
                   "TOVAR_NAME" => $arItem["NAME"],*/
                   "SECTION_ID" => $arPath["ID"],
                   "SECTION_NAME" => $arPath["NAME"]
               );
               $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
            }
            else
            {
               $arChange = $_SESSION['PARSE_PREVPIC_NONE'];
               $arChange[] = array(
                  "ID" => $arItem["ID"],
                  "NAME" => $arItem["NAME"],
                  "FIRM" => $arItem["PROP"]["FIRM"]["VALUE"],
                  "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
               );
               $_SESSION['PARSE_PREVPIC_NONE'] = $arChange;
            }
         }
         //PrintAdmin($arItem);
         //PrintAdmin($arItem["NAME"]);
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_company_topsections.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}