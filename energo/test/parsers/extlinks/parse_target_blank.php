<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

die();

global $USER;
if(!CModule::IncludeModule("iblock")) die();
$objElement = new CIBlockElement();
session_start();

$KEY = array(
    "PARSE_AND_CHANGE" => true,
    "SAVE" => true,
    "MAXCOUNT" => 1000,//1000 reliz
    "PRINT" => true,
    "SESSION_CLEAR" => true,
    "RELOAD" => true
);

if($KEY["PARSE_AND_CHANGE"])
{
   if($KEY["PRINT"])
   {
      PrintObject("PARSE_AND_CHANGE");
   }
   $arSessionId = array();
   $arSessionId = $_SESSION['PARSE_EXTLINKS_IDS'];
   while(count($arSessionId) && $KEY["MAXCOUNT"])
   {
      $KEY["MAXCOUNT"]--;
      reset($arSessionId);
      $currentId = current($arSessionId);
      //$currentId = 29961;//*******
      $res = $objElement->GetByID($currentId);
      if($arItem = $res->GetNext())
      {
         //�����
         $arParseFieldsChange = array();
         //��������� ������ �����, ������� ���������� ��������
         $arParseFields = array();
         $arParseFields[] = "PREVIEW_TEXT";
         $arParseFields[] = "DETAIL_TEXT";
         //��������� ���������
         $regTarget = '/((<a )(?![^>]*target[ ]?=[ ]?[\"\\\']?_blank[\"\\\']?)([^>]*>))/isU';
         //������� target = _blank
         //PrintObject($regTarget);
         foreach($arParseFields as $parseField)
         {
            $parseText = $arItem[$parseField];
            if(strlen($parseText))
            {
               $newText = preg_replace($regTarget, '$2target="_blank" $3', $parseText);
               //PrintObject("-------------------");
               //PrintObject($parseText);
               //PrintObject($newText);
               //PrintObject(htmlspecialchars($regTarget));
               if($newText !== NULL)
               {
                  if($parseText != $newText)
                  {
                     $arChange = $_SESSION['PARSE_PREVPIC_CHANGE'];
                     $arChange[] = array(
                         "ID" => $arItem["ID"],
                         "NAME" => $arItem["NAME"],
                         "FIELD" => $parseField,
                         "LINK" => "http://".SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]
                     );
                     $_SESSION['PARSE_PREVPIC_CHANGE'] = $arChange;
                     
                     $arParseFieldsChange[$parseField] = $newText;
                     $arParseFieldsChange[$parseField."_TYPE"] = "html";//$arItem[$parseField."_TYPE"];
                     if($KEY["PRINT"])
                     {
                        PrintObject(" ");
                        PrintObject(" ");
                        PrintObject("( ".$arItem["ID"]." )  ".$arItem["NAME"]."  ( ".$parseField." )");
                     }
                  }
               }
            }
         }

         //����������
         if($KEY["SAVE"])
         {
            if(count($arParseFieldsChange))
            {
               $isUpdated = $objElement->Update($arItem["ID"], $arParseFieldsChange);
               if(!$isUpdated)
               {
                  if($KEY["PRINT"])
                  {
                     PrintObject("-----ERROR ( {$arItem["ID"]} ):");
                     PrintObject ($objElement->LAST_ERROR);
                  }
               }
               else
               {
                  if($KEY["PRINT"])
                     PrintObject ("���������! {$arItem["ID"]} ({$arItem["NAME"]})");
               }
            }
         }
      }
      //�������� �� ������
      if($KEY["SESSION_CLEAR"])
      {
         $currentKey = array_search($currentId, $arSessionId);
         if($currentKey !== false)
         {
            unset($arSessionId[$currentKey]);
            $_SESSION['PARSE_EXTLINKS_IDS'] = $arSessionId;
         }
      }
   }
   //�������������
   if($KEY["RELOAD"])
   {
      if(count($arSessionId) && !$KEY["MAXCOUNT"])
      {
         LocalRedirect("parse_target_blank.php");
         PrintObject("temp end");
      }
      else
      {
         PrintObject("END!!!");
      }
   }
}