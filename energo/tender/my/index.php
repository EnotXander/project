<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("show_left_area", "Y");
$APPLICATION->SetPageProperty("show_bread", "Y");
$APPLICATION->SetTitle("��� �������");
?>
<?global $USER;
$arGroups = $USER->GetUserGroupArray();
if (in_array(10,$arGroups)):?> <?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add",
	"vakansii",
	Array(
		"NAV_ON_PAGE" => "10",
		"USE_CAPTCHA" => "Y",
		"USER_MESSAGE_ADD" => "���� ������ ���������",
		"USER_MESSAGE_EDIT" => "���� ������ ���������",
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "N",
		"IBLOCK_TYPE" => "information_part",
		"IBLOCK_ID" => "2",
		"PROPERTY_CODES" => array(0=>"NAME",1=>"TAGS",2=>"IBLOCK_SECTION",3=>"PREVIEW_TEXT",4=>"PREVIEW_PICTURE",5=>"DETAIL_TEXT",6=>"DETAIL_PICTURE",7=>"1",8=>"145",9=>"146",10=>"153",),
		"PROPERTY_CODES_REQUIRED" => array(0=>"NAME",1=>"TAGS",2=>"IBLOCK_SECTION",3=>"PREVIEW_PICTURE",4=>"DETAIL_TEXT",5=>"1",),
		"GROUPS" => array(0=>"10",),
		"STATUS" => array(0=>"1",),
		"STATUS_NEW" => "1",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "N",
		"ELEMENT_ASSOC" => "PROPERTY_ID",
		"ELEMENT_ASSOC_PROPERTY" => "2",
		"MAX_USER_ENTRIES" => "1000",
		"MAX_LEVELS" => "1",
		"LEVEL_LAST" => "Y",
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CUSTOM_TITLE_NAME" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "����",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "����� ������",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "����� ������",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "�������� ��� ������",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);?> <?else:?> <?$APPLICATION->IncludeComponent("bitrix:iblock.element.add", "vakansii", Array(
	"NAV_ON_PAGE" => "10",	// ���������� ��������� �� ��������
	"USE_CAPTCHA" => "Y",	// ������������ CAPTCHA
	"USER_MESSAGE_ADD" => "��� ������ ��������",	// ��������� �� �������� ����������
	"USER_MESSAGE_EDIT" => "��� ������ ��������",	// ��������� �� �������� ����������
	"DEFAULT_INPUT_SIZE" => "30",	// ������ ����� �����
	"RESIZE_IMAGES" => "N",	// ������������ ��������� ��������� ��� ��������� �����������
	"IBLOCK_TYPE" => "personal_cabinet",	// ��� ���������
	"IBLOCK_ID" => "41",	// ��������
	"PROPERTY_CODES" => array(	// ��������, ��������� �� ��������������
		0 => "NAME",
		1 => "PREVIEW_TEXT",
		2 => "DETAIL_TEXT",
		3 => "230",
	),
	"PROPERTY_CODES_REQUIRED" => array(	// ��������, ������������ ��� ����������
		0 => "NAME",
		1 => "PREVIEW_TEXT",
		2 => "DETAIL_TEXT",
	),
	"GROUPS" => array(	// ������ �������������, ������� ����� �� ����������/��������������
		0 => "5",
	),
	"STATUS" => "ANY",	// �������������� ��������
	"STATUS_NEW" => "N",	// �������������� ������� ����� ����������
	"ALLOW_EDIT" => "N",	// ��������� ��������������
	"ALLOW_DELETE" => "N",	// ��������� ��������
	"ELEMENT_ASSOC" => "CREATED_BY",	// �������� � ������������
	"MAX_USER_ENTRIES" => "1000",	// ���������� ���-�� ��������� ��� ������ ������������
	"MAX_LEVELS" => "1",	// ���������� ���-�� ������, � ������� ����� ��������� �������
	"LEVEL_LAST" => "Y",	// ��������� ���������� ������ �� ��������� ������� �����������
	"MAX_FILE_SIZE" => "0",	// ������������ ������ ����������� ������, ���� (0 - �� ������������)
	"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",	// ������������ ���������� ���������� �������� ��� �������������� ������ ������
	"DETAIL_TEXT_USE_HTML_EDITOR" => "Y",	// ������������ ���������� ���������� �������� ��� �������������� ���������� ������
	"SEF_MODE" => "Y",	// �������� ��������� ���
	"SEF_FOLDER" => "/tender/my/",	// ������� ��� (������������ ����� �����)
	"AJAX_MODE" => "N",	// �������� ����� AJAX
	"AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
	"AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
	"AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
	"CUSTOM_TITLE_NAME" => "",	// * ������������ *
	"CUSTOM_TITLE_TAGS" => "",	// * ���� *
	"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",	// * ���� ������ *
	"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",	// * ���� ���������� *
	"CUSTOM_TITLE_IBLOCK_SECTION" => "",	// * ������ ��������� *
	"CUSTOM_TITLE_PREVIEW_TEXT" => "",	// * ����� ������ *
	"CUSTOM_TITLE_PREVIEW_PICTURE" => "",	// * �������� ������ *
	"CUSTOM_TITLE_DETAIL_TEXT" => "",	// * ��������� ����� *
	"CUSTOM_TITLE_DETAIL_PICTURE" => "",	// * ��������� �������� *
	"AJAX_OPTION_ADDITIONAL" => "",	// �������������� �������������
	),
	false
);?> <?endif;?> 

 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>