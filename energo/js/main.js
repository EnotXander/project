function checkStringJS(str) {
   var regex=/^[a-zA-Z�-��-�0-9]*$/;
   if(str.search(regex)==-1)
      return false;
   else
      return true;
}

function checkEmailJS(str) {
   var regex=/^[-a-z0-9!#$%&\'*+\/=?^_`{|}~]+\.?([-a-z0-9!#$%&'*+\/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)+[a-z]{1,6}$/i;
   if(str.search(regex)==-1)
      return false;
   else
      return true;
}

//���������
function validation(args){
   return function(){
      args.element = $(args.selector);
      var status = true;
      for(var index in args.require){
         if(!args.require[index].compare(args.element)){
            args.showError(args.element, args.require[index].errortext);
            status = false;
            break;
         }
      }
      if(status)
         args.hideError(args.element);
      return status;
   }
}

/*
 *������
 var isValid = validation({
   element: $(this),
   require: [
      {
         compare: function($element){
            return $element.val().length
         },
         errortext: "Name required"
      },{
         compare: function($element){
            return (($element.val().length >= 3) && ($element.val().length <= 50))
         },
         errortext: "From 3 to 50 letters"
      },{
         compare: function($element){
            return checkStringJS($element.val())
         },
         errortext: "Only A-Z, �-�, 0-9"
      }
   ],
   showError: function($element, text){
      var $tr = $element.closest('tr');
      $tr.addClass("error");
      $tr.find('.errortext').text(text);
   },
   hideError: function($element){
      var $tr = $element.closest('tr');
      if($tr.hasClass("error")){
         $tr.removeClass("error");
         $tr.find('.errortext').text("");
      }
   }
})
 */
