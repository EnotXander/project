<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
die();
if(!CModule::IncludeModule("iblock")) return;
$objElement = new CIBlockElement(); // ������ ��� ������ � ����������

// ��������� ��������������
$arReplace = array(
    "max_len" => 100,
    "change_case" => 'L',
    "replace_space" => '_',
    "replace_other" => '_',
    "delete_repeat_replace" => true,
    "use_google" => false,
);

$i = 0;
$bad = array();
$good = array();
$res = $objElement->GetList(array(), array("IBLOCK_ID" => 27, "CODE" => false));
while($element = $res->GetNext())
{
   $save = array();
   $save["CODE"] = CUtil::translit($element["NAME"], LANGUAGE_ID, $arReplace);

   if($objElement->Update($element["ID"], $save))
   {
      $good[] = $element["ID"];
   } else
   {
      $save["CODE"] = $save["CODE"]."_".$element["ID"];
      if($objElement->Update($element["ID"], $save))
      {
         $good[] = $element["ID"];
      } else 
      {
         $bad[] = $element["ID"];
      }
   }
   $i++;
}
PrintObject($i);
PrintObject($bad);
PrintObject($good);