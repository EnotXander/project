<?php
/**
 * User: ZvvAsyA
 * Date: 28.01.16 -  17:36
 */

// �������� � ����� ������� �������� ������
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));

$_SERVER["DOCUMENT_ROOT"] = dirname((dirname(dirname(dirname(__FILE__)))));
    echo $_SERVER["DOCUMENT_ROOT"] , "\n";

$_SERVER['SERVER_PORT'] = 80;
$_SERVER['HTTP_HOST'] = 'agrobelarur.by';
$_SERVER['REQUEST_URI'] = '/';
$_SERVER['HTTP_REFERER'] = '';
$_SERVER['REMOTE_ADDR'] = '';
$_SERVER['REQUEST_METHOD'] = 'POST';


require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );

CModule::IncludeModule( "iblock" );
/** Include PHPExcel */
require_once $_SERVER["DOCUMENT_ROOT"].'/_lib/Classes/PHPExcel.php';

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('UTC');

define('COMPANY_ID', 254006); // ����������-���� ���
define('PRODUCT_IBLOCK_ID' , 58); // ����������-���� ���

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br/>');

$temp_files = glob(__dir__.'/data/*');
foreach($temp_files as $file) {

	echo $file."\n";
	$items = grocessFile($file);

	$res = toBase($items);
	//print_r($res);

//	die();
}

function grocessFile($file){
	if (!file_exists($file)) {
		exit("Please run $file first.\n");
	}


	$params = Array(
	       "max_len" => "100", // �������� ���������� ��� �� 100 ��������
	       "change_case" => "L", // ����� ������������� � ������� ��������
	       "replace_space" => "_", // ������ ������� �� ������ �������������
	       "replace_other" => "_", // ������ ����� ������� �� ������ �������������
	       "delete_repeat_replace" => "true", // ������� ������������� ������ �������������
	       "use_google" => "false", // ��������� ������������� google
	    );

	echo date('H:i:s') , " Load workbook from Excel5 file" , EOL;
	$callStartTime = microtime(true);

	$objPHPExcel = PHPExcel_IOFactory::load($file);
	$Sheet = $objPHPExcel->setActiveSheetIndex(0);

	$rows = $Sheet->getHighestRow();
	$products = [];
	for ($i=0; $i<$rows; $i+=1) {
		$name   =  trim($Sheet->getCell('A'.$i)->getValue());
		$empty =   trim($Sheet->getCell('B'.$i)->getValue());
		$edinica = trim($Sheet->getCell('C'.$i)->getValue());
		$price =   trim($Sheet->getCell('D'.$i)->getValue());

		$code = explode(':', $name);

		if (isset($code[1]))
			$code = explode(' ', $code[1]);

		if (isset($code[1]))
			$code = trim($code[1]);


		$products []= [
			'name' => iconv( "UTF-8", "cp1251", $name),
			'empty' => $empty,
			'edinica' => iconv( "UTF-8", "cp1251", $edinica),
			'price' => $price,
			'xml_id' => CUtil::translit(iconv( "UTF-8", "cp1251", $code), "ru" , $params)
		];
	}

	$callEndTime = microtime(true);
	$callTime = $callEndTime - $callStartTime;

	echo 'Call time to load Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
	// Echo memory usage
	echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;

	return $products;
}

function toBase($products){
	$res = [];

	foreach ($products as $product) {
		// 5338 ������
		if ($product['price'] > 0 ) {
			$res []= processProduct($product, PRODUCT_IBLOCK_ID, 5338 , false, false); //
			//die();
		}
	}

	return $res;
}






function processProduct($item, $iblock, $section){

	if ($item['xml_id'] == '')
		$item['xml_id'] = '=====---'; // ����� ������

	// ����� �� ����. ���� ���� �� ��������
	$res = CIBlockElement::GetList([], ['IBLOCK_ID'=>$iblock, '=PROPERTY_FIRM'=>COMPANY_ID, '=XML_ID' => COMPANY_ID.'-'.$item['xml_id']]);


	if ($em = $res->GetNext()) {
		echo iconv("cp1251", "UTF-8", $em['NAME']).' '. $em['ID']. ' =u= '. $item['NAME']. $item['xml_id']. "\n";

		return updateProduct($em['ID'], $item);
	} else {

		// ����� �� �����
		$res = CIBlockElement::GetList([], ['IBLOCK_ID'=>$iblock, '=PROPERTY_FIRM'=>COMPANY_ID, 'NAME' => '%'.$item['xml_id'].'%']);
		if ($em = $res->GetNext()) {
			  updateProduct($em['ID'], $item);
			 return iconv("cp1251", "UTF-8", $em['NAME']). ' =:= '. $item['name']. ' > '. $item['xml_id']. "\n";

		} else {
			addProduct ($item, $iblock, $section);
			return  ' no =:= '. $item['name']. ' > '. $item['xml_id']. "\n";
		}

		//return addProduct ($item, $iblock, $section,  $parent_id);
	}

};

function addProduct ($item, $iblock, $section){
    //$item->NAME;
	//$item->CODE;
	//$item->PRICE;

	$el = new CIBlockElement;

	$params = Array(
	       "max_len" => "100", // �������� ���������� ��� �� 100 ��������
	       "change_case" => "L", // ����� ������������� � ������� ��������
	       "replace_space" => "_", // ������ ������� �� ������ �������������
	       "replace_other" => "_", // ������ ����� ������� �� ������ �������������
	       "delete_repeat_replace" => "true", // ������� ������������� ������ �������������
	       "use_google" => "false", // ��������� ������������� google
	    );

	echo "\n";

	$item['name'] = trim( $item['name']);
	$item['xml_id'] = trim( $item['xml_id']);


	$arLoadProductArray = Array(
	  //"MODIFIED_BY"    => , // ������� ������� ������� �������������
	  "IBLOCK_SECTION_ID"   => $section,          // ������� ����� � ����� �������
	  "IBLOCK_ID"           => $iblock,
	  "NAME"                => $item['name'],
	  "CODE"                => CUtil::translit($item['name'], "ru" , $params),
	  "XML_ID"              => COMPANY_ID.'-'.$item['xml_id'],
	  "ACTIVE"              => "Y",            // �������
	  "PREVIEW_TEXT"        => "",
	  "DETAIL_TEXT"         => "",
	  "DETAIL_TEXT_TYPE"    => 'html',

	  );



	if($PRODUCT_ID = $el->Add($arLoadProductArray)){
		echo "New ID: ".$PRODUCT_ID;


		if (CIBlockElement::SetPropertyValueCode(
						$PRODUCT_ID,
				        "COST",
						$item['price']
			        ));


		if (CIBlockElement::SetPropertyValueCode(
						$PRODUCT_ID,
				        "S_COST",
						$item['price']
			        ));


		if (CIBlockElement::SetPropertyValueCode(
						$PRODUCT_ID,
				        "CURRENCY",
						121
			        ));

		if (CIBlockElement::SetPropertyValueCode(
						$PRODUCT_ID,
				        "COST_NDS",
						2132
			        ));

		if (CIBlockElement::SetPropertyValueCode(
					$PRODUCT_ID,
			        "FIRM",
					COMPANY_ID
		        ));

		$ed = '��.' == $item['edinica'] ? 2101 : '';
		if ($ed == '')
			$ed = '�-�' == $item['edinica'] ? 2200 : '';

		if (CIBlockElement::SetPropertyValueCode(
					$PRODUCT_ID,
			        "unit_tov",
					$ed
		        ));

		if (CIBlockElement::SetPropertyValueCode(
					$PRODUCT_ID,
			        "AUTO_IMPORT",
					"Y"
		        ));

		if (CIBlockElement::SetPropertyValueCode(
					$PRODUCT_ID,
			        "PARENT",
					"N"
		        ));



		echo  "\n";

		return $PRODUCT_ID;

	}

	else
	  echo "Error: ".iconv("cp1251", "UTF-8", $el->LAST_ERROR),' : ' ,CUtil::translit($item['name'], "ru" , $params), "\n";

	return false;
}


function updateProduct ($PRODUCT_ID, $item){


	$arLoadProductArray["XML_ID"] = COMPANY_ID.'-'.$item['xml_id'];

	//$arLoadProductArray["IBLOCK_SECTION"] = 4408;



	$el = new CIBlockElement;
	$el->Update( $PRODUCT_ID, $arLoadProductArray );

	if (CIBlockElement::SetPropertyValueCode(
						$PRODUCT_ID,
				        "COST",
						$item['price']
			        ));

	if (CIBlockElement::SetPropertyValueCode(
						$PRODUCT_ID,
				        "S_COST",
						$item['price']
			        ));

	if (CIBlockElement::SetPropertyValueCode(
						$PRODUCT_ID,
				        "AUTO_IMPORT",
						"Y"
			        ));

	if (CIBlockElement::SetPropertyValueCode(
					$PRODUCT_ID,
			        "CURRENCY",
					121
		        ));



	$ed = '�-�' == $item['edinica'] ? 2200 : '';
	if ($ed == '')
		$ed =  2101; // �-�


	if (CIBlockElement::SetPropertyValueCode(
				$PRODUCT_ID,
		        "unit_tov",
				$ed
	        ));

	if (CIBlockElement::SetPropertyValueCode(
					$PRODUCT_ID,
			        "COST_NDS",
		2132
		        ));

	if (CIBlockElement::SetPropertyValueCode(
				$PRODUCT_ID,
		        "FIRM",
				COMPANY_ID
	        ));

	return true;
}
