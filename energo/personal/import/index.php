<?php
/**
 * User: ZvvAsyA
 * Date: 27.01.16 -  16:43
 */

require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );

global $map;
$map  = json_decode(file_get_contents('data/crazy.map.json'));

$data =	json_decode(file_get_contents('data/data.json'));


?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>����������</title>

    <!-- Bootstrap -->
	  <!-- Latest compiled and minified CSS -->
	      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	      <!-- Optional theme -->
	      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		 <link rel="stylesheet" href="style.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
        <h1>����������</h1>
        <div>
	        <div class="btn-group" role="group" >
	          <button type="button" class="btn btn-default">��������� �����</button>
	          <button type="button" class="btn btn-default saveMap" >��������� �����</button>
			</div>
        </div>

		<table style="width: 100%; height: 750px" border="1">
			<tr>
				<td style="vertical-align: top">

					<h2>������� �����</h2>
					<div class="tree_root">
						<?
							$res = CIBlockSection::GetTreeList(
								[
									'IBLOCK_ID' => IBLOCK_PRODUCTS,
									'ACTIVE'    => 'Y'
								]
							);

							while ( $item = $res->Fetch() ) {

								$level = $item['DEPTH_LEVEL'];
								?><div  class="use_section pointer level<?= $level ?>" data-id="<?= $item['ID'] ?>" ><?= $item['NAME'] ?></div><?
							}
						?>
					</div>
				</td>
				<td style="vertical-align: top">
					<h2>������� ��������</h2>
					<div class="tree_root" >
					<?

						$level = 0;
						foreach ($data as $item){
							showLine($item, $level);
						}
					?></div>
				</td>
			</tr>
		</table>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="script.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </body>
</html>

<?

function showLine($item, $level){
   $item->NAME = iconv("UTF-8", "cp1251", $item->NAME);
	global $map;

	?>
<div class=" level<?= $level ?>">
	<? if (empty($item->CHILD)) {
		showProduct($item, $level+1);
	} else {

		if (isset($map->{$item->CODE})){
			$value = $map->{$item->CODE}->item;
			$checked  = $map->{$item->CODE}->group == "true";

		}else{
			$value = '';
			$checked  = $item->PRODUCT ;
		}

		?>
		<div class="tree_line">
			<span class=" action_toggle glyphicon glyphicon-arrow-up"></span>

			<?= $item->NAME. ' ('.$item->CODE.')'. ($item->PRODUCT?' <b>��������� �����</b>':'') ?>

			<input type="checkbox" data-type="group" data-id="<?= $item->CODE ?>" class=" saveInput " <?= ( $checked ? 'checked="checked"' : '') ; ?> />
			<input type="text"     value="<?= $value ?>" data-type="item" data-id="<?= $item->CODE ?>" class=" saveInput input_text " />


		</div>

			<div>
				<?
					if (!empty($item->CHILD) ){
						foreach ($item->CHILD as $it)
							showLine($it, $level+1);
					}
					?>
			</div>

		<?
	} ?>

</div>
<?
}
function showProduct ($item, $level){

	 ?><div class=" level<?= $level ?>">
	 	<?= $item->NAME. ' ('.$item->CODE.')' . ' <b>'.$item->PRICE.'</b>' ?>
	 	</div>
	<?
}