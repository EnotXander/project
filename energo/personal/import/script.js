/**
 * Created by zvvasya on 28.01.16.
 */

var cur_input = false;


$('.action_toggle').on('click', function(){

    $(this).toggleClass('glyphicon-arrow-down');
    $(this).toggleClass('glyphicon-arrow-up');

    $(this).parent().next().toggle();

});

$('.use_section').on('click', function(){
    var code = $(this).data('id');
    $(cur_input).val(code);
});

$('.input_text').on('click', function(){
    cur_input = this;
});

$('.saveMap').on('click', function(){

    var map = {};

    $('.saveInput').each(function (){

        if(!($(this).data('id') in map)) {
            map[$(this).data('id')]= {};
        }
        if(!($(this).data('type') in map[$(this).data('id')])) {

            var val = false;
            if ($(this).data('type') == 'group')
                    val = $(this).is(":checked");
            else
                    val = $(this).val();


            map[$(this).data('id')][ $(this).data('type')] =  val;
        }

    });

    $.post('ajax.php?action=savemap', {data: map}, function(data){

            alert(data);

    });
});

