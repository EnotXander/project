<?php
/**
 * User: ZvvAsyA
 * Date: 05.08.15 -  16:41
 */
$_SERVER["DOCUMENT_ROOT"] = '/home/project';
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( 'highloadblock' ); //модуль highload инфоблоков
CModule::IncludeModule( 'iblock' ); //модуль highload инфоблоков
global $USER;
ob_start();

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
//добавить событие

$connection = Bitrix\Main\Application::getConnection();

	$result = $connection->query(
		"SELECT
				`events`.`ID` AS `ID`, DATE_FORMAT(`events`.`UF_TIME`, '%Y-%m-%d') as `DATE`,
				`events`.`UF_CLIENT` AS `UF_CLIENT`,
				`events`.`UF_URL` AS `UF_URL`,
				`events`.`UF_DATA` AS `UF_DATA`,
				`events`.`UF_TYPE` AS `UF_TYPE`,
				`events`.`UF_TIME` AS `UF_TIME`, COUNT(*) as CNT
		FROM
		`events` `events`
		WHERE
		`events`.`UF_TYPE` = 'lost_img' and `events`.`UF_TIME` > NOW() - INTERVAL 1 DAY
		GROUP BY
		`events`.`UF_URL`, DATE_FORMAT(`events`.`UF_TIME`, '%Y%m%d')
		ORDER BY
		`events`.`UF_TIME` DESC
		LIMIT 0, 200 "
);

$i = 0;
?>
<style type="text/css">
	table, tr, td , th{
		border-collapse: separate;
		border: 1px solid black;
		padding: 2px;
	}
	.table{
		border: 1px;
	}
</style>
	<table border="1" class="table">
 <tr>
     <th>ид</th>
	 <th>дата</th>
	 <th>событие / повторения</th>
	 <th>Адресс url / данные</th>
 </tr>
<?
while($res = $result->fetch())
   {
      ?>
        <tr>
	        <td rowspan="2" ><?= $i++ ?></td>
	        <td rowspan="2" style="width: 70px"><?= $res['DATE'] ?></td>
	        <td rowspan="2"><?= $res['UF_TYPE'] ?> / <?= $res['CNT'] ?></td>
	        <td >
		        <div style="overflow: auto;     width: 800px;">
			        <a target="_blank" href="<?= $res['UF_URL'] ?>"><?= $res['UF_URL'] ?></a>
		        </div>
	        </td>
        </tr>
        <tr>
	        <td ><div style="overflow: auto;     width: 800px; max-height: 300px">
                    <pre><?= json_encode(json_decode($res['UF_DATA']), JSON_PRETTY_PRINT) ?></pre>
                </div>
            </td>
        </tr>
     <?
   }
?>
</table>

<?
$message = ob_get_flush();
ob_end_clean();

$to = 'zvvasya@gmail.com';

$subject = 'Уведомления по картинкам за '.date('Y-m-d');

$headers = "From: \r\n";
$headers .= "Reply-To: Энергобеларусь<info@energobelarus.by>\r\n";
//$headers .= "CC: susan@example.com\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=utf-8\r\n";

//$message = mb_convert_encoding($message, "utf-8", "windows-1251");

if ( mail($to, $subject, $message, $headers) ) {
	echo 'ok', "\n";
} else {
	echo 'error', "\n";
};