<?php
/**
 * User: ZvvAsyA
 * Date: 20.08.15 -  11:59
 */


require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );



if ( isset($_REQUEST['id']) && intval($_REQUEST['id']) >0 ) {
	CIBlockElement::SetPropertyValuesEx($_REQUEST['id'], 82, array('CLOSED' => $_REQUEST['state']=='on'?2084:false));
	//CIBlockElement::SetPropertyValues($_REQUEST['id'], 82, $_REQUEST['state']=='on'?2084:false , 'CLOSED');
	echo 'ok';
} else {
	echo 'error';
}