<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<style>
	.user-contacts .nd_skype_n {background: url('/bitrix/templates/energorb/img/skype_n.png') no-repeat;}
</style>
<?
global $USER;
global $APPLICATION;
$APPLICATION->SetTitle($_REQUEST["PROPERTY"]["NAME"][0]." �� ������� EnergoBelarus.by");
$arUser = CUser::GetByID($USER->GetID())->Fetch();
$arNewResImg = array();
foreach($_REQUEST["MORE_PHOTO"] as $key => &$fileSrc){
	$ext = end(explode(".", $fileSrc));
	$fileSrcWithoutExt = substr($fileSrc, 0, strpos($fileSrc, '.'));
	$newFileSrc = $_SERVER["DOCUMENT_ROOT"].$fileSrcWithoutExt.'_res.'.$ext;
	$newFileSrc88 = $_SERVER["DOCUMENT_ROOT"].$fileSrcWithoutExt.'_res88.'.$ext;
	CFile::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$fileSrc, $newFileSrc, array('width' => 200, 'height' => 200), BX_RESIZE_IMAGE_EXACT);
	CFile::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$fileSrc, $newFileSrc88, array('width' => 88, 'height' => 64), BX_RESIZE_IMAGE_EXACT);
	$arNewResImg["200x200"][$key] = $fileSrcWithoutExt.'_res.'.$ext;
	$arNewResImg["88x64"][$key] = $fileSrcWithoutExt.'_res88.'.$ext;
}

?>
<style>
    .carousel li img{
        width: 88px;
        height: 64px;
    }
    .big-imgs img{
        width: 200px;
        height: 200px;
    }
    .adv-inner h1, .user-contacts .nd_address, .user-contacts .nd_skype, .nd_detailed_description{
	    word-wrap: break-word;
    }
    .content_right_col{
	    float: left;
	    width: 300px;
    }
</style>
<?
$arCurrency = CIBlockPropertyEnum::GetByID($_REQUEST["PROPERTY"]["337"]);
$currency = $arCurrency["VALUE"];
//echo "<pre>"; print_r($currency); echo "</pre>";
$arUnit = CIBlockPropertyEnum::GetByID($_REQUEST["PROPERTY"]["373"]);
$unit = $arUnit["VALUE"];
switch ($_REQUEST['PROPERTY']['378'])
{
	case 98:
		$sectionClass = 'sell';
		break;
	case 97:
		$sectionClass = 'buy';
		break;
	case 99:
		$sectionClass = 'services';
		break;
	default:
		$sectionClass = '';
		break;
}
?>

<div class="middle_col adv-inner with-bar detail_adv">
	<h1><?= $_REQUEST["PROPERTY"]["NAME"][0]; ?></h1>
	<div class="meta author-wrap">
		<span class="adv-type <?= $sectionClass; ?>"></span>
			<a href="#"><?= $arUser["LAST_NAME"] . " " . $arUser['NAME']; ?></a>
			\ ���������� ���������  <?= date('������� � G:i') ?>. ����������: 1
		<div style="display: none;">
			<span class="user-name"><?= $arUser['LOGIN']; ?></span>
			<span class="user-org"><?= $arUser['COMPANY_NAME']; ?></span>
			<table class="user-info">
				<tbody><tr>
					<td>
						<img src="<?= CFile::GetPath($arUser['PERSONAL_PHOTO']); ?>" alt="">
					</td>
					<td>
						<a href="#" onclick="alert('�� ���������� � ������ �������������'); return false;">��������� ���������</a>
					</td>
				</tr>
				</tbody>
			</table>
			<a href="javascript:void(0)" class="all-adv">��� ����������</a>
			<span class="status">������</span>
		</div>
	</div>

	<table class="img-price">
		<tbody>
		<tr>
			<td>
				<div class="big-imgs">
					<? foreach($arNewResImg["200x200"] as $i => $fileName): ?>
						<div id="img<?= $i+1; ?>" class="img-wrap">
							<img src="<?= $fileName; ?>" alt="">
							<div>
								<a href="javascript:void(0)" class="fancybox" rel="group">���������</a>
							</div>
						</div>
					<? endforeach; ?>
				</div>
			</td>
			<td>
					<span class="price">
						<? if (!empty($_REQUEST["PROPERTY"][198][0])): ?>
							<span>����:</span>
							<?= $_REQUEST["PROPERTY"][198][0]; ?>
							<span class="val">
								<?= $currency." / ".$unit; ?>
							</span>
						<? else: ?>
							<span>���� ���������</span>
						<? endif; ?>
					</span>
			</td>
		</tr>
		</tbody>
	</table>
	<div class="carousel">
		<ul class="slides">
			<? foreach($arNewResImg["88x64"] as $i => $fileName): ?>
				<li><a href="#" data-num="<?= $i+1; ?>"><img src="<?= $fileName; ?>" alt=""></a></li>
			<? endforeach; ?>
		</ul>
	</div>

	<div class="nd_detailed_description">
		<h3>��������� ��������</h3>
		<?= html_entity_decode($_REQUEST["PROPERTY"]["DETAIL_TEXT"][0]) ; ?>
	</div>
	<?
	$filesCount = count($_REQUEST["MORE_FILE"]) - 1;
	?>
	<? if ($filesCount >= 0): ?>
		<div class="nd_attached_files">
			<h3>��������� �����</h3>
			<ul class="nd_float-left">
				<? for($i = 0; $i<=(int)($filesCount/2); $i++): ?>
					<?
					$extension = end(explode(".", $_REQUEST["MORE_FILE"][$i]));
					switch ($extension)
					{
						case "xls":
							$css = "nd_xls";
							break;
						case "xlsx":
							$css = "nd_xlsx";
							break;
						case "doc":
							$css = "nd_doc";
							break;
						case "docx":
							$css = "nd_docx";
							break;
						case "pdf":
							$css = "nd_pdf";
							break;
						case "djvu":
							$css = "nd_djv";
							break;

						case "epub":
							$css = "nd_epub";
							break;
						case "gif":
							$css = "nd_gif";
							break;
						case "jpeg":
							$css = "nd_jpeg";
							break;
						case "jpg":
							$css = "nd_jpg";
							break;
						case "png":
							$css = "nd_png";
							break;
						case "ppt":
							$css = "nd_ppt";
							break;
						case "pptx":
							$css = "nd_ppt";
							break;
						case "rtf":
							$css = "nd_rtf";
							break;
						default:
							$css = "nd_file";
					}
					?>
					<li>
						<a class="<?= $css; ?>" href="<?= $_REQUEST["MORE_FILE"][$i]; ?>" target="_blank">
							<?= basename($_REQUEST["MORE_FILE"][$i]); ?>
						</a>
					</li>
				<? endfor; ?>
			</ul><!--nd_float-left-->

			<ul class="nd_float-right">
				<? for($j = $i; $j<=$filesCount; $j++): ?>
					<?
					$extension = end(explode(".", $_REQUEST["MORE_FILE"][$j]));
					switch ($extension)
					{
						case "xls":
							$css = "nd_xls";
							break;
						case "xlsx":
							$css = "nd_xlsx";
							break;
						case "doc":
							$css = "nd_doc";
							break;
						case "docx":
							$css = "nd_docx";
							break;
						case "pdf":
							$css = "nd_pdf";
							break;
						case "djvu":
							$css = "nd_djv";
							break;

						case "epub":
							$css = "nd_epub";
							break;
						case "gif":
							$css = "nd_gif";
							break;
						case "jpeg":
							$css = "nd_jpeg";
							break;
						case "jpg":
							$css = "nd_jpg";
							break;
						case "png":
							$css = "nd_png";
							break;
						case "ppt":
							$css = "nd_ppt";
							break;
						case "pptx":
							$css = "nd_ppt";
							break;
						case "rtf":
							$css = "nd_rtf";
							break;
						default:
							$css = "nd_file";
					}
					?>
					<li>
						<a class="<?= $css; ?>" href="<?= $_REQUEST["MORE_FILE"][$j]; ?>" target="_blank">
							<?= basename($_REQUEST["MORE_FILE"][$j]); ?>
						</a>
					</li>
				<? endfor; ?>
			</ul><!--nd_float-right-->
		</div>
	<? endif; ?>

</div>
<div class="content_right_col">
	<div class="user-contacts">
		<span class="title">���������� ����������</span>
		<ul>
			<? if(count($_REQUEST["PROPERTY"][249]) > 0): ?>
				<? foreach ($_REQUEST["PROPERTY"][249] as $phone): ?>
					<? if ($phone == '') continue; ?>
					<li class="nd_phone">
						+375 <strong><?= $phone; ?></strong>
					</li>
				<? endforeach; ?>
			<? endif; ?>
			<? if(!empty($_REQUEST["PROPERTY"][375][0])): ?>
				<li class="nd_skype_n"><?= $_REQUEST["PROPERTY"][375][0]; ?></li>
			<? endif; ?>
			<? if(!empty($_REQUEST["PROPERTY"][374][0])): ?>
				<li class="nd_address"><?= $_REQUEST["PROPERTY"][374][0]; ?></li>
			<? endif; ?>
			<li><a href="javascript:void(0)">��� ���������� ������ </a></li>
		</ul>
		<a class="nd_contact_user" href="#" onclick="alert('�� ���������� � ������ �������������'); return false;">��������� � �������������</a>
		<div class="nd_forget">�� �������� ��������  ����������, ��� ����� ��� �� <strong>EnergoBelarus.by</strong></div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.author-wrap > a').click(function(){
			if ($(this).hasClass('act')) {
				$(this).parent().find('div').fadeOut(200);
				$(this).removeClass('act');
			}
			else {
				$('.author-wrap a.act').parent().find('div').fadeOut(200);
				$('.author-wrap a.act').removeClass('act');
				$(this).parent().find('div').fadeIn(200);
				$(this).addClass('act');
			}
			return false;
		});
		$('.to-mark span').click(function(){
			$(this).toggleClass('act');
			return false;
		});
		$(document).click(function(event){
			if($(event.target).closest('.author-wrap > div').length)
				return;
			$('.author-wrap > div').fadeOut(200);
			$('.author-wrap a').removeClass('act');
			event.stopPropagation();
		});
		$('.carousel').flexslider({
			animation: "slide",
			slideshow: false,
			animationLoop: false,
			controlNav: false,
			itemWidth: 102,
			itemMargin: 0,
			maxItems: 6,
			minItems: 6,
			move: 6
		});
		$('.carousel .slides a').click(function() {
			$('.carousel .slides a').removeClass('act');
			$(this).addClass('act');
			$('.img-wrap').hide();
			$('#img'+$(this).data('num')).show();
			return false;
		});
	});

</script>