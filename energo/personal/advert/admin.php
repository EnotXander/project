<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//���� ��������, � ������� �������������� ������� �
global $USER;
if($USER->IsAuthorized())
{
    $rsProducts = CIBlockElement::GetList(
        array(),
        array(
            "IBLOCK_ID" => IBLOCK_ADVARE,
            "PROPERTY_AUTHOR" => $USER->GetID()
        )
    );
    while($product = $rsProducts->GetNext())
        $products[] = $product["ID"];
} else
    LocalRedirect("/personal/advert/");

// ���������� ������� � ���������� � ������������
$APPLICATION->IncludeComponent("bitrix:main.include", "title", array(
    "AREA_FILE_SHOW" => "sect",
    "AREA_FILE_SUFFIX" => "title",
    "AREA_FILE_RECURSIVE" => "Y",
    "EDIT_MODE" => "html",
), false, Array('HIDE_ICONS' => 'Y')
);
?>

<? if(isset($_REQUEST["preview"])): ?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        ".default",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/personal/advert/preview_advert.php",
            "EDIT_TEMPLATE" => "standard.php"
        ),
        false
    );?>
<? else: ?>

    <div class="middle_col with-bar add-adv">
        <h1><?$APPLICATION->ShowTitle();?></h1>

        <?$APPLICATION->IncludeComponent("whipstudio:product.add.form","advert", array(
                //"COMPANY" => $arCompany,
                "PRODUCTS_IDS" => $products,
                "SEF_MODE" => "N",
                "IBLOCK_TYPE" => "services",
                "IBLOCK_ID" => IBLOCK_ADVARE,
                "PROPERTY_CODES" => array(
                    //1 �������
                    "���������� �� ����������" => array(
                        array(
                            "IBLOCK_SECTION",
                            GetPropertyId("TYPE", IBLOCK_ADVARE),
                            GetPropertyId("pr2", IBLOCK_ADVARE),//REGION
                            GetPropertyId("CITY", IBLOCK_ADVARE),
                            GetPropertyId("ADDRESS", IBLOCK_ADVARE),
                            "NAME",
	                        GetPropertyId("COST", IBLOCK_ADVARE),//����
                            GetPropertyId("CURRENCY", IBLOCK_ADVARE),//CURRENCY
                            GetPropertyId("UNITS", IBLOCK_ADVARE),
                            //GetPropertyId("TORG", IBLOCK_ADVARE),//TORG
	                        GetPropertyId("more_photo", IBLOCK_ADVARE),//more_photo
                            "PREVIEW_PICTURE",
	                        GetPropertyId("LOAD", IBLOCK_ADVARE), // LOAD GetPropertyId("LOAD", IBLOCK_ADVARE)
                            "DETAIL_TEXT",
                            "PREVIEW_TEXT",
                            GetPropertyId("CONTACT", IBLOCK_ADVARE),
                            GetPropertyId("pr1", IBLOCK_ADVARE),
                            GetPropertyId("SKYPE", IBLOCK_ADVARE),
                        )
                    )
                ),
                "PROPERTY_CODES_REQUIRED" => array(
                    "IBLOCK_SECTION",
                    "NAME",
                    "DETAIL_TEXT",
                    GetPropertyId("pr1", IBLOCK_ADVARE),
                ),
                "GROUPS" => array(5), // ������ ������� ����� ���������/�������������
                "STATUS_NEW" => 1,
                "STATUS" => array(1, 2, 3),
                "LIST_URL" => "/personal/advert/",
                "ELEMENT_ASSOC" => "", // �� ������ �������� ������ ����� � ��������������
                "ELEMENT_ASSOC_PROPERTY" => "",
                "MAX_USER_ENTRIES" => 1000000,
                "MAX_LEVELS" => 1, // ���������� ������ � ���������
                "LEVEL_LAST" => "N",
                "USE_CAPTCHA" => "N",
                "USER_MESSAGE_EDIT" => "",
                "USER_MESSAGE_ADD" => "",
                "DEFAULT_INPUT_SIZE" => 30,
                "RESIZE_IMAGES" => "Y",
                "MAX_FILE_SIZE" => 3145728,
                "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
                "CUSTOM_TITLE_IBLOCK_SECTION" => "���������",
                "CUSTOM_TITLE_NAME" => "���������",
                "CUSTOM_TITLE_PREVIEW_PICTURE" => "����",
                "CUSTOM_TITLE_263" => "�������������� ����",
                "CUSTOM_TITLE_DETAIL_TEXT" => "��������� ��������",
                "SEF_FOLDER" => "/",
                "VARIABLE_ALIASES" => array(),
                "THUMB_WIDTH" => 450,
                "THUMB_HEIGHT" => 150,
                "PAGE_TITLE" => "���������� ����������",
                "DEFAULT_PROP" => array(
                    "AUTHOR" => $USER->GetID()
                ),
                "PREVIEW_TEXT_USE_HTML_EDITOR" => "N"
            )
        );?>
        <?/*else:?>
   ����� �������� ����������� �������� ����������, ���������, ����������, <a href="/personal/profile/">������</a> � ����� ����� ����������
<?endif;*/?>
    </div>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        ".default",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/personal/advert/add_comment.php",
            "EDIT_TEMPLATE" => "standard.php"
        ),
        false
    );?>

<? endif; ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
