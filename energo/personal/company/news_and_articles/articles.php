<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������ ��������");
$APPLICATION->AddChainItem($APPLICATION->GetTitle());
?>

<?
$APPLICATION->IncludeComponent("bitrix:main.include", "title", Array(
    "AREA_FILE_SHOW" => "sect",
    "AREA_FILE_SUFFIX" => "title",
    "AREA_FILE_RECURSIVE" => "Y",
    "EDIT_MODE" => "html",
        ), false, Array('HIDE_ICONS' => 'Y')
);
?>

<?
$arrProductListFilter = array("PROPERTY_FIRM" => 1);
if(ENABLE_PREDSTAVITEL_MODE)
{
   $arPredstavitelInfo = PredstavitelGetByUser($USER->GetID());
   if($arPredstavitelInfo["RELATED"])
   {
      $arrProductListFilter = array("PROPERTY_FIRM" => $arPredstavitelInfo["RELATED"], "ACTIVE" => "");
      $element["ID"] = $arPredstavitelInfo["RELATED"];
   }
}
else
{
   if (!CModule::IncludeModule("iblock")) die();
   $objElement = new CIBlockElement();
   $res = $objElement->GetList(array(), array("IBLOCK_ID" => IBLOCK_COMPANY, "ACTIVE" => "Y", "PROPERTY_USER" => $USER->GetID()));
   if ($element = $res->GetNext())
      $arrProductListFilter = array("PROPERTY_FIRM" => $element["ID"], "ACTIVE" => "");
}

if ($_REQUEST['per_page'] > 0)
   $perPage = $_REQUEST['per_page'];
else
   $perPage = "20";

$APPLICATION->IncludeComponent("bitrix:news.list", "personal_news_list", array(
                  "IBLOCK_TYPE" => "information_part",
                  "IBLOCK_ID" => IBLOCK_ARTICLE,
                  "NEWS_COUNT" => $perPage,
                  "SORT_BY1" => "PROPERTY_SORT_COMPANY",
                  "SORT_ORDER1" => "DESC",
                  "SORT_BY2" => "ACTIVE_FROM",
                  "SORT_ORDER2" => "ASC",
                  "FILTER_NAME" => "arrProductListFilter",
                  "FIELD_CODE" => array("ACTIVE", "PREVIEW_PICTURE", "DETAIL_PICTURE"),
                  "PROPERTY_CODE" => array("FAVORITES", "FIRM", "NUMBER", "COST", "SORT_COMPANY"),
                  "CHECK_DATES" => "N",
                  "DETAIL_URL" => "",
                  "AJAX_MODE" => "N",
                  "AJAX_OPTION_JUMP" => "N",
                  "AJAX_OPTION_STYLE" => "Y",
                  "AJAX_OPTION_HISTORY" => "N",
                  "CACHE_TYPE" => "N",
                  "CACHE_TIME" => 3600*24,
                  "CACHE_FILTER" => "N",
                  "CACHE_GROUPS" => "Y",
                  "PREVIEW_TRUNCATE_LEN" => "",
                  "ACTIVE_DATE_FORMAT" => "d.m h:m",
                  "SET_TITLE" => "N",
                  "SET_STATUS_404" => "N",
                  "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                  "ADD_SECTIONS_CHAIN" => "N",
                  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                  "PARENT_SECTION" => "",
                  "PARENT_SECTION_CODE" => "",
                  "DISPLAY_TOP_PAGER" => "Y",
                  "DISPLAY_BOTTOM_PAGER" => "Y",
                  "PAGER_TITLE" => "�������",
                  "PAGER_SHOW_ALWAYS" => "Y",
                  "PAGER_TEMPLATE" => "defaultv2",
                  "PAGER_DESC_NUMBERING" => "N",
                  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                  "PAGER_SHOW_ALL" => "Y",
                  "DISPLAY_DATE" => "N",
                  "DISPLAY_NAME" => "N",
                  "DISPLAY_PICTURE" => "Y",
                  "DISPLAY_PREVIEW_TEXT" => "Y",
                  "AJAX_OPTION_ADDITIONAL" => "",
                  "EDIT_URL" => "admin_articles.php",
                  "COMPANY_ID" => $element["ID"]
                      )
        );
?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>