<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//���� ��������, � ������� �������������� ������� �
if(!CModule::IncludeModule("iblock")) return;
if(ENABLE_PREDSTAVITEL_MODE)
{
   $arPredstavitelInfo = PredstavitelGetByUser($USER->GetID());
   if($arPredstavitelInfo["RELATED"])
   {
      $arrProductListFilter = array("PROPERTY_FIRM" => $arPredstavitelInfo["RELATED"], "ACTIVE" => "");
      $arCompany = CIBlockElement::GetByID($arPredstavitelInfo["RELATED"])->GetNext();
   }
}
else
{
   
   $rsCompany = CIBlockElement::GetList(
      array("NAME" => "ASC", "SORT" => "ASC"),
      array(
         "IBLOCK_ID" => IBLOCK_COMPANY,
         "ACTIVE" => "Y",
         "PROPERTY_USER" => $USER->GetID()
      )
   );
   if($arCompany = $rsCompany->GetNext())
   {}
}
if(is_array($arCompany) && count($arCompany))
{
   $rsProducts = CIBlockElement::GetList(
      array(),
      array(
         "IBLOCK_ID" => IBLOCK_NEWS,
         "PROPERTY_FIRM" => $arCompany["ID"]
      )
   );
   while($product = $rsProducts->GetNext())
      $products[] = $product["ID"];
}
else LocalRedirect("/personal/company/");




?>

<div class="middle_col">
    <h1><?$APPLICATION->ShowTitle();?></h1>
    <div class="clearfix"></div>
<?$APPLICATION->IncludeComponent("whipstudio:product.add.form", "news", array(
      "COMPANY" => $arCompany,
      "PRODUCTS_IDS" => $products,
      "SEF_MODE" => "N",
      "IBLOCK_TYPE" => "information_part",
      "IBLOCK_ID" => IBLOCK_NEWS,
      "PROPERTY_CODES" => array(
          //1 �������
          "���������� � �������" => array(
              array(
                  "IBLOCK_SECTION",
                  "NAME",
                  "DETAIL_PICTURE",
                  //"DATE_ACTIVE_FROM",
                  "PREVIEW_TEXT",
                  "DETAIL_TEXT",
	              "TAGS"
               )
          )
      ),
      "PROPERTY_CODES_REQUIRED" => array(
         "IBLOCK_SECTION",
         "NAME",
         "DETAIL_TEXT",
         "DETAIL_PICTURE", "TAGS"
      ),
      "GROUPS" => array(5), // ������ ������� ����� ���������/�������������
      "STATUS_NEW" => "2",
      "STATUS" => array(1, 2, 3),
      "LIST_URL" => "/personal/company/news_and_articles/",
      "ELEMENT_ASSOC" => "", // �� ������ �������� ������ ����� � ��������������
      "ELEMENT_ASSOC_PROPERTY" => "",
      "MAX_USER_ENTRIES" => 1000000,
      "MAX_LEVELS" => 1, // ���������� ������ � ���������
      "LEVEL_LAST" => "N",
      "USE_CAPTCHA" => "N",
      "USER_MESSAGE_EDIT" => "������� ����� ������������ ����� ������������",
      "USER_MESSAGE_ADD" => "������� ����� ������������ ����� ������������",
      "DEFAULT_INPUT_SIZE" => 30,
      "RESIZE_IMAGES" => "Y",
      "MAX_FILE_SIZE" => 0,
      "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
      "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
      "CUSTOM_TITLE_IBLOCK_SECTION" => "���������",
      "CUSTOM_TITLE_NAME" => "",
      "CUSTOM_TITLE_DETAIL_PICTURE" => "�����������",
      "CUSTOM_TITLE_ACTIVE_FROM" => "���� �������",
      "CUSTOM_TITLE_DETAIL_TEXT" => "����� �������",
      "SEF_FOLDER" => "/",
      "VARIABLE_ALIASES" => array(),
      "THUMB_WIDTH" => 450,
      "THUMB_HEIGHT" => 150,
      "PAGE_TITLE" => "���������� �������",
      "PREMODERATION" => true
   )
);?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
