<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//���� ��������, � ������� �������������� ������� �
if ( ! CModule::IncludeModule( "iblock" ) ) {
    return;
}
if ( ENABLE_PREDSTAVITEL_MODE ) {
    $arPredstavitelInfo = LANG == 's1' ? PredstavitelGetByUser( $USER->GetID() ) : PredstavitelGetByUserAgro( $USER->GetID() );

    if ( $arPredstavitelInfo["RELATED"] ) {
        $arrProductListFilter = array(
            "PROPERTY_FIRM" => $arPredstavitelInfo["RELATED"],
            "ACTIVE"        => ""
        );
        $arCompany            = CIBlockElement::GetByID( $arPredstavitelInfo["RELATED"] )->GetNext();
    }
} else {
    $rsCompany = CIBlockElement::GetList(
        array( "NAME" => "ASC", "SORT" => "ASC" ),
        array(
            "IBLOCK_ID"     => IBLOCK_COMPANY,
            "ACTIVE"        => "Y",
            "PROPERTY_USER" => $USER->GetID()
        )
    );
    if ( $arCompany = $rsCompany->GetNext() ) {
    }
}
if ( is_array( $arCompany ) && count( $arCompany ) ) {
    $rsProducts = CIBlockElement::GetList(
        array(),
        array(
            "IBLOCK_ID"     => IBLOCK_PRODUCTS,
            "PROPERTY_FIRM" => $arCompany["ID"]
        )
    );
    while ( $product = $rsProducts->GetNext() ) {
        $products[] = $product["ID"];
    }
} else {
    LocalRedirect( "/personal/company/" );
}

$limits = CheckTarifLimit($arPredstavitelInfo['RELATED'], IBLOCK_PRODUCTS);
if ($limits['LIMIT'] <= 0) {
    $limits['LIMIT'] = 10;
}

?>
<div id="typeedit" data-type="IBLOCK_PRODUCTS"></div>

<div class="middle_col with-bar add-adv">
	<h1><?$APPLICATION->ShowTitle();?></h1>

<?
$arPROD = [];
if (intval($_REQUEST['OLD_ID'])>0){

$rsOld = CIBlockElement::GetList(
      array(  ),
      array(
          "ID"     => $_REQUEST['OLD_ID']
      )
  );
  if ( $arPROD = $rsOld->GetNext() ) {
  }
}
?>
	<!-- w <? // print_r($arCompany) ?> -->
<?$APPLICATION->IncludeComponent("whipstudio:product.add.form","", array(
      "COMPANY" => $arCompany,
      "OLD_NAME" => $arPROD['NAME'],
      "PRODUCTS_IDS" => $products,
      "SEF_MODE" => "N",
      "IBLOCK_TYPE" => "services",
      "IBLOCK_ID" => IBLOCK_PRODUCTS,
      "PROPERTY_CODES" => array(
          //1 �������
          "���������� � ������" => array(
              array(
	              "IBLOCK_SECTION",
	              "NAME",
	              GetPropertyId( "COST", IBLOCK_PRODUCTS ),
	              //����  GetPropertyId("SKYPE", IBLOCK_ADVARE),
	              GetPropertyId( "COST_NDS", IBLOCK_PRODUCTS ),
	              //���
	              GetPropertyId( "CURRENCY", IBLOCK_PRODUCTS ),
	              //������
	              GetPropertyId( "unit_tov", IBLOCK_PRODUCTS ),
	              //��. ���.
	              "PREVIEW_PICTURE",
	              GetPropertyId( "MORE_PHOTO", IBLOCK_PRODUCTS ),
	              //MORE_PHOTO
	              GetPropertyId( "FILES", IBLOCK_PRODUCTS ),
	              //FILES
	              "DETAIL_TEXT"
              )
          )
      ),
      "PROPERTY_CODES_REQUIRED" => array(
         "IBLOCK_SECTION",
         "NAME",
         "PREVIEW_PICTURE",
         "DETAIL_TEXT"
      ),
      "GROUPS" => array(5, 12), // ������ ������� ����� ���������/�������������
      "STATUS_NEW" => 1,
      "STATUS" => array(1, 2, 3),
      "LIST_URL" => "/personal/company/prod_and_serv/",
      "ELEMENT_ASSOC" => "", // �� ������ �������� ������ ����� � ��������������
      "ELEMENT_ASSOC_PROPERTY" => "",
      "MAX_USER_ENTRIES" => $limits['LIMIT'],
      "MAX_LEVELS" => 1, // ���������� ������ � ���������
      "LEVEL_LAST" => "N", "MIN_LEVEL"=>1,
      "USE_CAPTCHA" => "N",
      "USER_MESSAGE_EDIT" => "",
      "USER_MESSAGE_ADD" => "",
      "DEFAULT_INPUT_SIZE" => 30,
      "RESIZE_IMAGES" => "Y",
      "MAX_FILE_SIZE" => 0,
      "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
      "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
      "CUSTOM_TITLE_IBLOCK_SECTION" => "���������",
      "CUSTOM_TITLE_NAME" => "",
      "CUSTOM_TITLE_PREVIEW_PICTURE" => "����",
      "CUSTOM_TITLE_264" => "�������������� ����",
      "CUSTOM_TITLE_243" => "������������",
      "CUSTOM_TITLE_DETAIL_TEXT" => "��������� ��������",
      "SEF_FOLDER" => "/",
      "VARIABLE_ALIASES" => array(),
      "THUMB_WIDTH" => 450,
      "THUMB_HEIGHT" => 150,
      "PAGE_TITLE" => "���������� ������",
   )
);?>

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
