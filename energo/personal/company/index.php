<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$arPredstavitelInfo = false;
//���� ��������, � ������� �������������� ������� �
if(ENABLE_PREDSTAVITEL_MODE)
{

   $arPredstavitelInfo = PredstavitelGetByUser($USER->GetID());
   if((int)$arPredstavitelInfo["RELATED"] > 0)
   {
      $APPLICATION->SetTitle("���������� ���������");
      $sSectionName = "���������� ���������";
   }
    else
   {
       $APPLICATION->SetTitle("�������� ��������");
       $sSectionName = "�������� ��������";
   }
}
else
{
   if(!CModule::IncludeModule("iblock")) return;
    $rsCompany = CIBlockElement::GetList(
      array(),
      array(
         "IBLOCK_ID" => IBLOCK_COMPANY,
         "ACTIVE" => "Y",
         "PROPERTY_USER" => $USER->GetID()
      )
   );
   if($arCompany = $rsCompany->GetNext())
   {
      $APPLICATION->SetTitle("���������� ���������");
      $sSectionName = "���������� ���������";
   }
    else
   {
       $APPLICATION->SetTitle("�������� ��������");
       $sSectionName = "�������� ��������";
   }
}

$available = $arPredstavitelInfo['YARMARKA'] == '��';


//ForumActions();
//PrintAdmin($arPredstavitelInfo);
// GetPropertyId( "REGION_DOST", IBLOCK_COMPANY ), // �����
//print_r()
?>

<div class="middle_col">
    <h1><?$APPLICATION->ShowTitle();?></h1>
    <div class="clearfix"></div>
  <?$APPLICATION->IncludeComponent("whipstudio:iblock.element.add.form","", array(
        "PredstavitelInfo" => $arPredstavitelInfo,
        "SEF_MODE" => "N",
        "IBLOCK_TYPE" => "directories",
        "IBLOCK_ID" => IBLOCK_COMPANY,
        "PROPERTY_CODES" => array(
            //1 �������
            "������������ ����"   => array(
                array(
                    "NAME",
                    //GetPropertyId( "ALTER_TITLE", IBLOCK_COMPANY ),//ALTER_TITLE
                    GetPropertyId( "USER", IBLOCK_COMPANY ),
                    "IBLOCK_SECTION",
                    "DETAIL_TEXT",
                    GetPropertyId( "COUNTRY", IBLOCK_COMPANY ), // ������
                    GetPropertyId( "REGION", IBLOCK_COMPANY ), // ������
                    GetPropertyId( "CITY", IBLOCK_COMPANY ), // �����
                    GetPropertyId( "adress", IBLOCK_COMPANY ), // �����
                  //  GetPropertyId( "REGION_DOST", IBLOCK_COMPANY ),
                    GetPropertyId( "MAP", IBLOCK_COMPANY ), // �����
                    GetPropertyId( "phone", IBLOCK_COMPANY ), // �������
                    GetPropertyId( "EMAIL", IBLOCK_COMPANY ), // ��� email
                    GetPropertyId( "EMAIL_REQUEST", IBLOCK_COMPANY ), // email ��� ��������
                )
            ),
            //2 �������
            "�������������� ����" => array(
                array( "DETAIL_PICTURE" ),
                array(
                    GetPropertyId( "index", IBLOCK_COMPANY ), // ������
                    GetPropertyId( "adress_store", IBLOCK_COMPANY ), // ����� ������
                    GetPropertyId( "shedule", IBLOCK_COMPANY ), // ����� ������
                    GetPropertyId( "URL", IBLOCK_COMPANY ), // ���� ��������
                    GetPropertyId( "SKYPE", IBLOCK_COMPANY ), // �����
                    GetPropertyId( "PRICES", IBLOCK_COMPANY )    //�����-�����
                    //360 //����������� �����
                    //112, // �������� ����
                ),
                array(
                    GetPropertyId( "RASCHETNY", IBLOCK_COMPANY ), // ��������� ����
                    GetPropertyId( "ADRESS_BANK", IBLOCK_COMPANY ), // ����� �����
                    GetPropertyId( "UNN", IBLOCK_COMPANY ), // ���
                ),
              //  array(
              //      GetPropertyId( "ENGLISH_TITLE", IBLOCK_COMPANY ), // ENGLISH_TITLE
              //      GetPropertyId( "ENGLISH_DESCRIPTION", IBLOCK_COMPANY ), // ENGLISH_DESCRIPTION
              //  ),
                array(
                    GetPropertyId( "VIDEO_LINK", IBLOCK_COMPANY ), // ����� � youtube
                )
            ),
            /*"����������" => array(
				array(
					 GetPropertyId("METRIKA_ID", IBLOCK_COMPANY), // �������
				 )
			)*/
        ),
         "PROPERTY_CODES_REQUIRED" => array(
           "NAME",
           //305,
           "IBLOCK_SECTION",
           "DETAIL_TEXT",
              GetPropertyId( "phone", IBLOCK_COMPANY ),
              GetPropertyId( "EMAIL_REQUEST", IBLOCK_COMPANY ),
              GetPropertyId( "adress", IBLOCK_COMPANY ),
              GetPropertyId( "COUNTRY", IBLOCK_COMPANY ),
              GetPropertyId( "REGION", IBLOCK_COMPANY ),
              GetPropertyId( "CITY", IBLOCK_COMPANY ),
        ),
        "GROUPS" => array(5), // ������ ������� ����� ���������/�������������
        "STATUS_NEW" => 1,
        "STATUS" => array(1, 2, 3),
        "LIST_URL" => "",
        "ELEMENT_ASSOC" => "PROPERTY_ID", // �� ������ �������� ������ ����� � ��������������
        "ELEMENT_ASSOC_PROPERTY" => "USER",
        "MAX_USER_ENTRIES" => 10000,
        "MAX_LEVELS" => 100, // ���������� ������ � ���������
        "LEVEL_LAST" => "N",
        "USE_CAPTCHA" => "N",
        "USER_MESSAGE_EDIT" => "",
        "USER_MESSAGE_ADD" => "",
        "DEFAULT_INPUT_SIZE" => 30,
        "RESIZE_IMAGES" => "Y",
        "MAX_FILE_SIZE" => 0,
        "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
        "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
        "CUSTOM_TITLE_NAME" => "",
        "CUSTOM_TITLE_305" => "������������� ��������",
        "CUSTOM_TITLE_DETAIL_PICTURE" => "�������",
        "CUSTOM_TITLE_DETAIL_TEXT" => "���������� � ��������", 
        "CUSTOM_TITLE_IBLOCK_SECTION" => "����� ������������",
        "CUSTOM_TITLE_69" => "���� ��������",
        "CUSTOM_TITLE_70" => "Email ��� �����",
        "CUSTOM_TITLE_308" => "Email ��� ������",
        "CUSTOM_TITLE_102" => "��������� �� �����",    
        "CUSTOM_TITLE_316" => "Description in English",  
        "CUSTOM_TITLE_".GetPropertyId("ENGLISH_TITLE", IBLOCK_COMPANY) => "Title in english",
        "CUSTOM_TITLE_".GetPropertyId("METRIKA_ID", IBLOCK_COMPANY) => "����������",
        "SEF_FOLDER" => "/",
        "VARIABLE_ALIASES" => array()
     )
  );?>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
