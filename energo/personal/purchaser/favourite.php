<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");
$APPLICATION->AddChainItem($APPLICATION->GetTitle());
?>

<?/*
$APPLICATION->IncludeComponent("bitrix:main.include", "title", Array(
    "AREA_FILE_SHOW" => "sect",
    "AREA_FILE_SUFFIX" => "title",
    "AREA_FILE_RECURSIVE" => "Y",
    "EDIT_MODE" => "html",
        ), false, Array('HIDE_ICONS' => 'Y')
);
*/?>

<div class="middle_col">
    <h1><?$APPLICATION->ShowTitle();?></h1>
    <div class="clearfix"></div>


<?
if ((int)$_REQUEST['per_page'] > 0)
   $perPage = (int)$_REQUEST['per_page'];
else
   $perPage = "20";
?>


<?
global $arrFilterFavorites;
$arrFilterFavorites["PROPERTY_FAVORITES"] = $USER->GetID();
$APPLICATION->IncludeComponent("bitrix:news.list", "favorites", array(
                  "IBLOCK_TYPE" => "services",
                  "IBLOCK_ID" => IBLOCK_PRODUCTS,
                  "NEWS_COUNT" => $perPage,
                  "SORT_BY1" => "ACTIVE_FROM",
                  "SORT_ORDER1" => "DESC",
                  "SORT_BY2" => "SORT",
                  "SORT_ORDER2" => "ASC",
                  "FILTER_NAME" => "arrFilterFavorites",
                  "FIELD_CODE" => array("DETAIL_PICTURE", "PREVIEW_PICTURE", "DATE_CREATE"),
                  "PROPERTY_CODE" => array("COST", "FIRM"),
                  "CHECK_DATES" => "N",
                  "DETAIL_URL" => "",
                  "AJAX_MODE" => "N",
                  "AJAX_OPTION_JUMP" => "N",
                  "AJAX_OPTION_STYLE" => "Y",
                  "AJAX_OPTION_HISTORY" => "N",
                  "CACHE_TYPE" => "N",
                  "CACHE_TIME" => 3600*24,
                  "CACHE_FILTER" => "N",
                  "CACHE_GROUPS" => "Y",
                  "PREVIEW_TRUNCATE_LEN" => "",
                  "ACTIVE_DATE_FORMAT" => "d.m H:i",
                  "SET_TITLE" => "N",
                  "SET_STATUS_404" => "N",
                  "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                  "ADD_SECTIONS_CHAIN" => "N",
                  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                  "PARENT_SECTION" => "",
                  "PARENT_SECTION_CODE" => "",
                  "DISPLAY_TOP_PAGER" => "N",
                  "DISPLAY_BOTTOM_PAGER" => "Y",
                  "PAGER_TITLE" => "Новости",
                  "PAGER_SHOW_ALWAYS" => "Y",
                  "PAGER_TEMPLATE" => "market",
                  "PAGER_DESC_NUMBERING" => "N",
                  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                  "PAGER_SHOW_ALL" => "Y",
                  "DISPLAY_DATE" => "N",
                  "DISPLAY_NAME" => "N",
                  "DISPLAY_PICTURE" => "Y",
                  "DISPLAY_PREVIEW_TEXT" => "N",
                  "AJAX_OPTION_ADDITIONAL" => "",
                  "PAGE_ELEMENT_COUNT" => $perPage
                      )
        );
?>

</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>