<?php
/**
 * User: ZvvAsyA
 * Date: 13.08.15 -  14:52
 */

$_SERVER["DOCUMENT_ROOT"] = dirname(dirname(__FILE__));
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );

$ib = 17;

$row = 1;
if (($handle = fopen(dirname(__FILE__).'/'.$ib.'_file.csv', "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
        $id = $data[0];
        $block_id = $data[2];

        $new_rating = $data[4];
        $new_count = $data[6];
        $new_sum = $data[8];

        echo ' update '.$id;
        if ( CIBlockElement::SetPropertyValues($id, $block_id, $new_sum, 'vote_sum')){
            echo $new_sum." ";
        };
        if ( CIBlockElement::SetPropertyValues($id, $block_id, $new_count, 'vote_count')){
            echo $new_count." ";
        };
        if ( CIBlockElement::SetPropertyValues($id, $block_id, $new_rating, 'rating')){
            echo $new_rating."  \n";
        };
    }

    fclose($handle);
}

exit;

$rsAllElements = CIBlockElement::GetList(
	array( ),
	array(
		"IBLOCK_ID" => $ib, //"ID"=>28552 27
        "ACTIVE" => 'Y',

	), false, false, array('ID','ACTIVE','IBLOCK_ID', 'NAME', 'PROPERTY_rating', 'PROPERTY_vote_count', 'PROPERTY_vote_sum', 'SHOW_COUNTER')
);
$items = [];
while ($item = $rsAllElements->Fetch()){

       $phone = array();
       $res = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], "sort", "asc", array("CODE" => "phone"));
    $i = 0;
       while ($ob = $res->GetNext())
       {
          $item []= $ob['VALUE'];
       }
    while ($i++<10)
        $item []= $ob['VALUE'];

    $items []= $item;

}


$fp = fopen(dirname(__FILE__).'/'.$ib.'_file.csv', 'w');

foreach ($items as $fields) {
    fputcsv($fp, $fields);
}

fclose($fp);