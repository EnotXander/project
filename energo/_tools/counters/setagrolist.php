<?php
/**
 * User: ZvvAsyA
 * Date: 12.10.15 -  14:23
 */

$_SERVER["DOCUMENT_ROOT"] = dirname( dirname(dirname(__FILE__)));
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );


$ib = 65;//17;

$row = 1;
if (($handle = fopen(dirname(__FILE__).'/'.$ib.'_counters.csv', "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
        $id = $data[0];
        $block_id = $data[1];
        $MTK_ID = $data[3];

        if ( CIBlockElement::SetPropertyValues($id, $block_id, $MTK_ID, 'METRIKA_ID')){
            echo 'set '.$id." ok \n";
        };
        //if ($row > 3) die();
    }

    fclose($handle);
}

exit;
