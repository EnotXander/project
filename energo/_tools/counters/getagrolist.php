<?php
/**
 * User: ZvvAsyA
 * Date: 12.10.15 -  14:23
 */

$_SERVER["DOCUMENT_ROOT"] = dirname( dirname(dirname(__FILE__)));
require_once( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php" );
CModule::IncludeModule( "iblock" );

$ib = 65;
$rsAllElements = CIBlockElement::GetList(
	array( ),
	array(
		"IBLOCK_ID" => $ib, //"ID"=>28552 27
        "ACTIVE" => 'Y',

	), false, false, array('ID','ACTIVE','IBLOCK_ID', 'NAME')
);

$items = [];

while ($item = $rsAllElements->Fetch()){
       $phone = array();
       $res = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], "sort", "asc", array("CODE" => "METRIKA_ID"));
	   $counter = '';
       if ($ob = $res->GetNext())
       {
          $counter = $ob['VALUE'];
       }

    $items []= array('ID'=>$item['ID'], 'IBLOCK_ID'=>$item['IBLOCK_ID'], 'NAME'=>$item['NAME'], 'METRIKA_ID'=>$counter);
}


$fp = fopen(dirname(__FILE__).'/'.$ib.'_counters.csv', 'w');

foreach ($items as $fields) {
    fputcsv($fp, $fields);
}

fclose($fp);