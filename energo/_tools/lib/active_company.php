<?php
/**
 * User: ZvvAsyA
 * Date: 24.11.15 -  13:51
 */


function getActiveCounter($FILTER = '', $agro=false) {

	CModule::IncludeModule("iblock");

	$objElement = new CIBlockElement();
	$firmArr = array();
	$fullCompaniesCnt = 0;

	$IBLOCK_PRODUCTS = $agro?58:27;

	//получение товаров
	$arFilter = array(
	    "IBLOCK_ID" => $IBLOCK_PRODUCTS, // 58
	    "!PROPERTY_FIRM" => false
	);
	$res = $objElement->GetList(array(), $arFilter, array("PROPERTY_FIRM"));

	$fullTovarsCnt = 0;
	while($element = $res->GetNext())
	{
	    if (!isset($firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]]))
	    {
	        $fullCompaniesCnt++;
	        $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] = 0;
	    }
	    $fullTovarsCnt += $element['CNT'];
	    $firmArr[$IBLOCK_PRODUCTS][$element["PROPERTY_FIRM_VALUE"]] = $element['CNT'];
	    $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] += $element['CNT'];
	}

	$IBLOCK_USLUGI =$agro?64:28;

	//получение услуг
	$arFilter = array(
	    "IBLOCK_ID" => $IBLOCK_USLUGI, //64
	    "!PROPERTY_FIRM" => false
	);
	$res = $objElement->GetList(array(), $arFilter, array("PROPERTY_FIRM"));

	$fullServicesCnt = 0;
	while($element = $res->GetNext())
	{
	    if (!isset($firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]]))
	    {
	        $fullCompaniesCnt++;
	        $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] = 0;
	    }
	    $fullServicesCnt += $element['CNT'];
	    $firmArr[$IBLOCK_USLUGI][$element["PROPERTY_FIRM_VALUE"]] = $element['CNT'];
	    $firmArr['ALL'][$element["PROPERTY_FIRM_VALUE"]] += $element['CNT'];
	}

	$IBLOCK_COMPANY = $agro?65:17;

	if ($FILTER == 'OFF') {

		//получение компаний
		$arFilter = array(
		    "IBLOCK_ID" => $IBLOCK_COMPANY,
		    "ID" => array_keys($firmArr['ALL']),
		    "!PROPERTY_REMOVE_REL" => false
		);
	} else {

		//получение компаний
		$arFilter = array(
		    "IBLOCK_ID" => $IBLOCK_COMPANY,
		    "ID" => array_keys($firmArr['ALL']),
		    "=PROPERTY_REMOVE_REL" => false
		);
	}

	$res = $objElement->GetList(
	    array("NAME"=>"ASC", "ID"=>"ASC"),
	    $arFilter,
	    false,
	    false,
	    array('ID', 'IBLOCK_ID', 'PROPERTY_METRIKA_ID')
	);

	$arCounters = [];

	while($element = $res->GetNext())
	{
	    $arCounters[$element['ID']] =$element['PROPERTY_METRIKA_ID_VALUE'];
	}

	$arCounters = array_unique($arCounters);



	return $arCounters;
}
