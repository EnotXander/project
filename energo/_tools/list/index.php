<?php
/**
 * User: ZvvAsyA
 * Date: 23.06.15 -  18:03
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");?>

<? $APPLICATION->IncludeComponent(
				"zvv:news.line",
				"bloglist-mainpage",
				array(
					"IBLOCK_TYPE" => "information_part",
	                "IBLOCKS" => Array(50),
	                "NEWS_COUNT" => "3",
	                "FIELD_CODE" => Array("ID", "CODE"),
	                "SORT_BY1" => "ID",
	                "SORT_ORDER1" => "DESC",
	                "SORT_BY2" => "DATE_CREATE",
	                "SORT_ORDER2" => "DESC",
	                "DETAIL_URL" => "",
	                "ACTIVE_DATE_FORMAT" => "d.m.y",
	                "CACHE_TYPE" => "A",
	                "CACHE_TIME" => "3600",
	                "CACHE_GROUPS" => "N"

				),
				false
			); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>